<?php

namespace app\api\controller\tool;

use controller\BasicApi;
use \think\facade\Cache;
use think\facade\Request;
use service\LogService;
use think\Db;
use org\RegexCheck;
class Email extends BasicApi
{
    /**
     * 发送邮件
     * http://door.mydanweb.com/api/tool.email/send?tomail=7795442@qq.com&subject=邮件发送测试2222&body=<h>RONGKANGSEN</h>
     *
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */

    function send ($tomail=false,$subject=false,$body=false)
    {

        //TODO 发送条件
        if (false==$tomail) $tomail = $this->request->request('tomail', '', 'trim');//要发送的邮箱地址

        if (!RegexCheck::email($tomail)) {
            $this->apiError('发送邮件格式不正确');
        }

        if (false==$subject) $subject = $this->request->request('subject', '', 'trim');//自定义邮件标题
        if (empty($subject)) {
            $this->apiError('参数subject为空');
        }
        if (false==$body) $body = $this->request->request('body', '', 'trim');//自定义内容
        if (empty($body)) {
            $this->apiError('参数body为空');
        }
        //发送邮件
        $res = sendmail($tomail, $subject, $body, '');
        if ($res == true) {
            $this->apiSuccess($res, '发送邮件成功');
        } else {
            $this->apiError('发送邮件失败');
        }
    }
}
