<?php

namespace app\api\controller\tool;

use controller\BasicApi;
use \think\facade\Cache;
use think\facade\Request;
use service\LogService;
use service\DataService;
use org\RegexCheck;
use think\Db;

class Tools extends BasicApi
{
    /**
     * 发送邮件，点开链接修改密码
     *  http://door.mydanweb.com/api/tool.Tools/updatePasswd?passwd=1
     *
     */

    public function updatePasswd()
    {
        $db = Db::name('users');
        $reset_md5 =   $this->request->request('passwd', '', 'trim');
        if (false==$reset_md5) {
            $this->apiError('passwd参数错误!');
        }
        $res = $db->where("reset_md5='{$reset_md5}'")->find();
        if (false==$res) {
            $this->apiError('未找到对应的数据!');
        }
        $data = [];
        // $data['password'] = md5($res['update_passwd']);
        $data['password'] = sysmd5($res['update_passwd']);
        $data['updatetime'] = time();
        $data['update_passwd'] = "";   //-1表示失败
        $data['reset_md5'] = "";
        $data['id'] = $res['id'];

        try {
            $res = DataService::save('users', $data);
            if ($res) {
                $this->apiSuccess($res, '操作成功');
            } else {
                $this->apiError('操作失败');
            }
        } catch (\Exception $e) {
            $this->apiError('操作失败' . $e->getMessage());
        }


    }


    //通过邮件申请更新密码前准备的数据
    public function afterUpdatePasswd()
    {

        $appid = $this->request->request('appid', '', 'trim');
        empty($appid) && $this->apiError('appid为空!');

        $username = $this->request->request('username', '', 'trim');
        if(empty($username)) $this->apiError('登录账号必输');
        if(isset($username) && strlen($username) < 2) $this->apiError('登录账号长度不能少于2位有效字符!');

        //信息验证
        if (RegexCheck::email($username)) {
            $email = $username;
            unset($username);
        }

        $password = $this->request->request('password', '', 'trim');
        empty($password) && $this->apiError('密码必输!');
        (strlen($password) < 6 || strlen($password) > 16) && $this->apiError('登录密码长度不能少于6位,不能多于16位有效字符!');


        // 用户信息验证
        $map =[];
        isset($email) &&  $map[] = ['email','=',$email];
        isset($username) &&  $map[] = ['username','=',$username];
        $userInfo = Db::name('users')->where($map)->find();

        (false==$userInfo) && $this->apiError('登录账号不存在，请重新输入!');
        (false==$userInfo['status']) && $this->apiError('账号已经被禁用，请联系管理!');

        $data = [];
        $data['update_passwd'] = $password;
        $data['reset_md5'] = md5($userInfo['id'].$password.time());
        $data['id'] = $userInfo['id'];

        try {
            $res = DataService::save('users', $data);
            if ($res) {
                $this->apiSuccess($res, '操作成功');
            } else {
                $this->apiError('操作失败');
            }
        } catch (\Exception $e) {
            $this->apiError('操作失败' . $e->getMessage());
        }


    }

}
