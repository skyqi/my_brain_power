<?php

namespace app\api\controller\tool;

use controller\BasicApi;
use think\facade\Request;
use service\LogService;
use think\Db;
use org\RegexCheck;
/*
 *  推送消息,极光推送
 *  极光类型
 *  https://www.jiguang.cn
 *  帮助说明：http://note.youdao.com/noteshare?id=2799a8c6efe32820c006593cbe4aab5e&sub=047E60B04934475CA46E52FEED87C526
 */
use JPush;
use JPush\Client as JPushClient;

class Pushmess extends BasicApi
{
    const  APPKEY ="2c9e591c41f9982e269c25ae";
    const  SECRET ="b90d22040fbeae4b663332ce";


    //极光推送appkey
    static public function app_key()
    {
        $app_key = self::APPKEY;
        return $app_key;
    }

    //极光推送master_secret
    static public function master_secret()
    {
        $master_secret = self::SECRET;
        return $master_secret;
    }

    //获取alias和tags
    public function getDevices($registrationID)
    {
       // require_once APP_PATH . '/../extend/jpush/autoload.php';
        $app_key = $this->app_key();
        $master_secret = $this->master_secret();
        $client = new JPushClient($app_key, $master_secret);
        $result = $client->device()->getDevices($registrationID);
        return $result;

    }

    //添加tags
    public function addTags($registrationID, $tags)
    {
       // require_once APP_PATH . '/../extend/jpush/autoload.php';
        $app_key = $this->app_key();
        $master_secret = $this->master_secret();
        $client = new JPushClient($app_key, $master_secret);
        $result = $client->device()->addTags($registrationID, $tags);
        return $result;

    }

    //移除tags
    public function removeTags($registrationID, $tags)
    {

       // require_once APP_PATH . '/../extend/jpush/autoload.php';

        $app_key = $this->app_key();
        $master_secret = $this->master_secret();
        $client = new JPushClient($app_key, $master_secret);
        $result = $client->device()->removeTags($registrationID, $tags);
        return $result;

    }

    //标签推送
    public function push($regID)
    {

       // require_once APP_PATH . '/../extend/jpush/autoload.php';

        $app_key = $this->app_key();
        $master_secret = $this->master_secret();


        $client = new JPushClient($app_key, $master_secret);
       // $regID = "190e35f7e014655875a";
        $push_payload = $client->push()
            ->setPlatform('all')
       //     ->addAllAudience()
            ->addRegistrationId($regID)
            ->setNotificationAlert('Hi, JPush '."你好，时间".date("H:i:s"));
        try {
            $response = $push_payload->send();
            prt($response);
        } catch (\JPush\Exceptions\APIConnectionException $e) {
            // try something here
            prt($e);
        } catch (\JPush\Exceptions\APIRequestException $e) {
            // try something here
            prt($e);
        }


        die;
        /*
         *  使用分类及标签，报错的,使用指定设备和所有人不报错
        $tags = implode(",", $tag);
        $client->push()
            ->setPlatform(array('ios', 'android'))
            ->addTag($tags)//标签
            ->setNotificationAlert($alert)//内容
            ->send();
        */
    }

    //别名推送
    public function push_a($alias, $alert)
    {

        // require_once APP_PATH . '/../extend/jpush/autoload.php';

        $app_key = $this->app_key();
        $master_secret = $this->master_secret();

        $client = new JPushClient($app_key, $master_secret);
        $alias = implode(",", $alias);
        $client->push()
            ->setPlatform(array('ios', 'android'))
            ->addAlias($alias)//别名
            ->setNotificationAlert($alert)//内容
            ->send();

    }

    //示例：http://door.mydanweb.com/api/tool.Pushmess/ex?regid=

    public function ex()
    {
        $RegistrationID = $_REQUEST['regid'];


        $this->push($RegistrationID);
    }

}