<?php

namespace app\api\controller\tool;

use controller\BasicApi;
use \think\facade\Cache;
use think\facade\Request;
use service\LogService;
use service\DataService;
use org\RegexCheck;
use think\Db;

class Resetpasswd extends BasicApi
{

    //发送密码重置邮件
    //http://door.mydanweb.com/api/tool.Reset/SendResetMail
    public function SendResetMail()
    {
   
        $email = new Email();
        $resID = $this->afterUpdatePasswd();
        if ($resID>0) {
            $userInfo = Db::name('users')->find($resID);
            $subject = "找回密码";
            $appDomain = $this->request->root(true);
            $url =  $appDomain."/api/tool.Resetpasswd/updatePasswd?passwd=".$userInfo['reset_md5'];
            $tpl = "Door用户：";
            $tpl .= "   您好！请点击链接重启密码";
            $tpl .= "<a href='{$url}'>".$url."</a>";
            $tpl .= "\r\n";
            $tpl .= "                                               时间：".date("Y-m-d H:i:s");
            $email->send($userInfo['email'],$subject,$tpl);
        }
    }

	//忘记密码发送邮件获取验证码修改密码
    //   api/tool.Resetpasswd/SendCodeMail
    public function SendCodeMail()
    {
		$appid = $this->request->request('appid', '', 'trim');
        empty($appid) && $this->apiError('appid为空!');
		$e_mail = $this->request->request('email', '', 'trim');
        empty($e_mail) && $this->apiError('email为空!');
        $code = $this->request->request('code', '', 'trim');
        if (false==$code) {
            $code=mt_rand(1000,9999);
        }
		$userInfo = Db::name('users')->where(['email' => $e_mail])->find();
		 
        $email = new Email();
        //$userInfo['email'] = "7795442@qq.com";
        if ($userInfo['id']>0) {
        	          
            Db::name('users')->where(['id'=>$userInfo['id']])->update(['emailcode'=>$code]);             
             

            $subject = "发送验证码";
            $tpl = "Door用户：";
            $tpl .= "   您好！您的验证码为:".$code;
            $tpl .= "<br>";
            $tpl .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 时间：".date("Y-m-d H:i:s");
            $res =$email->send($userInfo['email'],$subject,$tpl);

        } else {
            $this->apiError('此邮未注册');
        }
    }

    /**
     * 发送邮件，点开链接修改密码
     *  http://chekumen.maxhom.cn/api/tool.Tools/updatePasswd?passwd=1
     *
     */

    // public function updatePasswd()
    // {
    //     $db = Db::name('users');
    //     $reset_md5 =   $this->request->request('passwd', '', 'trim');
    //     if (false==$reset_md5) {
    //         $this->apiError('passwd参数错误!');
    //     }
    //     $res = $db->where("reset_md5='{$reset_md5}'")->find();
    //     if (false==$res) {
    //         $this->apiError('未找到对应的数据!');
    //     }
    //     $data = [];
    //     $data['password'] = md5($res['update_passwd']);
    //     $data['updatetime'] = time();
    //     $data['update_passwd'] = "";   //-1表示失败
    //     $data['reset_md5'] = "";
    //     $data['id'] = $res['id'];

    //     try {
    //         $res = DataService::save('users', $data);
    //         if ($res) {
    //             $this->apiSuccess($res, '操作成功');
    //         } else {
    //             $this->apiError('操作失败');
    //         }
    //     } catch (\Exception $e) {
    //         $this->apiError('操作失败' . $e->getMessage());
    //     }


    // }


    // //通过邮件申请更新密码前准备的数据
    // public function afterUpdatePasswd()
    // {

    //     $appid = $this->request->request('appid', '', 'trim');
    //     empty($appid) && $this->apiError('appid为空!');

    //     $username = $this->request->request('username', '', 'trim');
    //     if(empty($username)) $this->apiError('登录账号必输');
    //     if(isset($username) && strlen($username) < 2) $this->apiError('登录账号长度不能少于2位有效字符!');

    //     //信息验证
    //     if (RegexCheck::email($username)) {
    //         $email = $username;
    //         unset($username);
    //     }

    //     $password = $this->request->request('password', '', 'trim');
    //     empty($password) && $this->apiError('密码必输!');
    //     (strlen($password) < 6 || strlen($password) > 16) && $this->apiError('登录密码长度不能少于6位,不能多于16位有效字符!');


    //     // 用户信息验证
    //     $map =[];
    //     isset($email) &&  $map[] = ['email','=',$email];
    //     isset($username) &&  $map[] = ['username','=',$username];
    //     $userInfo = Db::name('users')->where($map)->find();

    //     (false==$userInfo) && $this->apiError('登录账号不存在，请重新输入!');
    //     (false==$userInfo['status']) && $this->apiError('账号已经被禁用，请联系管理!');

    //     $data = [];
    //     $data['update_passwd'] = $password;
    //     $data['reset_md5'] = md5($userInfo['id'].$password.time());
    //     $data['id'] = $userInfo['id'];

    //     try {
    //         $res = DataService::save('users', $data);
    //         if ($res) {
    //           //  $this->apiSuccess($res, '操作成功');
    //             return $data['id'];
    //         } else {
    //             $this->apiError('操作失败');
    //             return 0;
    //         }
    //     } catch (\Exception $e) {
    //          $this->apiError('操作失败' . $e->getMessage());
    //     }


    // }

    // 修改个人中心（登录后）的密码
    public function updatePersonalCenterPasswd()
    {
        //验证登录状态
        $this->verfiyToken();

        //验证appid
        $appid = $this->request->request('appid', '', 'trim');
        empty($appid) && $this->apiError('appid为空!');
        $map = $_REQUEST['map'];
        $update = $_REQUEST['update'];
        empty($update['newpasswd']) && $this->apiError('newpasswd为空!');

        empty($map['email']) && $this->apiError('email为空!');
        empty($map['oldpasswd']) && $this->apiError('oldpasswd为空!');

        if (strlen($update['newpasswd'])<6 || strlen($update['newpasswd'])>16 ) {
        	$this->apiError('newpasswd长度不能少于6位并且不得大于16位');
        }

		$userInfo = session("users");
		if (false==$userInfo) {
		    $this->apiError('未登录或登录已经失效!');
		}

        $oldpasswd = sysmd5($map['oldpasswd']);
        $db = Db::name('users');
        $userRes = $db->where(array('email'=>$map['email'],'password'=>$oldpasswd,'id'=>$userInfo['id'],'status'=>1))->find();
        if (false==$userRes) {
            $this->apiError('系统未找到符合条件的值');
        }

       
        
 
        $data = [];
        $data['password'] = sysmd5($update['newpasswd']);
        $data['updatetime'] = time();
        $data['id'] =$userRes['id'];
       
        try {
            $res = DataService::save('users', $data);
            if ($res) {
                $this->apiSuccess($res, '操作成功');
            } else {
                $this->apiError('操作失败');
            }
        } catch (\Exception $e) {
            $this->apiError('操作失败' . $e->getMessage());
        }


    }

    //忘记密码
    //  http://chekumen.maxhom.cn/api/tool.Resetpasswd/forgetpassword?appid=53eead42b14ef9bb9834bfb35a48c5d0&email=rongkangsen@163.com
    function forgetpassword ()
    {
    	//检测AppID状态
        parent::verfiyAppID();
       
         //验证appid
        $appid = $this->request->request('appid', '', 'trim');
        empty($appid) && $this->apiError('appid为空!');
        $map = $_REQUEST['map'];
        $update = $_REQUEST['update'];
        empty($update['newpasswd']) && $this->apiError('newpasswd为空!');
        empty($map['email']) && $this->apiError('email为空!');
        empty($map['code']) && $this->apiError('code为空!');

        

        if (strlen($update['newpasswd'])<6 || strlen($update['newpasswd'])>16 ) {
        	$this->apiError('newpasswd长度不能少于6位并且不得大于16位');
        }
        $db = Db::name('users');
        $userRes = $db->where(array('email'=>trim($map['email'])))->find();

        if ($userRes['status']==0) {
        	$this->apiError('登录账号已经被锁定');
        }
         
        if ($userRes['code']!=$map['emailcode']) {
        	if ($userRes['emailcode_time']>0 && $userRes['emailcode_time']-time()>3600*2) {
        		//大于2小时
        		Db::name('users')->where(array('id'=>$userRes['id']))->update(array('emailcode_time'=>0,'verfiy_emailcode_err_num'=>0));
        	} else {

        		$verfiy_emailcode_err_num = $userRes['verfiy_emailcode_err_num']++ ; 
       		
        		Db::name('users')->where(array('id'=>$userRes['id']))->update(array('emailcode_time'=>time(),'verfiy_emailcode_err_num'=>$verfiy_emailcode_err_num));
        		if ($verfiy_emailcode_err_num>10) {
        			$this->apiError('您在2小时内尝试次数过多，请过2小时在试');
        		}
        	}
        	
        	Db::name('users')->where(['id'=>$userInfo['id']])->update(['emailcode'=>$code]);  
        	$this->apiError('邮件验证码不正确');
        }

        $data = [];
        $data['password'] = sysmd5($update['newpasswd']);
        $data['updatetime'] = time();
        $data['code'] = '';
        $data['id'] =$userRes['id'];
       
        try {
            $res = DataService::save('users', $data);
            if ($res) {
                $this->apiSuccess($res, '操作成功');
            } else {
                $this->apiError('操作失败');
            }
        } catch (\Exception $e) {
            $this->apiError('操作失败' . $e->getMessage());
        }
    }

}
