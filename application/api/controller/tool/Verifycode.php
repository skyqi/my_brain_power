<?php

namespace app\api\controller\tool;

use controller\BasicApi;
use \think\facade\Cache;
use think\facade\Request;
use service\LogService;
use think\Db;
use think\captcha\Captcha;

class Verifycode extends BasicApi
{
    /**
     * 图文验证码获取
     */
    function verify ()
    {
        // 自定义验证码格式
        $config = [
            // 验证码字体大小
            'fontSize' => 60,
            // 验证码位数
            'length'   => 4,
            // 关闭验证码杂点
            'useNoise' => false,
            // 验证码图片高度
            // 'imageH'   => 38,
            // 验证码图片宽度
            // 'imageW'   => 200,
            // 验证码过期时间（s）
            'expire'   => 1800,
        ];
        $captcha = new Captcha($config);

        return $captcha->entry();
    }

    function check_verifycode(){
        $verify_code = $this->request->request('verify_code', '','trim');
        //验证验证码
        if (captcha_check($verify_code)) {
            // 验证通过
            $this->apiSuccess(true,'验证通过');
        }else{
            $this->apiError('验证失败');

        }
    }



}
