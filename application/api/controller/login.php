<?php

namespace app\api\controller;

use controller\BasicApi;
use \think\facade\Cache;
use think\facade\Request;
use service\LogService;
use think\Db;

class login extends BasicApi
{
    /**
     * 登录接口
     * http://naoli.dev.com:81/api/login/login
     *
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    function login ()
    {
        // 输入数据效验
        $username = $this->request->post('username', '', 'trim');
        $password = $this->request->post('password', '', 'trim');
        $appid = $this->request->post('appid', '', 'trim');

        strlen($username) < 4 && $this->apiError('登录账号长度不能少于4位有效字符');
        strlen($password) < 4 && $this->apiError('登录密码长度不能少于4位有效字符');
        empty($appid) && $this->apiError('appid必输');
        empty($username) && $this->apiError('登录账号必输');
        empty($password) && $this->apiError('密码必输');

        // 用户信息验证
        $user = Db::name('SystemUser')->where('username', $username)->find();
        // prt($user);
        empty($user) && $this->apiError('登录账号不存在,请重新输入');
        ($user['password'] !== md5($password)) && $this->apiError('登录密码与账号不匹配,请重新输入');
        empty($user['status']) && $this->apiError('账号已经被禁用,请联系管理');

        // 更新登录信息
        $data = ['login_at' => ['exp', 'now()'], 'login_num' => ['exp', 'login_num+1']];
        Db::name('SystemUser')->where(['id' => $user['id']])->update($data);
        session('user', $user);

        LogService::write('用户登录', '用户登录成功');
        $Info['appid'] = $appid;
        if ($user['identity']) {
            $Identity_info = DB::name($user['identity'])->where('id', $user['identity_id'])->find();;
            $Info['identity_info'] = $Identity_info;
        }

        // token 生成
        $login_token = $this->createToken($user['id'], $user['username'], $appid);

        $expireTime = intval($_REQUEST['expireTime']);
        $expireTime = $expireTime ? $expireTime : 60 * 60 * 24 * 365;

        //缓存用户信息
        $res = Cache::set($login_token, $Info, $expireTime);//存储缓存
        if ($res) {
            $this->apiSuccess(array('token' => $login_token), '登录成功');
        } else {
            $this->apiError('登录失败');
        }
    }

    /**
     * 退出登录
     */
    public function logout() {
        $login_token = $this->request->post('token', '', 'trim');
        $this->verfiyToken();
        Cache::rm($login_token);
        session("login",null);
        $this->apiSuccess(null,'成功注销');
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index ()
    {
        //
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create ()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request $request
     * @return \think\Response
     */
    public function save (Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int $id
     * @return \think\Response
     */
    public function read ($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int $id
     * @return \think\Response
     */
    public function edit ($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request $request
     * @param  int $id
     * @return \think\Response
     */
    public function update (Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int $id
     * @return \think\Response
     */
    public function delete ($id)
    {
        //
    }


}
