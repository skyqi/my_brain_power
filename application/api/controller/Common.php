<?php

namespace app\api\controller;

use controller\BasicApi;

class Common extends BasicApi
{
    /**
     * 获取appid
     * @return mixed
     */
    public function getAppid(){
        $appid = config("api.AppID");
        session('AppId',$appid);
        $this->apiSuccess(['appid'=>$appid],'获取appid成功');
    }
}
