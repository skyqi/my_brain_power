<?php

namespace app\api\controller\door;

use controller\BasicApi;
use think\Validate;
use think\Db;
use think\db\Query;
use service\DataService;
use service\LogService;
use org\RegexCheck;
use \think\facade\Cache;


class User extends BasicApi
{
    /**
     * 登录 ，用户名或者邮箱
     */
    function getinfo ()
    {
        // 输入数据效验
        $id = $this->request->request('id', '', 'trim');
        $token = $this->request->request('token', '', 'trim');
        $appid = $this->request->request('appid', '', 'trim');

        //信息验证
        empty($appid) && $this->apiError('appid必输');
        empty($token) && empty($id) && $this->apiError('登录账号id，或者token为空');


        // 用户信息验证

        //还没登录
        if (false != $id) {
            $userInfo = Db::name('users')->find($id);
        }

        //已登录
        if ($token != false) {
            $this->verfiyToken();
            $userInfo = session('users');
        }

        if ($userInfo) {
            //token 为登录的标志 e897d58e9c1ff78dc737f03d9185bb97bb7b1407
            $this->apiSuccess(array($userInfo), '操作成功');
        } else {
            $this->apiError('用户信息不存在');
        }
    }

}