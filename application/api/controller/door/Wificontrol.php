<?php

namespace app\api\controller\door;

use controller\BasicApi;
use think\Validate;
use think\Db;
use think\db\Query;
use service\DataService;
use service\LogService;
use org\RegexCheck;
use \think\facade\Cache;


class Wificontrol extends BasicApi
{
    /**
     * 获取控制器列表
     */
    function wifictlList ()
    {
        //检测登录状态
        $this->verfiyToken();

        $appid = $this->request->request('appid', '', 'trim');
        empty($appid) && $this->apiError('appid为空');

        //获取用户信息
        $userInfo = session("users");
        // prt($userInfo);

        try {
            $list = Db::name('wificontrol')->where(['user_id' => $userInfo['id'], 'status' => 1])->select();
            // prt(Db::name('wificontrol')->getLastSql());
            if (!empty($list)) {
                foreach ($list as $key => & $val) {
                    $doorList = Db::name('cardoor')->where(['user_id' => $userInfo['id'], 'wificontrol_id' => $val['id']])->select();
                    if(!empty($doorList)){
                        $val['doorlists'] = $doorList;
                    }
                    unset($doorList);
                    //获取用户信息
                    $macaddress =$val['macaddress'];
                    $mac_count = DB::name("client_online")->where("mac='{$macaddress}'")->count();
                    $val['online'] = ($mac_count>0)?1:0;
                }
                $this->apiSuccess($list, '获取成功');
            } else {
                $this->apiError('未找到控制器');
            }
        } catch (\Exception $e) {
            $this->apiError('获取失败');
        }
    }

    /**
     * 新增控制器
     */
    public function addWifictl ()
    {
        //验证登录状态
        $this->verfiyToken();
        //验证appid
        $appid = $this->request->request('appid', '', 'trim');
        empty($appid) && $this->apiError('appid必输');

        $postdata = $_REQUEST['data'];

        empty($postdata['wificontrol']) && $this->apiError('控制器名称必输');
        (strlen($postdata['wificontrol']) <2|| strlen($postdata['wificontrol']) > 30) && $this->apiError('设备名称长度不能少于2位,不能多于30位有效字符');

        //获取用户信息
        $macaddress = $this->macTocStr($postdata['macaddress']);
        $userInfo = session("users");
        $data['user_id'] = $userInfo['id'];
        $data['createtime'] = time();
        $data['macaddress'] = $macaddress;
        $postdata['wificontrol']  && $data['wificontrol'] = $postdata['wificontrol'];
        $db = Db::name('wificontrol');
        if (!empty($postdata['macaddress'])) {
            $data['macaddress'] = $this->macTocStr($postdata['macaddress']);
            if (Db::name('wificontrol')->where(['user_id'=>$userInfo['id'],'macaddress'=>$macaddress])->count()>0) {
                $this->apiError('该设备已被添加');
            }
        }

        if (false!=$postdata['wificontrol']) {
            $map = [];
         
            $map[] = ['wificontrol','=',$macaddress];
            $map[] = ['user_id','=',$userInfo['id']];
            $count = $db->where($map)->count();
            if ($count>0) {
                $this->apiError('控制器名称已经存在');
            }
        }

        try {
            $res = DataService::save($db, $data);
            if ($res) {
                $mac_count = DB::name("client_online")->where("mac='{$macaddress}'")->count();
                $res['online'] = ($mac_count>0)?1:0;
                $this->apiSuccess($res, '新增成功');
            } else {
                $this->apiError('新增失败');
            }
        } catch (\Exception $e) {
            $this->apiError('新增失败' . $e->getMessage());
        }

    }




    /**
     * 修改控制器
     */
    public function editWifictl ()
    {
        //验证登录状态
         $this->verfiyToken();
         //验证appid
         $appid = $this->request->request('appid', '', 'trim');
         empty($appid) && $this->apiError('appid为空');
         $data = $_REQUEST['data'];
         empty($data['id']) && $this->apiError('缺少参数id');
         intval($data['id'])==0  && $this->apiError('参数id错误');
         if (false==$data['wificontrol'] &&  false==$data['macaddress']){
             $this->apiError('控制器和地址不能都为空,二者必选一');
         }


        $id = $data['id'];
        //获取用户信息
        $userInfo = session("users");
        $data['user_id'] = $userInfo['id'];          $data['updatetime'] = time();

        $db = Db::name('wificontrol');
        $count = $db->where("id=".$id)->count();
        if ($count ==0 )  $this->apiError('未找到id记录');

        if (!empty($data['wificontrol'])) {
            $map = [];
            $map[] = ['wificontrol','=',$data['wificontrol']];
            $map[] = ['user_id','=',$userInfo['id']];
            $map[] = ['id','<>',$id];
            $count = Db::name('wificontrol')->where($map)->count();

            if ($count>0) {
                $this->apiError('控制器名称已经存在');
            }
        }

        if (!empty($data['macaddress'])) {
            $data['macaddress'] = $this->macTocStr($data['macaddress']);
        }

        try {
            $res = DataService::save($db, $data);
            if ($res) {
                $mac_count = DB::name("client_online")->where("mac='{$macaddress}'")->count();
                $res['online'] = ($mac_count>0)?1:0;
                $this->apiSuccess($res, '修改成功');
            } else {
                $this->apiError('修改失败');
            }
        } catch (\Exception $e) {
            $this->apiError('修改失败' . $e->getMessage());
        }
    }



    /**
     * 删除控制器
     */
    public function delWifictl ()
    {
        //验证登录状态
        $this->verfiyToken();
        //验证appid
        $appid = $this->request->request('appid', '', 'trim');
        empty($appid) && $this->apiError('appid为空');
        $data = $_REQUEST['data'];

        $userInfo = session("users");
        $user_id = $userInfo['id'];

        empty($data['id']) && $this->apiError('缺少参数id');
        $wificontrol_id = intval($data['id']);
        $db = Db::name('wificontrol');
        try {
            $res = $db->where(['id'=>$wificontrol_id,'user_id'=>$user_id])->delete();
            if ($res) {
                //删除车库门cardoor,2018-8-13
                $map = [];
                $map[] = ['wificontrol_id','=',$wificontrol_id];
                $map[] = ['user_id','=',$userInfo['id']];
                Db::name('cardoor')->where($map)->delete();
                $this->apiSuccess($res, '删除成功');
            } else {
                $this->apiError('删除失败');
            }
        } catch (\Exception $e) {
            $this->apiError('删除失败' . $e->getMessage());
        }
    }

    private function macTocStr($mac) {
        $mac = strtolower($mac);
        if (!strstr($mac, ":")) {
            $bitArr = str_split($mac, 2);
            $newmacstr = implode(":", $bitArr);
        } else $newmacstr = $mac;
        return $newmacstr;
    }

    public function test ()
    {
        $data = ['login_at' => time(), 'login_num' => ['inc', '1']];

        // prt($data);
        // $res =  Db::name('user')->where(['id' => 3])->update(['login_at'=>time()]);
        $res = Db::name('user')->where(['id' => 3])->update($data);
        prt($res, 0);
        prt(Db::name('user')->getLastSql());
    }

}