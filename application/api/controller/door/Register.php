<?php
/**
 * Created by PhpStorm.
 * User: conson
 * Date: 2018/7/9
 * Time: 12:01
 */

namespace app\api\controller\door;

use controller\BasicApi;
use think\Validate;
use think\Db;
use think\db\Query;
use service\DataService;
use org\RegexCheck;

class Register extends BasicApi
{

    public function doregister ()
    {
        $username = $this->request->request('username', '', 'trim');
        $email = $this->request->request('email', '', 'trim');
        $password = $this->request->request('password', '', 'trim');
        $appid = $this->request->request('appid', '', 'trim');
        $registrationID = $this->request->request('registrationID', '', 'trim');

        // prt($email);
        //TODO 验证 用户名，邮箱是否已注册，是否唯一,是否是邮箱，密码字符串长度

        //信息验证
        // 用户名2-15位且不能为纯数字，且不能是纯数字
        // prt(preg_match("/^(?![0-9]+$)[0-9A-Za-z]{2,15}$/", trim($username)));
//        if(!(preg_match("/^(?![0-9]+$)[0-9A-Za-z]{2,15}$/", trim($username)))){
//            $this->apiError('用户名长度不能少于2位有效字符,不能多于15位有效字符,且不能是纯数字');
//        }

        //用户名2-15为只要不是纯数字就能注册
        if(is_numeric($username)){
            $this->apiError('用户名长度不能少于2位有效字符,不能多于15位有效字符,且不能是纯数字');
        }

        (strlen($username) < 2 || strlen($username) > 15)  && $this->apiError('登录账号长度不能少于2位有效字符,不能多于15位有效字符');
        (strlen($password) < 6 || strlen($password) > 16) && $this->apiError('登录密码长度不能少于6位,不能多于16位有效字符');
        empty($appid) && $this->apiError('appid必输');
        empty($username) && $this->apiError('登录账号必输');
        empty($password) && $this->apiError('密码必输');
        empty($email) && $this->apiError('邮箱不能必输');
        empty($registrationID) && $this->apiError('缺少参数registrationID必输');

        $this->checkEmail($email);
        $this->checkusername($username);

        $data['email'] = $email;
        $data['username'] = $username;
        $data['password'] = sysmd5($password); //是否用sysmd5 还是md5 要统一
        $data['registrationID'] = $registrationID;
        $data['createtime'] = time();
        $db = Db::name('users');
        // prt($data);
        try {
            $res = DataService::save($db, $data);
            if ($res) {
                $this->apiSuccess($data, '注册成功');
            }
        } catch (\Exception $e) {
            $this->apiError('注册失败');
        }
    }


    function checkEmail ($mail = '')
    {
        $email = $mail != false ? $mail : $_REQUEST['eamil'];
        if (!empty($email)) {
            $user = Db::name('users')->where(['email' => $email])->find();
            if (!empty($user)) {
                $this->apiError('邮箱已注册,请更换邮箱');
            }

            if (!RegexCheck::email($email)) {
                $this->apiError('邮箱格式错误');
            }
        }
    }

    function checkusername ($name = '')
    {
        $username = $name != false ? $name : $_REQUEST['username'];
        if (!empty($username)) {
            $user = Db::name('users')->where(['username' => $username])->find();
            if (!empty($user)) {
                $this->apiError('用户名已注册,请更换用户名');
            }
             // if (!RegexCheck::en_numAction($username)) {
             //     $this->apiError('用户名值允许数字和字母');
             // }
        }
    }
}