<?php

namespace app\api\controller\door;

use controller\BasicApi;
use think\Validate;
use think\Db;
use think\db\Query;
use service\DataService;
use service\LogService;
use org\RegexCheck;
use \think\facade\Cache;


class Login extends BasicApi
{
    /**
     * 登录 ，用户名或者邮箱
     * 登录示例：
     *   "username": "conson",
     *   "password": "123456",
     */
    function login ()
    {
        // 输入数据效验
        $username = $this->request->request('username', '', 'trim');
        $password = $this->request->request('password', '', 'trim');
        $appid = $this->request->request('appid', '', 'trim');
        $registrationID = $this->request->request('registrationID', '', 'trim');//appid设备id，用于激光推送
        if (empty($username)) {
            $this->apiError('登录账号必输');
        }

        if (empty($registrationID)) {
            $this->apiError('缺少参数registrationID必输');
        }

        //信息验证
        if (RegexCheck::email($username)) {
            $email = $username;
            unset($username);
        }



        (isset($username) && strlen($username) < 2) && $this->apiError('登录账号长度不能少于2位有效字符');
        (strlen($password) < 6 || strlen($password) > 16) && $this->apiError('登录密码长度不能少于6位,不能多于16位有效字符');
        empty($appid) && $this->apiError('appid必输');
        empty($password) && $this->apiError('密码必输');

        // 用户信息验证
        $map =[];
        isset($email) &&  $map[] = ['email','=',$email];
        isset($username) &&  $map[] = ['username','=',$username];
        $user = Db::name('users')->where($map)->find();

        empty($user) && $this->apiError('登录账号不存在，请重新输入');
        ($user['password'] !== sysmd5($password)) && $this->apiError('登录密码与账号不匹配，请重新输入');
        (false==$user['status']) && $this->apiError('账号已经被禁用，请联系管理');
        // var_dump($_REQUEST);die;

        $registrationIDs = explode(",",$user['registrationID']);
        if (!in_array($registrationID,$registrationIDs)) {
            $registrationIDs[] = $registrationID;
        }
        // 更新登录信息
        $data = ['login_at' => time(), 'login_num' => ['inc', '1'], 'registrationID'=>implode(",",$registrationIDs)];

        $res =  Db::name('users')->where(['id' => $user['id']])->update($data);
        // prt(Db::name('users')->getLastSql());die;
        //session个人信息
        session('users', $user);
        // prt(session('useraaa'));
        LogService::write('用户登录', '用户' . $_REQUEST['username'] . '登录成功');
        $Info['appid'] = $appid;

        // token 生成
        $login_token = $this->createToken($user['id'], $user['username'], $appid);

        $expireTime = intval($_REQUEST['expireTime']);
        $expireTime = $expireTime ? $expireTime : 60 * 60 * 24 * 365;

        //缓存用户信息
        $res = Cache::set($login_token, $Info, $expireTime);//存储缓存
        $post = $user;
        unset($post['password']);
        $post['token'] = $login_token;

        if ($res) {
            $this->apiSuccess($post, '登录成功');
        } else {
            $this->apiError('登录失败');
        }

    }

    /**
     * 退出登录
     */
    public function logout ()
    {
        $login_token = $this->request->request('token', '', 'trim');
        $this->verfiyToken();
        Cache::rm($login_token);
        session("login", null);
        $this->apiSuccess(null, '成功注销');
    }

    //重置密码 （还需要调试）
    function repassword ()
    {
        if ($_POST['dosubmit']) {
            $verifyCode = trim($_POST['verify']);
            if (md5($verifyCode) != $_SESSION['verify']) {
                $this->apiError('验证码错误');
            }
            if (trim($_POST['repassword']) != trim($_POST['password'])) {
                $this->apiError('密码不一致');
            }
            list($username, $email) = [$_POST['username'], $_POST['email']];
            $user = Db::name('users');
            //判断邮箱是用户是否正确
            $data = $user->where("username='{$username}' and email='{$email}'")->find();
            if ($data) {
                $data['password'] = sysmd5(trim($_POST['password']));
                $data['updatetime'] = time();
                $res = $user->where("username='{$username}' and email='{$email}'")->update($data);
                if ($res) {
                    $this->apiSuccess($res, '修改成功');
                }
            } else {
                $this->apiError('修改失败');
            }
            exit;

        }
    }

    public function test(){
        $data = ['login_at' => time(), 'login_num' => ['inc', '1']];

        // prt($data);
        // $res =  Db::name('users')->where(['id' => 3])->update(['login_at'=>time()]);
        $res =  Db::name('users')->where(['id' => 3])->update($data);
        prt($res,0);
        prt(Db::name('users')->getLastSql());
    }

}