<?php

namespace app\api\controller\door;

use controller\BasicApi;
use think\Validate;
use think\Db;
use think\db\Query;
use service\DataService;
use service\LogService;
use org\RegexCheck;
use \think\facade\Cache;


class Cardoor extends BasicApi
{
    /**
     * 列表
     */
    function cardoorList ()
    {
        //检测登录状态
       // $this->verfiyToken();

        $appid = $this->request->request('appid', '', 'trim');
        $wificontrol_id = $this->request->request('wificontrol_id', '', 'int');
        empty($appid) && $this->apiError('appid为空');

        //获取用户信息
        $userInfo = session("users");
        if (false==$userInfo) {
            $this->apiError('未登录或登录已经失效');
        }

        $map['status'] = 1;
        $map['user_id'] = $userInfo['id'];
        if ($wificontrol_id>0) {
            $map['wificontrol_id'] = $wificontrol_id;
        }
        try {
            $list = Db::name('cardoor')->where($map)->select();
            // prt(Db::name('cardoor')->getLastSql());
            if (!empty($list)) {
                $this->apiSuccess($list, '获取成功');
            } else {
                $this->apiError('未找到车库门信息');
            }
        } catch (\Exception $e) {
            $this->apiError('获取失败');
        }
    }


    /**
     * 新增
     */
    public function addCardoor ()
    {
        //验证登录状态
        $this->verfiyToken();

        //验证appid
        $appid = $this->request->request('appid', '', 'trim');
        empty($appid) && $this->apiError('appid为空');
        $data = $_REQUEST['data'];

        empty($data['wificontrol_id']) && $this->apiError('wificontrol_id为空');
        intval($data['wificontrol_id'])==0 && $this->apiError('wificontrol_id必须大于0');
        empty($data['doorname']) && $this->apiError('doorname为空');
        (strlen($data['doorname']) < 3|| strlen($data['doorname']) > 20) && $this->apiError('设备名称长度不能少于3位,不能多于20位有效字符');

        //获取用户信息
        $userInfo = session("users");
        $data['user_id'] = $userInfo['id'];
        $data['createtime'] = time();
        $data['status'] = 1;
        $data['doornumb'] = intval($data['doornumb']);

        $db = Db::name('cardoor');

        $map = [];
        $map[] = ['wificontrol_id','=',$data['wificontrol_id']];
        $map[] = ['doorname','=',$data['doorname']];
        $map[] = ['user_id','=',$userInfo['id']];
        $count = $db->where($map)->count();
        if ($count>0) {
            $this->apiError('设备名称已经存在');
        }

        try {
            $res = DataService::save($db, $data);
            if ($res) {
                $this->apiSuccess($res, '新增成功');
            } else {
                $this->apiError('新增失败');
            }
        } catch (\Exception $e) {
            $this->apiError('新增失败' . $e->getMessage());
        }

    }


    /**
     * 修改
     */
    public function editCardoor ()
    {
        //验证登录状态
        $this->verfiyToken();
        //验证appid
        $appid = $this->request->request('appid', '', 'trim');
        empty($appid) && $this->apiError('appid为空');
        $data = $_REQUEST['data'];

        empty($data['id']) && $this->apiError('缺少参数id');
        intval($data['id'])==0  && $this->apiError('参数id必需大于0');
        if (isset($data['doorname']) && empty($data['doorname']) ) {
            $this->apiError('参数doorname不能是空');
        }

        if (false==$data['doorname'] &&  false==$data['wificontrol_id'] && false==$data['sitesensor_id']){
            $this->apiError('参数设备名称，wifi控制器ID和位置传感器id不能都为空');
        }

        //获取用户信息
        $userInfo = session("users");
        $data['user_id'] = $userInfo['id'];
        $data['doornumb'] = intval($data['doornumb']);
        $data['updatetime'] = time();


        $db = Db::name('cardoor');


        if (!empty($data['doorname']) ) {
            $map = [];
            $map[] = ['doorname','=',$data['doorname']];
            $map[] = ['user_id','=',$userInfo['id']];
            $map[] = ['id','<>',$data['id']];

            $count = $db->where($map)->count();
            if ($count>0) {
                $this->apiError('设备名称已经存在');
            }
        }

            $map = [];
            $map[] = ['id','=',$data['id']];
            !empty($data['wificontrol_id']) &&  $map[] = ['wificontrol_id','=',$data['wificontrol_id']];
            $map[] = ['user_id','=',$userInfo['id']];
            $count = $db->where($map)->count();

            if ($count==0) {
                $this->apiError('该记录不存在');
            }


        try {
            $res = DataService::save($db, $data);
            if ($res) {
                //$this->apiSuccess($res, '修改成功');

                $this->apiSuccess(Db::name('cardoor')->find($res), '修改成功');
                $this->apiError('修改失败');
            }
        } catch (\Exception $e) {
            $this->apiError('修改失败' . $e->getMessage());
        }
    }


    /**
     * 删除
     */
    public function delCardoor ()
    {
        //验证登录状态
        $this->verfiyToken();
        //验证appid
        $appid = $this->request->request('appid', '', 'trim');
        empty($appid) && $this->apiError('appid必输');
        $data = $_REQUEST['data'];

        empty($data['id']) && $this->apiError('id必输');

        $db = Db::name('cardoor');
        try {
            $res = $db->delete($data['id']);
            if ($res) {
                $this->apiSuccess($res, '删除成功');
            } else {
                $this->apiError('删除失败');
            }
        } catch (\Exception $e) {
            $this->apiError('删除失败' . $e->getMessage());
        }
    }


    public function test ()
    {
        $data = ['login_at' => time(), 'login_num' => ['inc', '1']];

        // prt($data);
        // $res =  Db::name('user')->where(['id' => 3])->update(['login_at'=>time()]);
        $res = Db::name('user')->where(['id' => 3])->update($data);
        prt($res, 0);
        prt(Db::name('user')->getLastSql());
    }

}