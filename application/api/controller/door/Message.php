<?php

namespace app\api\controller\door;

use controller\BasicApi;
use think\Validate;
use think\Db;
use think\db\Query;
use service\DataService;
use service\LogService;
use org\RegexCheck;
use \think\facade\Cache;

class Message extends BasicApi
{

    /**
     * 系统消息
     *
     */
    function getinfo ()
    {
            if (1) {
                //检测登录状态
                $appid = $this->request->request('appid', '', 'trim');
                empty($appid) && $this->apiError('appid必输');
                $this->verfiyToken();
            }

        // 用户信息验证
        //获取用户信息
        $userInfo = session("users");

        $db =  Db::name('message');
        $map = [];
        $map['userid'] = $userInfo['id'] ;
        $map['status'] = 1;
        $db->where($map)->order('id desc');


        $articleInfo = parent::_list($db,1);

        if ($articleInfo['list']) {
         	foreach ($articleInfo['list'] as $key => &$value) {
         		if ($value['createtime']>0)  $value['createtime'] = date("Y-m-d H:i:s",$value['createtime']);
 
         		if ($value['mtag']=='wkman') {
         		    parse_str($value['title'],$arr);
         		    $map = array();
         		    $map['wificontrol_id'] = $arr['wificontrol_id'];
         		    $map['userid'] = $userInfo['id'];
                    $cardoorRes = Db::name('cardoor')->where($map)->find();
                    $arr['doorname'] =  $cardoorRes['doorname'];
                    $arr['sitesensor_id'] =  $cardoorRes['sitesensor_id'];
                    $value['title'] = http_build_query($arr);
                }
         	}
            $this->apiSuccess($articleInfo, '操作成功');
        } else {
            $this->apiError('查询数据为空');
        }

    }


    function add() {
        //检测登录状态
        $appid = $this->request->request('appid', '', 'trim');
        empty($appid) && $this->apiError('appid为空');
        //验证登录状态
        $this->verfiyToken();
        // 用户信息验证
        //获取用户信息
        $userInfo = session("users");
        $db =  Db::name('message');
        $data['user_id'] = $userInfo['id'];
        $data['createtime'] = time();
        $data['title'] = $this->request->request('title', '', 'trim');
        $data['memo'] = $this->request->request('memo', '', 'trim');

        empty($data['title']) && $this->apiError('消息标题必输');
        empty($data['memo']) && $this->apiError('消息内容必输');

        try {
            $res = DataService::save($db, $data);
            if ($res) {
                $this->apiSuccess($res, '新增成功');
            } else {
                $this->apiError('新增失败');
            }
        } catch (\Exception $e) {
            $this->apiError('新增失败' . $e->getMessage());
        }

    }


    function edit() {
        //检测登录状态
        $appid = $this->request->request('appid', '', 'trim');
        empty($appid) && $this->apiError('appid为空');
        $id = $this->request->request('id', 0, 'int');
        if ($id==0) {
            $this->apiError('id必输并且不能为零');
        }

        if (false==$_REQUEST['ib_read'])   $this->apiError('ib_read必输');
        //验证登录状态
        $this->verfiyToken();
        // 用户信息验证
        //获取用户信息
        $userInfo = session("users");
        $db =  Db::name('message');
        $data['id'] = $id;
        $data['user_id'] = $userInfo['id'];
        $data['updatetime'] = time();
        $data['ib_read'] = $this->request->request('ib_read', 1, 'int');



        try {
            $res = DataService::save($db, $data);
            if ($res) {
                $this->apiSuccess($res, '操作成功');
            } else {
                $this->apiError('操作失败');
            }
        } catch (\Exception $e) {
            $this->apiError('操作失败' . $e->getMessage());
        }

    }


}