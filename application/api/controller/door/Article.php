<?php

namespace app\api\controller\door;

use controller\BasicApi;
use think\Validate;
use think\Db;
use think\db\Query;
use service\DataService;
use service\LogService;
use org\RegexCheck;
use \think\facade\Cache;


class Article extends BasicApi
{

    /**
     * 登录 ，用户名或者邮箱
     */
    function getinfo ()
    {
        // 输入数据效验
        $id = $this->request->request('id', '', '');
     //   $token = $this->request->request('token', '', 'trim');
//        $appid = $this->request->request('appid', '', 'trim');
//
//        //信息验证
//        empty($appid) && $this->apiError('appid必输');
        empty($id) &&  $this->apiError('id必输');
        $id ==0 &&  $this->apiError('id不能为零必输');

        //检测登录状态
         parent::verfiyAppID();

        // 用户信息验证

        //还没登录
        $db =  Db::name('article');
        $articleInfo = $db->find($id);

        if ($articleInfo) {
            $this->apiSuccess(array($articleInfo), '操作成功');
        } else {
            $this->apiError('ID信息不存在');
        }

    }

    //常见问题列表
    function questionlist()
    {
        // 输入数据效验
        $appid = $this->request->request('appid', '', 'trim');
//        //信息验证
        empty($appid) && $this->apiError('appid必输');
        //检测AppID 状态
        parent::verfiyAppID();
        //还没登录
        $db =  Db::name('article');
        $map = [];
        $map[] = ['typeId','=',5];
        $articleInfo = $db->where($map)->select();

        if ($articleInfo) {
            $this->apiSuccess(array($articleInfo), '操作成功');
        } else {
            $this->apiError('返回数据为空');
        }

    }

}