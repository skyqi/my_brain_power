<?php

namespace app\admin\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;
use org\Crontab;
use think\Db;
use think\Exception;

class Backup extends Command
{

    protected function configure()
    {
        $this->setName('Cmdtask')->setDescription('Here is the remark ');
    }

//    public function doBackup() {
//        $runtime = env('runtime_path');
//        $cn = "backup_".date("Y-m-d H:i:s")."\r\n";
//        file_put_contents ($runtime."/crontab.log",$cn, FILE_APPEND );
//
//    }

    public function testBackup() {

        $runtime = env('runtime_path');
        $cn = "testBackup_".date("Y-m-d H:i:s")."\r\n";
        file_put_contents ($runtime."/phpdebug/crontab_testBackup_".date("Ymd").".log",$cn, FILE_APPEND );

    }

    public function dbBackup() {

        header("Content-type:text/html;charset=utf-8");
        $path =   env('ROOT_PATH'). 'BackUp/';
//        $message = date("Y-m-d H:i:s") ."-- test \r\n";
//        file_put_contents ($path."crontabs.log",$message, FILE_APPEND );
//
//        die;
        // 检查目录是否存在
        if(is_dir($path)){
            // 检查目录是否可写
            if(is_writable($path)){
                //echo '目录可写';exit;
            }else{
                //echo '目录不可写';exit;
                chmod($path,0777);
            }
        }else{
            //echo '目录不存在';exit;
            // 新建目录
            mkdir($path, 0777, true);
            //chmod($path,0777);
        }

        try {
            $database = config('database.database');
            // prt($database);
            //echo "运行中，请耐心等待...<br/>";
            $info = "-- ----------------------------\r\n";
            $info .= "-- 日期：" . date("Y-m-d H:i:s", time()) . "\r\n";
            $info .= "-- MySQL - 5.5.52-MariaDB : Database - " . $database . "\r\n";
            $info .= "-- ----------------------------\r\n\r\n";
            $info .= "CREATE DATAbase IF NOT EXISTS `" . $database . "` DEFAULT CHARACTER SET utf8 ;\r\n\r\n";
            $info .= "USE `" . $database . "`;\r\n\r\n";

            // 检查文件是否存在
            $back_filename = $database . '-' . date("YmdHis") . rand(100, 99);
            $filename = htmlspecialchars(strip_tags($back_filename));
            $filename = !$filename ? 'Yp_' . rand_string(10) . '_' . date('YmdH') : $filename;
            $volume = intval($_GET['volume']) + 1;
            $filename_valume = sprintf($filename . "-%s" . '.sql', $volume);
            $file_name = $path . $filename_valume;


            if (file_exists($file_name)) {
                echo "数据备份文件已存在！";
                exit;
            }
            file_put_contents($file_name, $info, FILE_APPEND);

            //查询数据库的所有表
            $result = Db::query('show tables');
            //print_r($result);exit;
            foreach ($result as $k => $v) {
                //查询表结构
                $val = $v['Tables_in_' . $database];
                $sql_table = "show create table `" . $val . "`";
                $res = Db::query($sql_table);
                //print_r($res);exit;
                $info_table = "-- ----------------------------\r\n";
                $info_table .= "-- Table structure for `" . $val . "`\r\n";
                $info_table .= "-- ----------------------------\r\n\r\n";
                $info_table .= "DROP TABLE IF EXISTS `" . $val . "`;\r\n\r\n";
                $info_table .= $res[0]['Create Table'] . ";\r\n\r\n";
                //查询表数据
                $info_table .= "-- ----------------------------\r\n";
                $info_table .= "-- Data for the table `" . $val . "`\r\n";
                $info_table .= "-- ----------------------------\r\n\r\n";
                file_put_contents($file_name, $info_table, FILE_APPEND);
                $sql_data = "select * from `" . $val . "`";
                $data = Db::query($sql_data);
                //print_r($data);exit;
                $count = count($data);
                //print_r($count);exit;
                if ($count < 1) continue;
                foreach ($data as $key => $value) {
                    $sqlStr = "INSERT INTO `" . $val . "` VALUES (";
                    foreach ($value as $v_d) {
                        $v_d = str_replace("'", "\'", $v_d);
                        $sqlStr .= "'" . $v_d . "', ";
                    }
                    //需要特别注意对数据的单引号进行转义处理
                    //去掉最后一个逗号和空格
                    $sqlStr = substr($sqlStr, 0, strlen($sqlStr) - 2);
                    $sqlStr .= ");\r\n";
                    file_put_contents($file_name, $sqlStr, FILE_APPEND);
                }
                $info = "\r\n";
                file_put_contents($file_name, $info, FILE_APPEND);
            }

            $message = "\r\n".date("Y-m-d H:i:s") . $filename_valume . ' 备份成功!' . "\r\n";
            file_put_contents (env('runtime_path')."/phpdebug/crontabs.log",$message, FILE_APPEND );

        }catch (Exception $e) {
            $message = date("Y-m-d H:i:s") ."-- error:" .$e->getMessage() . "\r\n";
            file_put_contents (env('runtime_path')."/phpdebug/crontabs.log",$message, FILE_APPEND );
        }

    }


    public function dbBackup2(){
        header("Content-type:text/html;charset=utf-8");
        // $path = RUNTIME_PATH.'mysql/';
        $path =   env('ROOT_PATH'). 'BackUp/';


        savelog("文件备份目录--".$path,"crontab_");
        $database = config('database.database');
        // prt($database);
        //echo "运行中，请耐心等待...<br/>";
        $info = "-- ----------------------------\r\n";
        $info .= "-- 日期：".date("Y-m-d H:i:s",time())."\r\n";
        $info .= "-- MySQL - 5.5.52-MariaDB : Database - ".$database."\r\n";
        $info .= "-- ----------------------------\r\n\r\n";
        $info .= "CREATE DATAbase IF NOT EXISTS `".$database."` DEFAULT CHARACTER SET utf8 ;\r\n\r\n";
        $info .= "USE `".$database."`;\r\n\r\n";

        // 检查目录是否存在
        if(is_dir($path)){
            // 检查目录是否可写
            if(is_writable($path)){
                //echo '目录可写';exit;
            }else{
                //echo '目录不可写';exit;
                chmod($path,0777);
            }
        }else{
            //echo '目录不存在';exit;
            // 新建目录
            mkdir($path, 0777, true);
            //chmod($path,0777);
        }

        // 检查文件是否存在
        $filename = htmlspecialchars(strip_tags($_GET['filename']));
        $filename = !$filename ? 'Yp_' . rand_string(10) . '_' . date('YmdH') : $filename;
        $volume = intval($_GET['volume']) + 1;
        $filename_valume = sprintf($filename . "-%s" . '.sql', $volume);
        $file_name = $path . $filename_valume;

        // $file_name = $path.$database.'-'.date("Y-m-d",time()).'.sql';
        if(file_exists($file_name)){
            echo "数据备份文件已存在！";
            exit;
        }
        file_put_contents($file_name,$info,FILE_APPEND);

        //查询数据库的所有表
        $result = Db::query('show tables');
        //print_r($result);exit;
        foreach ($result as $k=>$v) {
            //查询表结构
            $val = $v['Tables_in_'.$database];
            $sql_table = "show create table `".$val."`";
            $res = Db::query($sql_table);
            //print_r($res);exit;
            $info_table = "-- ----------------------------\r\n";
            $info_table .= "-- Table structure for `".$val."`\r\n";
            $info_table .= "-- ----------------------------\r\n\r\n";
            $info_table .= "DROP TABLE IF EXISTS `".$val."`;\r\n\r\n";
            $info_table .= $res[0]['Create Table'].";\r\n\r\n";
            //查询表数据
            $info_table .= "-- ----------------------------\r\n";
            $info_table .= "-- Data for the table `".$val."`\r\n";
            $info_table .= "-- ----------------------------\r\n\r\n";
            file_put_contents($file_name,$info_table,FILE_APPEND);
            $sql_data = "select * from `".$val."`";
            $data = Db::query($sql_data);
            //print_r($data);exit;
            $count= count($data);
            //print_r($count);exit;
            if($count<1) continue;
            foreach ($data as $key => $value){
                $sqlStr = "INSERT INTO `".$val."` VALUES (";
                foreach($value as $v_d){
                    $v_d = str_replace("'","\'",$v_d);
                    $sqlStr .= "'".$v_d."', ";
                }
                //需要特别注意对数据的单引号进行转义处理
                //去掉最后一个逗号和空格
                $sqlStr = substr($sqlStr,0,strlen($sqlStr)-2);
                $sqlStr .= ");\r\n";
                file_put_contents($file_name,$sqlStr,FILE_APPEND);
            }
            $info = "\r\n";
            file_put_contents($file_name,$info,FILE_APPEND);
        }
        $message = $filename_valume . ' 备份成功!';
        // download($file_name,'123.sql');

        savelog($message,"crontab_");


    }

}
