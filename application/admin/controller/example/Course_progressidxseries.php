<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/24
 * Time: 8:42
 */

namespace app\admin\controller;
use controller\BasicAdmin;
use service\DataService;
use service\NodeService;
use service\ToolsService;
use think\Db;
use think\Cache;
use org\FieldFromClass;
use org\ContentList;
use org\QueryFrom;
use think\Exception;
use think\Request;
class Course_progressidxseries extends BasicAdmin
{
    public $table,$fields,$map;
    public function __construct()
    {
        parent::__construct();

//        $this->url = url("/admin/{$this->controller_name}/index", ['spm' => $_REQUEST['spm'], 'runtime' => time()]);  // 放在前面组装
//        $this->table = $this->controller_name;
//        $moduleId = $this->getModuleId($this->controller_name);
//        $this->fields = F($moduleId."_Field");
//        $this->assign("tbField",$this->fields);
//        $this->assign("fields",$this->fields);
//        $this->assign("url_index",$this->url);

    }

    public function index(){

        $db = DB::table($this->controller_name) ;
        $pk = $db->getPk();

        $tbList = $this->_list( $db ,true,false);
        $this->assign("pk",$pk);
        $this->assign("pages",$tbList['page']);


        $tplName =  (app('view')->exists("{$this->controller_name}/index"))?"{$this->controller_name}/index":"content/index";
        $this->assign("ContentList",new ContentList());

        $lesson_lists = DB::table("lesson-content")->where("status=1")->column("id,lesson_series,lesson_name");
//        foreach ($lesson_lists as $k=>&$v) {
//            $v['lesson_series_name'] = getCacheField("system_types",$v['lesson_series'],'name',0);
//        }

        unset($k,$v);
        //组装lesson_content数据
        foreach ($tbList['list'] as $k=>&$v) {
            $lesson_content_arr = explode(",", $v['lesson_content']);
            $lesson = [];
            foreach ($lesson_content_arr as $k1 => $v1) {
                $lesson[] =  $lesson_lists[$v1]['lesson_name'] ;
            }
            $v['lesson_content'] = implode(",",$lesson);
        }
        $this->assign("list",$tbList['list']);

        echo $this->fetch($tplName);
        exit;

    }



    public function edit()
    {

        $res = DB::name("system_module")->where("name","=",$this->controller_name)->find();
        $moduleid = $res['id'];

        $fields = $this->FieldCache($moduleid,'get');
        //prt($fields);
        $this->assign ( 'fields', $fields );

        $id = intval($_REQUEST ['id']);
        if ($id>0) {
            //  parent::returnMsg ('error',L('edit_error') );
            $vo = DB::name($this->controller_name)->getById( $id );

        }

        $pk = DB::name($this->controller_name)->getPk();
        $this->assign("pk",$pk);

        //prt($vo);
        false == $vo['content'] && $vo['content'] = htmlspecialchars($vo['content']);


        $this->assign ( 'vo', $vo );

        // prt($vo);
        //   $this->assign ( 'form', $form );
        $this->assign ( 'FieldFromClass', new FieldFromClass() );

        $tplName =  (app('view')->exists("{$this->controller_name}/edit"))?"{$this->controller_name}/edit":"content/edit";
        $tplCn =$this->fetch();

        echo $tplCn;
        exit;
    }


    public function before_update() {

        if (is_array($_POST['lesson_content'])) {
            $_POST['lesson_content'] = implode(",",$_POST['lesson_content']);
        }
    }

    //编辑教学编辑内容
    public function lesson_list() {
        $table = "exercise-types";
        $typesDb = DB::table($table);
        $pk = $typesDb->getPk();

        //progress_id
        $progress_ids = DB::table("course-progressidxseries")->where("id=".intval($_REQUEST['progress_id']))->field("id,lesson_series,exercise_types_ids")->find();
        $lesson_series = $progress_ids['lesson_series'] ;
        $exercise_types_ids = $progress_ids['exercise_types_ids'] ;


        $this->assign("exercise_types_arr",explode(",",$exercise_types_ids));
        $this->assign("progress_id",$_REQUEST['progress_id']);

        $moduleId = $this->getModuleId($table);
        $fields = F($moduleId."_Field");

        $this->assign("tbField",$fields);

        $typesDb = $typesDb->where("status=1 and lesson_series=$lesson_series");
        $tbList = $this->_list( $typesDb ,true,false);
        $this->assign("pk",$pk);
        $this->assign("pages",$tbList['page']);
        $this->assign("ContentList",new ContentList());
        $this->assign("list",$tbList['list']);
        echo $this->fetch("lesson-content-index");
        exit;
    }

    // update_exercise_types_ids
    public function update_exercise_types_ids() {

         $db = DB::table("course-progressidxseries");
         $data = [];
         $data['exercise_types_ids'] = implode(",",array_unique($_POST['ids'])) ;
         $data['id'] = $_POST['progress_id'] ;
         $result = DataService::save($db, $data);
        // prt($data);
         $this->success("提交成功",'/admin#'.$this->url);
    }

    public function lesson_edit() {
        $table = "exercise-types";
        $res = DB::name("system_module")->where("name","=","exercise-types")->find();
        $moduleid = $res['id'];
        $fields = $this->FieldCache($moduleid,'get');
        //prt($fields);
        $this->assign ( 'fields', $fields );

        $id = intval($_REQUEST ['id']);
        if ($id>0) {
            //  parent::returnMsg ('error',L('edit_error') );
            $vo = DB::name($this->controller_name)->getById( $id );

        }

        $pk = DB::name($this->controller_name)->getPk();
        $this->assign("pk",$pk);

        //prt($vo);
        false == $vo['content'] && $vo['content'] = htmlspecialchars($vo['content']);


        $this->assign ( 'vo', $vo );

        // prt($vo);
        //   $this->assign ( 'form', $form );
        $this->assign ( 'FieldFromClass', new FieldFromClass() );


        echo $this->fetch("lesson-content-index");
        exit;
    }


}