<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/24
 * Time: 8:42
 */

namespace app\admin\controller;
use controller\BasicAdmin;
use service\DataService;
use service\NodeService;
use service\ToolsService;
use think\Db;
use think\Cache;
use org\FieldFromClass;
use org\ContentList;
use org\QueryFrom;
use think\Exception;
use think\Request;
use org\CreateFromControl;

class Classes extends BasicAdmin
{
    public $table,$fields,$map,$openClassStatus;
    public function __construct()
    {
        parent::__construct();

        $this->controller_name = "classes";
        $this->url = url("/admin/{$this->controller_name}/index", ['spm' => $_REQUEST['spm'], 'runtime' => time()]);  // 放在前面组装
        $this->table = $this->controller_name;
        $moduleId = $this->getModuleId($this->controller_name);
        $this->fields = F($moduleId."_Field");
        $this->assign("tbField",$this->fields);
        $this->assign("fields",$this->fields);

        //
        try {
            $pk = DB::name($this->controller_name)->getPk();
            $pk && $this->assign("pk", $pk);
        }catch (Exception $e) {

        }

    }


    public function index(){
        $db = DB::table($this->controller_name) ;
        $pk = $db->getPk();
        $map = [];
        if (session('user.campus_id')>0) {
            if (in_array("campus_id",$db->getTableFields())){
                $map[] = ['campus_id', '=', session('user.campus_id')];
            }
        }
        $db->where($map);
        $tbList = $this->_list( $db ,true,false);
        $this->assign("pk",$pk);
        $this->assign("pages",$tbList['page']);
        $this->assign("list",$tbList['list']);


        $this->assign("ContentList",new ContentList());
        echo $this->fetch();
        exit;
    }



    protected function getTimeList() {

        for ($i=7;$i<23;$i++) {
            $time_h[$i] = $i;
        }

        for ($i=0;$i<60;$i++) {
            $time_i[$i] = $i;
        }
        $this->assign("time_h",$time_h);
        $this->assign("time_i",$time_i);
    }

//    public function _before_insert()
//    {
//        if ($_POST) {
//            //检查学员是否符合要求
//            $has_intersect_stu = 0;
//            if (!empty($_POST['fixed_students']) && !is_array($_POST['fixed_students']))  $_POST['fixed_students'] = explode(",",$_POST['fixed_students']);
//            if (!empty($_POST['temp_students']) && !is_array($_POST['temp_students']))  $_POST['temp_students'] = explode(",",$_POST['temp_students']);
//            if (!empty($_POST['fixed_students']) && !empty($_POST['temp_students']) ) {
//                $arr = array_intersect($_POST['fixed_students'], $_POST['temp_students']);
//                if (count($arr) >0) {
//                    $has_intersect_stu = 1;
//                }
//            }
//            // prt($_POST);
//
//            $_validate = array(
//                array(false==$_POST['classroom'],"分校管理必须输入！"),
//                array(false==intval($_POST['classroom']),"分校管理必须数值型！"),
//                array(false==$_POST['teacher_id'],"教师必须输入！"),
//                array(false==intval($_POST['teacher_id']),"教师必须输入数值型！"),
//                array(false==$_POST['lesson_series'],"课程系列必须输入！"),
//                array(false==$_POST['lesson_grader'],"课程等级必须输入！"),
//                array(false==$_POST['open_time'],"开班时间必须输入！"),
//                array(false==$_POST['end_time'],"截止时间必须输入！"),
//                array(strtotime($_POST['open_time']) > strtotime($_POST['end_time']),"截止时间不能小于开班时间！"),
//                array(empty($_POST['fixed_students']),'未选择学员！'),
//                array(($_POST['cycle'] == 'week' && empty($_POST['schedule_week'])),'未选择排课周时间！'),
//                array(($_POST['cycle'] == 'day' && empty($_POST['schedule_time'])),'未选择排课时间！'),
//                array($has_intersect_stu,'有学员同时出现在固定学员，临时学员内，错误！'),
//            );
//
//
//            foreach ($_validate as $k=>$v) {
//                if ($v[0]) $this->error($v[1]);
//            }
//
//            if (!empty($_POST['schedule_week']) && $_POST['cycle'] == 'week') {
//                $schedule_week = $_POST['schedule_week'];
//                $time = array();
//                foreach ($schedule_week as $k => $v) {
//                    $time[$v] = ($_POST['time_h'][$v] ? $_POST['time_h'][$v] : '0') . ":" . ($_POST['time_i'][$v] ? $_POST['time_i'][$v] : 0) . ':0';
//                }
//
//                unset($_POST['time_h'], $_POST['time_i'], $_POST['schedule_time']);
//                $_POST['schedule_week'] = stripslashes(array2string($time));
//
//            }
//            $_POST['status'] = 1;
//
//        }
//
//    }

    //在update事件内调用
    public function beforeUpdate()
    {
        if ($_POST) {
            //检查学员是否符合要求
            $has_intersect_stu = 0;

            if (!empty($_POST['fixed_students']) && !is_array($_POST['fixed_students'])) {
                $fixed_students_arr = explode(",",$_POST['fixed_students']);

            } else   $fixed_students_arr =  $_POST['fixed_students'] ;

            if (!empty($_POST['temp_students']) && !is_array($_POST['temp_students'])) {
                $temp_students_arr = explode(",",$_POST['temp_students']);
            } else  $temp_students_arr =  $_POST['temp_students'] ;

            $fixed_students_count = count($fixed_students_arr);  //固定学员数量
            $temp_students_count = count($temp_students_arr);  //固定学员数量

            if (!empty($fixed_students_arr) && !empty($temp_students_arr) ) {
                $arr = array_intersect($fixed_students_arr, $temp_students_arr);
                if (count($arr) >0) {
                    $has_intersect_stu = 1;
                }
            }

            $_validate = array(
                array(false==$_POST['title'],"班级名称必须输入！"),
                array(false==intval($_POST['campus_id']),"所属分校必须输入！"),
                array(false==$_POST['classroom_id'],"教室必须输入！"),
                array(false==intval($_POST['classroom_id']),"教室必须数值型！"),
                array(false==intval($_POST['teacher_id']),"教师必须输入！"),
                array(false==intval($_POST['teacher_id']),"教师必须输入数值型！"),
                array(false==$_POST['lesson_series'],"课程系列必须输入！"),
                array(false==$_POST['lesson_grader'],"课程等级必须输入！"),
                array(false==$_POST['open_time'],"开班时间必须输入！"),
                array(false==$_POST['end_time'],"截止时间必须输入！"),
                array(($_POST['open_time']) > ($_POST['end_time']),"截止时间不能小于开班时间！"),
                array(($_POST['cycle'] == 'week' && empty($_POST['schedule_week'])),'未选择排课周时间！'),
                array(($_POST['cycle'] == 'day' && empty($_POST['schedule_time'])),'未选择排课时间！'),
                array($has_intersect_stu,'有学员同时出现在固定学员，临时学员内，错误！'),
                array($fixed_students_count==0,'未选择固定学员，错误！'),
                array(intval($_POST['deduct_hour'])==0,'扣课率必须大于0'),
                array(intval($_POST['site_number'])==0,'学位数必须大于0'),

            );

            if (false==$_POST['id']) {
                foreach ($_validate as $k => $v) {
                    if ($v[0]) $this->error($v[1]);
                }
            }

            if (!empty($_POST['schedule_week']) && $_POST['cycle'] == 'week') {
                $schedule_week = $_POST['schedule_week'];
                $time = array();
                foreach ($schedule_week as $k => $v) {
                    $time[$v] = ($_POST['time_h'][$v] ? $_POST['time_h'][$v] : '0') . ":" . ($_POST['time_i'][$v] ? $_POST['time_i'][$v] : 0) . ':0';
                }
                unset($_POST['time_h'], $_POST['time_i'], $_POST['schedule_time']);
                $_POST['schedule_week'] = stripslashes(array2string($time));

            }
            unset($_POST['time_h'], $_POST['time_i']);



            if ($_POST['cycle'] == 'day') {
                unset($_POST['schedule_week']);
            }
            if ($_POST['cycle'] == 'week') {
                unset($temp_students_arr);      //去掉
                unset($_POST['temp_students_num']);      //去掉
            }

            $fixed_students_arr = array_unique($fixed_students_arr);
            $_POST['fixed_students']  =  implode(",",$fixed_students_arr);

            $temp_students_arr = array_unique($temp_students_arr);
            $_POST['temp_students']  =  implode(",",$temp_students_arr);

            $_POST['fixed_students_num'] = count($fixed_students_arr);
            $_POST['temp_students_num'] = count($temp_students_arr);
            //site_number
            if ($_POST['site_number']<  $_POST['fixed_students_num']+  $_POST['temp_students_num'] ) {
                $this->error("固定学员和临时学员总和超过学位数");
            }

            if ($this->action_name=='edit'){
                $_POST['updatetime'] =   time();
            } else {
                $_POST['updatetime'] =   $_POST['createtime'] = time();
            }
            // open_time
            $_POST['status'] = 1;
            $map = [];
            $map[] = ["title","eq",$_POST['title']];
            if ($_POST['id']>0) {
                $map[] = ["id","neq",$_REQUEST['id']];
            }

            $count = DB::table("classes")->where($map)->count();
            if($count>0) {
                $this->error("班级名称[{$_POST['title']}]已经存在");
            }
            unset($_POST['forward']);
            $open_time  = toTimestamp($_POST['open_time']);
            $end_time  = toTimestamp($_POST['end_time']);
            $_POST['open_time'] =  strtotime(date("Y-m-d 0:0:0",$open_time));
            $_POST['end_time'] = strtotime(date("Y-m-d 23:59:59",$end_time));
            if (isset($_POST['igr_hour'])) {
                cookie('left_hours',1);
                unset($_POST['igr_hour']);
            }

        }

    }

    public function _query_filter(&$map) {

        foreach ($_REQUEST['query'] as $k=>&$v) {
            if ($k=='province' && $v['title']==-1) unset($v['title']);
            if ($k=='city' && $v['title']==-1) unset($v['title']);

            if (!empty($v['title'])) {
                $title = trim($v['title']);
                if (!isset($v['symbol'])) {
                    $frmMap[]  = $map[]  = array($k,"eq","{$title}");
                }elseif(strtolower($v['symbol'])=='like') {
                    $frmMap[]  = $map[]  = array($k,"like","%{$title}%");
                }else {
                    $frmMap[]  = $map[]  = array($k,"{$v['symbol']}","{$title}");
                }
            }

            if (!empty($v['starttime'])) {
                $starttime = $v['starttime'].' 0:0:0';
                $starttime = strtotime($starttime);
                $frmMap[]  = array($k.'_start_time',"egt","{$starttime}");
                $map[]  = array($k,"egt","{$starttime}");
            }

            if (!empty($v['endtime'])) {
                $endtime = $v['endtime'].' 23:23:59';
                $endtime = strtotime($endtime);
                $frmMap[]  = array($k.'_end_time',"elt","{$endtime}");
                $map[]  = array($k,"elt","{$endtime}");
            }

            if (!empty($v['province']) && $v['province']>0) {

                $frmMap[]  = $map[]  = array('province',"eq","{$v['province']}");
            }

        }

        $frmMap &&  $this->assign("map",json_encode($frmMap));

    }





    public function edit()
    {
        $this->getTimeList();
        $res = DB::name("system_module")->where("name","=",$this->controller_name)->find();
     //   $moduleid = $res['id'];

//        if (intval(session('user.campus_id'))>0 && $bindModules[0]=='campus' ) {
//            $map['id'] = intval(session('user.campus_id'));
//        };

        //权限
      //  $user = session('user');
        $campus_where[] = ["status",'=',1];
        if (session('user.campus_id')>0){
            $campus_where[] = ["id",'=',session('user.campus_id')];
        }

        $select['campus'] = Db::name('campus')->where($campus_where)->order("sort desc,id desc")->limit(10)->column(['id','title']);
        // $select['classroom'] = Db::name('system_types')->where(["status"=>1, "parentid"=>120])->order("sort desc,id desc")->limit(999)->cache(60)->column(['id','name']);
        if (intval(session('user.campus_id'))>0  && intval(session('user.campus_id')>0 )) {
            $campus_id = intval(session('user.campus_id'));
            $sql = "SELECT c.`id` AS campus_id ,c.`title` AS title , r.`id` AS classroom_id,r.`classroom_name` FROM  `classroom` r INNER JOIN  `campus` c  ON (r.`campus_id` = c.`id`) where c.id={$campus_id} order by c.id asc";
        } else {
            $sql = "SELECT c.`id` AS campus_id ,c.`title` AS title , r.`id` AS classroom_id,r.`classroom_name` FROM  `classroom` r INNER JOIN  `campus` c  ON (r.`campus_id` = c.`id`) order by c.id asc";
        }

        $campus_list = DB::query($sql);
        foreach ($campus_list as $k=>$v) {
            $select['campus_list'][$v['title']][] = $v;
        }




        $select['lesson_series'] = Db::name('system_types')->where(["status"=>1, "parentid"=>116])->order("id asc")->limit(999)->column(['id','name']);



        $id = intval($_REQUEST ['id']);
        if ($id>0) {
          //  parent::returnMsg ('error',L('edit_error') );
            $class_vo = DB::name($this->controller_name)->getById( $id );
            if ($id>0 && $class_vo['open_time'] <= time()) {
                $this->assign("open_status",1);
            }
        }


        $class_vo['schedule_time']= string2array($class_vo['schedule_time']);
        $class_vo['schedule_week'] = string2array($class_vo['schedule_week']);
        false == $class_vo['content'] && $class_vo['content'] = htmlspecialchars($class_vo['content']);
        if(!empty($class_vo['schedule_week'])){
            foreach ($class_vo['schedule_week'] as $k=>&$v){
                $v = explode(':', $v);
            }
        }

        $pk = DB::name($this->controller_name)->getPk();
        $this->assign("pk",$pk);


        $id && $this->assign ( 'id',  $id);
        $this->assign ( 'CreateFromControl', new CreateFromControl() );
      //  unset($class_vo);

        //课程等级
        $parentid = $class_vo['lesson_series'];
        $select['lesson_grader'] = Db::name('system_types')->where(["status"=>1, "parentid"=>$parentid])->order("sort asc,id asc")->limit(999)->column(['id','name']);


        if ( intval(session('user.campus_id')>0 )) {
            $campus_id = session('user.campus_id');
            $select['teacher'] = Db::name('teacher')->where(["status"=>1,"campus_id"=>$campus_id])->order("id desc")->column(['id','teacher_name']);
        } else {
            $filter  = array();
            $filter[] = ['status','=',1];
            if (intval($class_vo['campus_id'])>0) {
                $filter[] = ['campus_id','=',$class_vo['campus_id']];
            }
            $select['teacher'] = Db::name('teacher')->where($filter)->order("id desc")->limit(999)->column(['id','teacher_name']);
        }

        $this->assign ( 'select_list', $select );


        $Mdl = Db::name('campus');
        $pk = $Mdl->getPk();
        // prt($pk, 0);
        $order = "$pk asc";
        $fieldList =  $Mdl->getTableFields();
        if (in_array('sort',$fieldList)){
            $order = 'sort asc';
        };
        $campusList = $Mdl->where(['status'=>1])->order($order)->cache(true,60)->column([$pk,'title']);
        // prt($campusList);

        $this->assign ( 'campusList', $campusList );
        // prt($vo);
        $this->assign ( 'FieldFromClass', new FieldFromClass() );
        // prt(new FieldFromClass());
        // $res = CreateFromControl::test();

        $this->assign ( 'class_vo', $class_vo);
        $tplName =  (app('view')->exists("{$this->controller_name}/edit"))?"{$this->controller_name}/edit":"content/edit";
        // prt($tplName);

        echo $this->fetch($tplName);
        die;
    }

    public function before_edit() {
        prt("test");
    }


    public function before_update() {

        foreach ($_POST as $kk=>&$vv) {
            foreach ($this->fields as $k => $v) {
                if ($v['type'] == 'datetime' && $v['field']==$kk) {   //转换时间格式
                    if ($v['field']=='updatetime') {
                        $_POST['updatetime'] =  time();
                    }else {
                        $vv = toTimestamp($vv);
                    }
                }

                if ($v['type']=='typeid' && $v['field']==$kk ) {
                    is_array($vv) && $vv = implode(",",$vv);
                }
                if ($v['type']=='editor') {  }
                if ($v['type']=='image' && $v['field']==$kk) {
                    $vv = implode(",",$vv);
                }
                if ($v['type']=='regional_Linkage') {
                    $vv==-1  && $vv=0;
                }
            }
        }




    }


    function update() {
        $this->openClassStatus = 0;
        if ($_POST['open_status']>0) {
            //开班后存盘
            $this->openClassesUpdate();
            exit;
        }

        if (false==intval($_POST['classroom_id'])) {
            $this->error("班级未输入值");
            exit;
        }

        $_POST['campus_id'] = DB::name("classroom")->where("id=".$_POST['classroom_id'])->value("campus_id");
        $forward = $_POST['forward'];
        //$_POST['campus_id']

        if($_POST['setup']) $_POST['setup']=array2string($_POST['setup']);
        if (false !== $this->_callback('before_update', $_POST, [])) {
            //.todo
        }

        $this->beforeUpdate();


        if (false==$_POST)  {
            $this->returnMsg('error','','未找到POST值');
        }



        // 启动事务
        Db::startTrans();
         try{
             if ($_POST['id']) {
                 //删除学生原来的计划数据
                 $fixed_students = DB::table("classes")->where("id={$_POST['id']}")->value("fixed_students");
                 $map = [];
                 $map[] = ["students_id", "in", $fixed_students];
                 $map[] = ["classes_id", "=", $_POST['id']];
                 $map[] = ["schedule_time", ">", time()];
                 DB::table("class-schedule")->where($map)->delete();
             }
             $classDB = DB::table($this->table);
             $getLastInsID = DataService::save($classDB,$_POST);

            //更新排课表
             if(false!=$_POST['id']) {
                 $getLastInsID = intval($_POST['id']);
             }

            $scheduleRes = $this->add_class_schedule($getLastInsID);

            if (false==$scheduleRes) {
                Db::rollback();
              //  $this->error('在开班起止时间未找到排课周期的时间，请检查！');

                $errorData['msg']=  '在开班起止时间未找到排课周期的时间，请检查！';
                die (json_encode($errorData));
                exit;
            }
             $this->totalclass($getLastInsID);
             // 提交事务
             Db::commit();
       }catch(\Exception $e){
             Db::rollback();
             $getMessage = $e->getMessage()?$e->getMessage():'错误请重试！';
             $this->error("error:".$getMessage); exit;

        }
        //成功
        savecache($this->table);
        $this->success('操作成功',$forward);
    }


    //开班后的修改
    public function openClassesUpdate() {

        if (false==($id=intval($_POST['id']))) {
            return false;
        }else {
            $map = [];
            $map[] = ["id","eq",$id];
        }
        $classesInfo = DB::table("classes")->where($map)->find();


        $_POST['fixed_students_num'] = count($_POST['fixed_students']);

        if ($classesInfo['site_number']<$_POST['fixed_students_num']+$classesInfo['temp_students_num']) {
            $this->error("固定人员位数+临时人员位数，已经超过了学位数");
            return false;
        }


        //删除学生原来的计划数据
        $fixed_students = DB::table("classes")->where("id={$_POST['id']}")->value("fixed_students");
        $map = [];
        $map[] = ["students_id", "in", $fixed_students];
        $map[] = ["classes_id", "=", $_POST['id']];
        $map[] = ["schedule_time", ">", time()];
        DB::table("class-schedule")->where($map)->delete();

//        $map = [];
//        $map[] = ["students_id", "in", $fixed_students];
//        $map[] = ["classes_id", "=", $_POST['id']];
//        $map[] = ["schedule_time", "<=", time()];
//        $map[] = ["status", "=", 1];
//        $scheduleList = DB::table("class-schedule")->where($map)->select();
//        $msg = '';
//        foreach ($scheduleList as $k=>$v) {
//            $msgarr = [];
//            $msgarr[] = 'classes_id='.$v['classes_id'];
//            $msgarr[] = 'schedule_id='.$v['schedule_id'];
//            $msgarr[] = 'class_title='.$v['class_title'];
//            $msgarr[] = 'class_title='.$v['class_title'];
//            $msgarr[] = 'students_id='.getCacheField("students",$v['students_id'],'student_name',0);
//            $msgarr[] = 'schedule_time='.date("Y-m-d H:i:s",$v['schedule_time']);
//            $msgarr[] = '<br>';
//            $msg .=  implode(";",$msgarr);
//        }
//        if ($msg!='') {
//            $this->error("数据表内有重复的记录出现,操作失败".$msg);
//            return false;
//        }



            //更新排课表
        // 启动事务
        Db::startTrans();

        try {
            $saveRet = DataService::save($this->table, [
                'id' => $_POST['id'],
                'fixed_students' => implode(",", $_POST['fixed_students']),
                'fixed_students_num' => $_POST['fixed_students_num'],
            ]);

            //更新排课表
            $this->openClassStatus = 1;
            $scheduleRes = $this->add_class_schedule($_POST['id']);

            if (false == $scheduleRes) {
                die('在开班起止时间未找到排课周期的时间，请检查！');
            }
            // 提交事务
            cookie('left_hours',null);
            Db::commit();
         }catch(\Exception $e){
            Db::rollback();
            $getMessage = $e->getMessage()?$e->getMessage():'错误请重试！';
            die("error:".$getMessage);

         }

        $this->success('操作成功',$_POST['forward']);


    }

//    public function updateHour() {
//        DB::name("")->where()->
//    }

    public function query(){

        $this->assign ('QueryFrom', new QueryFrom() );

        if ($_REQUEST['param']) {
            foreach (json_decode($_REQUEST['param']) as $k => $v) {
                $map[$v[0]] = array('symbol' => $v[1], 'title' => $v[2]);
            }
//            prt($_REQUEST['param'],0);
//            prt($map,0);
            $map && $this->assign('param', $map);
        }

        $tplName =  (app('view')->exists("{$this->controller_name}/query"))?"{$this->controller_name}/query":"content/query";
        $tplCn = $this->fetch($tplName);
       // savelog($tplCn,"query");
        echo $tplCn;
    }


    /*
     *  插入分解数据
     *  http://ivehice2.mydanweb.com/admin/classes/add_class_schedule.html
     */
    public function add_class_schedule($recId)
    {

        $Mdl = DB::table($this->controller_name);

        $res = $Mdl->find($recId);

        if (false==$res) {
            $this->error("未找到id是{$recId}的表记录"); exit();
        }

        $instData = array();
        $arr_fixed_students = explode(",", $res['fixed_students']);
        //  prt(false!=$res['fixed_students'] && count($arr_fixed_students)>0);
     //  prt($arr_fixed_students);
        if (false!=$res['fixed_students'] && count($arr_fixed_students)>0) {
            foreach ($arr_fixed_students as $k => $stu_id) {

                if ($res['cycle'] == 'week') {
                    $arrTmp = $this->procWeek($res, $stu_id, 1);
                   // if (false==$arrTmp) return false;
                    $instData = array_merge($instData, $arrTmp);
                } else {

                    $arrTmp = $this->procDay($stu_id, 1);
                   // if (false==$arrTmp) return false;
                    $instData = array_merge($instData, $arrTmp);
                   // prt($instData);
                }
            }
            //prt($instData );
            if (false==$instData  || count($instData)==0) {
                //正式的学员不能为0
                return false;
            }
        }

        $arr_temp_students = explode(",",$res['temp_students']);
        if (false!=$res['temp_students'] && count($arr_temp_students)>0) {
            foreach ($arr_temp_students as $k => $stu_id) {
                if ($res['cycle'] == 'week') {
                    $arrTmp = $this->procWeek($res, $stu_id, 2);
                    $instData = array_merge($instData, $arrTmp);
                } else {
                    $arrTmp = $this->procDay($stu_id, 2);
                    $instData = array_merge($instData, $arrTmp);
                }
            }
        }

        // prt($res,0);
        $hours_msg_arr = [];
        if (count($instData)>0) {
             //检查课时
            foreach ($instData as $k=>$v) {
                $stu_left_hours = Db::name("students")->where("id={$v['students_id']}")->value("left_hours");
                if ($stu_left_hours < $res['deduct_hour']) {
                    //如果课时不满
                    $students_name = getCacheField('students', $v['students_id'], 'student_name', 0);
                    if (!in_array($students_name,$hours_msg_arr))   $hours_msg_arr[] = $students_name;
                }
            }

            $cookie_left_hours = cookie('left_hours');
            //prt($cookie_left_hours);
            if (false==$cookie_left_hours && isset($hours_msg_arr) && count($hours_msg_arr)>0) {
                echo("【" . implode("; ",$hours_msg_arr) . "】学生课时不足，上课记录无法新增！");
                die;
            }

           // DB::table('class-schedule')->delete($recId);
           // prt($instData,0);
            $studentsDB = Db::table('students');
            foreach ($instData as $k=>&$v) {
                $v['class_title'] = $res['title'];
                $v['classroom_id'] = $res['classroom_id'];
                $v['teacher_id'] = $res['teacher_id'];
                $stu_left_hours = Db::name("students")->where("id={$v['students_id']}")->value("left_hours");
                $hours = $res['deduct_hour'];
                if ($stu_left_hours<$res['deduct_hour'])  {
                    $hours = 0;
                } else {
                    //减一
                    $studentsDB->where('id', $v['students_id'])->setDec('left_hours', $v['use_hours']);
                }
                $v['use_hours'] =$hours;   //扣课时间,课时为零，不能签到

                /////
                $map = [];
                $map['students_id'] = $v['students_id'];
                $map['schedule_time'] = $v['schedule_time'];
                //教室，时间和学生，时间不能是已经上课的时间
                $scheduleTmp =  DB::table('class-schedule')->where($map)->find();

                if ($scheduleTmp['schedule_id']>0 && $scheduleTmp['classes_id']!=$v['classes_id']) {
//                    prt($scheduleTmp ,0);
//                    prt($v['classes_id'],0);
//prt("test");
                    $classes_title = getCacheField('classes',$scheduleTmp['classes_id'],'title',0);
                    $students_name = getCacheField('students',$v['students_id'],'student_name',0);

                    echo ("【".$students_name."】学生已有在ID:{$scheduleTmp['classes_id']},班级是".$classes_title.",有一条时间是".date("Y-m-d H:i:s",$scheduleTmp['schedule_time'])."上课记录无法新增！");
                    exit;
                }elseif($scheduleTmp['schedule_id']>0){
//                    prt($scheduleTmp ,0);
//                   prt($v['classes_id'],0);
//prt("test");
                } else {
                    $newInstData[] = $v;
                }

            }

            if (false==$newInstData && count($newInstData)==0) {
                $this->error('未找到维护的记录');
                exit;
            }

            DB::table('class-schedule')->insertAll($newInstData);



            return 1;

//            try{
//                $ret =  DB::table('class-schedule')->insertAll($newInstData);
//                return 1;
//            }catch(\Exception $e){
//                $this->error('未选的学员和时间');
//                exit;
//            }




        }else {
            return false;
        }

    }

    //分解周时间
    public function procWeek($res,$stu_id,$ib_fixed=1) {

        $arrWeek = string2array($res['schedule_week']);
        $instData = array();
        // prt($arrWeek,0);
        if ($res['cycle']!='week') return $instData;
        //prt($res );
//        prt($res['open_time'],0);
//       prt($res['end_time'],0);
//       prt($res['end_time']+86400,0);
//       die;

        $open_time = $res['open_time'] ;

        for($d=$open_time;$d<$res['end_time'];$d+=86400) {
            $w = date("w",$d);
             // prt($w,0);
            if (array_key_exists($w,$arrWeek)){
                $time = $arrWeek[$w];
                $schedule_time = strtotime(date("Y-m-d {$time}",$d));
                $newData['campus_id'] = $res['campus_id'];
                $newData['classes_id'] = $res['id'];
                $newData['students_id'] = $stu_id;
                $newData['ib_fixed'] = $ib_fixed;
                $newData['schedule_time'] = $schedule_time;
                $newData['create_time'] = $newData['update_time'] = date("Y-m-d H:i:s");
                $newData['status'] = 1;
 
                if ($schedule_time-time()>0 && $this->openClassStatus == 1  ) {
                    $instData[] = $newData;
                }elseif(false==$this->openClassStatus) {
                    $instData[] = $newData;
                }
//                prt($this->openClassStatus ,0);
//                prt(date("Y-m-d H:i:s",$schedule_time),0);
//                prt($instData,0);
            }
        }

        return $instData;
    }

    //分解天时间
    public function procDay($stu_id,$ib_fixed=1) {

        $instData = array();
        if ($_POST['cycle']!='day') return $instData;
        if (false== $_POST['schedule_time']) return $instData;
        $newData = [];
        foreach ($_POST['schedule_time'] as $k=>$v) {
            $newData['campus_id'] = $_POST['campus_id'];
            $newData['classes_id'] = $_POST['id'];
            $newData['students_id'] = $stu_id;
            $newData['ib_fixed'] = $ib_fixed;
            $newData['schedule_time'] = toTimestamp($v);
            if ($this->action_name=='edit') {
                 $newData['update_time']  = date("Y-m-d H:i:s");
            } else {
                $newData['create_time'] = $newData['update_time'] = date("Y-m-d H:i:s");
            }
            $newData['status'] = 1;
            $instData[] = $newData;
        }
        prt($instData);
        return $instData;
    }

    /*
     *  固定学生不能出现在一个以上的班级
     *
     */
    public function stu_search(){

        $_validate = array(
            array(false==($campus_id = $_REQUEST['campus_id']),"分校必须输入！"),
            array(false==($lesson_series=$_REQUEST['lesson_series']),"课程系列必须输入！"),
            array(false==($lesson_grader=$_REQUEST['lesson_grader']),"等级必须输入！"),
        );
        $classes_id = intval($_REQUEST['classes_id']);
        $studentname = trim($_POST['student_name']);

        foreach ($_validate as $k=>$v) {
            if ($v[0]) $this->error($v[1]);
        }
		$open_status = $_POST['open_status'];
		 
        if ($_POST['stu_type']=="temp_students") {
            $this->tmp_stu_search();
            exit;
        }
		
//        $map['status'] = 1;
//
//        if ($campus_id != false){
//            $map[] = ['campus_id', 'eq', $campus_id];
//        }
//        if ($lesson_series != false){
//            $map[] = ['lesson_series', 'eq', $lesson_series];
//        }
//        if ($lesson_grader != false){
//            $map[] = ['lesson_grader', 'eq', $lesson_grader];
//        }

        //排除报了别的班级的固定学名
        $classes_filter = [] ;
        $classes_filter[] = ["status","eq","1"];
        $classes_filter[] = ["fixed_students","neq",""];
      //  $classes_id > 0 && $classes_filter[] = ["id","eq",$classes_id];  //如果是修改，就要加上本班的学生判断条件
        $classes_filter[] = ["campus_id","eq",$campus_id];  //分校条件
        //$stu_type=='fixed_students' ;暂时未区分临时和固定学员
        //找到参班的学生,并排除掉
        $res = Db::name('classes')->where($classes_filter)->order("id desc")->field("fixed_students,temp_students")->select();
        $students_list=[];
        foreach ($res as $k=>$v) {
            $students_list[] = $v['fixed_students'];
            $students_list[] = $v['temp_students'];
        }
//        prt($students_list,0);
        $student_fitler = [];
        $student_fitler[] =  ["study_status","eq","1"];
        $student_fitler[] =  ["campus_id","eq",$campus_id];
        $student_fitler[] =  ["lesson_series","eq",$lesson_series];
        $student_fitler[] =  ["lesson_grader","eq",$lesson_grader];
        $studentname  &&    $student_fitler[] =  ["student_name","like", "%{$studentname}%"];
        $student_fitler[] =  ["id","not in", implode(",",$students_list)];
        $stu_ids = DB::name("students")->where($student_fitler)->column("id");   //找出未参班的学生

        //本班级的固定学名
        $theclass_students_arr = [];
        if ( $classes_id >0) {
            $classes_filter = [];
            $classes_filter[] = ["status", "eq", "1"];
            $classes_filter[] = ["fixed_students", "neq", ""];
            $classes_id > 0 && $classes_filter[] = ["id", "eq", $classes_id];  //如果是修改，就要加上本班的学生判断条件

            $res = Db::name('classes')->where($classes_filter)->order("id desc")->field("fixed_students,temp_students")->select();
            $theclass_students_str = '';
            foreach ($res as $k => $v) {

                $theclass_students_str .= $v['fixed_students'];   //本班的学生
                // $theclass_students_str .= $v['temp_students'];    //本班的固定学生
            }
            $theclass_students_arr = explode(",", $theclass_students_str);
         }
         $students_list = array_merge($theclass_students_arr, $stu_ids);

         $students_cache = F("students");
         foreach($students_list  as  $v) {
             if ($v>0) {
                 $attr = [];
                 $attr['name'] = "fixed_students[]";
                 $attr['class'] = "fixed_students";
                 if (in_array($v, $theclass_students_arr)) {
                     $attr['checked'] = "checked";
                     //开课，可以删除大于当前时间的数据
                    //  $open_status &&  $attr['disabled'] = 'disabled' ;
                 }
                 $attr['value'] = $v;
                 foreach ($attr as $kk => $vv) {
                     $new2[] = "{$kk}='{$vv}'";
                 }

                 $attr2 = implode(" ", $new2);
                 $title = $students_cache[$v]['student_name'];
                 unset($new2, $attr);
                 $_checkArr[] = "<label><input type='checkbox' {$attr2} />" . $title . "</label>";
             }
         }
        $_html = implode(" ",$_checkArr);
        if (false==$_html) {
            $this->error("未找到条件的数据");
            exit;
        }
        $this->result($_html);

    }

    /*
     *  tmp学生不能出现在一个以上的班级
     *  1，取全部的有效的学名
     *  2，排除本班的固定学员
     */
    public function tmp_stu_search(){

        $_validate = array(
            array(false==($campus_id = $_POST['campus_id']),"分校必须输入！"),
            array(false==($lesson_series=$_POST['lesson_series']),"课程系列必须输入！"),
            array(false==($lesson_grader=$_POST['lesson_grader']),"等级必须输入！"),
        );
        $classes_id = intval($_POST['classes_id']);
        foreach ($_validate as $k=>$v) {
            if ($v[0]) $this->error($v[1]);
        }
        $open_status = $_POST['open_status'];

        $student_fitler = [];
        $student_fitler[] =  ["study_status","eq","1"];
        $student_fitler[] =  ["campus_id","eq",$campus_id];
        $student_fitler[] =  ["lesson_series","eq",$lesson_series];
        $student_fitler[] =  ["lesson_grader","eq",$lesson_grader];
     //   $student_fitler[] =  ["id","not in", implode(",",$students_list)];
        $students_list = DB::name("students")->where($student_fitler)->column("id");   //找出未参班的学生
        //prt(DB::name("students")->getLastSql());
        //本班级的固定学名
        $theclass_gd_students_arr =[];
        $theclass_tmp_students_arr = [];
        if ($classes_id > 0) {
            $classes_filter = [];
            $classes_filter[] = ["status", "eq", "1"];
            $classes_filter[] = ["fixed_students", "neq", ""];
            $classes_id > 0 && $classes_filter[] = ["id", "eq", $classes_id];  //如果是修改，就要排除加上本班的id
            $res = Db::name('classes')->where($classes_filter)->order("id desc")->field("fixed_students,temp_students")->select();

            foreach ($res as $k => $v) {
                $v['fixed_students'] && $theclass_gd_students_arr[] = $v['fixed_students'];   //本班的学生
                $v['temp_students'] && $theclass_tmp_students_arr[] = $v['temp_students'];    //本班的固定学生
            }
        }
//prt($students_list,0);
//prt($theclass_gd_students_arr,0);
//prt($theclass_tmp_students_arr);

        $students_cache = F("students");
        foreach($students_list  as  $v) {
            if ($v>0 &&  !in_array($v, $theclass_gd_students_arr)  ) {
                $attr = [];
                $attr['name'] = "temp_students[]";
                $attr['class'] = "temp_students";
                if (in_array($v, $theclass_tmp_students_arr)) {
                    $attr['checked'] = "checked";
                    // $open_status &&  $attr['disabled'] = 'disabled' ;
                }

                $attr['value'] = $v;
                foreach ($attr as $kk => $vv) {
                    $new2[] = "{$kk}='{$vv}'";
                }

                $attr2 = implode(" ", $new2);
                $title = $students_cache[$v]['student_name'];
                unset($new2, $attr);
                $_checkArr[] = "<label><input type='checkbox' {$attr2} />" . $title . "</label>";
            }
        }
        $_html = implode(" ",$_checkArr);
        if (false==$_html) {
            $this->error("未找到条件的数据");
            exit;
        }
        $this->result($_html);
    }

    //
    public function delete() {
        if (false==($id= $_REQUEST['id'])) {
            $this->error("id参数错误");
        }

        $classesRes = DB::name("classes")->find($id);
        if (false==$classesRes) {
            $this->error("未找到记录");
        }
        if ($classesRes['open_time'] <= time()) {
            //开班了
            $this->error("不能删除数据，已经开班了");
            exit;
        }
        //删除数据
        $classes_filter = [] ;
        $classes_filter[] = ["classes_id","eq",$id];
        $res = Db::name('class-schedule')->where($classes_filter)->delete();
        $res = Db::name('classes')->delete($id);
        $this->success("成功删除",adminUrl('classes/index'));
     }

    public function totalclass($last_id) {
        if (false==($id=intval($last_id))) {
            return false;
        }
        $s_db = DB::name("classes");

        $campus_id = $s_db->where("id={$id}")->value("campus_id");

        $count = DB::name("classes")->where("campus_id={$campus_id}")->count();

        DB::name("campus")->where("id={$campus_id}")->update([
            'classroom_totalnum'=>$count
        ]);
    }
}