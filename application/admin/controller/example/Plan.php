<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/24
 * Time: 8:42
 */

namespace app\admin\controller;
use controller\BasicAdmin;
use service\DataService;
use service\NodeService;
use service\ToolsService;
use think\Db;
use think\Cache;
use org\FieldFromClass;
use org\ContentList;
use org\QueryFrom;
use think\Exception;
use think\Request;
use org\CreateFromControl;
class Plan extends BasicAdmin
{
//    public $table,$fields,$map;
    public function __construct()
    {
        parent::__construct();




    }

    public function index(){

        $db = DB::table($this->table) ;
        $pk = $db->getPk();
        $map = [];
        if (false!= ($study_status=intval($_REQUEST['study_status']))) {
            $map[] = ["study_status","=",$study_status];
        }
        if (session('user.campus_id')>0) {
            if (in_array("campus_id",$db->getTableFields())){
                $map[] = ['campus_id', '=', session('user.campus_id')];
            }
        }

        $db->where($map);
        $tbList = $this->_list( $db ,true,false);
        $this->assign("pk",$pk);
        $this->assign("pages",$tbList['page']);
        $this->assign("list",$tbList['list']);

        $this->assign("ContentList",new ContentList());

        if (intval($_REQUEST['menu'])>0) $this->assign("formMenu",$_REQUEST['menu']);


//        if ($study_status!=5) {
//            // prt($this->fields);
//            foreach ($this->fields as $k=>&$v) {
//                //不是退学退费模块，不显示退学退费
//                if (in_array($v['field'],['leaving_fees','leaving_updatetime'])) {
//                    $v['listShowField'] = '0';
//                }else {
//                    $v['listShowField'] = '1';
//                }
//            }
//            $this->assign("tbField",$this->fields);
//        }

        return $this->fetch();
        exit;
    }



    public function _query_filter(&$map) {

        foreach ($_REQUEST['query'] as $k=>&$v) {
            if ($k=='province' && $v['title']==-1) unset($v['title']);
            if ($k=='city' && $v['title']==-1) unset($v['title']);

            if (!empty($v['title'])) {
                $title = trim($v['title']);
                if (!isset($v['symbol'])) {
                    $frmMap[]  = $map[]  = array($k,"eq","{$title}");
                }elseif(strtolower($v['symbol'])=='like') {
                    $frmMap[]  = $map[]  = array($k,"like","%{$title}%");
                }else {
                    $frmMap[]  = $map[]  = array($k,"{$v['symbol']}","{$title}");
                }
            }

            if (!empty($v['starttime'])) {
                $starttime = $v['starttime'].' 0:0:0';
                $starttime = strtotime($starttime);
                $frmMap[]  = array($k.'_start_time',"egt","{$starttime}");
                $map[]  = array($k,"egt","{$starttime}");
            }

            if (!empty($v['endtime'])) {
                $endtime = $v['endtime'].' 23:23:59';
                $endtime = strtotime($endtime);
                $frmMap[]  = array($k.'_end_time',"elt","{$endtime}");
                $map[]  = array($k,"elt","{$endtime}");
            }

            if (!empty($v['province']) && $v['province']>0) {

                $frmMap[]  = $map[]  = array('province',"eq","{$v['province']}");
            }

        }

        $frmMap &&  $this->assign("map",json_encode($frmMap));

    }

    public function query(){

        $this->assign ('QueryFrom', new QueryFrom() );

        if ($_REQUEST['param']) {
            foreach (json_decode($_REQUEST['param']) as $k => $v) {
                $map[$v[0]] = array('symbol' => $v[1], 'title' => $v[2]);
            }
//            prt($_REQUEST['param'],0);
//            prt($map,0);
            $map && $this->assign('param', $map);
        }

        $tplName =  (app('view')->exists("{$this->controller_name}/query"))?"{$this->controller_name}/query":"content/query";
        $tplCn = $this->fetch($tplName);
        savelog($tplCn ,"content_query_frm");
        echo $tplCn;
    }

    public function edit()
    {
  
        $res = DB::name("system_module")->where("name","=",$this->controller_name)->find();
        $moduleid = $res['id'];

        $fields = $this->FieldCache($moduleid,'get');
        //prt($fields);
        $this->assign ( 'fields', $fields );

        $id = intval($_REQUEST ['id']);
        if ($id>0) {
            //  parent::returnMsg ('error',L('edit_error') );
            $vo = DB::name($this->controller_name)->getById( $id );

        }

        $staff_type_list = [
            'teacher'=>'教师',
            'market'=>"市场",
            'salesman'=>'销售',
            'aftersales'=>'售后'
        ];

        if($vo['staff_type'] !=false && $vo['staff_id'] != false){
            $vo['staff_type_name'] = $staff_type_list[$vo['staff_type']];
            $staffRes = DB::name($vo['staff_type'])->field("{$vo['staff_type']}_name")->where(['id'=>$vo['staff_id']])->find();
            $staffnamekey = "{$vo['staff_type']}_name";
            $vo['staff_id_name'] = $staffRes[$staffnamekey];
        }


        $pk = DB::name($this->controller_name)->getPk();
        $this->assign("pk",$pk);

        $this->assign ( 'vo', $vo );
        //   $this->assign ( 'form', $form );
        $this->assign ( 'FieldFromClass', new FieldFromClass() );
        $this->assign("ContentList",new ContentList());
        $tplName =  (app('view')->exists("{$this->controller_name}/edit"))?"{$this->controller_name}/edit":"content/edit";
        if($this->action_name=='edit'){
            return  $this->fetch($tplName);
        }elseif($this->action_name=='add'){
            echo  $this->fetch($tplName);
            return false;
        }

    }


    public function staff(){
        return $this->fetch();
        exit;

    }

    public function before_update() {
        //        $moduleId = $this->getModuleId($this->controller_name);
        //        $fields = F($moduleId."_Field");
        //         prt($this->fields,0);
        foreach ($_POST as $kk=>&$vv) {
            foreach ($this->fields as $k => $v) {
                if ($v['type'] == 'datetime' && $v['field']==$kk) {   //转换时间格式
                    if ($v['field']=='updatetime') {
                        $_POST['updatetime'] =  time();
                    }else {
                        $vv = toTimestamp($vv);
                        // $vv = strtotime($vv);
                    }

                    if ($v['field']=='createtime') {
                        $_POST['createtime'] =  time();
                    }else {
                        $vv = toTimestamp($vv);
                    }
                }

                if ($v['type']=='typeid' && $v['field']==$kk ) {
                    is_array($vv) && $vv = implode(",",$vv);
                }
                if ($v['type']=='editor') {  }
                if ($v['type']=='image' && $v['field']==$kk) {
                    $vv = implode(",",$vv);
                }
                if ($v['type']=='regional_Linkage') {
                    $vv==-1  && $vv=0;
                }
            }
        }
    }




}