<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/24
 * Time: 8:42
 */

namespace app\admin\controller;
use controller\BasicAdmin;
use service\DataService;
use service\NodeService;
use service\ToolsService;
use think\Db;
use think\Cache;
use org\FieldFromClass;
use org\ContentList;
use org\QueryFrom;
use think\Exception;
use think\Request;
use org\CreateFromControl;
class Students_stopclass extends BasicAdmin
{
    public $table, $fields, $map;

    public function __construct()
    {
        parent::__construct();

        $this->url = url("/admin/{$this->controller_name}/index", ['spm' => $_REQUEST['spm'], 'runtime' => time()]);  // 放在前面组装
        $this->table = "students-stopclass";
        $moduleId = $this->getModuleId($this->table);
        $this->fields = F($moduleId."_Field");
        $this->assign("tbField",$this->fields);
        $this->assign("fields",$this->fields);
        $this->assign("url_index",$this->url);

        $pk = DB::name($this->table)->getPk();
        $pk && $this->assign("pk", $pk);


    }

    public function index(){

        $db = DB::table($this->table) ;
        $pk = $db->getPk();
        $map=[];
        if (session('user.campus_id')>0) {
            if (in_array("campus_id",$db->getTableFields())){
                $map[] = ['campus_id', '=', session('user.campus_id')];
            }
        }
        $db->where($map);
        $tbList = $this->_list( $db ,true,false);
        // echo '<pre>';var_dump($tbList['list']);
        foreach ($tbList['list'] as $k=>&$v) {
            // $study_status = getCacheField("students",$v['students_id'],'study_status',0);
            // $v['study_status'] = L('study_status_'.$study_status);
            $study_status = Db::name('students')->field('study_status')->find($v['students_id']);
            $v['study_status'] = L('study_status_'.$study_status['study_status']);
        }
        // echo '<pre>';var_dump($tbList['list']);die;
        $this->assign("pk",$pk);
        $this->assign("pages",$tbList['page']);
        $this->assign("list",$tbList['list']);

        $this->assign("ContentList",new ContentList());
        echo $this->fetch();
        exit;
    }


    public function _query_filter(&$map) {

        foreach ($_REQUEST['query'] as $k=>&$v) {
            if ($k=='province' && $v['title']==-1) unset($v['title']);
            if ($k=='city' && $v['title']==-1) unset($v['title']);

            if (!empty($v['title'])) {
                $title = trim($v['title']);
                if (!isset($v['symbol'])) {
                    $frmMap[]  = $map[]  = array($k,"eq","{$title}");
                }elseif(strtolower($v['symbol'])=='like') {
                    $frmMap[]  = $map[]  = array($k,"like","%{$title}%");
                }else {
                    $frmMap[]  = $map[]  = array($k,"{$v['symbol']}","{$title}");
                }
            }

            if (!empty($v['starttime'])) {
                $starttime = $v['starttime'].' 0:0:0';
                $starttime = strtotime($starttime);
                $frmMap[]  = array($k.'_start_time',"egt","{$starttime}");
                $map[]  = array($k,"egt","{$starttime}");
            }

            if (!empty($v['endtime'])) {
                $endtime = $v['endtime'].' 23:23:59';
                $endtime = strtotime($endtime);
                $frmMap[]  = array($k.'_end_time',"elt","{$endtime}");
                $map[]  = array($k,"elt","{$endtime}");
            }

            if (!empty($v['province']) && $v['province']>0) {

                $frmMap[]  = $map[]  = array('province',"eq","{$v['province']}");
            }

        }

        $frmMap &&  $this->assign("map",json_encode($frmMap));

    }

    public function edit()
    {

        $res = DB::name("system_module")->where("name","=",$this->controller_name)->find();
        $moduleid = $res['id'];

        $fields = $this->FieldCache($moduleid,'get');
        //prt($fields);
        $this->assign ( 'fields', $fields );

        $id = intval($_REQUEST ['id']);
        if ($id>0) {
            $vo = DB::name($this->table)->getById( $id );

        } else {
            $vo['stopclass_starttime'] = time();
            $vo['stopclass_endtime'] = time();
        }
        $this->assign ( 'vo', $vo );

        $pk = DB::name($this->table)->getPk();
        $this->assign("pk",$pk);


        $this->assign ( 'FieldFromClass', new FieldFromClass() );

        echo $this->fetch( );
        exit;
    }

    //恢复上课
    //Students_stopclass/renew
    public function renew() {


        $id = intval($_REQUEST['id']);
        if (false==$id) return false;
        $this->assign("id",$id);
        $stopRow = DB::table("students-stopclass")->find($id);
        $this->assign("stopRow",$stopRow);
        if (false==$_POST) {

            echo  $this->fetch();
            die;
        }


        if (false==($renew_date=$_POST['renew_date'])) {
            $this->error("请输入renew_date的值");
            return false;
        }

        //1，该用户是否停课
        $stopRow = DB::table("students-stopclass")->find($id);
        $studentRow = DB::table("students")->find($stopRow['students_id']);

        if ($studentRow['study_status']!=4) {
            $this->error("该学生不是停课状态，请检查");
            return false;
        }


        if ($stopRow['stopclass_starttime'] > strtotime($renew_date) ) {
            $this->error("恢复时间不能小于停课开始时间");
            exit;
        }

        if ($stopRow['stopclass_endtime'] < strtotime($renew_date) ) {
            $this->error("恢复时间不能大于停课时间截止时间");
            exit;
        }


        //2,存盘恢复上课时间的字段
        $saveRet = DataService::save("students-stopclass",array(
            "id"=>$stopRow['id'],
            "renew_date"=>strtotime($renew_date)
        ));


     //   if ($saveRet>0 || 1) {
            //3，计算学生的上课的有效起止和截止时间
            $day =floor((toTimestamp($stopRow['stopclass_endtime'])-toTimestamp($_POST['renew_date']))/86400);
            $closedate = date("Y-m-d",$studentRow['expire_time']);

            $newday = date('Y-m-d 23:59:59',strtotime("$closedate -{$day} day"));
//prt($day,0);
//prt($closedate,0);
//prt($newday);
            $expire_time =  strtotime($newday);
            $debug_stopRow = DB::table("students-stopclass")->find($id);
            savelog("恢复上课时间调整开始......","renew_date");
            savelog($debug_stopRow,"renew_date");

            $data = [];
            $current_day = strtotime(date("Y-m-d 23:59:59"));
            if (strtotime($renew_date)<=$current_day) {
                $data['study_status'] =1;   //输入的时间比当前时间提前了，就恢复状态
                DataService::save("students-stopclass", ['status'=>0,"id"=>$stopRow['id']]);

            } else {
                $data['study_status'] = 4;
            }
            $data['id'] = $studentRow['id'];  //修改到期时间
            $data['expire_time'] = $expire_time;  //修改到期时间

            $result = DataService::save("students", $data);
            if ($result>0) {
                $url = adminUrl("{$this->controller_name}/index", ['runtime' => time()]);
                $this->success("该学生的到期时间修改为".date("Y-m-d", $expire_time),$url);
            }

       // }


    }



    //计算停课时间
    private function sum_stopclass_time($closedate,$startdate,$enddate, $is_renew = false){
        $day =floor((($enddate)-($startdate))/86400);
        // var_dump(date('Y-m-d H:i:s', $startdate));
        // var_dump(date('Y-m-d H:i:s', $enddate));
        // var_dump(strtotime($enddate));
        // var_dump($day);die;
//prt($day,0);
//prt($startdate,0);
//prt($enddate,0);
//prt($closedate);
        $closedate = date("Y-m-d",$closedate);
        //是否为恢复
        if($is_renew){
            $new_closedate = date('Y-m-d 23:59:59',strtotime("$closedate -{$day} day"));
        }else{
            $new_closedate = date('Y-m-d 23:59:59',strtotime("$closedate +{$day} day"));
        }
        return $new_closedate;
    }

    //截止7-1 ，请假 6-2 到 6-8

    public function testTime($closedate = 1530374400,$startdate=1527868800,$enddate=1528387200){
        $day =floor((($enddate)-($startdate))/86400);
        $day2 =floor(((strtotime('2018-07-07 23:59:59'))-strtotime('2018-07-01 23:59:59'))/86400);
        // prt((($enddate)-($startdate))/86400,1);
        prt($day,0);
        prt($day2,0);
        prt($closedate,0);
        prt($startdate,0);
        prt($enddate,0);
        $closedate = date("Y-m-d",$closedate);
        prt($closedate,0);

        $new_closedate = date('Y-m-d 23:59:59',strtotime("$closedate +{$day} day"));
        prt($new_closedate,0);

        die;
        return $new_closedate;
    }


    //停课维护

    function update()
    {

        $sched  = DB::table($this->table);
        $moduleid = $this->getModuleId($this->table);
        if ($moduleid > 0) $this->validateField($moduleid);



        $_validate = array(
            array(false==$_POST['stopclass_starttime'],"停课开始时间必须输入！"),
            array(false==$_POST['stopclass_endtime'],"停课截止时间必须输入！"),
            array(false==ibfalse($_POST['campus_id']),"分校必须输入！"),
            array(false==ibfalse($_REQUEST['students_id']),"学生必须输入！"),
            array(false==intval($_REQUEST['students_id']),"学生id参数不正确！"),
        );

        foreach ($_validate as $k=>$v) {
            if ($v[0]) $this->error($v[1]);
        }
         
        $students_id =  intval($_REQUEST['students_id']);
        $studentsRes = DB::table("students")->field("id,admission_time,expire_time")->find($students_id);
        if (toTimestamp($_POST['stopclass_starttime']) < toTimestamp($studentsRes['admission_time']) ) {
            $this->error("停课起始时间不能小于入学时间");
            exit;
        }


        if (toTimestamp($_POST['stopclass_endtime'])> toTimestamp($studentsRes['expire_time']) ) {
            $this->error("停课截止时间不能大于到期时间");
            exit;
        }

        if (toTimestamp($_POST['stopclass_endtime']) < toTimestamp($_POST['stopclass_starttime']) ) {
            $this->error("停课截止时间不能小于停课开始时间");
            exit;
        }

        $add  = $_POST;
        $add['stopclass_starttime'] = strtotime($_POST['stopclass_starttime']." 0:0:0");
        $add['stopclass_endtime'] = strtotime($_POST['stopclass_endtime']." 23:59:59");
        $add['status'] = 1;
        $add['memo'] = trim($_POST['memo']) ;
        $add['createtime'] = time() ;
        if ($add['stopclass_starttime']>$add['stopclass_endtime']) {
            $this->error("开始时间不能大于截止时间");
        }
        $time = $add['stopclass_starttime'] ;
        $countMap = [];
        $countMap[] = ["status","=",1];
        $countMap[] = ["students_id","=",$_REQUEST['students_id']];
        $countMap[] = ["stopclass_starttime","<=",$time];
        $countMap[] = ["stopclass_endtime",">=",$time];   //在此时间段存在记录
        $count = $sched->where($countMap)->count();

        if($count>0) {
            $this->error("在".$_POST['stopclass_starttime']."-".$_POST['stopclass_endtime']."时间段，该学生已经存在停课停学的记录");
            exit;
        }
 
            $saveRet = DataService::save($this->table, $add);

            //停课开始时间小于于现在的时间才改变状态

                if ($saveRet>0 && strtotime($_POST['stopclass_starttime'])<=time() ) {
                    ////
                    //处理为停课状态
                    $new = [];
                    $new['study_status'] = 4;
                    $new['id'] = $_REQUEST['students_id'];
                    $stuSaveRet = DataService::save("students", $new);

                    //修改student表的记录
                    $expire_time  = $this->sum_stopclass_time($studentsRes['expire_time'],strtotime($_POST['stopclass_starttime']),strtotime($_POST['stopclass_endtime']));
                   // prt($expire_time);
                    $stuData = array();
                    $stuData['id'] = $students_id;
                    $stuData['expire_time'] =  strtotime($expire_time);
                    $studentsRes = DataService::save("students", $stuData);

                    $msg="该学生的上课的状态修改为停课停学，截止时间已经修改为".$expire_time;
                }

            $this->returnMsg('success', $this->url,$msg?$msg:'');


    }





}