<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2017 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://think.ctolog.com
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zoujingli/ThinkAdmin
// +----------------------------------------------------------------------

namespace app\admin\controller;

use controller\BasicAdmin;
use org\FieldFromClass;
use org\CreateFromControl;
use org\ContentList;
use service\DataService;
use service\NodeService;
use service\ToolsService;
use think\App;
use think\Db;
use think\Cache;

use think\exception\ErrorException;
use think\Loader;
use think\Exception;

/**
 * 清理缓存
 * Class Admincache
 * @package app\admin\controller
 * @author Anyon <zoujingli@qq.com>
 * @date 2017/02/15 10:41
 */
class Class2schedule extends BasicAdmin
{

    public function __construct ()
    {
        parent::__construct();
        $this->classDb = DB::table("classes");
        $this->scheduleDb = DB::table("class-schedule");
    }

    /*
   *  课表细表： 列出class-schedule表内容，过滤条件是班级名称，教室，教师，上课时间。不含学生。
    * 将class-schedule表的调课表放在class-adjust课程调节表内容。
   */
    public function index ()
    {

        $adjustDb = DB::table("class-adjust");

        $db = $this->scheduleDb->distinct(true)->field("campus_id,class_title,classroom_id,teacher_id,classes_id");

        $pk = $db->getPk();
        $map = [];
        if (session('user.campus_id')>0) {
            if (in_array("campus_id",$db->getTableFields())){
                $map[] = ['campus_id', '=', session('user.campus_id')];
            }
        }
        $db->where($map);
        $tbList = $this->_list($db, true, false);


        $this->assign("pk", $pk);
        $this->assign("pages", $tbList['page']);
        $this->assign("list", $tbList['list']);
        $this->assign("ContentList", new ContentList());

        return $this->fetch();

    }

    /*
    *  课表细表： 列出class-schedule表内容，过滤条件是班级名称，教室，教师，上课时间。不含学生。
     * 将class-schedule表的调课表放在class-adjust课程调节表内容。
     *  http://ivehice.mydanweb.com/admin#/admin/class2schedule/scheduleList.html?spm=m-45-46-49
    */
//    public function scheduleList()
//    {
//
//        $adjustDb = DB::table("class-adjust");
//
//        $db = $this->scheduleDb->distinct(true)->field("classes_id,class_title,classroom_id,teacher_id,schedule_time,is_adjust,is_stop");
//
//        $pk = $db->getPk();
//
//        $tbList = $this->_list($db, true, false);
//prt($tbList);
//      //  prt($db->getLastSql());
//        foreach ($tbList['list'] as $k => &$v) {
//            $res = DB::table("class-adjust")->where(['classroom_id' => $v['classroom_id'], 'teacher_id' => $v['teacher_id'], 'schedule_time' => $v['schedule_time']])->find();
//
//            if ($res) {
//                $v = array_merge($v, $res);
//            }
//            if ($v['schedule_time']) {
//                $v['schedule_timestamps'] = $v['schedule_time'];
//            }
//            $v['schedule_time'] = date("Y-m-d H:i:s", $v['schedule_time']);
//            if ($v['adjust_schedule_time']) {
//                $v['adjust_schedule_time'] = date("Y-m-d H:i:s", $v['adjust_schedule_time']);
//            }
//            $v['adjust_txt'] = $res['adjust_txt'];
//            $v['adjust_teacher_id'] = $res['adjust_teacher_id'];
//
//        }
//        // die;
//       prt($tbList);
//
//        $this->assign("pk", $pk);
//        $this->assign("pages", $tbList['page']);
//        $this->assign("list", $tbList['list']);
//        $this->assign("ContentList", new ContentList());
//
//        return $this->fetch();
//
//    }


    //调课
    public function adjust ()
    {

        $map['classes_id'] = intval($_REQUEST['classes_id']);
        $map['teacher_id'] = intval($_REQUEST['teacher_id']);
        $map['schedule_time'] = $_REQUEST['schedule_timestamps'];

        $scheduleRes = DB::name("class-schedule")->field("campus_id,classes_id,schedule_id,class_title,classroom_id,teacher_id,schedule_time")->where($map)->find();  //需要调整的计划表记录，取一条表示有记录,这个表由于是多个学生对应一个老师，所以后面调节需要批量替换
        // prt(DB::name("class-schedule")->getLastSql(),0);
        if (false == $scheduleRes) {
            $this->error("未到到记录");
            return false;
        }

        $this->assign("class_title", $scheduleRes['class_title']);
        $this->assign("teacher_current_id", $scheduleRes['teacher_id']);
        $this->assign("schedule_id", $scheduleRes['schedule_id']);
        $this->assign("schedule_current_time", date("Y-m-d H:i:s", $scheduleRes['schedule_time']));

        if ($_POST) {
            //   prt($scheduleRes,1);
            //保存调课记录 //class_adjust
            $data = [];
            if (false != $_POST['adjust_schedule_time'] && strtotime($_POST['adjust_schedule_time']) == $scheduleRes['schedule_time'] && $_POST['adjust_teacher_id'] == $scheduleRes['teacher_id']) {
                $this->error("记录的时间和教师内容一样，无需变更！");
                exit;
            }
            if ($_POST['adjust_schedule_time'] == false && $_POST['adjust_teacher_id'] == $scheduleRes['teacher_id']) {
                $this->error("记录的时间和教师内容一样，无需变更！");
                exit;
            }
            if (intval($_POST['adjust_teacher_id']) > 0) {
                $data['adjust_teacher_id'] = $scheduleRes['teacher_id'];
                $data['teacher_id'] = $_POST['adjust_teacher_id'];  //新的老师ID
            }
            if (false != $_POST['adjust_schedule_time']) {
                $data['adjust_schedule_time'] = $scheduleRes['schedule_time'];
                $data['schedule_time'] = strtotime($_POST['adjust_schedule_time']);  //新的时间
            }
            $data['adjust_txt'] = trim($_POST['adjust_txt']);
            // $data['schedule_id'] =trim($_POST['schedule_id']);
            // $data['is_adjust'] =1;
            $data['class_title_id'] = $scheduleRes['class_title'];
            $data['classroom_id'] = $scheduleRes['classroom_id'];
            $data['update_time'] = time();
            $data['adjust_type'] = 1; //调课
            try {
                // $saveRet = DataService::save(Db::name("class-schedule"),$data,'schedule_id');
                // class-adjust 加记录
                $adjustDb = DB::table("class-adjust");
                $pk = $adjustDb->getPk();
                $saveRet = DataService::save($adjustDb, $data, $pk);

                //要做对class-schedule，更新上课时间,上课老师，调课的标记is_adjust=1，这一步要做
                //批量更新class-schedule
                $updateRet = Db::table("class-schedule")->where(['teacher_id' => $data['teacher_id'], 'classroom_id' => $scheduleRes['classroom_id'], 'schedule_time' => $scheduleRes['schedule_time']])->update(['schedule_time' => $data['schedule_time'], 'is_adjust' => 1, 'update_time' => date('Y-m-d H:i:s', time())]);
            } catch (\Exception $e) {
                $this->error('执行错误,' . $e->getMessage());
            }

            $this->success('调课完成', adminUrl('/admin/class-schedule/index'));
        } else {

            $teacherList = DB::name("teacher")->where("status=1 and campus_id=".$scheduleRes['campus_id'])->column("id,teacher_name");
            $this->assign("CreateFromControl", new CreateFromControl());
            $this->assign("teacherList", $teacherList);

            echo $this->fetch("class2schedule/adjust_frm");
        }

    }

    /*
 *  课表细表： 列出class-schedule表内容，过滤条件是班级名称，教室，教师，上课时间。不含学生。
  * 将class-schedule表的调课表放在class-adjust课程调节表内容。
 */
    function teacherlist() {

        $adjustDb = DB::table("class-adjust");
        $teacher_id  = intval($_REQUEST['teacher_id']);
        $classes_id = intval($_REQUEST['classes_id']);
        $db = $this->scheduleDb->distinct(true)->where("teacher_id={$teacher_id} and classes_id={$classes_id}")->field("campus_id,classes_id,class_title,classroom_id,teacher_id,schedule_time,is_adjust,is_stop")->order("schedule_time desc");
        $Res = $db->select();

        $total = count($Res);

        $pk = $db->getPk();
        $this->rows = 70;
        $this->assign("rows", $this->rows);
        $tbList = $this->_list($db, true, false,$total);

      
        foreach ($tbList['list'] as $k => &$v) {
            $res = DB::table("class-adjust")->where(['classroom_id' => $v['classroom_id'], 'teacher_id' => $v['teacher_id'], 'schedule_time' => $v['schedule_time']])->find();

            if ($res) {
                $v = array_merge($v, $res);
            }
            if ($v['schedule_time']) {
                $v['schedule_timestamps'] = $v['schedule_time'];
            }
            $v['schedule_time'] = date("Y-m-d H:i:s", $v['schedule_time']);
            if ($v['adjust_schedule_time']) {
                $v['adjust_schedule_time'] = date("Y-m-d H:i:s", $v['adjust_schedule_time']);
            }
            $v['adjust_txt'] = $res['adjust_txt'];
            $v['adjust_teacher_id'] = $res['adjust_teacher_id'];

        }
        // die;
        // prt($tbList);

        $this->assign("pk", $pk);
        $this->assign("pages", $tbList['page']);
        $this->assign("list", $tbList['list']);
        $this->assign("ContentList", new ContentList());

        return $this->fetch();

    }



    //查询学生
    function stulist ()
    {
        $map['teacher_id'] = intval($_REQUEST['teacher_id']);
        $map['classes_id']= intval($_REQUEST['classes_id']);
        $map['schedule_time'] = $_REQUEST['schedule_timestamps'];
        $db = $this->scheduleDb->where($map);
        $this->rows = 70;
        $this->assign("rows", $this->rows);
        $pk = $db->getPk();
        $tbList = $this->_list($db, true, false);

        if (($classes_id=$tbList['list'][0]['classes_id'])>0) {
            $classesInfo = DB::name("classes")->find($classes_id);
            $this->assign("classesInfo", $classesInfo);
        }
        $this->assign("pk", $pk);
        $this->assign("pages", $tbList['page']);
        $this->assign("list", $tbList['list']);
        $this->assign("ContentList", new ContentList());

        return $this->fetch();
        exit;
    }

    //停课
    public function stop ()
    {
        $map['classroom_id'] = intval($_REQUEST['classroom_id']);
        $map['teacher_id'] = intval($_REQUEST['teacher_id']);
        $map['schedule_time'] = $_REQUEST['schedule_timestamps'];
        $scheduleRes = DB::name("class-schedule")->field("campus_id,schedule_id,class_title,classroom_id,teacher_id,schedule_time")->where($map)->find();  //需要调整的计划表记录，取一条表示有记录,这个表由于是多个学生对应一个老师，所以后面调节需要批量替换
        if (false == $scheduleRes) return false;

        $data['update_time'] = time();
        $data['adjust_type'] = 2; //停课
        $data['class_title_id'] = $scheduleRes['class_title'];
        $data['classroom_id'] = $scheduleRes['classroom_id'];
        $data['teacher_id'] = $scheduleRes['teacher_id'];
        $data['schedule_time'] = $scheduleRes['schedule_time'];
        // prt($data);
        try {
            // $saveRet = DataService::save(Db::name("class-schedule"),$data,'schedule_id');
            // class-adjust 加记录
            $adjustDb = DB::table("class-adjust");
            $pk = $adjustDb->getPk();
            $saveRet = DataService::save($adjustDb, $data, $pk);

            //要做对class-schedule，更新上课时间,上课老师，调课的标记is_adjust=1，这一步要做
            //批量更新class-schedule
            $updateRet = Db::table("class-schedule")->where(['teacher_id' => $data['teacher_id'], 'classroom_id' => $scheduleRes['classroom_id'], 'schedule_time' => $scheduleRes['schedule_time']])->update(['adjust_type' => 2,'is_adjust' => 1,'is_stop' => 1, 'update_time' => date('Y-m-d H:i:s', time())]);
        } catch (\Exception $e) {
            $this->error('执行错误,' . $e->getMessage());
        }

        // admin/class2schedule/index
        $this->success('停课完成', adminUrl('/admin/class2schedule/index'));


    }
    //查询条件
    function query()
    {

        $Mdl = Db::name('teacher');
        $pk = $Mdl->getPk();
        $order = "$pk asc";
        $fieldList =  $Mdl->getTableFields();
        if (in_array('sort',$fieldList)){
            $order = 'sort asc';
        };
        $map[] = ['status','=','1'];
        if (session('user.campus_id')>0) {
            if (in_array("campus_id",$Mdl->getTableFields())){
                $map[] = ['campus_id', '=', session('user.campus_id')];
            }
        }

        $teacherList = $Mdl->where($map)->order($order)->cache(true,60)->column([$pk,'teacher_name']);
        $this->assign('teacherList', $teacherList);
        return $this->fetch();

    }

    //查询条件

    public function _query_filter(&$map) {

        if(isset($_REQUEST['query']['teacher_id'])){
            $map[] = ['teacher_id', 'eq', $_REQUEST['query']['teacher_id']];
        }
        if(isset($_REQUEST['query']['schedule']['starttime']) && isset($_REQUEST['query']['schedule']['starttime'])!=false){
            $map[] = ['schedule_time', 'between', [$_REQUEST['query']['schedule']['starttime'], $_REQUEST['query']['schedule']['endtime']]];
        }
        if(isset($_REQUEST['query']['is_adjust'])){
            $map[] = ['is_adjust', 'eq', $_REQUEST['query']['is_adjust']];
        }

        foreach ($_REQUEST['query'] as $k=>&$v) {


            if (!empty($v['starttime'])) {
                $starttime = $v['starttime'].' 0:0:0';
                $starttime = strtotime($starttime);
                $frmMap[]  = array($k.'_start_time',"egt","{$starttime}");
                $map[]  = array("schedule_time","egt","{$starttime}");
            }

            if (!empty($v['endtime'])) {
                $endtime = $v['endtime'].' 23:23:59';
                $endtime = strtotime($endtime);
                $frmMap[]  = array($k.'_end_time',"elt","{$endtime}");
                $map[]  = array('schedule_time',"elt","{$endtime}");
            }

            if (!empty($v['teacher_id']) && $v['teacher_id']>0) {

                $frmMap[]  = $map[]  = array('teacher_id',"eq","{$v['teacher_id']}");
            }
            if (!empty($v['is_adjust'])) {
                $frmMap[]  = $map[]  = array('is_adjust',"eq","{$v['is_adjust']}");
            }

            if (!empty($v['is_stop'])) {

                $frmMap[]  = $map[]  = array('is_stop',"eq","{$v['is_stop']}");
            }

        }

        $frmMap &&  $this->assign("map",json_encode($frmMap));

    }

    //新增临时学员
    public function addtmpstu() {

        //权限
        $teacher_id = intval($_REQUEST['teacher_id']);
        $classes_id = intval($_REQUEST['classes_id']);
        $schedule_timestamps = $_REQUEST['schedule_timestamps'];

        if (false==$teacher_id || false==$classes_id || false==$schedule_timestamps ) {
            $this->error("传参无效,errorline:".__LINE__);
        }

        $map = [];
        $map[] = ["teacher_id","=",$teacher_id];
        $map[] = ["classes_id","=",$classes_id];
        $map[] = ["schedule_time","=",$schedule_timestamps];
        $scheduleInfo = Db::name('class-schedule')->where($map)->find();
        $classesInfo = Db::name('classes')->where("id={$scheduleInfo['classes_id']}")->find();
        if (false==$scheduleInfo) {
            $this->error("找到对应的记录,errorline:".__LINE__);
        }
        $this->assign("scheduleInfo", $scheduleInfo);
        $this->assign("classesInfo", $classesInfo);




        $this->assign("CreateFromControl", new CreateFromControl());
        echo $this->fetch();
        exit;
    }

    /*
  *    学生不能出现在一个以上的班级
  *  1，取全部的有效的学名
  *  2，排除本班的固定学员
  */
    public function tmp_stu_search(){

        $_validate = array(
            array(false==($campus_id = $_POST['campus_id']),"分校必须输入！"),
            array(false==($lesson_series=$_POST['lesson_series']),"课程系列必须输入！"),
            array(false==($lesson_grader=$_POST['lesson_grader']),"等级必须输入！"),
        );
        $classes_id = intval($_POST['classes_id']);
        $studentname = trim($_POST['student_name']);

        foreach ($_validate as $k=>$v) {
            if ($v[0]) $this->error($v[1]);
        }
        $open_status = $_POST['open_status'];

        $student_fitler = [];
        $student_fitler[] =  ["study_status","eq","1"];
        $student_fitler[] =  ["campus_id","eq",$campus_id];
        $student_fitler[] =  ["lesson_series","eq",$lesson_series];
        $student_fitler[] =  ["lesson_grader","eq",$lesson_grader];
        $student_fitler[] =  ["lesson_grader","eq",$lesson_grader];
        $studentname  &&    $student_fitler[] =  ["student_name","like", "%{$studentname}%"];
        $students_list = DB::name("students")->where($student_fitler)->column("id");   //找出未参班的学生
        //prt(DB::name("students")->getLastSql());
        //本班级的固定学名
        $theclass_gd_students_arr =[];
        $theclass_tmp_students_arr = [];
        if ($classes_id > 0) {
            $classes_filter = [];
            $classes_filter[] = ["status", "eq", "1"];
            $classes_filter[] = ["fixed_students", "neq", ""];
            $classes_id > 0 && $classes_filter[] = ["id", "eq", $classes_id];  //如果是修改，就要排除加上本班的id
            $res = Db::name('classes')->where($classes_filter)->order("id desc")->field("fixed_students,temp_students")->select();

            foreach ($res as $k => $v) {
                $v['fixed_students'] && $theclass_students_arr[] = $v['fixed_students'];   //本班的学生
                $v['temp_students'] && $theclass_students_arr[] = $v['temp_students'];    //本班的固定学生
            }
        }

        $students_cache = F("students");

        foreach($students_list  as  $v) {
            if ($v>0 &&  !in_array($v, $theclass_students_arr)  ) {
                $attr = [];
                $attr['name'] = "temp_students[]";
                $attr['class'] = "temp_students";
                if (in_array($v, $theclass_tmp_students_arr)) {
                    $attr['checked'] = "checked";
                    $open_status &&  $attr['disabled'] = 'disabled' ;
                }

                $attr['value'] = $v;
                foreach ($attr as $kk => $vv) {
                    $new2[] = "{$kk}='{$vv}'";
                }

                $attr2 = implode(" ", $new2);

                $title = $students_cache[$v]['student_name'];
                if (false==$title) {
                    $this->error("未找到学生的姓名，请清理一下缓存数据");
                    exit;
                }
                unset($new2, $attr);
                $_checkArr[] = "<label><input type='checkbox' {$attr2} />" . $title . "</label>";
            }
        }
        $_html = implode(" ",$_checkArr);
        if (false==$_html) {
            $this->error("未找到条件的数据");
            exit;
        }
        $this->result($_html);
    }


    public function savetmpstu() {
        //prt($_POST);
        $campus_id = intval($_REQUEST['campus_id']);
        $classroom_id = intval($_REQUEST['classroom_id']);
        $lesson_grader = intval($_REQUEST['lesson_grader']);
        $lesson_series = intval($_REQUEST['lesson_series']);
        $teacher_id = intval($_REQUEST['teacher_id']);
        $temp_students =  $_REQUEST['temp_students']  ;
        $schedule_time =  $_REQUEST['schedule_time']  ;

        $_validate = array(
            array(false==$campus_id,"分校管理必须输入！"),
            array(false==$classroom_id,"班级参数必须输入！"),
            array(false==$lesson_grader,"课程等级必须输入！"),
            array(false==$lesson_series,"课程系列必须输入！"),
            array(false==$teacher_id,"教师id必须输入！"),
            array(false==$temp_students,"学生id参数必须输入！"),
            array(false==$schedule_time,"上课时间必须输入！"),
        );
        foreach ($_validate as $k=>$v) {
            if ($v[0]) $this->error($v[1]);
        }



        $map = [];
        $map['campus_id'] = $campus_id;
        $map['classroom_id'] = $classroom_id;
        $map['teacher_id'] = $teacher_id;
        $map['lesson_series'] = $lesson_series;
        $map['lesson_grader'] = $lesson_grader;
        $classesDB =  DB::table("classes");
        $classesRow= $classesDB->where($map)->find();
        $classes_id = $classesRow['id'];
        $scheduleDB = DB::table("class-schedule");
        $scheduleRow= $scheduleDB->where("classes_id=$classes_id")->find();

        foreach ($temp_students as $student) {
            $query = [];
         //   $query['campus_id'] = $campus_id;
         //   $query['classes_id'] =  $classes_id;
         //   $query['classroom_id'] =   $classroom_id;
            $query['teacher_id'] = $teacher_id;
          //  $query['students_id'] =  $student;
            $query['schedule_time'] =   $schedule_time;
            $res = $scheduleDB->where($query)->find();
            if ($res['schedule_id']>0) {
                $this->error("学生ID是{$student}在时间段".date("Y-m-d H:i:s",$schedule_time)."，已经有课了!");
            }
        }

        foreach ($temp_students as $student) {
            $add = [];
            $add['campus_id'] = $campus_id;
            $add['classes_id'] = $scheduleRow['classes_id'];
            $add['class_title'] = $scheduleRow['class_title'];
            $add['classroom_id'] = $scheduleRow['classroom_id'];
            $add['teacher_id'] = $scheduleRow['teacher_id'];
            $add['students_id'] = $student;
            $add['schedule_time'] = $schedule_time;
            $add['create_time'] = $add['update_time'] = date("Y-m-d H:i:s");
            $add['ib_fixed'] = 0;
            $insert[] = $add;
        }

        if (count($insert)>0) {
            $result =  $scheduleDB->strict(false)->insertAll($insert);

            if ($result > 0) {
                //修改classes temp_students，2，本班是否加了该学生
                $classdata = [];
                $classdata['id'] = $classes_id;
                $old_stu_arr = explode(",", $classesRow['temp_students']);
                $temp_students_arr = array_unique(array_merge($temp_students, $old_stu_arr?$old_stu_arr:[]));
                $classdata['temp_students'] =  implode(",",$temp_students_arr);
                $classdata['temp_students_num'] = count($temp_students_arr);
                DataService::save($classesDB, $classdata);
            }

            $this->success("提交成功");

        }

    }

    public function stulistdelete() {
        $table = "class-schedule";

        if (false==($schedule_id=intval($_REQUEST['schedule_id']))) {
            $this->error("参数错误");
            exit;
        }
        $db = DB::name($table);
        $schedule_time = $db->where("schedule_id=$schedule_id")->value("schedule_time");

        if ($schedule_time - time() <=0 ) {
            $this->error("已经开课了，不能删除");
            exit;
        }


        $scheduleInfo = $db->where("schedule_id=$schedule_id")->find();


        $schedule_time = $db->where("schedule_id=$schedule_id")->value("schedule_time");

        $res = $db->delete($schedule_id);
        savecache($table);
        //修改主表的功能没实现。
        if ($res) {
            $this->success("操作成功");
        } else {
            $this->error("已经开课了，不能删除");
        }

        exit;
    }

    public function stulistedit() {

        if (false==$_POST) {
            if (false==($schedule_id=$_REQUEST['schedule_id'])) {
                $this->error("参数错误");
            }

//            $students_id = DB::table("class-schedule")->where("schedule_id=".$schedule_id)->find();
//            if (false==$students_id) {
//                $this->error("参数错误"); exit;
//            }

            $vo = DB::table("class-schedule")->find($schedule_id);
          
            if (false==$vo) {
                $this->error("参数错误"); exit;
            }

            $this->assign("vo",$vo);
            $this->assign("forward",adminUrl("admin/class2schedule/stulist",['teacher_id'=>$_REQUEST['teacher_id'],"classes_id"=>$_REQUEST['classes_id'],"schedule_timestamps"=>$_REQUEST['schedule_timestamps'],"spm"=>$_REQUEST['spm'],'runtime'=>time()]));
            echo $this->fetch();
            exit;
        }

        if (intval($_POST['schedule_id'])==0) {
            $this->error("参数错误");
            exit;
        }

        $result = DataService::save(DB::table("class-schedule"), [
            'schedule_id'=>intval($_POST['schedule_id'])
            ,'sign_in'=>$_POST['sign_in']
            ,'sign_in_memo'=>$_POST['sign_in_memo']
        ],'schedule_id');
//prt(DB::table("class-schedule")->getLastSql());
        if ($result) {
            $data = [];
            $data['closeLaryer'] = 1;

            $this->success("操作成功",$_POST['forward'],$data);
        }else {
            $this->success("操作失败",$_POST['forward']);
        }

    }
}
