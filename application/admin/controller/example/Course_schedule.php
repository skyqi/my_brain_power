<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/24
 * Time: 8:42
 */

namespace app\admin\controller;

use controller\BasicAdmin;
use service\DataService;
use service\NodeService;
use service\ToolsService;
use think\Db;
use think\Cache;
use org\FieldFromClass;
use org\ContentList;
use org\QueryFrom;
use think\Exception;
use think\Request;
use org\CreateFromControl;


class Course_schedule extends BasicAdmin
{
    public $table, $fields, $map;

    public function __construct ()
    {
        parent::__construct();

//        $this->url = url("/admin/{$this->controller_name}/index", ['spm' => $_REQUEST['spm'], 'runtime' => time()]);  // 放在前面组装
//        $this->table = "course-schedule";
//        $moduleId = $this->getModuleId($this->table);
//        $this->fields = F($moduleId . "_Field");
//        $this->assign("tbField", $this->fields);
//        $this->assign("fields", $this->fields);



    }

    public function index(){

        $db = DB::table($this->table) ;
        $pk = $db->getPk();
        $map= [];
        if (session('user.campus_id')>0) {
            if (in_array("campus_id",$db->getTableFields())){
                $map[] = ['campus_id', '=', session('user.campus_id')];
            }
        }
        $db->where($map);
        $tbList = $this->_list( $db ,true,false);
        $this->assign("pk",$pk);
        $this->assign("pages",$tbList['page']);
        $this->assign("list",$tbList['list']);
        if (session('user.campus_id')>0) {
            $this->assign("campus_id", session('user.campus_id'));
        }
        $tplName =  (app('view')->exists("{$this->controller_name}/index"))?"{$this->controller_name}/index":"content/index";

        $this->assign("ContentList",new ContentList());
        echo $this->fetch($tplName);
        exit;
    }

    //列表显示前的数据处理
    protected function _data_filter (&$list) {
        foreach ($list as $k=>&$res) {
            $section_arr = explode(",",$res['section']);
            $section_name = [];
            foreach ($section_arr as $section) {
               // prt($section);
                $section_name[]= getCacheField("lesson-content",$section,"lesson_name",0,1);
            }
            $res['section'] = implode(",",$section_name);
        }
    }

    public function add ()
    {
        $this->edit();
    }

    public function edit ()
    {

        $res = DB::name("system_module")->where("name", "=", $this->controller_name)->find();
        $moduleid = $res['id'];
        // prt($res);
        $fields = $this->FieldCache($moduleid, 'get');
        // prt($fields);
        $this->assign('fields', $fields);

        $id = intval($_REQUEST ['id']);
        if ($id > 0) {
            //  parent::returnMsg ('error',L('edit_error') );
            $vo = DB::name($this->controller_name)->getById($id);
        }

        $pk = DB::name($this->controller_name)->getPk();
        $this->assign("pk", $pk);

        $this->assign('vo', $vo);

        $lesson_series = Db::name('system_types')->where('parentid=116')->column('id,name');
        $this->assign('lesson_series',$lesson_series);

        $progressList = DB::table("course-progressidxseries")->distinct(true)->where("status=1")->field("lesson_series,progress")->select();

        $this->assign('progressList', $progressList);
        $this->assign('CreateFromControl', new CreateFromControl());
        $this->assign('FieldFromClass', new FieldFromClass());
        $this->assign("ContentList", new ContentList());

        $tplCn = $this->fetch("edit");
        //savelog($tplCn,"editTpl_{$this->controller_name}");
        echo $tplCn;
        exit;
    }

    public function before_update() {

        if (is_array($_POST['section'])) {
            $_POST['section'] = implode(",",$_POST['section']);
        }
        
    }

    public function before_detail(&$vo) {
        $section_arr = explode(",",$vo['section']);
        foreach ($section_arr as $section) {
            // prt($section);
            $section_name[]= getCacheField("lesson-content",$section,"lesson_name",0,1);
        }
        $vo['section'] = implode(",",$section_name);
    }


    public function _query_filter(&$map) {

        foreach ($_REQUEST['query'] as $k=>&$v) {
            if ($k=='province' && $v['title']==-1) unset($v['title']);
            if ($k=='city' && $v['title']==-1) unset($v['title']);

            if (!empty($v['title'])) {
                $title = trim($v['title']);
                if (!isset($v['symbol'])) {
                    $frmMap[]  = $map[]  = array($k,"eq","{$title}");
                }elseif(strtolower($v['symbol'])=='like') {
                    $frmMap[]  = $map[]  = array($k,"like","%{$title}%");
                }else {
                    $frmMap[]  = $map[]  = array($k,"{$v['symbol']}","{$title}");
                }
            }

            if (!empty($v['starttime'])) {
                $starttime = $v['starttime'].' 0:0:0';
                $starttime = strtotime($starttime);
                $frmMap[]  = array($k.'_start_time',"egt","{$starttime}");
                $map[]  = array($k,"egt","{$starttime}");
            }

            if (!empty($v['endtime'])) {
                $endtime = $v['endtime'].' 23:23:59';
                $endtime = strtotime($endtime);
                $frmMap[]  = array($k.'_end_time',"elt","{$endtime}");
                $map[]  = array($k,"elt","{$endtime}");
            }

            if (!empty($v['province']) && $v['province']>0) {

                $frmMap[]  = $map[]  = array('province',"eq","{$v['province']}");
            }

        }

        $frmMap &&  $this->assign("map",json_encode($frmMap));

    }

    public function query(){

        $this->assign ('QueryFrom', new QueryFrom() );

        if ($_REQUEST['param']) {
            foreach (json_decode($_REQUEST['param']) as $k => $v) {
                $map[$v[0]] = array('symbol' => $v[1], 'title' => $v[2]);
            }
//            prt($_REQUEST['param'],0);
//            prt($map,0);
            $map && $this->assign('param', $map);
        }

        $tplName =  (app('view')->exists("{$this->controller_name}/query"))?"{$this->controller_name}/query":"content/query";
        $tplCn = $this->fetch($tplName);

        echo $tplCn;
        exit;
    }

}