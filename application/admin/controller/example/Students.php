<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/24
 * Time: 8:42
 */

namespace app\admin\controller;
use controller\BasicAdmin;
use service\DataService;
use service\NodeService;
use service\ToolsService;
use think\Db;
use think\Cache;
use org\FieldFromClass;
use org\ContentList;
use org\QueryFrom;
use think\Exception;
use think\Request;
use org\CreateFromControl;
class students extends BasicAdmin
{
    public $table,$fields,$map;
    public function __construct()
    {
        parent::__construct();

        $this->url = url("/admin/{$this->controller_name}/index", ['spm' => $_REQUEST['spm'], 'runtime' => time()]);  // 放在前面组装
        //$this->table = $this->controller_name;
       // $moduleId = $this->getModuleId($this->controller_name);
        // $moduleTb = strtolower($this->table);
        // $this->tbFields = F($moduleTb."_Field");
     //   $this->tbFields = F($moduleId."_Field");
       // $this->assign("tbField",$this->tbFields);
//prt($this->tbFields,0);
        $this->assign("url_index",$this->url);

        $this->table = $this->controller_name;
        //   $moduleId = $this->getModuleId($this->controller_name);
        $moduleTb = strtolower($this->table);
        $this->tbFields = F($moduleTb."_Field");
        $this->assign("tbField",$this->tbFields);

        $module_info = Db::name('system_module')->where("name='{$moduleTb}'")->find();
        $this->assign("module_info",$module_info);


        $pk = DB::name($this->table)->getPk();
        $pk && $this->assign("pk", $pk);


    }

    public function index(){

        $db = DB::table($this->table)  ;
        $pk = $db->getPk();
          $db->order("$pk desc");
        $map = [];
        if (false!= ($study_status=intval($_REQUEST['study_status']))) {
            $map[] = ['study_status','=',$study_status];

        }
        if (session('user.campus_id')>0) {
            $map[] = ['campus_id', '=', session('user.campus_id')];
        }
        $db->where($map);
        $tbList = $this->_list( $db ,true,false);
        $this->assign("pk",$pk);
        $this->assign("pages",$tbList['page']);
        $this->assign("list",$tbList['list']);

        $this->assign("ContentList",new ContentList());

        if (intval($_REQUEST['menu'])>0) $this->assign("formMenu",$_REQUEST['menu']);


//        if ($study_status!=5) {
//           // prt($this->fields);
//            foreach ($this->fields as $k=>&$v) {
//                //不是退学退费模块，不显示退学退费
//                if (in_array($v['field'],['leaving_fees','leaving_updatetime'])) {
//                    $v['listShowField'] = '0';
//                }
//            }
//            $this->assign("tbField",$this->fields);
//        }

        return $this->fetch();
        exit;
    }




    public function _query_filter(&$map) {

        foreach ($_REQUEST['query'] as $k=>&$v) {
            if ($k=='province' && $v['title']==-1) unset($v['title']);
            if ($k=='city' && $v['title']==-1) unset($v['title']);

            if (!empty($v['title'])) {
                $title = trim($v['title']);
                if (!isset($v['symbol'])) {
                    $frmMap[]  = $map[]  = array($k,"eq","{$title}");
                }elseif(strtolower($v['symbol'])=='like') {
                    $frmMap[]  = $map[]  = array($k,"like","%{$title}%");
                }else {
                    $frmMap[]  = $map[]  = array($k,"{$v['symbol']}","{$title}");
                }
            }

            if (!empty($v['starttime'])) {
                $starttime = $v['starttime'].' 0:0:0';
                $starttime = strtotime($starttime);
                $frmMap[]  = array($k.'_start_time',"egt","{$starttime}");
                $map[]  = array($k,"egt","{$starttime}");
            }

            if (!empty($v['endtime'])) {
                $endtime = $v['endtime'].' 23:23:59';
                $endtime = strtotime($endtime);
                $frmMap[]  = array($k.'_end_time',"elt","{$endtime}");
                $map[]  = array($k,"elt","{$endtime}");
            }

            if (!empty($v['province']) && $v['province']>0) {

                $frmMap[]  = $map[]  = array('province',"eq","{$v['province']}");
            }

        }
 
        $frmMap &&  $this->assign("map",json_encode($frmMap));

    }


    public function edit()
    {

        $res = DB::name("system_module")->where("name","=",$this->controller_name)->find();
        $moduleid = $res['id'];
        $fields = F($this->controller_name."_Field");

        //prt($fields);
        $this->assign ( 'tbFields', $fields );

        $id = intval($_REQUEST ['id']);
        if ($id>0) {
          //  parent::returnMsg ('error',L('edit_error') );
            $vo = DB::name($this->controller_name)->getById( $id );
        }

        $pk = DB::name($this->controller_name)->getPk();
        $this->assign("pk",$pk);

        //prt($vo);
        false == $vo['content'] && $vo['content'] = htmlspecialchars($vo['content']);


        $this->assign ( 'vo', $vo );
        //   $this->assign ( 'form', $form );
        $this->assign ( 'FieldFromClass', new FieldFromClass() );
        $this->assign("ContentList",new ContentList());
        $tplName =  (app('view')->exists("{$this->controller_name}/edit"))?"{$this->controller_name}/edit":"content/edit";
        $tplCn =$this->fetch($tplName);
        //savelog($tplCn,"editTpl_{$this->controller_name}");
        echo $tplCn;
    }


    public function before_update() {
//        $moduleId = $this->getModuleId($this->controller_name);
//        $fields = F($moduleId."_Field");
//         prt($_POST,0);

        foreach ($_POST as $kk=>&$vv) {
            foreach ($this->fields as $k => $v) {
                if ($v['type'] == 'datetime' && $v['field']==$kk) {   //转换时间格式
                    if ($v['field']=='updatetime') {
                        $_POST['updatetime'] =  time();
                    }else {
                        $vv = toTimestamp($vv);
                    }
                }
                if ($v['type']=='typeid' && $v['field']==$kk ) {
                    is_array($vv) && $vv = implode(",",$vv);
                }
                if ($v['type']=='editor') {  }
                if ($v['type']=='image' && $v['field']==$kk) {
                    $vv = implode(",",$vv);
                }
                if ($v['type']=='regional_Linkage') {
                    $vv==-1  && $vv=0;
                }
            }
        }

        if ($_POST['admission_time'] >= $_POST['expire_time']) {
            $this->error("截止时间不得小等于开课时间");
        }
        $student_name = trim($_POST['student_name']);
        $id = intval($_POST['id']);
        $count = DB::table("students")->where("student_name='{$student_name}' and id!=$id")->count();
        if ($count>0) {
            $this->error("学员名{$student_name}已经被占用");
        }

    }

    public function query(){

        $this->assign ('QueryFrom', new QueryFrom() );

        if ($_REQUEST['param']) {
            foreach (json_decode($_REQUEST['param']) as $k => $v) {
                $map[$v[0]] = array('symbol' => $v[1], 'title' => $v[2]);
            }
//            prt($_REQUEST['param'],0);
//            prt($map,0);
            $map && $this->assign('param', $map);
        }

        $tplName =  (app('view')->exists("{$this->controller_name}/query"))?"{$this->controller_name}/query":"content/query";
        $tplCn = $this->fetch($tplName);
       // savelog($tplCn ,"content_query_frm");
        echo $tplCn;
    }

    /**
     * http://naoli.dev.com:81/admin/campus/export.html
     *
     * 导出excel数据表
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
//    public function export2() {
//        $arr =parse_url($_SERVER['HTTP_REFERER']);
//        parse_str(urldecode($arr['query']),$query);
//        clearstatcache();
//        $xlsCell = array();
//        foreach ($this->fields as $k=>$v) {
//            if ($v['listShowField']==1) {
//                $xlsCell[] = array($v['field'], $v['name']);
//                $tmp_cell[] = $v['field'];
//            }
//        }
//        $db = DB::table($this->controller_name)->field($tmp_cell) ;
//        $this->rows = "9999";
//        $tbList = $this->_list( $db ,true,false);
//        //TODO 数据转换
//
//        unset($k,$v);
//        $map = array();
//        foreach ($query['querycontrol'] as $k=>$v) {
//            if ($v['value']) {
//                if ($v['formula0']=='like') {
//                    $map[$k] = array($v['formula'], "%".$v['value']."%");
//                } else
//                    $map[$k] = array($v['formula'], $v['value']);
//            }
//        }
//
//        $module = Db::name('system_module')->where('name="'.$this->controller_name.'"')->select();
//        $xlsName = isset($module[0]['title']) ? $module[0]['title'] : '';
//        //组织数据格式
//        set_time_limit(0);      //执行时间无限
//        ini_set('memory_limit', '-1');
//        dataExcel($xlsName, $xlsCell, $tbList['list']);
//    }

    //退学退费
    //http://ivehice.mydanweb.com/admin/students/leaving.html
    public function leaving() {

        if (false==$_POST) {
            $id = intval($_REQUEST['id']);
            $this->assign("id",$id);
            $leavingRes = Db::name('students')->where("id=".$id)->field("leaving_fees,leaving_reason")->find();
            $this->assign ('leavingRes',$leavingRes);
            $leaving_feesList =  Db::name('system_types')->where("parentid=154 and status=1")->order("sort asc,id asc")->select();
            $leaving_feesList = array_column($leaving_feesList, 'name', 'id');
            $this->assign ('CreateFromControl', new CreateFromControl());
            $this->assign("leaving_feesList",$leaving_feesList);
            echo   $this->fetch("leaving_frm");
            exit;
        }
        $_validate = array(
            array(false==$_POST['leaving_fees'],"退学费值必须输入！"),
            array(false==intval($_POST['leaving_reason']),"原因必须数值型！"),
            array(false==intval($_POST['id']),"id必须输入！"),
        );

        foreach ($_validate as $k=>$v) {
            if ($v[0]) $this->error($v[1]);
        }

        $data['id'] = intval($_POST['id']);
        $data['leaving_fees'] = $_POST['leaving_fees'];
        $data['leaving_reason'] = $_POST['leaving_reason'];  //个人原因
        $data['study_status'] = 5;  //
        $data['leaving_updatetime'] = time();
        $result = DataService::save($this->table , $data);
        if ($result !== false) {
            $this->success('恭喜, 数据保存成功!', '',Db::getLastInsID());
        }
        $this->error('数据保存失败, 请稍候再试!');

    }


    //停课休学
    //http://ivehice.mydanweb.com/admin/students/stopclass.html
    public function stopclass()
    {
        $id = intval($_REQUEST['id']);
        //停课记录取
        $stopclassRes = DB::table("students-stopclass")->where("students_id=" . $id)->order("id desc")->limit("1")->find();
        $this->assign('stopclassRes', $stopclassRes);
        $studentsRes = DB::table($this->table)->field("id,admission_time,expire_time")->find($id);

        if (false == $_POST) {
            $this->assign("id",$id);
            $this->assign('CreateFromControl', new CreateFromControl());
            echo $this->fetch("stopclass_frm");
            exit;
        }

        $_validate = array(
            array(false==$_POST['stopclass_starttime'],"停课开始时间必须输入！"),
            array(false==$_POST['stopclass_endtime'],"停课截止时间必须输入！"),
            array(false==intval($_POST['id']),"id必须输入！"),
        );

        foreach ($_validate as $k=>$v) {
            if ($v[0]) $this->error($v[1]);
        }

        if (strtotime($_POST['stopclass_starttime']) < ($studentsRes['admission_time']) ) {
            $this->error("停课起始时间不能小于入学时间");
            exit;
        }

        if (strtotime($_POST['stopclass_endtime'])> ($studentsRes['expire_time']) ) {
            $this->error("停课截止时间不能大于到期时间");
            exit;
        }

        if (strtotime($_POST['stopclass_endtime']) < ($_POST['stopclass_starttime']) ) {
            $this->error("停课截止时间不能小于停课开始时间");
            exit;
        }

        $data = array();
        $data['id'] = intval($_POST['id']);
        $data['study_status'] = 4;  //停课
        $expire_time  = $this->sum_stopclass_time($studentsRes['expire_time'],$_POST['stopclass_starttime'],$_POST['stopclass_endtime']);
        $data['expire_time'] =  strtotime($expire_time);
        //prt($data['expire_time'] );
        savelog("停课调整开始......","stopclass");
        savelog($studentsRes,"stopclass");
        $result = DataService::save($this->table, $data);
        if ($result) {
            //停课的附表插入
            $data = array();
            $data['id'] = intval($_POST['id']);
            $data['students_id'] = intval($_POST['id']);
            $data['stopclass_starttime'] = strtotime($_POST['stopclass_starttime']) ;
            $data['stopclass_endtime'] =  strtotime($_POST['stopclass_endtime']) ;
            $data['memo'] = trim($_POST['memo']) ;
            $data['createtime'] =  time() ;
            $data['status'] =1;  //停课，不对应students表的status的状态
            $result2 = DataService::save("students-stopclass", $data);
            if ($result2 !== false) {
                $this->success("数据保存成功!该学生的截止时间已延期至{$expire_time}", '',Db::getLastInsID());
            }
            $this->error('数据保存失败, 请稍候再试!');
        } else {
            $this->error('数据保存失败, 请稍候再试!');
        }

    }

    //计算停课时间
    private function sum_stopclass_time($closedate,$startdate,$enddate){
//        $closedate = "2012-12-14 11:50:00";
//        $startdate="2012-12-09 11:50:00";
//        $enddate="2012-12-12 12:12:12";
        $day =floor((strtotime($enddate)-strtotime($startdate))/86400);
        //prt($day,0);
        //prt($closedate,0);
        $closedate = date("Y-m-d");
        $new_closedate = date('Y-m-d',strtotime("$closedate +{$day} day"));
        return $new_closedate;
    }

    //  http://ivehice2.mydanweb.com/admin.html#/admin/students/lesson_series.html
    public function lesson_series() {

        $this->assign ( 'FieldFromClass', new FieldFromClass() );
        $this->assign("ContentList",new ContentList());
        echo $this->fetch();
    }


}