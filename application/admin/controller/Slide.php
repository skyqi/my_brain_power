<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2017 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://think.ctolog.com
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zoujingli/ThinkAdmin
// +----------------------------------------------------------------------

namespace app\admin\controller;

use controller\BasicAdmin;
use service\ToolsService;
use think\Db;
use service\DataService;
/**
 * 后台入口
 * Class Index
 * @package app\admin\controller
 * @author Anyon <zoujingli@qq.com>
 * @date 2017/02/15 10:41
 */
class Slide extends BasicAdmin
{
    public $attrdata;
    public function __construct()
    {
        parent::__construct();
        $this->attrdata = "system_slideattrdata";
    }

    //http://ivehice2.mydanweb.com/admin/Slide/index.html
    public function index ()
    {
        $db = Db::name('SystemSlide');

        $this->assign("pk",$db->getPk());
        $list =  $db->where(['status' => '1'])->order('slide_id asc')->select();
        return $this->fetch('', ['title' => '轮播图', 'list' => $list]);

    }

    public function add() {
        $this->edit();
    }

    //新增或编辑
    public function edit()
    {

        $action='edit';
        intval($_REQUEST['slide_id'])==0 && $action='add';
        $slide_id = $_REQUEST['slide_id'];
        $table = $_REQUEST['module']=='slideattrdata'?'system_slideattrdata': 'system_slide';

        $db = Db::name($table);
        $pk = $db->getPk();
        $this->assign("pk",$pk);
        $this->assign("slide_id",$slide_id);
        // prt($slide_id);
        if ($action=='edit') {
            $where[$pk] = $slide_id;
        }

        if ($_POST) { unset($where); $where= false; prt($_POST); }

        $pkField = $_REQUEST['module']=='slideattrdata'?'id':'slide_id';
        $form = $_REQUEST['module']=='slideattrdata'?'slideattrdata_form':'form';
        // prt($action);
        echo $this->_form($db, $form,$pkField,$where);
        exit;


    }

    //将post数据过滤后提交
    public function _form_filter(&$data) {
        // prt($data,0);
        // prt($this->appRoot,0);

        if (!empty($data)) {
            if(isset($_POST['pic']) && $_POST['pic']!=false){
                //post保存数据
                !empty($data['pic']) && $data['pic']  =  str_replace($this->appRoot,'',$_POST['pic']);
            }else{
                //编辑加载数据
                !empty($data['pic']) && $data['pic']  =  $this->appRoot.$data['pic'];
            }
        }
    }

    /**
     * 删除菜单
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function delete()
    {
        $table = 'system_slide';
        $module= $this->request->request('module', '','trim');

        if ($_REQUEST['module']=='slideattrdata') $table= "system_slideattrdata";
        $db = Db::table($table);
        $id = $this->request->request('id', 0);
        $db ->delete($id);

        $this->success("删除成功!", '');

    }


    public function slideattrdata() {
        $slideRes = Db::table("system_slide")->find(intval($_REQUEST['slide_id'])?$_REQUEST['slide_id']:0);
        if (false==$slideRes) return false;
        $db = Db::name($this->attrdata);
        $this->assign("pk",$db->getPk());
        $this->assign("title",$slideRes['slide_name']." [{$slideRes['slide_cn_name']}]");
        echo $this->_list($db,false,true);

    }


}