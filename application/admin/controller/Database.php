<?php
/**
 * Created by PhpStorm.
 * User: conson
 * Date: 2018/6/29
 * Time: 16:42
 */

namespace app\admin\controller;

use controller\BasicAdmin;
use service\LogService;
use service\FileService;
use think\Db;
use think\Exception;

/**
 * 后台参数配置控制器
 * Class Config
 * @package app\admin\controller
 * @author Anyon <zoujingli@qq.com>
 * @date 2017/02/15 18:05
 */
class Database extends BasicAdmin
{

    protected $db = '', $datadir = '', $startrow = 0, $startfrom = 0, $complete = true;

    function __construct ()
    {
        parent::__construct();
        $this->datadir =  env('ROOT_PATH'). 'BackUp/';

        if (!is_dir($this->datadir)) mkdir($this->datadir, 0755, true);
    }

    public function index ()
    {
        $dataList = Db::query("SHOW TABLE STATUS LIKE '" . "%'");
        $total = 0;
        foreach ($dataList as $row) {
            $total += $row['Data_length'];
        }
        $this->assign('totalSize', $total);
        $this->assign("dataList", $dataList);

        return $this->fetch();
    }

    public function excuteQuery ($sql = '')
    {
        if (empty($sql)) {
            $this->error(L('do_empty'));
        }
        $queryType = 'INSERT|UPDATE|DELETE|REPLACE|CREATE|DROP|LOAD DATA|SELECT .* INTO|COPY|ALTER|GRANT|TRUNCATE|REVOKE|LOCK|UNLOCK';
        if (preg_match('/^\s*"?(' . $queryType . ')\s+/i', $sql)) {
            $data['result'] = Db::execute($sql);
            $data['type'] = 'execute';
        } else {

            $data['result'] = Db::query($sql);
            $data['type'] = 'query';

        }

        return $data;
    }

    public function query ()
    {
        $this->display();
    }

    public function doquery ()
    {
        $sqls = explode("\n", stripcslashes($_POST['sql']));
        foreach ((array)$sqls as $sql) {
            if ($sql) $r = $this->excuteQuery($sql);
        }
        if ($r['result'] != '') {
            $this->success(L('do_ok'));
        } else {
            if ($r['dberror']) $this->error(L($r['dberror']));
        }
    }

    public function recover ()
    {
        if ($_GET['do'] == 'delete') {
            // prt($this->datadir.$_REQUEST['filename']);
            // foreach ((array)$_POST['files'] as $r) {
            //     unlink($this->datadir.$_REQUEST['files']);
            // }
            unlink($this->datadir . $_REQUEST['filename']);
            $this->success(L('do_ok'), adminUrl('database/recover'));
        } elseif ($_GET['do'] == 'import') {
            header('Content-Type: text/html; charset=UTF-8');
            $filename = $_GET['filename'];
            $filelist = dir_list($this->datadir);
            foreach ((array)$filelist as $r) {
                $file = explode('-', basename($r));

                if ($file[0] == $filename) {
                    $files[] = $r;
                }
            }
            foreach ((array)$files as $file) {
                //读取数据文件
                $sqldata = file_get_contents($file);
                $sqlFormat = sql_split($sqldata, ''); //这个函数找不到，不知道怎么分割字符串的
                $sqlFormat = $sqldata;
                foreach ((array)$sqlFormat as $sql) {
                    $sql = trim($sql);
                    if (strstr($sql, 'CREATE TABLE')) {
                        preg_match('/CREATE TABLE `([^ ]*)`/', $sql, $matches);
                        $ret = $this->excuteQuery($sql);
                        //if($ret){echo   L('CREATE_TABLE_OK').$matches[0].' <br />';}else{echo 'Error sql:'.$sql;}exit;
                    } else {
                        $ret = $this->excuteQuery($sql);
                    }
                }
                echo L('CREATE_TABLE_OK') . basename($file) . '<br>';
            }

        } else {
            $filelist = dir_list($this->datadir);
            foreach ((array)$filelist as $r) {
                $filename = explode('-', basename($r));
                $files[] = array('path' => $r, 'file' => basename($r), 'name' => $filename[0], 'size' => filesize($r), 'time' => filemtime($r));
            }
            $this->assign('dataList', $files);
            $this->assign('datadir', $this->datadir);

            return $this->fetch();
            // $this->display();
        }
    }

    public function docommand ()
    {
        $tables = $_GET['tables'];
        $do = trim($_GET['do']);
        // prt($tables);
        if (empty($do) || empty($tables)) $this->error(L('do_empty'));
        if ($do == 'show') {
            // foreach ((array)$tables as $t) {
            $res = Db::query("SHOW COLUMNS FROM {$tables}");

            // prt($res);

            return $this->fetch('docommand_' . $do, ['result' => $res]);
            // prt($res);
            // }
        } else {
            // $tables = implode(',', $tables);
            // prt($do . ' TABLE ' . $tables);
            $res = Db::query($do . ' TABLE ' . $tables);
            //TODO check table `after_sales` 报错
            // $res = Db::execute($do . ' TABLE ' . $tables);
            // 共用一个
            return $this->fetch('docommand_repair', ['result' => $res]);
        }
    }

    public function backup_old ()
    {
        $tableid = intval($_GET['tableid']);
        $this->startfrom = intval($_GET['startfrom']);
        $sizelimit = intval($_REQUEST['sizelimit']);
        $volume = intval($_GET['volume']) + 1;

        $dataList = Db::query("SHOW TABLE STATUS LIKE '" . "%'");
        // prt($dataList);
        foreach ($dataList as $row) {
            $table_info[$row['Name']] = $row;
        }
        $tables = F('backuptables');
        // prt($tables);

        if (empty($_POST['tables']) && empty($tables)) {
            foreach ($dataList as $row) {
                $tables[] = $row['Name'];
            }
        } else {
            $tables = array();
            if (!$tableid) {
                $tables = $_POST['tables'];
                F('backuptables', $tables);
            } else {
                $tables = F('backuptables');
            }
            if (!is_array($tables) || empty($tables)) {
                $this->success(L('do_empty'));
            }
        }
        // prt($tables);
        unset($dataList);
        $sql = '';
        if (!$tableid) {
            $sql .= "-- Naoli SQL Backup\n-- Time:" . toDate(time()) . "\n-- {$this->request->domain()} \n\n";
            foreach ($tables as $key => $table) {
                $sql .= "--\n-- Maxhom Table `$table`\n-- \n";
                $sql .= "DROP TABLE IF EXISTS `$table`;\n";
                $info = Db::query("SHOW CREATE TABLE  `$table`");
                // prt($info,0);
                $sql .= str_replace(array('USING BTREE', 'ROW_FORMAT=DYNAMIC'), '', $info[0]['Create Table']) . ";\n";
            }
        }
        // die;
        // prt($sql);
        for (; $this->complete && $tableid < count($tables) && strlen($sql) + 500 < $sizelimit * 1000; $tableid++) {
            if ($table_info[$tables[$tableid]]['Rows'] > 0) {
                $sql .= $this->dumptablesql($tables[$tableid], $this->startfrom, strlen($sql), $table_info[$tables[$tableid]]['Auto_increment']);
                if ($this->complete) {
                    $this->startfrom = 0;
                }

            }
        }
        !$this->complete && $tableid--;
        $filename = htmlspecialchars(strip_tags($_GET['filename']));
        $filename = !$filename ? 'Yp_' . rand_string(10) . '_' . date('YmdH') : $filename;
        $filename_valume = sprintf($filename . "-%s" . '.sql', $volume);

        if (trim($sql)) {
            $putfile = $this->datadir . $filename_valume;
            $r = file_put_contents($putfile, trim($sql));
        }


        if ($tableid < count($tables) || $r) {
            // prt($r);

            $this->assign('waitSecond', 0);
            $urlarray = array(
                'tableid'   => $tableid,
                'startfrom' => $this->startfrom,
                'sizelimit' => $sizelimit,
                'volume'    => $volume,
                'filename'  => $filename,
            );
            $message = $filename_valume . ' File Create Ok!';
            $forward = adminUrl("Database/backup", $urlarray);
            $this->assign('jumpUrl', $forward);
            $this->success($message, adminUrl('Database/index'));

        } else {
            F('backuptables', null);
            // prt($r);
            // $this->result($r);
            $this->assign('jumpUrl', url(MODULE_NAME . '/recover'));
            $this->success(L('do_ok'));
        }

    }

    public function dumptablesql ($table, $startfrom = 0, $currsize = 0, $auto_increment = 0)
    {
        $offset = 300;
        $insertsql = '';
        $sizelimit = intval($_REQUEST['sizelimit']);
        if (C('DB_PREFIX') . 'online' == $tbale) return '';

        $modelname = str_replace(C('DB_PREFIX'), '', $table);
        $model = M($modelname);
        $keyfield = $model->getPk();
        $rows = $offset;
        while ($currsize + strlen($insertsql) + 500 < $sizelimit * 1000 && $rows == $offset) {
            if ($auto_increment) {
                $selectsql = "SELECT * FROM $table WHERE $keyfield > $startfrom ORDER BY $keyfield LIMIT $offset";
            } else {
                $selectsql = "SELECT * FROM $table LIMIT $startfrom, $offset";
            }
            $tabledumped = 1;
            $row = Db::query($selectsql);
            $rows = count($row);
            foreach ($row as $key => $val) {
                foreach ($val as $k => $field) {
                    if (is_string($field)) {
                        $val[$k] = '\'' . Db::escapeString($field) . '\'';
                    } elseif (empty($field)) {
                        $val[$k] = 'NULL';
                    }
                }
                if ($currsize + strlen($insertsql) + 500 < $sizelimit * 1000) {
                    if ($auto_increment) {
                        $startfrom = $row[$key][$keyfield];
                    } else {
                        $startfrom++;
                    }
                    $insertsql .= "INSERT INTO `$table` VALUES (" . implode(',', $val) . ");\n";
                } else {
                    $this->complete = false;
                    break 2;
                }
            }

        }
        $this->startfrom = $startfrom;

        return $insertsql;
    }

    /**
     * 下载备份
     */
    public function download ()
    {
        $name = $this->request->request('file', '', 'trim');
        $url = $this->datadir . $name;
        download($url, $name);
    }


    /**
     * 导出数据库备份
     * conson
     * 2018-07-20
     */
    public function backup(){
        header("Content-type:text/html;charset=utf-8");
        // $path = RUNTIME_PATH.'mysql/';
        $path =  env('ROOT_PATH'). 'BackUp/';

        $database = config('database.database');
        // prt($database);
        //echo "运行中，请耐心等待...<br/>";
        $info = "-- ----------------------------\r\n";
        $info .= "-- 日期：".date("Y-m-d H:i:s",time())."\r\n";
        $info .= "-- MySQL - 5.5.52-MariaDB : Database - ".$database."\r\n";
        $info .= "-- ----------------------------\r\n\r\n";
        $info .= "CREATE DATAbase IF NOT EXISTS `".$database."` DEFAULT CHARACTER SET utf8 ;\r\n\r\n";
        $info .= "USE `".$database."`;\r\n\r\n";

        // 检查目录是否存在
        if(is_dir($path)){
            // 检查目录是否可写
            if(is_writable($path)){
                //echo '目录可写';exit;
            }else{
                //echo '目录不可写';exit;
                chmod($path,0777);
            }
        }else{
            //echo '目录不存在';exit;
            // 新建目录
            mkdir($path, 0777, true);
            //chmod($path,0777);
        }

        // 检查文件是否存在
        $filename = htmlspecialchars(strip_tags($_GET['filename']));
        $filename = !$filename ? 'Yp_' . rand_string(10) . '_' . date('YmdH') : $filename;
        $volume = intval($_GET['volume']) + 1;
        $filename_valume = sprintf($filename . "-%s" . '.sql', $volume);
        $file_name = $path . $filename_valume;

        // $file_name = $path.$database.'-'.date("Y-m-d",time()).'.sql';
        if(file_exists($file_name)){
            echo "数据备份文件已存在！";
            exit;
        }
        file_put_contents($file_name,$info,FILE_APPEND);

        //查询数据库的所有表
        $result = Db::query('show tables');
        //print_r($result);exit;
        foreach ($result as $k=>$v) {
            //查询表结构
            $val = $v['Tables_in_'.$database];
            $sql_table = "show create table `".$val."`";
            $res = Db::query($sql_table);
            //print_r($res);exit;
            $info_table = "-- ----------------------------\r\n";
            $info_table .= "-- Table structure for `".$val."`\r\n";
            $info_table .= "-- ----------------------------\r\n\r\n";
            $info_table .= "DROP TABLE IF EXISTS `".$val."`;\r\n\r\n";
            $info_table .= $res[0]['Create Table'].";\r\n\r\n";
            //查询表数据
            $info_table .= "-- ----------------------------\r\n";
            $info_table .= "-- Data for the table `".$val."`\r\n";
            $info_table .= "-- ----------------------------\r\n\r\n";
            file_put_contents($file_name,$info_table,FILE_APPEND);
            $sql_data = "select * from `".$val."`";
            $data = Db::query($sql_data);
            //print_r($data);exit;
            $count= count($data);
            //print_r($count);exit;
            if($count<1) continue;
            foreach ($data as $key => $value){
                $sqlStr = "INSERT INTO `".$val."` VALUES (";
                foreach($value as $v_d){
                    $v_d = str_replace("'","\'",$v_d);
                    $sqlStr .= "'".$v_d."', ";
                }
                //需要特别注意对数据的单引号进行转义处理
                //去掉最后一个逗号和空格
                $sqlStr = substr($sqlStr,0,strlen($sqlStr)-2);
                $sqlStr .= ");\r\n";
                file_put_contents($file_name,$sqlStr,FILE_APPEND);
            }
            $info = "\r\n";
            file_put_contents($file_name,$info,FILE_APPEND);
        }
        $message = $filename_valume . ' 备份成功!';
        // download($file_name,'123.sql');
        $this->success($message, adminUrl('Database/index'));
    }

    //导出table表
    public function exportTable(){
        header("Content-type:text/html;charset=utf-8");
        $table_name = trim($_REQUEST['table_name']);
        $module_table = 'system_module';
        $field_table = 'system_field';
        $path = env('runtime_path') . 'Table/';
        $database = config('database.database');
        //echo "运行中，请耐心等待...<br/>";
        $info = "-- ----------------------------\r\n";
        $info .= "-- 日期：".date("Y-m-d H:i:s",time())."\r\n";
        $info .= "-- MySQL - 5.5.52-MariaDB : Database - ".$database."\r\n";
        $info .= "-- ----------------------------\r\n\r\n";
        // $info .= "CREATE DATAbase IF NOT EXISTS `".$database."` DEFAULT CHARACTER SET utf8 ;\r\n\r\n";
        // $info .= "USE `".$database."`;\r\n\r\n";

        // 检查目录是否存在
        if(is_dir($path)){
            // 检查目录是否可写
            if(is_writable($path)){
                //echo '目录可写';exit;
            }else{
                //echo '目录不可写';exit;
                chmod($path,0777);
            }
        }else{
            //echo '目录不存在';exit;
            // 新建目录
            mkdir($path, 0777, true);
            //chmod($path,0777);
        }

        // 检查文件是否存在
        $filename = htmlspecialchars(strip_tags($_GET['filename']));
        $filename = !$filename ? 'Yp_' . rand_string(10) . '_' . date('YmdH') : $filename;
        $volume = intval($_GET['volume']) + 1;
        $filename_valume = sprintf($filename . "-%s" . '.sql', $volume);
        $file_name = $path . $filename_valume;

        // $file_name = $path.$database.'-'.date("Y-m-d",time()).'.sql';
        if(file_exists($file_name)){
            echo "数据备份文件已存在！";
            exit;
        }
        file_put_contents($file_name,$info,FILE_APPEND);

        //查询数据库的所有表
        // $result = Db::query('show tables');
        $result = [
            ['Tables_in_'.$database=>$table_name]
        ];
        foreach ($result as $k=>$v) {
            //查询表结构
            $val = $v['Tables_in_'.$database];
            $sql_table = "show create table `".$val."`";
            $res = Db::query($sql_table);
            $info_table = "-- ----------------------------\r\n";
            $info_table .= "-- Table structure for `".$val."`\r\n";
            $info_table .= "-- ----------------------------\r\n\r\n";
            $info_table .= "DROP TABLE IF EXISTS `".$val."`;\r\n\r\n";
            $info_table .= $res[0]['Create Table'].";\r\n\r\n";
            //查询表数据
            $info_table .= "-- ----------------------------\r\n";
            $info_table .= "-- Data for the table `".$val."`\r\n";
            $info_table .= "-- ----------------------------\r\n\r\n";
            file_put_contents($file_name,$info_table,FILE_APPEND);

            $sql_data = "select * from `".$val."`";
            $data = Db::query($sql_data);
            $count= count($data);
            if($count<1) continue;

            $table_fileds_str = implode(',', (array)Db::name($val)->getTableFields());

            foreach ($data as $key => $value){
                $sqlStr = "INSERT INTO `".$val."` (".$table_fileds_str.") VALUES (";
                foreach($value as $v_d){
                    $v_d = str_replace("'","\'",$v_d);
                    $sqlStr .= "'".$v_d."', ";
                }
                //需要特别注意对数据的单引号进行转义处理
                //去掉最后一个逗号和空格
                $sqlStr = substr($sqlStr,0,strlen($sqlStr)-2);
                $sqlStr .= ");\r\n";
                file_put_contents($file_name,$sqlStr,FILE_APPEND);
            }
            $info = "\r\n";
            file_put_contents($file_name,$info,FILE_APPEND);


            $info_table_module_start = '';
            $info_table_module_start .= "-- ----------------------------\r\n";
            $info_table_module_start .= "-- Data for the table `".$module_table."`\r\n";
            $info_table_module_start .= "-- ----------------------------\r\n\r\n";
            // $info_table_module_start .= "\r\n";
            file_put_contents($file_name,$info_table_module_start,FILE_APPEND);

            $sql_data_module = "select * from `".$module_table."` where name='$table_name'";

            $module_data = Db::query($sql_data_module);

            // $module_table_fileds_str = implode(',', (array)Db::name($module_table)->getTableFields());
            $module_table_fileds_str = implode(',', array_merge(array_diff((array)Db::name($module_table)->getTableFields(), array('id'))));


            // 处理`system_module` start =========================================================================
            if(count($module_data)>0){
                foreach ($module_data as $key => $value){
                    $sqlStr = "INSERT INTO `".$module_table."` (".$module_table_fileds_str.") VALUES (";
                        foreach($value as $module_k => $v_d){
                            if($module_k=='id'){  //去掉module_id
                                continue;
                            }
                            $v_d = str_replace("'","\'",$v_d);
                            $sqlStr .= "'".$v_d."', ";
                        }
                        //需要特别注意对数据的单引号进行转义处理
                        //去掉最后一个逗号和空格
                        $sqlStr = substr($sqlStr,0,strlen($sqlStr)-2);
                        $sqlStr .= ");\r\n";
                        file_put_contents($file_name,$sqlStr,FILE_APPEND);

                }
            }


            $info_table_module = '';
            $info_table_module .= "-- ----------------------------\r\n";
            $info_table_module .= "-- Data for the table `".$module_table."`\r\n";
            $info_table_module .= "-- ----------------------------\r\n\r\n";
            $info_table_module .= "\r\n";
            file_put_contents($file_name,$info_table_module,FILE_APPEND);

        }
        // 处理`system_module` end =========================================================================



        // 处理`system_field` start =========================================================================
        $module_id = Db::name($module_table)->where(['name'=>$table_name])->value('id');
        // prt($module_id);

        $info_table_field_start = '';
        $info_table_field_start .= "-- ----------------------------\r\n";
        $info_table_field_start .= "-- Data for the table `".$field_table."`\r\n";
        $info_table_field_start .= "-- ----------------------------\r\n\r\n";
        // $info_table_module_start .= "\r\n";
        file_put_contents($file_name,$info_table_field_start,FILE_APPEND);

        $sql_data_field = "select * from `".$field_table."` where moduleid='$module_id'";
        $field_data = Db::query($sql_data_field);

        $field_table_fileds_arr = (array)Db::name($field_table)->getTableFields();

        $pk = Db::name($field_table)->getPk();

        foreach( $field_table_fileds_arr as $k=>$v) {
            if($pk == $k) unset($field_table_fileds_arr[$k]);
        }
        $field_table_fileds_str = implode(',', $field_table_fileds_arr);
        $field_table_fileds_str = preg_replace('/moduleid/', 'mo_uuid', $field_table_fileds_str);

        if(count($field_data)>0){
            unset($sqlStr);
            foreach ($field_data as $key => $value){
                $sqlStr = "INSERT INTO `".$field_table."` (".$field_table_fileds_str.") VALUES (";
                foreach($value as $k => $v_d){
                    if($k!=$pk){
                        $v_d = str_replace("'","\'",$v_d);
                        $sqlStr .= "'".$v_d."', ";
                    }

                }
                //需要特别注意对数据的单引号进行转义处理
                //去掉最后一个逗号和空格
                $sqlStr = substr($sqlStr,0,strlen($sqlStr)-2);
                $sqlStr .= ");\r\n";
                file_put_contents($file_name,$sqlStr,FILE_APPEND);
            }
        }

        $info_field_module = '';
        $info_field_module .= "-- ----------------------------\r\n";
        $info_field_module .= "-- Data for the table `".$field_table."`\r\n";
        $info_field_module .= "-- ----------------------------\r\n\r\n";
        $info_field_module .= "\r\n";
        file_put_contents($file_name,$info_field_module,FILE_APPEND);

        // 处理`system_field` end =========================================================================

        $message = $filename_valume . ' 备份成功!';
        download($file_name,$table_name.'-'.date("Y-m-d",time()).'.sql');
        $this->success($message, adminUrl('Database/index'));
    }


}
