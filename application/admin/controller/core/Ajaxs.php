<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/22
 * Time: 17:50
 */
namespace app\admin\controller\core;
use controller\BasicAdmin;
use service\DataService;
use service\NodeService;
use service\ToolsService;
use think\Db;
use think\Cache;
use org\FieldFromClass;
use org\ContentList;
use org\QueryFrom;
use think\Exception;
use think\Request;
use org\CreateFromControl;

// Ajax回调处理
class Ajaxs extends BasicAdmin
{
    public $table, $fields, $map;

    public function __construct()
    {
        parent::__construct();
    }

    public function getAttachment() {
        $aid = $this->request->request('aid',0,'int') ;
        $type = $this->request->request('type','','trim') ;
        $file_numb = $this->request->request('file_numb',1,'int') ;
        $data = getAttachment($aid);

        if (false!= $data && $file_numb==1) {
            $new = $data[0];
            if (false!=$type) {
                echo  $data[0]['imagepath'][$type];
                exit;
            }
        }
    }


}