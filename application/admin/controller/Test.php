<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/24
 * Time: 8:42
 */

namespace app\admin\controller;

use controller\BasicAdmin;
use service\DataService;
use service\ToolsService;
use think\Db;
use org\Baidu_transapi;
use think\Request;

class Test
{
    // http://naoli.mydanweb.com/admin/test/index.html
    public function index()
     {

        if (class_exists('app\\admin\\behavior\\Test')) {
            \think\facade\Hook::exec('app\\admin\\behavior\\Test', 'aaaa');
        }

     }

    // http://door.mydanweb.com/admin/test/trans.html
      public function trans()
    {

        $LangsPack = config('config.Trans_LangsPack');
        if (false==$LangsPack) return false;
        $trans = new Baidu_transapi();
        $from_lang = "zh";
        $to_langs =  array("en","jp");

        Db::execute("TRUNCATE TABLE  `langtrans`");
        foreach ($to_langs as $to_lang) {
            foreach ($LangsPack as $k => $v) {
                $trans_arr = $trans->translate($v, $from_lang, $to_lang);
                $trans_dst = trim($trans_arr['trans_result'][0]['dst']);
                $res = DB::name("langtrans")->insert(array('trans_src' => $v, 'to' => $to_lang, 'trans_dst' => $trans_dst));
            }
        }


        foreach ($to_langs as $to_lang) {
            $filename = "langtrans";
            $langRes[$to_lang] = DB::name("langtrans")->where(['to' => $to_lang])->column("trans_dst", 'trans_src');
        }
        prt($langRes);
        F($filename,$langRes);
    }


}