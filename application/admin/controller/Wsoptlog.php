<?php



namespace app\admin\controller;
use controller\BasicAdmin;
use service\LogService;
use service\NodeService;
use think\Db;
use think\captcha\Captcha;
use org\FieldFromClass;
use org\ContentList;
use org\QueryFrom;

/**
 * 系统登录控制器
 * class Login
 * @package app\admin\controller
 * @author Anyon <zoujingli@qq.com>
 * @date 2017/02/10 13:59
 */
class Wsoptlog extends BasicAdmin
{

    public $table,$fields,$map;
    public function __construct()
    {
        parent::__construct();

        $this->url = url("/admin/{$this->controller_name}/index", ['spm' => $_REQUEST['spm'], 'runtime' => time()]);  // 放在前面组装
        $this->table = $this->controller_name;
        //   $moduleId = $this->getModuleId($this->controller_name);
        $moduleTb = strtolower($this->table);
        $this->tbFields = F($moduleTb."_Field");
        $this->assign("tbField",$this->tbFields);

        $this->assign("url_index",$this->url);


    }

    public function index(){

           $tplName = "wsoptlog/index";
           echo $this->fetch($tplName);
           exit;
    }


    public function ajaxLogs() {
        $db = DB::table("wsoptlog") ;
        $res = $db->order("id desc")->select();
        
        $_logs = "";
        foreach ($res as $k=>$v) {
          
             $_logs .= ($k+1).":::: [".date("Y-m-d H:i:s",$v['updatetime'])."] id=".$v['id']." ;; client_id=".$v['client_id']." ;; cmd=".$v['cmd'].":: type=".$v['type']." ;; 传十六进度值=".($v['ibHex']?'是':'否')."\r\n\r\n";

        }
        exit ($_logs);
    }

    public function test(){


        prt(session_id(),0);
          $session_id = "93pkdc0c9l0cui9enbqud3tja2";
        session_id($session_id);
//         cookie(session_name(),$session_id) ;
//         session_start();
        session_destroy();

        prt(session("user"));
die;
         prt(cookie("sess_name_naoli"));
//        $session_id = 'naoli';
//        session_id($session_id);
//        session_start();
        prt(session_id(),0);

        prt(session("user"));
    }
}