<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/24
 * Time: 8:42
 */

namespace app\admin\controller;
use controller\BasicAdmin;
use service\DataService;
use service\NodeService;
use service\ToolsService;
use think\Db;
use think\Cache;
use org\FieldFromClass;
use org\ContentList;
use org\QueryFrom;
use think\Exception;
use think\Request;
class Content extends BasicAdmin
{

    public function __construct()
    {
        parent::__construct( );
        parent::setRequest(\request());
    }

    public function index(){

        $db = DB::table($this->table) ;
        $pk = $db->getPk();

        $module_info = Db::name('system_module')->where("tb_name='$this->table'")->find();

        $this->assign("module_info",$module_info);

        $tbList = $this->_list( $db ,true,false);
        $this->assign("pk",$pk);
        $this->assign("pages",$tbList['page']);
        $this->assign("list",$tbList['list']);

        $tplName =  (app('view')->exists("{$this->controller_name}/index"))?"{$this->controller_name}/index":"content/index";
        $this->assign("ContentList",new ContentList());
        echo $this->fetch($tplName);
        return false;
    }

    public function _query_filter(&$map) {

        foreach ($_REQUEST['query'] as $k=>&$v) {
            if ($k=='province' && $v['title']==-1) unset($v['title']);
            if ($k=='city' && $v['title']==-1) unset($v['title']);

            if (!empty($v['title'])) {
                $title = trim($v['title']);
                if (!isset($v['symbol'])) {
                    $frmMap[]  = $map[]  = array($k,"eq","{$title}");
                }elseif(strtolower($v['symbol'])=='like') {
                    $frmMap[]  = $map[]  = array($k,"like","%{$title}%");
                }else {
                    $frmMap[]  = $map[]  = array($k,"{$v['symbol']}","{$title}");
                }
            }

            if (!empty($v['starttime'])) {
                $starttime = $v['starttime'].' 0:0:0';
                $starttime = strtotime($starttime);
                $frmMap[]  = array($k.'_start_time',"egt","{$starttime}");
                $map[]  = array($k,"egt","{$starttime}");
            }

            if (!empty($v['endtime'])) {
                $endtime = $v['endtime'].' 23:23:59';
                $endtime = strtotime($endtime);
                $frmMap[]  = array($k.'_end_time',"elt","{$endtime}");
                $map[]  = array($k,"elt","{$endtime}");
            }

            if (!empty($v['province']) && $v['province']>0) {

                $frmMap[]  = $map[]  = array('province',"eq","{$v['province']}");
            }

        }
 
        $frmMap &&  $this->assign("map",json_encode($frmMap));

    }


    public function edit()
    {

        $id = intval($_REQUEST ['id']);
        if ($id>0) {
          //  parent::returnMsg ('error',L('edit_error') );
            $vo = DB::name($this->table)->getById( $id );

        }

        $pk = DB::name($this->table)->getPk();
        $this->assign("pk",$pk);

        false == $vo['content'] && $vo['content'] = htmlspecialchars($vo['content']);
        false!=$vo && $this->assign ( 'vo', $vo );
        $this->assign ( 'FieldFromClass', new FieldFromClass() );
        $tplName =  (app('view')->exists("{$this->controller_name}/edit"))?"{$this->controller_name}/edit":"content/edit";
        echo $this->fetch($tplName);

    }

    //存盘之前转换
    public function before_update() {

        foreach ($_POST as $kk=>&$vv) {
            foreach ($this->tbFields as $k => $v) {
                if ($v['type'] == 'datetime' && $v['field']==$kk) {   //转换时间格式
                    if ($v['field']=='updatetime') {
                        $_POST['updatetime'] =  time();
                    }else {
                        $vv = toTimestamp($vv);
                        // $vv = strtotime($vv);
                    }

                    if ($v['field']=='createtime') {
                        $_POST['createtime'] =  time();
                    }else {
                        $vv = toTimestamp($vv);
                    }
                }

                if ($v['type']=='typeid' && $v['field']==$kk ) {
                    is_array($vv) && $vv = implode(",",$vv);
                }
                if ($v['type']=='editor') {  }
                if (in_array($v['type'],['image','images']) && $v['field']==$kk) {
                    $vv = implode(",",$vv);
                }
                if ($v['type']=='regional_Linkage') {
                    $vv==-1  && $vv=0;
                }
            }
        }
        $fields_keys = array_keys($this->tbFields);

        if (in_array('regional_Linkage',$fields_keys) && isset($_POST['area']) && isset($_POST['city']) ) {
            $aree=[];
            $_POST['area']>0 & $aree[] = "p:"."{$_POST['area']}:".Db::name("system_areabase")->where("area_bn=".$_POST['area'])->value("name");
            $_POST['city']>0 & $aree[]  = "c:"."{$_POST['city']}:".Db::name("system_areabase")->where("area_bn=".$_POST['city'])->value("name");
            $_POST['province']>0 & $aree[] = "d:"."{$_POST['province']}:".Db::name("system_areabase")->where("area_bn=".$_POST['province'])->value("name");

            unset($_POST['area'],$_POST['city'],$_POST['province']);
            $_POST['regional_Linkage'] = implode(",",$aree);
        }

    }

    public function query(){

        $this->assign ('QueryFrom', new QueryFrom() );

        if ($_REQUEST['param']) {
            foreach (json_decode($_REQUEST['param']) as $k => $v) {
                $map[$v[0]] = array('symbol' => $v[1], 'title' => $v[2]);
            }
//            prt($_REQUEST['param'],0);
//            prt($map,0);
            $map && $this->assign('param', $map);
        }

        $tplName =  (app('view')->exists("{$this->controller_name}/query"))?"{$this->controller_name}/query":"content/query";
        $tplCn = $this->fetch($tplName);
        savelog($tplCn ,"content_query_frm");
        echo $tplCn;
    }


    /**
     * http://naoli.dev.com:81/admin/campus/export.html
     *
     * 导出excel数据表
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function export() {
        $arr =parse_url($_SERVER['HTTP_REFERER']);
        parse_str(urldecode($arr['query']),$query);
        clearstatcache();
        $xlsCell = array();
        foreach ($this->fields as $k=>$v) {
            if ($v['listShowField']==1) {
                $xlsCell[] = array($v['field'], $v['name']);
                $tmp_cell[] = $v['field'];
            }
        }
        $db = DB::table($this->controller_name)->field($tmp_cell) ;
        $tbList = $this->_list( $db ,true,false);
        //TODO 数据转换

        unset($k,$v);
        $map = array();
        foreach ($query['querycontrol'] as $k=>$v) {
            if ($v['value']) {
                if ($v['formula0']=='like') {
                    $map[$k] = array($v['formula'], "%".$v['value']."%");
                } else
                    $map[$k] = array($v['formula'], $v['value']);
            }
        }

        $module = Db::name('system_module')->where('name="'.$this->controller_name.'"')->select();
        $xlsName = isset($module[0]['title']) ? $module[0]['title'] : '';
        //组织数据格式
        set_time_limit(0);      //执行时间无限
        ini_set('memory_limit', '-1');
        dataExcel($xlsName, $xlsCell, $tbList['list']);
    }
}