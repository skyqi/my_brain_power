<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/24
 * Time: 8:42
 */

namespace app\admin\controller;

use controller\BasicAdmin;
use service\DataService;
use service\NodeService;
use service\ToolsService;
use think\Db;
use think\Cache;

class Module extends BasicAdmin
{
    public $table = "system_module";

    public function __construct ()
    {
        parent::__construct();
        parent::setRequest(\request());
    }

    public function index ()
    {

        if ((session('module_auth') && session('module_auth') == config("config.module_passwd")) || false == config("config.module_passwd")) {
            $this->title = '模块管理';

            $db = Db::name($this->table)->order('sort asc,id asc')->where("status=1");
            return parent::_list($db, false);
        } else {
            if (false != $_POST['password']) {
                $module_passwd = config("config.module_passwd");
                if ($_POST['password'] != $module_passwd) {
                    $this->error("密码不正确");
                } else {
                    session('module_auth', $module_passwd);
                    $this->success("密码正确");
                    // $this->redirect("/admin#/admin/module/index.html?spm=".$_REQUEST['spm']);
                }
                exit;
            }
            echo $this->fetch("verfiy_win");
            exit;
        }

    }

    function insert ()
    {
        // prt($_REQUEST);
        // prt($_REQUEST);
        $instData = $_POST;
        $tablename = $instData['tb_name'] = trim(strtolower($instData['tb_name']));

        $url = $this->url;
        if (false == ($module_name=$instData['name'])) {
            $this->returnMsg('error', '', '模型名称不能为空');
        }

        if(strpos($instData['name'],'_')>0){
            $this->returnMsg('error', '', '模型名称不能有下划线');
        }

        if(strpos($instData['name'],'-')>0){
            $this->returnMsg('error', '', '模型名称不能有中划线');
        }


        if (false == $instData['tb_name']) {
            $this->returnMsg('error', '', '模型表名不能为空');
        }

        $count = Db::name('system_module')->where("tb_name='$tablename'")->count();
        if ($count > 0) {
            $this->returnMsg('error', '', "已经存在{$tablename}模型名称");
        }
        $count = Db::name('system_module')->where("name='$module_name'")->count();
        if ($count > 0) {
            $this->returnMsg('error', '', "已经存在{$tablename}模型名称");
        }

        if (in_array($tablename,getDBTables())) {
            $this->returnMsg('error', '', '数据库的表名已经存在');
        }

        (false == $instData['description']) && $instData['description'] = $instData['title'];
        $instData['createtime'] = time();
        //表操作
        if ($instData['table_operation'] && !empty($instData['table_operation'])) {
            $instData['iscache'] = (isset($instData['table_operation']['iscache']) && $instData['table_operation']['iscache'] == 1) ? 1 : 0;
            $instData['isdelete'] = isset($instData['table_operation']['isdelete']) && $instData['table_operation']['isdelete'] == 1 ? 1 : 0;
            $instData['issort'] = isset($instData['table_operation']['issort']) && $instData['table_operation']['issort'] == 1 ? 1 : 0;
            $instData['isexport'] = isset($instData['table_operation']['isexport']) && $instData['table_operation']['isexport'] == 1 ? 1 : 0;
        }
        $mo_uuid = $instData['mo_uuid'] = uniqid();
        $instData['status']  = 1 ;

        try {
            Db::table($this->table)->strict(false)->insert($instData);
        } catch (\Exception $e) {

            $this->returnMsg('error', $url, $e->getMessage());
        }
        $moduleid = db::getLastInsID();
        if ($moduleid == 0) {
            $this->returnMsg('error', $url, 'moduleid不能为0');
        }

        Db::execute("CREATE TABLE `" . $tablename . "` (
                  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,		    
                  `sort` int(10) unsigned NOT NULL DEFAULT '90',
                  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
                  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
                   `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
                  PRIMARY KEY (`id`)
                ) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8");


        $newField = array();
        $newField['mo_uuid'] = $mo_uuid;

        $newField['tb_name'] = $instData['tb_name'];
        $newField['field'] = "id";
        $newField['type'] = "number";
        $newField['name'] = "ID";
        $newField['sort'] = "0";
        $newField['status'] = "1";
        $newField['issystem'] = 1;

        DB::name('system_field')->insert($newField);

        $newField = array();
        $newField['mo_uuid'] = $mo_uuid;

        $newField['tb_name'] = $instData['tb_name'];
        $newField['field'] = "sort";
        $newField['type'] = "number";
        $newField['name'] = L('sort');
        $newField['sort'] = "100";
        $newField['status'] = "1";
        $newField['issystem'] = 1;
        DB::name('system_field')->insert($newField);
        //createtime
        $newField = array();
        $newField['mo_uuid'] = $mo_uuid;

        $newField['tb_name'] = $instData['tb_name'];
        $newField['field'] = "createtime";
        $newField['type'] = "datetime";
        $newField['name'] = L('createtime');
        $newField['sort'] = "100";
        $newField['status'] = "1";
        $newField['issystem'] = 1;
        DB::name('system_field')->insert($newField);

        //updatetime
        $newField = array();

        $newField['tb_name'] = $instData['tb_name'];
        $newField['mo_uuid'] = $mo_uuid;
        $newField['field'] = "updatetime";
        $newField['type'] = "datetime";
        $newField['name'] = L('updatetime');
        $newField['sort'] = "100";
        $newField['status'] = "1";
        $newField['issystem'] = 1;
        DB::name('system_field')->insert($newField);

        //status
        $nb = chr(10);
        $newField = array();

        $newField['tb_name'] = $instData['tb_name'];
        $newField['mo_uuid'] = $mo_uuid;
        $newField['field'] = "status";
        $newField['type'] = "radio";
        $newField['name'] = L('status');
        $newField['sort'] = "100";
        $newField['status'] = 1;
        $newField['setup'] = "array ('options' => '有效|1','fieldtype' => 'varchar','numbertype' => '1','labelwidth' => '','default' => '1')";
        $newField['issystem'] = 1;
        DB::name('system_field')->insert($newField);

        DB::name("system_field")->column("*","id");
        $this->returnMsg('success', $url);

    }

    function delete ()
    {
        if (false == ($ids = $_REQUEST['id'])) {
            $this->error("id叁数错误");
        }
        $ids2 = explode(",", $ids);
        foreach ($ids2 as $id) {
            $r = Db::name($this->table)->find($id);
            if (empty($r)) $this->error("在表{$this->table}未找到id记录");
            $m = Db::table($this->table)->delete($id);
            if ($m) {
                $tablename = $r['tb_name'];
                Db::execute("DROP TABLE IF EXISTS `" . $tablename . "`");
                Db::table('system_field')->where("mo_uuid='{$r['mo_uuid']}'")->delete();
            }
        }
        $this->returnMsg('success', $this->url);
    }


    //启用模块
    public function resume ()
    {
        // $controller_name = $this->controller_name;
        // if (false==$controller_name) {
        //     $this->error("controller_name取参错误,Line:".__LINE__);
        // }

        $model = DB::name($this->table);
        $pk = $model->getPk();
        $id = intval($_POST['id']);

        if (false == $id) {
            $this->error("id取参错误,Line:" . __LINE__);
        }

        try {
            $res = $model->strict(false)->where([$pk => $id])->update([$_POST['field'] => $_POST['value']]);
        } catch (\Exception $e) {
            $this->error("更新失败,请重试!error:" . $e->getMessage());
        }
        //savecache($this->table);
        DB::name("system_field")->column("*","id");
        $this->success(L('do_ok'));
    }

    //禁用模块
    public function forbid ()
    {
        // $controller_name = $this->controller_name;
        // if (false==$controller_name) {
        //     $this->error("controller_name取参错误,Line:".__LINE__);
        // }
        $model = DB::name($this->table);
        $pk = $model->getPk();
        $id = intval($_POST['id']);

        if (false == $id) {
            $this->error("id取参错误,Line:" . __LINE__);
        }

        try {
            $res = $model->strict(false)->where([$pk => $id])->update([$_POST['field'] => $_POST['value']]);
        } catch (\Exception $e) {
            $this->error("更新失败,请重试!error:" . $e->getMessage());
        }
        //savecache($this->table);
        DB::name("system_field")->column("*","id");
        $this->success(L('do_ok'));
    }


}