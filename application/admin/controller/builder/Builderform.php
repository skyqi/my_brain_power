<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2017 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://think.ctolog.com
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zoujingli/ThinkAdmin
// +----------------------------------------------------------------------

namespace app\admin\controller\builder;

use controller\BasicAdmin;
use service\DataService;
use service\NodeService;
use service\ToolsService;
use think\Db;

/**
 * 系统功能节点管理
 * Class Node
 * @package app\admin\controller
 * @author Anyon <zoujingli@qq.com>
 * @date 2017/02/15 18:13
 */
class Builderform extends BasicAdmin
{

    // http://ivehice.mydanweb.com/admin/builder.builderform/index.html
    public function index() {
        $_frm = "<form class='form'>
                        <div class='controls'>
                          <label class='control-label'>Label Text</label> <input class='input-large' type='text' name='label' id='label'>
                          <label class='control-label'>Placeholder</label> <input type='text' name='placeholder' id='placeholder'>
                          <label class='control-label'>name</label> <input type='text' name='name' id='name'>
                          <label class='control-label'>Help Text</label> <input type='text' name='help' id='help'>
                          <hr/>
                          <button class='btn btn-info'>Save</button><button class='btn btn-danger'>Cancel</button>
                        </div>
                      </form>";
        $this->assign("frm",$_frm);
        return $this->fetch();
    }



}