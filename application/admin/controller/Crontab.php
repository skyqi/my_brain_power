<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/24
 * Time: 8:42
 */

namespace app\admin\controller;
use controller\BasicAdmin;
use service\DataService;
use service\NodeService;
use service\ToolsService;
use think\Db;
use think\Cache;
use org\FieldFromClass;
use org\ContentList;
use org\QueryFrom;
use think\Exception;
use think\Request;
class Crontab extends BasicAdmin
{
    public $table,$fields,$map;
    public function __construct()
    {
        $this->table = 'system_crontab';
        parent::__construct();
        parent::setRequest(\request());
        $this->url = url("/admin/{$this->controller_name}/index", ['spm' => $_REQUEST['spm'], 'runtime' => time()]);  // 放在前面组装

    }

    public function index(){

        $db = DB::table($this->table) ;
        $pk = $db->getPk();
        $module_info = Db::name('system_module')->where("tb_name='$this->table'")->find();
        $this->assign("module_info",$module_info);

        $tbList = $this->_list( $db ,false,false);
        $this->assign("pk",$pk);

        $this->assign("list",$tbList['list']);


        echo $this->fetch( );
        exit;

    }

    public function save() {
        validateField($this->table);
        $cron_time = trim($_REQUEST['cron_time']);  //“*/2 * * * *" 去掉未部
        $slices = preg_split("/[\s]+/", $cron_time, 6);
        if( count($slices) < 5 ) {
            die("时间格式错误" );
        }
        $_POST['createtime'] = time();
        $_POST['updatetime'] = time();

        $result = DataService::save($this->table, $_POST);
        if ($result) {

            $this->success("操作成功！",adminUrl("crontab/index"));
        }else {
            $this->error("操作失败！请重试",adminUrl("crontab/index"));
        }
    }

    public function delete() {
        $model = DB::table('crontab');

        if (false==$_REQUEST["id"]) {
            $this->error("pk取参错误,Line:".__LINE__);
        }
        $ids = explode(",",$_REQUEST["id"]);

        foreach ($ids as $k=>$id) {
            if (intval($id)>0) {
                try {
                    $model->delete($id);
                }catch (Exception $e) {
                    $this->error("更新失败,请重试!error:".$e->getMessage(),'');
                }
            }
        }

        $this->error("操作成功",'');
    }
}