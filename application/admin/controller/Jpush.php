<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/24
 * Time: 8:42
 */

namespace app\admin\controller;

use controller\BasicAdmin;
use service\DataService;
use service\NodeService;
use service\ToolsService;
use think\Db;
use think\Cache;
use org\FieldFromClass;
use org\ContentList;
use org\QueryFrom;
use think\Exception;
use think\Request;
use JPush\Client as JPushClient;


class Jpush extends BasicAdmin
{
    public $table, $fields, $map;

    public function __construct ()
    {
        parent::__construct();

        $this->url = url("/admin/{$this->controller_name}/index", ['spm' => $_REQUEST['spm'], 'runtime' => time()]);  // 放在前面组装
        $this->table = $this->controller_name;
        //   $moduleId = $this->getModuleId($this->controller_name);
        $moduleTb = strtolower($this->table);
        $this->tbFields = F($moduleTb . "_Field");
        $this->assign("tbField", $this->tbFields);

        $this->assign("url_index", $this->url);


    }

    public function index ()
    {

        $db = DB::table($this->controller_name);
        $pk = $db->getPk();
        $module_id = $this->getModuleId($this->controller_name);
        $module_info = Db::name('system_module')->find($module_id);
        $this->assign("module_info", $module_info);

        $tbList = $this->_list($db, true, false);
        $this->assign("pk", $pk);
        $this->assign("pages", $tbList['page']);
        $this->assign("list", $tbList['list']);

        $tplName = (app('view')->exists("{$this->controller_name}/index")) ? "{$this->controller_name}/index" : "content/index";
        // var_dump('sdfsad');die;
        $this->assign("ContentList", new ContentList());

        return $this->fetch($tplName);

        return false;
    }

    public function _query_filter (&$map)
    {

        foreach ($_REQUEST['query'] as $k => &$v) {
            if ($k == 'province' && $v['title'] == -1) unset($v['title']);
            if ($k == 'city' && $v['title'] == -1) unset($v['title']);

            if (!empty($v['title'])) {
                $title = trim($v['title']);
                if (!isset($v['symbol'])) {
                    $frmMap[] = $map[] = array($k, "eq", "{$title}");
                } elseif (strtolower($v['symbol']) == 'like') {
                    $frmMap[] = $map[] = array($k, "like", "%{$title}%");
                } else {
                    $frmMap[] = $map[] = array($k, "{$v['symbol']}", "{$title}");
                }
            }

            if (!empty($v['starttime'])) {
                $starttime = $v['starttime'] . ' 0:0:0';
                $starttime = strtotime($starttime);
                $frmMap[] = array($k . '_start_time', "egt", "{$starttime}");
                $map[] = array($k, "egt", "{$starttime}");
            }

            if (!empty($v['endtime'])) {
                $endtime = $v['endtime'] . ' 23:23:59';
                $endtime = strtotime($endtime);
                $frmMap[] = array($k . '_end_time', "elt", "{$endtime}");
                $map[] = array($k, "elt", "{$endtime}");
            }

            if (!empty($v['province']) && $v['province'] > 0) {

                $frmMap[] = $map[] = array('province', "eq", "{$v['province']}");
            }

        }

        $frmMap && $this->assign("map", json_encode($frmMap));

    }


    public function edit ()
    {

        $res = DB::name("system_module")->where("name", "=", $this->controller_name)->find();
        $moduleid = $res['id'];

       // $fields = $this->FieldCache($moduleid, 'get');
        //prt($fields);
        $this->assign('fields', $fields);

        $id = intval($_REQUEST ['id']);
        if ($id > 0) {
            //  parent::returnMsg ('error',L('edit_error') );
            $vo = DB::name($this->controller_name)->getById($id);

        }

        $pk = DB::name($this->controller_name)->getPk();
        $this->assign("pk", $pk);

        //prt($vo);
        false == $vo['content'] && $vo['content'] = htmlspecialchars($vo['content']);


        $this->assign('vo', $vo);

        // prt($vo);
        //   $this->assign ( 'form', $form );
        $this->assign('FieldFromClass', new FieldFromClass());

        $tplName = (app('view')->exists("{$this->controller_name}/edit")) ? "{$this->controller_name}/edit" : "content/edit";
        $tplCn = $this->fetch($tplName);
        echo $tplCn;
    }


    public function before_update ()
    {
        //        $moduleId = $this->getModuleId($this->controller_name);
        //        $fields = F($moduleId."_Field");
        //         prt($this->fields,0);
        foreach ($_POST as $kk => &$vv) {
            foreach ($this->fields as $k => $v) {
                if ($v['type'] == 'datetime' && $v['field'] == $kk) {   //转换时间格式
                    if ($v['field'] == 'updatetime') {
                        $_POST['updatetime'] = time();
                    } else {
                        $vv = toTimestamp($vv);
                        // $vv = strtotime($vv);
                    }

                    if ($v['field'] == 'createtime') {
                        $_POST['createtime'] = time();
                    } else {
                        $vv = toTimestamp($vv);
                    }
                }

                if ($v['type'] == 'typeid' && $v['field'] == $kk) {
                    is_array($vv) && $vv = implode(",", $vv);
                }
                if ($v['type'] == 'editor') {
                }
                if ($v['type'] == 'image' && $v['field'] == $kk) {
                    $vv = implode(",", $vv);
                }
                if ($v['type'] == 'regional_Linkage') {
                    $vv == -1 && $vv = 0;
                }
            }
        }
    }

    public function query ()
    {

        $this->assign('QueryFrom', new QueryFrom());

        if ($_REQUEST['param']) {
            foreach (json_decode($_REQUEST['param']) as $k => $v) {
                $map[$v[0]] = array('symbol' => $v[1], 'title' => $v[2]);
            }
            //            prt($_REQUEST['param'],0);
            //            prt($map,0);
            $map && $this->assign('param', $map);
        }

        $tplName = (app('view')->exists("{$this->controller_name}/query")) ? "{$this->controller_name}/query" : "content/query";
        $tplCn = $this->fetch($tplName);
        savelog($tplCn, "content_query_frm");
        echo $tplCn;
    }


    /**
     * http://naoli.dev.com:81/admin/campus/export.html
     *
     * 导出excel数据表
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function export ()
    {
        $arr = parse_url($_SERVER['HTTP_REFERER']);
        parse_str(urldecode($arr['query']), $query);
        clearstatcache();
        $xlsCell = array();
        foreach ($this->fields as $k => $v) {
            if ($v['listShowField'] == 1) {
                $xlsCell[] = array($v['field'], $v['name']);
                $tmp_cell[] = $v['field'];
            }
        }
        $db = DB::table($this->controller_name)->field($tmp_cell);
        $tbList = $this->_list($db, true, false);
        //TODO 数据转换

        unset($k, $v);
        $map = array();
        foreach ($query['querycontrol'] as $k => $v) {
            if ($v['value']) {
                if ($v['formula0'] == 'like') {
                    $map[$k] = array($v['formula'], "%" . $v['value'] . "%");
                } else
                    $map[$k] = array($v['formula'], $v['value']);
            }
        }

        $module = Db::name('system_module')->where('name="' . $this->controller_name . '"')->select();
        $xlsName = isset($module[0]['title']) ? $module[0]['title'] : '';
        //组织数据格式
        set_time_limit(0);      //执行时间无限
        ini_set('memory_limit', '-1');
        dataExcel($xlsName, $xlsCell, $tbList['list']);
    }



    public function push ()
    {
        $title = '发送测试';
        $map[] = ['status', 'eq', 1];
        $map[] = ['registrationID', 'neq', ''];
        $userList = Db::name('users')->where($map)->column(['id','registrationID']);

        // prt($userList);
        return $this->fetch('form', ['userList' => $userList,'title'=>$title]);
    }



    public function jpush_send(){
        $user = $this->request->post('user', '', ''); //用户 all 、id
        $type = $this->request->post('type', '', 'trim'); //用户 all 、one
        $content = $this->request->post('content', '', 'trim'); //用户 all 、one
        // try{
            if($type=='all'){
                //TODO 推送全部用户
                $map[] = ['status', 'eq', 1];
                $map[] = ['registrationID', 'neq', ''];
                $userList = Db::name('users')->where($map)->column(['id','registrationID']);
                // prt($userList);
                if(!empty($userList)){
                    foreach ($userList as $val){
                        // echo $val;
                        //TODO 方法待定
                        // $this->ex($val,$content);
                    }
                }
            }else{
                //TODO 推送指定用户 $user //TODO 方法待定
                // $this->ex($user,$content);
            }
            // $data, $code = 0, $msg = ''
            // $this->result('',1,'发送成功');
            $this->result('',1,'发送通道暂未开通');
        // }catch(\Exception $e){
        //     // prt($e->getMessage());
        //     $this->result('',0,'发送失败');
        // }

    }


    const  APPKEY ="2c9e591c41f9982e269c25ae";
    const  SECRET ="b90d22040fbeae4b663332ce";


    //极光推送appkey
    static public function app_key()
    {
        $app_key = self::APPKEY;
        return $app_key;
    }

    //极光推送master_secret
    static public function master_secret()
    {
        $master_secret = self::SECRET;
        return $master_secret;
    }

    //获取alias和tags
    public function getDevices($registrationID)
    {
        // require_once APP_PATH . '/../extend/jpush/autoload.php';
        $app_key = $this->app_key();
        $master_secret = $this->master_secret();
        $client = new JPushClient($app_key, $master_secret);
        $result = $client->device()->getDevices($registrationID);
        return $result;

    }

    //添加tags
    public function addTags($registrationID, $tags)
    {
        // require_once APP_PATH . '/../extend/jpush/autoload.php';
        $app_key = $this->app_key();
        $master_secret = $this->master_secret();
        $client = new JPushClient($app_key, $master_secret);
        $result = $client->device()->addTags($registrationID, $tags);
        return $result;

    }

    //移除tags
    public function removeTags($registrationID, $tags)
    {

        // require_once APP_PATH . '/../extend/jpush/autoload.php';

        $app_key = $this->app_key();
        $master_secret = $this->master_secret();
        $client = new JPushClient($app_key, $master_secret);
        $result = $client->device()->removeTags($registrationID, $tags);
        return $result;

    }

    //标签推送
    public function push1($tag, $alert)
    {

        // require_once APP_PATH . '/../extend/jpush/autoload.php';

        $app_key = $this->app_key();
        $master_secret = $this->master_secret();


        $client = new JPushClient($app_key, $master_secret);
        $regID = "";
        $push_payload = $client->push()
            ->setPlatform('all')
            //     ->addAllAudience()
            ->addRegistrationId($regID)
            ->setNotificationAlert('Hi, JPush');
        try {
            $response = $push_payload->send();
            prt($response);
        } catch (\JPush\Exceptions\APIConnectionException $e) {
            // try something here
            prt($e);
        } catch (\JPush\Exceptions\APIRequestException $e) {
            // try something here
            prt($e);
        }


        die;
        $tags = implode(",", $tag);
        $client->push()
            ->setPlatform(array('ios', 'android'))
            ->addTag($tags)//标签
            ->setNotificationAlert($alert)//内容
            ->send();

    }

    //别名推送
    public function push_a($alias, $alert)
    {

        // require_once APP_PATH . '/../extend/jpush/autoload.php';

        $app_key = $this->app_key();
        $master_secret = $this->master_secret();

        $client = new JPushClient($app_key, $master_secret);
        $alias = implode(",", $alias);
        $client->push()
            ->setPlatform(array('ios', 'android'))
            ->addAlias($alias)//别名
            ->setNotificationAlert($alert)//内容
            ->send();

    }

    //示例：http://door.mydanweb.com/api/tool.Pushmess/ex

    public function ex($RegistrationID='',$content = '')
    {
        $RegistrationID = $RegistrationID !=false ? $RegistrationID : '1507bfd3f7968d33e84';
        $tag = array(
            "123"
        );
        $alert = $content!=''?$content :"标签推送";
        $this->push($tag,$alert);
    }
}