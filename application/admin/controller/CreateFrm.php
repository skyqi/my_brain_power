<?php


namespace app\admin\controller;

use controller\BasicAdmin;
use service\ToolsService;
use think\Db;
use service\DataService;
use org\FieldFromClass;
/**
 * 后台入口
 * Class Index
 * @package app\admin\controller
 * @author Anyon <zoujingli@qq.com>
 * @date 2017/02/15 10:41
 */
class CreateFrm extends BasicAdmin
{
    //  http://ivehice.mydanweb.com/admin.html#/admin/Create_frm/index.html
    public function index()
    {
        $module_name = $_REQUEST['table'];
        $this->table = DB::table("system_module")->where("name='$module_name'")->value("tb_name");
      //  $title = '<div class="site-title"><span>'.$this->table."</span><a href='".adminUrl("module/index")."'>返回</a>".'</div>';
        $title = "<a href='javascript:void(0);'>{$this->table}</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href='".adminUrl("module/index")."'>返回</a>";
        $this->assign("title",$title);
        $this->phptpl();
        $this->phptpl2();
        $this->phptpl3();
        $this->phptpl4();
        $this->phptpl_detail();
        echo $this->fetch();
    }



    public function phptpl()
    {
        $file = __DIR__."/../view/content/index.html";
        $txt =  file_get_contents($file);

        $this->assign("tplCn",($txt));
    }

    public function phptpl2()
    {
        $table = $this->table ;
        $tbfields = F($table."_Field");

        $this->assign ( 'fields', $tbfields );

        $pk = DB::name($table)->getPk();
        $this->assign("pk",$pk);

        $this->assign ( 'FieldFromClass', new FieldFromClass() );


        $tplCn = $this->fetch("edit_tpl");
        $this->assign("tplCn2",$tplCn);
    }

    public function phptpl3()
    {

        $tbfields = F($this->table."_Field");

        $file = __DIR__."/../view/content/edit_control.tpl";
        $filetxt =  file_get_contents($file);
        $tpl = '{~$r=$tbField[\'__name__\']} <!-- __field_cn__ -->
        <tr>
            <td  >{if condition="$r[\'required\']"}<font color="red">*</font>{/if}{$r.name}</td>
            <td  >
                {$r|$FieldFromClass::index=###,$r|raw}
            </td>
        </tr>';

        foreach ($tbfields as $k=>$v) {
            $rep_tpl = str_replace("__name__",$k,$tpl);
            $_field_htmls[] = str_replace("__field_cn__",$v['name'],$rep_tpl);
        }

        $tplcn = str_replace("<tpm></tpm>",implode(" ",$_field_htmls),$filetxt);

        $this->assign("tplCn3",$tplcn);


    }


    public function phptpl4()
    {

        $file = __DIR__."/../view/content/edit.html";
        $filetxt =  file_get_contents($file);

        $this->assign("tplCn4",$filetxt);
    }

    public function phptpl_detail()
    {
        $file = __DIR__."/../view/content/detail.tpl";
        $filetxt =  file_get_contents($file);
        $this->assign("detailCn",$filetxt);
    }

    //新增或编辑
    public function edit()
    {
        // prt($_REQUEST);
        $action='edit';
        intval($_REQUEST['slide_id'])==0 && $action='add';
        $slide_id = $_REQUEST['slide_id'];
        $table = $_REQUEST['module']=='slideattrdata'?'system_slideattrdata': 'system_slide';

        $db = Db::name($table);
        $pk = $db->getPk();
        $this->assign("pk",$pk);
        $this->assign("slide_id",$slide_id);
        // prt($slide_id);
        if ($action=='edit') {
            $where[$pk] = $slide_id;
        }

        if ($_POST) { unset($where); $where= false; }

        $pkField = $_REQUEST['module']=='slideattrdata'?'id':'slide_id';
        $form = $_REQUEST['module']=='slideattrdata'?'slideattrdata_form':'form';
        // prt($action);
        echo $this->_form($db, $form,$pkField,$where);
        exit;


    }

    //将post数据过滤后提交
    public function _form_filter(&$data) {
        // prt($data,0);
        // prt($this->appRoot,0);

        if (!empty($data)) {
            if(isset($_POST['pic']) && $_POST['pic']!=false){
                //post保存数据
                !empty($data['pic']) && $data['pic']  =  str_replace($this->appRoot,'',$_POST['pic']);
            }else{
                //编辑加载数据
                !empty($data['pic']) && $data['pic']  =  $this->appRoot.$data['pic'];
            }
        }
    }





}