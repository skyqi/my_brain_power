<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2017 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://think.ctolog.com
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zoujingli/ThinkAdmin
// +----------------------------------------------------------------------

namespace app\admin\controller;

use controller\BasicAdmin;
use org\FieldFromClass;
use org\CreateFromControl;
use service\DataService;
use service\NodeService;
use service\ToolsService;
use think\App;
use think\Db;
use think\Cache;

use think\exception\ErrorException;
use think\Loader;
use think\Exception;
/**
 * 后台入口
 * Class Field
 * @package app\admin\controller
 * @author Anyon <zoujingli@qq.com>
 * @date 2017/02/15 10:41
 */
class Field extends BasicAdmin
{
    public $table  ;


    public function __construct()
    {
        //  Cache($this->table.'_cache',null);
        parent::__construct();

        $this->assign('moduleid',$this->moduleid);
        if(method_exists(__CLASS__,'_before_'.$this->action_name)){
            // call_user_func('_before_'.$this->action_name);
            $action = '_before_'.$this->action_name;
            $this->$action();
        }

        $this->table = "system_field";
        $this->url = url('/admin/field/index',['moduleid'=>$_REQUEST['moduleid'],'spm'=>$_REQUEST['spm'], 'runtime' => time()]);


    }

    public function index()
    {
        $mo_uuid =  trim($_REQUEST['mo_uuid']);
        $moduleList = Db::name("system_module")->where("mo_uuid='".$mo_uuid."'")->find();

        if (false==$moduleList) {
            $this->error("mo_uuid不存在的数据");
        }
        $this->assign('mo_uuid',$mo_uuid);

        $this->title =  $moduleList['title']."  [".$moduleList['name']."]";
        $pk = Db::name($this->table)->getPk();

        $this->assign("pk",$pk);
        $db = Db::name($this->table)->field("*")->order('sort asc,id asc')->where("mo_uuid='".$mo_uuid."'");

        return parent::_list($db, false);
    }



    //http://ivehice2.mydanweb.com/admin/field/index.html
    public function fieldlist()
    {
        $mo_uuid = trim($_REQUEST['mo_uuid']);

        $moduleList = DB::name("system_module")->where("mo_uuid='{$mo_uuid}'")->find();

        //$moduleList = F("system_module");
        // prt($this->url);
        $this->title =  $moduleList['title']."  [".$moduleList['name']."]";

        $pk = Db::name($this->table)->getPk();
        $this->assign("pk",$pk);

        $db = Db::name($this->table)->field("*")->order('sort asc,id asc')->where("mo_uuid='{$mo_uuid}'");
        $dlist= parent::_list($db, false,false);

        $this->assign("list",$dlist['list']);
        echo $this->fetch("fieldlist");

    }

    /**
     * 右侧编辑，添加字段页面加载
     */
    public function fieldedit() {
        $model = Db::name("system_field");
        $pk=ucfirst($model->getPk());
        $id = intval($_REQUEST[$model->getPk ()]);
        // prt($_REQUEST);
        if(!empty($id))  {

            //$this->error(L('do_empty'));
            $do='getBy'.$pk;
            $vo = $model->$do ( $id );
            if(!empty($vo['setup']) && is_string($vo['setup'])) {
                $vo['setup']= string2array( stripslashes($vo['setup']));
            }
            $this->assign ( 'vo', $vo );
        }
        // prt($vo);
        $this->assign ( 'FieldFromClass', new FieldFromClass());
        $this->assign ( 'CreateFromControl', new CreateFromControl());
        echo $this->fetch ( 'edit');
    }

    public function edit() {

        $model = Db::name("system_field");
        $pk=ucfirst($model->getPk());
        $id = intval($_REQUEST[$model->getPk ()]);

        if(!empty($id))  {

            //$this->error(L('do_empty'));
            $do='getBy'.$pk;
            $vo = $model->$do ( $id );
            if(!empty($vo['setup']) && is_string($vo['setup'])) {
                $vo['setup']= string2array( stripslashes($vo['setup']));
            }

            $this->assign ( 'vo', $vo );
        }

        if (false==($mo_uuid=$_REQUEST['mo_uuid'])) {
            $system_field_res = Db::name("system_field")->where("id=" . $id)->field("tb_name,mo_uuid")->find();
            $mo_uuid = $system_field_res['mo_uuid'];
        }
        $tb_name = Db::name("system_module")->where("mo_uuid='$mo_uuid'" )->value("tb_name");

        $fieldList =  Db::name($tb_name)->getTableFields();

        $this->assign("fieldList",$fieldList);


        $this->assign ( 'FieldFromClass', new FieldFromClass());
        $this->assign ( 'CreateFromControl', new CreateFromControl());
        echo $this->fetch ( 'edit');

    }

    public function _before_add(){
        // prt($_REQUEST);
        //if(empty($this->moduleid))$this->error(L('do_empty'));

        if($_REQUEST['isajax']) {
            if (intval($_REQUEST['id']) > 0) {
                $info = DB::name('system_field')->field("setup")->find($_REQUEST['id']);
                if (!empty($info['setup']) && is_string($info['setup'])) {
                    $info['setup'] = string2array($info['setup']);
                }

                if (false!=$info['setup']['bindModule']) {
                    $info['setup']['bindModule2'] = explode("||",$info['setup']['bindModule']);
                }

                $this->assign($info['setup']);
            }
            $this->assign($_REQUEST);
            $DBTables = DB::name("system_module")->where("status",1)->column("id,title,name,tb_name");

            $this->assign ( 'DBTables', $DBTables);
            echo $this->fetch('type');
            exit;
        }

        $this->assign ( 'FieldFromClass', new FieldFromClass());
        $this->assign ( 'CreateFromControl', new CreateFromControl());
    }

    public function update()
    {

        $_validate = array(

            array(false==$_POST['field'],"字段名必须输入！"),
            array(false==$_POST['name'],"中文别名必须输入！"),
        );

        foreach ($_validate as $k=>$v) {
            if ($v[0]) $this->error($v[1]);
        }

       // $tbFields = DB::name("system_field")->getTableFields();


        $editfieldsql = $this->get_tablesql($_POST, 'edit');

        if ($_POST['setup']) $_POST['setup'] = array2string($_POST['setup']);
        if (!empty($_POST['listShowField']))  $_POST['listShowField']=1;
        if (!empty($_POST['queryfield']))  $_POST['queryfield']=1;
        $_POST['unpostgroup'] = $_POST['unpostgroup'] ? implode(',', $_POST['unpostgroup']) : '';

        if ($_POST['type']=='regional_Linkage' ) {
            $_POST['field'] = 'regional_Linkage';
            $_POST['pattern'] = '';
        }

        //try {
        if(is_array($editfieldsql)){
            foreach($editfieldsql as $sql){
                DB::execute($sql);
            }
        }else{

            !empty($editfieldsql) && $r=DB::execute($editfieldsql);
        }

        $saveRet = DataService::save("system_field",$_POST);
        $this->FieldCache("mo_uuid='{$_POST['mo_uuid']}'",'w');


//        }catch(\Exception $e){
//            parent::returnMsg('error','','执行错误'.$e->getMessage());
//            die;
//        }

        parent::returnMsg('success');
    }

    public function get_tablesql($info,$do){

        $fieldtype = $info['type'];
        if (!empty($info['setup']) && is_string($info['setup'])) {
            $info['setup'] = string2array($info['setup']);
        }


        if($info['setup']['fieldtype']){
            $fieldtype=$info['setup']['fieldtype'];
        }

        $mo_uuid = $info['mo_uuid'];

        $default = $info['setup']['default'];
        $field = $info['field'];
        $tb_name=  Db::table("system_module")->where("mo_uuid='{$mo_uuid}'")->value("tb_name");
        $tablename = strtolower($tb_name);

        $maxlength = intval($info['maxlength']);
        $minlength = intval($info['minlength']);
        $numbertype = $info['setup']['numbertype'];
        $oldfield = $info['oldfield'];
        $before_field = '';
            !empty($info['befer_field']) && $before_field = " after `{$info['befer_field']}`" ;


        if (intval($info['id'])==0) {
            $do = ' ADD ';
        } else   $do =  " CHANGE `$oldfield` ";

        switch($fieldtype) {
            case 'varchar':
                if (!$maxlength) $maxlength = 255;
                $maxlength = min($maxlength, 255);
                $sql = "ALTER TABLE `$tablename` $do `$field` VARCHAR( $maxlength ) NOT NULL DEFAULT ''  $before_field ";
                break;

            case 'title':
                if (!$maxlength) $maxlength = 255;

                $maxlength = min($maxlength, 255);
                $sql[] = "ALTER TABLE `$tablename` $do `$field` VARCHAR( $maxlength ) NOT NULL DEFAULT ''  $before_field ";
//                $sql[] = "ALTER TABLE `$tablename` $do `title_style` VARCHAR( 40 ) NOT NULL DEFAULT ''";
//                $sql[] = "ALTER TABLE `$tablename` $do `thumb` VARCHAR( 100 ) NOT NULL DEFAULT ''";

                break;

            case 'catid':
                $sql = "ALTER TABLE `$tablename` $do `$field` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0'  $before_field ";
                break;

            case 'number':
                $decimaldigits = $info['setup']['decimaldigits'];
                $default = $decimaldigits == 0 ? intval($default) : floatval($default);
                if ($info['setup']['length']>0) $maxlength = $info['setup']['length'];
                $sql = "ALTER TABLE `$tablename` $do `$field` " . ($decimaldigits == 0 ? 'INT' : 'decimal( $maxlength,' . $decimaldigits . ' )') . " " . ($numbertype == 1 ? 'UNSIGNED' : '') . "  NOT NULL DEFAULT '0'  $before_field ";
                break;

            case 'tinyint':
                if (!$maxlength) $maxlength = 1;
                $maxlength = min($maxlength, 3);
                if ($info['setup']['length']>0) $maxlength = $info['setup']['length'];
                $default = intval($default);
                $sql = "ALTER TABLE `$tablename` $do `$field` TINYINT( $maxlength ) " . ($numbertype == 1 ? 'UNSIGNED' : '') . " NOT NULL DEFAULT  $default  $before_field ";
                break;


            case 'smallint':
                $default = intval($default);
                if (!$maxlength) $maxlength = 8;
                $maxlength = min($maxlength, 8);
                if ($info['setup']['length']>0) $maxlength = $info['setup']['length'];
                $default = intval($default);
                $sql = "ALTER TABLE `$tablename` $do `$field` SMALLINT( $maxlength ) " . ($numbertype == 1 ? 'UNSIGNED' : '') . " NOT NULL DEFAULT  $default   $before_field ";
                break;

            case 'int':
                $default = intval($default);
                $maxlength = 11;
                if ($info['setup']['length']>0) $maxlength = $info['setup']['length'];
                $default = intval($default);
                $sql = "ALTER TABLE `$tablename` $do `$field` INT ( $maxlength ) " . ($numbertype == 1 ? 'UNSIGNED' : '') . " NOT NULL DEFAULT  $default  $before_field ";
                break;

            case 'mediumint':
                $default = intval($default);
                $maxlength = 11;
                if ($info['setup']['length']>0) $maxlength = $info['setup']['length'];
                $default = intval($default);
                $sql = "ALTER TABLE `$tablename` $do `$field` INT ( $maxlength ) " . ($numbertype == 1 ? 'UNSIGNED' : '') . " NOT NULL DEFAULT  $default  $before_field ";
                break;

            case 'mediumtext':
                $sql = "ALTER TABLE `$tablename` $do `$field` MEDIUMTEXT NOT NULL  $before_field ";
                break;

            case 'text':
                $sql = "ALTER TABLE `$tablename` $do `$field` TEXT NULL  $before_field ";
                break;

            case 'posid':
                $sql = "ALTER TABLE `$tablename` $do `$field` TINYINT(2) UNSIGNED NOT NULL DEFAULT '0'  $before_field ";
                break;

            case 'typeid':
                $sql = "ALTER TABLE `$tablename` $do `$field` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0'  $before_field "   ;
                break;

            case 'datetime':
                $sql = "ALTER TABLE `$tablename` $do `$field` INT(11) UNSIGNED NOT NULL DEFAULT '0'  $before_field ";
                break;

            case 'editor':
                $sql = "ALTER TABLE `$tablename` $do `$field` TEXT NULL  $before_field ";
                break;

            case 'image':
                $sql = "ALTER TABLE `$tablename` $do `$field` VARCHAR( 80 ) NULL DEFAULT ''  $before_field ";
                break;

            case 'images':
                $sql = "ALTER TABLE `$tablename` $do `$field` MEDIUMTEXT NULL  $before_field ";
                break;

            case 'file':
                $sql = "ALTER TABLE `$tablename` $do `$field` VARCHAR( 80 ) NULL DEFAULT ''  $before_field ";
                break;

            case 'files':
                $sql = "ALTER TABLE `$tablename` $do `$field` MEDIUMTEXT NULL   $before_field ";
                break;

           case 'regional_Linkage':
//                if ($do == ' ADD ') {
//                    $areaField = array();
//                    $areaField[] = " $do `province` INT(11) UNSIGNED NOT NULL DEFAULT '0' AFTER `sort` ";
//                    $areaField[] = " $do `city` INT(11) UNSIGNED NOT NULL DEFAULT '0' AFTER `sort` ";
//                    // $_POST['setup']['district'] && $areaField[] =   " $do  district INT(11) UNSIGNED NOT NULL DEFAULT '0'" ;
//
//                    $sql = "ALTER TABLE `$tablename`  " . implode(",", $areaField);
//
//                }

                if (!$maxlength) $maxlength = 250;

                $sql = "ALTER TABLE `$tablename` $do `regional_Linkage` VARCHAR( 250 ) NOT NULL DEFAULT ''  $before_field ";

                break;
        }

        return $sql;
    }

    function insert() {

        if (false==($mo_uuid=$_REQUEST['mo_uuid'])) {
            parent::returnMsg("error",$this->url,"mo_uuid参数错误");
        }
        false!=$_POST['editShowField']  && $_POST['editShowField']=1;
        false!=$_POST['listShowField']  && $_POST['listShowField']=1;
        false!=$_POST['queryfield']  && $_POST['queryfield']=1;

        $moduleRes = DB::name("system_module")->where("mo_uuid='{$mo_uuid}'")->find();
        $_POST['tb_name'] = $moduleRes['tb_name'];
        $_validate = array(


            array(false==$_POST['type'],"字段类型必须输入！"),
            array(false==$_POST['field'],"字段名必须输入！"),
            array(false==$_POST['name'],"中文别名必须输入！"),
            array(false==$_POST['tb_name'],"表名必须输入！"),
        );

        foreach ($_validate as $k=>$v) {
            if ($v[0]) $this->error($v[1]);
        }
//        $befer_field = $_POST['befer_field'] ;
//        unset($_POST['befer_field']);

        $map['mo_uuid'] = $_POST['mo_uuid'];
        $map['field'] = $_POST['field'];
        $count = DB::table("system_field")->where($map)->count();
        if ($count>0) {
            $this->error("一个模块不能重复输入同样的字段名");
        }

        if($_POST['setup']) $_POST['setup']=array2string($_POST['setup']);
        $_POST['status'] =1;

//        if ($_POST['type'] == 'regional_Linkage') {
//            $callRes = $this->_callback($_POST['type']."_action");
//        };


        $addfieldsql =$this->get_tablesql($_POST,'add');

        if(is_array($addfieldsql)){
            foreach($addfieldsql as $sql){
                DB::execute($sql);
            }
        }else{
            if($addfieldsql) DB::execute($addfieldsql);
        }

        if (false!=$_POST['queryfield'])  $_POST['queryfield']= 1;
        if (false!=$_POST['editShowField'])  $_POST['editShowField']=1;
        if (false!=$_POST['listShowField'])  $_POST['listShowField']=1;

        DataService::save("system_field",$_POST);
        $this->FieldCache("mo_uuid='{$_POST['mo_uuid']}'",'w');
        $url = url("/admin/{$this->controller_name}/index",['moduleid'=>$_POST['moduleid'],'spm'=>$_REQUEST['spm']]);  // 放在前面组装
        $jumpUrl = $_POST['forward'] ? $_POST['forward'] :$url;
        parent::returnMsg('success',$jumpUrl);



    }



    function delete() {
        $id= intval($_REQUEST['id']);

        $r = Db::table($this->table)->find($id);

        $url = url("/admin/{$this->controller_name}/index",['mo_uuid'=>$r['mo_uuid'],'spm'=>$_REQUEST['spm'],'runtime'=>time()]);  // 放在前面组装
        if(empty($r))  $this->returnMsg('error',$url,L('do_empty'));

        $tbname = trim(strtolower($r['tb_name']));

        if (false==$tbname) {
            $this->returnMsg('error',null,"未找到缓存模块名，错误文件名Field.php");
        }
        $field = $r['field'];

        try{

            $sql = "ALTER TABLE `{$tbname}` DROP `$field`";
            Db::execute("ALTER TABLE `{$tbname}` DROP `$field`");
            Db::table($this->table)->delete($id);

        }catch(\Exception $e){
            prt($sql,0);
            prt($e->getMessage());
            //  $this->returnMsg('error',null,'执行错误'.$e->getMessage());
        }
        $this->FieldCache("tb_name='$tbname'",'del');
        $this->returnMsg('success',$url,L('delete_ok'));
    }

    //显示，列表查询字段
    public function ajaxAction()
    {
        $txt =  input('request.txt');
        if (preg_match("/√/",$txt) || preg_match("/禁用/Ui",$txt) ) {
            $value = 0;
        }else $value = 1;

        ////
        $tbField = $this->request->request('action','','trim') ;
        list($where, $update) = [['id' => input('request.id/d')], [$tbField => $value]];

        if ($value==1) { $resData = "1"; } else { $resData = "0" ;}
        // prt($txt);
        $db = Db::table('system_field')->where($where);
        if (false === Db::table('system_field')->where($where)->update($update)) {
            $this->error('操作失败, 请刷新再试');
        }
        $mo_uuid = Db::table('system_field')->where($where)->value("mo_uuid");
        $this->FieldCache("mo_uuid='{$mo_uuid}'",'w');
        $this->success('操作成功', '',$resData);

    }

    //取字段名
    public function getTBFields()
    {
        $module = $_REQUEST['bindTable'];
        $tb_name = Db::table("system_module")->where("name='$module'")->cache(true,60)->value("tb_name");
        $fields = Db::table("system_field")->where("tb_name='$tb_name'")->field("name,field")->select();


        foreach ($fields as $k=>$v) {
            if (!in_array($v['field'],array("id","sort","updatetime","createtime"))) {
                $new[] = ['name' => $v['name'], 'field' => $v['field']];
            }
        }
        die (json_encode($new));
    }

}
