<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2017 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://think.ctolog.com
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zoujingli/ThinkAdmin
// +----------------------------------------------------------------------

namespace app\admin\controller;

use controller\BasicAdmin;
use service\LogService;
use service\NodeService;
use think\Db;
use think\captcha\Captcha;


/**
 * 系统登录控制器
 * class Login
 * @package app\admin\controller
 * @author Anyon <zoujingli@qq.com>
 * @date 2017/02/10 13:59
 */
class Login extends BasicAdmin
{

    /**
     * 控制器基础方法
     */
    public function initialize ()
    {
        if (session('user.id') && $this->request->action() !== 'out') {
            $this->redirect('@admin');
        }
    }

    /**
     * 用户登录
     * @return string
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function index ()
    {
        if ($this->request->isGet()) {

            return $this->fetch('', ['title' => '用户登录']);
        }
        // 输入数据效验
        $username = $this->request->post('username', '', 'trim');
        $password = $this->request->post('password', '', 'trim');
        $verify_code = $this->request->post('verify_code', '', 'trim');
        strlen($username) < 4 && $this->error('登录账号长度不能少于4位有效字符!');
        strlen($password) < 4 && $this->error('登录密码长度不能少于4位有效字符!');
        // $user = Db::name('SystemUser')->where('username', $username)->find();
        // prt($user);
        //开启验证码模式或者尝试登陆密码错误5次后
        if(sysconf('ADMIN_VERIFY') || session('try_logins')>5){
            //验证验证码
            if (!captcha_check($verify_code)) {
                // 验证失败
                $this->error('验证码错误',url('admin/login/index'));
            }
        }


        // 用户信息验证
        $user = Db::name('SystemUser')->where('username', $username)->find();

        empty($user) && $this->error('登录账号不存在，请重新输入!');

        // prt(session('try_logins'),0);

        if($user['password'] !== md5($password)){
            if (session('try_logins') !=false ){
                session('try_logins', session('try_logins')+1);
            }else{
                session('try_logins',1);//记录密码错误次数
            }

            //登陆失败10次锁定账号
            if (session('try_logins')>10){
                Db::name('SystemUser')->where(['id' => $user['id']])->update(['status'=>0,'login_at'=>date('Y-m-d H:i:s')]);
                LogService::write('系统管理', '用户登录系统超过10次密码错误，账号被锁定');
                $user['status'] = 0;
                empty($user['status']) && session('try_logins',null) && $this->error('超过10次密码错误，账号已经被禁用2小时!');
            }

            $this->error('登录密码与账号不匹配，请重新输入!');
        } else {

            if (false==$user['status'] && time() - strtotime($user['login_at'])> 60*60*2 ) {
                Db::name('SystemUser')->where(['id' => $user['id']])->update(['status'=>1,'login_at'=>date('Y-m-d H:i:s')]);
            } else {
                false==$user['status'] && $this->error('账号已经被禁用，请联系管理!');
            }

        }


        // 更新登录信息
        // $data = ['login_at' => ['exp', 'now()'], 'login_num' => ['exp', 'login_num+1']];
        $data = ['login_at' => date('Y-m-d H:i:s'), 'login_num' => ['inc', '1']];
        Db::name('SystemUser')->where(['id' => $user['id']])->update($data);

       // prt(session_id("user_". $user['id']),0);
//        session_id("user_". $user['id']);
//        session_start();

        session('user', $user);
        session('try_logins',null);//删除登陆次数信息
      //  prt($_SESSION);
        !empty($user['authorize']) && NodeService::applyAuthNode();
        LogService::write('系统管理', '用户登录系统成功');
        $this->success('登录成功，正在进入系统...', '@admin');
    }

    /**
     * 退出登录
     */
    public function out ()
    {
        session('user') && LogService::write('系统管理', '用户退出系统成功');
        !empty($_SESSION) && $_SESSION = [];
        [session_unset(), session_destroy()];
        $this->success('退出登录成功！', '@admin/login');
    }


    //验证码
    public function verify ()
    {
        // 自定义验证码格式
        $config = [
            // 验证码字体大小
            'fontSize' => 60,
            // 验证码位数
            'length'   => 4,
            // 关闭验证码杂点
            'useNoise' => false,
            // 验证码图片高度
            // 'imageH'   => 38,
            // 验证码图片宽度
            // 'imageW'   => 200,
            // 验证码过期时间（s）
            'expire'   => 1800,
        ];
        $captcha = new Captcha($config);

        return $captcha->entry();
    }

}
