<?php


namespace app\admin\controller;
use think\Controller;
use think\Request;
 class Error extends Controller
 {
     public function _empty(Request $request)
     {
         $action = $request->action();
         if( preg_match("/[a-z]+-[a-z]+/Ui",$request->controller())) {
             $controller = str_replace("-","_",$request->controller());
             $class = "app\admin\controller\\".$controller ;


             if(class_exists($class) && method_exists($class,$action) ){

                 action("{$class}/{$action}");
                 return false;
             }
         }

         if ($action== "add") $action="edit";
         action('Content/'.($action?$action:'index'));
     }
 }
