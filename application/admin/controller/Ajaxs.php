<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/22
 * Time: 17:50
 */
namespace app\admin\controller;
use controller\BasicAdmin;
use service\DataService;
use service\NodeService;
use service\ToolsService;
use think\Db;
use think\Cache;
use org\FieldFromClass;
use org\ContentList;
use org\QueryFrom;
use think\Exception;
use think\Request;
use org\CreateFromControl;
use think\Facade;
// Ajax回调处理
class Ajaxs extends BasicAdmin
{
    public $table, $fields, $map;

    public function __construct()
    {
        parent::__construct();
    }


    /**
     *更新system_field 配置
     */
    public function ajax_setting_field() {
        $id = $this->request->post('id', 0, 'int'); //字段id
        $val = $this->request->post('val', 0, 'int');//字段要设置的值
        $field = $this->request->post('field', 0, 'trim');//要设置的字段
       // $mo_uuid = $this->request->post('mo_uuid', 0, 'trim');//要设置的字段

        if (false==$id) {
            $this->error("id叁数失败");
        }

        if (false==$field) {
            $this->error("field叁数失败");
        }
        $map = [];
        $id && $map[] = ["id","eq",$id];

        $res = DB::table("system_field")->where($map)->update([$field=>$val]);
        \think\facade\Cache::clear();

        
        $this->result($res);
    }

}