<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/4
 * Time: 8:44
 *  分类管理
 */

namespace app\admin\controller;
use controller\BasicAdmin;
use service\DataService;
use service\NodeService;
use service\ToolsService;
use think\Db;
use think\Cache;
use org\FieldFromClass;
use org\TreeClass;

use org\ContentList;
class Types extends BasicAdmin
{

    public function __construct()
    {
        parent::__construct();
        $this->table = 'system_types';
    }


    public function index()
    {
        $parentList = $result = DB::name($this->table)->where("parentid=0")->order("sort asc,id asc")->select();
        $this->assign("parentList",$parentList);
        $map = '';
        if (intval($_REQUEST['parentid'])>0) {
            $map   = "(parentid=".$_REQUEST['parentid'] ." or id=".$_REQUEST['parentid'].')';

        }
        if (isset($_REQUEST['status']) && $_REQUEST['status'] != null){
            if ($map != ''){
                $map .= " and status=".intval($_REQUEST['status']);
            }else{
                $map .= "status=".intval($_REQUEST['status']);
            }
        }
        $result = DB::name($this->table)->where($map)->select();


        foreach($result as $r) {
           // if($r['type']!=1) continue;
            $r['str_manage'] = '<a data-title="维护菜单" data-modal="'.url('types/add',array( 'parentid' => $r['id'])).'">'.L('addrec').'</a> ';
            $r['str_manage'] .= '| <a data-title="添加菜单" data-modal="'.url('types/edit',array( 'id' => $r['id'])).'">'.L('edit').'</a> ';
            $r['str_manage'] .= '| <a href="javascript:confirm_delete(\'delect_acion\',\''.url('types/delete',array( 'id' => $r['id'])).'\')">'.L('delete').'</a> ';
            $r['status'] ? $r['status']='<font color="green">'.L('enable').'</font>' : $r['status']='<font color="red">'.L('disable').'</font>' ;
            $array[] = $r;
        }

        $str  = "<tr>					
					<td width='40' align='center'><input name='sort[\$id]' type='text' size='2' value='\$sort'></td>
					<td align='center'>\$id</td>
					<td >\$spacer\$name</td>
					<td align='center'>\$status</td>
					<td align='center'>\$str_manage</td>
				</tr>";

        // prt($array);
        $tree = new treeClass ($array);
        // prt($tree);
        $tree->icon = array('&nbsp;&nbsp;&nbsp;'.L('tree_1'),'&nbsp;&nbsp;&nbsp;'.L('tree_2'),'&nbsp;&nbsp;&nbsp;'.L('tree_3'));
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';
        $select_categorys = $tree->get_tree(0, $str);
        $this->assign('select_categorys', $select_categorys);
        $this->assign('controller_name', 'system_types');
        echo  $this->fetch();
    }

    /**
     * 添加菜单
     * @return array|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     */
    public function add()
    {
        // prt($_REQUEST);
        return $this->_form($this->table, 'form');
    }

    /**
     * 编辑菜单
     * @return array|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     */
    public function edit()
    {
        return $this->_form($this->table, 'form');
    }

    /**
     * 表单数据前缀方法
     * @param array $vo
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    protected function _form_filter(&$vo)
    {
        // prt($vo);//empty
        if ($this->request->isGet()) {
            // 上级菜单处理
           // $_menus = Db::name($this->table)->where(['status' => '1'])->order('sort asc,id asc')->select();
            $_menus = Db::name($this->table)->order('sort asc,id asc')->select();
            // prt($_menus);
            $_menus[] = ['name' => '顶级菜单', 'id' => '0', 'parentid' => '-1'];
            $menus = ToolsService::arr2table($_menus,'id', 'parentid');
            // prt($menus);
            foreach ($menus as $key => &$menu) {
                // if (substr_count($menu['path'], '-') > 3) {
                //     unset($menus[$key]);
                //     continue;
                // }
                if (isset($vo['parentid'])) {
                    $current_path = "-{$vo['parentid']}-{$vo['id']}";
                    if ($vo['parentid'] !== '' && (stripos("{$menu['path']}-", "{$current_path}-") !== false || $menu['path'] === $current_path)) {
                        unset($menus[$key]);
                        // continue;
                    }
                }
            }
            // prt($menus);
            // 读取系统功能节点
//            $nodes = NodeService::get();
//            foreach ($nodes as $key => $node) {
//                if (empty($node['is_menu'])) {
//                    unset($nodes[$key]);
//                }
//            }
            // 设置上级菜单
            if (!isset($vo['parentid']) && $this->request->get('parentid', '0')) {
                $vo['parentid'] = $this->request->get('parentid', '0');
            }

            $this->assign(['nodes' => array_column($nodes, 'node'), 'menus' => $menus]);
        }
    }

    public function _form_result() {

    }

    public function delete()
    {

        $model = DB::table($this->table);
        $pk = $model->getPk();
        $id= intval($_REQUEST['id']);
        if (false==$id) {
            $this->error("参数错误");
        }
        $count = $model->where("parentid=$id")->count();
        if ($count>0) {
            $this->error("该数据下面有子层，请先删除子层数据");
        }

        DB::table($this->table)->where("id={$id}")->delete();

        $this->success(L('do_ok'));
    }


}