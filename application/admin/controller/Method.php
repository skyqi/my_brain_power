<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/24
 * Time: 8:42
 */

namespace app\admin\controller;
use controller\BasicAdmin;
use service\DataService;
use service\NodeService;
use service\ToolsService;
use think\Db;
use think\Cache;
use org\FieldFromClass;
use org\ContentList;
use org\QueryFrom;
use think\Exception;
use think\Request;
// 碎块性临时回调事件处理
class Method extends BasicAdmin
{

    // extend/controller/BasicAdmin.php:325
    public function teacher_before_update() {

        if ($_POST['teacher_position']==77) {
            //校长，大人职位只能一个
            $where[] = ["teacher_position","eq",77];
            if ($_POST['id']>0) {
                $where[] = ["id","neq",$_POST['id']];
            }
            $count = DB::name('teacher')->where($where)->count();

            if ($count>0) {
                $this->error("校长的职位已经存在，不能抢哟");
                exit;
            }
        }

    }
    //教师teacher表更新提交后，回调teacher_after_update
    //向用户登录表插入记录，教师需要登录后台。
    public function teacher_after_update($last_id) {
        if (false==($id=intval($last_id))) {
            return false;
        }
        $s_db = DB::name("teacher");

        $campus_id = $s_db->where("id={$id}")->value("campus_id");

        $count = DB::name("teacher")->where("campus_id={$campus_id}")->count();

        DB::name("campus")->where("id={$campus_id}")->update([
            'teacher_totalnum'=>$count
        ]);
    }

    //extend/controller/BasicAdmin.php:242
    //查询后的事件
    public function teacher_after_list($data1,$data2) {

    }



    //students-stopclass
//    public function students-stopclass_before_edit() {
//        prt($_POST);
//    }
//
//    public function students-stopclass_after_update() {
//        prt($_POST);
//    }


    //将campus记录作废
    public function campusinvalid() {
        $id = intval($_REQUEST['id']);

        if (false==$id) return false;
        $campusDb = DB::name("campus");
        $campusRes = $campusDb->find($id);
        $status = $campusRes['status']==1?0:1;
        $res = $campusDb->where("id={$id}")->update(["status"=>$status]);

        echo $status;

    }



}