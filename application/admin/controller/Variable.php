<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2017 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://think.ctolog.com
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zoujingli/ThinkAdmin
// +----------------------------------------------------------------------

namespace app\admin\controller;

use controller\BasicAdmin;
use service\DataService;
use service\LogService;
use think\Db;

/**
 * 后台参数配置控制器
 * Class Variable
 * @package app\admin\controller
 * @author
 */
class Variable extends BasicAdmin
{

    public function __construct ()
    {
        parent::__construct();
        $this->url = url("/admin/{$this->controller_name}/index", ['spm' => $_REQUEST['spm'], 'runtime' => time()]);  // 放在前面组装
    }

    /**
     * 当前默认数据模型
     * @var string
     */
    public $table = 'system_variable';

    /**
     * 当前页面标题
     * @var string
     */
    public $title = '系统参数配置';

    /**
     * 显示系统常规配置 tab_list , 分页怎么同时存在
     * @return string
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function index ()
    {
        if ($this->request->isGet()) {

            $tab_displaynames = Db::table('system_variable_group')->order(['sort'=>'desc', 'id'=>'asc'])->column('display_name','id');

            // prt($_GET,1);
            $group_id = isset($_REQUEST['group_id']) &&  $_REQUEST['group_id']!=false ?  $_REQUEST['group_id'] : 1;
            $this->assign("group_id", $group_id);
            $db = Db::table('system_variable')->where("group_id=".$group_id)->order('id desc');
            $this->rows = 30;
            $variableList = $this->_list($db, 1, 0);

            return $this->fetch('', ['title' => $this->title,  'variableList' => $variableList, 'tab_displaynames'=>$tab_displaynames]);

        }
        if ($this->request->isPost()) {
            $postData = $this->request->post();
            foreach ($postData as $key => $vo) {
                if ($key != 'group_id') {
                    sysconf($key, $vo, $postData['group_id']);
                }
            }
            LogService::write('系统管理', '系统参数配置成功');
            $this->success('系统参数配置成功！', '');
        }
    }


    public function group2Load ()
    {

        if ($this->request->isPost()) {
            $postData = $this->request->post();
            foreach ($postData as $key => $vo) {
                if ($key != 'group_id') {
                    sysconf($key, $vo, $postData['group_id']);
                }
            }

            LogService::write('系统管理', '系统参数配置成功');
            $this->success('系统参数配置成功！', '');
        }

        $db = Db::table('system_variable')->where("group_id=2")->order('id desc');
        $this->rows = 2;
        $group2List = $this->_list($db, 1, 0);



        return $this->fetch('group2List', ['title' => $this->title,  'group2List' => $group2List]);


    }

    public function group3Load ()
    {

        if ($this->request->isPost()) {
            $postData = $this->request->post();
            foreach ($postData as $key => $vo) {
                if ($key != 'group_id') {
                    sysconf($key, $vo, $postData['group_id']);
                }
            }

            LogService::write('系统管理', '系统参数配置成功');
            $this->success('系统参数配置成功！', '');
        }

        $db = Db::table('system_variable')->where("group_id=3")->order('id desc');
        $this->rows = 2;
        $group3List = $this->_list($db, 1, 0);
        // prt($group3List['list']);

        return $this->fetch('group3List', ['title' => $this->title,  'group3List' => $group3List]);


    }

    /**
     * 文件存储配置
     * @return string
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function file ()
    {
        $this->title = '文件存储配置';

        return $this->index();
    }

    public function add ()
    {
        if ($this->request->isGet()) {
            $tab_displayname = Db::table('system_variable_group')->order(['sort'=>'desc', 'id'=>'asc'])->column(['id','display_name']);
            $this->assign('tab_displayname',$tab_displayname);
        }
        if ($_REQUEST['group_id']) {
            $data = ['group_id' => $_REQUEST['group_id']];
        }

 

        if ($_POST['type_id']==3) {
            if (!is_numeric($_POST['value'])) {
                $this->result($_POST['value'],1,'参数值类型错误');
            }
        }

        if ($this->request->isPost()) {
            $postData = $this->request->post();
            if(DataService::save(Db::table($this->table), $postData)){
                $this->success('参数添加成功！', '');
            }else{
                $this->error('参数添加失败！', '');

            }
        }
 
        return $this->_form($this->table, 'form', '', '', $data);
    }

    // 添加分组
    public function addGroup ()
    {
        if($_REQUEST['display_name']){
            $data['display_name'] = $_REQUEST['display_name'];
        }

        if ($this->request->isPost()) {
            $postData = $this->request->post();
            $data['display_name'] =  $postData['display_name'];
            if(DataService::save(Db::table('system_variable_group'), $data)){
                $this->success('参数分组添加成功！', '');
            }else{
                $this->error('参数分组添加失败！', '');

            }
        }

        return $this->_form(Db::table('system_variable_group'), 'groupForm', '', '', $data);
    }




    //删除参数
    public function del ()
    {
        if (!$_REQUEST['id']) {
            $this->error('缺少删除id');
        }
        if (!(Db::table('system_variable')->where("id=" . trim($_REQUEST['id']))->count() > 0)) {
            $this->error('参数不存在');
        }
        if (Db::table('system_variable')->where("id=" . trim($_REQUEST['id']))->delete()) {
            $this->success("删除成功！", '');
        }
        $this->error("删除失败，请稍候再试！");
    }

    //查看分组
    public function readGroup ()
    {

        $tab_display_name = Db::table('system_variable_group')->order('id')->field(['id','display_name','sort'])->select();

        return $this->fetch('readGroup', ['tab_display_name'=>$tab_display_name]);


        return $this->_form(Db::table('system_variable_group'), 'groupForm', '', '', $data);
    }


    // 添加分组
    public function editGroup ()
    {
        if($_REQUEST['id']){
            $data['id'] = $_REQUEST['id'];
        }

        if ($this->request->isPost()) {
            $postData = $this->request->post();
            $data['display_name'] =  $postData['display_name'];
            if(DataService::save(Db::table('system_variable_group'), $data,'id')){
                $this->success('参数分组修改成功！', '');
            }else{
                $this->error('参数分组修改失败！', '');

            }
        }

        return $this->_form(Db::table('system_variable_group'), 'groupForm', '', '', $data);
    }

    //删除参数分组
    public function delGroup ()
    {
        if (!$_REQUEST['id']) {
            $this->error('缺少删除id');
        }
        if (!(Db::table('system_variable_group')->where("id=" . trim($_REQUEST['id']))->count() > 0)) {
            $this->error('分组不存在');
        }
        if (Db::table('system_variable_group')->where("id=" . trim($_REQUEST['id']))->delete()) {
            $this->success("删除成功！", '');
        }
        $this->error("删除失败，请稍候再试！");
    }


    public function email ()
    {
        if ($_POST) {
            $data = $_POST;
            foreach ($data as $key => $val) {
                $res = Db::name('system_variable')->where(['name' => $key])->update(['value' => $val]);
            }
            $this->success('保存成功', '');

        } else {
            $this->title = '邮箱配置';
            $data = Db::name('system_variable')->where('group_id', 3)->column(['name', 'value']);

            return $this->fetch('email', ['vo' => $data]);
        }
    }

    //邮箱测试
    public function testmail ()
    {
        $subject = 'test 邮箱发送测试';
        $mailto = $_POST['mail_sendmail'];
        $message = sysconf('site_name') . 'test mail';
        $r = sendmail($mailto, $subject, $message, $_POST);
        if ($r == true) {
            $this->result($r, 1, '发送邮件成功');
        } else {
            $this->result($r, 0, '发送邮件失败');
        }
    }


}
