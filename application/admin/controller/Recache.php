<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2017 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://think.ctolog.com
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zoujingli/ThinkAdmin
// +----------------------------------------------------------------------

namespace app\admin\controller;

use controller\BasicAdmin;
use org\FieldFromClass;
use org\CreateFromControl;
use service\DataService;
use service\NodeService;
use service\ToolsService;
use think\App;
use think\Db;


use think\exception\ErrorException;
use think\Loader;
use think\Exception;
/**
 * 重建缓存
 * Class Admincache
 * @package app\admin\controller
 * @author Anyon <zoujingli@qq.com>
 * @date 2017/02/15 10:41
 */
class Recache extends BasicAdmin
{

    public function __construct()
    {
        // parent::__construct();
    }

    //清空缓存
    public function index(){

        //     try {
        //  $this->distpickerCreate();
        //Fields

        cache('system_field_cache', NULL); //清除
        cache('system_module_cache', NULL); //清除
        \think\Facade\Cache::clear();
        
        $modules = Db::name('SystemModule')->select();
        if (false==$modules) return false;

       // $no_cache_model_lists = [] ;
        if($modules != false){
            foreach ($modules as $kk=>$vv){
                //写入
                $mo_uuid = trim($vv['mo_uuid']);

                $res = DB::table("system_field")->where("mo_uuid='$mo_uuid'")->order("sort asc")->select();
                $newRes = [];
                foreach ($res as $v) {
                    $pkey = $v['field'];
                    $newRes[$pkey] = $v;
                }

                if ($newRes) {
                    F(strtolower($vv['tb_name']), $newRes);
                    $this->writeln(strtolower($vv['tb_name']) . ",  生成缓存...");
                    unset($newRes);
                }

            }
        }


        // sysconf($key, $vo)
        if (sysconf('Lang_mark_status')>0) {
            $this->trans();
        }
        $this->writeln( "--------完成--------");
        echo "<br> <br> <br> <br>";
        exit;


    }


    public function index_del(){

        //     try {
      //  $this->distpickerCreate();
        //Fields

        unlink(RUNTIME_PATH . "cache");
        $modules = Db::name('SystemModule')->select();
        $no_cache_model_lists = [] ;
        if($modules != false){
            foreach ($modules as $kk=>$vv){
                //写入
                $id = intval($vv['id']);
                $res = DB::table("system_field")->where("moduleid={$id}")->order("sort asc")->select();
                $newRes = [];
                foreach ($res as $v) {
                    $pkey = $v['field'];
                    $newRes[$pkey] = $v;
                }

                if ($newRes) {

                    F(strtolower($vv['name']) . "_Field.php", $newRes);
                    $this->writeln(strtolower($vv['name']) . "_Field.php". "  生成缓存...");
                    unset($newRes);
                }
                if ($modules['hascache']  ==1)  $no_cache_model_lists[] = $modules['name'] ;
            }
        }

        $tmp_tables = Db::query("SHOW TABLES");
        foreach ($tmp_tables as $k=>$v) {
            foreach ($v as $k2=>$v2) {
                $tables[] = $v2;
            }
        }

        //缓存表


        foreach ($tables as $k=>$v){

            if (!in_array($v,$no_cache_model_lists)) {
                $tb_key = Db::table($v)->getPk();
                if (false== $tb_key) continue ;
                $res = DB::table($v)->order("$tb_key desc")->select();   //用于后台查询，不需要排序
                foreach ($res as $kk => $vv) {
                    $table_arr[$vv[$tb_key]] = $vv;
                }
                F($v, $table_arr);
                $this->writeln(strtolower($v). "  生成缓存...");
                unset($table_arr);
            }
        }

        //cacheAttachment
        $attachments = Db::name('SystemAttachment')->where('status=1')->select();
        if (is_array($attachments) && count($attachments)>0){
            $attachments_arr = array();
            foreach ($attachments as $k=>$v){
                if($v['table']!=false){
                    $attachments_arr[$v['table']][$k] = $v;
                }
            }
            foreach ($attachments_arr as $k=>$v){
                F($k."_attachmentCache",$v);
                $this->writeln($k."_attachmentCache". "  生成缓存...");
            }
        }
        // sysconf($key, $vo)
        if (sysconf('Lang_mark_status')>0) {
            $this->trans();
        }
        $this->writeln( "--------完成--------");
        echo "<br> <br> <br> <br>";
        exit;


    }

    // http://door.mydanweb.com/admin/recache/trans.html
    // 翻译包
    public function trans()
    {
        $LangsPack = config('config.Trans_LangsPack');

        if (false==$LangsPack) return false;
        $trans = new Baidu_transapi();
        $from_lang = "zh";
        $to_langs =  config('config.Trans_langs');

        Db::execute("TRUNCATE TABLE  `langtrans`");
        foreach ($to_langs as $to_lang) {
            foreach ($LangsPack as $k => $v) {
                $trans_arr = $trans->translate($v, $from_lang, $to_lang);
                $trans_dst = trim($trans_arr['trans_result'][0]['dst']);
                $res = DB::name("langtrans")->insert(array('trans_src' => $v, 'to' => $to_lang, 'trans_dst' => $trans_dst));
            }
        }


        foreach ($to_langs as $to_lang) {
            $filename = "langtrans";
            $langRes[$to_lang] = DB::name("langtrans")->where(['to' => $to_lang])->column("trans_dst", 'trans_src');
        }

        F($filename,$langRes);
        $this->writeln( "langtrans 多语言包". "  生成缓存...");
    }


    public function  writeln($cn) {
        echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;" .$cn. "<br>";
    }


}