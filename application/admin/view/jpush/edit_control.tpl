<!--************************************************-->
**  使用范围：用于编辑的表单，自动循环整个field表单
**  功能：是每个字段定制开发
**  控制器请参考：http://note.youdao.com/noteshare?id=1fbb3378ad68e969adea106f01415794
**
<!--************************************************-->
{extend name='admin@public/content'}


{block name="style"}
{/block}
{block name="content"}
<!--   view/content/edit_control.html  -->
<form name="myform" id="myform" class="bootstrap-frm y_queryform ajaxform" action="{if condition="$controller_name=='add'"}{:url($controller_name.'/insert')}{else/}{:url($controller_name.'/update')}{/if}" method="post">

<table cellpadding=0 cellspacing=0 class="table_form" width="100%">
        <col align="left" width="10%"  />
        <col />
        <tpm></tpm>

    {if condition="(false==$vo[$pk])"}
    <input type="hidden" name="createtime" value="{:time()}" />
    {else}
    <input type="hidden" name="updatetime" value="{:time()}" />
    {/if}

</table>

<div id="bootline"></div>
<div id="btnbox y_wtc" class="btn" style="width: 100%;">
    {if condition="($vo[$pk]>0)"}
        <input type="hidden" name="{$pk}" value="{:$vo[$pk]}" />
    {/if}

    <input type="hidden" name="forward" value="{:$forward?$forward:$_SERVER['HTTP_REFERER']}" />
    <INPUT TYPE="submit"  value="{:L('dosubmit')}"  class="layui-btn layui-btn-normal" >
    <input TYPE="reset"  value="{:L('cancel')}" class="layui-btn layui-btn-primary " onclick="window.history.back(-1);">
</div>
</form>


{/block}