<!--************************************************-->
**  使用范围：用于查看记录的明细项
**  功能：是每个字段定制开发
**  控制器请参考：http://note.youdao.com/noteshare?id=1fbb3378ad68e969adea106f01415794
**
<!--************************************************-->

{extend name='admin@public/content'}

<!-- 右则内容区域 开始 -->
{block name="style"}
{/block}

{block name="content"}

    <div class="table-list">
        <div class="left-list">
            <table  class="layui-table" lay-filter="demo">

                <tbody>
                    {foreach name="tbField" item="f" key="key2" }
                    {if condition="($f.status>0 && !in_array($f.field,array($pk,'sort')) && $f.detailfield>0)" }
                    <tr>
                    <th>{$f.name}</th> <td>{$vo[$f.field]|$ContentList::index=###,$f|raw}</td>
                    </tr>
                    {/if}
                    {/foreach}
                </tbody>

            </table>
        </div>
    </div>


<p style="margin-top: 10px;">

<div style="">
    <a href="javascript:;"  class="layui-btn layui-btn-normal closepopwin" >关闭</a>
 </div>

</p>

{/block}

{block name="script"}
{/block}