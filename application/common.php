<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2017 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://think.ctolog.com
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zoujingli/ThinkAdmin
// +----------------------------------------------------------------------

use service\DataService;
use service\NodeService;
use think\Db;
use think\Url;
use think\Request;
use org\CreateFromControl;
// use PHPExcel_IOFactory;
use PHPExcel as PHPExcel;
use org\PHPMailer;

// use PHPExcel;

error_reporting(E_ERROR | E_PARSE);


/**
 * 打印输出数据到文件
 * @param mixed $data 输出的数据
 * @param bool $force 强制替换
 * @param string|null $file
 */
function p ($data, $force = false, $file = null)
{
    is_null($file) && $file = env('runtime_path') . date('Ymd') . '.txt';
    $str = (is_string($data) ? $data : (is_array($data) || is_object($data)) ? print_r($data, true) : var_export($data, true)) . PHP_EOL;
    $force ? file_put_contents($file, $str) : file_put_contents($file, $str, FILE_APPEND);
}

/**
 * RBAC节点权限验证
 * @param string $node
 * @return bool
 */
function auth ($node)
{
    return NodeService::checkAuthNode($node);
}

/**
 * 设备或配置系统参数
 * @param string $name 参数名称
 * @param bool $value 默认是null为获取值，否则为更新
 * @return string|bool
 * @throws \think\Exception
 * @throws \think\exception\PDOException
 */
function sysconf ($name, $value = null)
{
    static $config = [];
    if ($value !== null) {
        list($config, $data) = [[], ['name' => $name, 'value' => $value]];

        return DataService::save('system_variable', $data, 'name');
    }
    if (empty($config)) {
        $config = Db::name('system_variable')->column('name,value');
    }

    return isset($config[$name]) ? $config[$name] : '';
}

/**
 * 设备或配置系统参数
 * @param string $name 参数名称
 * @param bool $value 默认是null为获取值，否则为更新
 * @return string|bool
 * @throws \think\Exception
 * @throws \think\exception\PDOException
 */
function sysvar ($name, $value = null)
{
    static $config_var = [];
    if ($value !== null) {
        list($config_var, $data) = [[], ['name' => $name, 'value' => $value]];

        return DataService::save('system_variable', $data, 'name');
    }
    if (empty($config_var)) {
        $config_var = Db::name('system_variable')->column('name,value, group_id');
    }

    return isset($config_var[$name]) ? $config_var[$name] : '';
}

/**
 * 日期格式标准输出
 * @param string $datetime 输入日期
 * @param string $format 输出格式
 * @return false|string
 */
function format_datetime ($datetime, $format = 'Y年m月d日 H:i:s')
{
    return date($format, strtotime($datetime));
}

/**
 * UTF8字符串加密
 * @param string $string
 * @return string
 */
function encode ($string)
{
    list($chars, $length) = ['', strlen($string = iconv('utf-8', 'gbk', $string))];
    for ($i = 0; $i < $length; $i++) {
        $chars .= str_pad(base_convert(ord($string[$i]), 10, 36), 2, 0, 0);
    }

    return $chars;
}

/**
 * UTF8字符串解密
 * @param string $string
 * @return string
 */
function decode ($string)
{
    $chars = '';
    foreach (str_split($string, 2) as $char) {
        $chars .= chr(intval(base_convert($char, 36, 10)));
    }

    return iconv('gbk', 'utf-8', $chars);
}

/**
 * 下载远程文件到本地
 * @param string $url 远程图片地址
 * @return string
 */
function local_image ($url)
{
    return \service\FileService::download($url)['url'];
}


function prt ($vars, $die = 1)
{

    if (!(preg_match("/221.233/Ui", getIP()) || preg_match("/148.56/Ui", getIP()) || preg_match("/113.88/Ui", getIP()) || preg_match("/127.0/Ui", getIP()) || getIP() == "::1")) {
        return false;
    }

    $backtrace = debug_backtrace();
    $caller = isset($backtrace[0]) ? $backtrace[0] : '';
    if (isset($caller['file']) && !empty($caller['file'])) {
        @$fp = fopen($caller['file'], 'r');
        for ($i = 0; $i < $caller['line']; $i++) $line = fgets($fp);
        @fclose($fp);
        preg_match('/' . __FUNCTION__ . '.*?\((.*?)\)\;/', $line, $matches);
        $varname = isset($matches[1]) ? ($matches[1]) : 'sys-var_name_err';

        echo "<h5> Filename:" . $backtrace[0]['file'] . "</h5>";  //文件名
        echo "Line:" . $backtrace[0]['line'] . "<br>";
        echo "<div align=left><pre>\n##{$varname}##" . htmlspecialchars(print_r($vars, true)) . "<br>============debug end ==================\n</pre></div>\n";

    }
    if ($die) die;

}


function getIP ()
{
    static $realip;
    if (isset($_SERVER)) {
        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else if (isset($_SERVER["HTTP_CLIENT_IP"])) {
            $realip = $_SERVER["HTTP_CLIENT_IP"];
        } else {
            $realip = $_SERVER["REMOTE_ADDR"];
        }
    } else {
        if (getenv("HTTP_X_FORWARDED_FOR")) {
            $realip = getenv("HTTP_X_FORWARDED_FOR");
        } else if (getenv("HTTP_CLIENT_IP")) {
            $realip = getenv("HTTP_CLIENT_IP");
        } else {
            $realip = getenv("REMOTE_ADDR");
        }
    }


    return $realip;
}


function consoleLog ($val)
{
    $debug = debug_backtrace();
    unset($debug[0]['args']);
    echo '<script> try{',
        'console.log(' . json_encode(str_repeat("~~~", 40)) . ');',
        'console.log(' . json_encode($debug[0]) . ');',
        'console.log(' . addslashes(json_encode($val)) . ');',
        'console.log(' . json_encode(str_repeat("~~~", 40)) . ');',
    '}catch(e){}</script>';
}




function savelog ($cn, $filename = false)
{

    $path = ROOT_DIR . "/runtime/phpdebug";
    @mkdir($path, 0777, true);

    $backtrace = debug_backtrace();

    if (false == $filename) {
        $filename = basename($backtrace[0]['args'][0][0]['url']);
    }
    $line = $backtrace[0]['line'];

    if (false == $filename) {
        $filename = $path . "/log_" . date("Y-m-d") . ".logs";
    } else {
        $filename = $path . "/" . $filename . "_" . date("Y-m-d") . ".logs";
    }

    if (empty($cn)) {
        $cn = "php未取得值!";
    }

    if (is_array($cn)) {
        $cn = json_encode($cn);
    }


    $key = $backtrace[0]['args'][0];
    if (false == $key) {
        $key = "no find field";
    }
    $class = $backtrace[1]['class'];
    $function = $backtrace[1]['function'];
    $title = date("Y-m-d H:i:s") . "::file={$filename};class={$class};function={$function};" . PHP_EOL;
    error_log($title, 3, $filename);
    $cn = date("Y-m-d H:i:s") . "::line={$line};{$key}=" . $cn . PHP_EOL . PHP_EOL;
    error_log($cn, 3, $filename);

}

// isCheckDataTime('2017-01-02 23:23:59') 返回1
function isCheckDataTime($date)
{
    if (false == $date) return false;
    if (preg_match("/[0-9][0-9]:[0-9][0-9]:/",$date)) {
        $fmt = 'Y-m-d H:i:s';
    } else {
        $fmt = 'Y-m-d';
    }

    return date($fmt, strtotime($date)) == $date;

}

// 转换任何时间为时间戳
function toTimestamp ($date)
{
    if (false == $date) return false;
    if (isCheckDataTime($date)) {
        return strtotime($date);
    } else {
        return $date;
    }

}



function string2array ($info)
{
    if ($info == '') return array();
    if (is_array($info)) return $info;
    $info = stripcslashes($info);
    
    eval("\$r = $info;");

    return $r;
}

function array2string ($info)
{
    if ($info == '') return '';

    if (!is_array($info)) $string = stripslashes($info);
    foreach ($info as $key => $val) $string[$key] = stripslashes($val);
    $c = var_export($string, true);

    return ($c);
}

/**
 * +----------------------------------------------------------
 * 产生随机字串，可用来自动生成密码
 * 默认长度6位 字母和数字混合 支持中文
 * +----------------------------------------------------------
 * @param string $len 长度
 * @param string $type 字串类型
 * 0 字母 1 数字 其它 混合
 * @param string $addChars 额外字符
 * +----------------------------------------------------------
 * @return string
 * +----------------------------------------------------------
 */
function rand_string ($len = 6, $type = '', $addChars = '')
{
    $str = '';
    switch ($type) {
        case 0 :
            $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' . $addChars;
            break;
        case 1 :
            $chars = str_repeat('0123456789', 3);
            break;
        case 2 :
            $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' . $addChars;
            break;
        case 3 :
            $chars = 'abcdefghijklmnopqrstuvwxyz' . $addChars;
            break;
        default :
            // 默认去掉了容易混淆的字符oOLl和数字01，要添加请使用addChars参数
            $chars = 'ABCDEFGHIJKMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789' . $addChars;
            break;
    }
    if ($len > 10) { //位数过长重复字符串一定次数
        $chars = $type == 1 ? str_repeat($chars, $len) : str_repeat($chars, 5);
    }
    if ($type != 4) {
        $chars = str_shuffle($chars);
        $str = substr($chars, 0, $len);
    } else {
        // 中文随机字
        for ($i = 0; $i < $len; $i++) {
            $str .= msubstr($chars, floor(mt_rand(0, mb_strlen($chars, 'utf-8') - 1)), 1);
        }
    }

    return $str;
}

function sysmd5 ($str, $key = '', $type = 'sha1')
{
    $key = $key ? $key : config('config.ADMIN_ACCESS');

    return hash($type, $str . $key);
}

function pwdHash ($password, $type = 'md5')
{
    return hash($type, $password);
}

function getform ($form, $info, $value = '')
{
    return $form->$info['type']($info, $value);  //FieldFromClass
}

function getvalidate ($info)
{

    $validate_data = array();
    if ($info['minlength']) $validate_data['minlength'] = ' minlength:' . $info['minlength'];
    if ($info['maxlength']) $validate_data['maxlength'] = ' maxlength:' . $info['maxlength'];
    if ($info['required']) $validate_data['required'] = ' required:true';
    if ($info['pattern']) $validate_data['pattern'] = ' ' . $info['pattern'] . ':true';
    if ($info['errormsg']) $errormsg = ' title="' . $info['errormsg'] . '"';
    $validate = implode(',', $validate_data);
    $validate = $validate ? 'validate="' . $validate . '" ' : '';
    $parseStr = $validate . $errormsg;

    return $parseStr;
}

function L ($value)
{
    return lang($value);
}

//翻译语言包
function _t($value)
{
    //$lang =  input('request.lang/s',config('config.DEFAULT_TRANS_LANG'));
    $lang =  input('request.lang/s');
    if (false==$lang) return $value;
    ////
    if (!in_array($lang,config('config.Trans_langs'))) {
        return 'Undefined language!';
    }
    $langtrans_file  = env('ROOT_PATH') . "module_data/langtrans.php" ;
    $langArr = include($langtrans_file);
    return $langArr[$lang][$value];
}


function F ($tb_name, $ct = false)
{

    if ($ct == false) {
        $count = Db::table("system_field_cache")->where("tb_name='$tb_name'")->count();

        if ($count>0) {
            $cache = Db::table("system_field_cache")->where("tb_name='$tb_name'")->value("content");

            return json_decode($cache, true);
        } else {

//            $res =  Db::query('show tables');
//
//            foreach ($res as $v) {
//                $db_arr[] = $v['Tables_in_naoli_master'];
//            }
//
//            if (!in_array($tb_name,$db_arr)) {
//                exception("数据表不存在，line:".__LINE__);
//            }
//            $pk = Db::table($tb_name)->getPk();
//            $tb_lists = Db::table($tb_name)->column("*",$pk);
//          //  $tb_lists = Db::table($tb_name)->cache($tb_name.'_cache', 60)->column("*",$pk);
//
//            return $tb_lists;

        }

    } else {
        //写入
        $cn = json_encode($ct,JSON_UNESCAPED_UNICODE );
        $data = [];
        $data['tb_name'] = trim($tb_name);
        $data['content'] = $cn;
        DataService::save("system_field_cache",$data,'tb_name');


    }

}

function D ($name)
{
    return DB::name($name);
}

function adminUrl ($url, $param = array())
{
    $param['spm'] = $_REQUEST['spm'];
    $url = url($url, $param);
    list($base, $url) = [('/admin'), $url];

    return "{$base}#{$url}";
}

function toDate ($time, $format = 'Y-m-d H:i:s')
{
    if (empty ($time)) {
        return '';
    }
    $format = str_replace('#', ':', $format);

    return date($format, $time);
}

function byte_format ($input, $dec = 0)
{
    $prefix_arr = array("B", "K", "M", "G", "T");
    $value = round($input, $dec);
    $i = 0;
    while ($value > 1024) {
        $value /= 1024;
        $i++;
    }
    $return_str = round($value, $dec) . $prefix_arr[$i];

    return $return_str;
}

function mk_dir ($dir, $mode = 0777)
{
    if (is_dir($dir) || @mkdir($dir, $mode))
        return true;
    if (!mk_dir(dirname($dir), $mode))
        return false;

    return @mkdir($dir, $mode);
}


/**
 * @param string $string 原文或者密文
 * @param string $operation 操作(ENCODE | DECODE), 默认为 DECODE
 * @param string $key 密钥
 * @param int $expiry 密文有效期, 加密时候有效， 单位 秒，0 为永久有效
 * @return string 处理后的 原文或者 经过 base64_encode 处理后的密文
 *
 * @example
 *
 *  $a = authcode('abc', 'ENCODE', 'key');
 *  $b = authcode($a, 'DECODE', 'key');  // $b(abc)
 *
 *  $a = authcode('abc', 'ENCODE', 'key', 3600);
 *  $b = authcode('abc', 'DECODE', 'key'); // 在一个小时内，$b(abc)，否则 $b 为空
 */
function authcode ($string, $operation = 'DECODE', $key = '', $expiry = 0)
{
    $ckey_length = 4;
    // 随机密钥长度 取值 0-32;
    // 加入随机密钥，可以令密文无任何规律，即便是原文和密钥完全相同，加密结果也会每次不同，增大破解难度。
    // 取值越大，密文变动规律越大，密文变化 = 16 的 $ckey_length 次方
    // 当此值为 0 时，则不产生随机密钥


    $keya = md5(substr($key, 0, 16));
    $keyb = md5(substr($key, 16, 16));
    $keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length) : substr(md5(microtime()), -$ckey_length)) : '';

    $cryptkey = $keya . md5($keya . $keyc);
    $key_length = strlen($cryptkey);

    $string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0) . substr(md5($string . $keyb), 0, 16) . $string;
    $string_length = strlen($string);

    $result = '';
    $box = range(0, 255);

    $rndkey = array();
    for ($i = 0; $i <= 255; $i++) {
        $rndkey[$i] = ord($cryptkey[$i % $key_length]);
    }

    for ($j = $i = 0; $i < 256; $i++) {
        $j = ($j + $box[$i] + $rndkey[$i]) % 256;
        $tmp = $box[$i];
        $box[$i] = $box[$j];
        $box[$j] = $tmp;
    }

    for ($a = $j = $i = 0; $i < $string_length; $i++) {
        $a = ($a + 1) % 256;
        $j = ($j + $box[$a]) % 256;
        $tmp = $box[$a];
        $box[$a] = $box[$j];
        $box[$j] = $tmp;
        $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
    }

    if ($operation == 'DECODE') {
        if ((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26) . $keyb), 0, 16)) {
            return substr($result, 26);
        } else {
            return '';
        }
    } else {
        return $keyc . str_replace('=', '', base64_encode($result));
    }
}


//字符串截取
function str_cut ($sourcestr, $cutlength, $suffix = '...')
{
    $str_length = strlen($sourcestr);
    if ($str_length <= $cutlength) {
        return $sourcestr;
    }
    $returnstr = '';
    $n = $i = $noc = 0;
    while ($n < $str_length) {
        $t = ord($sourcestr[$n]);
        if ($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
            $i = 1;
            $n++;
            $noc++;
        } elseif (194 <= $t && $t <= 223) {
            $i = 2;
            $n += 2;
            $noc += 2;
        } elseif (224 <= $t && $t <= 239) {
            $i = 3;
            $n += 3;
            $noc += 2;
        } elseif (240 <= $t && $t <= 247) {
            $i = 4;
            $n += 4;
            $noc += 2;
        } elseif (248 <= $t && $t <= 251) {
            $i = 5;
            $n += 5;
            $noc += 2;
        } elseif ($t == 252 || $t == 253) {
            $i = 6;
            $n += 6;
            $noc += 2;
        } else {
            $n++;
        }
        if ($noc >= $cutlength) {
            break;
        }
    }
    if ($noc > $cutlength) {
        $n -= $i;
    }
    $returnstr = substr($sourcestr, 0, $n);


    if (substr($sourcestr, $n, 6)) {
        $returnstr = $returnstr . $suffix;//超过长度时在尾处加上省略号
    }

    return $returnstr;
}


// 自动转换字符集 支持数组转换
function auto_charset ($fContents, $from = 'gbk', $to = 'utf-8')
{
    $from = strtoupper($from) == 'UTF8' ? 'utf-8' : $from;
    $to = strtoupper($to) == 'UTF8' ? 'utf-8' : $to;
    if (strtoupper($from) === strtoupper($to) || empty($fContents) || (is_scalar($fContents) && !is_string($fContents))) {
        //如果编码相同或者非字符串标量则不转换
        return $fContents;
    }
    if (is_string($fContents)) {
        if (function_exists('mb_convert_encoding')) {
            return mb_convert_encoding($fContents, $to, $from);
        } elseif (function_exists('iconv')) {
            return iconv($from, $to, $fContents);
        } else {
            return $fContents;
        }
    } elseif (is_array($fContents)) {
        foreach ($fContents as $key => $val) {
            $_key = auto_charset($key, $from, $to);
            $fContents[$_key] = auto_charset($val, $from, $to);
            if ($key != $_key)
                unset($fContents[$key]);
        }

        return $fContents;
    } else {
        return $fContents;
    }
}


function fieldoption ($setup_options, $value = null, $space = '')
{
    $options = explode("\n", $setup_options);
    foreach ($options as $r) {
        $v = explode("|", $r);
        $k = trim($v[1]);
        $optionsarr[$k] = $v[0];
    }
    if (isset($value)) {
        if (strpos($value, ',')) {
            $value = explode(",", $value);
            $data = array();
            foreach ((array)$value as $val) {
                $data[] = $optionsarr[$val];
            }
            if ($space != '') {
                return implode(stripcslashes($space), $data);
            } else {
                return $data;
            }
        } else {
            return $optionsarr[$value];
        }
    } else {
        return $optionsarr;
    }
}

function picstoarr ($str = '')
{
    if (empty($str)) return false;
    $data = array();
    $v = explode(":::", $str);
    foreach ((array)$v as $r) {
        $r = explode('|', $r);
        $res['file'] = $r[0];
        $res['desc'] = $r[1];
        $data[] = $res;
    }

    return $data;
}



function getAttachment ($aids = 0, $type)
{
    $appRoot = \request()->root(true);
    if (false == $aids) return false;
    $aid = $aids;
    if (is_array($aids)) $aid = implode(",", $aids);
    $res = DB::name("system_attachment")->cache(true, 60)->where("aid in ({$aid}) and status=1")->order("sort asc")->select();

    foreach ($res as $k => &$v) {
        if ($v['isimage'] == 1) {
            $filename = getImagePathFile($v['filename']);
            foreach ($filename as $kk => $vv) {
                $v['imagepath'][$kk] = $appRoot . (str_replace("./", '/', $v['filepath'])) . $vv;
            }
        } else {
            $v['filepath'] = $appRoot . (str_replace("./", '/', $v['filepath'])) . $v['filename'];
        }
    }
    if (false==$type) return $res;
    $row = $res[0];
    return getImagePathFile ($row['filepath'].$row['filename'], $type);

}


function getImagePathFile ($filename, $type)
{

    $path_parts = pathinfo($filename);

    $filename = $path_parts["filename"];
    $extension = $path_parts["extension"];
    $pathfile['s'] = "{$filename}_s.{$extension}";
    $pathfile['m'] = "{$filename}_m.{$extension}";
    $pathfile['b'] = "{$filename}_b.{$extension}";
    $pathfile['thumb'] = "thumb_{$filename}.{$extension}";
    if ($type) {
       return $pathfile[$type] ;
    }
    return $pathfile;
}


//将数组组织成树状
function genTree ($items, $id = 'typeid', $pid = 'parentid', $son = 'children')
{
    $tree = array(); //格式化的树
    $tmpMap = array();  //临时扁平数据

    foreach ($items as $item) {
        $tmpMap[$item[$id]] = $item;
    }

    foreach ($items as $item) {
        if (isset($tmpMap[$item[$pid]])) {
            $key2 = $tmpMap[$item[$id]][$id];

            $tmpMap[$item[$pid]][$son][$key2] = &$tmpMap[$item[$id]];
        } else {
            $tree[$item[$id]] = &$tmpMap[$item[$id]];
        }
    }
    unset($tmpMap);

    return $tree;
}

function select ($msg_list, $attr, $debug)
{
    $arr_list = $msg_list;
    if (!is_array($arr_list)) return "";
    $str_return = "";
    $multiple = '';
    $size = '';
    $id = '';
    $name = '';
    $class = '';
    $select_val = '';
    $disabled = '';
    if ($attr['multiple']) $multiple = ' multiple="multiple" ';
    if ($attr['size']) $size = " size='" . $attr['size'] . "'";
    if ($attr['name']) $name = " name='" . $attr['name'] . "'";
    if ($attr['id']) $id = " id='" . $attr['id'] . "'";
    if ($attr['class']) $class = " class='" . $attr['class'] . "'";
    if (!empty($attr['select_val'])) $select_val = $attr['select_val'];
    if (!empty($attr['disabled'])) $disabled = " disabled='true' ";
    if ($attr['option_null']) {
        $str_return = "<option value='0'>请选择</option>";
    }
    //prt($select_val);
    foreach ($arr_list as $k => $item) {
        $str_sel = "";
        if (false != $select_val && $k == $select_val) $str_sel = " selected ";
        $str_return .= "<option value='" . $k . "' " . $str_sel . ">" . $item . "</option>";
    }
    $_select = "<select {$multiple} {$size} {$name} {$id} {$class} {$disabled}>";

    return $_select . $str_return . "</select>";
}


/**
 * 导出数据为excal文件
 *
 * @param unknown $expTitle
 * @param unknown $expCellName
 * @param unknown $expTableData
 */
function dataExcel ($expTitle, $expCellName, $expTableData)
{
    $xlsTitle = iconv('utf-8', 'gb2312', $expTitle); // 文件名称
    $fileName = $expTitle . date('_YmdHis'); // or $xlsTitle 文件名称可根据自己情况设定
    $cellNum = count($expCellName);
    $dataNum = count($expTableData);
    $objPHPExcel = new \PHPExcel();
    $cellName = array(
        'A',
        'B',
        'C',
        'D',
        'E',
        'F',
        'G',
        'H',
        'I',
        'J',
        'K',
        'L',
        'M',
        'N',
        'O',
        'P',
        'Q',
        'R',
        'S',
        'T',
        'U',
        'V',
        'W',
        'X',
        'Y',
        'Z',
        'AA',
        'AB',
        'AC',
        'AD',
        'AE',
        'AF',
        'AG',
        'AH',
        'AI',
        'AJ',
        'AK',
        'AL',
        'AM',
        'AN',
        'AO',
        'AP',
        'AQ',
        'AR',
        'AS',
        'AT',
        'AU',
        'AV',
        'AW',
        'AX',
        'AY',
        'AZ',
    );
    for ($i = 0; $i < $cellNum; $i++) {
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[$i] . '2', $expCellName[$i][1]);
    }
    for ($i = 0; $i < $dataNum; $i++) {
        for ($j = 0; $j < $cellNum; $j++) {
            $objPHPExcel->getActiveSheet(0)->setCellValue($cellName[$j] . ($i + 3), $expTableData[$i][$expCellName[$j][0]]);
        }
    }
    $objPHPExcel->setActiveSheetIndex(0);
    header('pragma:public');
    header('Content-type:application/vnd.ms-excel;charset=utf-8;name="' . $xlsTitle . '.xls"');
    header("Content-Disposition:attachment;filename=$fileName.xls"); // attachment新窗口打印inline本窗口打印
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
}


function stripslashes_array($array) {
    if(!is_array($array)) return stripslashes($array);
    foreach($array as $k => $val) $array[$k] = stripslashes_array($val);
    return $array;
}

function htmlspecialchars_array($array) {
    if(!is_array($array)) return htmlspecialchars($array,ENT_QUOTES);
    foreach($array as $k => $val) $array[$k] = htmlspecialchars_array($val);
    return $array;
}

function safe_replace($string) {
    $string = trim($string);
    $string = str_replace(array('\\',';','\'','%2527','%27','%20','&', '"', '<', '>'), array('','','','','','','&amp;', '&quot;', '&lt;', '&gt;'), $string);
    $string=nl2br($string);
    return $string;
}
function get_safe_replace($array){
    if(!is_array($array)) return safe_replace(strip_tags($array));
    foreach($array as $k => $val) $array[$k] = get_safe_replace($val);
    return $array;
}



//function savecache($table)
//{
//    if (false==$table) return false;
//    $hascache = DB::name("system_module")->where("name='{$table}'")->value("hascache");
//    if (false==$hascache) {  return false; }
//
//    $moduleRes = F('system_module');
//     if (false == $moduleRes) {
//        return false;
//    }
//
//    $fields =  F(strtolower($table)  . "_Field.php");
//    $out_types_arr = ['editor'];   //不存的字段，防止文件太大
//    foreach ($fields as $k=>$v) {
//        if (!in_array($v['type'],$out_types_arr) ) {
//            if (isset($v['status']) && $v['status']>0  ) {
//                $save_field_arr[] = $v['field'];
//            }else {
//                $save_field_arr[] = $v['field'];
//            }
//        }
//    }
//
//    if (false==$save_field_arr) return false;
//
//    $pk = Db::table($table)->getPk();
//
//    if (!in_array($pk,$save_field_arr)) {
//        $save_field_arr[] = $pk;
//    }
//
//    $field = implode(",",$save_field_arr);
//    $result = Db::table($table)->field($field)->order("{$pk} desc")->select();
//    F($table,$result);
//
//}



//取缓存字的记录,根据id值返回对应字段的值
//$table 表名
//$idField  当前记录的id,
//应用于： 知道学生表的id, 取学生的姓名
// getCacheField ("students",1,"student_name")

function getCacheField ($cacheTable,$PkValue,$resfield,$out=1,$debug=0)
{
    $returnStatus = '';

    if (false == $cacheTable || $cacheTable==null  ) {
        if ($debug) $returnStatus="未找到缓存表,errorline:".__LINE__;
        echo $returnStatus;
    }

    if (false == trim($PkValue) || $PkValue==null ) {
        if ($debug) $returnStatus="PkValue参数无效,errorline:".__LINE__;
        echo $returnStatus;
    }


    if (false == trim($resfield) || $resfield==null ) {
        if ($debug) $returnStatus="resfield参数无效,errorline:".__LINE__;
        echo $returnStatus;
    }

    $filename = $cacheTable;
    if (!in_array($resfield,DB::table($cacheTable)->getTableFields())) {
        if ($debug) $returnStatus="resfield字段不存在{$cacheTable}表内,errorline:".__LINE__;
        echo $returnStatus;
    }

    !preg_match("/\.php/", $cacheTable) && $filename = $cacheTable . ".php";

    $cnArr = F($filename);

    if (false == $cnArr) {
        if ($debug) $returnStatus = "未找到缓存表内容,errorline:".__LINE__;
        echo $returnStatus;
    }

    $row = $cnArr[$PkValue];


    if (false==$row) {
        //prt($row,0);
        if ($debug) $returnStatus = "未找到缓存表内容,errorline:".__LINE__;
        echo $returnStatus;
    }
    // prt($row[$resfield],0);
    if ($row[$resfield]) {
        if ($out==1) {
            echo trim($row[$resfield]);
        }else {
            return  trim($row[$resfield]);
        }
    } else {
         if ($debug) $returnStatus = "未找到缓存表字段内容,errorline:".__LINE__;
        echo $returnStatus  ;
    }


}

//判断值是否为false
function ibfalse($value) {
    if (empty($value)) return false;
    if (!trim($value)) return false;
    if (intval($value)==0) return false;
    if ($value==NULL) return false;
    return trim($value);

}


function gotoback($url) {
    if (false== $url) {
        return false;
    }
    prt($url);
   echo "<a class='layui-btn layui-btn-primary'  href='{$url}'>返回</a>" ;
}

function ajaxtestb( ) {
    die("ajaxtestb_123456");
}

function dir_list($path, $exts = '', $list= array()) {
    $path = dir_path($path);
    $files = glob($path.'*');
    foreach($files as $v) {
        $fileext = fileext($v);
        if (!$exts || preg_match("/\.($exts)/i", $v)) {
            $list[] = $v;
            if (is_dir($v)) {
                $list = dir_list($v, $exts, $list);
            }
        }
    }
    return $list;
}

function fileext($filename) {
    return strtolower(trim(substr(strrchr($filename, '.'), 1, 10)));
}

function dir_path($path) {
    $path = str_replace('\\', '/', $path);
    if(substr($path, -1) != '/') $path = $path.'/';
    return $path;
}

/**
 * 下载文件
 * @param $filename
 * @param string $showname
 * @param string $content
 * @param int $expire
 */
function download ($filename, $showname='',$content='',$expire=180) {
    if(is_file($filename)) {
        $length = filesize($filename);
        // prt($length);
    }elseif(is_file(UPLOAD_PATH.$filename)) {
        $filename = UPLOAD_PATH.$filename;
        $length = filesize($filename);
    }elseif($content != '') {
        $length = strlen($content);
    }else {
        new Exception($filename.L('下载文件不存在！'));
    }
    if(empty($showname)) {
        $showname = $filename;
    }
    $showname = basename($showname);
    if(!empty($filename)) {
        if(function_exists('finfo_open')){
            $finfo = finfo_open(FILEINFO_MIME_TYPE); // 返回 mime 类型

            $type = finfo_file($finfo, $filename);

            finfo_close($finfo);
        }elseif(function_exists('mime_content_type')){
            $type = mime_content_type($filename);
        }else{
            $type = 'text/plain';
        }
    }else{
        $type    =   "application/octet-stream";
    }
    //发送Http Header信息 开始下载
    header("Pragma: public");
    header("Cache-control: max-age=".$expire);
    //header('Cache-Control: no-store, no-cache, must-revalidate');
    header("Expires: " . gmdate("D, d M Y H:i:s",time()+$expire) . "GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s",time()) . "GMT");
    header("Content-Disposition: attachment; filename=".$showname);
    header("Content-Length: ".$length);
    header("Content-type: ".$type);
    header('Content-Encoding: none');
    header("Content-Transfer-Encoding: binary" );
    if($content == '' ) {
        readfile($filename);
    }else {
        echo($content);
    }
    exit();
}


// 发送邮件
function sendmail($tomail,$subject,$body,$config=''){

    //TODO 获取系统参数
    // if(!$config)$config = F('system_variable');
    // prt($config);
    if(!$config){
        $config = Db::name('system_variable')->where('group_id', 3)->column(['name', 'value']);
    }

    $mail = new PHPMailer();

    if($config['mail_type']==1){
        $mail->IsSMTP();
    }elseif($config['mail_type']==2){
        $mail->IsMail();
    }else{
        if($config['sendmailpath']){
            $mail->Sendmail =$config['mail_sendmail'];
        }else{
            $mail->Sendmail =ini_get('sendmail_path');
        }
        $mail->IsSendmail();
    }
    if($config['mail_auth']){
        $mail->SMTPAuth = true; // 开启SMTP认证
    }else{
        $mail->SMTPAuth = false; // 开启SMTP认证
    }

    $mail->PluginDir=LIB_PATH."ORG/";
    $mail->CharSet='utf-8';
    $mail->SMTPDebug  = false;        // 改为2可以开启调试
    $mail->Host = $config['mail_server'];      // GMAIL的SMTP
    //$mail->SMTPSecure = "ssl"; // 设置连接服务器前缀
    //$mail->Encoding = "base64";
    $mail->Port = $config['mail_port'];    // GMAIL的SMTP端口号
    $mail->Username = $config['mail_user']; // GMAIL用户名,必须以@gmail结尾
    $mail->Password = $config['mail_password']; // GMAIL密码
    //$mail->From ="maxhom@163.com";
    //$mail->FromName = "maxhom企业建站系统";
    $mail->SetFrom($config['mail_from'], $config['site_name']);     //发送者邮箱
    $mail->AddAddress($tomail); //可同时发多个
    //$mail->AddReplyTo('147613338@qq.com', 'maxhom'); //回复到这个邮箱
    //$mail->WordWrap = 50; // 设定 word wrap
    //$mail->AddAttachment("/var/tmp/file.tar.gz"); // 附件1
    //$mail->AddAttachment("/tmp/image.jpg", "new.jpg"); // 附件2
    $mail->IsHTML(true); // 以HTML发送
    $mail->Subject = $subject;
    $mail->Body = $body;
    //$mail->AltBody = "This is the body when user views in plain text format";     //纯文字时的Body
    if(!$mail->Send())
    {
        return false;
    }else{
        return true;
    }
}

//覆写字段
//文件使用config/module.php
function overwrite_setfield($module_name, $fields)
{

    if (false != ($admin_table_layout = (config('module.admin_table_layout')))) {
        if (isset($admin_table_layout[$module_name])) {
            $module_fields = $admin_table_layout[$module_name];
            foreach ($fields as $k => &$v) {
                foreach ($module_fields as $k2 => $v2) {
                    if ($k == $k2) {
                        foreach ($v2 as $k3 => $v3) {
                            if (isset($v[$k3])) {
                                $v[$k3] = $v2[$k3];
                            }
                        }
                    }
                }
            }
        }
    }

    return $fields;
}



//取数据库全部的表
function getDBTables() {
    $tmp_tables = Db::query("SHOW TABLES");
    foreach ($tmp_tables as $k=>$v) {
        foreach ($v as $k2=>$v2) {
            $tables[] = $v2;
        }
    }
    return $tables;
}


//验证输入数据
function validateField($table,$data=[])
{
    $tables = getDBTables();
    if (!in_array($table,$tables)) {
        exception("{$table}数据表不存在，validateField函数报错，line:".__LINE__);
    }

    $db = DB::table($table);
    $tbFields = $db->getTableFields();
    $cachefields = F($table);

    $data = false==$data?$_POST:$data;
    foreach ($data as $k => $postv) {
        // prt($k,0);
        if (in_array($k, $tbFields)) {
            foreach ($cachefields as $kk => $vv) {
                if ($k == $vv['field']) {
                    if ($vv['required'] === 1) {
                        if (empty($postv) && false == $postv) {
                            //必输
                            $mess = $vv['errormsg'] ? $vv['errormsg'] : "{$vv['name']}字段必输！";
                            exception($mess);
                            exit;
                        }
                    }

                    if (!empty($postv) && 　false == $vv['pattern']) {
                        //输入的值必须符合条件
                        $pattern = $vv['pattern'];
                        $RegexCheck = lang('RegexCheck');
                        $RegexArr = array_keys($RegexCheck);
                        $tmp_Reg = implode(",", $RegexArr);
                        $tmp_Reg = strtolower($tmp_Reg);
                        $tmp_Reg = explode(",", $tmp_Reg);
                        if (in_array(strtolower($pattern . 'Action'), $tmp_Reg)) {
                            $mess = "请在{$vv['name']}字段,输入合法的" . $RegexCheck[$pattern] . "格式！";
                            if (false == \org\RegexCheck::$pattern($postv))  exception($mess);
                        }
                    }
                }
            }
        }
    }

}