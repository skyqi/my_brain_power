<?php
return [

    $cache_options = [
        // 缓存类型为File
        'type'   => 'File',
        // 缓存有效期为永久有效
        'expire' => 0,
        // 指定缓存目录
      //  'path'   => env('runtime_path') . 'cache/',
    ],


    'AUTH_PWD_ENCODER'		=>'md5',	// 用户认证密码加密方式
    'USER_AUTH_GATEWAY'	    =>'/admin/login.html',	// 默认认证网关

    'UPLOAD_DIR'        =>  'Public/Admin/upload/',//不能有
    'LOGIN_PASS_DEFAULT' =>  '123456',
    'DB_PREFIX'=>'maxhom_',
    'ADMIN_ACCESS'=>'123456',
    'Has_index_setup'=>'1',    //在列表表上出现设置窗
    'attach_maxsize'=>ini_get('upload_max_filesize'),
    'UPLOAD_FILE_PATH'=>'./Uploads/'.date('Ym')."/",
    'Trans_langs'=>array('en'),   //可加多个语言，http://note.youdao.com/noteshare?id=68b9753852151d48b23a7438d0043f28
    'DEFAULT_TRANS_LANG'=>'en',  //默认翻译语言
    'Trans_LangsPack'=>include_once("langspack.php"),


]
?>