
<?php
/**
 * This file is part of workerman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link http://www.workerman.net/
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * 用于检测业务代码死循环或者长时间阻塞等问题
 * 如果发现业务卡死，可以将下面declare打开（去掉//注释），并执行php start.php reload
 * 然后观察一段时间workerman.log看是否有process_timeout异常
 */
//declare(ticks=1);

use \GatewayWorker\Lib\Gateway;
use \GatewayWorker\Lib\Db;

class EventTools
{
    protected static $str_message,$hex_message,$client_id,$apsdb,$clients;
	
	//判断用户对应的硬件是否在线
    public static function ibHdOnline($user_client_id,$message,$type='app') {
		if (false == Gateway::isOnline($user_client_id)) {
			return false;
		}
		$apsdb = Db::instance('mysqlDb');
        $row = $apsdb->row("SELECT client_id,online,mac,user_id FROM `client_online` WHERE client_id='".$user_client_id."'");
		 
		 
		if (!empty($row['mac'])) {
			$row = $apsdb->query("SELECT client_id,online,mac,user_id FROM `client_online` WHERE user_id=0 and   mac='".$row['mac']."'");
			if(count($row)==0) {
				Gateway::sendToClient($user_client_id,self::resMsg($message,1,'未找到上线的硬件',$type));
				return '';
			}
			
		}
		
	}
	
    public static function runFunc($client_id,$message)
    {     
        self::updateOnlie($client_id);
       
        self::$apsdb = $apsdb = Db::instance('mysqlDb');
		self::$client_id = $client_id;
        if (false!=($ibHex = self::ibHexMessage($message))) {
            $str_message = $message;
            $hex_message = self::hexToStr($message);

        } else {
            $str_message = self::strToHex($message);
            $hex_message = $message;

        }
		 
        self::$str_message = trim($str_message);
        self::$hex_message = $hex_message;
       
        self::set_fun($client_id,$message,$ibHex);
    }

    protected static function set_fun($client_id,$message,$ibHex)
    {
	 
        echo "str_message=".self::$str_message.",line:".__LINE__.",--- start ---\r\n";
		

        if (preg_match("/=32/i", self::$str_message)) {
            return '';
        }else if (preg_match("/=2/i", self::$str_message)) {
            return '';
        }elseif (preg_match("/^dd+/i", self::$str_message)) {
            //硬件登录
            // $mac = substr($message, 2);
            // $controller->dd_login($client_id, $message,$mac,2);
            // return '';
        } elseif (preg_match("/^cd+/i", self::$str_message)) {
            //cd app登录
            self::writeDbLog($ibHex,'app');
            self::ibHdOnline($client_id,$message); 
            self::cd_LoginUserid($client_id, self::$str_message);
            return '';
        }elseif (preg_match("/^delAll+/i", self::$str_message)) {
            //cd app登录
            self::writeDbLog($ibHex,'app');
            self::ibHdOnline($client_id,$message,'app'); 
            self::delclient($client_id, self::$str_message);
            return '';
        } elseif (preg_match('/"maxhom"."json","cmd"."login"+/Ui', self::$str_message)) {
             //硬件登录,发的字符串
            // {"maxhom":"json","cmd":"login","mac":"5CCF7F41259E"}
            $arr = json_decode($message);
            self::writeDbLog($ibHex,'hd');
			self::updateOnlie($client_id,self::macTocStr($arr->mac));
            self::dd_login($client_id, self::$str_message, $arr->mac,1,'hd');
            return '';
        } elseif (preg_match("/^cc+/i", self::$str_message)) {
            //cc ，app发送消息
            self::writeDbLog($ibHex,'app');
            self::ibHdOnline($client_id,$message,'app'); 
            self::cc_cmd($client_id, self::$str_message,'app');
            return '';
        } elseif (preg_match("/^ff+/i", self::$str_message)) {
            //硬件消息			
            self::writeDbLog($ibHex,'hd');
            self::ff_cmd($client_id,self::$str_message,'hd');
            return '';
        } else {
            //直接发回
            self::writeDbLog($ibHex,'other');
			Gateway::sendToClient($client_id, $message);
			return '';
        }


        //echo date("Y-m-d H:i:s") . "error:{self::$str_message},other not is default command!";

 
    }



    // 硬件发送消息
    protected static function ff_cmd($client_id, $message,$type) {

            echo "\n>>>> line:".__LINE__.";start ff_cmd：".$message."\n";
            $apsdb =  self::$apsdb; 
            $wifiRes = $apsdb->select('id,user_id,macaddress')->from('wificontrol')->where("client_id='{$client_id}'")->query();

            foreach ($wifiRes  as $key => $v) {
                    $user_id = $v['user_id'];
                    $client_ids2= $apsdb->select('clientid')->from('wifi-clientinfo')->where('user_id ='.$user_id)->query();
              // echo ">>>>>>>>>>>>>===";
			   //print_r ($client_ids2);      
                   foreach ($client_ids2 as $key2 => $vv2) {
                      //  var_dump(__LINE__,'clientid',$vv2['clientid'],'message',$message,'发送');
                        $resMgs =  json_encode(array('errorcode'=>0,'msg'=>'操作成功','message'=>$message,'mac'=>$v['macaddress'],'type'=>$type),JSON_UNESCAPED_UNICODE);

                        if (!empty($vv2['clientid'])) {
                                
                                $cardoorRes = $apsdb->row("SELECT id FROM `cardoor` WHERE user_id='".$hd_client_id."' and wificontrol_id=".$v['id']);

                                $bitArr = str_split($message, 2);     
                                $switch_status = $bitArr[5]-0;                                          
                                $apsdb->update('cardoor')->cols(array('switch_status'=> $switch_status))->where("user_id='".$user_id."' and wificontrol_id=".$v['id'])->query();
                                $apsdb->insert('message')->cols(array(
												'ib_read'=>'0',
												'title'=>"id={$cardoorRes['id']}&switch_status={$switch_status}",
												'mtag'=>'wkman',
												'assort'=>3))->query(); 

                                unset($bitArr);
                            Gateway::sendToClient($vv2['clientid'],  $resMgs);
                        }  

                        empty($vv2['clientid']) &&  var_dump(__LINE__,'clientid',$vv2['clientid'],'message',$resMgs,'###error###');
                   }   
            }

     
             
        return '';
       


    }


    //cd +userid
    // app 发过来消息
    protected static function cd_LoginUserid($client_id, $message,$type='app') {
     
          $user_id =substr($message,2);

        if (false==$user_id) {

            return '';
        }

        $apsdb = self::$apsdb;
        $users = $apsdb->select('id')->from('users')->where("id='{$user_id}'")->single();
   
        if(false==$users){
            self::savelog("未找到用户id={$user_id}");
             Gateway::sendToClient($client_id,json_encode(array('errorcode'=>1,'msg'=>"未找到用户id={$user_id}",'message'=>$message,'type'=>$type)));
            return '';
        }
        self::updateOnlie($client_id,'',$user_id);
        try{
            // 全部插入

            /*
			 $crow= $apsdb->select('clientid')->from('wifi-clientinfo')->where('user_id ='.$user_id)->query();
			 if(count( $crow)){
				$apsdb->update('wifi-clientinfo')->cols(array('clientid'=>$client_id))->where("user_id='".$user_id."'")->query();
			 }else{
				$apsdb->insert('wifi-clientinfo')->cols(array('clientid'=>$client_id,'user_id'=>$user_id ,"updatetime"=>time()))->query();
			 }
			 */
			 $apsdb->delete('wifi-clientinfo')->where("user_id='{$user_id}'")->query();   
			 $apsdb->insert('wifi-clientinfo')->cols(array('clientid'=>$client_id,'user_id'=>$user_id ,"updatetime"=>time()))->query();

			
            
			$msg  = json_encode(array('errorcode'=>0,'msg'=>'操作成功','message'=>$message,'type'=>$type),JSON_UNESCAPED_UNICODE);
            
            Gateway::sendToClient($client_id, $msg);
            return '';

        }catch (Exception $e) {
            self::savelog($e->getMessage());
            echo date("Y-m-d H:i:s")."error:".$e->getMessage();
            Gateway::sendToClient($client_id,json_encode(array('errorcode'=>1,'msg'=>'mysql操作失败','message'=>$message,'type'=>$type)));
          
            return '';
        }



    }

    //4.app发送消息   cc#mcumac#cmd   标示/硬件地址长度/硬件地址/命令
    protected static function cc_cmd($client_id, $message,$type='app') {
        //当app没有登录先发送消息时：返回： cc cc cc cc
        $apsdb= self::$apsdb;

        $arr = explode("#", $message);
        if (false==isset($arr[1]) && false==$arr[1]) {
            Gateway::sendToClient($client_id,self::resMsg($message,1,'mac地址错误',$type));
            return '';
        }
        if (false==isset($arr[2]) &&false==$arr[2]) {
            Gateway::sendToClient($client_id,self::resMsg($message,1,'指令错误',$type));
            return '';
        }

        //硬件地址
        $mac= self::macTocStr($arr[1]);
        $cmd = $arr[2];
        self::updateOnlie($client_id,$mac);

        //找出上线硬件
        $hdOnlineRes = $apsdb->query("SELECT client_id,online,mac,user_id FROM `client_online` WHERE user_id=0 and   mac='".$mac."'");
 
        foreach ($hdOnlineRes as $key => $res) {
            $hd_client_id = $res['client_id'];
           if (!empty($hd_client_id) && Gateway::isOnline($hd_client_id)){

                Gateway::sendToClient($hd_client_id, self::strToHex($cmd));
                Gateway::sendToClient($client_id, self::resMsg($cmd,1,'操作成功','app'));
            }else {
                Gateway::sendToClient($client_id, self::resMsg($message,1,'对应的硬件离线','app'));
            }
        }




        // $tcli = $apsdb->select("id,user_id,client_id")->from('wificontrol')->where("macaddress='" . $mac . "' ")->query();

        // if(!$tcli){
        //     //向app发
        //     Gateway::sendToClient($client_id,self::resMsg($message,1,'用户未登录',$type));
        //     return '';
        // }

        // var_dump(__LINE__,'mac=',$mac,'tcli',$tcli);
    //     $client_ids = array();
    //     foreach ($tcli as $key => $value) {
    //         if (!empty($value['client_id']) && !in_array($value['client_id'],$client_ids) ) {
    //             //向硬件发送
    //             $hd_client_id = $value['client_id'];
				// $hd_row = $apsdb->row("SELECT client_id,mac,user_id FROM `client_online` WHERE client_id='".$hd_client_id."'");
    //            // var_dump(__LINE__,'hd_client_id=',$hd_client_id,$hd_row);
    //             $client_ids[] = $value['client_id'];   //去复
				// if (false!=$hd_row['client_id']) {
    //                 Gateway::sendToClient($hd_client_id, self::hexToStr($cmd));
				// 	Gateway::sendToClient($value['client_id'], self::resMsg($cmd,1,'操作成功','app'));
				// } else {
				// 	//向app通知
				// 	Gateway::sendToClient($client_id,self::resMsg($message,1,'对应的硬件离线',$type));
				// }
    //         } else {
    //             //向app通知
				// 	Gateway::sendToClient($client_id,self::resMsg($message,1,'对应的硬件离线',$type));
    //         }
    //     }
         
        return '';
 
    }

    //dd登录
    //dd+mac
    protected static function dd_login($client_id, $message,$mac=0,$type='hd')
     {
         $macaddress = $mac;
         echo " mcu login mac=" . ($macaddress);

         $macaddress =   self::macTocStr($macaddress);
         if (false==$macaddress) {
             Gateway::sendToClient($client_id,self::resMsg($message,1,'mac参数不正确'));
         }
         $apsdb = self::$apsdb;
         //找到匹配地址的user_id

        
         $res = $apsdb->select("id,user_id")->from('wificontrol')->where("macaddress='" . $macaddress . "' ")->query();
         
        if (false==count($res)) {

            Gateway::sendToClient($client_id,self::resMsg($message,1,'未找到mac地址',$type));
            return '';
        }    

         $apsdb->update('wificontrol')->cols(array('online'=>1,'client_id'=>$client_id,'updatetime'=>time()))->where("macaddress='".$macaddress."'")->query(); 

         try {
             //  
             if(count($res)>0){
                // $resid = $res[0]['id'];
                 foreach($res as $value){
                     $user_id = $value['user_id']; 
                     $userinfo = $apsdb->select("clientid")->from('wifi-clientinfo')->where("user_id=".$user_id)->query();
                     if(count($userinfo)>0){
                         foreach($userinfo as $uclid){
                             $userclientid = $uclid['clientid'];
                            
                             if($userclientid>0){
                               
                                 Gateway::sendToClient($userclientid,self::resMsg($message,0,'登录成功',$type));
                                 //Gateway::sendToClient($client_id,$message);

                             }
                         }
                     }
                 }
             }

             if($type=='hd'){ 
                //回复硬件，格式不做调整
                $cmd = strtoupper('{"MAXHOM":"JSON","cmd":"login_ok"}');
                 echo $cmd;
                 Gateway::sendToClient($client_id,$cmd);
              }
            
             
         }catch (Exception $e) {

             echo date("Y-m-d H:i:s")."error:".$e->getMessage().";Line:".__LINE__ ;
             exit;
         }

     }

    




    // 删除所有的设备
    // 1，找出在线client_id，
    // 2，与wifi-clientinfo表对比，删除不在线的client_id记录

    // protected static function  delclient($client_id, $message) {
    //     echo ">>> delclient status  \n";
    //     $onlinearr = Gateway::getAllClientIdList();
    //     print_r ($onlinearr);
    //     $apsdb = self::$apsdb;
    //     $infoarr = $apsdb->select("id,clientid")->from('wifi-clientinfo')->query();
        
    //     foreach($infoarr as $ors){
    //         $clientid = $ors['clientid'];
    //         $infoid = $ors['id'];
    //         if(!in_array($clientid, $onlinearr)){ // 不在在线设备
    //            self::$apsdb->delete('wifi-clientinfo')->where("id='{$infoid}'")->query();   

    //         }
    //     }
    //     unset($infoarr);

    //     $infoarr = $apsdb->select("id,client_id")->from('wificontrol')->query();
        
    //     foreach($infoarr as $ors){
    //         $client_id = $ors['client_id']; 
    //         if(!in_array($client_id, $onlinearr)){ // 不在在线设备
    //           // self::$apsdb->delete('wifi-clientinfo')->where("id='{$infoid}'")->query();   
    //             self::onCloseAPP($client_id);
    //         }
    //     }
    //     return '';
    // }

    //断开与client_id对应的客户端的连接
    protected static function  closeClinet($cid) {
        echo "close client id = ".$cid[1];
        Gateway::closeClient($cid);
    }

    /**
     * 当硬件离线，找到硬件表修改为离线状态。 通知用户app硬件已经离
     * @param int $client_id 连接id
     */
    protected static function  onCloseAPP($client_id)
    {
        //var_dump("__LINE__",__LINE__,$client_id);die;

        echo 'onCloseAPP --- _LINE__='.__LINE__.' client_id = '.$client_id;

        $apsdb =   Db::instance('mysqlDb');
       

        //退出的时候 删除登录信息
        //$clientidcount= $apsdb->select('clientinfo,client_type,id')->from('wificontrol')->where('clientid= :clientid')->bindValues(array('clientid'=>$client_id))->query();
       
        

        $message = "onCloseAPP";

        //  通知用户app硬件已经离线
      //  $res = $apsdb->select("id,user_id")->from('wificontrol')->where("client_id='".$client_id."'  ")->query();
          //  $clientids = array();
        
            //  if(count($res)>0){
            //     // $resid = $res[0]['id'];
            //      foreach($res as $value){
            //          $user_id = $value['user_id']; 
            //          $userinfo = $apsdb->select("clientid")->from('wifi-clientinfo')->where("user_id=".$user_id)->query();
            //          if(count($userinfo)>0){
                        
            //              foreach($userinfo as $uclid){
            //                  $userclientid = $uclid['clientid'];
                              
            //                  if($userclientid>0  && !in_array($userclientid,$clientids)){
            //                     $clientids[] = $userclientid;
            //                     Gateway::sendToClient($client_id,self::resMsg($message,1,'硬件已下线'));
            //                  }
            //              }
            //          }
            //      }
            // }

            $apsdb->update('wificontrol')->cols(array('online'=>0,'client_id'=>'','updatetime'=>time()))->where("client_id='".$client_id."'  ")->query(); 
 

    }

    

    // 根据地址返回用户数组['user_id'=>client_id]
    protected static function  getmacIdxappClicentid($macaddress) {

        $res = self::$apsdb->select("id,user_id")->from('wificontrol')->where("macaddress='" . $macaddress . "' ")->query();
         
        if (false==count($res)) {

            var_dump(__LINE__,'未找到mac地址');
            return '';
        }    

        $user_arr = array();
        if(count($res)>0){
        // $resid = $res[0]['id'];
            foreach($res as $value){
                 $user_id = $value['user_id']; 
                 $userinfo = self::$apsdb->select("clientid")->from('wifi-clientinfo')->where("user_id=".$user_id)->query();
                 if(count($userinfo)>0){
                     foreach($userinfo as $uclid){
                        $user_arr[$uclid['user_id']] = $uclid['clientid'];                         
                     }
                 }
            }
        }
        return $user_arr?$user_arr:false;

    }       



    //  当用户断开连接时触发
    protected static function    triggerClose($client_id, $message = false)
    {
        
        self::onCloseAPP($client_id);        
    }


    protected static function ibHexMessage($message) {
        $tmpMsg = str_replace(array('{','}','\n','\r','"',',',':','#','!','@','$'.'%','^','&','*','(',')','-','+','-','=','[',']','|','\/','\\',' '),'',$message);
        if (preg_match("/[a-zA-Z0-9.]+/i", $tmpMsg)) {
            return 1;   //是字符串
        } else {
            return false;   //不是字符串，是二进制
        }
      
    }

    protected static function updateOnlie($client_id,$mac='',$user_id='') {
        $online = Gateway::isOnline($client_id);
        $apsdb = Db::instance('mysqlDb');
		$mac && $mac = self::macTocStr($mac);
        $row = $apsdb->row("SELECT client_id,online FROM `client_online` WHERE client_id='".$client_id."'");
        if (false==$row && $online==1) {
            $insert_id = $apsdb->insert('client_online')->cols(array(
                'client_id'=>$client_id,
                'mac'=>$mac?trim($mac):'',
                'online'=>$online,
            ))->query();

        } else {
            $cols['online'] = $online;
            if ($mac) $cols['mac'] = trim($mac);
            if ($user_id>0) $cols['user_id'] = $user_id;
            $res = $apsdb->update('client_online')->cols($cols)->where("client_id='" . $client_id . "'")->query();
        }
	 
		$rows = $apsdb->query("SELECT client_id,online FROM `client_online`");
		$off_client_id = array();
		foreach($rows as $k=>$client_id2) {			 
			if (false==Gateway::isOnline($client_id2['client_id'])) {
				$off_client_id[] = "'".$client_id2['client_id']."'";
			}
		}
		if (count($off_client_id)>0) {
			$off_client_id_str = implode(",",$off_client_id);
			$apsdb->delete('client_online')->where("client_id in (".$off_client_id_str.")")->query();
		}
		
    }


    protected static function  writeDbLog($ibHex,$type='app')
    {

       // $flag = substr($message, 0, 2);
        //$mac = '';
        // var_dump(self::$str_message);
            try {
                if (preg_match("/2=.32/Ui",self::$str_message)) {
                    //心跳不记录日志
                    return '';
                }
                if (!empty(self::$str_message)) {
                    self::$apsdb->insert('wsoptlog')->cols(array('client_id'=>self::$client_id,'cmd' =>self::$str_message, 'updatetime' => time(),   'ibHex' => $ibHex?0:1,"type"=>$type))->query();
                }
                //log end
            } catch (Exception $e) {
                $errmsg = $e->getMessage();
               
                echo "error:" . $errmsg .";Line:".__LINE__."\r\n";
                exit;
            }


        
        /////
        $wsoptlogs = self::$apsdb->select("id")->from('wsoptlog')->query();

        if (count($wsoptlogs) > 500) {
            self::$apsdb->query("DELETE FROM  wsoptlog  WHERE id < (SELECT MAX( id )-500 FROM (SELECT * FROM  wsoptlog) AS t)");
        }

    }

    //字符串转十六进制
    protected static function strToHex($string)
    {

        $hex = "";
        for ($i = 0; $i < strlen($string); $i++)
            //$hex.= " ".dechex(ord($string[$i]));
            //ord  返回字符的 ASCII 码值  dechex  十进制转换为十六进制
            $hex .= " " . dechex(ord($string[$i]));
        $hex = trim($hex);
        $hex = strtoupper($hex);
        return $hex;
    }

    //十六进制转字符串
    protected static function hexToStr($hex)
    {
        $string = "";
        //$hex = str_replace(" ","",$hex);

        // hexdec — 十六进制转换为十进制  chr — 返回指定的字符(ASCII码转换字符串)
        for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
            $string .= chr(hexdec($hex[$i] . $hex[$i + 1]));
        }
        return $string;
    }

    protected static function macTocStr($mac) {
        if (false==$mac) return '';
        $mac = strtolower($mac);
        if (!strstr($mac, ":")) {
            $bitArr = str_split($mac, 2);
            $newmacstr = implode(":", $bitArr);
        } else $newmacstr = $mac;
        return $newmacstr;
    }



    //$sendMessage 是数组
   protected static function  resMsg($sendMessage,$errcode=1,$msg='操作成功',$type='app') {
        if (!is_array($sendMessage)) {
            $ret['message'] =  $sendMessage ;
        }else $ret = $sendMessage;
		$ret['errorcode'] = $errcode;
		$ret['msg'] = $msg;
		$ret['type'] = $type;
        return json_encode($ret,JSON_UNESCAPED_UNICODE);
    }

}

