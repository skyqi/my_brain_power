<?php
/**
 * This file is part of workerman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link http://www.workerman.net/
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * 用于检测业务代码死循环或者长时间阻塞等问题
 * 如果发现业务卡死，可以将下面declare打开（去掉//注释），并执行php start.php reload
 * 然后观察一段时间workerman.log看是否有process_timeout异常
 */
//declare(ticks=1);
//namespace  YourApp  ;
use \GatewayWorker\Lib\Gateway;
use \GatewayWorker\Lib\Db;
/**
 * 主逻辑
 * 主要是处理 onConnect onMessage onClose 三个方法
 * onConnect 和 onClose 如果不需要可以不用实现并删除
 */
class Events extends EventTools
{

    public function __construct()
    {
      //  parent::__construct();

    }


    /**
     * 当客户端连接时触发
     * 如果业务不需此回调可以删除onConnect
     * 
     * @param int $client_id 连接id
     */
    public static function onConnect($client_id)
    {
        // 向当前client_id发送数据
         #  echo date("Y-m-d H:i:s") . ";LINE:".__LINE__. "  message={$message}\r\n";
        self::updateOnlie($client_id);
        // Gateway::sendToClient($client_id, "onConnect success  client_id=[$client_id]\r\n");
         

    }
    
   /**
    * 当客户端发来消息时触发
    * @param int $client_id 连接id
    * @param mixed $message 具体消息
    */
   public static function onMessage($client_id, $message)
   {
       if (false!=($ibHex = self::ibHexMessage($message))) {
           $str_message = $message;
           $hex_message = self::hexToStr($message);
       } else {
           $str_message = self::strToHex($message);
           $hex_message = $message;
       }

       echo "\n line:".__LINE__." client_id:[$client_id],message:[$str_message] runFunc ...... \n";
       self::runFunc($client_id, $message);
   }


   /**
    * 当用户断开连接时触发
    * @param int $client_id 连接id
    */
   public static function onClose($client_id)
   {
       
      //  var_dump(__LINE__,'client_id',$client_id); 
       $apsdb = Db::instance('mysqlDb');
       $row = $apsdb->row("SELECT client_id,online,mac,user_id FROM `client_online` WHERE client_id='".$client_id."'");
       $mac = $row['mac'];
       var_dump(__LINE__,"mac",$mac);
       if (false==$mac) return '';
       $aPPOnlineRes = $apsdb->query("SELECT client_id,online,mac,user_id FROM `client_online` WHERE user_id>0 and   mac='".$mac."'");

        $resMgs =  json_encode(array('errorcode'=>1,'msg'=>'提示','message'=>'服务器离线了','mac'=>$mac,'type'=>'app'),JSON_UNESCAPED_UNICODE);

        foreach ($aPPOnlineRes as $key => $res) {
            $app_client_id = $res['client_id'];
              var_dump(__LINE__,'app_client_id',$app_client_id); 
           !empty($app_client_id) &&  Gateway::sendToClient($app_client_id, $resMgs);
        }


       self::updateOnlie($client_id);
       $apsdb->update('wificontrol')->cols(array('online'=>0,'client_id'=>'','updatetime'=>time()))->where("client_id='".$client_id."'  ")->query(); 

       
   }


}
