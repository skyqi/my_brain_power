-- ----------------------------
-- 日期：2019-05-26 02:00:01
-- MySQL - 5.5.52-MariaDB : Database - naoli_master
-- ----------------------------

CREATE DATAbase IF NOT EXISTS `naoli_master` DEFAULT CHARACTER SET utf8 ;

USE `naoli_master`;

-- ----------------------------
-- Table structure for `cardoor`
-- ----------------------------

DROP TABLE IF EXISTS `cardoor`;

CREATE TABLE `cardoor` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `doorname` text COMMENT '	设备名称',
  `user_id` varchar(255) NOT NULL DEFAULT '',
  `wificontrol_id` varchar(255) NOT NULL DEFAULT '',
  `sitesensor_id` text COMMENT '位置传感器id',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(10) unsigned NOT NULL DEFAULT '90',
  `doornumb` int(10) unsigned NOT NULL DEFAULT '0',
  `switch_status` varchar(255) NOT NULL DEFAULT '',
  `btn_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COMMENT='车库门表';

-- ----------------------------
-- Data for the table `cardoor`
-- ----------------------------

INSERT INTO `cardoor` VALUES ('1', '车库门001', '7', '1', '1', '1531125182', '1533807792', '1', '90', '0', '1', '1');
INSERT INTO `cardoor` VALUES ('2', '车库门002', '7', '1', '1', '1531125182', '1533802340', '1', '90', '0', '1', '4');
INSERT INTO `cardoor` VALUES ('3', '车库门003', '3', '1', '8', '1531205288', '1531205409', '1', '90', '0', '0', '3');
INSERT INTO `cardoor` VALUES ('5', '车库门004', '3', '2', '9', '1531208321', '0', '1', '90', '0', '0', '4');
INSERT INTO `cardoor` VALUES ('7', 'sfadf', '3', '2', '', '1531234094', '0', '1', '90', '0', '0', '0');
INSERT INTO `cardoor` VALUES ('15', '车库门测试1', '7', '31', '', '1531552765', '0', '1', '90', '0', '1', '0');
INSERT INTO `cardoor` VALUES ('14', '公司车库门', '7', '31', '', '1531489624', '0', '1', '90', '0', '1', '0');
INSERT INTO `cardoor` VALUES ('13', '车库门修改1', '7', '31', '', '1531466407', '1531466453', '1', '90', '0', '1', '0');
INSERT INTO `cardoor` VALUES ('16', '验证', '7', '31', '', '1531962234', '0', '1', '90', '0', '1', '0');
INSERT INTO `cardoor` VALUES ('17', '验证', '7', '33', '', '1531962305', '0', '1', '90', '0', '1', '0');
INSERT INTO `cardoor` VALUES ('18', 'a1435', '3', '3', '', '1531982148', '0', '1', '90', '0', '', '0');
INSERT INTO `cardoor` VALUES ('19', '公司车库门', '7', '35', '2', '1533777114', '1533799025', '1', '90', '0', '1', '2');
INSERT INTO `cardoor` VALUES ('35', '', '7', '19', '', '0', '1533798911', '1', '90', '0', '1', '3');
INSERT INTO `cardoor` VALUES ('44', '测试2', '3', '39', '1', '1534413625', '0', '1', '90', '0', '1', '1');
INSERT INTO `cardoor` VALUES ('55', '公司车库门', '16', '46', '', '1534930019', '1534930020', '1', '90', '2', '1', '2');
INSERT INTO `cardoor` VALUES ('53', 'xxxxx', '0', '1', '', '1534929009', '0', '1', '90', '3', '', '0');

-- ----------------------------
-- Table structure for `client_online`
-- ----------------------------

DROP TABLE IF EXISTS `client_online`;

CREATE TABLE `client_online` (
  `client_id` varchar(100) NOT NULL,
  `online` tinyint(1) NOT NULL COMMENT '0',
  `mac` varchar(50) DEFAULT '',
  `user_id` int(11) DEFAULT '0'
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `client_online`
-- ----------------------------

-- ----------------------------
-- Table structure for `dbbackup_log`
-- ----------------------------

DROP TABLE IF EXISTS `dbbackup_log`;

CREATE TABLE `dbbackup_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL,
  `createtime` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除 0：否 1：是',
  `updatetime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `dbbackup_log`
-- ----------------------------

INSERT INTO `dbbackup_log` VALUES ('1', 'Yp_udtRaWYHLk_2018071318-1.sql', '1531539398', '0', '0');
INSERT INTO `dbbackup_log` VALUES ('2', 'Yp_kzvwiPmBxS_2018071411-1.sql', '1531539411', '0', '0');
INSERT INTO `dbbackup_log` VALUES ('3', 'Yp_aYEqweydoQ_2018071521-1.sql', '1531661406', '0', '');
INSERT INTO `dbbackup_log` VALUES ('4', 'Yp_AnEQNiOudI_2018071521-1.sql', '1531662016', '0', '');
INSERT INTO `dbbackup_log` VALUES ('5', 'Yp_THgXydNGaW_2018071723-1.sql', '1531839752', '0', '');
INSERT INTO `dbbackup_log` VALUES ('6', 'Yp_rdtpSkPagV_2018072017-1.sql', '1532078467', '0', '');

-- ----------------------------
-- Table structure for `debuglog`
-- ----------------------------

DROP TABLE IF EXISTS `debuglog`;

CREATE TABLE `debuglog` (
  `id` int(11) NOT NULL,
  `filename` tinytext NOT NULL,
  `line` int(11) NOT NULL,
  `errormsg` text NOT NULL,
  `updatetime` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `debuglog`
-- ----------------------------

-- ----------------------------
-- Table structure for `goods`
-- ----------------------------

DROP TABLE IF EXISTS `goods`;

CREATE TABLE `goods` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `brand_id` bigint(20) unsigned DEFAULT '0' COMMENT '品牌ID',
  `cate_id` bigint(20) unsigned DEFAULT '0' COMMENT '商品分类id',
  `unit_id` bigint(20) DEFAULT NULL COMMENT '商品单位ID',
  `spec_id` bigint(20) unsigned DEFAULT '0' COMMENT '规格ID',
  `tags_id` varchar(255) DEFAULT '' COMMENT '商品标签ID',
  `is_code` bigint(1) DEFAULT '1' COMMENT '是否有码商品',
  `goods_title` varchar(255) DEFAULT '' COMMENT '商品标签',
  `goods_content` text COMMENT '商品内容',
  `goods_logo` varchar(255) DEFAULT '' COMMENT '商品LOGO',
  `goods_image` text COMMENT '商品图片地址',
  `goods_video` varchar(500) DEFAULT '' COMMENT '商品视频URL',
  `goods_desc` varchar(500) DEFAULT '' COMMENT '商品描述',
  `package_stock` bigint(20) unsigned DEFAULT '0' COMMENT '总库存数量',
  `package_sale` bigint(20) unsigned DEFAULT '0' COMMENT '已销售数量',
  `favorite_num` bigint(20) unsigned DEFAULT '0' COMMENT '收藏次数',
  `sort` bigint(20) unsigned DEFAULT '0' COMMENT '数据排序',
  `status` bigint(1) unsigned DEFAULT '1' COMMENT '商品状态(1有效,0无效)',
  `is_deleted` bigint(1) unsigned DEFAULT '0' COMMENT '删除状态(1删除,0未删除)',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商城商品主表';

-- ----------------------------
-- Data for the table `goods`
-- ----------------------------

-- ----------------------------
-- Table structure for `goods_brand`
-- ----------------------------

DROP TABLE IF EXISTS `goods_brand`;

CREATE TABLE `goods_brand` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `brand_logo` varchar(1024) DEFAULT '' COMMENT '品牌logo',
  `brand_cover` varchar(1024) DEFAULT '' COMMENT '品牌封面',
  `brand_title` varchar(255) DEFAULT '' COMMENT '商品品牌名称',
  `brand_desc` text COMMENT '商品品牌描述',
  `brand_detail` text COMMENT '品牌图文信息',
  `sort` int(11) unsigned DEFAULT '0' COMMENT '商品分类排序',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '商品状态(1有效,0无效)',
  `is_deleted` tinyint(1) unsigned DEFAULT '0' COMMENT '删除状态(1删除,0未删除)',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品产品品牌';

-- ----------------------------
-- Data for the table `goods_brand`
-- ----------------------------

-- ----------------------------
-- Table structure for `goods_cate`
-- ----------------------------

DROP TABLE IF EXISTS `goods_cate`;

CREATE TABLE `goods_cate` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) unsigned DEFAULT '0' COMMENT '上级分类编号',
  `brand_id` bigint(20) DEFAULT '0' COMMENT '品牌ID',
  `cate_title` varchar(255) DEFAULT '' COMMENT '商品分类名称',
  `cate_desc` text COMMENT '商品分类',
  `sort` bigint(20) unsigned DEFAULT '0' COMMENT '商品分类排序',
  `status` bigint(1) unsigned DEFAULT '1' COMMENT '商品状态(1有效,0无效)',
  `is_deleted` bigint(1) unsigned DEFAULT '0' COMMENT '删除状态(1删除,0未删除)',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商城商品分类';

-- ----------------------------
-- Data for the table `goods_cate`
-- ----------------------------

-- ----------------------------
-- Table structure for `goods_list`
-- ----------------------------

DROP TABLE IF EXISTS `goods_list`;

CREATE TABLE `goods_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `goods_id` bigint(20) unsigned DEFAULT '0' COMMENT '商品ID',
  `goods_spec` varchar(255) DEFAULT '' COMMENT '商品规格名称',
  `goods_number` bigint(20) unsigned DEFAULT '0' COMMENT '商品礼品-商品数量',
  `market_price` decimal(20,2) unsigned DEFAULT '0.00' COMMENT '销售价格',
  `selling_price` decimal(20,2) unsigned DEFAULT '0.00' COMMENT '商品价格',
  `goods_stock` bigint(20) unsigned DEFAULT '0' COMMENT '商品库存统计',
  `goods_sale` bigint(20) unsigned DEFAULT '0' COMMENT '已销售数量',
  `status` bigint(1) unsigned DEFAULT '1' COMMENT '商品状态(1有效,0无效)',
  `is_deleted` bigint(1) unsigned DEFAULT '0' COMMENT '删除状态(1删除,0未删除)',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商城商品列表';

-- ----------------------------
-- Data for the table `goods_list`
-- ----------------------------

-- ----------------------------
-- Table structure for `goods_spec`
-- ----------------------------

DROP TABLE IF EXISTS `goods_spec`;

CREATE TABLE `goods_spec` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mch_id` bigint(20) unsigned DEFAULT '0' COMMENT '商户ID',
  `spec_title` varchar(255) DEFAULT '' COMMENT '商品规格名称',
  `spec_param` varchar(255) DEFAULT '' COMMENT '商品规格参数',
  `spec_desc` varchar(255) DEFAULT '' COMMENT '商品规格描述',
  `sort` bigint(20) unsigned DEFAULT '0' COMMENT '商品规格排序',
  `status` bigint(1) unsigned DEFAULT '1' COMMENT '商品状态(1有效,0无效)',
  `is_deleted` bigint(1) unsigned DEFAULT '0' COMMENT '删除状态(1删除,0未删除)',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `index_store_goods_spec_mch_id` (`mch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商城商品规格';

-- ----------------------------
-- Data for the table `goods_spec`
-- ----------------------------

-- ----------------------------
-- Table structure for `jpush`
-- ----------------------------

DROP TABLE IF EXISTS `jpush`;

CREATE TABLE `jpush` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sort` int(10) unsigned NOT NULL DEFAULT '90',
  `content` mediumtext NOT NULL,
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `user_id` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `jpush`
-- ----------------------------

-- ----------------------------
-- Table structure for `langtrans`
-- ----------------------------

DROP TABLE IF EXISTS `langtrans`;

CREATE TABLE `langtrans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_src` text,
  `to` char(10) DEFAULT NULL,
  `trans_dst` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `langtrans`
-- ----------------------------

INSERT INTO `langtrans` VALUES ('1', '获取appid成功', 'en', 'Get appid success');
INSERT INTO `langtrans` VALUES ('2', '登录账号必输', 'en', 'Login account must be lost.');
INSERT INTO `langtrans` VALUES ('3', '缺少参数registrationID必输', 'en', 'RegistrationID will lose if there is no parameter.');
INSERT INTO `langtrans` VALUES ('4', '登录账号长度不能少于2位有效字符', 'en', 'Login account length can not be less than 2 valid characters.');
INSERT INTO `langtrans` VALUES ('5', '登录密码长度不能少于6位,不能多于16位有效字符', 'en', 'Login password length can not be less than 6 bits, not more than 16 valid characters.');
INSERT INTO `langtrans` VALUES ('6', 'appid必输', 'en', 'Appid must lose');
INSERT INTO `langtrans` VALUES ('7', '密码必输', 'en', 'Password must be lost');
INSERT INTO `langtrans` VALUES ('8', '登录账号不存在，请重新输入', 'en', 'Login account does not exist. Please re input.');
INSERT INTO `langtrans` VALUES ('9', '登录密码与账号不匹配，请重新输入', 'en', 'The login password does not match the account number. Please retype it.');
INSERT INTO `langtrans` VALUES ('10', '账号已经被禁用，请联系管理', 'en', 'Account has been disabled, please contact management.');
INSERT INTO `langtrans` VALUES ('11', '登录成功', 'en', 'Login success');
INSERT INTO `langtrans` VALUES ('12', '登录失败', 'en', 'Login failure');
INSERT INTO `langtrans` VALUES ('13', 'id必输', 'en', 'ID must lose');
INSERT INTO `langtrans` VALUES ('14', 'id不能为零必输', 'en', 'ID must not lose for zero.');
INSERT INTO `langtrans` VALUES ('15', '操作成功', 'en', 'Successful operation');
INSERT INTO `langtrans` VALUES ('16', 'ID信息不存在', 'en', 'ID information does not exist.');
INSERT INTO `langtrans` VALUES ('17', '返回数据为空', 'en', 'Return data is empty.');
INSERT INTO `langtrans` VALUES ('18', '缺少参数id', 'en', 'Missing parameters ID');
INSERT INTO `langtrans` VALUES ('19', '参数id必需大于0', 'en', 'Parameter ID must be greater than 0.');
INSERT INTO `langtrans` VALUES ('20', '参数doorname不能是空', 'en', 'Parameter doorname can not be empty.');
INSERT INTO `langtrans` VALUES ('21', '参数设备名称，wifi控制器ID和位置传感器id不能都为空', 'en', 'The parameter device name, WiFi controller ID and location sensor ID can not be empty.');
INSERT INTO `langtrans` VALUES ('22', '设备名称已经存在', 'en', 'Device name already exists.');
INSERT INTO `langtrans` VALUES ('23', '该记录不存在', 'en', 'The record does not exist.');
INSERT INTO `langtrans` VALUES ('24', '修改成功', 'en', 'Amend the success');
INSERT INTO `langtrans` VALUES ('25', '修改失败', 'en', 'Failure to modify');
INSERT INTO `langtrans` VALUES ('26', '删除成功', 'en', 'Delete success');
INSERT INTO `langtrans` VALUES ('27', '删除失败', 'en', 'Delete failure');
INSERT INTO `langtrans` VALUES ('28', '查询数据为空', 'en', 'Query data is empty.');
INSERT INTO `langtrans` VALUES ('29', '用户名长度不能少于2位有效字符,不能多于15位有效字符,且不能是纯数字', 'en', 'The length of the user name can not be less than 2 valid characters, not more than 15 valid characters, and cannot be a pure number.');
INSERT INTO `langtrans` VALUES ('30', '登录账号长度不能少于2位有效字符,不能多于15位有效字符', 'en', 'Login account length can not be less than 2 valid characters, not more than 15 valid characters.');
INSERT INTO `langtrans` VALUES ('31', '登录密码长度不能少于6位,不能多于16位有效字符', 'en', 'Login password length can not be less than 6 bits, not more than 16 valid characters.');
INSERT INTO `langtrans` VALUES ('32', '邮箱不能必输', 'en', 'Mailbox must not be lost.');
INSERT INTO `langtrans` VALUES ('33', '登录账号必输', 'en', 'Login account must be lost.');
INSERT INTO `langtrans` VALUES ('34', '注册成功', 'en', 'login was successful');
INSERT INTO `langtrans` VALUES ('35', '注册失败', 'en', 'login has failed');
INSERT INTO `langtrans` VALUES ('36', '邮箱已注册,请更换邮箱', 'en', 'The mailbox is registered. Please change the mailbox.');
INSERT INTO `langtrans` VALUES ('37', '邮箱格式错误', 'en', 'Error in mailbox format');
INSERT INTO `langtrans` VALUES ('38', '用户名已注册,请更换用户名', 'en', 'User name has been registered, please change user name.');
INSERT INTO `langtrans` VALUES ('39', '登录账号id，或者token为空', 'en', 'Login account ID, or token is empty.');
INSERT INTO `langtrans` VALUES ('40', '用户信息不存在', 'en', 'User information does not exist.');
INSERT INTO `langtrans` VALUES ('41', '控制器名称必输', 'en', 'Controller name must be lost.');
INSERT INTO `langtrans` VALUES ('42', '设备名称长度不能少于2位,不能多于30位有效字符', 'en', 'The length of the device name can not be less than 2 bits, not more than 30 valid characters.');
INSERT INTO `langtrans` VALUES ('43', '控制器名称已经存在', 'en', 'Controller name already exists.');
INSERT INTO `langtrans` VALUES ('44', '新增成功', 'en', 'New success');
INSERT INTO `langtrans` VALUES ('45', '新增失败', 'en', 'New failure');
INSERT INTO `langtrans` VALUES ('46', '登录账号长度不能少于4位有效字符', 'en', 'Login account length can not be less than 4 valid characters.');
INSERT INTO `langtrans` VALUES ('47', '登录密码长度不能少于4位有效字符', 'en', 'The length of the login password can not be less than 4 bit valid characters.');
INSERT INTO `langtrans` VALUES ('48', '登录账号必输', 'en', 'Login account must be lost.');
INSERT INTO `langtrans` VALUES ('49', '登录账号不存在,请重新输入', 'en', 'Login account does not exist. Please re input.');
INSERT INTO `langtrans` VALUES ('50', '登录密码与账号不匹配,请重新输入', 'en', 'The login password does not match the account number. Please retype it.');
INSERT INTO `langtrans` VALUES ('51', '账号已经被禁用,请联系管理', 'en', 'Account has been disabled, please contact management.');
INSERT INTO `langtrans` VALUES ('52', '成功注销', 'en', 'Successful cancellation');

-- ----------------------------
-- Table structure for `member`
-- ----------------------------

DROP TABLE IF EXISTS `member`;

CREATE TABLE `member` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `headimage` mediumtext,
  `regional_Linkage` varchar(250) NOT NULL DEFAULT '',
  `sort` int(10) unsigned NOT NULL DEFAULT '90',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `member`
-- ----------------------------

INSERT INTO `member` VALUES ('1', 'zzzeeee', '', 'p:110101:东城区,c:110100:北京市,d:110000:北京', '90', '1538812614', '1538988661', '1');

-- ----------------------------
-- Table structure for `store_goods_cate`
-- ----------------------------

DROP TABLE IF EXISTS `store_goods_cate`;

CREATE TABLE `store_goods_cate` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) unsigned DEFAULT '0' COMMENT '上级分类编号',
  `brand_id` bigint(20) DEFAULT '0' COMMENT '品牌ID',
  `cate_title` varchar(255) DEFAULT '' COMMENT '商品分类名称',
  `cate_desc` text COMMENT '商品分类',
  `sort` bigint(20) unsigned DEFAULT '0' COMMENT '商品分类排序',
  `status` bigint(1) unsigned DEFAULT '1' COMMENT '商品状态(1有效,0无效)',
  `is_deleted` bigint(1) unsigned DEFAULT '0' COMMENT '删除状态(1删除,0未删除)',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='商城商品分类';

-- ----------------------------
-- Data for the table `store_goods_cate`
-- ----------------------------

INSERT INTO `store_goods_cate` VALUES ('1', '0', '0', '1', '', '0', '1', '0', '2018-06-20 14:29:02');
INSERT INTO `store_goods_cate` VALUES ('2', '1', '0', '2', '', '0', '1', '0', '2018-06-20 14:29:19');
INSERT INTO `store_goods_cate` VALUES ('3', '2', '0', '3', '', '0', '1', '0', '2018-06-20 14:29:30');
INSERT INTO `store_goods_cate` VALUES ('4', '3', '0', '4', '', '0', '1', '0', '2018-06-20 14:29:51');
INSERT INTO `store_goods_cate` VALUES ('5', '2', '0', '2.1', '', '0', '1', '0', '2018-06-20 14:30:01');
INSERT INTO `store_goods_cate` VALUES ('6', '1', '0', '1.1', '', '0', '1', '0', '2018-06-20 14:30:15');

-- ----------------------------
-- Table structure for `system_areabase`
-- ----------------------------

DROP TABLE IF EXISTS `system_areabase`;

CREATE TABLE `system_areabase` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sort` int(10) unsigned NOT NULL DEFAULT '90',
  `p_id` int(10) unsigned NOT NULL DEFAULT '0',
  `area_bn` int(10) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3707 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `system_areabase`
-- ----------------------------

INSERT INTO `system_areabase` VALUES ('1', '90', '0', '110000', '0', '0', '1', '北京');
INSERT INTO `system_areabase` VALUES ('2', '90', '110000', '110100', '0', '0', '1', '北京市');
INSERT INTO `system_areabase` VALUES ('3', '90', '110100', '110101', '0', '0', '1', '东城区');
INSERT INTO `system_areabase` VALUES ('4', '90', '110100', '110102', '0', '0', '1', '西城区');
INSERT INTO `system_areabase` VALUES ('5', '90', '110100', '110103', '0', '0', '1', '崇文区');
INSERT INTO `system_areabase` VALUES ('6', '90', '110100', '110104', '0', '0', '1', '宣武区');
INSERT INTO `system_areabase` VALUES ('7', '90', '110100', '110105', '0', '0', '1', '朝阳区');
INSERT INTO `system_areabase` VALUES ('8', '90', '110100', '110106', '0', '0', '1', '丰台区');
INSERT INTO `system_areabase` VALUES ('9', '90', '110100', '110107', '0', '0', '1', '石景山区');
INSERT INTO `system_areabase` VALUES ('10', '90', '110100', '110108', '0', '0', '1', '海淀区');
INSERT INTO `system_areabase` VALUES ('11', '90', '110100', '110109', '0', '0', '1', '门头沟区');
INSERT INTO `system_areabase` VALUES ('12', '90', '110100', '110111', '0', '0', '1', '房山区');
INSERT INTO `system_areabase` VALUES ('13', '90', '110100', '110112', '0', '0', '1', '通州区');
INSERT INTO `system_areabase` VALUES ('14', '90', '110100', '110113', '0', '0', '1', '顺义区');
INSERT INTO `system_areabase` VALUES ('15', '90', '110100', '110114', '0', '0', '1', '昌平区');
INSERT INTO `system_areabase` VALUES ('16', '90', '110100', '110115', '0', '0', '1', '大兴区');
INSERT INTO `system_areabase` VALUES ('17', '90', '110100', '110116', '0', '0', '1', '怀柔区');
INSERT INTO `system_areabase` VALUES ('18', '90', '110100', '110117', '0', '0', '1', '平谷区');
INSERT INTO `system_areabase` VALUES ('19', '90', '110100', '110228', '0', '0', '1', '密云县');
INSERT INTO `system_areabase` VALUES ('20', '90', '110100', '110229', '0', '0', '1', '延庆县');
INSERT INTO `system_areabase` VALUES ('21', '90', '110100', '110230', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('22', '90', '0', '120000', '0', '0', '1', '天津');
INSERT INTO `system_areabase` VALUES ('23', '90', '120000', '120100', '0', '0', '1', '天津市');
INSERT INTO `system_areabase` VALUES ('24', '90', '120100', '120101', '0', '0', '1', '和平区');
INSERT INTO `system_areabase` VALUES ('25', '90', '120100', '120102', '0', '0', '1', '河东区');
INSERT INTO `system_areabase` VALUES ('26', '90', '120100', '120103', '0', '0', '1', '河西区');
INSERT INTO `system_areabase` VALUES ('27', '90', '120100', '120104', '0', '0', '1', '南开区');
INSERT INTO `system_areabase` VALUES ('28', '90', '120100', '120105', '0', '0', '1', '河北区');
INSERT INTO `system_areabase` VALUES ('29', '90', '120100', '120106', '0', '0', '1', '红桥区');
INSERT INTO `system_areabase` VALUES ('30', '90', '120100', '120107', '0', '0', '1', '塘沽区');
INSERT INTO `system_areabase` VALUES ('31', '90', '120100', '120108', '0', '0', '1', '汉沽区');
INSERT INTO `system_areabase` VALUES ('32', '90', '120100', '120109', '0', '0', '1', '大港区');
INSERT INTO `system_areabase` VALUES ('33', '90', '120100', '120110', '0', '0', '1', '东丽区');
INSERT INTO `system_areabase` VALUES ('34', '90', '120100', '120111', '0', '0', '1', '西青区');
INSERT INTO `system_areabase` VALUES ('35', '90', '120100', '120112', '0', '0', '1', '津南区');
INSERT INTO `system_areabase` VALUES ('36', '90', '120100', '120113', '0', '0', '1', '北辰区');
INSERT INTO `system_areabase` VALUES ('37', '90', '120100', '120114', '0', '0', '1', '武清区');
INSERT INTO `system_areabase` VALUES ('38', '90', '120100', '120115', '0', '0', '1', '宝坻区');
INSERT INTO `system_areabase` VALUES ('39', '90', '120100', '120221', '0', '0', '1', '宁河县');
INSERT INTO `system_areabase` VALUES ('40', '90', '120100', '120223', '0', '0', '1', '静海县');
INSERT INTO `system_areabase` VALUES ('41', '90', '120100', '120225', '0', '0', '1', '蓟县');
INSERT INTO `system_areabase` VALUES ('42', '90', '120100', '120226', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('43', '90', '0', '130000', '0', '0', '1', '河北省');
INSERT INTO `system_areabase` VALUES ('44', '90', '130000', '130100', '0', '0', '1', '石家庄市');
INSERT INTO `system_areabase` VALUES ('45', '90', '130100', '130102', '0', '0', '1', '长安区');
INSERT INTO `system_areabase` VALUES ('46', '90', '130100', '130103', '0', '0', '1', '桥东区');
INSERT INTO `system_areabase` VALUES ('47', '90', '130100', '130104', '0', '0', '1', '桥西区');
INSERT INTO `system_areabase` VALUES ('48', '90', '130100', '130105', '0', '0', '1', '新华区');
INSERT INTO `system_areabase` VALUES ('49', '90', '130100', '130107', '0', '0', '1', '井陉矿区');
INSERT INTO `system_areabase` VALUES ('50', '90', '130100', '130108', '0', '0', '1', '裕华区');
INSERT INTO `system_areabase` VALUES ('51', '90', '130100', '130121', '0', '0', '1', '井陉县');
INSERT INTO `system_areabase` VALUES ('52', '90', '130100', '130123', '0', '0', '1', '正定县');
INSERT INTO `system_areabase` VALUES ('53', '90', '130100', '130124', '0', '0', '1', '栾城县');
INSERT INTO `system_areabase` VALUES ('54', '90', '130100', '130125', '0', '0', '1', '行唐县');
INSERT INTO `system_areabase` VALUES ('55', '90', '130100', '130126', '0', '0', '1', '灵寿县');
INSERT INTO `system_areabase` VALUES ('56', '90', '130100', '130127', '0', '0', '1', '高邑县');
INSERT INTO `system_areabase` VALUES ('57', '90', '130100', '130128', '0', '0', '1', '深泽县');
INSERT INTO `system_areabase` VALUES ('58', '90', '130100', '130129', '0', '0', '1', '赞皇县');
INSERT INTO `system_areabase` VALUES ('59', '90', '130100', '130130', '0', '0', '1', '无极县');
INSERT INTO `system_areabase` VALUES ('60', '90', '130100', '130131', '0', '0', '1', '平山县');
INSERT INTO `system_areabase` VALUES ('61', '90', '130100', '130132', '0', '0', '1', '元氏县');
INSERT INTO `system_areabase` VALUES ('62', '90', '130100', '130133', '0', '0', '1', '赵县');
INSERT INTO `system_areabase` VALUES ('63', '90', '130100', '130181', '0', '0', '1', '辛集市');
INSERT INTO `system_areabase` VALUES ('64', '90', '130100', '130182', '0', '0', '1', '藁城市');
INSERT INTO `system_areabase` VALUES ('65', '90', '130100', '130183', '0', '0', '1', '晋州市');
INSERT INTO `system_areabase` VALUES ('66', '90', '130100', '130184', '0', '0', '1', '新乐市');
INSERT INTO `system_areabase` VALUES ('67', '90', '130100', '130185', '0', '0', '1', '鹿泉市');
INSERT INTO `system_areabase` VALUES ('68', '90', '130100', '130186', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('69', '90', '130000', '130200', '0', '0', '1', '唐山市');
INSERT INTO `system_areabase` VALUES ('70', '90', '130200', '130202', '0', '0', '1', '路南区');
INSERT INTO `system_areabase` VALUES ('71', '90', '130200', '130203', '0', '0', '1', '路北区');
INSERT INTO `system_areabase` VALUES ('72', '90', '130200', '130204', '0', '0', '1', '古冶区');
INSERT INTO `system_areabase` VALUES ('73', '90', '130200', '130205', '0', '0', '1', '开平区');
INSERT INTO `system_areabase` VALUES ('74', '90', '130200', '130207', '0', '0', '1', '丰南区');
INSERT INTO `system_areabase` VALUES ('75', '90', '130200', '130208', '0', '0', '1', '丰润区');
INSERT INTO `system_areabase` VALUES ('76', '90', '130200', '130223', '0', '0', '1', '滦县');
INSERT INTO `system_areabase` VALUES ('77', '90', '130200', '130224', '0', '0', '1', '滦南县');
INSERT INTO `system_areabase` VALUES ('78', '90', '130200', '130225', '0', '0', '1', '乐亭县');
INSERT INTO `system_areabase` VALUES ('79', '90', '130200', '130227', '0', '0', '1', '迁西县');
INSERT INTO `system_areabase` VALUES ('80', '90', '130200', '130229', '0', '0', '1', '玉田县');
INSERT INTO `system_areabase` VALUES ('81', '90', '130200', '130230', '0', '0', '1', '唐海县');
INSERT INTO `system_areabase` VALUES ('82', '90', '130200', '130281', '0', '0', '1', '遵化市');
INSERT INTO `system_areabase` VALUES ('83', '90', '130200', '130283', '0', '0', '1', '迁安市');
INSERT INTO `system_areabase` VALUES ('84', '90', '130200', '130284', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('85', '90', '130000', '130300', '0', '0', '1', '秦皇岛市');
INSERT INTO `system_areabase` VALUES ('86', '90', '130300', '130302', '0', '0', '1', '海港区');
INSERT INTO `system_areabase` VALUES ('87', '90', '130300', '130303', '0', '0', '1', '山海关区');
INSERT INTO `system_areabase` VALUES ('88', '90', '130300', '130304', '0', '0', '1', '北戴河区');
INSERT INTO `system_areabase` VALUES ('89', '90', '130300', '130321', '0', '0', '1', '青龙满族自治县');
INSERT INTO `system_areabase` VALUES ('90', '90', '130300', '130322', '0', '0', '1', '昌黎县');
INSERT INTO `system_areabase` VALUES ('91', '90', '130300', '130323', '0', '0', '1', '抚宁县');
INSERT INTO `system_areabase` VALUES ('92', '90', '130300', '130324', '0', '0', '1', '卢龙县');
INSERT INTO `system_areabase` VALUES ('93', '90', '130300', '130398', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('94', '90', '130300', '130399', '0', '0', '1', '经济技术开发区');
INSERT INTO `system_areabase` VALUES ('95', '90', '130000', '130400', '0', '0', '1', '邯郸市');
INSERT INTO `system_areabase` VALUES ('96', '90', '130400', '130402', '0', '0', '1', '邯山区');
INSERT INTO `system_areabase` VALUES ('97', '90', '130400', '130403', '0', '0', '1', '丛台区');
INSERT INTO `system_areabase` VALUES ('98', '90', '130400', '130404', '0', '0', '1', '复兴区');
INSERT INTO `system_areabase` VALUES ('99', '90', '130400', '130406', '0', '0', '1', '峰峰矿区');
INSERT INTO `system_areabase` VALUES ('100', '90', '130400', '130421', '0', '0', '1', '邯郸县');
INSERT INTO `system_areabase` VALUES ('101', '90', '130400', '130423', '0', '0', '1', '临漳县');
INSERT INTO `system_areabase` VALUES ('102', '90', '130400', '130424', '0', '0', '1', '成安县');
INSERT INTO `system_areabase` VALUES ('103', '90', '130400', '130425', '0', '0', '1', '大名县');
INSERT INTO `system_areabase` VALUES ('104', '90', '130400', '130426', '0', '0', '1', '涉县');
INSERT INTO `system_areabase` VALUES ('105', '90', '130400', '130427', '0', '0', '1', '磁县');
INSERT INTO `system_areabase` VALUES ('106', '90', '130400', '130428', '0', '0', '1', '肥乡县');
INSERT INTO `system_areabase` VALUES ('107', '90', '130400', '130429', '0', '0', '1', '永年县');
INSERT INTO `system_areabase` VALUES ('108', '90', '130400', '130430', '0', '0', '1', '邱县');
INSERT INTO `system_areabase` VALUES ('109', '90', '130400', '130431', '0', '0', '1', '鸡泽县');
INSERT INTO `system_areabase` VALUES ('110', '90', '130400', '130432', '0', '0', '1', '广平县');
INSERT INTO `system_areabase` VALUES ('111', '90', '130400', '130433', '0', '0', '1', '馆陶县');
INSERT INTO `system_areabase` VALUES ('112', '90', '130400', '130434', '0', '0', '1', '魏县');
INSERT INTO `system_areabase` VALUES ('113', '90', '130400', '130435', '0', '0', '1', '曲周县');
INSERT INTO `system_areabase` VALUES ('114', '90', '130400', '130481', '0', '0', '1', '武安市');
INSERT INTO `system_areabase` VALUES ('115', '90', '130400', '130482', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('116', '90', '130000', '130500', '0', '0', '1', '邢台市');
INSERT INTO `system_areabase` VALUES ('117', '90', '130500', '130502', '0', '0', '1', '桥东区');
INSERT INTO `system_areabase` VALUES ('118', '90', '130500', '130503', '0', '0', '1', '桥西区');
INSERT INTO `system_areabase` VALUES ('119', '90', '130500', '130521', '0', '0', '1', '邢台县');
INSERT INTO `system_areabase` VALUES ('120', '90', '130500', '130522', '0', '0', '1', '临城县');
INSERT INTO `system_areabase` VALUES ('121', '90', '130500', '130523', '0', '0', '1', '内丘县');
INSERT INTO `system_areabase` VALUES ('122', '90', '130500', '130524', '0', '0', '1', '柏乡县');
INSERT INTO `system_areabase` VALUES ('123', '90', '130500', '130525', '0', '0', '1', '隆尧县');
INSERT INTO `system_areabase` VALUES ('124', '90', '130500', '130526', '0', '0', '1', '任县');
INSERT INTO `system_areabase` VALUES ('125', '90', '130500', '130527', '0', '0', '1', '南和县');
INSERT INTO `system_areabase` VALUES ('126', '90', '130500', '130528', '0', '0', '1', '宁晋县');
INSERT INTO `system_areabase` VALUES ('127', '90', '130500', '130529', '0', '0', '1', '巨鹿县');
INSERT INTO `system_areabase` VALUES ('128', '90', '130500', '130530', '0', '0', '1', '新河县');
INSERT INTO `system_areabase` VALUES ('129', '90', '130500', '130531', '0', '0', '1', '广宗县');
INSERT INTO `system_areabase` VALUES ('130', '90', '130500', '130532', '0', '0', '1', '平乡县');
INSERT INTO `system_areabase` VALUES ('131', '90', '130500', '130533', '0', '0', '1', '威县');
INSERT INTO `system_areabase` VALUES ('132', '90', '130500', '130534', '0', '0', '1', '清河县');
INSERT INTO `system_areabase` VALUES ('133', '90', '130500', '130535', '0', '0', '1', '临西县');
INSERT INTO `system_areabase` VALUES ('134', '90', '130500', '130581', '0', '0', '1', '南宫市');
INSERT INTO `system_areabase` VALUES ('135', '90', '130500', '130582', '0', '0', '1', '沙河市');
INSERT INTO `system_areabase` VALUES ('136', '90', '130500', '130583', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('137', '90', '130000', '130600', '0', '0', '1', '保定市');
INSERT INTO `system_areabase` VALUES ('138', '90', '130600', '130602', '0', '0', '1', '新市区');
INSERT INTO `system_areabase` VALUES ('139', '90', '130600', '130603', '0', '0', '1', '北市区');
INSERT INTO `system_areabase` VALUES ('140', '90', '130600', '130604', '0', '0', '1', '南市区');
INSERT INTO `system_areabase` VALUES ('141', '90', '130600', '130621', '0', '0', '1', '满城县');
INSERT INTO `system_areabase` VALUES ('142', '90', '130600', '130622', '0', '0', '1', '清苑县');
INSERT INTO `system_areabase` VALUES ('143', '90', '130600', '130623', '0', '0', '1', '涞水县');
INSERT INTO `system_areabase` VALUES ('144', '90', '130600', '130624', '0', '0', '1', '阜平县');
INSERT INTO `system_areabase` VALUES ('145', '90', '130600', '130625', '0', '0', '1', '徐水县');
INSERT INTO `system_areabase` VALUES ('146', '90', '130600', '130626', '0', '0', '1', '定兴县');
INSERT INTO `system_areabase` VALUES ('147', '90', '130600', '130627', '0', '0', '1', '唐县');
INSERT INTO `system_areabase` VALUES ('148', '90', '130600', '130628', '0', '0', '1', '高阳县');
INSERT INTO `system_areabase` VALUES ('149', '90', '130600', '130629', '0', '0', '1', '容城县');
INSERT INTO `system_areabase` VALUES ('150', '90', '130600', '130630', '0', '0', '1', '涞源县');
INSERT INTO `system_areabase` VALUES ('151', '90', '130600', '130631', '0', '0', '1', '望都县');
INSERT INTO `system_areabase` VALUES ('152', '90', '130600', '130632', '0', '0', '1', '安新县');
INSERT INTO `system_areabase` VALUES ('153', '90', '130600', '130633', '0', '0', '1', '易县');
INSERT INTO `system_areabase` VALUES ('154', '90', '130600', '130634', '0', '0', '1', '曲阳县');
INSERT INTO `system_areabase` VALUES ('155', '90', '130600', '130635', '0', '0', '1', '蠡县');
INSERT INTO `system_areabase` VALUES ('156', '90', '130600', '130636', '0', '0', '1', '顺平县');
INSERT INTO `system_areabase` VALUES ('157', '90', '130600', '130637', '0', '0', '1', '博野县');
INSERT INTO `system_areabase` VALUES ('158', '90', '130600', '130638', '0', '0', '1', '雄县');
INSERT INTO `system_areabase` VALUES ('159', '90', '130600', '130681', '0', '0', '1', '涿州市');
INSERT INTO `system_areabase` VALUES ('160', '90', '130600', '130682', '0', '0', '1', '定州市');
INSERT INTO `system_areabase` VALUES ('161', '90', '130600', '130683', '0', '0', '1', '安国市');
INSERT INTO `system_areabase` VALUES ('162', '90', '130600', '130684', '0', '0', '1', '高碑店市');
INSERT INTO `system_areabase` VALUES ('163', '90', '130600', '130698', '0', '0', '1', '高开区');
INSERT INTO `system_areabase` VALUES ('164', '90', '130600', '130699', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('165', '90', '130000', '130700', '0', '0', '1', '张家口市');
INSERT INTO `system_areabase` VALUES ('166', '90', '130700', '130702', '0', '0', '1', '桥东区');
INSERT INTO `system_areabase` VALUES ('167', '90', '130700', '130703', '0', '0', '1', '桥西区');
INSERT INTO `system_areabase` VALUES ('168', '90', '130700', '130705', '0', '0', '1', '宣化区');
INSERT INTO `system_areabase` VALUES ('169', '90', '130700', '130706', '0', '0', '1', '下花园区');
INSERT INTO `system_areabase` VALUES ('170', '90', '130700', '130721', '0', '0', '1', '宣化县');
INSERT INTO `system_areabase` VALUES ('171', '90', '130700', '130722', '0', '0', '1', '张北县');
INSERT INTO `system_areabase` VALUES ('172', '90', '130700', '130723', '0', '0', '1', '康保县');
INSERT INTO `system_areabase` VALUES ('173', '90', '130700', '130724', '0', '0', '1', '沽源县');
INSERT INTO `system_areabase` VALUES ('174', '90', '130700', '130725', '0', '0', '1', '尚义县');
INSERT INTO `system_areabase` VALUES ('175', '90', '130700', '130726', '0', '0', '1', '蔚县');
INSERT INTO `system_areabase` VALUES ('176', '90', '130700', '130727', '0', '0', '1', '阳原县');
INSERT INTO `system_areabase` VALUES ('177', '90', '130700', '130728', '0', '0', '1', '怀安县');
INSERT INTO `system_areabase` VALUES ('178', '90', '130700', '130729', '0', '0', '1', '万全县');
INSERT INTO `system_areabase` VALUES ('179', '90', '130700', '130730', '0', '0', '1', '怀来县');
INSERT INTO `system_areabase` VALUES ('180', '90', '130700', '130731', '0', '0', '1', '涿鹿县');
INSERT INTO `system_areabase` VALUES ('181', '90', '130700', '130732', '0', '0', '1', '赤城县');
INSERT INTO `system_areabase` VALUES ('182', '90', '130700', '130733', '0', '0', '1', '崇礼县');
INSERT INTO `system_areabase` VALUES ('183', '90', '130700', '130734', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('184', '90', '130000', '130800', '0', '0', '1', '承德市');
INSERT INTO `system_areabase` VALUES ('185', '90', '130800', '130802', '0', '0', '1', '双桥区');
INSERT INTO `system_areabase` VALUES ('186', '90', '130800', '130803', '0', '0', '1', '双滦区');
INSERT INTO `system_areabase` VALUES ('187', '90', '130800', '130804', '0', '0', '1', '鹰手营子矿区');
INSERT INTO `system_areabase` VALUES ('188', '90', '130800', '130821', '0', '0', '1', '承德县');
INSERT INTO `system_areabase` VALUES ('189', '90', '130800', '130822', '0', '0', '1', '兴隆县');
INSERT INTO `system_areabase` VALUES ('190', '90', '130800', '130823', '0', '0', '1', '平泉县');
INSERT INTO `system_areabase` VALUES ('191', '90', '130800', '130824', '0', '0', '1', '滦平县');
INSERT INTO `system_areabase` VALUES ('192', '90', '130800', '130825', '0', '0', '1', '隆化县');
INSERT INTO `system_areabase` VALUES ('193', '90', '130800', '130826', '0', '0', '1', '丰宁满族自治县');
INSERT INTO `system_areabase` VALUES ('194', '90', '130800', '130827', '0', '0', '1', '宽城满族自治县');
INSERT INTO `system_areabase` VALUES ('195', '90', '130800', '130828', '0', '0', '1', '围场满族蒙古族自治县');
INSERT INTO `system_areabase` VALUES ('196', '90', '130800', '130829', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('197', '90', '130000', '130900', '0', '0', '1', '沧州市');
INSERT INTO `system_areabase` VALUES ('198', '90', '130900', '130902', '0', '0', '1', '新华区');
INSERT INTO `system_areabase` VALUES ('199', '90', '130900', '130903', '0', '0', '1', '运河区');
INSERT INTO `system_areabase` VALUES ('200', '90', '130900', '130921', '0', '0', '1', '沧县');
INSERT INTO `system_areabase` VALUES ('201', '90', '130900', '130922', '0', '0', '1', '青县');
INSERT INTO `system_areabase` VALUES ('202', '90', '130900', '130923', '0', '0', '1', '东光县');
INSERT INTO `system_areabase` VALUES ('203', '90', '130900', '130924', '0', '0', '1', '海兴县');
INSERT INTO `system_areabase` VALUES ('204', '90', '130900', '130925', '0', '0', '1', '盐山县');
INSERT INTO `system_areabase` VALUES ('205', '90', '130900', '130926', '0', '0', '1', '肃宁县');
INSERT INTO `system_areabase` VALUES ('206', '90', '130900', '130927', '0', '0', '1', '南皮县');
INSERT INTO `system_areabase` VALUES ('207', '90', '130900', '130928', '0', '0', '1', '吴桥县');
INSERT INTO `system_areabase` VALUES ('208', '90', '130900', '130929', '0', '0', '1', '献县');
INSERT INTO `system_areabase` VALUES ('209', '90', '130900', '130930', '0', '0', '1', '孟村回族自治县');
INSERT INTO `system_areabase` VALUES ('210', '90', '130900', '130981', '0', '0', '1', '泊头市');
INSERT INTO `system_areabase` VALUES ('211', '90', '130900', '130982', '0', '0', '1', '任丘市');
INSERT INTO `system_areabase` VALUES ('212', '90', '130900', '130983', '0', '0', '1', '黄骅市');
INSERT INTO `system_areabase` VALUES ('213', '90', '130900', '130984', '0', '0', '1', '河间市');
INSERT INTO `system_areabase` VALUES ('214', '90', '130900', '130985', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('215', '90', '130000', '131000', '0', '0', '1', '廊坊市');
INSERT INTO `system_areabase` VALUES ('216', '90', '131000', '131002', '0', '0', '1', '安次区');
INSERT INTO `system_areabase` VALUES ('217', '90', '131000', '131003', '0', '0', '1', '广阳区');
INSERT INTO `system_areabase` VALUES ('218', '90', '131000', '131022', '0', '0', '1', '固安县');
INSERT INTO `system_areabase` VALUES ('219', '90', '131000', '131023', '0', '0', '1', '永清县');
INSERT INTO `system_areabase` VALUES ('220', '90', '131000', '131024', '0', '0', '1', '香河县');
INSERT INTO `system_areabase` VALUES ('221', '90', '131000', '131025', '0', '0', '1', '大城县');
INSERT INTO `system_areabase` VALUES ('222', '90', '131000', '131026', '0', '0', '1', '文安县');
INSERT INTO `system_areabase` VALUES ('223', '90', '131000', '131028', '0', '0', '1', '大厂回族自治县');
INSERT INTO `system_areabase` VALUES ('224', '90', '131000', '131051', '0', '0', '1', '开发区');
INSERT INTO `system_areabase` VALUES ('225', '90', '131000', '131052', '0', '0', '1', '燕郊经济技术开发区');
INSERT INTO `system_areabase` VALUES ('226', '90', '131000', '131081', '0', '0', '1', '霸州市');
INSERT INTO `system_areabase` VALUES ('227', '90', '131000', '131082', '0', '0', '1', '三河市');
INSERT INTO `system_areabase` VALUES ('228', '90', '131000', '131083', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('229', '90', '130000', '131100', '0', '0', '1', '衡水市');
INSERT INTO `system_areabase` VALUES ('230', '90', '131100', '131102', '0', '0', '1', '桃城区');
INSERT INTO `system_areabase` VALUES ('231', '90', '131100', '131121', '0', '0', '1', '枣强县');
INSERT INTO `system_areabase` VALUES ('232', '90', '131100', '131122', '0', '0', '1', '武邑县');
INSERT INTO `system_areabase` VALUES ('233', '90', '131100', '131123', '0', '0', '1', '武强县');
INSERT INTO `system_areabase` VALUES ('234', '90', '131100', '131124', '0', '0', '1', '饶阳县');
INSERT INTO `system_areabase` VALUES ('235', '90', '131100', '131125', '0', '0', '1', '安平县');
INSERT INTO `system_areabase` VALUES ('236', '90', '131100', '131126', '0', '0', '1', '故城县');
INSERT INTO `system_areabase` VALUES ('237', '90', '131100', '131127', '0', '0', '1', '景县');
INSERT INTO `system_areabase` VALUES ('238', '90', '131100', '131128', '0', '0', '1', '阜城县');
INSERT INTO `system_areabase` VALUES ('239', '90', '131100', '131181', '0', '0', '1', '冀州市');
INSERT INTO `system_areabase` VALUES ('240', '90', '131100', '131182', '0', '0', '1', '深州市');
INSERT INTO `system_areabase` VALUES ('241', '90', '131100', '131183', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('242', '90', '0', '140000', '0', '0', '1', '山西省');
INSERT INTO `system_areabase` VALUES ('243', '90', '140000', '140100', '0', '0', '1', '太原市');
INSERT INTO `system_areabase` VALUES ('244', '90', '140100', '140105', '0', '0', '1', '小店区');
INSERT INTO `system_areabase` VALUES ('245', '90', '140100', '140106', '0', '0', '1', '迎泽区');
INSERT INTO `system_areabase` VALUES ('246', '90', '140100', '140107', '0', '0', '1', '杏花岭区');
INSERT INTO `system_areabase` VALUES ('247', '90', '140100', '140108', '0', '0', '1', '尖草坪区');
INSERT INTO `system_areabase` VALUES ('248', '90', '140100', '140109', '0', '0', '1', '万柏林区');
INSERT INTO `system_areabase` VALUES ('249', '90', '140100', '140110', '0', '0', '1', '晋源区');
INSERT INTO `system_areabase` VALUES ('250', '90', '140100', '140121', '0', '0', '1', '清徐县');
INSERT INTO `system_areabase` VALUES ('251', '90', '140100', '140122', '0', '0', '1', '阳曲县');
INSERT INTO `system_areabase` VALUES ('252', '90', '140100', '140123', '0', '0', '1', '娄烦县');
INSERT INTO `system_areabase` VALUES ('253', '90', '140100', '140181', '0', '0', '1', '古交市');
INSERT INTO `system_areabase` VALUES ('254', '90', '140100', '140182', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('255', '90', '140000', '140200', '0', '0', '1', '大同市');
INSERT INTO `system_areabase` VALUES ('256', '90', '140200', '140202', '0', '0', '1', '城区');
INSERT INTO `system_areabase` VALUES ('257', '90', '140200', '140203', '0', '0', '1', '矿区');
INSERT INTO `system_areabase` VALUES ('258', '90', '140200', '140211', '0', '0', '1', '南郊区');
INSERT INTO `system_areabase` VALUES ('259', '90', '140200', '140212', '0', '0', '1', '新荣区');
INSERT INTO `system_areabase` VALUES ('260', '90', '140200', '140221', '0', '0', '1', '阳高县');
INSERT INTO `system_areabase` VALUES ('261', '90', '140200', '140222', '0', '0', '1', '天镇县');
INSERT INTO `system_areabase` VALUES ('262', '90', '140200', '140223', '0', '0', '1', '广灵县');
INSERT INTO `system_areabase` VALUES ('263', '90', '140200', '140224', '0', '0', '1', '灵丘县');
INSERT INTO `system_areabase` VALUES ('264', '90', '140200', '140225', '0', '0', '1', '浑源县');
INSERT INTO `system_areabase` VALUES ('265', '90', '140200', '140226', '0', '0', '1', '左云县');
INSERT INTO `system_areabase` VALUES ('266', '90', '140200', '140227', '0', '0', '1', '大同县');
INSERT INTO `system_areabase` VALUES ('267', '90', '140200', '140228', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('268', '90', '140000', '140300', '0', '0', '1', '阳泉市');
INSERT INTO `system_areabase` VALUES ('269', '90', '140300', '140302', '0', '0', '1', '城区');
INSERT INTO `system_areabase` VALUES ('270', '90', '140300', '140303', '0', '0', '1', '矿区');
INSERT INTO `system_areabase` VALUES ('271', '90', '140300', '140311', '0', '0', '1', '郊区');
INSERT INTO `system_areabase` VALUES ('272', '90', '140300', '140321', '0', '0', '1', '平定县');
INSERT INTO `system_areabase` VALUES ('273', '90', '140300', '140322', '0', '0', '1', '盂县');
INSERT INTO `system_areabase` VALUES ('274', '90', '140300', '140323', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('275', '90', '140000', '140400', '0', '0', '1', '长治市');
INSERT INTO `system_areabase` VALUES ('276', '90', '140400', '140421', '0', '0', '1', '长治县');
INSERT INTO `system_areabase` VALUES ('277', '90', '140400', '140423', '0', '0', '1', '襄垣县');
INSERT INTO `system_areabase` VALUES ('278', '90', '140400', '140424', '0', '0', '1', '屯留县');
INSERT INTO `system_areabase` VALUES ('279', '90', '140400', '140425', '0', '0', '1', '平顺县');
INSERT INTO `system_areabase` VALUES ('280', '90', '140400', '140426', '0', '0', '1', '黎城县');
INSERT INTO `system_areabase` VALUES ('281', '90', '140400', '140427', '0', '0', '1', '壶关县');
INSERT INTO `system_areabase` VALUES ('282', '90', '140400', '140428', '0', '0', '1', '长子县');
INSERT INTO `system_areabase` VALUES ('283', '90', '140400', '140429', '0', '0', '1', '武乡县');
INSERT INTO `system_areabase` VALUES ('284', '90', '140400', '140430', '0', '0', '1', '沁县');
INSERT INTO `system_areabase` VALUES ('285', '90', '140400', '140431', '0', '0', '1', '沁源县');
INSERT INTO `system_areabase` VALUES ('286', '90', '140400', '140481', '0', '0', '1', '潞城市');
INSERT INTO `system_areabase` VALUES ('287', '90', '140400', '140482', '0', '0', '1', '城区');
INSERT INTO `system_areabase` VALUES ('288', '90', '140400', '140483', '0', '0', '1', '郊区');
INSERT INTO `system_areabase` VALUES ('289', '90', '140400', '140484', '0', '0', '1', '高新区');
INSERT INTO `system_areabase` VALUES ('290', '90', '140400', '140485', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('291', '90', '140000', '140500', '0', '0', '1', '晋城市');
INSERT INTO `system_areabase` VALUES ('292', '90', '140500', '140502', '0', '0', '1', '城区');
INSERT INTO `system_areabase` VALUES ('293', '90', '140500', '140521', '0', '0', '1', '沁水县');
INSERT INTO `system_areabase` VALUES ('294', '90', '140500', '140522', '0', '0', '1', '阳城县');
INSERT INTO `system_areabase` VALUES ('295', '90', '140500', '140524', '0', '0', '1', '陵川县');
INSERT INTO `system_areabase` VALUES ('296', '90', '140500', '140525', '0', '0', '1', '泽州县');
INSERT INTO `system_areabase` VALUES ('297', '90', '140500', '140581', '0', '0', '1', '高平市');
INSERT INTO `system_areabase` VALUES ('298', '90', '140500', '140582', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('299', '90', '140000', '140600', '0', '0', '1', '朔州市');
INSERT INTO `system_areabase` VALUES ('300', '90', '140600', '140602', '0', '0', '1', '朔城区');
INSERT INTO `system_areabase` VALUES ('301', '90', '140600', '140603', '0', '0', '1', '平鲁区');
INSERT INTO `system_areabase` VALUES ('302', '90', '140600', '140621', '0', '0', '1', '山阴县');
INSERT INTO `system_areabase` VALUES ('303', '90', '140600', '140622', '0', '0', '1', '应县');
INSERT INTO `system_areabase` VALUES ('304', '90', '140600', '140623', '0', '0', '1', '右玉县');
INSERT INTO `system_areabase` VALUES ('305', '90', '140600', '140624', '0', '0', '1', '怀仁县');
INSERT INTO `system_areabase` VALUES ('306', '90', '140600', '140625', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('307', '90', '140000', '140700', '0', '0', '1', '晋中市');
INSERT INTO `system_areabase` VALUES ('308', '90', '140700', '140702', '0', '0', '1', '榆次区');
INSERT INTO `system_areabase` VALUES ('309', '90', '140700', '140721', '0', '0', '1', '榆社县');
INSERT INTO `system_areabase` VALUES ('310', '90', '140700', '140722', '0', '0', '1', '左权县');
INSERT INTO `system_areabase` VALUES ('311', '90', '140700', '140723', '0', '0', '1', '和顺县');
INSERT INTO `system_areabase` VALUES ('312', '90', '140700', '140724', '0', '0', '1', '昔阳县');
INSERT INTO `system_areabase` VALUES ('313', '90', '140700', '140725', '0', '0', '1', '寿阳县');
INSERT INTO `system_areabase` VALUES ('314', '90', '140700', '140726', '0', '0', '1', '太谷县');
INSERT INTO `system_areabase` VALUES ('315', '90', '140700', '140727', '0', '0', '1', '祁县');
INSERT INTO `system_areabase` VALUES ('316', '90', '140700', '140728', '0', '0', '1', '平遥县');
INSERT INTO `system_areabase` VALUES ('317', '90', '140700', '140729', '0', '0', '1', '灵石县');
INSERT INTO `system_areabase` VALUES ('318', '90', '140700', '140781', '0', '0', '1', '介休市');
INSERT INTO `system_areabase` VALUES ('319', '90', '140700', '140782', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('320', '90', '140000', '140800', '0', '0', '1', '运城市');
INSERT INTO `system_areabase` VALUES ('321', '90', '140800', '140802', '0', '0', '1', '盐湖区');
INSERT INTO `system_areabase` VALUES ('322', '90', '140800', '140821', '0', '0', '1', '临猗县');
INSERT INTO `system_areabase` VALUES ('323', '90', '140800', '140822', '0', '0', '1', '万荣县');
INSERT INTO `system_areabase` VALUES ('324', '90', '140800', '140823', '0', '0', '1', '闻喜县');
INSERT INTO `system_areabase` VALUES ('325', '90', '140800', '140824', '0', '0', '1', '稷山县');
INSERT INTO `system_areabase` VALUES ('326', '90', '140800', '140825', '0', '0', '1', '新绛县');
INSERT INTO `system_areabase` VALUES ('327', '90', '140800', '140826', '0', '0', '1', '绛县');
INSERT INTO `system_areabase` VALUES ('328', '90', '140800', '140827', '0', '0', '1', '垣曲县');
INSERT INTO `system_areabase` VALUES ('329', '90', '140800', '140828', '0', '0', '1', '夏县');
INSERT INTO `system_areabase` VALUES ('330', '90', '140800', '140829', '0', '0', '1', '平陆县');
INSERT INTO `system_areabase` VALUES ('331', '90', '140800', '140830', '0', '0', '1', '芮城县');
INSERT INTO `system_areabase` VALUES ('332', '90', '140800', '140881', '0', '0', '1', '永济市');
INSERT INTO `system_areabase` VALUES ('333', '90', '140800', '140882', '0', '0', '1', '河津市');
INSERT INTO `system_areabase` VALUES ('334', '90', '140800', '140883', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('335', '90', '140000', '140900', '0', '0', '1', '忻州市');
INSERT INTO `system_areabase` VALUES ('336', '90', '140900', '140902', '0', '0', '1', '忻府区');
INSERT INTO `system_areabase` VALUES ('337', '90', '140900', '140921', '0', '0', '1', '定襄县');
INSERT INTO `system_areabase` VALUES ('338', '90', '140900', '140922', '0', '0', '1', '五台县');
INSERT INTO `system_areabase` VALUES ('339', '90', '140900', '140923', '0', '0', '1', '代县');
INSERT INTO `system_areabase` VALUES ('340', '90', '140900', '140924', '0', '0', '1', '繁峙县');
INSERT INTO `system_areabase` VALUES ('341', '90', '140900', '140925', '0', '0', '1', '宁武县');
INSERT INTO `system_areabase` VALUES ('342', '90', '140900', '140926', '0', '0', '1', '静乐县');
INSERT INTO `system_areabase` VALUES ('343', '90', '140900', '140927', '0', '0', '1', '神池县');
INSERT INTO `system_areabase` VALUES ('344', '90', '140900', '140928', '0', '0', '1', '五寨县');
INSERT INTO `system_areabase` VALUES ('345', '90', '140900', '140929', '0', '0', '1', '岢岚县');
INSERT INTO `system_areabase` VALUES ('346', '90', '140900', '140930', '0', '0', '1', '河曲县');
INSERT INTO `system_areabase` VALUES ('347', '90', '140900', '140931', '0', '0', '1', '保德县');
INSERT INTO `system_areabase` VALUES ('348', '90', '140900', '140932', '0', '0', '1', '偏关县');
INSERT INTO `system_areabase` VALUES ('349', '90', '140900', '140981', '0', '0', '1', '原平市');
INSERT INTO `system_areabase` VALUES ('350', '90', '140900', '140982', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('351', '90', '140000', '141000', '0', '0', '1', '临汾市');
INSERT INTO `system_areabase` VALUES ('352', '90', '141000', '141002', '0', '0', '1', '尧都区');
INSERT INTO `system_areabase` VALUES ('353', '90', '141000', '141021', '0', '0', '1', '曲沃县');
INSERT INTO `system_areabase` VALUES ('354', '90', '141000', '141022', '0', '0', '1', '翼城县');
INSERT INTO `system_areabase` VALUES ('355', '90', '141000', '141023', '0', '0', '1', '襄汾县');
INSERT INTO `system_areabase` VALUES ('356', '90', '141000', '141024', '0', '0', '1', '洪洞县');
INSERT INTO `system_areabase` VALUES ('357', '90', '141000', '141025', '0', '0', '1', '古县');
INSERT INTO `system_areabase` VALUES ('358', '90', '141000', '141026', '0', '0', '1', '安泽县');
INSERT INTO `system_areabase` VALUES ('359', '90', '141000', '141027', '0', '0', '1', '浮山县');
INSERT INTO `system_areabase` VALUES ('360', '90', '141000', '141028', '0', '0', '1', '吉县');
INSERT INTO `system_areabase` VALUES ('361', '90', '141000', '141029', '0', '0', '1', '乡宁县');
INSERT INTO `system_areabase` VALUES ('362', '90', '141000', '141030', '0', '0', '1', '大宁县');
INSERT INTO `system_areabase` VALUES ('363', '90', '141000', '141031', '0', '0', '1', '隰县');
INSERT INTO `system_areabase` VALUES ('364', '90', '141000', '141032', '0', '0', '1', '永和县');
INSERT INTO `system_areabase` VALUES ('365', '90', '141000', '141033', '0', '0', '1', '蒲县');
INSERT INTO `system_areabase` VALUES ('366', '90', '141000', '141034', '0', '0', '1', '汾西县');
INSERT INTO `system_areabase` VALUES ('367', '90', '141000', '141081', '0', '0', '1', '侯马市');
INSERT INTO `system_areabase` VALUES ('368', '90', '141000', '141082', '0', '0', '1', '霍州市');
INSERT INTO `system_areabase` VALUES ('369', '90', '141000', '141083', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('370', '90', '140000', '141100', '0', '0', '1', '吕梁市');
INSERT INTO `system_areabase` VALUES ('371', '90', '141100', '141102', '0', '0', '1', '离石区');
INSERT INTO `system_areabase` VALUES ('372', '90', '141100', '141121', '0', '0', '1', '文水县');
INSERT INTO `system_areabase` VALUES ('373', '90', '141100', '141122', '0', '0', '1', '交城县');
INSERT INTO `system_areabase` VALUES ('374', '90', '141100', '141123', '0', '0', '1', '兴县');
INSERT INTO `system_areabase` VALUES ('375', '90', '141100', '141124', '0', '0', '1', '临县');
INSERT INTO `system_areabase` VALUES ('376', '90', '141100', '141125', '0', '0', '1', '柳林县');
INSERT INTO `system_areabase` VALUES ('377', '90', '141100', '141126', '0', '0', '1', '石楼县');
INSERT INTO `system_areabase` VALUES ('378', '90', '141100', '141127', '0', '0', '1', '岚县');
INSERT INTO `system_areabase` VALUES ('379', '90', '141100', '141128', '0', '0', '1', '方山县');
INSERT INTO `system_areabase` VALUES ('380', '90', '141100', '141129', '0', '0', '1', '中阳县');
INSERT INTO `system_areabase` VALUES ('381', '90', '141100', '141130', '0', '0', '1', '交口县');
INSERT INTO `system_areabase` VALUES ('382', '90', '141100', '141181', '0', '0', '1', '孝义市');
INSERT INTO `system_areabase` VALUES ('383', '90', '141100', '141182', '0', '0', '1', '汾阳市');
INSERT INTO `system_areabase` VALUES ('384', '90', '141100', '141183', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('385', '90', '0', '150000', '0', '0', '1', '内蒙古自治区');
INSERT INTO `system_areabase` VALUES ('386', '90', '150000', '150100', '0', '0', '1', '呼和浩特市');
INSERT INTO `system_areabase` VALUES ('387', '90', '150100', '150102', '0', '0', '1', '新城区');
INSERT INTO `system_areabase` VALUES ('388', '90', '150100', '150103', '0', '0', '1', '回民区');
INSERT INTO `system_areabase` VALUES ('389', '90', '150100', '150104', '0', '0', '1', '玉泉区');
INSERT INTO `system_areabase` VALUES ('390', '90', '150100', '150105', '0', '0', '1', '赛罕区');
INSERT INTO `system_areabase` VALUES ('391', '90', '150100', '150121', '0', '0', '1', '土默特左旗');
INSERT INTO `system_areabase` VALUES ('392', '90', '150100', '150122', '0', '0', '1', '托克托县');
INSERT INTO `system_areabase` VALUES ('393', '90', '150100', '150123', '0', '0', '1', '和林格尔县');
INSERT INTO `system_areabase` VALUES ('394', '90', '150100', '150124', '0', '0', '1', '清水河县');
INSERT INTO `system_areabase` VALUES ('395', '90', '150100', '150125', '0', '0', '1', '武川县');
INSERT INTO `system_areabase` VALUES ('396', '90', '150100', '150126', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('397', '90', '150000', '150200', '0', '0', '1', '包头市');
INSERT INTO `system_areabase` VALUES ('398', '90', '150200', '150202', '0', '0', '1', '东河区');
INSERT INTO `system_areabase` VALUES ('399', '90', '150200', '150203', '0', '0', '1', '昆都仑区');
INSERT INTO `system_areabase` VALUES ('400', '90', '150200', '150204', '0', '0', '1', '青山区');
INSERT INTO `system_areabase` VALUES ('401', '90', '150200', '150205', '0', '0', '1', '石拐区');
INSERT INTO `system_areabase` VALUES ('402', '90', '150200', '150206', '0', '0', '1', '白云矿区');
INSERT INTO `system_areabase` VALUES ('403', '90', '150200', '150207', '0', '0', '1', '九原区');
INSERT INTO `system_areabase` VALUES ('404', '90', '150200', '150221', '0', '0', '1', '土默特右旗');
INSERT INTO `system_areabase` VALUES ('405', '90', '150200', '150222', '0', '0', '1', '固阳县');
INSERT INTO `system_areabase` VALUES ('406', '90', '150200', '150223', '0', '0', '1', '达尔罕茂明安联合旗');
INSERT INTO `system_areabase` VALUES ('407', '90', '150200', '150224', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('408', '90', '150000', '150300', '0', '0', '1', '乌海市');
INSERT INTO `system_areabase` VALUES ('409', '90', '150300', '150302', '0', '0', '1', '海勃湾区');
INSERT INTO `system_areabase` VALUES ('410', '90', '150300', '150303', '0', '0', '1', '海南区');
INSERT INTO `system_areabase` VALUES ('411', '90', '150300', '150304', '0', '0', '1', '乌达区');
INSERT INTO `system_areabase` VALUES ('412', '90', '150300', '150305', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('413', '90', '150000', '150400', '0', '0', '1', '赤峰市');
INSERT INTO `system_areabase` VALUES ('414', '90', '150400', '150402', '0', '0', '1', '红山区');
INSERT INTO `system_areabase` VALUES ('415', '90', '150400', '150403', '0', '0', '1', '元宝山区');
INSERT INTO `system_areabase` VALUES ('416', '90', '150400', '150404', '0', '0', '1', '松山区');
INSERT INTO `system_areabase` VALUES ('417', '90', '150400', '150421', '0', '0', '1', '阿鲁科尔沁旗');
INSERT INTO `system_areabase` VALUES ('418', '90', '150400', '150422', '0', '0', '1', '巴林左旗');
INSERT INTO `system_areabase` VALUES ('419', '90', '150400', '150423', '0', '0', '1', '巴林右旗');
INSERT INTO `system_areabase` VALUES ('420', '90', '150400', '150424', '0', '0', '1', '林西县');
INSERT INTO `system_areabase` VALUES ('421', '90', '150400', '150425', '0', '0', '1', '克什克腾旗');
INSERT INTO `system_areabase` VALUES ('422', '90', '150400', '150426', '0', '0', '1', '翁牛特旗');
INSERT INTO `system_areabase` VALUES ('423', '90', '150400', '150428', '0', '0', '1', '喀喇沁旗');
INSERT INTO `system_areabase` VALUES ('424', '90', '150400', '150429', '0', '0', '1', '宁城县');
INSERT INTO `system_areabase` VALUES ('425', '90', '150400', '150430', '0', '0', '1', '敖汉旗');
INSERT INTO `system_areabase` VALUES ('426', '90', '150400', '150431', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('427', '90', '150000', '150500', '0', '0', '1', '通辽市');
INSERT INTO `system_areabase` VALUES ('428', '90', '150500', '150502', '0', '0', '1', '科尔沁区');
INSERT INTO `system_areabase` VALUES ('429', '90', '150500', '150521', '0', '0', '1', '科尔沁左翼中旗');
INSERT INTO `system_areabase` VALUES ('430', '90', '150500', '150522', '0', '0', '1', '科尔沁左翼后旗');
INSERT INTO `system_areabase` VALUES ('431', '90', '150500', '150523', '0', '0', '1', '开鲁县');
INSERT INTO `system_areabase` VALUES ('432', '90', '150500', '150524', '0', '0', '1', '库伦旗');
INSERT INTO `system_areabase` VALUES ('433', '90', '150500', '150525', '0', '0', '1', '奈曼旗');
INSERT INTO `system_areabase` VALUES ('434', '90', '150500', '150526', '0', '0', '1', '扎鲁特旗');
INSERT INTO `system_areabase` VALUES ('435', '90', '150500', '150581', '0', '0', '1', '霍林郭勒');
INSERT INTO `system_areabase` VALUES ('436', '90', '150500', '150582', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('437', '90', '150000', '150600', '0', '0', '1', '鄂尔多斯市');
INSERT INTO `system_areabase` VALUES ('438', '90', '150600', '150602', '0', '0', '1', '东胜区');
INSERT INTO `system_areabase` VALUES ('439', '90', '150600', '150621', '0', '0', '1', '达拉特旗');
INSERT INTO `system_areabase` VALUES ('440', '90', '150600', '150622', '0', '0', '1', '准格尔旗');
INSERT INTO `system_areabase` VALUES ('441', '90', '150600', '150623', '0', '0', '1', '鄂托克前旗');
INSERT INTO `system_areabase` VALUES ('442', '90', '150600', '150624', '0', '0', '1', '鄂托克旗');
INSERT INTO `system_areabase` VALUES ('443', '90', '150600', '150625', '0', '0', '1', '杭锦旗');
INSERT INTO `system_areabase` VALUES ('444', '90', '150600', '150626', '0', '0', '1', '乌审旗');
INSERT INTO `system_areabase` VALUES ('445', '90', '150600', '150627', '0', '0', '1', '伊金霍洛旗');
INSERT INTO `system_areabase` VALUES ('446', '90', '150600', '150628', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('447', '90', '150000', '150700', '0', '0', '1', '呼伦贝尔市');
INSERT INTO `system_areabase` VALUES ('448', '90', '150700', '150702', '0', '0', '1', '海拉尔区');
INSERT INTO `system_areabase` VALUES ('449', '90', '150700', '150721', '0', '0', '1', '阿荣旗');
INSERT INTO `system_areabase` VALUES ('450', '90', '150700', '150722', '0', '0', '1', '莫力达瓦达斡尔族自治旗');
INSERT INTO `system_areabase` VALUES ('451', '90', '150700', '150723', '0', '0', '1', '鄂伦春自治旗');
INSERT INTO `system_areabase` VALUES ('452', '90', '150700', '150724', '0', '0', '1', '鄂温克族自治旗');
INSERT INTO `system_areabase` VALUES ('453', '90', '150700', '150725', '0', '0', '1', '陈巴尔虎旗');
INSERT INTO `system_areabase` VALUES ('454', '90', '150700', '150726', '0', '0', '1', '新巴尔虎左旗');
INSERT INTO `system_areabase` VALUES ('455', '90', '150700', '150727', '0', '0', '1', '新巴尔虎右旗');
INSERT INTO `system_areabase` VALUES ('456', '90', '150700', '150781', '0', '0', '1', '满洲里市');
INSERT INTO `system_areabase` VALUES ('457', '90', '150700', '150782', '0', '0', '1', '牙克石市');
INSERT INTO `system_areabase` VALUES ('458', '90', '150700', '150783', '0', '0', '1', '扎兰屯市');
INSERT INTO `system_areabase` VALUES ('459', '90', '150700', '150784', '0', '0', '1', '额尔古纳市');
INSERT INTO `system_areabase` VALUES ('460', '90', '150700', '150785', '0', '0', '1', '根河市');
INSERT INTO `system_areabase` VALUES ('461', '90', '150700', '150786', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('462', '90', '150000', '150800', '0', '0', '1', '巴彦淖尔市');
INSERT INTO `system_areabase` VALUES ('463', '90', '150800', '150802', '0', '0', '1', '临河区');
INSERT INTO `system_areabase` VALUES ('464', '90', '150800', '150821', '0', '0', '1', '五原县');
INSERT INTO `system_areabase` VALUES ('465', '90', '150800', '150822', '0', '0', '1', '磴口县');
INSERT INTO `system_areabase` VALUES ('466', '90', '150800', '150823', '0', '0', '1', '乌拉特前旗');
INSERT INTO `system_areabase` VALUES ('467', '90', '150800', '150824', '0', '0', '1', '乌拉特中旗');
INSERT INTO `system_areabase` VALUES ('468', '90', '150800', '150825', '0', '0', '1', '乌拉特后旗');
INSERT INTO `system_areabase` VALUES ('469', '90', '150800', '150826', '0', '0', '1', '杭锦后旗');
INSERT INTO `system_areabase` VALUES ('470', '90', '150800', '150827', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('471', '90', '150000', '150900', '0', '0', '1', '乌兰察布市');
INSERT INTO `system_areabase` VALUES ('472', '90', '150900', '150902', '0', '0', '1', '集宁区');
INSERT INTO `system_areabase` VALUES ('473', '90', '150900', '150921', '0', '0', '1', '卓资县');
INSERT INTO `system_areabase` VALUES ('474', '90', '150900', '150922', '0', '0', '1', '化德县');
INSERT INTO `system_areabase` VALUES ('475', '90', '150900', '150923', '0', '0', '1', '商都县');
INSERT INTO `system_areabase` VALUES ('476', '90', '150900', '150924', '0', '0', '1', '兴和县');
INSERT INTO `system_areabase` VALUES ('477', '90', '150900', '150925', '0', '0', '1', '凉城县');
INSERT INTO `system_areabase` VALUES ('478', '90', '150900', '150926', '0', '0', '1', '察哈尔右翼前旗');
INSERT INTO `system_areabase` VALUES ('479', '90', '150900', '150927', '0', '0', '1', '察哈尔右翼中旗');
INSERT INTO `system_areabase` VALUES ('480', '90', '150900', '150928', '0', '0', '1', '察哈尔右翼后旗');
INSERT INTO `system_areabase` VALUES ('481', '90', '150900', '150929', '0', '0', '1', '四子王旗');
INSERT INTO `system_areabase` VALUES ('482', '90', '150900', '150981', '0', '0', '1', '丰镇市');
INSERT INTO `system_areabase` VALUES ('483', '90', '150900', '150982', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('484', '90', '150000', '152200', '0', '0', '1', '兴安盟');
INSERT INTO `system_areabase` VALUES ('485', '90', '152200', '152201', '0', '0', '1', '乌兰浩特市');
INSERT INTO `system_areabase` VALUES ('486', '90', '152200', '152202', '0', '0', '1', '阿尔山市');
INSERT INTO `system_areabase` VALUES ('487', '90', '152200', '152221', '0', '0', '1', '科尔沁右翼前旗');
INSERT INTO `system_areabase` VALUES ('488', '90', '152200', '152222', '0', '0', '1', '科尔沁右翼中旗');
INSERT INTO `system_areabase` VALUES ('489', '90', '152200', '152223', '0', '0', '1', '扎赉特旗');
INSERT INTO `system_areabase` VALUES ('490', '90', '152200', '152224', '0', '0', '1', '突泉县');
INSERT INTO `system_areabase` VALUES ('491', '90', '152200', '152225', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('492', '90', '150000', '152500', '0', '0', '1', '锡林郭勒盟');
INSERT INTO `system_areabase` VALUES ('493', '90', '152500', '152501', '0', '0', '1', '二连浩特市');
INSERT INTO `system_areabase` VALUES ('494', '90', '152500', '152502', '0', '0', '1', '锡林浩特市');
INSERT INTO `system_areabase` VALUES ('495', '90', '152500', '152522', '0', '0', '1', '阿巴嘎旗');
INSERT INTO `system_areabase` VALUES ('496', '90', '152500', '152523', '0', '0', '1', '苏尼特左旗');
INSERT INTO `system_areabase` VALUES ('497', '90', '152500', '152524', '0', '0', '1', '苏尼特右旗');
INSERT INTO `system_areabase` VALUES ('498', '90', '152500', '152525', '0', '0', '1', '东乌珠穆沁旗');
INSERT INTO `system_areabase` VALUES ('499', '90', '152500', '152526', '0', '0', '1', '西乌珠穆沁旗');
INSERT INTO `system_areabase` VALUES ('500', '90', '152500', '152527', '0', '0', '1', '太仆寺旗');
INSERT INTO `system_areabase` VALUES ('501', '90', '152500', '152528', '0', '0', '1', '镶黄旗');
INSERT INTO `system_areabase` VALUES ('502', '90', '152500', '152529', '0', '0', '1', '正镶白旗');
INSERT INTO `system_areabase` VALUES ('503', '90', '152500', '152530', '0', '0', '1', '正蓝旗');
INSERT INTO `system_areabase` VALUES ('504', '90', '152500', '152531', '0', '0', '1', '多伦县');
INSERT INTO `system_areabase` VALUES ('505', '90', '152500', '152532', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('506', '90', '150000', '152900', '0', '0', '1', '阿拉善盟');
INSERT INTO `system_areabase` VALUES ('507', '90', '152900', '152921', '0', '0', '1', '阿拉善左旗');
INSERT INTO `system_areabase` VALUES ('508', '90', '152900', '152922', '0', '0', '1', '阿拉善右旗');
INSERT INTO `system_areabase` VALUES ('509', '90', '152900', '152923', '0', '0', '1', '额济纳旗');
INSERT INTO `system_areabase` VALUES ('510', '90', '152900', '152924', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('511', '90', '0', '210000', '0', '0', '1', '辽宁省');
INSERT INTO `system_areabase` VALUES ('512', '90', '210000', '210100', '0', '0', '1', '沈阳市');
INSERT INTO `system_areabase` VALUES ('513', '90', '210100', '210102', '0', '0', '1', '和平区');
INSERT INTO `system_areabase` VALUES ('514', '90', '210100', '210103', '0', '0', '1', '沈河区');
INSERT INTO `system_areabase` VALUES ('515', '90', '210100', '210104', '0', '0', '1', '大东区');
INSERT INTO `system_areabase` VALUES ('516', '90', '210100', '210105', '0', '0', '1', '皇姑区');
INSERT INTO `system_areabase` VALUES ('517', '90', '210100', '210106', '0', '0', '1', '铁西区');
INSERT INTO `system_areabase` VALUES ('518', '90', '210100', '210111', '0', '0', '1', '苏家屯区');
INSERT INTO `system_areabase` VALUES ('519', '90', '210100', '210112', '0', '0', '1', '东陵区');
INSERT INTO `system_areabase` VALUES ('520', '90', '210100', '210113', '0', '0', '1', '新城子区');
INSERT INTO `system_areabase` VALUES ('521', '90', '210100', '210114', '0', '0', '1', '于洪区');
INSERT INTO `system_areabase` VALUES ('522', '90', '210100', '210122', '0', '0', '1', '辽中县');
INSERT INTO `system_areabase` VALUES ('523', '90', '210100', '210123', '0', '0', '1', '康平县');
INSERT INTO `system_areabase` VALUES ('524', '90', '210100', '210124', '0', '0', '1', '法库县');
INSERT INTO `system_areabase` VALUES ('525', '90', '210100', '210181', '0', '0', '1', '新民市');
INSERT INTO `system_areabase` VALUES ('526', '90', '210100', '210182', '0', '0', '1', '浑南新区');
INSERT INTO `system_areabase` VALUES ('527', '90', '210100', '210183', '0', '0', '1', '张士开发区');
INSERT INTO `system_areabase` VALUES ('528', '90', '210100', '210184', '0', '0', '1', '沈北新区');
INSERT INTO `system_areabase` VALUES ('529', '90', '210100', '210185', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('530', '90', '210000', '210200', '0', '0', '1', '大连市');
INSERT INTO `system_areabase` VALUES ('531', '90', '210200', '210202', '0', '0', '1', '中山区');
INSERT INTO `system_areabase` VALUES ('532', '90', '210200', '210203', '0', '0', '1', '西岗区');
INSERT INTO `system_areabase` VALUES ('533', '90', '210200', '210204', '0', '0', '1', '沙河口区');
INSERT INTO `system_areabase` VALUES ('534', '90', '210200', '210211', '0', '0', '1', '甘井子区');
INSERT INTO `system_areabase` VALUES ('535', '90', '210200', '210212', '0', '0', '1', '旅顺口区');
INSERT INTO `system_areabase` VALUES ('536', '90', '210200', '210213', '0', '0', '1', '金州区');
INSERT INTO `system_areabase` VALUES ('537', '90', '210200', '210224', '0', '0', '1', '长海县');
INSERT INTO `system_areabase` VALUES ('538', '90', '210200', '210251', '0', '0', '1', '开发区');
INSERT INTO `system_areabase` VALUES ('539', '90', '210200', '210281', '0', '0', '1', '瓦房店市');
INSERT INTO `system_areabase` VALUES ('540', '90', '210200', '210282', '0', '0', '1', '普兰店市');
INSERT INTO `system_areabase` VALUES ('541', '90', '210200', '210283', '0', '0', '1', '庄河市');
INSERT INTO `system_areabase` VALUES ('542', '90', '210200', '210297', '0', '0', '1', '岭前区');
INSERT INTO `system_areabase` VALUES ('543', '90', '210200', '210298', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('544', '90', '210000', '210300', '0', '0', '1', '鞍山市');
INSERT INTO `system_areabase` VALUES ('545', '90', '210300', '210302', '0', '0', '1', '铁东区');
INSERT INTO `system_areabase` VALUES ('546', '90', '210300', '210303', '0', '0', '1', '铁西区');
INSERT INTO `system_areabase` VALUES ('547', '90', '210300', '210304', '0', '0', '1', '立山区');
INSERT INTO `system_areabase` VALUES ('548', '90', '210300', '210311', '0', '0', '1', '千山区');
INSERT INTO `system_areabase` VALUES ('549', '90', '210300', '210321', '0', '0', '1', '台安县');
INSERT INTO `system_areabase` VALUES ('550', '90', '210300', '210323', '0', '0', '1', '岫岩满族自治县');
INSERT INTO `system_areabase` VALUES ('551', '90', '210300', '210351', '0', '0', '1', '高新区');
INSERT INTO `system_areabase` VALUES ('552', '90', '210300', '210381', '0', '0', '1', '海城市');
INSERT INTO `system_areabase` VALUES ('553', '90', '210300', '210382', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('554', '90', '210000', '210400', '0', '0', '1', '抚顺市');
INSERT INTO `system_areabase` VALUES ('555', '90', '210400', '210402', '0', '0', '1', '新抚区');
INSERT INTO `system_areabase` VALUES ('556', '90', '210400', '210403', '0', '0', '1', '东洲区');
INSERT INTO `system_areabase` VALUES ('557', '90', '210400', '210404', '0', '0', '1', '望花区');
INSERT INTO `system_areabase` VALUES ('558', '90', '210400', '210411', '0', '0', '1', '顺城区');
INSERT INTO `system_areabase` VALUES ('559', '90', '210400', '210421', '0', '0', '1', '抚顺县');
INSERT INTO `system_areabase` VALUES ('560', '90', '210400', '210422', '0', '0', '1', '新宾满族自治县');
INSERT INTO `system_areabase` VALUES ('561', '90', '210400', '210423', '0', '0', '1', '清原满族自治县');
INSERT INTO `system_areabase` VALUES ('562', '90', '210400', '210424', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('563', '90', '210000', '210500', '0', '0', '1', '本溪市');
INSERT INTO `system_areabase` VALUES ('564', '90', '210500', '210502', '0', '0', '1', '平山区');
INSERT INTO `system_areabase` VALUES ('565', '90', '210500', '210503', '0', '0', '1', '溪湖区');
INSERT INTO `system_areabase` VALUES ('566', '90', '210500', '210504', '0', '0', '1', '明山区');
INSERT INTO `system_areabase` VALUES ('567', '90', '210500', '210505', '0', '0', '1', '南芬区');
INSERT INTO `system_areabase` VALUES ('568', '90', '210500', '210521', '0', '0', '1', '本溪满族自治县');
INSERT INTO `system_areabase` VALUES ('569', '90', '210500', '210522', '0', '0', '1', '桓仁满族自治县');
INSERT INTO `system_areabase` VALUES ('570', '90', '210500', '210523', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('571', '90', '210000', '210600', '0', '0', '1', '丹东市');
INSERT INTO `system_areabase` VALUES ('572', '90', '210600', '210602', '0', '0', '1', '元宝区');
INSERT INTO `system_areabase` VALUES ('573', '90', '210600', '210603', '0', '0', '1', '振兴区');
INSERT INTO `system_areabase` VALUES ('574', '90', '210600', '210604', '0', '0', '1', '振安区');
INSERT INTO `system_areabase` VALUES ('575', '90', '210600', '210624', '0', '0', '1', '宽甸满族自治县');
INSERT INTO `system_areabase` VALUES ('576', '90', '210600', '210681', '0', '0', '1', '东港市');
INSERT INTO `system_areabase` VALUES ('577', '90', '210600', '210682', '0', '0', '1', '凤城市');
INSERT INTO `system_areabase` VALUES ('578', '90', '210600', '210683', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('579', '90', '210000', '210700', '0', '0', '1', '锦州市');
INSERT INTO `system_areabase` VALUES ('580', '90', '210700', '210702', '0', '0', '1', '古塔区');
INSERT INTO `system_areabase` VALUES ('581', '90', '210700', '210703', '0', '0', '1', '凌河区');
INSERT INTO `system_areabase` VALUES ('582', '90', '210700', '210711', '0', '0', '1', '太和区');
INSERT INTO `system_areabase` VALUES ('583', '90', '210700', '210726', '0', '0', '1', '黑山县');
INSERT INTO `system_areabase` VALUES ('584', '90', '210700', '210727', '0', '0', '1', '义县');
INSERT INTO `system_areabase` VALUES ('585', '90', '210700', '210781', '0', '0', '1', '凌海市');
INSERT INTO `system_areabase` VALUES ('586', '90', '210700', '210782', '0', '0', '1', '北镇市');
INSERT INTO `system_areabase` VALUES ('587', '90', '210700', '210783', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('588', '90', '210000', '210800', '0', '0', '1', '营口市');
INSERT INTO `system_areabase` VALUES ('589', '90', '210800', '210802', '0', '0', '1', '站前区');
INSERT INTO `system_areabase` VALUES ('590', '90', '210800', '210803', '0', '0', '1', '西市区');
INSERT INTO `system_areabase` VALUES ('591', '90', '210800', '210804', '0', '0', '1', '鲅鱼圈区');
INSERT INTO `system_areabase` VALUES ('592', '90', '210800', '210811', '0', '0', '1', '老边区');
INSERT INTO `system_areabase` VALUES ('593', '90', '210800', '210881', '0', '0', '1', '盖州市');
INSERT INTO `system_areabase` VALUES ('594', '90', '210800', '210882', '0', '0', '1', '大石桥市');
INSERT INTO `system_areabase` VALUES ('595', '90', '210800', '210883', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('596', '90', '210000', '210900', '0', '0', '1', '阜新市');
INSERT INTO `system_areabase` VALUES ('597', '90', '210900', '210902', '0', '0', '1', '海州区');
INSERT INTO `system_areabase` VALUES ('598', '90', '210900', '210903', '0', '0', '1', '新邱区');
INSERT INTO `system_areabase` VALUES ('599', '90', '210900', '210904', '0', '0', '1', '太平区');
INSERT INTO `system_areabase` VALUES ('600', '90', '210900', '210905', '0', '0', '1', '清河门区');
INSERT INTO `system_areabase` VALUES ('601', '90', '210900', '210911', '0', '0', '1', '细河区');
INSERT INTO `system_areabase` VALUES ('602', '90', '210900', '210921', '0', '0', '1', '阜新蒙古族自治县');
INSERT INTO `system_areabase` VALUES ('603', '90', '210900', '210922', '0', '0', '1', '彰武县');
INSERT INTO `system_areabase` VALUES ('604', '90', '210900', '210923', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('605', '90', '210000', '211000', '0', '0', '1', '辽阳市');
INSERT INTO `system_areabase` VALUES ('606', '90', '211000', '211002', '0', '0', '1', '白塔区');
INSERT INTO `system_areabase` VALUES ('607', '90', '211000', '211003', '0', '0', '1', '文圣区');
INSERT INTO `system_areabase` VALUES ('608', '90', '211000', '211004', '0', '0', '1', '宏伟区');
INSERT INTO `system_areabase` VALUES ('609', '90', '211000', '211005', '0', '0', '1', '弓长岭区');
INSERT INTO `system_areabase` VALUES ('610', '90', '211000', '211011', '0', '0', '1', '太子河区');
INSERT INTO `system_areabase` VALUES ('611', '90', '211000', '211021', '0', '0', '1', '辽阳县');
INSERT INTO `system_areabase` VALUES ('612', '90', '211000', '211081', '0', '0', '1', '灯塔市');
INSERT INTO `system_areabase` VALUES ('613', '90', '211000', '211082', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('614', '90', '210000', '211100', '0', '0', '1', '盘锦市');
INSERT INTO `system_areabase` VALUES ('615', '90', '211100', '211102', '0', '0', '1', '双台子区');
INSERT INTO `system_areabase` VALUES ('616', '90', '211100', '211103', '0', '0', '1', '兴隆台区');
INSERT INTO `system_areabase` VALUES ('617', '90', '211100', '211121', '0', '0', '1', '大洼县');
INSERT INTO `system_areabase` VALUES ('618', '90', '211100', '211122', '0', '0', '1', '盘山县');
INSERT INTO `system_areabase` VALUES ('619', '90', '211100', '211123', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('620', '90', '210000', '211200', '0', '0', '1', '铁岭市');
INSERT INTO `system_areabase` VALUES ('621', '90', '211200', '211202', '0', '0', '1', '银州区');
INSERT INTO `system_areabase` VALUES ('622', '90', '211200', '211204', '0', '0', '1', '清河区');
INSERT INTO `system_areabase` VALUES ('623', '90', '211200', '211221', '0', '0', '1', '铁岭县');
INSERT INTO `system_areabase` VALUES ('624', '90', '211200', '211223', '0', '0', '1', '西丰县');
INSERT INTO `system_areabase` VALUES ('625', '90', '211200', '211224', '0', '0', '1', '昌图县');
INSERT INTO `system_areabase` VALUES ('626', '90', '211200', '211281', '0', '0', '1', '调兵山市');
INSERT INTO `system_areabase` VALUES ('627', '90', '211200', '211282', '0', '0', '1', '开原市');
INSERT INTO `system_areabase` VALUES ('628', '90', '211200', '211283', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('629', '90', '210000', '211300', '0', '0', '1', '朝阳市');
INSERT INTO `system_areabase` VALUES ('630', '90', '211300', '211302', '0', '0', '1', '双塔区');
INSERT INTO `system_areabase` VALUES ('631', '90', '211300', '211303', '0', '0', '1', '龙城区');
INSERT INTO `system_areabase` VALUES ('632', '90', '211300', '211321', '0', '0', '1', '朝阳县');
INSERT INTO `system_areabase` VALUES ('633', '90', '211300', '211322', '0', '0', '1', '建平县');
INSERT INTO `system_areabase` VALUES ('634', '90', '211300', '211324', '0', '0', '1', '喀喇沁左翼蒙古族自治县');
INSERT INTO `system_areabase` VALUES ('635', '90', '211300', '211381', '0', '0', '1', '北票市');
INSERT INTO `system_areabase` VALUES ('636', '90', '211300', '211382', '0', '0', '1', '凌源市');
INSERT INTO `system_areabase` VALUES ('637', '90', '211300', '211383', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('638', '90', '210000', '211400', '0', '0', '1', '葫芦岛市');
INSERT INTO `system_areabase` VALUES ('639', '90', '211400', '211402', '0', '0', '1', '连山区');
INSERT INTO `system_areabase` VALUES ('640', '90', '211400', '211403', '0', '0', '1', '龙港区');
INSERT INTO `system_areabase` VALUES ('641', '90', '211400', '211404', '0', '0', '1', '南票区');
INSERT INTO `system_areabase` VALUES ('642', '90', '211400', '211421', '0', '0', '1', '绥中县');
INSERT INTO `system_areabase` VALUES ('643', '90', '211400', '211422', '0', '0', '1', '建昌县');
INSERT INTO `system_areabase` VALUES ('644', '90', '211400', '211481', '0', '0', '1', '兴城市');
INSERT INTO `system_areabase` VALUES ('645', '90', '211400', '211482', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('646', '90', '0', '220000', '0', '0', '1', '吉林省');
INSERT INTO `system_areabase` VALUES ('647', '90', '220000', '220100', '0', '0', '1', '长春市');
INSERT INTO `system_areabase` VALUES ('648', '90', '220100', '220102', '0', '0', '1', '南关区');
INSERT INTO `system_areabase` VALUES ('649', '90', '220100', '220103', '0', '0', '1', '宽城区');
INSERT INTO `system_areabase` VALUES ('650', '90', '220100', '220104', '0', '0', '1', '朝阳区');
INSERT INTO `system_areabase` VALUES ('651', '90', '220100', '220105', '0', '0', '1', '二道区');
INSERT INTO `system_areabase` VALUES ('652', '90', '220100', '220106', '0', '0', '1', '绿园区');
INSERT INTO `system_areabase` VALUES ('653', '90', '220100', '220112', '0', '0', '1', '双阳区');
INSERT INTO `system_areabase` VALUES ('654', '90', '220100', '220122', '0', '0', '1', '农安县');
INSERT INTO `system_areabase` VALUES ('655', '90', '220100', '220181', '0', '0', '1', '九台市');
INSERT INTO `system_areabase` VALUES ('656', '90', '220100', '220182', '0', '0', '1', '榆树市');
INSERT INTO `system_areabase` VALUES ('657', '90', '220100', '220183', '0', '0', '1', '德惠市');
INSERT INTO `system_areabase` VALUES ('658', '90', '220100', '220184', '0', '0', '1', '高新技术产业开发区');
INSERT INTO `system_areabase` VALUES ('659', '90', '220100', '220185', '0', '0', '1', '汽车产业开发区');
INSERT INTO `system_areabase` VALUES ('660', '90', '220100', '220186', '0', '0', '1', '经济技术开发区');
INSERT INTO `system_areabase` VALUES ('661', '90', '220100', '220187', '0', '0', '1', '净月旅游开发区');
INSERT INTO `system_areabase` VALUES ('662', '90', '220100', '220188', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('663', '90', '220000', '220200', '0', '0', '1', '吉林市');
INSERT INTO `system_areabase` VALUES ('664', '90', '220200', '220202', '0', '0', '1', '昌邑区');
INSERT INTO `system_areabase` VALUES ('665', '90', '220200', '220203', '0', '0', '1', '龙潭区');
INSERT INTO `system_areabase` VALUES ('666', '90', '220200', '220204', '0', '0', '1', '船营区');
INSERT INTO `system_areabase` VALUES ('667', '90', '220200', '220211', '0', '0', '1', '丰满区');
INSERT INTO `system_areabase` VALUES ('668', '90', '220200', '220221', '0', '0', '1', '永吉县');
INSERT INTO `system_areabase` VALUES ('669', '90', '220200', '220281', '0', '0', '1', '蛟河市');
INSERT INTO `system_areabase` VALUES ('670', '90', '220200', '220282', '0', '0', '1', '桦甸市');
INSERT INTO `system_areabase` VALUES ('671', '90', '220200', '220283', '0', '0', '1', '舒兰市');
INSERT INTO `system_areabase` VALUES ('672', '90', '220200', '220284', '0', '0', '1', '磐石市');
INSERT INTO `system_areabase` VALUES ('673', '90', '220200', '220285', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('674', '90', '220000', '220300', '0', '0', '1', '四平市');
INSERT INTO `system_areabase` VALUES ('675', '90', '220300', '220302', '0', '0', '1', '铁西区');
INSERT INTO `system_areabase` VALUES ('676', '90', '220300', '220303', '0', '0', '1', '铁东区');
INSERT INTO `system_areabase` VALUES ('677', '90', '220300', '220322', '0', '0', '1', '梨树县');
INSERT INTO `system_areabase` VALUES ('678', '90', '220300', '220323', '0', '0', '1', '伊通满族自治县');
INSERT INTO `system_areabase` VALUES ('679', '90', '220300', '220381', '0', '0', '1', '公主岭市');
INSERT INTO `system_areabase` VALUES ('680', '90', '220300', '220382', '0', '0', '1', '双辽市');
INSERT INTO `system_areabase` VALUES ('681', '90', '220300', '220383', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('682', '90', '220000', '220400', '0', '0', '1', '辽源市');
INSERT INTO `system_areabase` VALUES ('683', '90', '220400', '220402', '0', '0', '1', '龙山区');
INSERT INTO `system_areabase` VALUES ('684', '90', '220400', '220403', '0', '0', '1', '西安区');
INSERT INTO `system_areabase` VALUES ('685', '90', '220400', '220421', '0', '0', '1', '东丰县');
INSERT INTO `system_areabase` VALUES ('686', '90', '220400', '220422', '0', '0', '1', '东辽县');
INSERT INTO `system_areabase` VALUES ('687', '90', '220400', '220423', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('688', '90', '220000', '220500', '0', '0', '1', '通化市');
INSERT INTO `system_areabase` VALUES ('689', '90', '220500', '220502', '0', '0', '1', '东昌区');
INSERT INTO `system_areabase` VALUES ('690', '90', '220500', '220503', '0', '0', '1', '二道江区');
INSERT INTO `system_areabase` VALUES ('691', '90', '220500', '220521', '0', '0', '1', '通化县');
INSERT INTO `system_areabase` VALUES ('692', '90', '220500', '220523', '0', '0', '1', '辉南县');
INSERT INTO `system_areabase` VALUES ('693', '90', '220500', '220524', '0', '0', '1', '柳河县');
INSERT INTO `system_areabase` VALUES ('694', '90', '220500', '220581', '0', '0', '1', '梅河口市');
INSERT INTO `system_areabase` VALUES ('695', '90', '220500', '220582', '0', '0', '1', '集安市');
INSERT INTO `system_areabase` VALUES ('696', '90', '220500', '220583', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('697', '90', '220000', '220600', '0', '0', '1', '白山市');
INSERT INTO `system_areabase` VALUES ('698', '90', '220600', '220602', '0', '0', '1', '八道江区');
INSERT INTO `system_areabase` VALUES ('699', '90', '220600', '220621', '0', '0', '1', '抚松县');
INSERT INTO `system_areabase` VALUES ('700', '90', '220600', '220622', '0', '0', '1', '靖宇县');
INSERT INTO `system_areabase` VALUES ('701', '90', '220600', '220623', '0', '0', '1', '长白朝鲜族自治县');
INSERT INTO `system_areabase` VALUES ('702', '90', '220600', '220625', '0', '0', '1', '江源县');
INSERT INTO `system_areabase` VALUES ('703', '90', '220600', '220681', '0', '0', '1', '临江市');
INSERT INTO `system_areabase` VALUES ('704', '90', '220600', '220682', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('705', '90', '220000', '220700', '0', '0', '1', '松原市');
INSERT INTO `system_areabase` VALUES ('706', '90', '220700', '220702', '0', '0', '1', '宁江区');
INSERT INTO `system_areabase` VALUES ('707', '90', '220700', '220721', '0', '0', '1', '前郭尔罗斯蒙古族自治县');
INSERT INTO `system_areabase` VALUES ('708', '90', '220700', '220722', '0', '0', '1', '长岭县');
INSERT INTO `system_areabase` VALUES ('709', '90', '220700', '220723', '0', '0', '1', '乾安县');
INSERT INTO `system_areabase` VALUES ('710', '90', '220700', '220724', '0', '0', '1', '扶余县');
INSERT INTO `system_areabase` VALUES ('711', '90', '220700', '220725', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('712', '90', '220000', '220800', '0', '0', '1', '白城市');
INSERT INTO `system_areabase` VALUES ('713', '90', '220800', '220802', '0', '0', '1', '洮北区');
INSERT INTO `system_areabase` VALUES ('714', '90', '220800', '220821', '0', '0', '1', '镇赉县');
INSERT INTO `system_areabase` VALUES ('715', '90', '220800', '220822', '0', '0', '1', '通榆县');
INSERT INTO `system_areabase` VALUES ('716', '90', '220800', '220881', '0', '0', '1', '洮南市');
INSERT INTO `system_areabase` VALUES ('717', '90', '220800', '220882', '0', '0', '1', '大安市');
INSERT INTO `system_areabase` VALUES ('718', '90', '220800', '220883', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('719', '90', '220000', '222400', '0', '0', '1', '延边朝鲜族自治州');
INSERT INTO `system_areabase` VALUES ('720', '90', '222400', '222401', '0', '0', '1', '延吉市');
INSERT INTO `system_areabase` VALUES ('721', '90', '222400', '222402', '0', '0', '1', '图们市');
INSERT INTO `system_areabase` VALUES ('722', '90', '222400', '222403', '0', '0', '1', '敦化市');
INSERT INTO `system_areabase` VALUES ('723', '90', '222400', '222404', '0', '0', '1', '珲春市');
INSERT INTO `system_areabase` VALUES ('724', '90', '222400', '222405', '0', '0', '1', '龙井市');
INSERT INTO `system_areabase` VALUES ('725', '90', '222400', '222406', '0', '0', '1', '和龙市');
INSERT INTO `system_areabase` VALUES ('726', '90', '222400', '222424', '0', '0', '1', '汪清县');
INSERT INTO `system_areabase` VALUES ('727', '90', '222400', '222426', '0', '0', '1', '安图县');
INSERT INTO `system_areabase` VALUES ('728', '90', '222400', '222427', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('729', '90', '0', '230000', '0', '0', '1', '黑龙江省');
INSERT INTO `system_areabase` VALUES ('730', '90', '230000', '230100', '0', '0', '1', '哈尔滨市');
INSERT INTO `system_areabase` VALUES ('731', '90', '230100', '230102', '0', '0', '1', '道里区');
INSERT INTO `system_areabase` VALUES ('732', '90', '230100', '230103', '0', '0', '1', '南岗区');
INSERT INTO `system_areabase` VALUES ('733', '90', '230100', '230104', '0', '0', '1', '道外区');
INSERT INTO `system_areabase` VALUES ('734', '90', '230100', '230106', '0', '0', '1', '香坊区');
INSERT INTO `system_areabase` VALUES ('735', '90', '230100', '230107', '0', '0', '1', '动力区');
INSERT INTO `system_areabase` VALUES ('736', '90', '230100', '230108', '0', '0', '1', '平房区');
INSERT INTO `system_areabase` VALUES ('737', '90', '230100', '230109', '0', '0', '1', '松北区');
INSERT INTO `system_areabase` VALUES ('738', '90', '230100', '230111', '0', '0', '1', '呼兰区');
INSERT INTO `system_areabase` VALUES ('739', '90', '230100', '230123', '0', '0', '1', '依兰县');
INSERT INTO `system_areabase` VALUES ('740', '90', '230100', '230124', '0', '0', '1', '方正县');
INSERT INTO `system_areabase` VALUES ('741', '90', '230100', '230125', '0', '0', '1', '宾县');
INSERT INTO `system_areabase` VALUES ('742', '90', '230100', '230126', '0', '0', '1', '巴彦县');
INSERT INTO `system_areabase` VALUES ('743', '90', '230100', '230127', '0', '0', '1', '木兰县');
INSERT INTO `system_areabase` VALUES ('744', '90', '230100', '230128', '0', '0', '1', '通河县');
INSERT INTO `system_areabase` VALUES ('745', '90', '230100', '230129', '0', '0', '1', '延寿县');
INSERT INTO `system_areabase` VALUES ('746', '90', '230100', '230181', '0', '0', '1', '阿城市');
INSERT INTO `system_areabase` VALUES ('747', '90', '230100', '230182', '0', '0', '1', '双城市');
INSERT INTO `system_areabase` VALUES ('748', '90', '230100', '230183', '0', '0', '1', '尚志市');
INSERT INTO `system_areabase` VALUES ('749', '90', '230100', '230184', '0', '0', '1', '五常市');
INSERT INTO `system_areabase` VALUES ('750', '90', '230100', '230186', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('751', '90', '230000', '230200', '0', '0', '1', '齐齐哈尔市');
INSERT INTO `system_areabase` VALUES ('752', '90', '230200', '230202', '0', '0', '1', '龙沙区');
INSERT INTO `system_areabase` VALUES ('753', '90', '230200', '230203', '0', '0', '1', '建华区');
INSERT INTO `system_areabase` VALUES ('754', '90', '230200', '230204', '0', '0', '1', '铁锋区');
INSERT INTO `system_areabase` VALUES ('755', '90', '230200', '230205', '0', '0', '1', '昂昂溪区');
INSERT INTO `system_areabase` VALUES ('756', '90', '230200', '230206', '0', '0', '1', '富拉尔基区');
INSERT INTO `system_areabase` VALUES ('757', '90', '230200', '230207', '0', '0', '1', '碾子山区');
INSERT INTO `system_areabase` VALUES ('758', '90', '230200', '230208', '0', '0', '1', '梅里斯达斡尔族区');
INSERT INTO `system_areabase` VALUES ('759', '90', '230200', '230221', '0', '0', '1', '龙江县');
INSERT INTO `system_areabase` VALUES ('760', '90', '230200', '230223', '0', '0', '1', '依安县');
INSERT INTO `system_areabase` VALUES ('761', '90', '230200', '230224', '0', '0', '1', '泰来县');
INSERT INTO `system_areabase` VALUES ('762', '90', '230200', '230225', '0', '0', '1', '甘南县');
INSERT INTO `system_areabase` VALUES ('763', '90', '230200', '230227', '0', '0', '1', '富裕县');
INSERT INTO `system_areabase` VALUES ('764', '90', '230200', '230229', '0', '0', '1', '克山县');
INSERT INTO `system_areabase` VALUES ('765', '90', '230200', '230230', '0', '0', '1', '克东县');
INSERT INTO `system_areabase` VALUES ('766', '90', '230200', '230231', '0', '0', '1', '拜泉县');
INSERT INTO `system_areabase` VALUES ('767', '90', '230200', '230281', '0', '0', '1', '讷河市');
INSERT INTO `system_areabase` VALUES ('768', '90', '230200', '230282', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('769', '90', '230000', '230300', '0', '0', '1', '鸡西市');
INSERT INTO `system_areabase` VALUES ('770', '90', '230300', '230302', '0', '0', '1', '鸡冠区');
INSERT INTO `system_areabase` VALUES ('771', '90', '230300', '230303', '0', '0', '1', '恒山区');
INSERT INTO `system_areabase` VALUES ('772', '90', '230300', '230304', '0', '0', '1', '滴道区');
INSERT INTO `system_areabase` VALUES ('773', '90', '230300', '230305', '0', '0', '1', '梨树区');
INSERT INTO `system_areabase` VALUES ('774', '90', '230300', '230306', '0', '0', '1', '城子河区');
INSERT INTO `system_areabase` VALUES ('775', '90', '230300', '230307', '0', '0', '1', '麻山区');
INSERT INTO `system_areabase` VALUES ('776', '90', '230300', '230321', '0', '0', '1', '鸡东县');
INSERT INTO `system_areabase` VALUES ('777', '90', '230300', '230381', '0', '0', '1', '虎林市');
INSERT INTO `system_areabase` VALUES ('778', '90', '230300', '230382', '0', '0', '1', '密山市');
INSERT INTO `system_areabase` VALUES ('779', '90', '230300', '230383', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('780', '90', '230000', '230400', '0', '0', '1', '鹤岗市');
INSERT INTO `system_areabase` VALUES ('781', '90', '230400', '230402', '0', '0', '1', '向阳区');
INSERT INTO `system_areabase` VALUES ('782', '90', '230400', '230403', '0', '0', '1', '工农区');
INSERT INTO `system_areabase` VALUES ('783', '90', '230400', '230404', '0', '0', '1', '南山区');
INSERT INTO `system_areabase` VALUES ('784', '90', '230400', '230405', '0', '0', '1', '兴安区');
INSERT INTO `system_areabase` VALUES ('785', '90', '230400', '230406', '0', '0', '1', '东山区');
INSERT INTO `system_areabase` VALUES ('786', '90', '230400', '230407', '0', '0', '1', '兴山区');
INSERT INTO `system_areabase` VALUES ('787', '90', '230400', '230421', '0', '0', '1', '萝北县');
INSERT INTO `system_areabase` VALUES ('788', '90', '230400', '230422', '0', '0', '1', '绥滨县');
INSERT INTO `system_areabase` VALUES ('789', '90', '230400', '230423', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('790', '90', '230000', '230500', '0', '0', '1', '双鸭山市');
INSERT INTO `system_areabase` VALUES ('791', '90', '230500', '230502', '0', '0', '1', '尖山区');
INSERT INTO `system_areabase` VALUES ('792', '90', '230500', '230503', '0', '0', '1', '岭东区');
INSERT INTO `system_areabase` VALUES ('793', '90', '230500', '230505', '0', '0', '1', '四方台区');
INSERT INTO `system_areabase` VALUES ('794', '90', '230500', '230506', '0', '0', '1', '宝山区');
INSERT INTO `system_areabase` VALUES ('795', '90', '230500', '230521', '0', '0', '1', '集贤县');
INSERT INTO `system_areabase` VALUES ('796', '90', '230500', '230522', '0', '0', '1', '友谊县');
INSERT INTO `system_areabase` VALUES ('797', '90', '230500', '230523', '0', '0', '1', '宝清县');
INSERT INTO `system_areabase` VALUES ('798', '90', '230500', '230524', '0', '0', '1', '饶河县');
INSERT INTO `system_areabase` VALUES ('799', '90', '230500', '230525', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('800', '90', '230000', '230600', '0', '0', '1', '大庆市');
INSERT INTO `system_areabase` VALUES ('801', '90', '230600', '230602', '0', '0', '1', '萨尔图区');
INSERT INTO `system_areabase` VALUES ('802', '90', '230600', '230603', '0', '0', '1', '龙凤区');
INSERT INTO `system_areabase` VALUES ('803', '90', '230600', '230604', '0', '0', '1', '让胡路区');
INSERT INTO `system_areabase` VALUES ('804', '90', '230600', '230605', '0', '0', '1', '红岗区');
INSERT INTO `system_areabase` VALUES ('805', '90', '230600', '230606', '0', '0', '1', '大同区');
INSERT INTO `system_areabase` VALUES ('806', '90', '230600', '230621', '0', '0', '1', '肇州县');
INSERT INTO `system_areabase` VALUES ('807', '90', '230600', '230622', '0', '0', '1', '肇源县');
INSERT INTO `system_areabase` VALUES ('808', '90', '230600', '230623', '0', '0', '1', '林甸县');
INSERT INTO `system_areabase` VALUES ('809', '90', '230600', '230624', '0', '0', '1', '杜尔伯特蒙古族自治县');
INSERT INTO `system_areabase` VALUES ('810', '90', '230600', '230625', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('811', '90', '230000', '230700', '0', '0', '1', '伊春市');
INSERT INTO `system_areabase` VALUES ('812', '90', '230700', '230702', '0', '0', '1', '伊春区');
INSERT INTO `system_areabase` VALUES ('813', '90', '230700', '230703', '0', '0', '1', '南岔区');
INSERT INTO `system_areabase` VALUES ('814', '90', '230700', '230704', '0', '0', '1', '友好区');
INSERT INTO `system_areabase` VALUES ('815', '90', '230700', '230705', '0', '0', '1', '西林区');
INSERT INTO `system_areabase` VALUES ('816', '90', '230700', '230706', '0', '0', '1', '翠峦区');
INSERT INTO `system_areabase` VALUES ('817', '90', '230700', '230707', '0', '0', '1', '新青区');
INSERT INTO `system_areabase` VALUES ('818', '90', '230700', '230708', '0', '0', '1', '美溪区');
INSERT INTO `system_areabase` VALUES ('819', '90', '230700', '230709', '0', '0', '1', '金山屯区');
INSERT INTO `system_areabase` VALUES ('820', '90', '230700', '230710', '0', '0', '1', '五营区');
INSERT INTO `system_areabase` VALUES ('821', '90', '230700', '230711', '0', '0', '1', '乌马河区');
INSERT INTO `system_areabase` VALUES ('822', '90', '230700', '230712', '0', '0', '1', '汤旺河区');
INSERT INTO `system_areabase` VALUES ('823', '90', '230700', '230713', '0', '0', '1', '带岭区');
INSERT INTO `system_areabase` VALUES ('824', '90', '230700', '230714', '0', '0', '1', '乌伊岭区');
INSERT INTO `system_areabase` VALUES ('825', '90', '230700', '230715', '0', '0', '1', '红星区');
INSERT INTO `system_areabase` VALUES ('826', '90', '230700', '230716', '0', '0', '1', '上甘岭区');
INSERT INTO `system_areabase` VALUES ('827', '90', '230700', '230722', '0', '0', '1', '嘉荫县');
INSERT INTO `system_areabase` VALUES ('828', '90', '230700', '230781', '0', '0', '1', '铁力市');
INSERT INTO `system_areabase` VALUES ('829', '90', '230700', '230782', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('830', '90', '230000', '230800', '0', '0', '1', '佳木斯市');
INSERT INTO `system_areabase` VALUES ('831', '90', '230800', '230802', '0', '0', '1', '永红区');
INSERT INTO `system_areabase` VALUES ('832', '90', '230800', '230803', '0', '0', '1', '向阳区');
INSERT INTO `system_areabase` VALUES ('833', '90', '230800', '230804', '0', '0', '1', '前进区');
INSERT INTO `system_areabase` VALUES ('834', '90', '230800', '230805', '0', '0', '1', '东风区');
INSERT INTO `system_areabase` VALUES ('835', '90', '230800', '230811', '0', '0', '1', '郊区');
INSERT INTO `system_areabase` VALUES ('836', '90', '230800', '230822', '0', '0', '1', '桦南县');
INSERT INTO `system_areabase` VALUES ('837', '90', '230800', '230826', '0', '0', '1', '桦川县');
INSERT INTO `system_areabase` VALUES ('838', '90', '230800', '230828', '0', '0', '1', '汤原县');
INSERT INTO `system_areabase` VALUES ('839', '90', '230800', '230833', '0', '0', '1', '抚远县');
INSERT INTO `system_areabase` VALUES ('840', '90', '230800', '230881', '0', '0', '1', '同江市');
INSERT INTO `system_areabase` VALUES ('841', '90', '230800', '230882', '0', '0', '1', '富锦市');
INSERT INTO `system_areabase` VALUES ('842', '90', '230800', '230883', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('843', '90', '230000', '230900', '0', '0', '1', '七台河市');
INSERT INTO `system_areabase` VALUES ('844', '90', '230900', '230902', '0', '0', '1', '新兴区');
INSERT INTO `system_areabase` VALUES ('845', '90', '230900', '230903', '0', '0', '1', '桃山区');
INSERT INTO `system_areabase` VALUES ('846', '90', '230900', '230904', '0', '0', '1', '茄子河区');
INSERT INTO `system_areabase` VALUES ('847', '90', '230900', '230921', '0', '0', '1', '勃利县');
INSERT INTO `system_areabase` VALUES ('848', '90', '230900', '230922', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('849', '90', '230000', '231000', '0', '0', '1', '牡丹江市');
INSERT INTO `system_areabase` VALUES ('850', '90', '231000', '231002', '0', '0', '1', '东安区');
INSERT INTO `system_areabase` VALUES ('851', '90', '231000', '231003', '0', '0', '1', '阳明区');
INSERT INTO `system_areabase` VALUES ('852', '90', '231000', '231004', '0', '0', '1', '爱民区');
INSERT INTO `system_areabase` VALUES ('853', '90', '231000', '231005', '0', '0', '1', '西安区');
INSERT INTO `system_areabase` VALUES ('854', '90', '231000', '231024', '0', '0', '1', '东宁县');
INSERT INTO `system_areabase` VALUES ('855', '90', '231000', '231025', '0', '0', '1', '林口县');
INSERT INTO `system_areabase` VALUES ('856', '90', '231000', '231081', '0', '0', '1', '绥芬河市');
INSERT INTO `system_areabase` VALUES ('857', '90', '231000', '231083', '0', '0', '1', '海林市');
INSERT INTO `system_areabase` VALUES ('858', '90', '231000', '231084', '0', '0', '1', '宁安市');
INSERT INTO `system_areabase` VALUES ('859', '90', '231000', '231085', '0', '0', '1', '穆棱市');
INSERT INTO `system_areabase` VALUES ('860', '90', '231000', '231086', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('861', '90', '230000', '231100', '0', '0', '1', '黑河市');
INSERT INTO `system_areabase` VALUES ('862', '90', '231100', '231102', '0', '0', '1', '爱辉区');
INSERT INTO `system_areabase` VALUES ('863', '90', '231100', '231121', '0', '0', '1', '嫩江县');
INSERT INTO `system_areabase` VALUES ('864', '90', '231100', '231123', '0', '0', '1', '逊克县');
INSERT INTO `system_areabase` VALUES ('865', '90', '231100', '231124', '0', '0', '1', '孙吴县');
INSERT INTO `system_areabase` VALUES ('866', '90', '231100', '231181', '0', '0', '1', '北安市');
INSERT INTO `system_areabase` VALUES ('867', '90', '231100', '231182', '0', '0', '1', '五大连池市');
INSERT INTO `system_areabase` VALUES ('868', '90', '231100', '231183', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('869', '90', '230000', '231200', '0', '0', '1', '绥化市');
INSERT INTO `system_areabase` VALUES ('870', '90', '231200', '231202', '0', '0', '1', '北林区');
INSERT INTO `system_areabase` VALUES ('871', '90', '231200', '231221', '0', '0', '1', '望奎县');
INSERT INTO `system_areabase` VALUES ('872', '90', '231200', '231222', '0', '0', '1', '兰西县');
INSERT INTO `system_areabase` VALUES ('873', '90', '231200', '231223', '0', '0', '1', '青冈县');
INSERT INTO `system_areabase` VALUES ('874', '90', '231200', '231224', '0', '0', '1', '庆安县');
INSERT INTO `system_areabase` VALUES ('875', '90', '231200', '231225', '0', '0', '1', '明水县');
INSERT INTO `system_areabase` VALUES ('876', '90', '231200', '231226', '0', '0', '1', '绥棱县');
INSERT INTO `system_areabase` VALUES ('877', '90', '231200', '231281', '0', '0', '1', '安达市');
INSERT INTO `system_areabase` VALUES ('878', '90', '231200', '231282', '0', '0', '1', '肇东市');
INSERT INTO `system_areabase` VALUES ('879', '90', '231200', '231283', '0', '0', '1', '海伦市');
INSERT INTO `system_areabase` VALUES ('880', '90', '231200', '231284', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('881', '90', '230000', '232700', '0', '0', '1', '大兴安岭地区');
INSERT INTO `system_areabase` VALUES ('882', '90', '232700', '232721', '0', '0', '1', '呼玛县');
INSERT INTO `system_areabase` VALUES ('883', '90', '232700', '232722', '0', '0', '1', '塔河县');
INSERT INTO `system_areabase` VALUES ('884', '90', '232700', '232723', '0', '0', '1', '漠河县');
INSERT INTO `system_areabase` VALUES ('885', '90', '232700', '232724', '0', '0', '1', '加格达奇区');
INSERT INTO `system_areabase` VALUES ('886', '90', '232700', '232725', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('887', '90', '0', '310000', '0', '0', '1', '上海');
INSERT INTO `system_areabase` VALUES ('888', '90', '310000', '310100', '0', '0', '1', '上海市');
INSERT INTO `system_areabase` VALUES ('889', '90', '310100', '310101', '0', '0', '1', '黄浦区');
INSERT INTO `system_areabase` VALUES ('890', '90', '310100', '310103', '0', '0', '1', '卢湾区');
INSERT INTO `system_areabase` VALUES ('891', '90', '310100', '310104', '0', '0', '1', '徐汇区');
INSERT INTO `system_areabase` VALUES ('892', '90', '310100', '310105', '0', '0', '1', '长宁区');
INSERT INTO `system_areabase` VALUES ('893', '90', '310100', '310106', '0', '0', '1', '静安区');
INSERT INTO `system_areabase` VALUES ('894', '90', '310100', '310107', '0', '0', '1', '普陀区');
INSERT INTO `system_areabase` VALUES ('895', '90', '310100', '310108', '0', '0', '1', '闸北区');
INSERT INTO `system_areabase` VALUES ('896', '90', '310100', '310109', '0', '0', '1', '虹口区');
INSERT INTO `system_areabase` VALUES ('897', '90', '310100', '310110', '0', '0', '1', '杨浦区');
INSERT INTO `system_areabase` VALUES ('898', '90', '310100', '310112', '0', '0', '1', '闵行区');
INSERT INTO `system_areabase` VALUES ('899', '90', '310100', '310113', '0', '0', '1', '宝山区');
INSERT INTO `system_areabase` VALUES ('900', '90', '310100', '310114', '0', '0', '1', '嘉定区');
INSERT INTO `system_areabase` VALUES ('901', '90', '310100', '310115', '0', '0', '1', '浦东新区');
INSERT INTO `system_areabase` VALUES ('902', '90', '310100', '310116', '0', '0', '1', '金山区');
INSERT INTO `system_areabase` VALUES ('903', '90', '310100', '310117', '0', '0', '1', '松江区');
INSERT INTO `system_areabase` VALUES ('904', '90', '310100', '310118', '0', '0', '1', '青浦区');
INSERT INTO `system_areabase` VALUES ('905', '90', '310100', '310119', '0', '0', '1', '南汇区');
INSERT INTO `system_areabase` VALUES ('906', '90', '310100', '310120', '0', '0', '1', '奉贤区');
INSERT INTO `system_areabase` VALUES ('907', '90', '310100', '310152', '0', '0', '1', '川沙区');
INSERT INTO `system_areabase` VALUES ('908', '90', '310100', '310230', '0', '0', '1', '崇明县');
INSERT INTO `system_areabase` VALUES ('909', '90', '310100', '310231', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('910', '90', '0', '320000', '0', '0', '1', '江苏省');
INSERT INTO `system_areabase` VALUES ('911', '90', '320000', '320100', '0', '0', '1', '南京市');
INSERT INTO `system_areabase` VALUES ('912', '90', '320100', '320102', '0', '0', '1', '玄武区');
INSERT INTO `system_areabase` VALUES ('913', '90', '320100', '320103', '0', '0', '1', '白下区');
INSERT INTO `system_areabase` VALUES ('914', '90', '320100', '320104', '0', '0', '1', '秦淮区');
INSERT INTO `system_areabase` VALUES ('915', '90', '320100', '320105', '0', '0', '1', '建邺区');
INSERT INTO `system_areabase` VALUES ('916', '90', '320100', '320106', '0', '0', '1', '鼓楼区');
INSERT INTO `system_areabase` VALUES ('917', '90', '320100', '320107', '0', '0', '1', '下关区');
INSERT INTO `system_areabase` VALUES ('918', '90', '320100', '320111', '0', '0', '1', '浦口区');
INSERT INTO `system_areabase` VALUES ('919', '90', '320100', '320113', '0', '0', '1', '栖霞区');
INSERT INTO `system_areabase` VALUES ('920', '90', '320100', '320114', '0', '0', '1', '雨花台区');
INSERT INTO `system_areabase` VALUES ('921', '90', '320100', '320115', '0', '0', '1', '江宁区');
INSERT INTO `system_areabase` VALUES ('922', '90', '320100', '320116', '0', '0', '1', '六合区');
INSERT INTO `system_areabase` VALUES ('923', '90', '320100', '320124', '0', '0', '1', '溧水县');
INSERT INTO `system_areabase` VALUES ('924', '90', '320100', '320125', '0', '0', '1', '高淳县');
INSERT INTO `system_areabase` VALUES ('925', '90', '320100', '320126', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('926', '90', '320000', '320200', '0', '0', '1', '无锡市');
INSERT INTO `system_areabase` VALUES ('927', '90', '320200', '320202', '0', '0', '1', '崇安区');
INSERT INTO `system_areabase` VALUES ('928', '90', '320200', '320203', '0', '0', '1', '南长区');
INSERT INTO `system_areabase` VALUES ('929', '90', '320200', '320204', '0', '0', '1', '北塘区');
INSERT INTO `system_areabase` VALUES ('930', '90', '320200', '320205', '0', '0', '1', '锡山区');
INSERT INTO `system_areabase` VALUES ('931', '90', '320200', '320206', '0', '0', '1', '惠山区');
INSERT INTO `system_areabase` VALUES ('932', '90', '320200', '320211', '0', '0', '1', '滨湖区');
INSERT INTO `system_areabase` VALUES ('933', '90', '320200', '320281', '0', '0', '1', '江阴市');
INSERT INTO `system_areabase` VALUES ('934', '90', '320200', '320282', '0', '0', '1', '宜兴市');
INSERT INTO `system_areabase` VALUES ('935', '90', '320200', '320296', '0', '0', '1', '新区');
INSERT INTO `system_areabase` VALUES ('936', '90', '320200', '320297', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('937', '90', '320000', '320300', '0', '0', '1', '徐州市');
INSERT INTO `system_areabase` VALUES ('938', '90', '320300', '320302', '0', '0', '1', '鼓楼区');
INSERT INTO `system_areabase` VALUES ('939', '90', '320300', '320303', '0', '0', '1', '云龙区');
INSERT INTO `system_areabase` VALUES ('940', '90', '320300', '320304', '0', '0', '1', '九里区');
INSERT INTO `system_areabase` VALUES ('941', '90', '320300', '320305', '0', '0', '1', '贾汪区');
INSERT INTO `system_areabase` VALUES ('942', '90', '320300', '320311', '0', '0', '1', '泉山区');
INSERT INTO `system_areabase` VALUES ('943', '90', '320300', '320321', '0', '0', '1', '丰县');
INSERT INTO `system_areabase` VALUES ('944', '90', '320300', '320322', '0', '0', '1', '沛县');
INSERT INTO `system_areabase` VALUES ('945', '90', '320300', '320323', '0', '0', '1', '铜山县');
INSERT INTO `system_areabase` VALUES ('946', '90', '320300', '320324', '0', '0', '1', '睢宁县');
INSERT INTO `system_areabase` VALUES ('947', '90', '320300', '320381', '0', '0', '1', '新沂市');
INSERT INTO `system_areabase` VALUES ('948', '90', '320300', '320382', '0', '0', '1', '邳州市');
INSERT INTO `system_areabase` VALUES ('949', '90', '320300', '320383', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('950', '90', '320000', '320400', '0', '0', '1', '常州市');
INSERT INTO `system_areabase` VALUES ('951', '90', '320400', '320402', '0', '0', '1', '天宁区');
INSERT INTO `system_areabase` VALUES ('952', '90', '320400', '320404', '0', '0', '1', '钟楼区');
INSERT INTO `system_areabase` VALUES ('953', '90', '320400', '320405', '0', '0', '1', '戚墅堰区');
INSERT INTO `system_areabase` VALUES ('954', '90', '320400', '320411', '0', '0', '1', '新北区');
INSERT INTO `system_areabase` VALUES ('955', '90', '320400', '320412', '0', '0', '1', '武进区');
INSERT INTO `system_areabase` VALUES ('956', '90', '320400', '320481', '0', '0', '1', '溧阳市');
INSERT INTO `system_areabase` VALUES ('957', '90', '320400', '320482', '0', '0', '1', '金坛市');
INSERT INTO `system_areabase` VALUES ('958', '90', '320400', '320483', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('959', '90', '320000', '320500', '0', '0', '1', '苏州市');
INSERT INTO `system_areabase` VALUES ('960', '90', '320500', '320502', '0', '0', '1', '沧浪区');
INSERT INTO `system_areabase` VALUES ('961', '90', '320500', '320503', '0', '0', '1', '平江区');
INSERT INTO `system_areabase` VALUES ('962', '90', '320500', '320504', '0', '0', '1', '金阊区');
INSERT INTO `system_areabase` VALUES ('963', '90', '320500', '320505', '0', '0', '1', '虎丘区');
INSERT INTO `system_areabase` VALUES ('964', '90', '320500', '320506', '0', '0', '1', '吴中区');
INSERT INTO `system_areabase` VALUES ('965', '90', '320500', '320507', '0', '0', '1', '相城区');
INSERT INTO `system_areabase` VALUES ('966', '90', '320500', '320581', '0', '0', '1', '常熟市');
INSERT INTO `system_areabase` VALUES ('967', '90', '320500', '320582', '0', '0', '1', '张家港市');
INSERT INTO `system_areabase` VALUES ('968', '90', '320500', '320583', '0', '0', '1', '昆山市');
INSERT INTO `system_areabase` VALUES ('969', '90', '320500', '320584', '0', '0', '1', '吴江市');
INSERT INTO `system_areabase` VALUES ('970', '90', '320500', '320585', '0', '0', '1', '太仓市');
INSERT INTO `system_areabase` VALUES ('971', '90', '320500', '320594', '0', '0', '1', '新区');
INSERT INTO `system_areabase` VALUES ('972', '90', '320500', '320595', '0', '0', '1', '园区');
INSERT INTO `system_areabase` VALUES ('973', '90', '320500', '320596', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('974', '90', '320000', '320600', '0', '0', '1', '南通市');
INSERT INTO `system_areabase` VALUES ('975', '90', '320600', '320602', '0', '0', '1', '崇川区');
INSERT INTO `system_areabase` VALUES ('976', '90', '320600', '320611', '0', '0', '1', '港闸区');
INSERT INTO `system_areabase` VALUES ('977', '90', '320600', '320621', '0', '0', '1', '海安县');
INSERT INTO `system_areabase` VALUES ('978', '90', '320600', '320623', '0', '0', '1', '如东县');
INSERT INTO `system_areabase` VALUES ('979', '90', '320600', '320681', '0', '0', '1', '启东市');
INSERT INTO `system_areabase` VALUES ('980', '90', '320600', '320682', '0', '0', '1', '如皋市');
INSERT INTO `system_areabase` VALUES ('981', '90', '320600', '320683', '0', '0', '1', '通州市');
INSERT INTO `system_areabase` VALUES ('982', '90', '320600', '320684', '0', '0', '1', '海门市');
INSERT INTO `system_areabase` VALUES ('983', '90', '320600', '320693', '0', '0', '1', '开发区');
INSERT INTO `system_areabase` VALUES ('984', '90', '320600', '320694', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('985', '90', '320000', '320700', '0', '0', '1', '连云港市');
INSERT INTO `system_areabase` VALUES ('986', '90', '320700', '320703', '0', '0', '1', '连云区');
INSERT INTO `system_areabase` VALUES ('987', '90', '320700', '320705', '0', '0', '1', '新浦区');
INSERT INTO `system_areabase` VALUES ('988', '90', '320700', '320706', '0', '0', '1', '海州区');
INSERT INTO `system_areabase` VALUES ('989', '90', '320700', '320721', '0', '0', '1', '赣榆县');
INSERT INTO `system_areabase` VALUES ('990', '90', '320700', '320722', '0', '0', '1', '东海县');
INSERT INTO `system_areabase` VALUES ('991', '90', '320700', '320723', '0', '0', '1', '灌云县');
INSERT INTO `system_areabase` VALUES ('992', '90', '320700', '320724', '0', '0', '1', '灌南县');
INSERT INTO `system_areabase` VALUES ('993', '90', '320700', '320725', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('994', '90', '320000', '320800', '0', '0', '1', '淮安市');
INSERT INTO `system_areabase` VALUES ('995', '90', '320800', '320802', '0', '0', '1', '清河区');
INSERT INTO `system_areabase` VALUES ('996', '90', '320800', '320803', '0', '0', '1', '楚州区');
INSERT INTO `system_areabase` VALUES ('997', '90', '320800', '320804', '0', '0', '1', '淮阴区');
INSERT INTO `system_areabase` VALUES ('998', '90', '320800', '320811', '0', '0', '1', '清浦区');
INSERT INTO `system_areabase` VALUES ('999', '90', '320800', '320826', '0', '0', '1', '涟水县');
INSERT INTO `system_areabase` VALUES ('1000', '90', '320800', '320829', '0', '0', '1', '洪泽县');
INSERT INTO `system_areabase` VALUES ('1001', '90', '320800', '320830', '0', '0', '1', '盱眙县');
INSERT INTO `system_areabase` VALUES ('1002', '90', '320800', '320831', '0', '0', '1', '金湖县');
INSERT INTO `system_areabase` VALUES ('1003', '90', '320800', '320832', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1004', '90', '320000', '320900', '0', '0', '1', '盐城市');
INSERT INTO `system_areabase` VALUES ('1005', '90', '320900', '320902', '0', '0', '1', '亭湖区');
INSERT INTO `system_areabase` VALUES ('1006', '90', '320900', '320903', '0', '0', '1', '盐都区');
INSERT INTO `system_areabase` VALUES ('1007', '90', '320900', '320921', '0', '0', '1', '响水县');
INSERT INTO `system_areabase` VALUES ('1008', '90', '320900', '320922', '0', '0', '1', '滨海县');
INSERT INTO `system_areabase` VALUES ('1009', '90', '320900', '320923', '0', '0', '1', '阜宁县');
INSERT INTO `system_areabase` VALUES ('1010', '90', '320900', '320924', '0', '0', '1', '射阳县');
INSERT INTO `system_areabase` VALUES ('1011', '90', '320900', '320925', '0', '0', '1', '建湖县');
INSERT INTO `system_areabase` VALUES ('1012', '90', '320900', '320981', '0', '0', '1', '东台市');
INSERT INTO `system_areabase` VALUES ('1013', '90', '320900', '320982', '0', '0', '1', '大丰市');
INSERT INTO `system_areabase` VALUES ('1014', '90', '320900', '320983', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1015', '90', '320000', '321000', '0', '0', '1', '扬州市');
INSERT INTO `system_areabase` VALUES ('1016', '90', '321000', '321002', '0', '0', '1', '广陵区');
INSERT INTO `system_areabase` VALUES ('1017', '90', '321000', '321003', '0', '0', '1', '邗江区');
INSERT INTO `system_areabase` VALUES ('1018', '90', '321000', '321011', '0', '0', '1', '维扬区');
INSERT INTO `system_areabase` VALUES ('1019', '90', '321000', '321023', '0', '0', '1', '宝应县');
INSERT INTO `system_areabase` VALUES ('1020', '90', '321000', '321081', '0', '0', '1', '仪征市');
INSERT INTO `system_areabase` VALUES ('1021', '90', '321000', '321084', '0', '0', '1', '高邮市');
INSERT INTO `system_areabase` VALUES ('1022', '90', '321000', '321088', '0', '0', '1', '江都市');
INSERT INTO `system_areabase` VALUES ('1023', '90', '321000', '321092', '0', '0', '1', '经济开发区');
INSERT INTO `system_areabase` VALUES ('1024', '90', '321000', '321093', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1025', '90', '320000', '321100', '0', '0', '1', '镇江市');
INSERT INTO `system_areabase` VALUES ('1026', '90', '321100', '321102', '0', '0', '1', '京口区');
INSERT INTO `system_areabase` VALUES ('1027', '90', '321100', '321111', '0', '0', '1', '润州区');
INSERT INTO `system_areabase` VALUES ('1028', '90', '321100', '321112', '0', '0', '1', '丹徒区');
INSERT INTO `system_areabase` VALUES ('1029', '90', '321100', '321181', '0', '0', '1', '丹阳市');
INSERT INTO `system_areabase` VALUES ('1030', '90', '321100', '321182', '0', '0', '1', '扬中市');
INSERT INTO `system_areabase` VALUES ('1031', '90', '321100', '321183', '0', '0', '1', '句容市');
INSERT INTO `system_areabase` VALUES ('1032', '90', '321100', '321184', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1033', '90', '320000', '321200', '0', '0', '1', '泰州市');
INSERT INTO `system_areabase` VALUES ('1034', '90', '321200', '321202', '0', '0', '1', '海陵区');
INSERT INTO `system_areabase` VALUES ('1035', '90', '321200', '321203', '0', '0', '1', '高港区');
INSERT INTO `system_areabase` VALUES ('1036', '90', '321200', '321281', '0', '0', '1', '兴化市');
INSERT INTO `system_areabase` VALUES ('1037', '90', '321200', '321282', '0', '0', '1', '靖江市');
INSERT INTO `system_areabase` VALUES ('1038', '90', '321200', '321283', '0', '0', '1', '泰兴市');
INSERT INTO `system_areabase` VALUES ('1039', '90', '321200', '321284', '0', '0', '1', '姜堰市');
INSERT INTO `system_areabase` VALUES ('1040', '90', '321200', '321285', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1041', '90', '320000', '321300', '0', '0', '1', '宿迁市');
INSERT INTO `system_areabase` VALUES ('1042', '90', '321300', '321302', '0', '0', '1', '宿城区');
INSERT INTO `system_areabase` VALUES ('1043', '90', '321300', '321311', '0', '0', '1', '宿豫区');
INSERT INTO `system_areabase` VALUES ('1044', '90', '321300', '321322', '0', '0', '1', '沭阳县');
INSERT INTO `system_areabase` VALUES ('1045', '90', '321300', '321323', '0', '0', '1', '泗阳县');
INSERT INTO `system_areabase` VALUES ('1046', '90', '321300', '321324', '0', '0', '1', '泗洪县');
INSERT INTO `system_areabase` VALUES ('1047', '90', '321300', '321325', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1048', '90', '0', '330000', '0', '0', '1', '浙江省');
INSERT INTO `system_areabase` VALUES ('1049', '90', '330000', '330100', '0', '0', '1', '杭州市');
INSERT INTO `system_areabase` VALUES ('1050', '90', '330100', '330102', '0', '0', '1', '上城区');
INSERT INTO `system_areabase` VALUES ('1051', '90', '330100', '330103', '0', '0', '1', '下城区');
INSERT INTO `system_areabase` VALUES ('1052', '90', '330100', '330104', '0', '0', '1', '江干区');
INSERT INTO `system_areabase` VALUES ('1053', '90', '330100', '330105', '0', '0', '1', '拱墅区');
INSERT INTO `system_areabase` VALUES ('1054', '90', '330100', '330106', '0', '0', '1', '西湖区');
INSERT INTO `system_areabase` VALUES ('1055', '90', '330100', '330108', '0', '0', '1', '滨江区');
INSERT INTO `system_areabase` VALUES ('1056', '90', '330100', '330109', '0', '0', '1', '萧山区');
INSERT INTO `system_areabase` VALUES ('1057', '90', '330100', '330110', '0', '0', '1', '余杭区');
INSERT INTO `system_areabase` VALUES ('1058', '90', '330100', '330122', '0', '0', '1', '桐庐县');
INSERT INTO `system_areabase` VALUES ('1059', '90', '330100', '330127', '0', '0', '1', '淳安县');
INSERT INTO `system_areabase` VALUES ('1060', '90', '330100', '330182', '0', '0', '1', '建德市');
INSERT INTO `system_areabase` VALUES ('1061', '90', '330100', '330183', '0', '0', '1', '富阳市');
INSERT INTO `system_areabase` VALUES ('1062', '90', '330100', '330185', '0', '0', '1', '临安市');
INSERT INTO `system_areabase` VALUES ('1063', '90', '330100', '330186', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1064', '90', '330000', '330200', '0', '0', '1', '宁波市');
INSERT INTO `system_areabase` VALUES ('1065', '90', '330200', '330203', '0', '0', '1', '海曙区');
INSERT INTO `system_areabase` VALUES ('1066', '90', '330200', '330204', '0', '0', '1', '江东区');
INSERT INTO `system_areabase` VALUES ('1067', '90', '330200', '330205', '0', '0', '1', '江北区');
INSERT INTO `system_areabase` VALUES ('1068', '90', '330200', '330206', '0', '0', '1', '北仑区');
INSERT INTO `system_areabase` VALUES ('1069', '90', '330200', '330211', '0', '0', '1', '镇海区');
INSERT INTO `system_areabase` VALUES ('1070', '90', '330200', '330212', '0', '0', '1', '鄞州区');
INSERT INTO `system_areabase` VALUES ('1071', '90', '330200', '330225', '0', '0', '1', '象山县');
INSERT INTO `system_areabase` VALUES ('1072', '90', '330200', '330226', '0', '0', '1', '宁海县');
INSERT INTO `system_areabase` VALUES ('1073', '90', '330200', '330281', '0', '0', '1', '余姚市');
INSERT INTO `system_areabase` VALUES ('1074', '90', '330200', '330282', '0', '0', '1', '慈溪市');
INSERT INTO `system_areabase` VALUES ('1075', '90', '330200', '330283', '0', '0', '1', '奉化市');
INSERT INTO `system_areabase` VALUES ('1076', '90', '330200', '330284', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1077', '90', '330000', '330300', '0', '0', '1', '温州市');
INSERT INTO `system_areabase` VALUES ('1078', '90', '330300', '330302', '0', '0', '1', '鹿城区');
INSERT INTO `system_areabase` VALUES ('1079', '90', '330300', '330303', '0', '0', '1', '龙湾区');
INSERT INTO `system_areabase` VALUES ('1080', '90', '330300', '330304', '0', '0', '1', '瓯海区');
INSERT INTO `system_areabase` VALUES ('1081', '90', '330300', '330322', '0', '0', '1', '洞头县');
INSERT INTO `system_areabase` VALUES ('1082', '90', '330300', '330324', '0', '0', '1', '永嘉县');
INSERT INTO `system_areabase` VALUES ('1083', '90', '330300', '330326', '0', '0', '1', '平阳县');
INSERT INTO `system_areabase` VALUES ('1084', '90', '330300', '330327', '0', '0', '1', '苍南县');
INSERT INTO `system_areabase` VALUES ('1085', '90', '330300', '330328', '0', '0', '1', '文成县');
INSERT INTO `system_areabase` VALUES ('1086', '90', '330300', '330329', '0', '0', '1', '泰顺县');
INSERT INTO `system_areabase` VALUES ('1087', '90', '330300', '330381', '0', '0', '1', '瑞安市');
INSERT INTO `system_areabase` VALUES ('1088', '90', '330300', '330382', '0', '0', '1', '乐清市');
INSERT INTO `system_areabase` VALUES ('1089', '90', '330300', '330383', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1090', '90', '330000', '330400', '0', '0', '1', '嘉兴市');
INSERT INTO `system_areabase` VALUES ('1091', '90', '330400', '330402', '0', '0', '1', '南湖区');
INSERT INTO `system_areabase` VALUES ('1092', '90', '330400', '330411', '0', '0', '1', '秀洲区');
INSERT INTO `system_areabase` VALUES ('1093', '90', '330400', '330421', '0', '0', '1', '嘉善县');
INSERT INTO `system_areabase` VALUES ('1094', '90', '330400', '330424', '0', '0', '1', '海盐县');
INSERT INTO `system_areabase` VALUES ('1095', '90', '330400', '330481', '0', '0', '1', '海宁市');
INSERT INTO `system_areabase` VALUES ('1096', '90', '330400', '330482', '0', '0', '1', '平湖市');
INSERT INTO `system_areabase` VALUES ('1097', '90', '330400', '330483', '0', '0', '1', '桐乡市');
INSERT INTO `system_areabase` VALUES ('1098', '90', '330400', '330484', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1099', '90', '330000', '330500', '0', '0', '1', '湖州市');
INSERT INTO `system_areabase` VALUES ('1100', '90', '330500', '330502', '0', '0', '1', '吴兴区');
INSERT INTO `system_areabase` VALUES ('1101', '90', '330500', '330503', '0', '0', '1', '南浔区');
INSERT INTO `system_areabase` VALUES ('1102', '90', '330500', '330521', '0', '0', '1', '德清县');
INSERT INTO `system_areabase` VALUES ('1103', '90', '330500', '330522', '0', '0', '1', '长兴县');
INSERT INTO `system_areabase` VALUES ('1104', '90', '330500', '330523', '0', '0', '1', '安吉县');
INSERT INTO `system_areabase` VALUES ('1105', '90', '330500', '330524', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1106', '90', '330000', '330600', '0', '0', '1', '绍兴市');
INSERT INTO `system_areabase` VALUES ('1107', '90', '330600', '330602', '0', '0', '1', '越城区');
INSERT INTO `system_areabase` VALUES ('1108', '90', '330600', '330621', '0', '0', '1', '绍兴县');
INSERT INTO `system_areabase` VALUES ('1109', '90', '330600', '330624', '0', '0', '1', '新昌县');
INSERT INTO `system_areabase` VALUES ('1110', '90', '330600', '330681', '0', '0', '1', '诸暨市');
INSERT INTO `system_areabase` VALUES ('1111', '90', '330600', '330682', '0', '0', '1', '上虞市');
INSERT INTO `system_areabase` VALUES ('1112', '90', '330600', '330683', '0', '0', '1', '嵊州市');
INSERT INTO `system_areabase` VALUES ('1113', '90', '330600', '330684', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1114', '90', '330000', '330700', '0', '0', '1', '金华市');
INSERT INTO `system_areabase` VALUES ('1115', '90', '330700', '330702', '0', '0', '1', '婺城区');
INSERT INTO `system_areabase` VALUES ('1116', '90', '330700', '330703', '0', '0', '1', '金东区');
INSERT INTO `system_areabase` VALUES ('1117', '90', '330700', '330723', '0', '0', '1', '武义县');
INSERT INTO `system_areabase` VALUES ('1118', '90', '330700', '330726', '0', '0', '1', '浦江县');
INSERT INTO `system_areabase` VALUES ('1119', '90', '330700', '330727', '0', '0', '1', '磐安县');
INSERT INTO `system_areabase` VALUES ('1120', '90', '330700', '330781', '0', '0', '1', '兰溪市');
INSERT INTO `system_areabase` VALUES ('1121', '90', '330700', '330782', '0', '0', '1', '义乌市');
INSERT INTO `system_areabase` VALUES ('1122', '90', '330700', '330783', '0', '0', '1', '东阳市');
INSERT INTO `system_areabase` VALUES ('1123', '90', '330700', '330784', '0', '0', '1', '永康市');
INSERT INTO `system_areabase` VALUES ('1124', '90', '330700', '330785', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1125', '90', '330000', '330800', '0', '0', '1', '衢州市');
INSERT INTO `system_areabase` VALUES ('1126', '90', '330800', '330802', '0', '0', '1', '柯城区');
INSERT INTO `system_areabase` VALUES ('1127', '90', '330800', '330803', '0', '0', '1', '衢江区');
INSERT INTO `system_areabase` VALUES ('1128', '90', '330800', '330822', '0', '0', '1', '常山县');
INSERT INTO `system_areabase` VALUES ('1129', '90', '330800', '330824', '0', '0', '1', '开化县');
INSERT INTO `system_areabase` VALUES ('1130', '90', '330800', '330825', '0', '0', '1', '龙游县');
INSERT INTO `system_areabase` VALUES ('1131', '90', '330800', '330881', '0', '0', '1', '江山市');
INSERT INTO `system_areabase` VALUES ('1132', '90', '330800', '330882', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1133', '90', '330000', '330900', '0', '0', '1', '舟山市');
INSERT INTO `system_areabase` VALUES ('1134', '90', '330900', '330902', '0', '0', '1', '定海区');
INSERT INTO `system_areabase` VALUES ('1135', '90', '330900', '330903', '0', '0', '1', '普陀区');
INSERT INTO `system_areabase` VALUES ('1136', '90', '330900', '330921', '0', '0', '1', '岱山县');
INSERT INTO `system_areabase` VALUES ('1137', '90', '330900', '330922', '0', '0', '1', '嵊泗县');
INSERT INTO `system_areabase` VALUES ('1138', '90', '330900', '330923', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1139', '90', '330000', '331000', '0', '0', '1', '台州市');
INSERT INTO `system_areabase` VALUES ('1140', '90', '331000', '331002', '0', '0', '1', '椒江区');
INSERT INTO `system_areabase` VALUES ('1141', '90', '331000', '331003', '0', '0', '1', '黄岩区');
INSERT INTO `system_areabase` VALUES ('1142', '90', '331000', '331004', '0', '0', '1', '路桥区');
INSERT INTO `system_areabase` VALUES ('1143', '90', '331000', '331021', '0', '0', '1', '玉环县');
INSERT INTO `system_areabase` VALUES ('1144', '90', '331000', '331022', '0', '0', '1', '三门县');
INSERT INTO `system_areabase` VALUES ('1145', '90', '331000', '331023', '0', '0', '1', '天台县');
INSERT INTO `system_areabase` VALUES ('1146', '90', '331000', '331024', '0', '0', '1', '仙居县');
INSERT INTO `system_areabase` VALUES ('1147', '90', '331000', '331081', '0', '0', '1', '温岭市');
INSERT INTO `system_areabase` VALUES ('1148', '90', '331000', '331082', '0', '0', '1', '临海市');
INSERT INTO `system_areabase` VALUES ('1149', '90', '331000', '331083', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1150', '90', '330000', '331100', '0', '0', '1', '丽水市');
INSERT INTO `system_areabase` VALUES ('1151', '90', '331100', '331102', '0', '0', '1', '莲都区');
INSERT INTO `system_areabase` VALUES ('1152', '90', '331100', '331121', '0', '0', '1', '青田县');
INSERT INTO `system_areabase` VALUES ('1153', '90', '331100', '331122', '0', '0', '1', '缙云县');
INSERT INTO `system_areabase` VALUES ('1154', '90', '331100', '331123', '0', '0', '1', '遂昌县');
INSERT INTO `system_areabase` VALUES ('1155', '90', '331100', '331124', '0', '0', '1', '松阳县');
INSERT INTO `system_areabase` VALUES ('1156', '90', '331100', '331125', '0', '0', '1', '云和县');
INSERT INTO `system_areabase` VALUES ('1157', '90', '331100', '331126', '0', '0', '1', '庆元县');
INSERT INTO `system_areabase` VALUES ('1158', '90', '331100', '331127', '0', '0', '1', '景宁畲族自治县');
INSERT INTO `system_areabase` VALUES ('1159', '90', '331100', '331181', '0', '0', '1', '龙泉市');
INSERT INTO `system_areabase` VALUES ('1160', '90', '331100', '331182', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1161', '90', '0', '340000', '0', '0', '1', '安徽省');
INSERT INTO `system_areabase` VALUES ('1162', '90', '340000', '340100', '0', '0', '1', '合肥市');
INSERT INTO `system_areabase` VALUES ('1163', '90', '340100', '340102', '0', '0', '1', '瑶海区');
INSERT INTO `system_areabase` VALUES ('1164', '90', '340100', '340103', '0', '0', '1', '庐阳区');
INSERT INTO `system_areabase` VALUES ('1165', '90', '340100', '340104', '0', '0', '1', '蜀山区');
INSERT INTO `system_areabase` VALUES ('1166', '90', '340100', '340111', '0', '0', '1', '包河区');
INSERT INTO `system_areabase` VALUES ('1167', '90', '340100', '340121', '0', '0', '1', '长丰县');
INSERT INTO `system_areabase` VALUES ('1168', '90', '340100', '340122', '0', '0', '1', '肥东县');
INSERT INTO `system_areabase` VALUES ('1169', '90', '340100', '340123', '0', '0', '1', '肥西县');
INSERT INTO `system_areabase` VALUES ('1170', '90', '340100', '340151', '0', '0', '1', '高新区');
INSERT INTO `system_areabase` VALUES ('1171', '90', '340100', '340191', '0', '0', '1', '中区');
INSERT INTO `system_areabase` VALUES ('1172', '90', '340100', '340192', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1173', '90', '340000', '340200', '0', '0', '1', '芜湖市');
INSERT INTO `system_areabase` VALUES ('1174', '90', '340200', '340202', '0', '0', '1', '镜湖区');
INSERT INTO `system_areabase` VALUES ('1175', '90', '340200', '340203', '0', '0', '1', '弋江区');
INSERT INTO `system_areabase` VALUES ('1176', '90', '340200', '340207', '0', '0', '1', '鸠江区');
INSERT INTO `system_areabase` VALUES ('1177', '90', '340200', '340208', '0', '0', '1', '三山区');
INSERT INTO `system_areabase` VALUES ('1178', '90', '340200', '340221', '0', '0', '1', '芜湖县');
INSERT INTO `system_areabase` VALUES ('1179', '90', '340200', '340222', '0', '0', '1', '繁昌县');
INSERT INTO `system_areabase` VALUES ('1180', '90', '340200', '340223', '0', '0', '1', '南陵县');
INSERT INTO `system_areabase` VALUES ('1181', '90', '340200', '340224', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1182', '90', '340000', '340300', '0', '0', '1', '蚌埠市');
INSERT INTO `system_areabase` VALUES ('1183', '90', '340300', '340302', '0', '0', '1', '龙子湖区');
INSERT INTO `system_areabase` VALUES ('1184', '90', '340300', '340303', '0', '0', '1', '蚌山区');
INSERT INTO `system_areabase` VALUES ('1185', '90', '340300', '340304', '0', '0', '1', '禹会区');
INSERT INTO `system_areabase` VALUES ('1186', '90', '340300', '340311', '0', '0', '1', '淮上区');
INSERT INTO `system_areabase` VALUES ('1187', '90', '340300', '340321', '0', '0', '1', '怀远县');
INSERT INTO `system_areabase` VALUES ('1188', '90', '340300', '340322', '0', '0', '1', '五河县');
INSERT INTO `system_areabase` VALUES ('1189', '90', '340300', '340323', '0', '0', '1', '固镇县');
INSERT INTO `system_areabase` VALUES ('1190', '90', '340300', '340324', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1191', '90', '340000', '340400', '0', '0', '1', '淮南市');
INSERT INTO `system_areabase` VALUES ('1192', '90', '340400', '340402', '0', '0', '1', '大通区');
INSERT INTO `system_areabase` VALUES ('1193', '90', '340400', '340403', '0', '0', '1', '田家庵区');
INSERT INTO `system_areabase` VALUES ('1194', '90', '340400', '340404', '0', '0', '1', '谢家集区');
INSERT INTO `system_areabase` VALUES ('1195', '90', '340400', '340405', '0', '0', '1', '八公山区');
INSERT INTO `system_areabase` VALUES ('1196', '90', '340400', '340406', '0', '0', '1', '潘集区');
INSERT INTO `system_areabase` VALUES ('1197', '90', '340400', '340421', '0', '0', '1', '凤台县');
INSERT INTO `system_areabase` VALUES ('1198', '90', '340400', '340422', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1199', '90', '340000', '340500', '0', '0', '1', '马鞍山市');
INSERT INTO `system_areabase` VALUES ('1200', '90', '340500', '340502', '0', '0', '1', '金家庄区');
INSERT INTO `system_areabase` VALUES ('1201', '90', '340500', '340503', '0', '0', '1', '花山区');
INSERT INTO `system_areabase` VALUES ('1202', '90', '340500', '340504', '0', '0', '1', '雨山区');
INSERT INTO `system_areabase` VALUES ('1203', '90', '340500', '340521', '0', '0', '1', '当涂县');
INSERT INTO `system_areabase` VALUES ('1204', '90', '340500', '340522', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1205', '90', '340000', '340600', '0', '0', '1', '淮北市');
INSERT INTO `system_areabase` VALUES ('1206', '90', '340600', '340602', '0', '0', '1', '杜集区');
INSERT INTO `system_areabase` VALUES ('1207', '90', '340600', '340603', '0', '0', '1', '相山区');
INSERT INTO `system_areabase` VALUES ('1208', '90', '340600', '340604', '0', '0', '1', '烈山区');
INSERT INTO `system_areabase` VALUES ('1209', '90', '340600', '340621', '0', '0', '1', '濉溪县');
INSERT INTO `system_areabase` VALUES ('1210', '90', '340600', '340622', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1211', '90', '340000', '340700', '0', '0', '1', '铜陵市');
INSERT INTO `system_areabase` VALUES ('1212', '90', '340700', '340702', '0', '0', '1', '铜官山区');
INSERT INTO `system_areabase` VALUES ('1213', '90', '340700', '340703', '0', '0', '1', '狮子山区');
INSERT INTO `system_areabase` VALUES ('1214', '90', '340700', '340711', '0', '0', '1', '郊区');
INSERT INTO `system_areabase` VALUES ('1215', '90', '340700', '340721', '0', '0', '1', '铜陵县');
INSERT INTO `system_areabase` VALUES ('1216', '90', '340700', '340722', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1217', '90', '340000', '340800', '0', '0', '1', '安庆市');
INSERT INTO `system_areabase` VALUES ('1218', '90', '340800', '340802', '0', '0', '1', '迎江区');
INSERT INTO `system_areabase` VALUES ('1219', '90', '340800', '340803', '0', '0', '1', '大观区');
INSERT INTO `system_areabase` VALUES ('1220', '90', '340800', '340811', '0', '0', '1', '宜秀区');
INSERT INTO `system_areabase` VALUES ('1221', '90', '340800', '340822', '0', '0', '1', '怀宁县');
INSERT INTO `system_areabase` VALUES ('1222', '90', '340800', '340823', '0', '0', '1', '枞阳县');
INSERT INTO `system_areabase` VALUES ('1223', '90', '340800', '340824', '0', '0', '1', '潜山县');
INSERT INTO `system_areabase` VALUES ('1224', '90', '340800', '340825', '0', '0', '1', '太湖县');
INSERT INTO `system_areabase` VALUES ('1225', '90', '340800', '340826', '0', '0', '1', '宿松县');
INSERT INTO `system_areabase` VALUES ('1226', '90', '340800', '340827', '0', '0', '1', '望江县');
INSERT INTO `system_areabase` VALUES ('1227', '90', '340800', '340828', '0', '0', '1', '岳西县');
INSERT INTO `system_areabase` VALUES ('1228', '90', '340800', '340881', '0', '0', '1', '桐城市');
INSERT INTO `system_areabase` VALUES ('1229', '90', '340800', '340882', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1230', '90', '340000', '341000', '0', '0', '1', '黄山市');
INSERT INTO `system_areabase` VALUES ('1231', '90', '341000', '341002', '0', '0', '1', '屯溪区');
INSERT INTO `system_areabase` VALUES ('1232', '90', '341000', '341003', '0', '0', '1', '黄山区');
INSERT INTO `system_areabase` VALUES ('1233', '90', '341000', '341004', '0', '0', '1', '徽州区');
INSERT INTO `system_areabase` VALUES ('1234', '90', '341000', '341021', '0', '0', '1', '歙县');
INSERT INTO `system_areabase` VALUES ('1235', '90', '341000', '341022', '0', '0', '1', '休宁县');
INSERT INTO `system_areabase` VALUES ('1236', '90', '341000', '341023', '0', '0', '1', '黟县');
INSERT INTO `system_areabase` VALUES ('1237', '90', '341000', '341024', '0', '0', '1', '祁门县');
INSERT INTO `system_areabase` VALUES ('1238', '90', '341000', '341025', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1239', '90', '340000', '341100', '0', '0', '1', '滁州市');
INSERT INTO `system_areabase` VALUES ('1240', '90', '341100', '341102', '0', '0', '1', '琅琊区');
INSERT INTO `system_areabase` VALUES ('1241', '90', '341100', '341103', '0', '0', '1', '南谯区');
INSERT INTO `system_areabase` VALUES ('1242', '90', '341100', '341122', '0', '0', '1', '来安县');
INSERT INTO `system_areabase` VALUES ('1243', '90', '341100', '341124', '0', '0', '1', '全椒县');
INSERT INTO `system_areabase` VALUES ('1244', '90', '341100', '341125', '0', '0', '1', '定远县');
INSERT INTO `system_areabase` VALUES ('1245', '90', '341100', '341126', '0', '0', '1', '凤阳县');
INSERT INTO `system_areabase` VALUES ('1246', '90', '341100', '341181', '0', '0', '1', '天长市');
INSERT INTO `system_areabase` VALUES ('1247', '90', '341100', '341182', '0', '0', '1', '明光市');
INSERT INTO `system_areabase` VALUES ('1248', '90', '341100', '341183', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1249', '90', '340000', '341200', '0', '0', '1', '阜阳市');
INSERT INTO `system_areabase` VALUES ('1250', '90', '341200', '341202', '0', '0', '1', '颍州区');
INSERT INTO `system_areabase` VALUES ('1251', '90', '341200', '341203', '0', '0', '1', '颍东区');
INSERT INTO `system_areabase` VALUES ('1252', '90', '341200', '341204', '0', '0', '1', '颍泉区');
INSERT INTO `system_areabase` VALUES ('1253', '90', '341200', '341221', '0', '0', '1', '临泉县');
INSERT INTO `system_areabase` VALUES ('1254', '90', '341200', '341222', '0', '0', '1', '太和县');
INSERT INTO `system_areabase` VALUES ('1255', '90', '341200', '341225', '0', '0', '1', '阜南县');
INSERT INTO `system_areabase` VALUES ('1256', '90', '341200', '341226', '0', '0', '1', '颍上县');
INSERT INTO `system_areabase` VALUES ('1257', '90', '341200', '341282', '0', '0', '1', '界首');
INSERT INTO `system_areabase` VALUES ('1258', '90', '341200', '341283', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1259', '90', '340000', '341300', '0', '0', '1', '宿州市');
INSERT INTO `system_areabase` VALUES ('1260', '90', '341300', '341302', '0', '0', '1', '埇桥区');
INSERT INTO `system_areabase` VALUES ('1261', '90', '341300', '341321', '0', '0', '1', '砀山县');
INSERT INTO `system_areabase` VALUES ('1262', '90', '341300', '341322', '0', '0', '1', '萧县');
INSERT INTO `system_areabase` VALUES ('1263', '90', '341300', '341323', '0', '0', '1', '灵璧县');
INSERT INTO `system_areabase` VALUES ('1264', '90', '341300', '341324', '0', '0', '1', '泗县');
INSERT INTO `system_areabase` VALUES ('1265', '90', '341300', '341325', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1266', '90', '340000', '341400', '0', '0', '1', '巢湖市');
INSERT INTO `system_areabase` VALUES ('1267', '90', '341400', '341402', '0', '0', '1', '居巢区');
INSERT INTO `system_areabase` VALUES ('1268', '90', '341400', '341421', '0', '0', '1', '庐江县');
INSERT INTO `system_areabase` VALUES ('1269', '90', '341400', '341422', '0', '0', '1', '无为县');
INSERT INTO `system_areabase` VALUES ('1270', '90', '341400', '341423', '0', '0', '1', '含山县');
INSERT INTO `system_areabase` VALUES ('1271', '90', '341400', '341424', '0', '0', '1', '和县');
INSERT INTO `system_areabase` VALUES ('1272', '90', '341400', '341425', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1273', '90', '340000', '341500', '0', '0', '1', '六安市');
INSERT INTO `system_areabase` VALUES ('1274', '90', '341500', '341502', '0', '0', '1', '金安区');
INSERT INTO `system_areabase` VALUES ('1275', '90', '341500', '341503', '0', '0', '1', '裕安区');
INSERT INTO `system_areabase` VALUES ('1276', '90', '341500', '341521', '0', '0', '1', '寿县');
INSERT INTO `system_areabase` VALUES ('1277', '90', '341500', '341522', '0', '0', '1', '霍邱县');
INSERT INTO `system_areabase` VALUES ('1278', '90', '341500', '341523', '0', '0', '1', '舒城县');
INSERT INTO `system_areabase` VALUES ('1279', '90', '341500', '341524', '0', '0', '1', '金寨县');
INSERT INTO `system_areabase` VALUES ('1280', '90', '341500', '341525', '0', '0', '1', '霍山县');
INSERT INTO `system_areabase` VALUES ('1281', '90', '341500', '341526', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1282', '90', '340000', '341600', '0', '0', '1', '亳州市');
INSERT INTO `system_areabase` VALUES ('1283', '90', '341600', '341602', '0', '0', '1', '谯城区');
INSERT INTO `system_areabase` VALUES ('1284', '90', '341600', '341621', '0', '0', '1', '涡阳县');
INSERT INTO `system_areabase` VALUES ('1285', '90', '341600', '341622', '0', '0', '1', '蒙城县');
INSERT INTO `system_areabase` VALUES ('1286', '90', '341600', '341623', '0', '0', '1', '利辛县');
INSERT INTO `system_areabase` VALUES ('1287', '90', '341600', '341624', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1288', '90', '340000', '341700', '0', '0', '1', '池州市');
INSERT INTO `system_areabase` VALUES ('1289', '90', '341700', '341702', '0', '0', '1', '贵池区');
INSERT INTO `system_areabase` VALUES ('1290', '90', '341700', '341721', '0', '0', '1', '东至县');
INSERT INTO `system_areabase` VALUES ('1291', '90', '341700', '341722', '0', '0', '1', '石台县');
INSERT INTO `system_areabase` VALUES ('1292', '90', '341700', '341723', '0', '0', '1', '青阳县');
INSERT INTO `system_areabase` VALUES ('1293', '90', '341700', '341724', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1294', '90', '340000', '341800', '0', '0', '1', '宣城市');
INSERT INTO `system_areabase` VALUES ('1295', '90', '341800', '341802', '0', '0', '1', '宣州区');
INSERT INTO `system_areabase` VALUES ('1296', '90', '341800', '341821', '0', '0', '1', '郎溪县');
INSERT INTO `system_areabase` VALUES ('1297', '90', '341800', '341822', '0', '0', '1', '广德县');
INSERT INTO `system_areabase` VALUES ('1298', '90', '341800', '341823', '0', '0', '1', '泾县');
INSERT INTO `system_areabase` VALUES ('1299', '90', '341800', '341824', '0', '0', '1', '绩溪县');
INSERT INTO `system_areabase` VALUES ('1300', '90', '341800', '341825', '0', '0', '1', '旌德县');
INSERT INTO `system_areabase` VALUES ('1301', '90', '341800', '341881', '0', '0', '1', '宁国市');
INSERT INTO `system_areabase` VALUES ('1302', '90', '341800', '341882', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1303', '90', '0', '350000', '0', '0', '1', '福建省');
INSERT INTO `system_areabase` VALUES ('1304', '90', '350000', '350100', '0', '0', '1', '福州市');
INSERT INTO `system_areabase` VALUES ('1305', '90', '350100', '350102', '0', '0', '1', '鼓楼区');
INSERT INTO `system_areabase` VALUES ('1306', '90', '350100', '350103', '0', '0', '1', '台江区');
INSERT INTO `system_areabase` VALUES ('1307', '90', '350100', '350104', '0', '0', '1', '仓山区');
INSERT INTO `system_areabase` VALUES ('1308', '90', '350100', '350105', '0', '0', '1', '马尾区');
INSERT INTO `system_areabase` VALUES ('1309', '90', '350100', '350111', '0', '0', '1', '晋安区');
INSERT INTO `system_areabase` VALUES ('1310', '90', '350100', '350121', '0', '0', '1', '闽侯县');
INSERT INTO `system_areabase` VALUES ('1311', '90', '350100', '350122', '0', '0', '1', '连江县');
INSERT INTO `system_areabase` VALUES ('1312', '90', '350100', '350123', '0', '0', '1', '罗源县');
INSERT INTO `system_areabase` VALUES ('1313', '90', '350100', '350124', '0', '0', '1', '闽清县');
INSERT INTO `system_areabase` VALUES ('1314', '90', '350100', '350125', '0', '0', '1', '永泰县');
INSERT INTO `system_areabase` VALUES ('1315', '90', '350100', '350128', '0', '0', '1', '平潭县');
INSERT INTO `system_areabase` VALUES ('1316', '90', '350100', '350181', '0', '0', '1', '福清市');
INSERT INTO `system_areabase` VALUES ('1317', '90', '350100', '350182', '0', '0', '1', '长乐市');
INSERT INTO `system_areabase` VALUES ('1318', '90', '350100', '350183', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1319', '90', '350000', '350200', '0', '0', '1', '厦门市');
INSERT INTO `system_areabase` VALUES ('1320', '90', '350200', '350203', '0', '0', '1', '思明区');
INSERT INTO `system_areabase` VALUES ('1321', '90', '350200', '350205', '0', '0', '1', '海沧区');
INSERT INTO `system_areabase` VALUES ('1322', '90', '350200', '350206', '0', '0', '1', '湖里区');
INSERT INTO `system_areabase` VALUES ('1323', '90', '350200', '350211', '0', '0', '1', '集美区');
INSERT INTO `system_areabase` VALUES ('1324', '90', '350200', '350212', '0', '0', '1', '同安区');
INSERT INTO `system_areabase` VALUES ('1325', '90', '350200', '350213', '0', '0', '1', '翔安区');
INSERT INTO `system_areabase` VALUES ('1326', '90', '350200', '350214', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1327', '90', '350000', '350300', '0', '0', '1', '莆田市');
INSERT INTO `system_areabase` VALUES ('1328', '90', '350300', '350302', '0', '0', '1', '城厢区');
INSERT INTO `system_areabase` VALUES ('1329', '90', '350300', '350303', '0', '0', '1', '涵江区');
INSERT INTO `system_areabase` VALUES ('1330', '90', '350300', '350304', '0', '0', '1', '荔城区');
INSERT INTO `system_areabase` VALUES ('1331', '90', '350300', '350305', '0', '0', '1', '秀屿区');
INSERT INTO `system_areabase` VALUES ('1332', '90', '350300', '350322', '0', '0', '1', '仙游县');
INSERT INTO `system_areabase` VALUES ('1333', '90', '350300', '350323', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1334', '90', '350000', '350400', '0', '0', '1', '三明市');
INSERT INTO `system_areabase` VALUES ('1335', '90', '350400', '350402', '0', '0', '1', '梅列区');
INSERT INTO `system_areabase` VALUES ('1336', '90', '350400', '350403', '0', '0', '1', '三元区');
INSERT INTO `system_areabase` VALUES ('1337', '90', '350400', '350421', '0', '0', '1', '明溪县');
INSERT INTO `system_areabase` VALUES ('1338', '90', '350400', '350423', '0', '0', '1', '清流县');
INSERT INTO `system_areabase` VALUES ('1339', '90', '350400', '350424', '0', '0', '1', '宁化县');
INSERT INTO `system_areabase` VALUES ('1340', '90', '350400', '350425', '0', '0', '1', '大田县');
INSERT INTO `system_areabase` VALUES ('1341', '90', '350400', '350426', '0', '0', '1', '尤溪县');
INSERT INTO `system_areabase` VALUES ('1342', '90', '350400', '350427', '0', '0', '1', '沙县');
INSERT INTO `system_areabase` VALUES ('1343', '90', '350400', '350428', '0', '0', '1', '将乐县');
INSERT INTO `system_areabase` VALUES ('1344', '90', '350400', '350429', '0', '0', '1', '泰宁县');
INSERT INTO `system_areabase` VALUES ('1345', '90', '350400', '350430', '0', '0', '1', '建宁县');
INSERT INTO `system_areabase` VALUES ('1346', '90', '350400', '350481', '0', '0', '1', '永安市');
INSERT INTO `system_areabase` VALUES ('1347', '90', '350400', '350482', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1348', '90', '350000', '350500', '0', '0', '1', '泉州市');
INSERT INTO `system_areabase` VALUES ('1349', '90', '350500', '350502', '0', '0', '1', '鲤城区');
INSERT INTO `system_areabase` VALUES ('1350', '90', '350500', '350503', '0', '0', '1', '丰泽区');
INSERT INTO `system_areabase` VALUES ('1351', '90', '350500', '350504', '0', '0', '1', '洛江区');
INSERT INTO `system_areabase` VALUES ('1352', '90', '350500', '350505', '0', '0', '1', '泉港区');
INSERT INTO `system_areabase` VALUES ('1353', '90', '350500', '350521', '0', '0', '1', '惠安县');
INSERT INTO `system_areabase` VALUES ('1354', '90', '350500', '350524', '0', '0', '1', '安溪县');
INSERT INTO `system_areabase` VALUES ('1355', '90', '350500', '350525', '0', '0', '1', '永春县');
INSERT INTO `system_areabase` VALUES ('1356', '90', '350500', '350526', '0', '0', '1', '德化县');
INSERT INTO `system_areabase` VALUES ('1357', '90', '350500', '350527', '0', '0', '1', '金门县');
INSERT INTO `system_areabase` VALUES ('1358', '90', '350500', '350581', '0', '0', '1', '石狮市');
INSERT INTO `system_areabase` VALUES ('1359', '90', '350500', '350582', '0', '0', '1', '晋江市');
INSERT INTO `system_areabase` VALUES ('1360', '90', '350500', '350583', '0', '0', '1', '南安市');
INSERT INTO `system_areabase` VALUES ('1361', '90', '350500', '350584', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1362', '90', '350000', '350600', '0', '0', '1', '漳州市');
INSERT INTO `system_areabase` VALUES ('1363', '90', '350600', '350602', '0', '0', '1', '芗城区');
INSERT INTO `system_areabase` VALUES ('1364', '90', '350600', '350603', '0', '0', '1', '龙文区');
INSERT INTO `system_areabase` VALUES ('1365', '90', '350600', '350622', '0', '0', '1', '云霄县');
INSERT INTO `system_areabase` VALUES ('1366', '90', '350600', '350623', '0', '0', '1', '漳浦县');
INSERT INTO `system_areabase` VALUES ('1367', '90', '350600', '350624', '0', '0', '1', '诏安县');
INSERT INTO `system_areabase` VALUES ('1368', '90', '350600', '350625', '0', '0', '1', '长泰县');
INSERT INTO `system_areabase` VALUES ('1369', '90', '350600', '350626', '0', '0', '1', '东山县');
INSERT INTO `system_areabase` VALUES ('1370', '90', '350600', '350627', '0', '0', '1', '南靖县');
INSERT INTO `system_areabase` VALUES ('1371', '90', '350600', '350628', '0', '0', '1', '平和县');
INSERT INTO `system_areabase` VALUES ('1372', '90', '350600', '350629', '0', '0', '1', '华安县');
INSERT INTO `system_areabase` VALUES ('1373', '90', '350600', '350681', '0', '0', '1', '龙海市');
INSERT INTO `system_areabase` VALUES ('1374', '90', '350600', '350682', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1375', '90', '350000', '350700', '0', '0', '1', '南平市');
INSERT INTO `system_areabase` VALUES ('1376', '90', '350700', '350702', '0', '0', '1', '延平区');
INSERT INTO `system_areabase` VALUES ('1377', '90', '350700', '350721', '0', '0', '1', '顺昌县');
INSERT INTO `system_areabase` VALUES ('1378', '90', '350700', '350722', '0', '0', '1', '浦城县');
INSERT INTO `system_areabase` VALUES ('1379', '90', '350700', '350723', '0', '0', '1', '光泽县');
INSERT INTO `system_areabase` VALUES ('1380', '90', '350700', '350724', '0', '0', '1', '松溪县');
INSERT INTO `system_areabase` VALUES ('1381', '90', '350700', '350725', '0', '0', '1', '政和县');
INSERT INTO `system_areabase` VALUES ('1382', '90', '350700', '350781', '0', '0', '1', '邵武市');
INSERT INTO `system_areabase` VALUES ('1383', '90', '350700', '350782', '0', '0', '1', '武夷山市');
INSERT INTO `system_areabase` VALUES ('1384', '90', '350700', '350783', '0', '0', '1', '建瓯市');
INSERT INTO `system_areabase` VALUES ('1385', '90', '350700', '350784', '0', '0', '1', '建阳市');
INSERT INTO `system_areabase` VALUES ('1386', '90', '350700', '350785', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1387', '90', '350000', '350800', '0', '0', '1', '龙岩市');
INSERT INTO `system_areabase` VALUES ('1388', '90', '350800', '350802', '0', '0', '1', '新罗区');
INSERT INTO `system_areabase` VALUES ('1389', '90', '350800', '350821', '0', '0', '1', '长汀县');
INSERT INTO `system_areabase` VALUES ('1390', '90', '350800', '350822', '0', '0', '1', '永定县');
INSERT INTO `system_areabase` VALUES ('1391', '90', '350800', '350823', '0', '0', '1', '上杭县');
INSERT INTO `system_areabase` VALUES ('1392', '90', '350800', '350824', '0', '0', '1', '武平县');
INSERT INTO `system_areabase` VALUES ('1393', '90', '350800', '350825', '0', '0', '1', '连城县');
INSERT INTO `system_areabase` VALUES ('1394', '90', '350800', '350881', '0', '0', '1', '漳平市');
INSERT INTO `system_areabase` VALUES ('1395', '90', '350800', '350882', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1396', '90', '350000', '350900', '0', '0', '1', '宁德市');
INSERT INTO `system_areabase` VALUES ('1397', '90', '350900', '350902', '0', '0', '1', '蕉城区');
INSERT INTO `system_areabase` VALUES ('1398', '90', '350900', '350921', '0', '0', '1', '霞浦县');
INSERT INTO `system_areabase` VALUES ('1399', '90', '350900', '350922', '0', '0', '1', '古田县');
INSERT INTO `system_areabase` VALUES ('1400', '90', '350900', '350923', '0', '0', '1', '屏南县');
INSERT INTO `system_areabase` VALUES ('1401', '90', '350900', '350924', '0', '0', '1', '寿宁县');
INSERT INTO `system_areabase` VALUES ('1402', '90', '350900', '350925', '0', '0', '1', '周宁县');
INSERT INTO `system_areabase` VALUES ('1403', '90', '350900', '350926', '0', '0', '1', '柘荣县');
INSERT INTO `system_areabase` VALUES ('1404', '90', '350900', '350981', '0', '0', '1', '福安市');
INSERT INTO `system_areabase` VALUES ('1405', '90', '350900', '350982', '0', '0', '1', '福鼎市');
INSERT INTO `system_areabase` VALUES ('1406', '90', '350900', '350983', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1407', '90', '0', '360000', '0', '0', '1', '江西省');
INSERT INTO `system_areabase` VALUES ('1408', '90', '360000', '360100', '0', '0', '1', '南昌市');
INSERT INTO `system_areabase` VALUES ('1409', '90', '360100', '360102', '0', '0', '1', '东湖区');
INSERT INTO `system_areabase` VALUES ('1410', '90', '360100', '360103', '0', '0', '1', '西湖区');
INSERT INTO `system_areabase` VALUES ('1411', '90', '360100', '360104', '0', '0', '1', '青云谱区');
INSERT INTO `system_areabase` VALUES ('1412', '90', '360100', '360105', '0', '0', '1', '湾里区');
INSERT INTO `system_areabase` VALUES ('1413', '90', '360100', '360111', '0', '0', '1', '青山湖区');
INSERT INTO `system_areabase` VALUES ('1414', '90', '360100', '360121', '0', '0', '1', '南昌县');
INSERT INTO `system_areabase` VALUES ('1415', '90', '360100', '360122', '0', '0', '1', '新建县');
INSERT INTO `system_areabase` VALUES ('1416', '90', '360100', '360123', '0', '0', '1', '安义县');
INSERT INTO `system_areabase` VALUES ('1417', '90', '360100', '360124', '0', '0', '1', '进贤县');
INSERT INTO `system_areabase` VALUES ('1418', '90', '360100', '360125', '0', '0', '1', '红谷滩新区');
INSERT INTO `system_areabase` VALUES ('1419', '90', '360100', '360126', '0', '0', '1', '经济技术开发区');
INSERT INTO `system_areabase` VALUES ('1420', '90', '360100', '360127', '0', '0', '1', '昌北区');
INSERT INTO `system_areabase` VALUES ('1421', '90', '360100', '360128', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1422', '90', '360000', '360200', '0', '0', '1', '景德镇市');
INSERT INTO `system_areabase` VALUES ('1423', '90', '360200', '360202', '0', '0', '1', '昌江区');
INSERT INTO `system_areabase` VALUES ('1424', '90', '360200', '360203', '0', '0', '1', '珠山区');
INSERT INTO `system_areabase` VALUES ('1425', '90', '360200', '360222', '0', '0', '1', '浮梁县');
INSERT INTO `system_areabase` VALUES ('1426', '90', '360200', '360281', '0', '0', '1', '乐平市');
INSERT INTO `system_areabase` VALUES ('1427', '90', '360200', '360282', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1428', '90', '360000', '360300', '0', '0', '1', '萍乡市');
INSERT INTO `system_areabase` VALUES ('1429', '90', '360300', '360302', '0', '0', '1', '安源区');
INSERT INTO `system_areabase` VALUES ('1430', '90', '360300', '360313', '0', '0', '1', '湘东区');
INSERT INTO `system_areabase` VALUES ('1431', '90', '360300', '360321', '0', '0', '1', '莲花县');
INSERT INTO `system_areabase` VALUES ('1432', '90', '360300', '360322', '0', '0', '1', '上栗县');
INSERT INTO `system_areabase` VALUES ('1433', '90', '360300', '360323', '0', '0', '1', '芦溪县');
INSERT INTO `system_areabase` VALUES ('1434', '90', '360300', '360324', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1435', '90', '360000', '360400', '0', '0', '1', '九江市');
INSERT INTO `system_areabase` VALUES ('1436', '90', '360400', '360402', '0', '0', '1', '庐山区');
INSERT INTO `system_areabase` VALUES ('1437', '90', '360400', '360403', '0', '0', '1', '浔阳区');
INSERT INTO `system_areabase` VALUES ('1438', '90', '360400', '360421', '0', '0', '1', '九江县');
INSERT INTO `system_areabase` VALUES ('1439', '90', '360400', '360423', '0', '0', '1', '武宁县');
INSERT INTO `system_areabase` VALUES ('1440', '90', '360400', '360424', '0', '0', '1', '修水县');
INSERT INTO `system_areabase` VALUES ('1441', '90', '360400', '360425', '0', '0', '1', '永修县');
INSERT INTO `system_areabase` VALUES ('1442', '90', '360400', '360426', '0', '0', '1', '德安县');
INSERT INTO `system_areabase` VALUES ('1443', '90', '360400', '360427', '0', '0', '1', '星子县');
INSERT INTO `system_areabase` VALUES ('1444', '90', '360400', '360428', '0', '0', '1', '都昌县');
INSERT INTO `system_areabase` VALUES ('1445', '90', '360400', '360429', '0', '0', '1', '湖口县');
INSERT INTO `system_areabase` VALUES ('1446', '90', '360400', '360430', '0', '0', '1', '彭泽县');
INSERT INTO `system_areabase` VALUES ('1447', '90', '360400', '360481', '0', '0', '1', '瑞昌市');
INSERT INTO `system_areabase` VALUES ('1448', '90', '360400', '360482', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1449', '90', '360000', '360500', '0', '0', '1', '新余市');
INSERT INTO `system_areabase` VALUES ('1450', '90', '360500', '360502', '0', '0', '1', '渝水区');
INSERT INTO `system_areabase` VALUES ('1451', '90', '360500', '360521', '0', '0', '1', '分宜县');
INSERT INTO `system_areabase` VALUES ('1452', '90', '360500', '360522', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1453', '90', '360000', '360600', '0', '0', '1', '鹰潭市');
INSERT INTO `system_areabase` VALUES ('1454', '90', '360600', '360602', '0', '0', '1', '月湖区');
INSERT INTO `system_areabase` VALUES ('1455', '90', '360600', '360622', '0', '0', '1', '余江县');
INSERT INTO `system_areabase` VALUES ('1456', '90', '360600', '360681', '0', '0', '1', '贵溪市');
INSERT INTO `system_areabase` VALUES ('1457', '90', '360600', '360682', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1458', '90', '360000', '360700', '0', '0', '1', '赣州市');
INSERT INTO `system_areabase` VALUES ('1459', '90', '360700', '360702', '0', '0', '1', '章贡区');
INSERT INTO `system_areabase` VALUES ('1460', '90', '360700', '360721', '0', '0', '1', '赣县');
INSERT INTO `system_areabase` VALUES ('1461', '90', '360700', '360722', '0', '0', '1', '信丰县');
INSERT INTO `system_areabase` VALUES ('1462', '90', '360700', '360723', '0', '0', '1', '大余县');
INSERT INTO `system_areabase` VALUES ('1463', '90', '360700', '360724', '0', '0', '1', '上犹县');
INSERT INTO `system_areabase` VALUES ('1464', '90', '360700', '360725', '0', '0', '1', '崇义县');
INSERT INTO `system_areabase` VALUES ('1465', '90', '360700', '360726', '0', '0', '1', '安远县');
INSERT INTO `system_areabase` VALUES ('1466', '90', '360700', '360727', '0', '0', '1', '龙南县');
INSERT INTO `system_areabase` VALUES ('1467', '90', '360700', '360728', '0', '0', '1', '定南县');
INSERT INTO `system_areabase` VALUES ('1468', '90', '360700', '360729', '0', '0', '1', '全南县');
INSERT INTO `system_areabase` VALUES ('1469', '90', '360700', '360730', '0', '0', '1', '宁都县');
INSERT INTO `system_areabase` VALUES ('1470', '90', '360700', '360731', '0', '0', '1', '于都县');
INSERT INTO `system_areabase` VALUES ('1471', '90', '360700', '360732', '0', '0', '1', '兴国县');
INSERT INTO `system_areabase` VALUES ('1472', '90', '360700', '360733', '0', '0', '1', '会昌县');
INSERT INTO `system_areabase` VALUES ('1473', '90', '360700', '360734', '0', '0', '1', '寻乌县');
INSERT INTO `system_areabase` VALUES ('1474', '90', '360700', '360735', '0', '0', '1', '石城县');
INSERT INTO `system_areabase` VALUES ('1475', '90', '360700', '360751', '0', '0', '1', '黄金区');
INSERT INTO `system_areabase` VALUES ('1476', '90', '360700', '360781', '0', '0', '1', '瑞金市');
INSERT INTO `system_areabase` VALUES ('1477', '90', '360700', '360782', '0', '0', '1', '南康市');
INSERT INTO `system_areabase` VALUES ('1478', '90', '360700', '360783', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1479', '90', '360000', '360800', '0', '0', '1', '吉安市');
INSERT INTO `system_areabase` VALUES ('1480', '90', '360800', '360802', '0', '0', '1', '吉州区');
INSERT INTO `system_areabase` VALUES ('1481', '90', '360800', '360803', '0', '0', '1', '青原区');
INSERT INTO `system_areabase` VALUES ('1482', '90', '360800', '360821', '0', '0', '1', '吉安县');
INSERT INTO `system_areabase` VALUES ('1483', '90', '360800', '360822', '0', '0', '1', '吉水县');
INSERT INTO `system_areabase` VALUES ('1484', '90', '360800', '360823', '0', '0', '1', '峡江县');
INSERT INTO `system_areabase` VALUES ('1485', '90', '360800', '360824', '0', '0', '1', '新干县');
INSERT INTO `system_areabase` VALUES ('1486', '90', '360800', '360825', '0', '0', '1', '永丰县');
INSERT INTO `system_areabase` VALUES ('1487', '90', '360800', '360826', '0', '0', '1', '泰和县');
INSERT INTO `system_areabase` VALUES ('1488', '90', '360800', '360827', '0', '0', '1', '遂川县');
INSERT INTO `system_areabase` VALUES ('1489', '90', '360800', '360828', '0', '0', '1', '万安县');
INSERT INTO `system_areabase` VALUES ('1490', '90', '360800', '360829', '0', '0', '1', '安福县');
INSERT INTO `system_areabase` VALUES ('1491', '90', '360800', '360830', '0', '0', '1', '永新县');
INSERT INTO `system_areabase` VALUES ('1492', '90', '360800', '360881', '0', '0', '1', '井冈山市');
INSERT INTO `system_areabase` VALUES ('1493', '90', '360800', '360882', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1494', '90', '360000', '360900', '0', '0', '1', '宜春市');
INSERT INTO `system_areabase` VALUES ('1495', '90', '360900', '360902', '0', '0', '1', '袁州区');
INSERT INTO `system_areabase` VALUES ('1496', '90', '360900', '360921', '0', '0', '1', '奉新县');
INSERT INTO `system_areabase` VALUES ('1497', '90', '360900', '360922', '0', '0', '1', '万载县');
INSERT INTO `system_areabase` VALUES ('1498', '90', '360900', '360923', '0', '0', '1', '上高县');
INSERT INTO `system_areabase` VALUES ('1499', '90', '360900', '360924', '0', '0', '1', '宜丰县');
INSERT INTO `system_areabase` VALUES ('1500', '90', '360900', '360925', '0', '0', '1', '靖安县');
INSERT INTO `system_areabase` VALUES ('1501', '90', '360900', '360926', '0', '0', '1', '铜鼓县');
INSERT INTO `system_areabase` VALUES ('1502', '90', '360900', '360981', '0', '0', '1', '丰城市');
INSERT INTO `system_areabase` VALUES ('1503', '90', '360900', '360982', '0', '0', '1', '樟树市');
INSERT INTO `system_areabase` VALUES ('1504', '90', '360900', '360983', '0', '0', '1', '高安市');
INSERT INTO `system_areabase` VALUES ('1505', '90', '360900', '360984', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1506', '90', '360000', '361000', '0', '0', '1', '抚州市');
INSERT INTO `system_areabase` VALUES ('1507', '90', '361000', '361002', '0', '0', '1', '临川区');
INSERT INTO `system_areabase` VALUES ('1508', '90', '361000', '361021', '0', '0', '1', '南城县');
INSERT INTO `system_areabase` VALUES ('1509', '90', '361000', '361022', '0', '0', '1', '黎川县');
INSERT INTO `system_areabase` VALUES ('1510', '90', '361000', '361023', '0', '0', '1', '南丰县');
INSERT INTO `system_areabase` VALUES ('1511', '90', '361000', '361024', '0', '0', '1', '崇仁县');
INSERT INTO `system_areabase` VALUES ('1512', '90', '361000', '361025', '0', '0', '1', '乐安县');
INSERT INTO `system_areabase` VALUES ('1513', '90', '361000', '361026', '0', '0', '1', '宜黄县');
INSERT INTO `system_areabase` VALUES ('1514', '90', '361000', '361027', '0', '0', '1', '金溪县');
INSERT INTO `system_areabase` VALUES ('1515', '90', '361000', '361028', '0', '0', '1', '资溪县');
INSERT INTO `system_areabase` VALUES ('1516', '90', '361000', '361029', '0', '0', '1', '东乡县');
INSERT INTO `system_areabase` VALUES ('1517', '90', '361000', '361030', '0', '0', '1', '广昌县');
INSERT INTO `system_areabase` VALUES ('1518', '90', '361000', '361031', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1519', '90', '360000', '361100', '0', '0', '1', '上饶市');
INSERT INTO `system_areabase` VALUES ('1520', '90', '361100', '361102', '0', '0', '1', '信州区');
INSERT INTO `system_areabase` VALUES ('1521', '90', '361100', '361121', '0', '0', '1', '上饶县');
INSERT INTO `system_areabase` VALUES ('1522', '90', '361100', '361122', '0', '0', '1', '广丰县');
INSERT INTO `system_areabase` VALUES ('1523', '90', '361100', '361123', '0', '0', '1', '玉山县');
INSERT INTO `system_areabase` VALUES ('1524', '90', '361100', '361124', '0', '0', '1', '铅山县');
INSERT INTO `system_areabase` VALUES ('1525', '90', '361100', '361125', '0', '0', '1', '横峰县');
INSERT INTO `system_areabase` VALUES ('1526', '90', '361100', '361126', '0', '0', '1', '弋阳县');
INSERT INTO `system_areabase` VALUES ('1527', '90', '361100', '361127', '0', '0', '1', '余干县');
INSERT INTO `system_areabase` VALUES ('1528', '90', '361100', '361128', '0', '0', '1', '鄱阳县');
INSERT INTO `system_areabase` VALUES ('1529', '90', '361100', '361129', '0', '0', '1', '万年县');
INSERT INTO `system_areabase` VALUES ('1530', '90', '361100', '361130', '0', '0', '1', '婺源县');
INSERT INTO `system_areabase` VALUES ('1531', '90', '361100', '361181', '0', '0', '1', '德兴市');
INSERT INTO `system_areabase` VALUES ('1532', '90', '361100', '361182', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1533', '90', '0', '370000', '0', '0', '1', '山东省');
INSERT INTO `system_areabase` VALUES ('1534', '90', '370000', '370100', '0', '0', '1', '济南市');
INSERT INTO `system_areabase` VALUES ('1535', '90', '370100', '370102', '0', '0', '1', '历下区');
INSERT INTO `system_areabase` VALUES ('1536', '90', '370100', '370103', '0', '0', '1', '市中区');
INSERT INTO `system_areabase` VALUES ('1537', '90', '370100', '370104', '0', '0', '1', '槐荫区');
INSERT INTO `system_areabase` VALUES ('1538', '90', '370100', '370105', '0', '0', '1', '天桥区');
INSERT INTO `system_areabase` VALUES ('1539', '90', '370100', '370112', '0', '0', '1', '历城区');
INSERT INTO `system_areabase` VALUES ('1540', '90', '370100', '370113', '0', '0', '1', '长清区');
INSERT INTO `system_areabase` VALUES ('1541', '90', '370100', '370124', '0', '0', '1', '平阴县');
INSERT INTO `system_areabase` VALUES ('1542', '90', '370100', '370125', '0', '0', '1', '济阳县');
INSERT INTO `system_areabase` VALUES ('1543', '90', '370100', '370126', '0', '0', '1', '商河县');
INSERT INTO `system_areabase` VALUES ('1544', '90', '370100', '370181', '0', '0', '1', '章丘市');
INSERT INTO `system_areabase` VALUES ('1545', '90', '370100', '370182', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1546', '90', '370000', '370200', '0', '0', '1', '青岛市');
INSERT INTO `system_areabase` VALUES ('1547', '90', '370200', '370202', '0', '0', '1', '市南区');
INSERT INTO `system_areabase` VALUES ('1548', '90', '370200', '370203', '0', '0', '1', '市北区');
INSERT INTO `system_areabase` VALUES ('1549', '90', '370200', '370205', '0', '0', '1', '四方区');
INSERT INTO `system_areabase` VALUES ('1550', '90', '370200', '370211', '0', '0', '1', '黄岛区');
INSERT INTO `system_areabase` VALUES ('1551', '90', '370200', '370212', '0', '0', '1', '崂山区');
INSERT INTO `system_areabase` VALUES ('1552', '90', '370200', '370213', '0', '0', '1', '李沧区');
INSERT INTO `system_areabase` VALUES ('1553', '90', '370200', '370214', '0', '0', '1', '城阳区');
INSERT INTO `system_areabase` VALUES ('1554', '90', '370200', '370251', '0', '0', '1', '开发区');
INSERT INTO `system_areabase` VALUES ('1555', '90', '370200', '370281', '0', '0', '1', '胶州市');
INSERT INTO `system_areabase` VALUES ('1556', '90', '370200', '370282', '0', '0', '1', '即墨市');
INSERT INTO `system_areabase` VALUES ('1557', '90', '370200', '370283', '0', '0', '1', '平度市');
INSERT INTO `system_areabase` VALUES ('1558', '90', '370200', '370284', '0', '0', '1', '胶南市');
INSERT INTO `system_areabase` VALUES ('1559', '90', '370200', '370285', '0', '0', '1', '莱西市');
INSERT INTO `system_areabase` VALUES ('1560', '90', '370200', '370286', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1561', '90', '370000', '370300', '0', '0', '1', '淄博市');
INSERT INTO `system_areabase` VALUES ('1562', '90', '370300', '370302', '0', '0', '1', '淄川区');
INSERT INTO `system_areabase` VALUES ('1563', '90', '370300', '370303', '0', '0', '1', '张店区');
INSERT INTO `system_areabase` VALUES ('1564', '90', '370300', '370304', '0', '0', '1', '博山区');
INSERT INTO `system_areabase` VALUES ('1565', '90', '370300', '370305', '0', '0', '1', '临淄区');
INSERT INTO `system_areabase` VALUES ('1566', '90', '370300', '370306', '0', '0', '1', '周村区');
INSERT INTO `system_areabase` VALUES ('1567', '90', '370300', '370321', '0', '0', '1', '桓台县');
INSERT INTO `system_areabase` VALUES ('1568', '90', '370300', '370322', '0', '0', '1', '高青县');
INSERT INTO `system_areabase` VALUES ('1569', '90', '370300', '370323', '0', '0', '1', '沂源县');
INSERT INTO `system_areabase` VALUES ('1570', '90', '370300', '370324', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1571', '90', '370000', '370400', '0', '0', '1', '枣庄市');
INSERT INTO `system_areabase` VALUES ('1572', '90', '370400', '370402', '0', '0', '1', '市中区');
INSERT INTO `system_areabase` VALUES ('1573', '90', '370400', '370403', '0', '0', '1', '薛城区');
INSERT INTO `system_areabase` VALUES ('1574', '90', '370400', '370404', '0', '0', '1', '峄城区');
INSERT INTO `system_areabase` VALUES ('1575', '90', '370400', '370405', '0', '0', '1', '台儿庄区');
INSERT INTO `system_areabase` VALUES ('1576', '90', '370400', '370406', '0', '0', '1', '山亭区');
INSERT INTO `system_areabase` VALUES ('1577', '90', '370400', '370481', '0', '0', '1', '滕州市');
INSERT INTO `system_areabase` VALUES ('1578', '90', '370400', '370482', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1579', '90', '370000', '370500', '0', '0', '1', '东营市');
INSERT INTO `system_areabase` VALUES ('1580', '90', '370500', '370502', '0', '0', '1', '东营区');
INSERT INTO `system_areabase` VALUES ('1581', '90', '370500', '370503', '0', '0', '1', '河口区');
INSERT INTO `system_areabase` VALUES ('1582', '90', '370500', '370521', '0', '0', '1', '垦利县');
INSERT INTO `system_areabase` VALUES ('1583', '90', '370500', '370522', '0', '0', '1', '利津县');
INSERT INTO `system_areabase` VALUES ('1584', '90', '370500', '370523', '0', '0', '1', '广饶县');
INSERT INTO `system_areabase` VALUES ('1585', '90', '370500', '370589', '0', '0', '1', '西城区');
INSERT INTO `system_areabase` VALUES ('1586', '90', '370500', '370590', '0', '0', '1', '东城区');
INSERT INTO `system_areabase` VALUES ('1587', '90', '370500', '370591', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1588', '90', '370000', '370600', '0', '0', '1', '烟台市');
INSERT INTO `system_areabase` VALUES ('1589', '90', '370600', '370602', '0', '0', '1', '芝罘区');
INSERT INTO `system_areabase` VALUES ('1590', '90', '370600', '370611', '0', '0', '1', '福山区');
INSERT INTO `system_areabase` VALUES ('1591', '90', '370600', '370612', '0', '0', '1', '牟平区');
INSERT INTO `system_areabase` VALUES ('1592', '90', '370600', '370613', '0', '0', '1', '莱山区');
INSERT INTO `system_areabase` VALUES ('1593', '90', '370600', '370634', '0', '0', '1', '长岛县');
INSERT INTO `system_areabase` VALUES ('1594', '90', '370600', '370681', '0', '0', '1', '龙口市');
INSERT INTO `system_areabase` VALUES ('1595', '90', '370600', '370682', '0', '0', '1', '莱阳市');
INSERT INTO `system_areabase` VALUES ('1596', '90', '370600', '370683', '0', '0', '1', '莱州市');
INSERT INTO `system_areabase` VALUES ('1597', '90', '370600', '370684', '0', '0', '1', '蓬莱市');
INSERT INTO `system_areabase` VALUES ('1598', '90', '370600', '370685', '0', '0', '1', '招远市');
INSERT INTO `system_areabase` VALUES ('1599', '90', '370600', '370686', '0', '0', '1', '栖霞市');
INSERT INTO `system_areabase` VALUES ('1600', '90', '370600', '370687', '0', '0', '1', '海阳市');
INSERT INTO `system_areabase` VALUES ('1601', '90', '370600', '370688', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1602', '90', '370000', '370700', '0', '0', '1', '潍坊市');
INSERT INTO `system_areabase` VALUES ('1603', '90', '370700', '370702', '0', '0', '1', '潍城区');
INSERT INTO `system_areabase` VALUES ('1604', '90', '370700', '370703', '0', '0', '1', '寒亭区');
INSERT INTO `system_areabase` VALUES ('1605', '90', '370700', '370704', '0', '0', '1', '坊子区');
INSERT INTO `system_areabase` VALUES ('1606', '90', '370700', '370705', '0', '0', '1', '奎文区');
INSERT INTO `system_areabase` VALUES ('1607', '90', '370700', '370724', '0', '0', '1', '临朐县');
INSERT INTO `system_areabase` VALUES ('1608', '90', '370700', '370725', '0', '0', '1', '昌乐县');
INSERT INTO `system_areabase` VALUES ('1609', '90', '370700', '370751', '0', '0', '1', '开发区');
INSERT INTO `system_areabase` VALUES ('1610', '90', '370700', '370781', '0', '0', '1', '青州市');
INSERT INTO `system_areabase` VALUES ('1611', '90', '370700', '370782', '0', '0', '1', '诸城市');
INSERT INTO `system_areabase` VALUES ('1612', '90', '370700', '370783', '0', '0', '1', '寿光市');
INSERT INTO `system_areabase` VALUES ('1613', '90', '370700', '370784', '0', '0', '1', '安丘市');
INSERT INTO `system_areabase` VALUES ('1614', '90', '370700', '370785', '0', '0', '1', '高密市');
INSERT INTO `system_areabase` VALUES ('1615', '90', '370700', '370786', '0', '0', '1', '昌邑市');
INSERT INTO `system_areabase` VALUES ('1616', '90', '370700', '370787', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1617', '90', '370000', '370800', '0', '0', '1', '济宁市');
INSERT INTO `system_areabase` VALUES ('1618', '90', '370800', '370802', '0', '0', '1', '市中区');
INSERT INTO `system_areabase` VALUES ('1619', '90', '370800', '370811', '0', '0', '1', '任城区');
INSERT INTO `system_areabase` VALUES ('1620', '90', '370800', '370826', '0', '0', '1', '微山县');
INSERT INTO `system_areabase` VALUES ('1621', '90', '370800', '370827', '0', '0', '1', '鱼台县');
INSERT INTO `system_areabase` VALUES ('1622', '90', '370800', '370828', '0', '0', '1', '金乡县');
INSERT INTO `system_areabase` VALUES ('1623', '90', '370800', '370829', '0', '0', '1', '嘉祥县');
INSERT INTO `system_areabase` VALUES ('1624', '90', '370800', '370830', '0', '0', '1', '汶上县');
INSERT INTO `system_areabase` VALUES ('1625', '90', '370800', '370831', '0', '0', '1', '泗水县');
INSERT INTO `system_areabase` VALUES ('1626', '90', '370800', '370832', '0', '0', '1', '梁山县');
INSERT INTO `system_areabase` VALUES ('1627', '90', '370800', '370881', '0', '0', '1', '曲阜市');
INSERT INTO `system_areabase` VALUES ('1628', '90', '370800', '370882', '0', '0', '1', '兖州市');
INSERT INTO `system_areabase` VALUES ('1629', '90', '370800', '370883', '0', '0', '1', '邹城市');
INSERT INTO `system_areabase` VALUES ('1630', '90', '370800', '370884', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1631', '90', '370000', '370900', '0', '0', '1', '泰安市');
INSERT INTO `system_areabase` VALUES ('1632', '90', '370900', '370902', '0', '0', '1', '泰山区');
INSERT INTO `system_areabase` VALUES ('1633', '90', '370900', '370903', '0', '0', '1', '岱岳区');
INSERT INTO `system_areabase` VALUES ('1634', '90', '370900', '370921', '0', '0', '1', '宁阳县');
INSERT INTO `system_areabase` VALUES ('1635', '90', '370900', '370923', '0', '0', '1', '东平县');
INSERT INTO `system_areabase` VALUES ('1636', '90', '370900', '370982', '0', '0', '1', '新泰市');
INSERT INTO `system_areabase` VALUES ('1637', '90', '370900', '370983', '0', '0', '1', '肥城市');
INSERT INTO `system_areabase` VALUES ('1638', '90', '370900', '370984', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1639', '90', '370000', '371000', '0', '0', '1', '威海市');
INSERT INTO `system_areabase` VALUES ('1640', '90', '371000', '371002', '0', '0', '1', '环翠区');
INSERT INTO `system_areabase` VALUES ('1641', '90', '371000', '371081', '0', '0', '1', '文登市');
INSERT INTO `system_areabase` VALUES ('1642', '90', '371000', '371082', '0', '0', '1', '荣成市');
INSERT INTO `system_areabase` VALUES ('1643', '90', '371000', '371083', '0', '0', '1', '乳山市');
INSERT INTO `system_areabase` VALUES ('1644', '90', '371000', '371084', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1645', '90', '370000', '371100', '0', '0', '1', '日照市');
INSERT INTO `system_areabase` VALUES ('1646', '90', '371100', '371102', '0', '0', '1', '东港区');
INSERT INTO `system_areabase` VALUES ('1647', '90', '371100', '371103', '0', '0', '1', '岚山区');
INSERT INTO `system_areabase` VALUES ('1648', '90', '371100', '371121', '0', '0', '1', '五莲县');
INSERT INTO `system_areabase` VALUES ('1649', '90', '371100', '371122', '0', '0', '1', '莒县');
INSERT INTO `system_areabase` VALUES ('1650', '90', '371100', '371123', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1651', '90', '370000', '371200', '0', '0', '1', '莱芜市');
INSERT INTO `system_areabase` VALUES ('1652', '90', '371200', '371202', '0', '0', '1', '莱城区');
INSERT INTO `system_areabase` VALUES ('1653', '90', '371200', '371203', '0', '0', '1', '钢城区');
INSERT INTO `system_areabase` VALUES ('1654', '90', '371200', '371204', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1655', '90', '370000', '371300', '0', '0', '1', '临沂市');
INSERT INTO `system_areabase` VALUES ('1656', '90', '371300', '371302', '0', '0', '1', '兰山区');
INSERT INTO `system_areabase` VALUES ('1657', '90', '371300', '371311', '0', '0', '1', '罗庄区');
INSERT INTO `system_areabase` VALUES ('1658', '90', '371300', '371312', '0', '0', '1', '河东区');
INSERT INTO `system_areabase` VALUES ('1659', '90', '371300', '371321', '0', '0', '1', '沂南县');
INSERT INTO `system_areabase` VALUES ('1660', '90', '371300', '371322', '0', '0', '1', '郯城县');
INSERT INTO `system_areabase` VALUES ('1661', '90', '371300', '371323', '0', '0', '1', '沂水县');
INSERT INTO `system_areabase` VALUES ('1662', '90', '371300', '371324', '0', '0', '1', '苍山县');
INSERT INTO `system_areabase` VALUES ('1663', '90', '371300', '371325', '0', '0', '1', '费县');
INSERT INTO `system_areabase` VALUES ('1664', '90', '371300', '371326', '0', '0', '1', '平邑县');
INSERT INTO `system_areabase` VALUES ('1665', '90', '371300', '371327', '0', '0', '1', '莒南县');
INSERT INTO `system_areabase` VALUES ('1666', '90', '371300', '371328', '0', '0', '1', '蒙阴县');
INSERT INTO `system_areabase` VALUES ('1667', '90', '371300', '371329', '0', '0', '1', '临沭县');
INSERT INTO `system_areabase` VALUES ('1668', '90', '371300', '371330', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1669', '90', '370000', '371400', '0', '0', '1', '德州市');
INSERT INTO `system_areabase` VALUES ('1670', '90', '371400', '371402', '0', '0', '1', '德城区');
INSERT INTO `system_areabase` VALUES ('1671', '90', '371400', '371421', '0', '0', '1', '陵县');
INSERT INTO `system_areabase` VALUES ('1672', '90', '371400', '371422', '0', '0', '1', '宁津县');
INSERT INTO `system_areabase` VALUES ('1673', '90', '371400', '371423', '0', '0', '1', '庆云县');
INSERT INTO `system_areabase` VALUES ('1674', '90', '371400', '371424', '0', '0', '1', '临邑县');
INSERT INTO `system_areabase` VALUES ('1675', '90', '371400', '371425', '0', '0', '1', '齐河县');
INSERT INTO `system_areabase` VALUES ('1676', '90', '371400', '371426', '0', '0', '1', '平原县');
INSERT INTO `system_areabase` VALUES ('1677', '90', '371400', '371427', '0', '0', '1', '夏津县');
INSERT INTO `system_areabase` VALUES ('1678', '90', '371400', '371428', '0', '0', '1', '武城县');
INSERT INTO `system_areabase` VALUES ('1679', '90', '371400', '371451', '0', '0', '1', '开发区');
INSERT INTO `system_areabase` VALUES ('1680', '90', '371400', '371481', '0', '0', '1', '乐陵市');
INSERT INTO `system_areabase` VALUES ('1681', '90', '371400', '371482', '0', '0', '1', '禹城市');
INSERT INTO `system_areabase` VALUES ('1682', '90', '371400', '371483', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1683', '90', '370000', '371500', '0', '0', '1', '聊城市');
INSERT INTO `system_areabase` VALUES ('1684', '90', '371500', '371502', '0', '0', '1', '东昌府区');
INSERT INTO `system_areabase` VALUES ('1685', '90', '371500', '371521', '0', '0', '1', '阳谷县');
INSERT INTO `system_areabase` VALUES ('1686', '90', '371500', '371522', '0', '0', '1', '莘县');
INSERT INTO `system_areabase` VALUES ('1687', '90', '371500', '371523', '0', '0', '1', '茌平县');
INSERT INTO `system_areabase` VALUES ('1688', '90', '371500', '371524', '0', '0', '1', '东阿县');
INSERT INTO `system_areabase` VALUES ('1689', '90', '371500', '371525', '0', '0', '1', '冠县');
INSERT INTO `system_areabase` VALUES ('1690', '90', '371500', '371526', '0', '0', '1', '高唐县');
INSERT INTO `system_areabase` VALUES ('1691', '90', '371500', '371581', '0', '0', '1', '临清市');
INSERT INTO `system_areabase` VALUES ('1692', '90', '371500', '371582', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1693', '90', '370000', '371600', '0', '0', '1', '滨州市');
INSERT INTO `system_areabase` VALUES ('1694', '90', '371600', '371602', '0', '0', '1', '滨城区');
INSERT INTO `system_areabase` VALUES ('1695', '90', '371600', '371621', '0', '0', '1', '惠民县');
INSERT INTO `system_areabase` VALUES ('1696', '90', '371600', '371622', '0', '0', '1', '阳信县');
INSERT INTO `system_areabase` VALUES ('1697', '90', '371600', '371623', '0', '0', '1', '无棣县');
INSERT INTO `system_areabase` VALUES ('1698', '90', '371600', '371624', '0', '0', '1', '沾化县');
INSERT INTO `system_areabase` VALUES ('1699', '90', '371600', '371625', '0', '0', '1', '博兴县');
INSERT INTO `system_areabase` VALUES ('1700', '90', '371600', '371626', '0', '0', '1', '邹平县');
INSERT INTO `system_areabase` VALUES ('1701', '90', '371600', '371627', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1702', '90', '370000', '371700', '0', '0', '1', '菏泽市');
INSERT INTO `system_areabase` VALUES ('1703', '90', '371700', '371702', '0', '0', '1', '牡丹区');
INSERT INTO `system_areabase` VALUES ('1704', '90', '371700', '371721', '0', '0', '1', '曹县');
INSERT INTO `system_areabase` VALUES ('1705', '90', '371700', '371722', '0', '0', '1', '单县');
INSERT INTO `system_areabase` VALUES ('1706', '90', '371700', '371723', '0', '0', '1', '成武县');
INSERT INTO `system_areabase` VALUES ('1707', '90', '371700', '371724', '0', '0', '1', '巨野县');
INSERT INTO `system_areabase` VALUES ('1708', '90', '371700', '371725', '0', '0', '1', '郓城县');
INSERT INTO `system_areabase` VALUES ('1709', '90', '371700', '371726', '0', '0', '1', '鄄城县');
INSERT INTO `system_areabase` VALUES ('1710', '90', '371700', '371727', '0', '0', '1', '定陶县');
INSERT INTO `system_areabase` VALUES ('1711', '90', '371700', '371728', '0', '0', '1', '东明县');
INSERT INTO `system_areabase` VALUES ('1712', '90', '371700', '371729', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1713', '90', '0', '410000', '0', '0', '1', '河南省');
INSERT INTO `system_areabase` VALUES ('1714', '90', '410000', '410100', '0', '0', '1', '郑州市');
INSERT INTO `system_areabase` VALUES ('1715', '90', '410100', '410102', '0', '0', '1', '中原区');
INSERT INTO `system_areabase` VALUES ('1716', '90', '410100', '410103', '0', '0', '1', '二七区');
INSERT INTO `system_areabase` VALUES ('1717', '90', '410100', '410104', '0', '0', '1', '管城回族区');
INSERT INTO `system_areabase` VALUES ('1718', '90', '410100', '410105', '0', '0', '1', '金水区');
INSERT INTO `system_areabase` VALUES ('1719', '90', '410100', '410106', '0', '0', '1', '上街区');
INSERT INTO `system_areabase` VALUES ('1720', '90', '410100', '410108', '0', '0', '1', '惠济区');
INSERT INTO `system_areabase` VALUES ('1721', '90', '410100', '410122', '0', '0', '1', '中牟县');
INSERT INTO `system_areabase` VALUES ('1722', '90', '410100', '410181', '0', '0', '1', '巩义市');
INSERT INTO `system_areabase` VALUES ('1723', '90', '410100', '410182', '0', '0', '1', '荥阳市');
INSERT INTO `system_areabase` VALUES ('1724', '90', '410100', '410183', '0', '0', '1', '新密市');
INSERT INTO `system_areabase` VALUES ('1725', '90', '410100', '410184', '0', '0', '1', '新郑市');
INSERT INTO `system_areabase` VALUES ('1726', '90', '410100', '410185', '0', '0', '1', '登封市');
INSERT INTO `system_areabase` VALUES ('1727', '90', '410100', '410186', '0', '0', '1', '郑东新区');
INSERT INTO `system_areabase` VALUES ('1728', '90', '410100', '410187', '0', '0', '1', '高新区');
INSERT INTO `system_areabase` VALUES ('1729', '90', '410100', '410188', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1730', '90', '410000', '410200', '0', '0', '1', '开封市');
INSERT INTO `system_areabase` VALUES ('1731', '90', '410200', '410202', '0', '0', '1', '龙亭区');
INSERT INTO `system_areabase` VALUES ('1732', '90', '410200', '410203', '0', '0', '1', '顺河回族区');
INSERT INTO `system_areabase` VALUES ('1733', '90', '410200', '410204', '0', '0', '1', '鼓楼区');
INSERT INTO `system_areabase` VALUES ('1734', '90', '410200', '410205', '0', '0', '1', '禹王台区');
INSERT INTO `system_areabase` VALUES ('1735', '90', '410200', '410211', '0', '0', '1', '金明区');
INSERT INTO `system_areabase` VALUES ('1736', '90', '410200', '410221', '0', '0', '1', '杞县');
INSERT INTO `system_areabase` VALUES ('1737', '90', '410200', '410222', '0', '0', '1', '通许县');
INSERT INTO `system_areabase` VALUES ('1738', '90', '410200', '410223', '0', '0', '1', '尉氏县');
INSERT INTO `system_areabase` VALUES ('1739', '90', '410200', '410224', '0', '0', '1', '开封县');
INSERT INTO `system_areabase` VALUES ('1740', '90', '410200', '410225', '0', '0', '1', '兰考县');
INSERT INTO `system_areabase` VALUES ('1741', '90', '410200', '410226', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1742', '90', '410000', '410300', '0', '0', '1', '洛阳市');
INSERT INTO `system_areabase` VALUES ('1743', '90', '410300', '410302', '0', '0', '1', '老城区');
INSERT INTO `system_areabase` VALUES ('1744', '90', '410300', '410303', '0', '0', '1', '西工区');
INSERT INTO `system_areabase` VALUES ('1745', '90', '410300', '410304', '0', '0', '1', '廛河回族区');
INSERT INTO `system_areabase` VALUES ('1746', '90', '410300', '410305', '0', '0', '1', '涧西区');
INSERT INTO `system_areabase` VALUES ('1747', '90', '410300', '410306', '0', '0', '1', '吉利区');
INSERT INTO `system_areabase` VALUES ('1748', '90', '410300', '410307', '0', '0', '1', '洛龙区');
INSERT INTO `system_areabase` VALUES ('1749', '90', '410300', '410322', '0', '0', '1', '孟津县');
INSERT INTO `system_areabase` VALUES ('1750', '90', '410300', '410323', '0', '0', '1', '新安县');
INSERT INTO `system_areabase` VALUES ('1751', '90', '410300', '410324', '0', '0', '1', '栾川县');
INSERT INTO `system_areabase` VALUES ('1752', '90', '410300', '410325', '0', '0', '1', '嵩县');
INSERT INTO `system_areabase` VALUES ('1753', '90', '410300', '410326', '0', '0', '1', '汝阳县');
INSERT INTO `system_areabase` VALUES ('1754', '90', '410300', '410327', '0', '0', '1', '宜阳县');
INSERT INTO `system_areabase` VALUES ('1755', '90', '410300', '410328', '0', '0', '1', '洛宁县');
INSERT INTO `system_areabase` VALUES ('1756', '90', '410300', '410329', '0', '0', '1', '伊川县');
INSERT INTO `system_areabase` VALUES ('1757', '90', '410300', '410381', '0', '0', '1', '偃师市');
INSERT INTO `system_areabase` VALUES ('1758', '90', '410300', '471004', '0', '0', '1', '高新区');
INSERT INTO `system_areabase` VALUES ('1759', '90', '410300', '471005', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1760', '90', '410000', '410400', '0', '0', '1', '平顶山市');
INSERT INTO `system_areabase` VALUES ('1761', '90', '410400', '410402', '0', '0', '1', '新华区');
INSERT INTO `system_areabase` VALUES ('1762', '90', '410400', '410403', '0', '0', '1', '卫东区');
INSERT INTO `system_areabase` VALUES ('1763', '90', '410400', '410404', '0', '0', '1', '石龙区');
INSERT INTO `system_areabase` VALUES ('1764', '90', '410400', '410411', '0', '0', '1', '湛河区');
INSERT INTO `system_areabase` VALUES ('1765', '90', '410400', '410421', '0', '0', '1', '宝丰县');
INSERT INTO `system_areabase` VALUES ('1766', '90', '410400', '410422', '0', '0', '1', '叶县');
INSERT INTO `system_areabase` VALUES ('1767', '90', '410400', '410423', '0', '0', '1', '鲁山县');
INSERT INTO `system_areabase` VALUES ('1768', '90', '410400', '410425', '0', '0', '1', '郏县');
INSERT INTO `system_areabase` VALUES ('1769', '90', '410400', '410481', '0', '0', '1', '舞钢市');
INSERT INTO `system_areabase` VALUES ('1770', '90', '410400', '410482', '0', '0', '1', '汝州市');
INSERT INTO `system_areabase` VALUES ('1771', '90', '410400', '410483', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1772', '90', '410000', '410500', '0', '0', '1', '安阳市');
INSERT INTO `system_areabase` VALUES ('1773', '90', '410500', '410502', '0', '0', '1', '文峰区');
INSERT INTO `system_areabase` VALUES ('1774', '90', '410500', '410503', '0', '0', '1', '北关区');
INSERT INTO `system_areabase` VALUES ('1775', '90', '410500', '410505', '0', '0', '1', '殷都区');
INSERT INTO `system_areabase` VALUES ('1776', '90', '410500', '410506', '0', '0', '1', '龙安区');
INSERT INTO `system_areabase` VALUES ('1777', '90', '410500', '410522', '0', '0', '1', '安阳县');
INSERT INTO `system_areabase` VALUES ('1778', '90', '410500', '410523', '0', '0', '1', '汤阴县');
INSERT INTO `system_areabase` VALUES ('1779', '90', '410500', '410526', '0', '0', '1', '滑县');
INSERT INTO `system_areabase` VALUES ('1780', '90', '410500', '410527', '0', '0', '1', '内黄县');
INSERT INTO `system_areabase` VALUES ('1781', '90', '410500', '410581', '0', '0', '1', '林州市');
INSERT INTO `system_areabase` VALUES ('1782', '90', '410500', '410582', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1783', '90', '410000', '410600', '0', '0', '1', '鹤壁市');
INSERT INTO `system_areabase` VALUES ('1784', '90', '410600', '410602', '0', '0', '1', '鹤山区');
INSERT INTO `system_areabase` VALUES ('1785', '90', '410600', '410603', '0', '0', '1', '山城区');
INSERT INTO `system_areabase` VALUES ('1786', '90', '410600', '410611', '0', '0', '1', '淇滨区');
INSERT INTO `system_areabase` VALUES ('1787', '90', '410600', '410621', '0', '0', '1', '浚县');
INSERT INTO `system_areabase` VALUES ('1788', '90', '410600', '410622', '0', '0', '1', '淇县');
INSERT INTO `system_areabase` VALUES ('1789', '90', '410600', '410623', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1790', '90', '410000', '410700', '0', '0', '1', '新乡市');
INSERT INTO `system_areabase` VALUES ('1791', '90', '410700', '410702', '0', '0', '1', '红旗区');
INSERT INTO `system_areabase` VALUES ('1792', '90', '410700', '410703', '0', '0', '1', '卫滨区');
INSERT INTO `system_areabase` VALUES ('1793', '90', '410700', '410704', '0', '0', '1', '凤泉区');
INSERT INTO `system_areabase` VALUES ('1794', '90', '410700', '410711', '0', '0', '1', '牧野区');
INSERT INTO `system_areabase` VALUES ('1795', '90', '410700', '410721', '0', '0', '1', '新乡县');
INSERT INTO `system_areabase` VALUES ('1796', '90', '410700', '410724', '0', '0', '1', '获嘉县');
INSERT INTO `system_areabase` VALUES ('1797', '90', '410700', '410725', '0', '0', '1', '原阳县');
INSERT INTO `system_areabase` VALUES ('1798', '90', '410700', '410726', '0', '0', '1', '延津县');
INSERT INTO `system_areabase` VALUES ('1799', '90', '410700', '410727', '0', '0', '1', '封丘县');
INSERT INTO `system_areabase` VALUES ('1800', '90', '410700', '410728', '0', '0', '1', '长垣县');
INSERT INTO `system_areabase` VALUES ('1801', '90', '410700', '410781', '0', '0', '1', '卫辉市');
INSERT INTO `system_areabase` VALUES ('1802', '90', '410700', '410782', '0', '0', '1', '辉县');
INSERT INTO `system_areabase` VALUES ('1803', '90', '410700', '410783', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1804', '90', '410000', '410800', '0', '0', '1', '焦作市');
INSERT INTO `system_areabase` VALUES ('1805', '90', '410800', '410802', '0', '0', '1', '解放区');
INSERT INTO `system_areabase` VALUES ('1806', '90', '410800', '410803', '0', '0', '1', '中站区');
INSERT INTO `system_areabase` VALUES ('1807', '90', '410800', '410804', '0', '0', '1', '马村区');
INSERT INTO `system_areabase` VALUES ('1808', '90', '410800', '410811', '0', '0', '1', '山阳区');
INSERT INTO `system_areabase` VALUES ('1809', '90', '410800', '410821', '0', '0', '1', '修武县');
INSERT INTO `system_areabase` VALUES ('1810', '90', '410800', '410822', '0', '0', '1', '博爱县');
INSERT INTO `system_areabase` VALUES ('1811', '90', '410800', '410823', '0', '0', '1', '武陟县');
INSERT INTO `system_areabase` VALUES ('1812', '90', '410800', '410825', '0', '0', '1', '温县');
INSERT INTO `system_areabase` VALUES ('1813', '90', '410800', '410882', '0', '0', '1', '沁阳市');
INSERT INTO `system_areabase` VALUES ('1814', '90', '410800', '410883', '0', '0', '1', '孟州市');
INSERT INTO `system_areabase` VALUES ('1815', '90', '410800', '410884', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1816', '90', '410000', '410881', '0', '0', '1', '济源市');
INSERT INTO `system_areabase` VALUES ('1817', '90', '410000', '410900', '0', '0', '1', '濮阳市');
INSERT INTO `system_areabase` VALUES ('1818', '90', '410900', '410902', '0', '0', '1', '华龙区');
INSERT INTO `system_areabase` VALUES ('1819', '90', '410900', '410922', '0', '0', '1', '清丰县');
INSERT INTO `system_areabase` VALUES ('1820', '90', '410900', '410923', '0', '0', '1', '南乐县');
INSERT INTO `system_areabase` VALUES ('1821', '90', '410900', '410926', '0', '0', '1', '范县');
INSERT INTO `system_areabase` VALUES ('1822', '90', '410900', '410927', '0', '0', '1', '台前县');
INSERT INTO `system_areabase` VALUES ('1823', '90', '410900', '410928', '0', '0', '1', '濮阳县');
INSERT INTO `system_areabase` VALUES ('1824', '90', '410900', '410929', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1825', '90', '410000', '411000', '0', '0', '1', '许昌市');
INSERT INTO `system_areabase` VALUES ('1826', '90', '411000', '411002', '0', '0', '1', '魏都区');
INSERT INTO `system_areabase` VALUES ('1827', '90', '411000', '411023', '0', '0', '1', '许昌县');
INSERT INTO `system_areabase` VALUES ('1828', '90', '411000', '411024', '0', '0', '1', '鄢陵县');
INSERT INTO `system_areabase` VALUES ('1829', '90', '411000', '411025', '0', '0', '1', '襄城县');
INSERT INTO `system_areabase` VALUES ('1830', '90', '411000', '411081', '0', '0', '1', '禹州市');
INSERT INTO `system_areabase` VALUES ('1831', '90', '411000', '411082', '0', '0', '1', '长葛市');
INSERT INTO `system_areabase` VALUES ('1832', '90', '411000', '411083', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1833', '90', '410000', '411100', '0', '0', '1', '漯河市');
INSERT INTO `system_areabase` VALUES ('1834', '90', '411100', '411102', '0', '0', '1', '源汇区');
INSERT INTO `system_areabase` VALUES ('1835', '90', '411100', '411103', '0', '0', '1', '郾城区');
INSERT INTO `system_areabase` VALUES ('1836', '90', '411100', '411104', '0', '0', '1', '召陵区');
INSERT INTO `system_areabase` VALUES ('1837', '90', '411100', '411121', '0', '0', '1', '舞阳县');
INSERT INTO `system_areabase` VALUES ('1838', '90', '411100', '411122', '0', '0', '1', '临颍县');
INSERT INTO `system_areabase` VALUES ('1839', '90', '411100', '411123', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1840', '90', '410000', '411200', '0', '0', '1', '三门峡市');
INSERT INTO `system_areabase` VALUES ('1841', '90', '411200', '411202', '0', '0', '1', '湖滨区');
INSERT INTO `system_areabase` VALUES ('1842', '90', '411200', '411221', '0', '0', '1', '渑池县');
INSERT INTO `system_areabase` VALUES ('1843', '90', '411200', '411222', '0', '0', '1', '陕县');
INSERT INTO `system_areabase` VALUES ('1844', '90', '411200', '411224', '0', '0', '1', '卢氏县');
INSERT INTO `system_areabase` VALUES ('1845', '90', '411200', '411281', '0', '0', '1', '义马市');
INSERT INTO `system_areabase` VALUES ('1846', '90', '411200', '411282', '0', '0', '1', '灵宝市');
INSERT INTO `system_areabase` VALUES ('1847', '90', '411200', '411283', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1848', '90', '410000', '411300', '0', '0', '1', '南阳市');
INSERT INTO `system_areabase` VALUES ('1849', '90', '411300', '411302', '0', '0', '1', '宛城区');
INSERT INTO `system_areabase` VALUES ('1850', '90', '411300', '411303', '0', '0', '1', '卧龙区');
INSERT INTO `system_areabase` VALUES ('1851', '90', '411300', '411321', '0', '0', '1', '南召县');
INSERT INTO `system_areabase` VALUES ('1852', '90', '411300', '411322', '0', '0', '1', '方城县');
INSERT INTO `system_areabase` VALUES ('1853', '90', '411300', '411323', '0', '0', '1', '西峡县');
INSERT INTO `system_areabase` VALUES ('1854', '90', '411300', '411324', '0', '0', '1', '镇平县');
INSERT INTO `system_areabase` VALUES ('1855', '90', '411300', '411325', '0', '0', '1', '内乡县');
INSERT INTO `system_areabase` VALUES ('1856', '90', '411300', '411326', '0', '0', '1', '淅川县');
INSERT INTO `system_areabase` VALUES ('1857', '90', '411300', '411327', '0', '0', '1', '社旗县');
INSERT INTO `system_areabase` VALUES ('1858', '90', '411300', '411328', '0', '0', '1', '唐河县');
INSERT INTO `system_areabase` VALUES ('1859', '90', '411300', '411329', '0', '0', '1', '新野县');
INSERT INTO `system_areabase` VALUES ('1860', '90', '411300', '411330', '0', '0', '1', '桐柏县');
INSERT INTO `system_areabase` VALUES ('1861', '90', '411300', '411381', '0', '0', '1', '邓州市');
INSERT INTO `system_areabase` VALUES ('1862', '90', '411300', '411382', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1863', '90', '410000', '411400', '0', '0', '1', '商丘市');
INSERT INTO `system_areabase` VALUES ('1864', '90', '411400', '411402', '0', '0', '1', '梁园区');
INSERT INTO `system_areabase` VALUES ('1865', '90', '411400', '411403', '0', '0', '1', '睢阳区');
INSERT INTO `system_areabase` VALUES ('1866', '90', '411400', '411421', '0', '0', '1', '民权县');
INSERT INTO `system_areabase` VALUES ('1867', '90', '411400', '411422', '0', '0', '1', '睢县');
INSERT INTO `system_areabase` VALUES ('1868', '90', '411400', '411423', '0', '0', '1', '宁陵县');
INSERT INTO `system_areabase` VALUES ('1869', '90', '411400', '411424', '0', '0', '1', '柘城县');
INSERT INTO `system_areabase` VALUES ('1870', '90', '411400', '411425', '0', '0', '1', '虞城县');
INSERT INTO `system_areabase` VALUES ('1871', '90', '411400', '411426', '0', '0', '1', '夏邑县');
INSERT INTO `system_areabase` VALUES ('1872', '90', '411400', '411481', '0', '0', '1', '永城市');
INSERT INTO `system_areabase` VALUES ('1873', '90', '411400', '411482', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1874', '90', '410000', '411500', '0', '0', '1', '信阳市');
INSERT INTO `system_areabase` VALUES ('1875', '90', '411500', '411502', '0', '0', '1', '浉河区');
INSERT INTO `system_areabase` VALUES ('1876', '90', '411500', '411503', '0', '0', '1', '平桥区');
INSERT INTO `system_areabase` VALUES ('1877', '90', '411500', '411521', '0', '0', '1', '罗山县');
INSERT INTO `system_areabase` VALUES ('1878', '90', '411500', '411522', '0', '0', '1', '光山县');
INSERT INTO `system_areabase` VALUES ('1879', '90', '411500', '411523', '0', '0', '1', '新县');
INSERT INTO `system_areabase` VALUES ('1880', '90', '411500', '411524', '0', '0', '1', '商城县');
INSERT INTO `system_areabase` VALUES ('1881', '90', '411500', '411525', '0', '0', '1', '固始县');
INSERT INTO `system_areabase` VALUES ('1882', '90', '411500', '411526', '0', '0', '1', '潢川县');
INSERT INTO `system_areabase` VALUES ('1883', '90', '411500', '411527', '0', '0', '1', '淮滨县');
INSERT INTO `system_areabase` VALUES ('1884', '90', '411500', '411528', '0', '0', '1', '息县');
INSERT INTO `system_areabase` VALUES ('1885', '90', '411500', '411529', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1886', '90', '410000', '411600', '0', '0', '1', '周口市');
INSERT INTO `system_areabase` VALUES ('1887', '90', '411600', '411602', '0', '0', '1', '川汇区');
INSERT INTO `system_areabase` VALUES ('1888', '90', '411600', '411621', '0', '0', '1', '扶沟县');
INSERT INTO `system_areabase` VALUES ('1889', '90', '411600', '411622', '0', '0', '1', '西华县');
INSERT INTO `system_areabase` VALUES ('1890', '90', '411600', '411623', '0', '0', '1', '商水县');
INSERT INTO `system_areabase` VALUES ('1891', '90', '411600', '411624', '0', '0', '1', '沈丘县');
INSERT INTO `system_areabase` VALUES ('1892', '90', '411600', '411625', '0', '0', '1', '郸城县');
INSERT INTO `system_areabase` VALUES ('1893', '90', '411600', '411626', '0', '0', '1', '淮阳县');
INSERT INTO `system_areabase` VALUES ('1894', '90', '411600', '411627', '0', '0', '1', '太康县');
INSERT INTO `system_areabase` VALUES ('1895', '90', '411600', '411628', '0', '0', '1', '鹿邑县');
INSERT INTO `system_areabase` VALUES ('1896', '90', '411600', '411681', '0', '0', '1', '项城市');
INSERT INTO `system_areabase` VALUES ('1897', '90', '411600', '411682', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1898', '90', '410000', '411700', '0', '0', '1', '驻马店市');
INSERT INTO `system_areabase` VALUES ('1899', '90', '411700', '411702', '0', '0', '1', '驿城区');
INSERT INTO `system_areabase` VALUES ('1900', '90', '411700', '411721', '0', '0', '1', '西平县');
INSERT INTO `system_areabase` VALUES ('1901', '90', '411700', '411722', '0', '0', '1', '上蔡县');
INSERT INTO `system_areabase` VALUES ('1902', '90', '411700', '411723', '0', '0', '1', '平舆县');
INSERT INTO `system_areabase` VALUES ('1903', '90', '411700', '411724', '0', '0', '1', '正阳县');
INSERT INTO `system_areabase` VALUES ('1904', '90', '411700', '411725', '0', '0', '1', '确山县');
INSERT INTO `system_areabase` VALUES ('1905', '90', '411700', '411726', '0', '0', '1', '泌阳县');
INSERT INTO `system_areabase` VALUES ('1906', '90', '411700', '411727', '0', '0', '1', '汝南县');
INSERT INTO `system_areabase` VALUES ('1907', '90', '411700', '411728', '0', '0', '1', '遂平县');
INSERT INTO `system_areabase` VALUES ('1908', '90', '411700', '411729', '0', '0', '1', '新蔡县');
INSERT INTO `system_areabase` VALUES ('1909', '90', '411700', '411730', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1910', '90', '0', '420000', '0', '0', '1', '湖北省');
INSERT INTO `system_areabase` VALUES ('1911', '90', '420000', '420100', '0', '0', '1', '武汉市');
INSERT INTO `system_areabase` VALUES ('1912', '90', '420100', '420102', '0', '0', '1', '江岸区');
INSERT INTO `system_areabase` VALUES ('1913', '90', '420100', '420103', '0', '0', '1', '江汉区');
INSERT INTO `system_areabase` VALUES ('1914', '90', '420100', '420104', '0', '0', '1', '硚口区');
INSERT INTO `system_areabase` VALUES ('1915', '90', '420100', '420105', '0', '0', '1', '汉阳区');
INSERT INTO `system_areabase` VALUES ('1916', '90', '420100', '420106', '0', '0', '1', '武昌区');
INSERT INTO `system_areabase` VALUES ('1917', '90', '420100', '420107', '0', '0', '1', '青山区');
INSERT INTO `system_areabase` VALUES ('1918', '90', '420100', '420111', '0', '0', '1', '洪山区');
INSERT INTO `system_areabase` VALUES ('1919', '90', '420100', '420112', '0', '0', '1', '东西湖区');
INSERT INTO `system_areabase` VALUES ('1920', '90', '420100', '420113', '0', '0', '1', '汉南区');
INSERT INTO `system_areabase` VALUES ('1921', '90', '420100', '420114', '0', '0', '1', '蔡甸区');
INSERT INTO `system_areabase` VALUES ('1922', '90', '420100', '420115', '0', '0', '1', '江夏区');
INSERT INTO `system_areabase` VALUES ('1923', '90', '420100', '420116', '0', '0', '1', '黄陂区');
INSERT INTO `system_areabase` VALUES ('1924', '90', '420100', '420117', '0', '0', '1', '新洲区');
INSERT INTO `system_areabase` VALUES ('1925', '90', '420100', '420118', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1926', '90', '420000', '420200', '0', '0', '1', '黄石市');
INSERT INTO `system_areabase` VALUES ('1927', '90', '420200', '420202', '0', '0', '1', '黄石港区');
INSERT INTO `system_areabase` VALUES ('1928', '90', '420200', '420203', '0', '0', '1', '西塞山区');
INSERT INTO `system_areabase` VALUES ('1929', '90', '420200', '420204', '0', '0', '1', '下陆区');
INSERT INTO `system_areabase` VALUES ('1930', '90', '420200', '420205', '0', '0', '1', '铁山区');
INSERT INTO `system_areabase` VALUES ('1931', '90', '420200', '420222', '0', '0', '1', '阳新县');
INSERT INTO `system_areabase` VALUES ('1932', '90', '420200', '420281', '0', '0', '1', '大冶市');
INSERT INTO `system_areabase` VALUES ('1933', '90', '420200', '420282', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1934', '90', '420000', '420300', '0', '0', '1', '十堰市');
INSERT INTO `system_areabase` VALUES ('1935', '90', '420300', '420302', '0', '0', '1', '茅箭区');
INSERT INTO `system_areabase` VALUES ('1936', '90', '420300', '420303', '0', '0', '1', '张湾区');
INSERT INTO `system_areabase` VALUES ('1937', '90', '420300', '420321', '0', '0', '1', '郧县');
INSERT INTO `system_areabase` VALUES ('1938', '90', '420300', '420322', '0', '0', '1', '郧西县');
INSERT INTO `system_areabase` VALUES ('1939', '90', '420300', '420323', '0', '0', '1', '竹山县');
INSERT INTO `system_areabase` VALUES ('1940', '90', '420300', '420324', '0', '0', '1', '竹溪县');
INSERT INTO `system_areabase` VALUES ('1941', '90', '420300', '420325', '0', '0', '1', '房县');
INSERT INTO `system_areabase` VALUES ('1942', '90', '420300', '420381', '0', '0', '1', '丹江口市');
INSERT INTO `system_areabase` VALUES ('1943', '90', '420300', '420382', '0', '0', '1', '城区');
INSERT INTO `system_areabase` VALUES ('1944', '90', '420300', '420383', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1945', '90', '420000', '420500', '0', '0', '1', '宜昌市');
INSERT INTO `system_areabase` VALUES ('1946', '90', '420500', '420502', '0', '0', '1', '西陵区');
INSERT INTO `system_areabase` VALUES ('1947', '90', '420500', '420503', '0', '0', '1', '伍家岗区');
INSERT INTO `system_areabase` VALUES ('1948', '90', '420500', '420504', '0', '0', '1', '点军区');
INSERT INTO `system_areabase` VALUES ('1949', '90', '420500', '420505', '0', '0', '1', '猇亭区');
INSERT INTO `system_areabase` VALUES ('1950', '90', '420500', '420506', '0', '0', '1', '夷陵区');
INSERT INTO `system_areabase` VALUES ('1951', '90', '420500', '420525', '0', '0', '1', '远安县');
INSERT INTO `system_areabase` VALUES ('1952', '90', '420500', '420526', '0', '0', '1', '兴山县');
INSERT INTO `system_areabase` VALUES ('1953', '90', '420500', '420527', '0', '0', '1', '秭归县');
INSERT INTO `system_areabase` VALUES ('1954', '90', '420500', '420528', '0', '0', '1', '长阳土家族自治县');
INSERT INTO `system_areabase` VALUES ('1955', '90', '420500', '420529', '0', '0', '1', '五峰土家族自治县');
INSERT INTO `system_areabase` VALUES ('1956', '90', '420500', '420551', '0', '0', '1', '葛洲坝区');
INSERT INTO `system_areabase` VALUES ('1957', '90', '420500', '420552', '0', '0', '1', '开发区');
INSERT INTO `system_areabase` VALUES ('1958', '90', '420500', '420581', '0', '0', '1', '宜都市');
INSERT INTO `system_areabase` VALUES ('1959', '90', '420500', '420582', '0', '0', '1', '当阳市');
INSERT INTO `system_areabase` VALUES ('1960', '90', '420500', '420583', '0', '0', '1', '枝江市');
INSERT INTO `system_areabase` VALUES ('1961', '90', '420500', '420584', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1962', '90', '420000', '420600', '0', '0', '1', '襄樊市');
INSERT INTO `system_areabase` VALUES ('1963', '90', '420600', '420602', '0', '0', '1', '襄城区');
INSERT INTO `system_areabase` VALUES ('1964', '90', '420600', '420606', '0', '0', '1', '樊城区');
INSERT INTO `system_areabase` VALUES ('1965', '90', '420600', '420607', '0', '0', '1', '襄阳区');
INSERT INTO `system_areabase` VALUES ('1966', '90', '420600', '420624', '0', '0', '1', '南漳县');
INSERT INTO `system_areabase` VALUES ('1967', '90', '420600', '420625', '0', '0', '1', '谷城县');
INSERT INTO `system_areabase` VALUES ('1968', '90', '420600', '420626', '0', '0', '1', '保康县');
INSERT INTO `system_areabase` VALUES ('1969', '90', '420600', '420682', '0', '0', '1', '老河口市');
INSERT INTO `system_areabase` VALUES ('1970', '90', '420600', '420683', '0', '0', '1', '枣阳市');
INSERT INTO `system_areabase` VALUES ('1971', '90', '420600', '420684', '0', '0', '1', '宜城市');
INSERT INTO `system_areabase` VALUES ('1972', '90', '420600', '420685', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1973', '90', '420000', '420700', '0', '0', '1', '鄂州市');
INSERT INTO `system_areabase` VALUES ('1974', '90', '420700', '420702', '0', '0', '1', '梁子湖区');
INSERT INTO `system_areabase` VALUES ('1975', '90', '420700', '420703', '0', '0', '1', '华容区');
INSERT INTO `system_areabase` VALUES ('1976', '90', '420700', '420704', '0', '0', '1', '鄂城区');
INSERT INTO `system_areabase` VALUES ('1977', '90', '420700', '420705', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1978', '90', '420000', '420800', '0', '0', '1', '荆门市');
INSERT INTO `system_areabase` VALUES ('1979', '90', '420800', '420802', '0', '0', '1', '东宝区');
INSERT INTO `system_areabase` VALUES ('1980', '90', '420800', '420804', '0', '0', '1', '掇刀区');
INSERT INTO `system_areabase` VALUES ('1981', '90', '420800', '420821', '0', '0', '1', '京山县');
INSERT INTO `system_areabase` VALUES ('1982', '90', '420800', '420822', '0', '0', '1', '沙洋县');
INSERT INTO `system_areabase` VALUES ('1983', '90', '420800', '420881', '0', '0', '1', '钟祥市');
INSERT INTO `system_areabase` VALUES ('1984', '90', '420800', '420882', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1985', '90', '420000', '420900', '0', '0', '1', '孝感市');
INSERT INTO `system_areabase` VALUES ('1986', '90', '420900', '420902', '0', '0', '1', '孝南区');
INSERT INTO `system_areabase` VALUES ('1987', '90', '420900', '420921', '0', '0', '1', '孝昌县');
INSERT INTO `system_areabase` VALUES ('1988', '90', '420900', '420922', '0', '0', '1', '大悟县');
INSERT INTO `system_areabase` VALUES ('1989', '90', '420900', '420923', '0', '0', '1', '云梦县');
INSERT INTO `system_areabase` VALUES ('1990', '90', '420900', '420981', '0', '0', '1', '应城市');
INSERT INTO `system_areabase` VALUES ('1991', '90', '420900', '420982', '0', '0', '1', '安陆市');
INSERT INTO `system_areabase` VALUES ('1992', '90', '420900', '420984', '0', '0', '1', '汉川市');
INSERT INTO `system_areabase` VALUES ('1993', '90', '420900', '420985', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('1994', '90', '420000', '421000', '0', '0', '1', '荆州市');
INSERT INTO `system_areabase` VALUES ('1995', '90', '421000', '421002', '0', '0', '1', '沙市区');
INSERT INTO `system_areabase` VALUES ('1996', '90', '421000', '421003', '0', '0', '1', '荆州区');
INSERT INTO `system_areabase` VALUES ('1997', '90', '421000', '421022', '0', '0', '1', '公安县');
INSERT INTO `system_areabase` VALUES ('1998', '90', '421000', '421023', '0', '0', '1', '监利县');
INSERT INTO `system_areabase` VALUES ('1999', '90', '421000', '421024', '0', '0', '1', '江陵县');
INSERT INTO `system_areabase` VALUES ('2000', '90', '421000', '421081', '0', '0', '1', '石首市');
INSERT INTO `system_areabase` VALUES ('2001', '90', '421000', '421083', '0', '0', '1', '洪湖市');
INSERT INTO `system_areabase` VALUES ('2002', '90', '421000', '421087', '0', '0', '1', '松滋市');
INSERT INTO `system_areabase` VALUES ('2003', '90', '421000', '421088', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2004', '90', '420000', '421100', '0', '0', '1', '黄冈市');
INSERT INTO `system_areabase` VALUES ('2005', '90', '421100', '421102', '0', '0', '1', '黄州区');
INSERT INTO `system_areabase` VALUES ('2006', '90', '421100', '421121', '0', '0', '1', '团风县');
INSERT INTO `system_areabase` VALUES ('2007', '90', '421100', '421122', '0', '0', '1', '红安县');
INSERT INTO `system_areabase` VALUES ('2008', '90', '421100', '421123', '0', '0', '1', '罗田县');
INSERT INTO `system_areabase` VALUES ('2009', '90', '421100', '421124', '0', '0', '1', '英山县');
INSERT INTO `system_areabase` VALUES ('2010', '90', '421100', '421125', '0', '0', '1', '浠水县');
INSERT INTO `system_areabase` VALUES ('2011', '90', '421100', '421126', '0', '0', '1', '蕲春县');
INSERT INTO `system_areabase` VALUES ('2012', '90', '421100', '421127', '0', '0', '1', '黄梅县');
INSERT INTO `system_areabase` VALUES ('2013', '90', '421100', '421181', '0', '0', '1', '麻城市');
INSERT INTO `system_areabase` VALUES ('2014', '90', '421100', '421182', '0', '0', '1', '武穴市');
INSERT INTO `system_areabase` VALUES ('2015', '90', '421100', '421183', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2016', '90', '420000', '421200', '0', '0', '1', '咸宁市');
INSERT INTO `system_areabase` VALUES ('2017', '90', '421200', '421202', '0', '0', '1', '咸安区');
INSERT INTO `system_areabase` VALUES ('2018', '90', '421200', '421221', '0', '0', '1', '嘉鱼县');
INSERT INTO `system_areabase` VALUES ('2019', '90', '421200', '421222', '0', '0', '1', '通城县');
INSERT INTO `system_areabase` VALUES ('2020', '90', '421200', '421223', '0', '0', '1', '崇阳县');
INSERT INTO `system_areabase` VALUES ('2021', '90', '421200', '421224', '0', '0', '1', '通山县');
INSERT INTO `system_areabase` VALUES ('2022', '90', '421200', '421281', '0', '0', '1', '赤壁市');
INSERT INTO `system_areabase` VALUES ('2023', '90', '421200', '421282', '0', '0', '1', '温泉城区');
INSERT INTO `system_areabase` VALUES ('2024', '90', '421200', '421283', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2025', '90', '420000', '421300', '0', '0', '1', '随州市');
INSERT INTO `system_areabase` VALUES ('2026', '90', '421300', '421302', '0', '0', '1', '曾都区');
INSERT INTO `system_areabase` VALUES ('2027', '90', '421300', '421381', '0', '0', '1', '广水市');
INSERT INTO `system_areabase` VALUES ('2028', '90', '421300', '421382', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2029', '90', '420000', '422800', '0', '0', '1', '恩施土家族苗族自治州');
INSERT INTO `system_areabase` VALUES ('2030', '90', '422800', '422801', '0', '0', '1', '恩施市');
INSERT INTO `system_areabase` VALUES ('2031', '90', '422800', '422802', '0', '0', '1', '利川市');
INSERT INTO `system_areabase` VALUES ('2032', '90', '422800', '422822', '0', '0', '1', '建始县');
INSERT INTO `system_areabase` VALUES ('2033', '90', '422800', '422823', '0', '0', '1', '巴东县');
INSERT INTO `system_areabase` VALUES ('2034', '90', '422800', '422825', '0', '0', '1', '宣恩县');
INSERT INTO `system_areabase` VALUES ('2035', '90', '422800', '422826', '0', '0', '1', '咸丰县');
INSERT INTO `system_areabase` VALUES ('2036', '90', '422800', '422827', '0', '0', '1', '来凤县');
INSERT INTO `system_areabase` VALUES ('2037', '90', '422800', '422828', '0', '0', '1', '鹤峰县');
INSERT INTO `system_areabase` VALUES ('2038', '90', '422800', '422829', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2039', '90', '420000', '429004', '0', '0', '1', '仙桃市');
INSERT INTO `system_areabase` VALUES ('2040', '90', '420000', '429005', '0', '0', '1', '潜江市');
INSERT INTO `system_areabase` VALUES ('2041', '90', '420000', '429006', '0', '0', '1', '天门市');
INSERT INTO `system_areabase` VALUES ('2042', '90', '420000', '429021', '0', '0', '1', '神农架林区');
INSERT INTO `system_areabase` VALUES ('2043', '90', '0', '430000', '0', '0', '1', '湖南省');
INSERT INTO `system_areabase` VALUES ('2044', '90', '430000', '430100', '0', '0', '1', '长沙市');
INSERT INTO `system_areabase` VALUES ('2045', '90', '430100', '430102', '0', '0', '1', '芙蓉区');
INSERT INTO `system_areabase` VALUES ('2046', '90', '430100', '430103', '0', '0', '1', '天心区');
INSERT INTO `system_areabase` VALUES ('2047', '90', '430100', '430104', '0', '0', '1', '岳麓区');
INSERT INTO `system_areabase` VALUES ('2048', '90', '430100', '430105', '0', '0', '1', '开福区');
INSERT INTO `system_areabase` VALUES ('2049', '90', '430100', '430111', '0', '0', '1', '雨花区');
INSERT INTO `system_areabase` VALUES ('2050', '90', '430100', '430121', '0', '0', '1', '长沙县');
INSERT INTO `system_areabase` VALUES ('2051', '90', '430100', '430122', '0', '0', '1', '望城县');
INSERT INTO `system_areabase` VALUES ('2052', '90', '430100', '430124', '0', '0', '1', '宁乡县');
INSERT INTO `system_areabase` VALUES ('2053', '90', '430100', '430181', '0', '0', '1', '浏阳市');
INSERT INTO `system_areabase` VALUES ('2054', '90', '430100', '430182', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2055', '90', '430000', '430200', '0', '0', '1', '株洲市');
INSERT INTO `system_areabase` VALUES ('2056', '90', '430200', '430202', '0', '0', '1', '荷塘区');
INSERT INTO `system_areabase` VALUES ('2057', '90', '430200', '430203', '0', '0', '1', '芦淞区');
INSERT INTO `system_areabase` VALUES ('2058', '90', '430200', '430204', '0', '0', '1', '石峰区');
INSERT INTO `system_areabase` VALUES ('2059', '90', '430200', '430211', '0', '0', '1', '天元区');
INSERT INTO `system_areabase` VALUES ('2060', '90', '430200', '430221', '0', '0', '1', '株洲县');
INSERT INTO `system_areabase` VALUES ('2061', '90', '430200', '430223', '0', '0', '1', '攸县');
INSERT INTO `system_areabase` VALUES ('2062', '90', '430200', '430224', '0', '0', '1', '茶陵县');
INSERT INTO `system_areabase` VALUES ('2063', '90', '430200', '430225', '0', '0', '1', '炎陵县');
INSERT INTO `system_areabase` VALUES ('2064', '90', '430200', '430281', '0', '0', '1', '醴陵市');
INSERT INTO `system_areabase` VALUES ('2065', '90', '430200', '430282', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2066', '90', '430000', '430300', '0', '0', '1', '湘潭市');
INSERT INTO `system_areabase` VALUES ('2067', '90', '430300', '430302', '0', '0', '1', '雨湖区');
INSERT INTO `system_areabase` VALUES ('2068', '90', '430300', '430304', '0', '0', '1', '岳塘区');
INSERT INTO `system_areabase` VALUES ('2069', '90', '430300', '430321', '0', '0', '1', '湘潭县');
INSERT INTO `system_areabase` VALUES ('2070', '90', '430300', '430381', '0', '0', '1', '湘乡市');
INSERT INTO `system_areabase` VALUES ('2071', '90', '430300', '430382', '0', '0', '1', '韶山市');
INSERT INTO `system_areabase` VALUES ('2072', '90', '430300', '430383', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2073', '90', '430000', '430400', '0', '0', '1', '衡阳市');
INSERT INTO `system_areabase` VALUES ('2074', '90', '430400', '430405', '0', '0', '1', '珠晖区');
INSERT INTO `system_areabase` VALUES ('2075', '90', '430400', '430406', '0', '0', '1', '雁峰区');
INSERT INTO `system_areabase` VALUES ('2076', '90', '430400', '430407', '0', '0', '1', '石鼓区');
INSERT INTO `system_areabase` VALUES ('2077', '90', '430400', '430408', '0', '0', '1', '蒸湘区');
INSERT INTO `system_areabase` VALUES ('2078', '90', '430400', '430412', '0', '0', '1', '南岳区');
INSERT INTO `system_areabase` VALUES ('2079', '90', '430400', '430421', '0', '0', '1', '衡阳县');
INSERT INTO `system_areabase` VALUES ('2080', '90', '430400', '430422', '0', '0', '1', '衡南县');
INSERT INTO `system_areabase` VALUES ('2081', '90', '430400', '430423', '0', '0', '1', '衡山县');
INSERT INTO `system_areabase` VALUES ('2082', '90', '430400', '430424', '0', '0', '1', '衡东县');
INSERT INTO `system_areabase` VALUES ('2083', '90', '430400', '430426', '0', '0', '1', '祁东县');
INSERT INTO `system_areabase` VALUES ('2084', '90', '430400', '430481', '0', '0', '1', '耒阳市');
INSERT INTO `system_areabase` VALUES ('2085', '90', '430400', '430482', '0', '0', '1', '常宁市');
INSERT INTO `system_areabase` VALUES ('2086', '90', '430400', '430483', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2087', '90', '430000', '430500', '0', '0', '1', '邵阳市');
INSERT INTO `system_areabase` VALUES ('2088', '90', '430500', '430502', '0', '0', '1', '双清区');
INSERT INTO `system_areabase` VALUES ('2089', '90', '430500', '430503', '0', '0', '1', '大祥区');
INSERT INTO `system_areabase` VALUES ('2090', '90', '430500', '430511', '0', '0', '1', '北塔区');
INSERT INTO `system_areabase` VALUES ('2091', '90', '430500', '430521', '0', '0', '1', '邵东县');
INSERT INTO `system_areabase` VALUES ('2092', '90', '430500', '430522', '0', '0', '1', '新邵县');
INSERT INTO `system_areabase` VALUES ('2093', '90', '430500', '430523', '0', '0', '1', '邵阳县');
INSERT INTO `system_areabase` VALUES ('2094', '90', '430500', '430524', '0', '0', '1', '隆回县');
INSERT INTO `system_areabase` VALUES ('2095', '90', '430500', '430525', '0', '0', '1', '洞口县');
INSERT INTO `system_areabase` VALUES ('2096', '90', '430500', '430527', '0', '0', '1', '绥宁县');
INSERT INTO `system_areabase` VALUES ('2097', '90', '430500', '430528', '0', '0', '1', '新宁县');
INSERT INTO `system_areabase` VALUES ('2098', '90', '430500', '430529', '0', '0', '1', '城步苗族自治县');
INSERT INTO `system_areabase` VALUES ('2099', '90', '430500', '430581', '0', '0', '1', '武冈市');
INSERT INTO `system_areabase` VALUES ('2100', '90', '430500', '430582', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2101', '90', '430000', '430600', '0', '0', '1', '岳阳市');
INSERT INTO `system_areabase` VALUES ('2102', '90', '430600', '430602', '0', '0', '1', '岳阳楼区');
INSERT INTO `system_areabase` VALUES ('2103', '90', '430600', '430603', '0', '0', '1', '云溪区');
INSERT INTO `system_areabase` VALUES ('2104', '90', '430600', '430611', '0', '0', '1', '君山区');
INSERT INTO `system_areabase` VALUES ('2105', '90', '430600', '430621', '0', '0', '1', '岳阳县');
INSERT INTO `system_areabase` VALUES ('2106', '90', '430600', '430623', '0', '0', '1', '华容县');
INSERT INTO `system_areabase` VALUES ('2107', '90', '430600', '430624', '0', '0', '1', '湘阴县');
INSERT INTO `system_areabase` VALUES ('2108', '90', '430600', '430626', '0', '0', '1', '平江县');
INSERT INTO `system_areabase` VALUES ('2109', '90', '430600', '430681', '0', '0', '1', '汨罗市');
INSERT INTO `system_areabase` VALUES ('2110', '90', '410300', '471004', '0', '0', '1', '高新区');
INSERT INTO `system_areabase` VALUES ('2111', '90', '410300', '471005', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2112', '90', '430600', '430682', '0', '0', '1', '临湘市');
INSERT INTO `system_areabase` VALUES ('2113', '90', '430600', '430683', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2114', '90', '430000', '430700', '0', '0', '1', '常德市');
INSERT INTO `system_areabase` VALUES ('2115', '90', '430700', '430702', '0', '0', '1', '武陵区');
INSERT INTO `system_areabase` VALUES ('2116', '90', '430700', '430703', '0', '0', '1', '鼎城区');
INSERT INTO `system_areabase` VALUES ('2117', '90', '430700', '430721', '0', '0', '1', '安乡县');
INSERT INTO `system_areabase` VALUES ('2118', '90', '430700', '430722', '0', '0', '1', '汉寿县');
INSERT INTO `system_areabase` VALUES ('2119', '90', '430700', '430723', '0', '0', '1', '澧县');
INSERT INTO `system_areabase` VALUES ('2120', '90', '430700', '430724', '0', '0', '1', '临澧县');
INSERT INTO `system_areabase` VALUES ('2121', '90', '430700', '430725', '0', '0', '1', '桃源县');
INSERT INTO `system_areabase` VALUES ('2122', '90', '430700', '430726', '0', '0', '1', '石门县');
INSERT INTO `system_areabase` VALUES ('2123', '90', '430700', '430781', '0', '0', '1', '津市市');
INSERT INTO `system_areabase` VALUES ('2124', '90', '430700', '430782', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2125', '90', '430000', '430800', '0', '0', '1', '张家界市');
INSERT INTO `system_areabase` VALUES ('2126', '90', '430800', '430802', '0', '0', '1', '永定区');
INSERT INTO `system_areabase` VALUES ('2127', '90', '430800', '430811', '0', '0', '1', '武陵源区');
INSERT INTO `system_areabase` VALUES ('2128', '90', '430800', '430821', '0', '0', '1', '慈利县');
INSERT INTO `system_areabase` VALUES ('2129', '90', '430800', '430822', '0', '0', '1', '桑植县');
INSERT INTO `system_areabase` VALUES ('2130', '90', '430800', '430823', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2131', '90', '430000', '430900', '0', '0', '1', '益阳市');
INSERT INTO `system_areabase` VALUES ('2132', '90', '430900', '430902', '0', '0', '1', '资阳区');
INSERT INTO `system_areabase` VALUES ('2133', '90', '430900', '430903', '0', '0', '1', '赫山区');
INSERT INTO `system_areabase` VALUES ('2134', '90', '430900', '430921', '0', '0', '1', '南县');
INSERT INTO `system_areabase` VALUES ('2135', '90', '430900', '430922', '0', '0', '1', '桃江县');
INSERT INTO `system_areabase` VALUES ('2136', '90', '430900', '430923', '0', '0', '1', '安化县');
INSERT INTO `system_areabase` VALUES ('2137', '90', '430900', '430981', '0', '0', '1', '沅江市');
INSERT INTO `system_areabase` VALUES ('2138', '90', '430900', '430982', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2139', '90', '430000', '431000', '0', '0', '1', '郴州市');
INSERT INTO `system_areabase` VALUES ('2140', '90', '431000', '431002', '0', '0', '1', '北湖区');
INSERT INTO `system_areabase` VALUES ('2141', '90', '431000', '431003', '0', '0', '1', '苏仙区');
INSERT INTO `system_areabase` VALUES ('2142', '90', '431000', '431021', '0', '0', '1', '桂阳县');
INSERT INTO `system_areabase` VALUES ('2143', '90', '431000', '431022', '0', '0', '1', '宜章县');
INSERT INTO `system_areabase` VALUES ('2144', '90', '431000', '431023', '0', '0', '1', '永兴县');
INSERT INTO `system_areabase` VALUES ('2145', '90', '431000', '431024', '0', '0', '1', '嘉禾县');
INSERT INTO `system_areabase` VALUES ('2146', '90', '431000', '431025', '0', '0', '1', '临武县');
INSERT INTO `system_areabase` VALUES ('2147', '90', '431000', '431026', '0', '0', '1', '汝城县');
INSERT INTO `system_areabase` VALUES ('2148', '90', '431000', '431027', '0', '0', '1', '桂东县');
INSERT INTO `system_areabase` VALUES ('2149', '90', '431000', '431028', '0', '0', '1', '安仁县');
INSERT INTO `system_areabase` VALUES ('2150', '90', '431000', '431081', '0', '0', '1', '资兴市');
INSERT INTO `system_areabase` VALUES ('2151', '90', '431000', '431082', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2152', '90', '430000', '431100', '0', '0', '1', '永州市');
INSERT INTO `system_areabase` VALUES ('2153', '90', '431100', '431102', '0', '0', '1', '零陵区');
INSERT INTO `system_areabase` VALUES ('2154', '90', '431100', '431103', '0', '0', '1', '冷水滩区');
INSERT INTO `system_areabase` VALUES ('2155', '90', '431100', '431121', '0', '0', '1', '祁阳县');
INSERT INTO `system_areabase` VALUES ('2156', '90', '431100', '431122', '0', '0', '1', '东安县');
INSERT INTO `system_areabase` VALUES ('2157', '90', '431100', '431123', '0', '0', '1', '双牌县');
INSERT INTO `system_areabase` VALUES ('2158', '90', '431100', '431124', '0', '0', '1', '道县');
INSERT INTO `system_areabase` VALUES ('2159', '90', '431100', '431125', '0', '0', '1', '江永县');
INSERT INTO `system_areabase` VALUES ('2160', '90', '431100', '431126', '0', '0', '1', '宁远县');
INSERT INTO `system_areabase` VALUES ('2161', '90', '431100', '431127', '0', '0', '1', '蓝山县');
INSERT INTO `system_areabase` VALUES ('2162', '90', '431100', '431128', '0', '0', '1', '新田县');
INSERT INTO `system_areabase` VALUES ('2163', '90', '431100', '431129', '0', '0', '1', '江华瑶族自治县');
INSERT INTO `system_areabase` VALUES ('2164', '90', '431100', '431130', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2165', '90', '430000', '431200', '0', '0', '1', '怀化市');
INSERT INTO `system_areabase` VALUES ('2166', '90', '431200', '431202', '0', '0', '1', '鹤城区');
INSERT INTO `system_areabase` VALUES ('2167', '90', '431200', '431221', '0', '0', '1', '中方县');
INSERT INTO `system_areabase` VALUES ('2168', '90', '431200', '431222', '0', '0', '1', '沅陵县');
INSERT INTO `system_areabase` VALUES ('2169', '90', '431200', '431223', '0', '0', '1', '辰溪县');
INSERT INTO `system_areabase` VALUES ('2170', '90', '431200', '431224', '0', '0', '1', '溆浦县');
INSERT INTO `system_areabase` VALUES ('2171', '90', '431200', '431225', '0', '0', '1', '会同县');
INSERT INTO `system_areabase` VALUES ('2172', '90', '431200', '431226', '0', '0', '1', '麻阳苗族自治县');
INSERT INTO `system_areabase` VALUES ('2173', '90', '431200', '431227', '0', '0', '1', '新晃侗族自治县');
INSERT INTO `system_areabase` VALUES ('2174', '90', '431200', '431228', '0', '0', '1', '芷江侗族自治县');
INSERT INTO `system_areabase` VALUES ('2175', '90', '431200', '431229', '0', '0', '1', '靖州苗族侗族自治县');
INSERT INTO `system_areabase` VALUES ('2176', '90', '431200', '431230', '0', '0', '1', '通道侗族自治县');
INSERT INTO `system_areabase` VALUES ('2177', '90', '431200', '431281', '0', '0', '1', '洪江市');
INSERT INTO `system_areabase` VALUES ('2178', '90', '431200', '431282', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2179', '90', '430000', '431300', '0', '0', '1', '娄底市');
INSERT INTO `system_areabase` VALUES ('2180', '90', '431300', '431302', '0', '0', '1', '娄星区');
INSERT INTO `system_areabase` VALUES ('2181', '90', '431300', '431321', '0', '0', '1', '双峰县');
INSERT INTO `system_areabase` VALUES ('2182', '90', '431300', '431322', '0', '0', '1', '新化县');
INSERT INTO `system_areabase` VALUES ('2183', '90', '431300', '431381', '0', '0', '1', '冷水江市');
INSERT INTO `system_areabase` VALUES ('2184', '90', '431300', '431382', '0', '0', '1', '涟源市');
INSERT INTO `system_areabase` VALUES ('2185', '90', '431300', '431383', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2186', '90', '430000', '433100', '0', '0', '1', '湘西土家族苗族自治州');
INSERT INTO `system_areabase` VALUES ('2187', '90', '433100', '433101', '0', '0', '1', '吉首市');
INSERT INTO `system_areabase` VALUES ('2188', '90', '433100', '433122', '0', '0', '1', '泸溪县');
INSERT INTO `system_areabase` VALUES ('2189', '90', '433100', '433123', '0', '0', '1', '凤凰县');
INSERT INTO `system_areabase` VALUES ('2190', '90', '433100', '433124', '0', '0', '1', '花垣县');
INSERT INTO `system_areabase` VALUES ('2191', '90', '433100', '433125', '0', '0', '1', '保靖县');
INSERT INTO `system_areabase` VALUES ('2192', '90', '433100', '433126', '0', '0', '1', '古丈县');
INSERT INTO `system_areabase` VALUES ('2193', '90', '433100', '433127', '0', '0', '1', '永顺县');
INSERT INTO `system_areabase` VALUES ('2194', '90', '433100', '433130', '0', '0', '1', '龙山县');
INSERT INTO `system_areabase` VALUES ('2195', '90', '433100', '433131', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2196', '90', '0', '440000', '0', '0', '1', '广东省');
INSERT INTO `system_areabase` VALUES ('2197', '90', '440000', '440100', '0', '0', '1', '广州市');
INSERT INTO `system_areabase` VALUES ('2198', '90', '440100', '440103', '0', '0', '1', '荔湾区');
INSERT INTO `system_areabase` VALUES ('2199', '90', '440100', '440104', '0', '0', '1', '越秀区');
INSERT INTO `system_areabase` VALUES ('2200', '90', '440100', '440105', '0', '0', '1', '海珠区');
INSERT INTO `system_areabase` VALUES ('2201', '90', '440100', '440106', '0', '0', '1', '天河区');
INSERT INTO `system_areabase` VALUES ('2202', '90', '440100', '440111', '0', '0', '1', '白云区');
INSERT INTO `system_areabase` VALUES ('2203', '90', '440100', '440112', '0', '0', '1', '黄埔区');
INSERT INTO `system_areabase` VALUES ('2204', '90', '440100', '440113', '0', '0', '1', '番禺区');
INSERT INTO `system_areabase` VALUES ('2205', '90', '440100', '440114', '0', '0', '1', '花都区');
INSERT INTO `system_areabase` VALUES ('2206', '90', '440100', '440115', '0', '0', '1', '南沙区');
INSERT INTO `system_areabase` VALUES ('2207', '90', '440100', '440116', '0', '0', '1', '萝岗区');
INSERT INTO `system_areabase` VALUES ('2208', '90', '440100', '440183', '0', '0', '1', '增城市');
INSERT INTO `system_areabase` VALUES ('2209', '90', '440100', '440184', '0', '0', '1', '从化市');
INSERT INTO `system_areabase` VALUES ('2210', '90', '440100', '440188', '0', '0', '1', '东山区');
INSERT INTO `system_areabase` VALUES ('2211', '90', '440100', '440189', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2212', '90', '440000', '440200', '0', '0', '1', '韶关市');
INSERT INTO `system_areabase` VALUES ('2213', '90', '440200', '440203', '0', '0', '1', '武江区');
INSERT INTO `system_areabase` VALUES ('2214', '90', '440200', '440204', '0', '0', '1', '浈江区');
INSERT INTO `system_areabase` VALUES ('2215', '90', '440200', '440205', '0', '0', '1', '曲江区');
INSERT INTO `system_areabase` VALUES ('2216', '90', '440200', '440222', '0', '0', '1', '始兴县');
INSERT INTO `system_areabase` VALUES ('2217', '90', '440200', '440224', '0', '0', '1', '仁化县');
INSERT INTO `system_areabase` VALUES ('2218', '90', '440200', '440229', '0', '0', '1', '翁源县');
INSERT INTO `system_areabase` VALUES ('2219', '90', '440200', '440232', '0', '0', '1', '乳源瑶族自治县');
INSERT INTO `system_areabase` VALUES ('2220', '90', '440200', '440233', '0', '0', '1', '新丰县');
INSERT INTO `system_areabase` VALUES ('2221', '90', '440200', '440281', '0', '0', '1', '乐昌市');
INSERT INTO `system_areabase` VALUES ('2222', '90', '440200', '440282', '0', '0', '1', '南雄市');
INSERT INTO `system_areabase` VALUES ('2223', '90', '440200', '440283', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2224', '90', '440000', '440300', '0', '0', '1', '深圳市');
INSERT INTO `system_areabase` VALUES ('2225', '90', '440300', '440303', '0', '0', '1', '罗湖区');
INSERT INTO `system_areabase` VALUES ('2226', '90', '440300', '440304', '0', '0', '1', '福田区');
INSERT INTO `system_areabase` VALUES ('2227', '90', '440300', '440305', '0', '0', '1', '南山区');
INSERT INTO `system_areabase` VALUES ('2228', '90', '440300', '440306', '0', '0', '1', '宝安区');
INSERT INTO `system_areabase` VALUES ('2229', '90', '440300', '440307', '0', '0', '1', '龙岗区');
INSERT INTO `system_areabase` VALUES ('2230', '90', '440300', '440308', '0', '0', '1', '盐田区');
INSERT INTO `system_areabase` VALUES ('2231', '90', '440300', '440309', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2232', '90', '440000', '440400', '0', '0', '1', '珠海市');
INSERT INTO `system_areabase` VALUES ('2233', '90', '440400', '440402', '0', '0', '1', '香洲区');
INSERT INTO `system_areabase` VALUES ('2234', '90', '440400', '440403', '0', '0', '1', '斗门区');
INSERT INTO `system_areabase` VALUES ('2235', '90', '440400', '440404', '0', '0', '1', '金湾区');
INSERT INTO `system_areabase` VALUES ('2236', '90', '440400', '440486', '0', '0', '1', '金唐区');
INSERT INTO `system_areabase` VALUES ('2237', '90', '440400', '440487', '0', '0', '1', '南湾区');
INSERT INTO `system_areabase` VALUES ('2238', '90', '440400', '440488', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2239', '90', '440000', '440500', '0', '0', '1', '汕头市');
INSERT INTO `system_areabase` VALUES ('2240', '90', '440500', '440507', '0', '0', '1', '龙湖区');
INSERT INTO `system_areabase` VALUES ('2241', '90', '440500', '440511', '0', '0', '1', '金平区');
INSERT INTO `system_areabase` VALUES ('2242', '90', '440500', '440512', '0', '0', '1', '濠江区');
INSERT INTO `system_areabase` VALUES ('2243', '90', '440500', '440513', '0', '0', '1', '潮阳区');
INSERT INTO `system_areabase` VALUES ('2244', '90', '440500', '440514', '0', '0', '1', '潮南区');
INSERT INTO `system_areabase` VALUES ('2245', '90', '440500', '440515', '0', '0', '1', '澄海区');
INSERT INTO `system_areabase` VALUES ('2246', '90', '440500', '440523', '0', '0', '1', '南澳县');
INSERT INTO `system_areabase` VALUES ('2247', '90', '440500', '440524', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2248', '90', '440000', '440600', '0', '0', '1', '佛山市');
INSERT INTO `system_areabase` VALUES ('2249', '90', '440600', '440604', '0', '0', '1', '禅城区');
INSERT INTO `system_areabase` VALUES ('2250', '90', '440600', '440605', '0', '0', '1', '南海区');
INSERT INTO `system_areabase` VALUES ('2251', '90', '440600', '440606', '0', '0', '1', '顺德区');
INSERT INTO `system_areabase` VALUES ('2252', '90', '440600', '440607', '0', '0', '1', '三水区');
INSERT INTO `system_areabase` VALUES ('2253', '90', '440600', '440608', '0', '0', '1', '高明区');
INSERT INTO `system_areabase` VALUES ('2254', '90', '440600', '440609', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2255', '90', '440000', '440700', '0', '0', '1', '江门市');
INSERT INTO `system_areabase` VALUES ('2256', '90', '440700', '440703', '0', '0', '1', '蓬江区');
INSERT INTO `system_areabase` VALUES ('2257', '90', '440700', '440704', '0', '0', '1', '江海区');
INSERT INTO `system_areabase` VALUES ('2258', '90', '440700', '440705', '0', '0', '1', '新会区');
INSERT INTO `system_areabase` VALUES ('2259', '90', '440700', '440781', '0', '0', '1', '台山市');
INSERT INTO `system_areabase` VALUES ('2260', '90', '440700', '440783', '0', '0', '1', '开平市');
INSERT INTO `system_areabase` VALUES ('2261', '90', '440700', '440784', '0', '0', '1', '鹤山市');
INSERT INTO `system_areabase` VALUES ('2262', '90', '440700', '440785', '0', '0', '1', '恩平市');
INSERT INTO `system_areabase` VALUES ('2263', '90', '440700', '440786', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2264', '90', '440000', '440800', '0', '0', '1', '湛江市');
INSERT INTO `system_areabase` VALUES ('2265', '90', '440800', '440802', '0', '0', '1', '赤坎区');
INSERT INTO `system_areabase` VALUES ('2266', '90', '440800', '440803', '0', '0', '1', '霞山区');
INSERT INTO `system_areabase` VALUES ('2267', '90', '440800', '440804', '0', '0', '1', '坡头区');
INSERT INTO `system_areabase` VALUES ('2268', '90', '440800', '440811', '0', '0', '1', '麻章区');
INSERT INTO `system_areabase` VALUES ('2269', '90', '440800', '440823', '0', '0', '1', '遂溪县');
INSERT INTO `system_areabase` VALUES ('2270', '90', '440800', '440825', '0', '0', '1', '徐闻县');
INSERT INTO `system_areabase` VALUES ('2271', '90', '440800', '440881', '0', '0', '1', '廉江市');
INSERT INTO `system_areabase` VALUES ('2272', '90', '440800', '440882', '0', '0', '1', '雷州市');
INSERT INTO `system_areabase` VALUES ('2273', '90', '440800', '440883', '0', '0', '1', '吴川市');
INSERT INTO `system_areabase` VALUES ('2274', '90', '440800', '440884', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2275', '90', '440000', '440900', '0', '0', '1', '茂名市');
INSERT INTO `system_areabase` VALUES ('2276', '90', '440900', '440902', '0', '0', '1', '茂南区');
INSERT INTO `system_areabase` VALUES ('2277', '90', '440900', '440903', '0', '0', '1', '茂港区');
INSERT INTO `system_areabase` VALUES ('2278', '90', '440900', '440923', '0', '0', '1', '电白县');
INSERT INTO `system_areabase` VALUES ('2279', '90', '440900', '440981', '0', '0', '1', '高州市');
INSERT INTO `system_areabase` VALUES ('2280', '90', '440900', '440982', '0', '0', '1', '化州市');
INSERT INTO `system_areabase` VALUES ('2281', '90', '440900', '440983', '0', '0', '1', '信宜市');
INSERT INTO `system_areabase` VALUES ('2282', '90', '440900', '440984', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2283', '90', '440000', '441200', '0', '0', '1', '肇庆市');
INSERT INTO `system_areabase` VALUES ('2284', '90', '441200', '441202', '0', '0', '1', '端州区');
INSERT INTO `system_areabase` VALUES ('2285', '90', '441200', '441203', '0', '0', '1', '鼎湖区');
INSERT INTO `system_areabase` VALUES ('2286', '90', '441200', '441223', '0', '0', '1', '广宁县');
INSERT INTO `system_areabase` VALUES ('2287', '90', '441200', '441224', '0', '0', '1', '怀集县');
INSERT INTO `system_areabase` VALUES ('2288', '90', '441200', '441225', '0', '0', '1', '封开县');
INSERT INTO `system_areabase` VALUES ('2289', '90', '441200', '441226', '0', '0', '1', '德庆县');
INSERT INTO `system_areabase` VALUES ('2290', '90', '441200', '441283', '0', '0', '1', '高要市');
INSERT INTO `system_areabase` VALUES ('2291', '90', '441200', '441284', '0', '0', '1', '四会市');
INSERT INTO `system_areabase` VALUES ('2292', '90', '441200', '441285', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2293', '90', '440000', '441300', '0', '0', '1', '惠州市');
INSERT INTO `system_areabase` VALUES ('2294', '90', '441300', '441302', '0', '0', '1', '惠城区');
INSERT INTO `system_areabase` VALUES ('2295', '90', '441300', '441303', '0', '0', '1', '惠阳区');
INSERT INTO `system_areabase` VALUES ('2296', '90', '441300', '441322', '0', '0', '1', '博罗县');
INSERT INTO `system_areabase` VALUES ('2297', '90', '441300', '441323', '0', '0', '1', '惠东县');
INSERT INTO `system_areabase` VALUES ('2298', '90', '441300', '441324', '0', '0', '1', '龙门县');
INSERT INTO `system_areabase` VALUES ('2299', '90', '441300', '441325', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2300', '90', '440000', '441400', '0', '0', '1', '梅州市');
INSERT INTO `system_areabase` VALUES ('2301', '90', '441400', '441402', '0', '0', '1', '梅江区');
INSERT INTO `system_areabase` VALUES ('2302', '90', '441400', '441421', '0', '0', '1', '梅县');
INSERT INTO `system_areabase` VALUES ('2303', '90', '441400', '441422', '0', '0', '1', '大埔县');
INSERT INTO `system_areabase` VALUES ('2304', '90', '441400', '441423', '0', '0', '1', '丰顺县');
INSERT INTO `system_areabase` VALUES ('2305', '90', '441400', '441424', '0', '0', '1', '五华县');
INSERT INTO `system_areabase` VALUES ('2306', '90', '441400', '441426', '0', '0', '1', '平远县');
INSERT INTO `system_areabase` VALUES ('2307', '90', '441400', '441427', '0', '0', '1', '蕉岭县');
INSERT INTO `system_areabase` VALUES ('2308', '90', '441400', '441481', '0', '0', '1', '兴宁市');
INSERT INTO `system_areabase` VALUES ('2309', '90', '441400', '441482', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2310', '90', '440000', '441500', '0', '0', '1', '汕尾市');
INSERT INTO `system_areabase` VALUES ('2311', '90', '441500', '441502', '0', '0', '1', '城区');
INSERT INTO `system_areabase` VALUES ('2312', '90', '441500', '441521', '0', '0', '1', '海丰县');
INSERT INTO `system_areabase` VALUES ('2313', '90', '441500', '441523', '0', '0', '1', '陆河县');
INSERT INTO `system_areabase` VALUES ('2314', '90', '441500', '441581', '0', '0', '1', '陆丰市');
INSERT INTO `system_areabase` VALUES ('2315', '90', '441500', '441582', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2316', '90', '440000', '441600', '0', '0', '1', '河源市');
INSERT INTO `system_areabase` VALUES ('2317', '90', '441600', '441602', '0', '0', '1', '源城区');
INSERT INTO `system_areabase` VALUES ('2318', '90', '441600', '441621', '0', '0', '1', '紫金县');
INSERT INTO `system_areabase` VALUES ('2319', '90', '441600', '441622', '0', '0', '1', '龙川县');
INSERT INTO `system_areabase` VALUES ('2320', '90', '441600', '441623', '0', '0', '1', '连平县');
INSERT INTO `system_areabase` VALUES ('2321', '90', '441600', '441624', '0', '0', '1', '和平县');
INSERT INTO `system_areabase` VALUES ('2322', '90', '441600', '441625', '0', '0', '1', '东源县');
INSERT INTO `system_areabase` VALUES ('2323', '90', '441600', '441626', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2324', '90', '440000', '441700', '0', '0', '1', '阳江市');
INSERT INTO `system_areabase` VALUES ('2325', '90', '441700', '441702', '0', '0', '1', '江城区');
INSERT INTO `system_areabase` VALUES ('2326', '90', '441700', '441721', '0', '0', '1', '阳西县');
INSERT INTO `system_areabase` VALUES ('2327', '90', '441700', '441723', '0', '0', '1', '阳东县');
INSERT INTO `system_areabase` VALUES ('2328', '90', '441700', '441781', '0', '0', '1', '阳春市');
INSERT INTO `system_areabase` VALUES ('2329', '90', '441700', '441782', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2330', '90', '440000', '441800', '0', '0', '1', '清远市');
INSERT INTO `system_areabase` VALUES ('2331', '90', '441800', '441802', '0', '0', '1', '清城区');
INSERT INTO `system_areabase` VALUES ('2332', '90', '441800', '441821', '0', '0', '1', '佛冈县');
INSERT INTO `system_areabase` VALUES ('2333', '90', '441800', '441823', '0', '0', '1', '阳山县');
INSERT INTO `system_areabase` VALUES ('2334', '90', '441800', '441825', '0', '0', '1', '连山壮族瑶族自治县');
INSERT INTO `system_areabase` VALUES ('2335', '90', '441800', '441826', '0', '0', '1', '连南瑶族自治县');
INSERT INTO `system_areabase` VALUES ('2336', '90', '441800', '441827', '0', '0', '1', '清新县');
INSERT INTO `system_areabase` VALUES ('2337', '90', '441800', '441881', '0', '0', '1', '英德市');
INSERT INTO `system_areabase` VALUES ('2338', '90', '441800', '441882', '0', '0', '1', '连州市');
INSERT INTO `system_areabase` VALUES ('2339', '90', '441800', '441883', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2340', '90', '440000', '441900', '0', '0', '1', '东莞市');
INSERT INTO `system_areabase` VALUES ('2341', '90', '440000', '442000', '0', '0', '1', '中山市');
INSERT INTO `system_areabase` VALUES ('2342', '90', '440000', '445100', '0', '0', '1', '潮州市');
INSERT INTO `system_areabase` VALUES ('2343', '90', '445100', '445102', '0', '0', '1', '湘桥区');
INSERT INTO `system_areabase` VALUES ('2344', '90', '445100', '445121', '0', '0', '1', '潮安县');
INSERT INTO `system_areabase` VALUES ('2345', '90', '445100', '445122', '0', '0', '1', '饶平县');
INSERT INTO `system_areabase` VALUES ('2346', '90', '445100', '445185', '0', '0', '1', '枫溪区');
INSERT INTO `system_areabase` VALUES ('2347', '90', '445100', '445186', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2348', '90', '440000', '445200', '0', '0', '1', '揭阳市');
INSERT INTO `system_areabase` VALUES ('2349', '90', '445200', '445202', '0', '0', '1', '榕城区');
INSERT INTO `system_areabase` VALUES ('2350', '90', '445200', '445221', '0', '0', '1', '揭东县');
INSERT INTO `system_areabase` VALUES ('2351', '90', '445200', '445222', '0', '0', '1', '揭西县');
INSERT INTO `system_areabase` VALUES ('2352', '90', '445200', '445224', '0', '0', '1', '惠来县');
INSERT INTO `system_areabase` VALUES ('2353', '90', '445200', '445281', '0', '0', '1', '普宁市');
INSERT INTO `system_areabase` VALUES ('2354', '90', '445200', '445284', '0', '0', '1', '东山区');
INSERT INTO `system_areabase` VALUES ('2355', '90', '445200', '445285', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2356', '90', '440000', '445300', '0', '0', '1', '云浮市');
INSERT INTO `system_areabase` VALUES ('2357', '90', '445300', '445302', '0', '0', '1', '云城区');
INSERT INTO `system_areabase` VALUES ('2358', '90', '445300', '445321', '0', '0', '1', '新兴县');
INSERT INTO `system_areabase` VALUES ('2359', '90', '445300', '445322', '0', '0', '1', '郁南县');
INSERT INTO `system_areabase` VALUES ('2360', '90', '445300', '445323', '0', '0', '1', '云安县');
INSERT INTO `system_areabase` VALUES ('2361', '90', '445300', '445381', '0', '0', '1', '罗定市');
INSERT INTO `system_areabase` VALUES ('2362', '90', '445300', '445382', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2363', '90', '0', '450000', '0', '0', '1', '广西壮族自治区');
INSERT INTO `system_areabase` VALUES ('2364', '90', '450000', '450100', '0', '0', '1', '南宁市');
INSERT INTO `system_areabase` VALUES ('2365', '90', '450100', '450102', '0', '0', '1', '兴宁区');
INSERT INTO `system_areabase` VALUES ('2366', '90', '450100', '450103', '0', '0', '1', '青秀区');
INSERT INTO `system_areabase` VALUES ('2367', '90', '450100', '450105', '0', '0', '1', '江南区');
INSERT INTO `system_areabase` VALUES ('2368', '90', '450100', '450107', '0', '0', '1', '西乡塘区');
INSERT INTO `system_areabase` VALUES ('2369', '90', '450100', '450108', '0', '0', '1', '良庆区');
INSERT INTO `system_areabase` VALUES ('2370', '90', '450100', '450109', '0', '0', '1', '邕宁区');
INSERT INTO `system_areabase` VALUES ('2371', '90', '450100', '450122', '0', '0', '1', '武鸣县');
INSERT INTO `system_areabase` VALUES ('2372', '90', '450100', '450123', '0', '0', '1', '隆安县');
INSERT INTO `system_areabase` VALUES ('2373', '90', '450100', '450124', '0', '0', '1', '马山县');
INSERT INTO `system_areabase` VALUES ('2374', '90', '450100', '450125', '0', '0', '1', '上林县');
INSERT INTO `system_areabase` VALUES ('2375', '90', '450100', '450126', '0', '0', '1', '宾阳县');
INSERT INTO `system_areabase` VALUES ('2376', '90', '450100', '450127', '0', '0', '1', '横县');
INSERT INTO `system_areabase` VALUES ('2377', '90', '450100', '450128', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2378', '90', '450000', '450200', '0', '0', '1', '柳州市');
INSERT INTO `system_areabase` VALUES ('2379', '90', '450200', '450202', '0', '0', '1', '城中区');
INSERT INTO `system_areabase` VALUES ('2380', '90', '450200', '450203', '0', '0', '1', '鱼峰区');
INSERT INTO `system_areabase` VALUES ('2381', '90', '450200', '450204', '0', '0', '1', '柳南区');
INSERT INTO `system_areabase` VALUES ('2382', '90', '450200', '450205', '0', '0', '1', '柳北区');
INSERT INTO `system_areabase` VALUES ('2383', '90', '450200', '450221', '0', '0', '1', '柳江县');
INSERT INTO `system_areabase` VALUES ('2384', '90', '450200', '450222', '0', '0', '1', '柳城县');
INSERT INTO `system_areabase` VALUES ('2385', '90', '450200', '450223', '0', '0', '1', '鹿寨县');
INSERT INTO `system_areabase` VALUES ('2386', '90', '450200', '450224', '0', '0', '1', '融安县');
INSERT INTO `system_areabase` VALUES ('2387', '90', '450200', '450225', '0', '0', '1', '融水苗族自治县');
INSERT INTO `system_areabase` VALUES ('2388', '90', '450200', '450226', '0', '0', '1', '三江侗族自治县');
INSERT INTO `system_areabase` VALUES ('2389', '90', '450200', '450227', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2390', '90', '450000', '450300', '0', '0', '1', '桂林市');
INSERT INTO `system_areabase` VALUES ('2391', '90', '450300', '450302', '0', '0', '1', '秀峰区');
INSERT INTO `system_areabase` VALUES ('2392', '90', '450300', '450303', '0', '0', '1', '叠彩区');
INSERT INTO `system_areabase` VALUES ('2393', '90', '450300', '450304', '0', '0', '1', '象山区');
INSERT INTO `system_areabase` VALUES ('2394', '90', '450300', '450305', '0', '0', '1', '七星区');
INSERT INTO `system_areabase` VALUES ('2395', '90', '450300', '450311', '0', '0', '1', '雁山区');
INSERT INTO `system_areabase` VALUES ('2396', '90', '450300', '450321', '0', '0', '1', '阳朔县');
INSERT INTO `system_areabase` VALUES ('2397', '90', '450300', '450322', '0', '0', '1', '临桂县');
INSERT INTO `system_areabase` VALUES ('2398', '90', '450300', '450323', '0', '0', '1', '灵川县');
INSERT INTO `system_areabase` VALUES ('2399', '90', '450300', '450324', '0', '0', '1', '全州县');
INSERT INTO `system_areabase` VALUES ('2400', '90', '450300', '450325', '0', '0', '1', '兴安县');
INSERT INTO `system_areabase` VALUES ('2401', '90', '450300', '450326', '0', '0', '1', '永福县');
INSERT INTO `system_areabase` VALUES ('2402', '90', '450300', '450327', '0', '0', '1', '灌阳县');
INSERT INTO `system_areabase` VALUES ('2403', '90', '450300', '450328', '0', '0', '1', '龙胜各族自治县');
INSERT INTO `system_areabase` VALUES ('2404', '90', '450300', '450329', '0', '0', '1', '资源县');
INSERT INTO `system_areabase` VALUES ('2405', '90', '450300', '450330', '0', '0', '1', '平乐县');
INSERT INTO `system_areabase` VALUES ('2406', '90', '450300', '450331', '0', '0', '1', '荔浦县');
INSERT INTO `system_areabase` VALUES ('2407', '90', '450300', '450332', '0', '0', '1', '恭城瑶族自治县');
INSERT INTO `system_areabase` VALUES ('2408', '90', '450300', '450333', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2409', '90', '450000', '450400', '0', '0', '1', '梧州市');
INSERT INTO `system_areabase` VALUES ('2410', '90', '450400', '450403', '0', '0', '1', '万秀区');
INSERT INTO `system_areabase` VALUES ('2411', '90', '450400', '450404', '0', '0', '1', '蝶山区');
INSERT INTO `system_areabase` VALUES ('2412', '90', '450400', '450405', '0', '0', '1', '长洲区');
INSERT INTO `system_areabase` VALUES ('2413', '90', '450400', '450421', '0', '0', '1', '苍梧县');
INSERT INTO `system_areabase` VALUES ('2414', '90', '450400', '450422', '0', '0', '1', '藤县');
INSERT INTO `system_areabase` VALUES ('2415', '90', '450400', '450423', '0', '0', '1', '蒙山县');
INSERT INTO `system_areabase` VALUES ('2416', '90', '450400', '450481', '0', '0', '1', '岑溪市');
INSERT INTO `system_areabase` VALUES ('2417', '90', '450400', '450482', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2418', '90', '450000', '450500', '0', '0', '1', '北海市');
INSERT INTO `system_areabase` VALUES ('2419', '90', '450500', '450502', '0', '0', '1', '海城区');
INSERT INTO `system_areabase` VALUES ('2420', '90', '450500', '450503', '0', '0', '1', '银海区');
INSERT INTO `system_areabase` VALUES ('2421', '90', '450500', '450512', '0', '0', '1', '铁山港区');
INSERT INTO `system_areabase` VALUES ('2422', '90', '450500', '450521', '0', '0', '1', '合浦县');
INSERT INTO `system_areabase` VALUES ('2423', '90', '450500', '450522', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2424', '90', '450000', '450600', '0', '0', '1', '防城港市');
INSERT INTO `system_areabase` VALUES ('2425', '90', '450600', '450602', '0', '0', '1', '港口区');
INSERT INTO `system_areabase` VALUES ('2426', '90', '450600', '450603', '0', '0', '1', '防城区');
INSERT INTO `system_areabase` VALUES ('2427', '90', '450600', '450621', '0', '0', '1', '上思县');
INSERT INTO `system_areabase` VALUES ('2428', '90', '450600', '450681', '0', '0', '1', '东兴市');
INSERT INTO `system_areabase` VALUES ('2429', '90', '450600', '450682', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2430', '90', '450000', '450700', '0', '0', '1', '钦州市');
INSERT INTO `system_areabase` VALUES ('2431', '90', '450700', '450702', '0', '0', '1', '钦南区');
INSERT INTO `system_areabase` VALUES ('2432', '90', '450700', '450703', '0', '0', '1', '钦北区');
INSERT INTO `system_areabase` VALUES ('2433', '90', '450700', '450721', '0', '0', '1', '灵山县');
INSERT INTO `system_areabase` VALUES ('2434', '90', '450700', '450722', '0', '0', '1', '浦北县');
INSERT INTO `system_areabase` VALUES ('2435', '90', '450700', '450723', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2436', '90', '450000', '450800', '0', '0', '1', '贵港市');
INSERT INTO `system_areabase` VALUES ('2437', '90', '450800', '450802', '0', '0', '1', '港北区');
INSERT INTO `system_areabase` VALUES ('2438', '90', '450800', '450803', '0', '0', '1', '港南区');
INSERT INTO `system_areabase` VALUES ('2439', '90', '450800', '450804', '0', '0', '1', '覃塘区');
INSERT INTO `system_areabase` VALUES ('2440', '90', '450800', '450821', '0', '0', '1', '平南县');
INSERT INTO `system_areabase` VALUES ('2441', '90', '450800', '450881', '0', '0', '1', '桂平市');
INSERT INTO `system_areabase` VALUES ('2442', '90', '450800', '450882', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2443', '90', '450000', '450900', '0', '0', '1', '玉林市');
INSERT INTO `system_areabase` VALUES ('2444', '90', '450900', '450902', '0', '0', '1', '玉州区');
INSERT INTO `system_areabase` VALUES ('2445', '90', '450900', '450921', '0', '0', '1', '容县');
INSERT INTO `system_areabase` VALUES ('2446', '90', '450900', '450922', '0', '0', '1', '陆川县');
INSERT INTO `system_areabase` VALUES ('2447', '90', '450900', '450923', '0', '0', '1', '博白县');
INSERT INTO `system_areabase` VALUES ('2448', '90', '450900', '450924', '0', '0', '1', '兴业县');
INSERT INTO `system_areabase` VALUES ('2449', '90', '450900', '450981', '0', '0', '1', '北流市');
INSERT INTO `system_areabase` VALUES ('2450', '90', '450900', '450982', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2451', '90', '450000', '451000', '0', '0', '1', '百色市');
INSERT INTO `system_areabase` VALUES ('2452', '90', '451000', '451002', '0', '0', '1', '右江区');
INSERT INTO `system_areabase` VALUES ('2453', '90', '451000', '451021', '0', '0', '1', '田阳县');
INSERT INTO `system_areabase` VALUES ('2454', '90', '451000', '451022', '0', '0', '1', '田东县');
INSERT INTO `system_areabase` VALUES ('2455', '90', '451000', '451023', '0', '0', '1', '平果县');
INSERT INTO `system_areabase` VALUES ('2456', '90', '451000', '451024', '0', '0', '1', '德保县');
INSERT INTO `system_areabase` VALUES ('2457', '90', '451000', '451025', '0', '0', '1', '靖西县');
INSERT INTO `system_areabase` VALUES ('2458', '90', '451000', '451026', '0', '0', '1', '那坡县');
INSERT INTO `system_areabase` VALUES ('2459', '90', '451000', '451027', '0', '0', '1', '凌云县');
INSERT INTO `system_areabase` VALUES ('2460', '90', '451000', '451028', '0', '0', '1', '乐业县');
INSERT INTO `system_areabase` VALUES ('2461', '90', '451000', '451029', '0', '0', '1', '田林县');
INSERT INTO `system_areabase` VALUES ('2462', '90', '451000', '451030', '0', '0', '1', '西林县');
INSERT INTO `system_areabase` VALUES ('2463', '90', '451000', '451031', '0', '0', '1', '隆林各族自治县');
INSERT INTO `system_areabase` VALUES ('2464', '90', '451000', '451032', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2465', '90', '450000', '451100', '0', '0', '1', '贺州市');
INSERT INTO `system_areabase` VALUES ('2466', '90', '451100', '451102', '0', '0', '1', '八步区');
INSERT INTO `system_areabase` VALUES ('2467', '90', '451100', '451121', '0', '0', '1', '昭平县');
INSERT INTO `system_areabase` VALUES ('2468', '90', '451100', '451122', '0', '0', '1', '钟山县');
INSERT INTO `system_areabase` VALUES ('2469', '90', '451100', '451123', '0', '0', '1', '富川瑶族自治县');
INSERT INTO `system_areabase` VALUES ('2470', '90', '451100', '451124', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2471', '90', '450000', '451200', '0', '0', '1', '河池市');
INSERT INTO `system_areabase` VALUES ('2472', '90', '451200', '451202', '0', '0', '1', '金城江区');
INSERT INTO `system_areabase` VALUES ('2473', '90', '451200', '451221', '0', '0', '1', '南丹县');
INSERT INTO `system_areabase` VALUES ('2474', '90', '451200', '451222', '0', '0', '1', '天峨县');
INSERT INTO `system_areabase` VALUES ('2475', '90', '451200', '451223', '0', '0', '1', '凤山县');
INSERT INTO `system_areabase` VALUES ('2476', '90', '451200', '451224', '0', '0', '1', '东兰县');
INSERT INTO `system_areabase` VALUES ('2477', '90', '451200', '451225', '0', '0', '1', '罗城仫佬族自治县');
INSERT INTO `system_areabase` VALUES ('2478', '90', '451200', '451226', '0', '0', '1', '环江毛南族自治县');
INSERT INTO `system_areabase` VALUES ('2479', '90', '451200', '451227', '0', '0', '1', '巴马瑶族自治县');
INSERT INTO `system_areabase` VALUES ('2480', '90', '451200', '451228', '0', '0', '1', '都安瑶族自治县');
INSERT INTO `system_areabase` VALUES ('2481', '90', '451200', '451229', '0', '0', '1', '大化瑶族自治县');
INSERT INTO `system_areabase` VALUES ('2482', '90', '451200', '451281', '0', '0', '1', '宜州市');
INSERT INTO `system_areabase` VALUES ('2483', '90', '451200', '451282', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2484', '90', '450000', '451300', '0', '0', '1', '来宾市');
INSERT INTO `system_areabase` VALUES ('2485', '90', '451300', '451302', '0', '0', '1', '兴宾区');
INSERT INTO `system_areabase` VALUES ('2486', '90', '451300', '451321', '0', '0', '1', '忻城县');
INSERT INTO `system_areabase` VALUES ('2487', '90', '451300', '451322', '0', '0', '1', '象州县');
INSERT INTO `system_areabase` VALUES ('2488', '90', '451300', '451323', '0', '0', '1', '武宣县');
INSERT INTO `system_areabase` VALUES ('2489', '90', '451300', '451324', '0', '0', '1', '金秀瑶族自治县');
INSERT INTO `system_areabase` VALUES ('2490', '90', '451300', '451381', '0', '0', '1', '合山市');
INSERT INTO `system_areabase` VALUES ('2491', '90', '451300', '451382', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2492', '90', '450000', '451400', '0', '0', '1', '崇左市');
INSERT INTO `system_areabase` VALUES ('2493', '90', '451400', '451402', '0', '0', '1', '江州区');
INSERT INTO `system_areabase` VALUES ('2494', '90', '451400', '451421', '0', '0', '1', '扶绥县');
INSERT INTO `system_areabase` VALUES ('2495', '90', '451400', '451422', '0', '0', '1', '宁明县');
INSERT INTO `system_areabase` VALUES ('2496', '90', '451400', '451423', '0', '0', '1', '龙州县');
INSERT INTO `system_areabase` VALUES ('2497', '90', '451400', '451424', '0', '0', '1', '大新县');
INSERT INTO `system_areabase` VALUES ('2498', '90', '451400', '451425', '0', '0', '1', '天等县');
INSERT INTO `system_areabase` VALUES ('2499', '90', '451400', '451481', '0', '0', '1', '凭祥市');
INSERT INTO `system_areabase` VALUES ('2500', '90', '451400', '451482', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2501', '90', '0', '460000', '0', '0', '1', '海南省');
INSERT INTO `system_areabase` VALUES ('2502', '90', '460000', '460100', '0', '0', '1', '海口市');
INSERT INTO `system_areabase` VALUES ('2503', '90', '460100', '460105', '0', '0', '1', '秀英区');
INSERT INTO `system_areabase` VALUES ('2504', '90', '460100', '460106', '0', '0', '1', '龙华区');
INSERT INTO `system_areabase` VALUES ('2505', '90', '460100', '460107', '0', '0', '1', '琼山区');
INSERT INTO `system_areabase` VALUES ('2506', '90', '460100', '460108', '0', '0', '1', '美兰区');
INSERT INTO `system_areabase` VALUES ('2507', '90', '460100', '460109', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2508', '90', '460000', '460200', '0', '0', '1', '三亚市');
INSERT INTO `system_areabase` VALUES ('2509', '90', '460000', '469001', '0', '0', '1', '五指山市');
INSERT INTO `system_areabase` VALUES ('2510', '90', '460000', '469002', '0', '0', '1', '琼海市');
INSERT INTO `system_areabase` VALUES ('2511', '90', '460000', '469003', '0', '0', '1', '儋州市');
INSERT INTO `system_areabase` VALUES ('2512', '90', '460000', '469005', '0', '0', '1', '文昌市');
INSERT INTO `system_areabase` VALUES ('2513', '90', '460000', '469006', '0', '0', '1', '万宁市');
INSERT INTO `system_areabase` VALUES ('2514', '90', '460000', '469007', '0', '0', '1', '东方市');
INSERT INTO `system_areabase` VALUES ('2515', '90', '460000', '469025', '0', '0', '1', '定安县');
INSERT INTO `system_areabase` VALUES ('2516', '90', '460000', '469026', '0', '0', '1', '屯昌县');
INSERT INTO `system_areabase` VALUES ('2517', '90', '460000', '469027', '0', '0', '1', '澄迈县');
INSERT INTO `system_areabase` VALUES ('2518', '90', '460000', '469028', '0', '0', '1', '临高县');
INSERT INTO `system_areabase` VALUES ('2519', '90', '460000', '469030', '0', '0', '1', '白沙黎族自治县');
INSERT INTO `system_areabase` VALUES ('2520', '90', '460000', '469031', '0', '0', '1', '昌江黎族自治县');
INSERT INTO `system_areabase` VALUES ('2521', '90', '460000', '469033', '0', '0', '1', '乐东黎族自治县');
INSERT INTO `system_areabase` VALUES ('2522', '90', '460000', '469034', '0', '0', '1', '陵水黎族自治县');
INSERT INTO `system_areabase` VALUES ('2523', '90', '460000', '469035', '0', '0', '1', '保亭黎族苗族自治县');
INSERT INTO `system_areabase` VALUES ('2524', '90', '460000', '469036', '0', '0', '1', '琼中黎族苗族自治县');
INSERT INTO `system_areabase` VALUES ('2525', '90', '460000', '469037', '0', '0', '1', '西沙群岛');
INSERT INTO `system_areabase` VALUES ('2526', '90', '460000', '469038', '0', '0', '1', '南沙群岛');
INSERT INTO `system_areabase` VALUES ('2527', '90', '460000', '469039', '0', '0', '1', '中沙群岛的岛礁及其海域');
INSERT INTO `system_areabase` VALUES ('2528', '90', '0', '500000', '0', '0', '1', '重庆');
INSERT INTO `system_areabase` VALUES ('2529', '90', '500000', '500100', '0', '0', '1', '重庆市');
INSERT INTO `system_areabase` VALUES ('2530', '90', '500100', '500101', '0', '0', '1', '万州区');
INSERT INTO `system_areabase` VALUES ('2531', '90', '500100', '500102', '0', '0', '1', '涪陵区');
INSERT INTO `system_areabase` VALUES ('2532', '90', '500100', '500103', '0', '0', '1', '渝中区');
INSERT INTO `system_areabase` VALUES ('2533', '90', '500100', '500104', '0', '0', '1', '大渡口区');
INSERT INTO `system_areabase` VALUES ('2534', '90', '500100', '500105', '0', '0', '1', '江北区');
INSERT INTO `system_areabase` VALUES ('2535', '90', '500100', '500106', '0', '0', '1', '沙坪坝区');
INSERT INTO `system_areabase` VALUES ('2536', '90', '500100', '500107', '0', '0', '1', '九龙坡区');
INSERT INTO `system_areabase` VALUES ('2537', '90', '500100', '500108', '0', '0', '1', '南岸区');
INSERT INTO `system_areabase` VALUES ('2538', '90', '500100', '500109', '0', '0', '1', '北碚区');
INSERT INTO `system_areabase` VALUES ('2539', '90', '500100', '500110', '0', '0', '1', '万盛区');
INSERT INTO `system_areabase` VALUES ('2540', '90', '500100', '500111', '0', '0', '1', '双桥区');
INSERT INTO `system_areabase` VALUES ('2541', '90', '500100', '500112', '0', '0', '1', '渝北区');
INSERT INTO `system_areabase` VALUES ('2542', '90', '500100', '500113', '0', '0', '1', '巴南区');
INSERT INTO `system_areabase` VALUES ('2543', '90', '500100', '500114', '0', '0', '1', '黔江区');
INSERT INTO `system_areabase` VALUES ('2544', '90', '500100', '500115', '0', '0', '1', '长寿区');
INSERT INTO `system_areabase` VALUES ('2545', '90', '500100', '500222', '0', '0', '1', '綦江县');
INSERT INTO `system_areabase` VALUES ('2546', '90', '500100', '500223', '0', '0', '1', '潼南县');
INSERT INTO `system_areabase` VALUES ('2547', '90', '500100', '500224', '0', '0', '1', '铜梁县');
INSERT INTO `system_areabase` VALUES ('2548', '90', '500100', '500225', '0', '0', '1', '大足县');
INSERT INTO `system_areabase` VALUES ('2549', '90', '500100', '500226', '0', '0', '1', '荣昌县');
INSERT INTO `system_areabase` VALUES ('2550', '90', '500100', '500227', '0', '0', '1', '璧山县');
INSERT INTO `system_areabase` VALUES ('2551', '90', '500100', '500228', '0', '0', '1', '梁平县');
INSERT INTO `system_areabase` VALUES ('2552', '90', '500100', '500229', '0', '0', '1', '城口县');
INSERT INTO `system_areabase` VALUES ('2553', '90', '500100', '500230', '0', '0', '1', '丰都县');
INSERT INTO `system_areabase` VALUES ('2554', '90', '500100', '500231', '0', '0', '1', '垫江县');
INSERT INTO `system_areabase` VALUES ('2555', '90', '500100', '500232', '0', '0', '1', '武隆县');
INSERT INTO `system_areabase` VALUES ('2556', '90', '500100', '500233', '0', '0', '1', '忠县');
INSERT INTO `system_areabase` VALUES ('2557', '90', '500100', '500234', '0', '0', '1', '开县');
INSERT INTO `system_areabase` VALUES ('2558', '90', '500100', '500235', '0', '0', '1', '云阳县');
INSERT INTO `system_areabase` VALUES ('2559', '90', '500100', '500236', '0', '0', '1', '奉节县');
INSERT INTO `system_areabase` VALUES ('2560', '90', '500100', '500237', '0', '0', '1', '巫山县');
INSERT INTO `system_areabase` VALUES ('2561', '90', '500100', '500238', '0', '0', '1', '巫溪县');
INSERT INTO `system_areabase` VALUES ('2562', '90', '500100', '500240', '0', '0', '1', '石柱土家族自治县');
INSERT INTO `system_areabase` VALUES ('2563', '90', '500100', '500241', '0', '0', '1', '秀山土家族苗族自治县');
INSERT INTO `system_areabase` VALUES ('2564', '90', '500100', '500242', '0', '0', '1', '酉阳土家族苗族自治县');
INSERT INTO `system_areabase` VALUES ('2565', '90', '500100', '500243', '0', '0', '1', '彭水苗族土家族自治县');
INSERT INTO `system_areabase` VALUES ('2566', '90', '500100', '500381', '0', '0', '1', '江津区');
INSERT INTO `system_areabase` VALUES ('2567', '90', '500100', '500382', '0', '0', '1', '合川区');
INSERT INTO `system_areabase` VALUES ('2568', '90', '500100', '500383', '0', '0', '1', '永川区');
INSERT INTO `system_areabase` VALUES ('2569', '90', '500100', '500384', '0', '0', '1', '南川区');
INSERT INTO `system_areabase` VALUES ('2570', '90', '500100', '500385', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2571', '90', '0', '510000', '0', '0', '1', '四川省');
INSERT INTO `system_areabase` VALUES ('2572', '90', '510000', '510100', '0', '0', '1', '成都市');
INSERT INTO `system_areabase` VALUES ('2573', '90', '510100', '510104', '0', '0', '1', '锦江区');
INSERT INTO `system_areabase` VALUES ('2574', '90', '510100', '510105', '0', '0', '1', '青羊区');
INSERT INTO `system_areabase` VALUES ('2575', '90', '510100', '510106', '0', '0', '1', '金牛区');
INSERT INTO `system_areabase` VALUES ('2576', '90', '510100', '510107', '0', '0', '1', '武侯区');
INSERT INTO `system_areabase` VALUES ('2577', '90', '510100', '510108', '0', '0', '1', '成华区');
INSERT INTO `system_areabase` VALUES ('2578', '90', '510100', '510112', '0', '0', '1', '龙泉驿区');
INSERT INTO `system_areabase` VALUES ('2579', '90', '510100', '510113', '0', '0', '1', '青白江区');
INSERT INTO `system_areabase` VALUES ('2580', '90', '510100', '510114', '0', '0', '1', '新都区');
INSERT INTO `system_areabase` VALUES ('2581', '90', '510100', '510115', '0', '0', '1', '温江区');
INSERT INTO `system_areabase` VALUES ('2582', '90', '510100', '510121', '0', '0', '1', '金堂县');
INSERT INTO `system_areabase` VALUES ('2583', '90', '510100', '510122', '0', '0', '1', '双流县');
INSERT INTO `system_areabase` VALUES ('2584', '90', '510100', '510124', '0', '0', '1', '郫县');
INSERT INTO `system_areabase` VALUES ('2585', '90', '510100', '510129', '0', '0', '1', '大邑县');
INSERT INTO `system_areabase` VALUES ('2586', '90', '510100', '510131', '0', '0', '1', '蒲江县');
INSERT INTO `system_areabase` VALUES ('2587', '90', '510100', '510132', '0', '0', '1', '新津县');
INSERT INTO `system_areabase` VALUES ('2588', '90', '510100', '510181', '0', '0', '1', '都江堰市');
INSERT INTO `system_areabase` VALUES ('2589', '90', '510100', '510182', '0', '0', '1', '彭州市');
INSERT INTO `system_areabase` VALUES ('2590', '90', '510100', '510183', '0', '0', '1', '邛崃市');
INSERT INTO `system_areabase` VALUES ('2591', '90', '510100', '510184', '0', '0', '1', '崇州市');
INSERT INTO `system_areabase` VALUES ('2592', '90', '510100', '510185', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2593', '90', '510000', '510300', '0', '0', '1', '自贡市');
INSERT INTO `system_areabase` VALUES ('2594', '90', '510300', '510302', '0', '0', '1', '自流井区');
INSERT INTO `system_areabase` VALUES ('2595', '90', '510300', '510303', '0', '0', '1', '贡井区');
INSERT INTO `system_areabase` VALUES ('2596', '90', '510300', '510304', '0', '0', '1', '大安区');
INSERT INTO `system_areabase` VALUES ('2597', '90', '510300', '510311', '0', '0', '1', '沿滩区');
INSERT INTO `system_areabase` VALUES ('2598', '90', '510300', '510321', '0', '0', '1', '荣县');
INSERT INTO `system_areabase` VALUES ('2599', '90', '510300', '510322', '0', '0', '1', '富顺县');
INSERT INTO `system_areabase` VALUES ('2600', '90', '510300', '510323', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2601', '90', '510000', '510400', '0', '0', '1', '攀枝花市');
INSERT INTO `system_areabase` VALUES ('2602', '90', '510400', '510402', '0', '0', '1', '东区');
INSERT INTO `system_areabase` VALUES ('2603', '90', '510400', '510403', '0', '0', '1', '西区');
INSERT INTO `system_areabase` VALUES ('2604', '90', '510400', '510411', '0', '0', '1', '仁和区');
INSERT INTO `system_areabase` VALUES ('2605', '90', '510400', '510421', '0', '0', '1', '米易县');
INSERT INTO `system_areabase` VALUES ('2606', '90', '510400', '510422', '0', '0', '1', '盐边县');
INSERT INTO `system_areabase` VALUES ('2607', '90', '510400', '510423', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2608', '90', '510000', '510500', '0', '0', '1', '泸州市');
INSERT INTO `system_areabase` VALUES ('2609', '90', '510500', '510502', '0', '0', '1', '江阳区');
INSERT INTO `system_areabase` VALUES ('2610', '90', '510500', '510503', '0', '0', '1', '纳溪区');
INSERT INTO `system_areabase` VALUES ('2611', '90', '510500', '510504', '0', '0', '1', '龙马潭区');
INSERT INTO `system_areabase` VALUES ('2612', '90', '510500', '510521', '0', '0', '1', '泸县');
INSERT INTO `system_areabase` VALUES ('2613', '90', '510500', '510522', '0', '0', '1', '合江县');
INSERT INTO `system_areabase` VALUES ('2614', '90', '510500', '510524', '0', '0', '1', '叙永县');
INSERT INTO `system_areabase` VALUES ('2615', '90', '510500', '510525', '0', '0', '1', '古蔺县');
INSERT INTO `system_areabase` VALUES ('2616', '90', '510500', '510526', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2617', '90', '510000', '510600', '0', '0', '1', '德阳市');
INSERT INTO `system_areabase` VALUES ('2618', '90', '510600', '510603', '0', '0', '1', '旌阳区');
INSERT INTO `system_areabase` VALUES ('2619', '90', '510600', '510623', '0', '0', '1', '中江县');
INSERT INTO `system_areabase` VALUES ('2620', '90', '510600', '510626', '0', '0', '1', '罗江县');
INSERT INTO `system_areabase` VALUES ('2621', '90', '510600', '510681', '0', '0', '1', '广汉市');
INSERT INTO `system_areabase` VALUES ('2622', '90', '510600', '510682', '0', '0', '1', '什邡市');
INSERT INTO `system_areabase` VALUES ('2623', '90', '510600', '510683', '0', '0', '1', '绵竹市');
INSERT INTO `system_areabase` VALUES ('2624', '90', '510600', '510684', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2625', '90', '510000', '510700', '0', '0', '1', '绵阳市');
INSERT INTO `system_areabase` VALUES ('2626', '90', '510700', '510703', '0', '0', '1', '涪城区');
INSERT INTO `system_areabase` VALUES ('2627', '90', '510700', '510704', '0', '0', '1', '游仙区');
INSERT INTO `system_areabase` VALUES ('2628', '90', '510700', '510722', '0', '0', '1', '三台县');
INSERT INTO `system_areabase` VALUES ('2629', '90', '510700', '510723', '0', '0', '1', '盐亭县');
INSERT INTO `system_areabase` VALUES ('2630', '90', '510700', '510724', '0', '0', '1', '安县');
INSERT INTO `system_areabase` VALUES ('2631', '90', '510700', '510725', '0', '0', '1', '梓潼县');
INSERT INTO `system_areabase` VALUES ('2632', '90', '510700', '510726', '0', '0', '1', '北川羌族自治县');
INSERT INTO `system_areabase` VALUES ('2633', '90', '510700', '510727', '0', '0', '1', '平武县');
INSERT INTO `system_areabase` VALUES ('2634', '90', '510700', '510751', '0', '0', '1', '高新区');
INSERT INTO `system_areabase` VALUES ('2635', '90', '510700', '510781', '0', '0', '1', '江油市');
INSERT INTO `system_areabase` VALUES ('2636', '90', '510700', '510782', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2637', '90', '510000', '510800', '0', '0', '1', '广元市');
INSERT INTO `system_areabase` VALUES ('2638', '90', '510800', '510802', '0', '0', '1', '利州区');
INSERT INTO `system_areabase` VALUES ('2639', '90', '510800', '510811', '0', '0', '1', '元坝区');
INSERT INTO `system_areabase` VALUES ('2640', '90', '510800', '510812', '0', '0', '1', '朝天区');
INSERT INTO `system_areabase` VALUES ('2641', '90', '510800', '510821', '0', '0', '1', '旺苍县');
INSERT INTO `system_areabase` VALUES ('2642', '90', '510800', '510822', '0', '0', '1', '青川县');
INSERT INTO `system_areabase` VALUES ('2643', '90', '510800', '510823', '0', '0', '1', '剑阁县');
INSERT INTO `system_areabase` VALUES ('2644', '90', '510800', '510824', '0', '0', '1', '苍溪县');
INSERT INTO `system_areabase` VALUES ('2645', '90', '510800', '510825', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2646', '90', '510000', '510900', '0', '0', '1', '遂宁市');
INSERT INTO `system_areabase` VALUES ('2647', '90', '510900', '510903', '0', '0', '1', '船山区');
INSERT INTO `system_areabase` VALUES ('2648', '90', '510900', '510904', '0', '0', '1', '安居区');
INSERT INTO `system_areabase` VALUES ('2649', '90', '510900', '510921', '0', '0', '1', '蓬溪县');
INSERT INTO `system_areabase` VALUES ('2650', '90', '510900', '510922', '0', '0', '1', '射洪县');
INSERT INTO `system_areabase` VALUES ('2651', '90', '510900', '510923', '0', '0', '1', '大英县');
INSERT INTO `system_areabase` VALUES ('2652', '90', '510900', '510924', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2653', '90', '510000', '511000', '0', '0', '1', '内江市');
INSERT INTO `system_areabase` VALUES ('2654', '90', '511000', '511002', '0', '0', '1', '市中区');
INSERT INTO `system_areabase` VALUES ('2655', '90', '511000', '511011', '0', '0', '1', '东兴区');
INSERT INTO `system_areabase` VALUES ('2656', '90', '511000', '511024', '0', '0', '1', '威远县');
INSERT INTO `system_areabase` VALUES ('2657', '90', '511000', '511025', '0', '0', '1', '资中县');
INSERT INTO `system_areabase` VALUES ('2658', '90', '511000', '511028', '0', '0', '1', '隆昌县');
INSERT INTO `system_areabase` VALUES ('2659', '90', '511000', '511029', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2660', '90', '510000', '511100', '0', '0', '1', '乐山市');
INSERT INTO `system_areabase` VALUES ('2661', '90', '511100', '511102', '0', '0', '1', '市中区');
INSERT INTO `system_areabase` VALUES ('2662', '90', '511100', '511111', '0', '0', '1', '沙湾区');
INSERT INTO `system_areabase` VALUES ('2663', '90', '511100', '511112', '0', '0', '1', '五通桥区');
INSERT INTO `system_areabase` VALUES ('2664', '90', '511100', '511113', '0', '0', '1', '金口河区');
INSERT INTO `system_areabase` VALUES ('2665', '90', '511100', '511123', '0', '0', '1', '犍为县');
INSERT INTO `system_areabase` VALUES ('2666', '90', '511100', '511124', '0', '0', '1', '井研县');
INSERT INTO `system_areabase` VALUES ('2667', '90', '511100', '511126', '0', '0', '1', '夹江县');
INSERT INTO `system_areabase` VALUES ('2668', '90', '511100', '511129', '0', '0', '1', '沐川县');
INSERT INTO `system_areabase` VALUES ('2669', '90', '511100', '511132', '0', '0', '1', '峨边彝族自治县');
INSERT INTO `system_areabase` VALUES ('2670', '90', '511100', '511133', '0', '0', '1', '马边彝族自治县');
INSERT INTO `system_areabase` VALUES ('2671', '90', '511100', '511181', '0', '0', '1', '峨眉山市');
INSERT INTO `system_areabase` VALUES ('2672', '90', '511100', '511182', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2673', '90', '510000', '511300', '0', '0', '1', '南充市');
INSERT INTO `system_areabase` VALUES ('2674', '90', '511300', '511302', '0', '0', '1', '顺庆区');
INSERT INTO `system_areabase` VALUES ('2675', '90', '511300', '511303', '0', '0', '1', '高坪区');
INSERT INTO `system_areabase` VALUES ('2676', '90', '511300', '511304', '0', '0', '1', '嘉陵区');
INSERT INTO `system_areabase` VALUES ('2677', '90', '511300', '511321', '0', '0', '1', '南部县');
INSERT INTO `system_areabase` VALUES ('2678', '90', '511300', '511322', '0', '0', '1', '营山县');
INSERT INTO `system_areabase` VALUES ('2679', '90', '511300', '511323', '0', '0', '1', '蓬安县');
INSERT INTO `system_areabase` VALUES ('2680', '90', '511300', '511324', '0', '0', '1', '仪陇县');
INSERT INTO `system_areabase` VALUES ('2681', '90', '511300', '511325', '0', '0', '1', '西充县');
INSERT INTO `system_areabase` VALUES ('2682', '90', '511300', '511381', '0', '0', '1', '阆中市');
INSERT INTO `system_areabase` VALUES ('2683', '90', '511300', '511382', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2684', '90', '510000', '511400', '0', '0', '1', '眉山市');
INSERT INTO `system_areabase` VALUES ('2685', '90', '511400', '511402', '0', '0', '1', '东坡区');
INSERT INTO `system_areabase` VALUES ('2686', '90', '511400', '511421', '0', '0', '1', '仁寿县');
INSERT INTO `system_areabase` VALUES ('2687', '90', '511400', '511422', '0', '0', '1', '彭山县');
INSERT INTO `system_areabase` VALUES ('2688', '90', '511400', '511423', '0', '0', '1', '洪雅县');
INSERT INTO `system_areabase` VALUES ('2689', '90', '511400', '511424', '0', '0', '1', '丹棱县');
INSERT INTO `system_areabase` VALUES ('2690', '90', '511400', '511425', '0', '0', '1', '青神县');
INSERT INTO `system_areabase` VALUES ('2691', '90', '511400', '511426', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2692', '90', '510000', '511500', '0', '0', '1', '宜宾市');
INSERT INTO `system_areabase` VALUES ('2693', '90', '511500', '511502', '0', '0', '1', '翠屏区');
INSERT INTO `system_areabase` VALUES ('2694', '90', '511500', '511521', '0', '0', '1', '宜宾县');
INSERT INTO `system_areabase` VALUES ('2695', '90', '511500', '511522', '0', '0', '1', '南溪县');
INSERT INTO `system_areabase` VALUES ('2696', '90', '511500', '511523', '0', '0', '1', '江安县');
INSERT INTO `system_areabase` VALUES ('2697', '90', '511500', '511524', '0', '0', '1', '长宁县');
INSERT INTO `system_areabase` VALUES ('2698', '90', '511500', '511525', '0', '0', '1', '高县');
INSERT INTO `system_areabase` VALUES ('2699', '90', '511500', '511526', '0', '0', '1', '珙县');
INSERT INTO `system_areabase` VALUES ('2700', '90', '511500', '511527', '0', '0', '1', '筠连县');
INSERT INTO `system_areabase` VALUES ('2701', '90', '511500', '511528', '0', '0', '1', '兴文县');
INSERT INTO `system_areabase` VALUES ('2702', '90', '511500', '511529', '0', '0', '1', '屏山县');
INSERT INTO `system_areabase` VALUES ('2703', '90', '511500', '511530', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2704', '90', '510000', '511600', '0', '0', '1', '广安市');
INSERT INTO `system_areabase` VALUES ('2705', '90', '511600', '511602', '0', '0', '1', '广安区');
INSERT INTO `system_areabase` VALUES ('2706', '90', '511600', '511621', '0', '0', '1', '岳池县');
INSERT INTO `system_areabase` VALUES ('2707', '90', '511600', '511622', '0', '0', '1', '武胜县');
INSERT INTO `system_areabase` VALUES ('2708', '90', '511600', '511623', '0', '0', '1', '邻水县');
INSERT INTO `system_areabase` VALUES ('2709', '90', '511600', '511681', '0', '0', '1', '华蓥市');
INSERT INTO `system_areabase` VALUES ('2710', '90', '511600', '511682', '0', '0', '1', '市辖区');
INSERT INTO `system_areabase` VALUES ('2711', '90', '511600', '511683', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2712', '90', '510000', '511700', '0', '0', '1', '达州市');
INSERT INTO `system_areabase` VALUES ('2713', '90', '511700', '511702', '0', '0', '1', '通川区');
INSERT INTO `system_areabase` VALUES ('2714', '90', '511700', '511721', '0', '0', '1', '达县');
INSERT INTO `system_areabase` VALUES ('2715', '90', '511700', '511722', '0', '0', '1', '宣汉县');
INSERT INTO `system_areabase` VALUES ('2716', '90', '511700', '511723', '0', '0', '1', '开江县');
INSERT INTO `system_areabase` VALUES ('2717', '90', '511700', '511724', '0', '0', '1', '大竹县');
INSERT INTO `system_areabase` VALUES ('2718', '90', '511700', '511725', '0', '0', '1', '渠县');
INSERT INTO `system_areabase` VALUES ('2719', '90', '511700', '511781', '0', '0', '1', '万源市');
INSERT INTO `system_areabase` VALUES ('2720', '90', '511700', '511782', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2721', '90', '510000', '511800', '0', '0', '1', '雅安市');
INSERT INTO `system_areabase` VALUES ('2722', '90', '511800', '511802', '0', '0', '1', '雨城区');
INSERT INTO `system_areabase` VALUES ('2723', '90', '511800', '511821', '0', '0', '1', '名山县');
INSERT INTO `system_areabase` VALUES ('2724', '90', '511800', '511822', '0', '0', '1', '荥经县');
INSERT INTO `system_areabase` VALUES ('2725', '90', '511800', '511823', '0', '0', '1', '汉源县');
INSERT INTO `system_areabase` VALUES ('2726', '90', '511800', '511824', '0', '0', '1', '石棉县');
INSERT INTO `system_areabase` VALUES ('2727', '90', '511800', '511825', '0', '0', '1', '天全县');
INSERT INTO `system_areabase` VALUES ('2728', '90', '511800', '511826', '0', '0', '1', '芦山县');
INSERT INTO `system_areabase` VALUES ('2729', '90', '511800', '511827', '0', '0', '1', '宝兴县');
INSERT INTO `system_areabase` VALUES ('2730', '90', '511800', '511828', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2731', '90', '510000', '511900', '0', '0', '1', '巴中市');
INSERT INTO `system_areabase` VALUES ('2732', '90', '511900', '511902', '0', '0', '1', '巴州区');
INSERT INTO `system_areabase` VALUES ('2733', '90', '511900', '511921', '0', '0', '1', '通江县');
INSERT INTO `system_areabase` VALUES ('2734', '90', '511900', '511922', '0', '0', '1', '南江县');
INSERT INTO `system_areabase` VALUES ('2735', '90', '511900', '511923', '0', '0', '1', '平昌县');
INSERT INTO `system_areabase` VALUES ('2736', '90', '511900', '511924', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2737', '90', '510000', '512000', '0', '0', '1', '资阳市');
INSERT INTO `system_areabase` VALUES ('2738', '90', '512000', '512002', '0', '0', '1', '雁江区');
INSERT INTO `system_areabase` VALUES ('2739', '90', '512000', '512021', '0', '0', '1', '安岳县');
INSERT INTO `system_areabase` VALUES ('2740', '90', '512000', '512022', '0', '0', '1', '乐至县');
INSERT INTO `system_areabase` VALUES ('2741', '90', '512000', '512081', '0', '0', '1', '简阳市');
INSERT INTO `system_areabase` VALUES ('2742', '90', '512000', '512082', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2743', '90', '510000', '513200', '0', '0', '1', '阿坝藏族羌族自治州');
INSERT INTO `system_areabase` VALUES ('2744', '90', '513200', '513221', '0', '0', '1', '汶川县');
INSERT INTO `system_areabase` VALUES ('2745', '90', '513200', '513222', '0', '0', '1', '理县');
INSERT INTO `system_areabase` VALUES ('2746', '90', '513200', '513223', '0', '0', '1', '茂县');
INSERT INTO `system_areabase` VALUES ('2747', '90', '513200', '513224', '0', '0', '1', '松潘县');
INSERT INTO `system_areabase` VALUES ('2748', '90', '513200', '513225', '0', '0', '1', '九寨沟县');
INSERT INTO `system_areabase` VALUES ('2749', '90', '513200', '513226', '0', '0', '1', '金川县');
INSERT INTO `system_areabase` VALUES ('2750', '90', '513200', '513227', '0', '0', '1', '小金县');
INSERT INTO `system_areabase` VALUES ('2751', '90', '513200', '513228', '0', '0', '1', '黑水县');
INSERT INTO `system_areabase` VALUES ('2752', '90', '513200', '513229', '0', '0', '1', '马尔康县');
INSERT INTO `system_areabase` VALUES ('2753', '90', '513200', '513230', '0', '0', '1', '壤塘县');
INSERT INTO `system_areabase` VALUES ('2754', '90', '513200', '513231', '0', '0', '1', '阿坝县');
INSERT INTO `system_areabase` VALUES ('2755', '90', '513200', '513232', '0', '0', '1', '若尔盖县');
INSERT INTO `system_areabase` VALUES ('2756', '90', '513200', '513233', '0', '0', '1', '红原县');
INSERT INTO `system_areabase` VALUES ('2757', '90', '513200', '513234', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2758', '90', '510000', '513300', '0', '0', '1', '甘孜藏族自治州');
INSERT INTO `system_areabase` VALUES ('2759', '90', '513300', '513321', '0', '0', '1', '康定县');
INSERT INTO `system_areabase` VALUES ('2760', '90', '513300', '513322', '0', '0', '1', '泸定县');
INSERT INTO `system_areabase` VALUES ('2761', '90', '513300', '513323', '0', '0', '1', '丹巴县');
INSERT INTO `system_areabase` VALUES ('2762', '90', '513300', '513324', '0', '0', '1', '九龙县');
INSERT INTO `system_areabase` VALUES ('2763', '90', '513300', '513325', '0', '0', '1', '雅江县');
INSERT INTO `system_areabase` VALUES ('2764', '90', '513300', '513326', '0', '0', '1', '道孚县');
INSERT INTO `system_areabase` VALUES ('2765', '90', '513300', '513327', '0', '0', '1', '炉霍县');
INSERT INTO `system_areabase` VALUES ('2766', '90', '513300', '513328', '0', '0', '1', '甘孜县');
INSERT INTO `system_areabase` VALUES ('2767', '90', '513300', '513329', '0', '0', '1', '新龙县');
INSERT INTO `system_areabase` VALUES ('2768', '90', '513300', '513330', '0', '0', '1', '德格县');
INSERT INTO `system_areabase` VALUES ('2769', '90', '513300', '513331', '0', '0', '1', '白玉县');
INSERT INTO `system_areabase` VALUES ('2770', '90', '513300', '513332', '0', '0', '1', '石渠县');
INSERT INTO `system_areabase` VALUES ('2771', '90', '513300', '513333', '0', '0', '1', '色达县');
INSERT INTO `system_areabase` VALUES ('2772', '90', '513300', '513334', '0', '0', '1', '理塘县');
INSERT INTO `system_areabase` VALUES ('2773', '90', '513300', '513335', '0', '0', '1', '巴塘县');
INSERT INTO `system_areabase` VALUES ('2774', '90', '513300', '513336', '0', '0', '1', '乡城县');
INSERT INTO `system_areabase` VALUES ('2775', '90', '513300', '513337', '0', '0', '1', '稻城县');
INSERT INTO `system_areabase` VALUES ('2776', '90', '513300', '513338', '0', '0', '1', '得荣县');
INSERT INTO `system_areabase` VALUES ('2777', '90', '513300', '513339', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2778', '90', '510000', '513400', '0', '0', '1', '凉山彝族自治州');
INSERT INTO `system_areabase` VALUES ('2779', '90', '513400', '513401', '0', '0', '1', '西昌市');
INSERT INTO `system_areabase` VALUES ('2780', '90', '513400', '513422', '0', '0', '1', '木里藏族自治县');
INSERT INTO `system_areabase` VALUES ('2781', '90', '513400', '513423', '0', '0', '1', '盐源县');
INSERT INTO `system_areabase` VALUES ('2782', '90', '513400', '513424', '0', '0', '1', '德昌县');
INSERT INTO `system_areabase` VALUES ('2783', '90', '513400', '513425', '0', '0', '1', '会理县');
INSERT INTO `system_areabase` VALUES ('2784', '90', '513400', '513426', '0', '0', '1', '会东县');
INSERT INTO `system_areabase` VALUES ('2785', '90', '513400', '513427', '0', '0', '1', '宁南县');
INSERT INTO `system_areabase` VALUES ('2786', '90', '513400', '513428', '0', '0', '1', '普格县');
INSERT INTO `system_areabase` VALUES ('2787', '90', '513400', '513429', '0', '0', '1', '布拖县');
INSERT INTO `system_areabase` VALUES ('2788', '90', '513400', '513430', '0', '0', '1', '金阳县');
INSERT INTO `system_areabase` VALUES ('2789', '90', '513400', '513431', '0', '0', '1', '昭觉县');
INSERT INTO `system_areabase` VALUES ('2790', '90', '513400', '513432', '0', '0', '1', '喜德县');
INSERT INTO `system_areabase` VALUES ('2791', '90', '513400', '513433', '0', '0', '1', '冕宁县');
INSERT INTO `system_areabase` VALUES ('2792', '90', '513400', '513434', '0', '0', '1', '越西县');
INSERT INTO `system_areabase` VALUES ('2793', '90', '513400', '513435', '0', '0', '1', '甘洛县');
INSERT INTO `system_areabase` VALUES ('2794', '90', '513400', '513436', '0', '0', '1', '美姑县');
INSERT INTO `system_areabase` VALUES ('2795', '90', '513400', '513437', '0', '0', '1', '雷波县');
INSERT INTO `system_areabase` VALUES ('2796', '90', '513400', '513438', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2797', '90', '0', '520000', '0', '0', '1', '贵州省');
INSERT INTO `system_areabase` VALUES ('2798', '90', '520000', '520100', '0', '0', '1', '贵阳市');
INSERT INTO `system_areabase` VALUES ('2799', '90', '520100', '520102', '0', '0', '1', '南明区');
INSERT INTO `system_areabase` VALUES ('2800', '90', '520100', '520103', '0', '0', '1', '云岩区');
INSERT INTO `system_areabase` VALUES ('2801', '90', '520100', '520111', '0', '0', '1', '花溪区');
INSERT INTO `system_areabase` VALUES ('2802', '90', '520100', '520112', '0', '0', '1', '乌当区');
INSERT INTO `system_areabase` VALUES ('2803', '90', '520100', '520113', '0', '0', '1', '白云区');
INSERT INTO `system_areabase` VALUES ('2804', '90', '520100', '520114', '0', '0', '1', '小河区');
INSERT INTO `system_areabase` VALUES ('2805', '90', '520100', '520121', '0', '0', '1', '开阳县');
INSERT INTO `system_areabase` VALUES ('2806', '90', '520100', '520122', '0', '0', '1', '息烽县');
INSERT INTO `system_areabase` VALUES ('2807', '90', '520100', '520123', '0', '0', '1', '修文县');
INSERT INTO `system_areabase` VALUES ('2808', '90', '520100', '520151', '0', '0', '1', '金阳开发区');
INSERT INTO `system_areabase` VALUES ('2809', '90', '520100', '520181', '0', '0', '1', '清镇市');
INSERT INTO `system_areabase` VALUES ('2810', '90', '520100', '520182', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2811', '90', '520000', '520200', '0', '0', '1', '六盘水市');
INSERT INTO `system_areabase` VALUES ('2812', '90', '520200', '520201', '0', '0', '1', '钟山区');
INSERT INTO `system_areabase` VALUES ('2813', '90', '520200', '520203', '0', '0', '1', '六枝特区');
INSERT INTO `system_areabase` VALUES ('2814', '90', '520200', '520221', '0', '0', '1', '水城县');
INSERT INTO `system_areabase` VALUES ('2815', '90', '520200', '520222', '0', '0', '1', '盘县');
INSERT INTO `system_areabase` VALUES ('2816', '90', '520200', '520223', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2817', '90', '520000', '520300', '0', '0', '1', '遵义市');
INSERT INTO `system_areabase` VALUES ('2818', '90', '520300', '520302', '0', '0', '1', '红花岗区');
INSERT INTO `system_areabase` VALUES ('2819', '90', '520300', '520303', '0', '0', '1', '汇川区');
INSERT INTO `system_areabase` VALUES ('2820', '90', '520300', '520321', '0', '0', '1', '遵义县');
INSERT INTO `system_areabase` VALUES ('2821', '90', '520300', '520322', '0', '0', '1', '桐梓县');
INSERT INTO `system_areabase` VALUES ('2822', '90', '520300', '520323', '0', '0', '1', '绥阳县');
INSERT INTO `system_areabase` VALUES ('2823', '90', '520300', '520324', '0', '0', '1', '正安县');
INSERT INTO `system_areabase` VALUES ('2824', '90', '520300', '520325', '0', '0', '1', '道真仡佬族苗族自治县');
INSERT INTO `system_areabase` VALUES ('2825', '90', '520300', '520326', '0', '0', '1', '务川仡佬族苗族自治县');
INSERT INTO `system_areabase` VALUES ('2826', '90', '520300', '520327', '0', '0', '1', '凤冈县');
INSERT INTO `system_areabase` VALUES ('2827', '90', '520300', '520328', '0', '0', '1', '湄潭县');
INSERT INTO `system_areabase` VALUES ('2828', '90', '520300', '520329', '0', '0', '1', '余庆县');
INSERT INTO `system_areabase` VALUES ('2829', '90', '520300', '520330', '0', '0', '1', '习水县');
INSERT INTO `system_areabase` VALUES ('2830', '90', '520300', '520381', '0', '0', '1', '赤水市');
INSERT INTO `system_areabase` VALUES ('2831', '90', '520300', '520382', '0', '0', '1', '仁怀市');
INSERT INTO `system_areabase` VALUES ('2832', '90', '520300', '520383', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2833', '90', '520000', '520400', '0', '0', '1', '安顺市');
INSERT INTO `system_areabase` VALUES ('2834', '90', '520400', '520402', '0', '0', '1', '西秀区');
INSERT INTO `system_areabase` VALUES ('2835', '90', '520400', '520421', '0', '0', '1', '平坝县');
INSERT INTO `system_areabase` VALUES ('2836', '90', '520400', '520422', '0', '0', '1', '普定县');
INSERT INTO `system_areabase` VALUES ('2837', '90', '520400', '520423', '0', '0', '1', '镇宁布依族苗族自治县');
INSERT INTO `system_areabase` VALUES ('2838', '90', '520400', '520424', '0', '0', '1', '关岭布依族苗族自治县');
INSERT INTO `system_areabase` VALUES ('2839', '90', '520400', '520425', '0', '0', '1', '紫云苗族布依族自治县');
INSERT INTO `system_areabase` VALUES ('2840', '90', '520400', '520426', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2841', '90', '520000', '522200', '0', '0', '1', '铜仁地区');
INSERT INTO `system_areabase` VALUES ('2842', '90', '522200', '522201', '0', '0', '1', '铜仁市');
INSERT INTO `system_areabase` VALUES ('2843', '90', '522200', '522222', '0', '0', '1', '江口县');
INSERT INTO `system_areabase` VALUES ('2844', '90', '522200', '522223', '0', '0', '1', '玉屏侗族自治县');
INSERT INTO `system_areabase` VALUES ('2845', '90', '522200', '522224', '0', '0', '1', '石阡县');
INSERT INTO `system_areabase` VALUES ('2846', '90', '522200', '522225', '0', '0', '1', '思南县');
INSERT INTO `system_areabase` VALUES ('2847', '90', '522200', '522226', '0', '0', '1', '印江土家族苗族自治县');
INSERT INTO `system_areabase` VALUES ('2848', '90', '522200', '522227', '0', '0', '1', '德江县');
INSERT INTO `system_areabase` VALUES ('2849', '90', '522200', '522228', '0', '0', '1', '沿河土家族自治县');
INSERT INTO `system_areabase` VALUES ('2850', '90', '522200', '522229', '0', '0', '1', '松桃苗族自治县');
INSERT INTO `system_areabase` VALUES ('2851', '90', '522200', '522230', '0', '0', '1', '万山特区');
INSERT INTO `system_areabase` VALUES ('2852', '90', '522200', '522231', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2853', '90', '520000', '522300', '0', '0', '1', '黔西南布依族苗族自治州');
INSERT INTO `system_areabase` VALUES ('2854', '90', '522300', '522301', '0', '0', '1', '兴义市');
INSERT INTO `system_areabase` VALUES ('2855', '90', '522300', '522322', '0', '0', '1', '兴仁县');
INSERT INTO `system_areabase` VALUES ('2856', '90', '522300', '522323', '0', '0', '1', '普安县');
INSERT INTO `system_areabase` VALUES ('2857', '90', '522300', '522324', '0', '0', '1', '晴隆县');
INSERT INTO `system_areabase` VALUES ('2858', '90', '522300', '522325', '0', '0', '1', '贞丰县');
INSERT INTO `system_areabase` VALUES ('2859', '90', '522300', '522326', '0', '0', '1', '望谟县');
INSERT INTO `system_areabase` VALUES ('2860', '90', '522300', '522327', '0', '0', '1', '册亨县');
INSERT INTO `system_areabase` VALUES ('2861', '90', '522300', '522328', '0', '0', '1', '安龙县');
INSERT INTO `system_areabase` VALUES ('2862', '90', '522300', '522329', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2863', '90', '520000', '522400', '0', '0', '1', '毕节地区');
INSERT INTO `system_areabase` VALUES ('2864', '90', '522400', '522401', '0', '0', '1', '毕节市');
INSERT INTO `system_areabase` VALUES ('2865', '90', '522400', '522422', '0', '0', '1', '大方县');
INSERT INTO `system_areabase` VALUES ('2866', '90', '522400', '522423', '0', '0', '1', '黔西县');
INSERT INTO `system_areabase` VALUES ('2867', '90', '522400', '522424', '0', '0', '1', '金沙县');
INSERT INTO `system_areabase` VALUES ('2868', '90', '522400', '522425', '0', '0', '1', '织金县');
INSERT INTO `system_areabase` VALUES ('2869', '90', '522400', '522426', '0', '0', '1', '纳雍县');
INSERT INTO `system_areabase` VALUES ('2870', '90', '522400', '522427', '0', '0', '1', '威宁彝族回族苗族自治县');
INSERT INTO `system_areabase` VALUES ('2871', '90', '522400', '522428', '0', '0', '1', '赫章县');
INSERT INTO `system_areabase` VALUES ('2872', '90', '522400', '522429', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2873', '90', '520000', '522600', '0', '0', '1', '黔东南苗族侗族自治州');
INSERT INTO `system_areabase` VALUES ('2874', '90', '522600', '522601', '0', '0', '1', '凯里市');
INSERT INTO `system_areabase` VALUES ('2875', '90', '522600', '522622', '0', '0', '1', '黄平县');
INSERT INTO `system_areabase` VALUES ('2876', '90', '522600', '522623', '0', '0', '1', '施秉县');
INSERT INTO `system_areabase` VALUES ('2877', '90', '522600', '522624', '0', '0', '1', '三穗县');
INSERT INTO `system_areabase` VALUES ('2878', '90', '522600', '522625', '0', '0', '1', '镇远县');
INSERT INTO `system_areabase` VALUES ('2879', '90', '522600', '522626', '0', '0', '1', '岑巩县');
INSERT INTO `system_areabase` VALUES ('2880', '90', '522600', '522627', '0', '0', '1', '天柱县');
INSERT INTO `system_areabase` VALUES ('2881', '90', '522600', '522628', '0', '0', '1', '锦屏县');
INSERT INTO `system_areabase` VALUES ('2882', '90', '522600', '522629', '0', '0', '1', '剑河县');
INSERT INTO `system_areabase` VALUES ('2883', '90', '522600', '522630', '0', '0', '1', '台江县');
INSERT INTO `system_areabase` VALUES ('2884', '90', '522600', '522631', '0', '0', '1', '黎平县');
INSERT INTO `system_areabase` VALUES ('2885', '90', '522600', '522632', '0', '0', '1', '榕江县');
INSERT INTO `system_areabase` VALUES ('2886', '90', '522600', '522633', '0', '0', '1', '从江县');
INSERT INTO `system_areabase` VALUES ('2887', '90', '522600', '522634', '0', '0', '1', '雷山县');
INSERT INTO `system_areabase` VALUES ('2888', '90', '522600', '522635', '0', '0', '1', '麻江县');
INSERT INTO `system_areabase` VALUES ('2889', '90', '522600', '522636', '0', '0', '1', '丹寨县');
INSERT INTO `system_areabase` VALUES ('2890', '90', '522600', '522637', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2891', '90', '520000', '522700', '0', '0', '1', '黔南布依族苗族自治州');
INSERT INTO `system_areabase` VALUES ('2892', '90', '522700', '522701', '0', '0', '1', '都匀市');
INSERT INTO `system_areabase` VALUES ('2893', '90', '522700', '522702', '0', '0', '1', '福泉市');
INSERT INTO `system_areabase` VALUES ('2894', '90', '522700', '522722', '0', '0', '1', '荔波县');
INSERT INTO `system_areabase` VALUES ('2895', '90', '522700', '522723', '0', '0', '1', '贵定县');
INSERT INTO `system_areabase` VALUES ('2896', '90', '522700', '522725', '0', '0', '1', '瓮安县');
INSERT INTO `system_areabase` VALUES ('2897', '90', '522700', '522726', '0', '0', '1', '独山县');
INSERT INTO `system_areabase` VALUES ('2898', '90', '522700', '522727', '0', '0', '1', '平塘县');
INSERT INTO `system_areabase` VALUES ('2899', '90', '522700', '522728', '0', '0', '1', '罗甸县');
INSERT INTO `system_areabase` VALUES ('2900', '90', '522700', '522729', '0', '0', '1', '长顺县');
INSERT INTO `system_areabase` VALUES ('2901', '90', '522700', '522730', '0', '0', '1', '龙里县');
INSERT INTO `system_areabase` VALUES ('2902', '90', '522700', '522731', '0', '0', '1', '惠水县');
INSERT INTO `system_areabase` VALUES ('2903', '90', '522700', '522732', '0', '0', '1', '三都水族自治县');
INSERT INTO `system_areabase` VALUES ('2904', '90', '522700', '522733', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2905', '90', '0', '530000', '0', '0', '1', '云南省');
INSERT INTO `system_areabase` VALUES ('2906', '90', '530000', '530100', '0', '0', '1', '昆明市');
INSERT INTO `system_areabase` VALUES ('2907', '90', '530100', '530102', '0', '0', '1', '五华区');
INSERT INTO `system_areabase` VALUES ('2908', '90', '530100', '530103', '0', '0', '1', '盘龙区');
INSERT INTO `system_areabase` VALUES ('2909', '90', '530100', '530111', '0', '0', '1', '官渡区');
INSERT INTO `system_areabase` VALUES ('2910', '90', '530100', '530112', '0', '0', '1', '西山区');
INSERT INTO `system_areabase` VALUES ('2911', '90', '530100', '530113', '0', '0', '1', '东川区');
INSERT INTO `system_areabase` VALUES ('2912', '90', '530100', '530121', '0', '0', '1', '呈贡县');
INSERT INTO `system_areabase` VALUES ('2913', '90', '530100', '530122', '0', '0', '1', '晋宁县');
INSERT INTO `system_areabase` VALUES ('2914', '90', '530100', '530124', '0', '0', '1', '富民县');
INSERT INTO `system_areabase` VALUES ('2915', '90', '530100', '530125', '0', '0', '1', '宜良县');
INSERT INTO `system_areabase` VALUES ('2916', '90', '530100', '530126', '0', '0', '1', '石林彝族自治县');
INSERT INTO `system_areabase` VALUES ('2917', '90', '530100', '530127', '0', '0', '1', '嵩明县');
INSERT INTO `system_areabase` VALUES ('2918', '90', '530100', '530128', '0', '0', '1', '禄劝彝族苗族自治县');
INSERT INTO `system_areabase` VALUES ('2919', '90', '530100', '530129', '0', '0', '1', '寻甸回族彝族自治县');
INSERT INTO `system_areabase` VALUES ('2920', '90', '530100', '530181', '0', '0', '1', '安宁市');
INSERT INTO `system_areabase` VALUES ('2921', '90', '530100', '530182', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2922', '90', '530000', '530300', '0', '0', '1', '曲靖市');
INSERT INTO `system_areabase` VALUES ('2923', '90', '530300', '530302', '0', '0', '1', '麒麟区');
INSERT INTO `system_areabase` VALUES ('2924', '90', '530300', '530321', '0', '0', '1', '马龙县');
INSERT INTO `system_areabase` VALUES ('2925', '90', '530300', '530322', '0', '0', '1', '陆良县');
INSERT INTO `system_areabase` VALUES ('2926', '90', '530300', '530323', '0', '0', '1', '师宗县');
INSERT INTO `system_areabase` VALUES ('2927', '90', '530300', '530324', '0', '0', '1', '罗平县');
INSERT INTO `system_areabase` VALUES ('2928', '90', '530300', '530325', '0', '0', '1', '富源县');
INSERT INTO `system_areabase` VALUES ('2929', '90', '530300', '530326', '0', '0', '1', '会泽县');
INSERT INTO `system_areabase` VALUES ('2930', '90', '530300', '530328', '0', '0', '1', '沾益县');
INSERT INTO `system_areabase` VALUES ('2931', '90', '530300', '530381', '0', '0', '1', '宣威市');
INSERT INTO `system_areabase` VALUES ('2932', '90', '530300', '530382', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2933', '90', '530000', '530400', '0', '0', '1', '玉溪市');
INSERT INTO `system_areabase` VALUES ('2934', '90', '530400', '530402', '0', '0', '1', '红塔区');
INSERT INTO `system_areabase` VALUES ('2935', '90', '530400', '530421', '0', '0', '1', '江川县');
INSERT INTO `system_areabase` VALUES ('2936', '90', '530400', '530422', '0', '0', '1', '澄江县');
INSERT INTO `system_areabase` VALUES ('2937', '90', '530400', '530423', '0', '0', '1', '通海县');
INSERT INTO `system_areabase` VALUES ('2938', '90', '530400', '530424', '0', '0', '1', '华宁县');
INSERT INTO `system_areabase` VALUES ('2939', '90', '530400', '530425', '0', '0', '1', '易门县');
INSERT INTO `system_areabase` VALUES ('2940', '90', '530400', '530426', '0', '0', '1', '峨山彝族自治县');
INSERT INTO `system_areabase` VALUES ('2941', '90', '530400', '530427', '0', '0', '1', '新平彝族傣族自治县');
INSERT INTO `system_areabase` VALUES ('2942', '90', '530400', '530428', '0', '0', '1', '元江哈尼族彝族傣族自治县');
INSERT INTO `system_areabase` VALUES ('2943', '90', '530400', '530429', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2944', '90', '530000', '530500', '0', '0', '1', '保山市');
INSERT INTO `system_areabase` VALUES ('2945', '90', '530500', '530502', '0', '0', '1', '隆阳区');
INSERT INTO `system_areabase` VALUES ('2946', '90', '530500', '530521', '0', '0', '1', '施甸县');
INSERT INTO `system_areabase` VALUES ('2947', '90', '530500', '530522', '0', '0', '1', '腾冲县');
INSERT INTO `system_areabase` VALUES ('2948', '90', '530500', '530523', '0', '0', '1', '龙陵县');
INSERT INTO `system_areabase` VALUES ('2949', '90', '530500', '530524', '0', '0', '1', '昌宁县');
INSERT INTO `system_areabase` VALUES ('2950', '90', '530500', '530525', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2951', '90', '530000', '530600', '0', '0', '1', '昭通市');
INSERT INTO `system_areabase` VALUES ('2952', '90', '530600', '530602', '0', '0', '1', '昭阳区');
INSERT INTO `system_areabase` VALUES ('2953', '90', '530600', '530621', '0', '0', '1', '鲁甸县');
INSERT INTO `system_areabase` VALUES ('2954', '90', '530600', '530622', '0', '0', '1', '巧家县');
INSERT INTO `system_areabase` VALUES ('2955', '90', '530600', '530623', '0', '0', '1', '盐津县');
INSERT INTO `system_areabase` VALUES ('2956', '90', '530600', '530624', '0', '0', '1', '大关县');
INSERT INTO `system_areabase` VALUES ('2957', '90', '530600', '530625', '0', '0', '1', '永善县');
INSERT INTO `system_areabase` VALUES ('2958', '90', '530600', '530626', '0', '0', '1', '绥江县');
INSERT INTO `system_areabase` VALUES ('2959', '90', '530600', '530627', '0', '0', '1', '镇雄县');
INSERT INTO `system_areabase` VALUES ('2960', '90', '530600', '530628', '0', '0', '1', '彝良县');
INSERT INTO `system_areabase` VALUES ('2961', '90', '530600', '530629', '0', '0', '1', '威信县');
INSERT INTO `system_areabase` VALUES ('2962', '90', '530600', '530630', '0', '0', '1', '水富县');
INSERT INTO `system_areabase` VALUES ('2963', '90', '530600', '530631', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2964', '90', '530000', '530700', '0', '0', '1', '丽江市');
INSERT INTO `system_areabase` VALUES ('2965', '90', '530700', '530702', '0', '0', '1', '古城区');
INSERT INTO `system_areabase` VALUES ('2966', '90', '530700', '530721', '0', '0', '1', '玉龙纳西族自治县');
INSERT INTO `system_areabase` VALUES ('2967', '90', '530700', '530722', '0', '0', '1', '永胜县');
INSERT INTO `system_areabase` VALUES ('2968', '90', '530700', '530723', '0', '0', '1', '华坪县');
INSERT INTO `system_areabase` VALUES ('2969', '90', '530700', '530724', '0', '0', '1', '宁蒗彝族自治县');
INSERT INTO `system_areabase` VALUES ('2970', '90', '530700', '530725', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2971', '90', '530000', '530800', '0', '0', '1', '普洱市');
INSERT INTO `system_areabase` VALUES ('2972', '90', '530800', '530802', '0', '0', '1', '思茅区');
INSERT INTO `system_areabase` VALUES ('2973', '90', '530800', '530821', '0', '0', '1', '宁洱哈尼族彝族自治县');
INSERT INTO `system_areabase` VALUES ('2974', '90', '530800', '530822', '0', '0', '1', '墨江哈尼族自治县');
INSERT INTO `system_areabase` VALUES ('2975', '90', '530800', '530823', '0', '0', '1', '景东彝族自治县');
INSERT INTO `system_areabase` VALUES ('2976', '90', '530800', '530824', '0', '0', '1', '景谷傣族彝族自治县');
INSERT INTO `system_areabase` VALUES ('2977', '90', '530800', '530825', '0', '0', '1', '镇沅彝族哈尼族拉祜族自治县');
INSERT INTO `system_areabase` VALUES ('2978', '90', '530800', '530826', '0', '0', '1', '江城哈尼族彝族自治县');
INSERT INTO `system_areabase` VALUES ('2979', '90', '530800', '530827', '0', '0', '1', '孟连傣族拉祜族佤族自治县');
INSERT INTO `system_areabase` VALUES ('2980', '90', '530800', '530828', '0', '0', '1', '澜沧拉祜族自治县');
INSERT INTO `system_areabase` VALUES ('2981', '90', '530800', '530829', '0', '0', '1', '西盟佤族自治县');
INSERT INTO `system_areabase` VALUES ('2982', '90', '530800', '530830', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2983', '90', '530000', '530900', '0', '0', '1', '临沧市');
INSERT INTO `system_areabase` VALUES ('2984', '90', '530900', '530902', '0', '0', '1', '临翔区');
INSERT INTO `system_areabase` VALUES ('2985', '90', '530900', '530921', '0', '0', '1', '凤庆县');
INSERT INTO `system_areabase` VALUES ('2986', '90', '530900', '530922', '0', '0', '1', '云县');
INSERT INTO `system_areabase` VALUES ('2987', '90', '530900', '530923', '0', '0', '1', '永德县');
INSERT INTO `system_areabase` VALUES ('2988', '90', '530900', '530924', '0', '0', '1', '镇康县');
INSERT INTO `system_areabase` VALUES ('2989', '90', '530900', '530925', '0', '0', '1', '双江拉祜族佤族布朗族傣族自治县');
INSERT INTO `system_areabase` VALUES ('2990', '90', '530900', '530926', '0', '0', '1', '耿马傣族佤族自治县');
INSERT INTO `system_areabase` VALUES ('2991', '90', '530900', '530927', '0', '0', '1', '沧源佤族自治县');
INSERT INTO `system_areabase` VALUES ('2992', '90', '530900', '530928', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('2993', '90', '530000', '532300', '0', '0', '1', '楚雄彝族自治州');
INSERT INTO `system_areabase` VALUES ('2994', '90', '532300', '532301', '0', '0', '1', '楚雄市');
INSERT INTO `system_areabase` VALUES ('2995', '90', '532300', '532322', '0', '0', '1', '双柏县');
INSERT INTO `system_areabase` VALUES ('2996', '90', '532300', '532323', '0', '0', '1', '牟定县');
INSERT INTO `system_areabase` VALUES ('2997', '90', '532300', '532324', '0', '0', '1', '南华县');
INSERT INTO `system_areabase` VALUES ('2998', '90', '532300', '532325', '0', '0', '1', '姚安县');
INSERT INTO `system_areabase` VALUES ('2999', '90', '532300', '532326', '0', '0', '1', '大姚县');
INSERT INTO `system_areabase` VALUES ('3000', '90', '532300', '532327', '0', '0', '1', '永仁县');
INSERT INTO `system_areabase` VALUES ('3001', '90', '532300', '532328', '0', '0', '1', '元谋县');
INSERT INTO `system_areabase` VALUES ('3002', '90', '532300', '532329', '0', '0', '1', '武定县');
INSERT INTO `system_areabase` VALUES ('3003', '90', '532300', '532331', '0', '0', '1', '禄丰县');
INSERT INTO `system_areabase` VALUES ('3004', '90', '532300', '532332', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3005', '90', '530000', '532500', '0', '0', '1', '红河哈尼族彝族自治州');
INSERT INTO `system_areabase` VALUES ('3006', '90', '532500', '532501', '0', '0', '1', '个旧市');
INSERT INTO `system_areabase` VALUES ('3007', '90', '532500', '532502', '0', '0', '1', '开远市');
INSERT INTO `system_areabase` VALUES ('3008', '90', '532500', '532522', '0', '0', '1', '蒙自县');
INSERT INTO `system_areabase` VALUES ('3009', '90', '532500', '532523', '0', '0', '1', '屏边苗族自治县');
INSERT INTO `system_areabase` VALUES ('3010', '90', '532500', '532524', '0', '0', '1', '建水县');
INSERT INTO `system_areabase` VALUES ('3011', '90', '532500', '532525', '0', '0', '1', '石屏县');
INSERT INTO `system_areabase` VALUES ('3012', '90', '532500', '532526', '0', '0', '1', '弥勒县');
INSERT INTO `system_areabase` VALUES ('3013', '90', '532500', '532527', '0', '0', '1', '泸西县');
INSERT INTO `system_areabase` VALUES ('3014', '90', '532500', '532528', '0', '0', '1', '元阳县');
INSERT INTO `system_areabase` VALUES ('3015', '90', '532500', '532529', '0', '0', '1', '红河县');
INSERT INTO `system_areabase` VALUES ('3016', '90', '532500', '532530', '0', '0', '1', '金平苗族瑶族傣族自治县');
INSERT INTO `system_areabase` VALUES ('3017', '90', '532500', '532531', '0', '0', '1', '绿春县');
INSERT INTO `system_areabase` VALUES ('3018', '90', '532500', '532532', '0', '0', '1', '河口瑶族自治县');
INSERT INTO `system_areabase` VALUES ('3019', '90', '532500', '532533', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3020', '90', '530000', '532600', '0', '0', '1', '文山壮族苗族自治州');
INSERT INTO `system_areabase` VALUES ('3021', '90', '532600', '532621', '0', '0', '1', '文山县');
INSERT INTO `system_areabase` VALUES ('3022', '90', '532600', '532622', '0', '0', '1', '砚山县');
INSERT INTO `system_areabase` VALUES ('3023', '90', '532600', '532623', '0', '0', '1', '西畴县');
INSERT INTO `system_areabase` VALUES ('3024', '90', '532600', '532624', '0', '0', '1', '麻栗坡县');
INSERT INTO `system_areabase` VALUES ('3025', '90', '532600', '532625', '0', '0', '1', '马关县');
INSERT INTO `system_areabase` VALUES ('3026', '90', '532600', '532626', '0', '0', '1', '丘北县');
INSERT INTO `system_areabase` VALUES ('3027', '90', '532600', '532627', '0', '0', '1', '广南县');
INSERT INTO `system_areabase` VALUES ('3028', '90', '532600', '532628', '0', '0', '1', '富宁县');
INSERT INTO `system_areabase` VALUES ('3029', '90', '532600', '532629', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3030', '90', '530000', '532800', '0', '0', '1', '西双版纳傣族自治州');
INSERT INTO `system_areabase` VALUES ('3031', '90', '532800', '532801', '0', '0', '1', '景洪市');
INSERT INTO `system_areabase` VALUES ('3032', '90', '532800', '532822', '0', '0', '1', '勐海县');
INSERT INTO `system_areabase` VALUES ('3033', '90', '532800', '532823', '0', '0', '1', '勐腊县');
INSERT INTO `system_areabase` VALUES ('3034', '90', '532800', '532824', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3035', '90', '530000', '532900', '0', '0', '1', '大理白族自治州');
INSERT INTO `system_areabase` VALUES ('3036', '90', '532900', '532901', '0', '0', '1', '大理市');
INSERT INTO `system_areabase` VALUES ('3037', '90', '532900', '532922', '0', '0', '1', '漾濞彝族自治县');
INSERT INTO `system_areabase` VALUES ('3038', '90', '532900', '532923', '0', '0', '1', '祥云县');
INSERT INTO `system_areabase` VALUES ('3039', '90', '532900', '532924', '0', '0', '1', '宾川县');
INSERT INTO `system_areabase` VALUES ('3040', '90', '532900', '532925', '0', '0', '1', '弥渡县');
INSERT INTO `system_areabase` VALUES ('3041', '90', '532900', '532926', '0', '0', '1', '南涧彝族自治县');
INSERT INTO `system_areabase` VALUES ('3042', '90', '532900', '532927', '0', '0', '1', '巍山彝族回族自治县');
INSERT INTO `system_areabase` VALUES ('3043', '90', '532900', '532928', '0', '0', '1', '永平县');
INSERT INTO `system_areabase` VALUES ('3044', '90', '532900', '532929', '0', '0', '1', '云龙县');
INSERT INTO `system_areabase` VALUES ('3045', '90', '532900', '532930', '0', '0', '1', '洱源县');
INSERT INTO `system_areabase` VALUES ('3046', '90', '532900', '532931', '0', '0', '1', '剑川县');
INSERT INTO `system_areabase` VALUES ('3047', '90', '532900', '532932', '0', '0', '1', '鹤庆县');
INSERT INTO `system_areabase` VALUES ('3048', '90', '532900', '532933', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3049', '90', '530000', '533100', '0', '0', '1', '德宏傣族景颇族自治州');
INSERT INTO `system_areabase` VALUES ('3050', '90', '533100', '533102', '0', '0', '1', '瑞丽市');
INSERT INTO `system_areabase` VALUES ('3051', '90', '533100', '533103', '0', '0', '1', '潞西市');
INSERT INTO `system_areabase` VALUES ('3052', '90', '533100', '533122', '0', '0', '1', '梁河县');
INSERT INTO `system_areabase` VALUES ('3053', '90', '533100', '533123', '0', '0', '1', '盈江县');
INSERT INTO `system_areabase` VALUES ('3054', '90', '533100', '533124', '0', '0', '1', '陇川县');
INSERT INTO `system_areabase` VALUES ('3055', '90', '533100', '533125', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3056', '90', '530000', '533300', '0', '0', '1', '怒江傈僳族自治州');
INSERT INTO `system_areabase` VALUES ('3057', '90', '533300', '533321', '0', '0', '1', '泸水县');
INSERT INTO `system_areabase` VALUES ('3058', '90', '533300', '533323', '0', '0', '1', '福贡县');
INSERT INTO `system_areabase` VALUES ('3059', '90', '533300', '533324', '0', '0', '1', '贡山独龙族怒族自治县');
INSERT INTO `system_areabase` VALUES ('3060', '90', '533300', '533325', '0', '0', '1', '兰坪白族普米族自治县');
INSERT INTO `system_areabase` VALUES ('3061', '90', '533300', '533326', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3062', '90', '530000', '533400', '0', '0', '1', '迪庆藏族自治州');
INSERT INTO `system_areabase` VALUES ('3063', '90', '533400', '533421', '0', '0', '1', '香格里拉县');
INSERT INTO `system_areabase` VALUES ('3064', '90', '533400', '533422', '0', '0', '1', '德钦县');
INSERT INTO `system_areabase` VALUES ('3065', '90', '533400', '533423', '0', '0', '1', '维西傈僳族自治县');
INSERT INTO `system_areabase` VALUES ('3066', '90', '533400', '533424', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3067', '90', '0', '540000', '0', '0', '1', '西藏自治区');
INSERT INTO `system_areabase` VALUES ('3068', '90', '540000', '540100', '0', '0', '1', '拉萨市');
INSERT INTO `system_areabase` VALUES ('3069', '90', '540100', '540102', '0', '0', '1', '城关区');
INSERT INTO `system_areabase` VALUES ('3070', '90', '540100', '540121', '0', '0', '1', '林周县');
INSERT INTO `system_areabase` VALUES ('3071', '90', '540100', '540122', '0', '0', '1', '当雄县');
INSERT INTO `system_areabase` VALUES ('3072', '90', '540100', '540123', '0', '0', '1', '尼木县');
INSERT INTO `system_areabase` VALUES ('3073', '90', '540100', '540124', '0', '0', '1', '曲水县');
INSERT INTO `system_areabase` VALUES ('3074', '90', '540100', '540125', '0', '0', '1', '堆龙德庆县');
INSERT INTO `system_areabase` VALUES ('3075', '90', '540100', '540126', '0', '0', '1', '达孜县');
INSERT INTO `system_areabase` VALUES ('3076', '90', '540100', '540127', '0', '0', '1', '墨竹工卡县');
INSERT INTO `system_areabase` VALUES ('3077', '90', '540100', '540128', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3078', '90', '540000', '542100', '0', '0', '1', '昌都地区');
INSERT INTO `system_areabase` VALUES ('3079', '90', '542100', '542121', '0', '0', '1', '昌都县');
INSERT INTO `system_areabase` VALUES ('3080', '90', '542100', '542122', '0', '0', '1', '江达县');
INSERT INTO `system_areabase` VALUES ('3081', '90', '542100', '542123', '0', '0', '1', '贡觉县');
INSERT INTO `system_areabase` VALUES ('3082', '90', '542100', '542124', '0', '0', '1', '类乌齐县');
INSERT INTO `system_areabase` VALUES ('3083', '90', '542100', '542125', '0', '0', '1', '丁青县');
INSERT INTO `system_areabase` VALUES ('3084', '90', '542100', '542126', '0', '0', '1', '察雅县');
INSERT INTO `system_areabase` VALUES ('3085', '90', '542100', '542127', '0', '0', '1', '八宿县');
INSERT INTO `system_areabase` VALUES ('3086', '90', '542100', '542128', '0', '0', '1', '左贡县');
INSERT INTO `system_areabase` VALUES ('3087', '90', '542100', '542129', '0', '0', '1', '芒康县');
INSERT INTO `system_areabase` VALUES ('3088', '90', '542100', '542132', '0', '0', '1', '洛隆县');
INSERT INTO `system_areabase` VALUES ('3089', '90', '542100', '542133', '0', '0', '1', '边坝县');
INSERT INTO `system_areabase` VALUES ('3090', '90', '542100', '542134', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3091', '90', '540000', '542200', '0', '0', '1', '山南地区');
INSERT INTO `system_areabase` VALUES ('3092', '90', '542200', '542221', '0', '0', '1', '乃东县');
INSERT INTO `system_areabase` VALUES ('3093', '90', '542200', '542222', '0', '0', '1', '扎囊县');
INSERT INTO `system_areabase` VALUES ('3094', '90', '542200', '542223', '0', '0', '1', '贡嘎县');
INSERT INTO `system_areabase` VALUES ('3095', '90', '542200', '542224', '0', '0', '1', '桑日县');
INSERT INTO `system_areabase` VALUES ('3096', '90', '542200', '542225', '0', '0', '1', '琼结县');
INSERT INTO `system_areabase` VALUES ('3097', '90', '542200', '542226', '0', '0', '1', '曲松县');
INSERT INTO `system_areabase` VALUES ('3098', '90', '542200', '542227', '0', '0', '1', '措美县');
INSERT INTO `system_areabase` VALUES ('3099', '90', '542200', '542228', '0', '0', '1', '洛扎县');
INSERT INTO `system_areabase` VALUES ('3100', '90', '542200', '542229', '0', '0', '1', '加查县');
INSERT INTO `system_areabase` VALUES ('3101', '90', '542200', '542231', '0', '0', '1', '隆子县');
INSERT INTO `system_areabase` VALUES ('3102', '90', '542200', '542232', '0', '0', '1', '错那县');
INSERT INTO `system_areabase` VALUES ('3103', '90', '542200', '542233', '0', '0', '1', '浪卡子县');
INSERT INTO `system_areabase` VALUES ('3104', '90', '542200', '542234', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3105', '90', '540000', '542300', '0', '0', '1', '日喀则地区');
INSERT INTO `system_areabase` VALUES ('3106', '90', '542300', '542301', '0', '0', '1', '日喀则市');
INSERT INTO `system_areabase` VALUES ('3107', '90', '542300', '542322', '0', '0', '1', '南木林县');
INSERT INTO `system_areabase` VALUES ('3108', '90', '542300', '542323', '0', '0', '1', '江孜县');
INSERT INTO `system_areabase` VALUES ('3109', '90', '542300', '542324', '0', '0', '1', '定日县');
INSERT INTO `system_areabase` VALUES ('3110', '90', '542300', '542325', '0', '0', '1', '萨迦县');
INSERT INTO `system_areabase` VALUES ('3111', '90', '542300', '542326', '0', '0', '1', '拉孜县');
INSERT INTO `system_areabase` VALUES ('3112', '90', '542300', '542327', '0', '0', '1', '昂仁县');
INSERT INTO `system_areabase` VALUES ('3113', '90', '542300', '542328', '0', '0', '1', '谢通门县');
INSERT INTO `system_areabase` VALUES ('3114', '90', '542300', '542329', '0', '0', '1', '白朗县');
INSERT INTO `system_areabase` VALUES ('3115', '90', '542300', '542330', '0', '0', '1', '仁布县');
INSERT INTO `system_areabase` VALUES ('3116', '90', '542300', '542331', '0', '0', '1', '康马县');
INSERT INTO `system_areabase` VALUES ('3117', '90', '542300', '542332', '0', '0', '1', '定结县');
INSERT INTO `system_areabase` VALUES ('3118', '90', '542300', '542333', '0', '0', '1', '仲巴县');
INSERT INTO `system_areabase` VALUES ('3119', '90', '542300', '542334', '0', '0', '1', '亚东县');
INSERT INTO `system_areabase` VALUES ('3120', '90', '542300', '542335', '0', '0', '1', '吉隆县');
INSERT INTO `system_areabase` VALUES ('3121', '90', '542300', '542336', '0', '0', '1', '聂拉木县');
INSERT INTO `system_areabase` VALUES ('3122', '90', '542300', '542337', '0', '0', '1', '萨嘎县');
INSERT INTO `system_areabase` VALUES ('3123', '90', '542300', '542338', '0', '0', '1', '岗巴县');
INSERT INTO `system_areabase` VALUES ('3124', '90', '542300', '542339', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3125', '90', '540000', '542400', '0', '0', '1', '那曲地区');
INSERT INTO `system_areabase` VALUES ('3126', '90', '542400', '542421', '0', '0', '1', '那曲县');
INSERT INTO `system_areabase` VALUES ('3127', '90', '542400', '542422', '0', '0', '1', '嘉黎县');
INSERT INTO `system_areabase` VALUES ('3128', '90', '542400', '542423', '0', '0', '1', '比如县');
INSERT INTO `system_areabase` VALUES ('3129', '90', '542400', '542424', '0', '0', '1', '聂荣县');
INSERT INTO `system_areabase` VALUES ('3130', '90', '542400', '542425', '0', '0', '1', '安多县');
INSERT INTO `system_areabase` VALUES ('3131', '90', '542400', '542426', '0', '0', '1', '申扎县');
INSERT INTO `system_areabase` VALUES ('3132', '90', '542400', '542427', '0', '0', '1', '索县');
INSERT INTO `system_areabase` VALUES ('3133', '90', '542400', '542428', '0', '0', '1', '班戈县');
INSERT INTO `system_areabase` VALUES ('3134', '90', '542400', '542429', '0', '0', '1', '巴青县');
INSERT INTO `system_areabase` VALUES ('3135', '90', '542400', '542430', '0', '0', '1', '尼玛县');
INSERT INTO `system_areabase` VALUES ('3136', '90', '542400', '542431', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3137', '90', '540000', '542500', '0', '0', '1', '阿里地区');
INSERT INTO `system_areabase` VALUES ('3138', '90', '542500', '542521', '0', '0', '1', '普兰县');
INSERT INTO `system_areabase` VALUES ('3139', '90', '542500', '542522', '0', '0', '1', '札达县');
INSERT INTO `system_areabase` VALUES ('3140', '90', '542500', '542523', '0', '0', '1', '噶尔县');
INSERT INTO `system_areabase` VALUES ('3141', '90', '542500', '542524', '0', '0', '1', '日土县');
INSERT INTO `system_areabase` VALUES ('3142', '90', '542500', '542525', '0', '0', '1', '革吉县');
INSERT INTO `system_areabase` VALUES ('3143', '90', '542500', '542526', '0', '0', '1', '改则县');
INSERT INTO `system_areabase` VALUES ('3144', '90', '542500', '542527', '0', '0', '1', '措勤县');
INSERT INTO `system_areabase` VALUES ('3145', '90', '542500', '542528', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3146', '90', '540000', '542600', '0', '0', '1', '林芝地区');
INSERT INTO `system_areabase` VALUES ('3147', '90', '542600', '542621', '0', '0', '1', '林芝县');
INSERT INTO `system_areabase` VALUES ('3148', '90', '542600', '542622', '0', '0', '1', '工布江达县');
INSERT INTO `system_areabase` VALUES ('3149', '90', '542600', '542623', '0', '0', '1', '米林县');
INSERT INTO `system_areabase` VALUES ('3150', '90', '542600', '542624', '0', '0', '1', '墨脱县');
INSERT INTO `system_areabase` VALUES ('3151', '90', '542600', '542625', '0', '0', '1', '波密县');
INSERT INTO `system_areabase` VALUES ('3152', '90', '542600', '542626', '0', '0', '1', '察隅县');
INSERT INTO `system_areabase` VALUES ('3153', '90', '542600', '542627', '0', '0', '1', '朗县');
INSERT INTO `system_areabase` VALUES ('3154', '90', '542600', '542628', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3155', '90', '0', '610000', '0', '0', '1', '陕西省');
INSERT INTO `system_areabase` VALUES ('3156', '90', '610000', '610100', '0', '0', '1', '西安市');
INSERT INTO `system_areabase` VALUES ('3157', '90', '610100', '610102', '0', '0', '1', '新城区');
INSERT INTO `system_areabase` VALUES ('3158', '90', '610100', '610103', '0', '0', '1', '碑林区');
INSERT INTO `system_areabase` VALUES ('3159', '90', '610100', '610104', '0', '0', '1', '莲湖区');
INSERT INTO `system_areabase` VALUES ('3160', '90', '610100', '610111', '0', '0', '1', '灞桥区');
INSERT INTO `system_areabase` VALUES ('3161', '90', '610100', '610112', '0', '0', '1', '未央区');
INSERT INTO `system_areabase` VALUES ('3162', '90', '610100', '610113', '0', '0', '1', '雁塔区');
INSERT INTO `system_areabase` VALUES ('3163', '90', '610100', '610114', '0', '0', '1', '阎良区');
INSERT INTO `system_areabase` VALUES ('3164', '90', '610100', '610115', '0', '0', '1', '临潼区');
INSERT INTO `system_areabase` VALUES ('3165', '90', '610100', '610116', '0', '0', '1', '长安区');
INSERT INTO `system_areabase` VALUES ('3166', '90', '610100', '610122', '0', '0', '1', '蓝田县');
INSERT INTO `system_areabase` VALUES ('3167', '90', '610100', '610124', '0', '0', '1', '周至县');
INSERT INTO `system_areabase` VALUES ('3168', '90', '610100', '610125', '0', '0', '1', '户县');
INSERT INTO `system_areabase` VALUES ('3169', '90', '610100', '610126', '0', '0', '1', '高陵县');
INSERT INTO `system_areabase` VALUES ('3170', '90', '610100', '610127', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3171', '90', '610000', '610200', '0', '0', '1', '铜川市');
INSERT INTO `system_areabase` VALUES ('3172', '90', '610200', '610202', '0', '0', '1', '王益区');
INSERT INTO `system_areabase` VALUES ('3173', '90', '610200', '610203', '0', '0', '1', '印台区');
INSERT INTO `system_areabase` VALUES ('3174', '90', '610200', '610204', '0', '0', '1', '耀州区');
INSERT INTO `system_areabase` VALUES ('3175', '90', '610200', '610222', '0', '0', '1', '宜君县');
INSERT INTO `system_areabase` VALUES ('3176', '90', '610200', '610223', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3177', '90', '610000', '610300', '0', '0', '1', '宝鸡市');
INSERT INTO `system_areabase` VALUES ('3178', '90', '610300', '610302', '0', '0', '1', '渭滨区');
INSERT INTO `system_areabase` VALUES ('3179', '90', '610300', '610303', '0', '0', '1', '金台区');
INSERT INTO `system_areabase` VALUES ('3180', '90', '610300', '610304', '0', '0', '1', '陈仓区');
INSERT INTO `system_areabase` VALUES ('3181', '90', '610300', '610322', '0', '0', '1', '凤翔县');
INSERT INTO `system_areabase` VALUES ('3182', '90', '610300', '610323', '0', '0', '1', '岐山县');
INSERT INTO `system_areabase` VALUES ('3183', '90', '610300', '610324', '0', '0', '1', '扶风县');
INSERT INTO `system_areabase` VALUES ('3184', '90', '610300', '610326', '0', '0', '1', '眉县');
INSERT INTO `system_areabase` VALUES ('3185', '90', '610300', '610327', '0', '0', '1', '陇县');
INSERT INTO `system_areabase` VALUES ('3186', '90', '610300', '610328', '0', '0', '1', '千阳县');
INSERT INTO `system_areabase` VALUES ('3187', '90', '610300', '610329', '0', '0', '1', '麟游县');
INSERT INTO `system_areabase` VALUES ('3188', '90', '610300', '610330', '0', '0', '1', '凤县');
INSERT INTO `system_areabase` VALUES ('3189', '90', '610300', '610331', '0', '0', '1', '太白县');
INSERT INTO `system_areabase` VALUES ('3190', '90', '610300', '610332', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3191', '90', '610000', '610400', '0', '0', '1', '咸阳市');
INSERT INTO `system_areabase` VALUES ('3192', '90', '610400', '610402', '0', '0', '1', '秦都区');
INSERT INTO `system_areabase` VALUES ('3193', '90', '610400', '610403', '0', '0', '1', '杨凌区');
INSERT INTO `system_areabase` VALUES ('3194', '90', '610400', '610404', '0', '0', '1', '渭城区');
INSERT INTO `system_areabase` VALUES ('3195', '90', '610400', '610422', '0', '0', '1', '三原县');
INSERT INTO `system_areabase` VALUES ('3196', '90', '610400', '610423', '0', '0', '1', '泾阳县');
INSERT INTO `system_areabase` VALUES ('3197', '90', '610400', '610424', '0', '0', '1', '乾县');
INSERT INTO `system_areabase` VALUES ('3198', '90', '610400', '610425', '0', '0', '1', '礼泉县');
INSERT INTO `system_areabase` VALUES ('3199', '90', '610400', '610426', '0', '0', '1', '永寿县');
INSERT INTO `system_areabase` VALUES ('3200', '90', '610400', '610427', '0', '0', '1', '彬县');
INSERT INTO `system_areabase` VALUES ('3201', '90', '610400', '610428', '0', '0', '1', '长武县');
INSERT INTO `system_areabase` VALUES ('3202', '90', '610400', '610429', '0', '0', '1', '旬邑县');
INSERT INTO `system_areabase` VALUES ('3203', '90', '610400', '610430', '0', '0', '1', '淳化县');
INSERT INTO `system_areabase` VALUES ('3204', '90', '610400', '610431', '0', '0', '1', '武功县');
INSERT INTO `system_areabase` VALUES ('3205', '90', '610400', '610481', '0', '0', '1', '兴平市');
INSERT INTO `system_areabase` VALUES ('3206', '90', '610400', '610482', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3207', '90', '610000', '610500', '0', '0', '1', '渭南市');
INSERT INTO `system_areabase` VALUES ('3208', '90', '610500', '610502', '0', '0', '1', '临渭区');
INSERT INTO `system_areabase` VALUES ('3209', '90', '610500', '610521', '0', '0', '1', '华县');
INSERT INTO `system_areabase` VALUES ('3210', '90', '610500', '610522', '0', '0', '1', '潼关县');
INSERT INTO `system_areabase` VALUES ('3211', '90', '610500', '610523', '0', '0', '1', '大荔县');
INSERT INTO `system_areabase` VALUES ('3212', '90', '610500', '610524', '0', '0', '1', '合阳县');
INSERT INTO `system_areabase` VALUES ('3213', '90', '610500', '610525', '0', '0', '1', '澄城县');
INSERT INTO `system_areabase` VALUES ('3214', '90', '610500', '610526', '0', '0', '1', '蒲城县');
INSERT INTO `system_areabase` VALUES ('3215', '90', '610500', '610527', '0', '0', '1', '白水县');
INSERT INTO `system_areabase` VALUES ('3216', '90', '610500', '610528', '0', '0', '1', '富平县');
INSERT INTO `system_areabase` VALUES ('3217', '90', '610500', '610581', '0', '0', '1', '韩城市');
INSERT INTO `system_areabase` VALUES ('3218', '90', '610500', '610582', '0', '0', '1', '华阴市');
INSERT INTO `system_areabase` VALUES ('3219', '90', '610500', '610583', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3220', '90', '610000', '610600', '0', '0', '1', '延安市');
INSERT INTO `system_areabase` VALUES ('3221', '90', '610600', '610602', '0', '0', '1', '宝塔区');
INSERT INTO `system_areabase` VALUES ('3222', '90', '610600', '610621', '0', '0', '1', '延长县');
INSERT INTO `system_areabase` VALUES ('3223', '90', '610600', '610622', '0', '0', '1', '延川县');
INSERT INTO `system_areabase` VALUES ('3224', '90', '610600', '610623', '0', '0', '1', '子长县');
INSERT INTO `system_areabase` VALUES ('3225', '90', '610600', '610624', '0', '0', '1', '安塞县');
INSERT INTO `system_areabase` VALUES ('3226', '90', '610600', '610625', '0', '0', '1', '志丹县');
INSERT INTO `system_areabase` VALUES ('3227', '90', '610600', '610626', '0', '0', '1', '吴起县');
INSERT INTO `system_areabase` VALUES ('3228', '90', '610600', '610627', '0', '0', '1', '甘泉县');
INSERT INTO `system_areabase` VALUES ('3229', '90', '610600', '610628', '0', '0', '1', '富县');
INSERT INTO `system_areabase` VALUES ('3230', '90', '610600', '610629', '0', '0', '1', '洛川县');
INSERT INTO `system_areabase` VALUES ('3231', '90', '610600', '610630', '0', '0', '1', '宜川县');
INSERT INTO `system_areabase` VALUES ('3232', '90', '610600', '610631', '0', '0', '1', '黄龙县');
INSERT INTO `system_areabase` VALUES ('3233', '90', '610600', '610632', '0', '0', '1', '黄陵县');
INSERT INTO `system_areabase` VALUES ('3234', '90', '610600', '610633', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3235', '90', '610000', '610700', '0', '0', '1', '汉中市');
INSERT INTO `system_areabase` VALUES ('3236', '90', '610700', '610702', '0', '0', '1', '汉台区');
INSERT INTO `system_areabase` VALUES ('3237', '90', '610700', '610721', '0', '0', '1', '南郑县');
INSERT INTO `system_areabase` VALUES ('3238', '90', '610700', '610722', '0', '0', '1', '城固县');
INSERT INTO `system_areabase` VALUES ('3239', '90', '610700', '610723', '0', '0', '1', '洋县');
INSERT INTO `system_areabase` VALUES ('3240', '90', '610700', '610724', '0', '0', '1', '西乡县');
INSERT INTO `system_areabase` VALUES ('3241', '90', '610700', '610725', '0', '0', '1', '勉县');
INSERT INTO `system_areabase` VALUES ('3242', '90', '610700', '610726', '0', '0', '1', '宁强县');
INSERT INTO `system_areabase` VALUES ('3243', '90', '610700', '610727', '0', '0', '1', '略阳县');
INSERT INTO `system_areabase` VALUES ('3244', '90', '610700', '610728', '0', '0', '1', '镇巴县');
INSERT INTO `system_areabase` VALUES ('3245', '90', '610700', '610729', '0', '0', '1', '留坝县');
INSERT INTO `system_areabase` VALUES ('3246', '90', '610700', '610730', '0', '0', '1', '佛坪县');
INSERT INTO `system_areabase` VALUES ('3247', '90', '610700', '610731', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3248', '90', '610000', '610800', '0', '0', '1', '榆林市');
INSERT INTO `system_areabase` VALUES ('3249', '90', '610800', '610802', '0', '0', '1', '榆阳区');
INSERT INTO `system_areabase` VALUES ('3250', '90', '610800', '610821', '0', '0', '1', '神木县');
INSERT INTO `system_areabase` VALUES ('3251', '90', '610800', '610822', '0', '0', '1', '府谷县');
INSERT INTO `system_areabase` VALUES ('3252', '90', '610800', '610823', '0', '0', '1', '横山县');
INSERT INTO `system_areabase` VALUES ('3253', '90', '610800', '610824', '0', '0', '1', '靖边县');
INSERT INTO `system_areabase` VALUES ('3254', '90', '610800', '610825', '0', '0', '1', '定边县');
INSERT INTO `system_areabase` VALUES ('3255', '90', '610800', '610826', '0', '0', '1', '绥德县');
INSERT INTO `system_areabase` VALUES ('3256', '90', '610800', '610827', '0', '0', '1', '米脂县');
INSERT INTO `system_areabase` VALUES ('3257', '90', '610800', '610828', '0', '0', '1', '佳县');
INSERT INTO `system_areabase` VALUES ('3258', '90', '610800', '610829', '0', '0', '1', '吴堡县');
INSERT INTO `system_areabase` VALUES ('3259', '90', '610800', '610830', '0', '0', '1', '清涧县');
INSERT INTO `system_areabase` VALUES ('3260', '90', '610800', '610831', '0', '0', '1', '子洲县');
INSERT INTO `system_areabase` VALUES ('3261', '90', '610800', '610832', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3262', '90', '610000', '610900', '0', '0', '1', '安康市');
INSERT INTO `system_areabase` VALUES ('3263', '90', '610900', '610902', '0', '0', '1', '汉滨区');
INSERT INTO `system_areabase` VALUES ('3264', '90', '610900', '610921', '0', '0', '1', '汉阴县');
INSERT INTO `system_areabase` VALUES ('3265', '90', '610900', '610922', '0', '0', '1', '石泉县');
INSERT INTO `system_areabase` VALUES ('3266', '90', '610900', '610923', '0', '0', '1', '宁陕县');
INSERT INTO `system_areabase` VALUES ('3267', '90', '610900', '610924', '0', '0', '1', '紫阳县');
INSERT INTO `system_areabase` VALUES ('3268', '90', '610900', '610925', '0', '0', '1', '岚皋县');
INSERT INTO `system_areabase` VALUES ('3269', '90', '610900', '610926', '0', '0', '1', '平利县');
INSERT INTO `system_areabase` VALUES ('3270', '90', '610900', '610927', '0', '0', '1', '镇坪县');
INSERT INTO `system_areabase` VALUES ('3271', '90', '610900', '610928', '0', '0', '1', '旬阳县');
INSERT INTO `system_areabase` VALUES ('3272', '90', '610900', '610929', '0', '0', '1', '白河县');
INSERT INTO `system_areabase` VALUES ('3273', '90', '610900', '610930', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3274', '90', '610000', '611000', '0', '0', '1', '商洛市');
INSERT INTO `system_areabase` VALUES ('3275', '90', '611000', '611002', '0', '0', '1', '商州区');
INSERT INTO `system_areabase` VALUES ('3276', '90', '611000', '611021', '0', '0', '1', '洛南县');
INSERT INTO `system_areabase` VALUES ('3277', '90', '611000', '611022', '0', '0', '1', '丹凤县');
INSERT INTO `system_areabase` VALUES ('3278', '90', '611000', '611023', '0', '0', '1', '商南县');
INSERT INTO `system_areabase` VALUES ('3279', '90', '611000', '611024', '0', '0', '1', '山阳县');
INSERT INTO `system_areabase` VALUES ('3280', '90', '611000', '611025', '0', '0', '1', '镇安县');
INSERT INTO `system_areabase` VALUES ('3281', '90', '611000', '611026', '0', '0', '1', '柞水县');
INSERT INTO `system_areabase` VALUES ('3282', '90', '611000', '611027', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3283', '90', '0', '620000', '0', '0', '1', '甘肃省');
INSERT INTO `system_areabase` VALUES ('3284', '90', '620000', '620100', '0', '0', '1', '兰州市');
INSERT INTO `system_areabase` VALUES ('3285', '90', '620100', '620102', '0', '0', '1', '城关区');
INSERT INTO `system_areabase` VALUES ('3286', '90', '620100', '620103', '0', '0', '1', '七里河区');
INSERT INTO `system_areabase` VALUES ('3287', '90', '620100', '620104', '0', '0', '1', '西固区');
INSERT INTO `system_areabase` VALUES ('3288', '90', '620100', '620105', '0', '0', '1', '安宁区');
INSERT INTO `system_areabase` VALUES ('3289', '90', '620100', '620111', '0', '0', '1', '红古区');
INSERT INTO `system_areabase` VALUES ('3290', '90', '620100', '620121', '0', '0', '1', '永登县');
INSERT INTO `system_areabase` VALUES ('3291', '90', '620100', '620122', '0', '0', '1', '皋兰县');
INSERT INTO `system_areabase` VALUES ('3292', '90', '620100', '620123', '0', '0', '1', '榆中县');
INSERT INTO `system_areabase` VALUES ('3293', '90', '620100', '620124', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3294', '90', '620000', '620200', '0', '0', '1', '嘉峪关市');
INSERT INTO `system_areabase` VALUES ('3295', '90', '620000', '620300', '0', '0', '1', '金昌市');
INSERT INTO `system_areabase` VALUES ('3296', '90', '620300', '620302', '0', '0', '1', '金川区');
INSERT INTO `system_areabase` VALUES ('3297', '90', '620300', '620321', '0', '0', '1', '永昌县');
INSERT INTO `system_areabase` VALUES ('3298', '90', '620300', '620322', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3299', '90', '620000', '620400', '0', '0', '1', '白银市');
INSERT INTO `system_areabase` VALUES ('3300', '90', '620400', '620402', '0', '0', '1', '白银区');
INSERT INTO `system_areabase` VALUES ('3301', '90', '620400', '620403', '0', '0', '1', '平川区');
INSERT INTO `system_areabase` VALUES ('3302', '90', '620400', '620421', '0', '0', '1', '靖远县');
INSERT INTO `system_areabase` VALUES ('3303', '90', '620400', '620422', '0', '0', '1', '会宁县');
INSERT INTO `system_areabase` VALUES ('3304', '90', '620400', '620423', '0', '0', '1', '景泰县');
INSERT INTO `system_areabase` VALUES ('3305', '90', '620400', '620424', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3306', '90', '620000', '620500', '0', '0', '1', '天水市');
INSERT INTO `system_areabase` VALUES ('3307', '90', '620500', '620502', '0', '0', '1', '秦州区');
INSERT INTO `system_areabase` VALUES ('3308', '90', '620500', '620503', '0', '0', '1', '麦积区');
INSERT INTO `system_areabase` VALUES ('3309', '90', '620500', '620521', '0', '0', '1', '清水县');
INSERT INTO `system_areabase` VALUES ('3310', '90', '620500', '620522', '0', '0', '1', '秦安县');
INSERT INTO `system_areabase` VALUES ('3311', '90', '620500', '620523', '0', '0', '1', '甘谷县');
INSERT INTO `system_areabase` VALUES ('3312', '90', '620500', '620524', '0', '0', '1', '武山县');
INSERT INTO `system_areabase` VALUES ('3313', '90', '620500', '620525', '0', '0', '1', '张家川回族自治县');
INSERT INTO `system_areabase` VALUES ('3314', '90', '620500', '620526', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3315', '90', '620000', '620600', '0', '0', '1', '武威市');
INSERT INTO `system_areabase` VALUES ('3316', '90', '620600', '620602', '0', '0', '1', '凉州区');
INSERT INTO `system_areabase` VALUES ('3317', '90', '620600', '620621', '0', '0', '1', '民勤县');
INSERT INTO `system_areabase` VALUES ('3318', '90', '620600', '620622', '0', '0', '1', '古浪县');
INSERT INTO `system_areabase` VALUES ('3319', '90', '620600', '620623', '0', '0', '1', '天祝藏族自治县');
INSERT INTO `system_areabase` VALUES ('3320', '90', '620600', '620624', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3321', '90', '620000', '620700', '0', '0', '1', '张掖市');
INSERT INTO `system_areabase` VALUES ('3322', '90', '620700', '620702', '0', '0', '1', '甘州区');
INSERT INTO `system_areabase` VALUES ('3323', '90', '620700', '620721', '0', '0', '1', '肃南裕固族自治县');
INSERT INTO `system_areabase` VALUES ('3324', '90', '620700', '620722', '0', '0', '1', '民乐县');
INSERT INTO `system_areabase` VALUES ('3325', '90', '620700', '620723', '0', '0', '1', '临泽县');
INSERT INTO `system_areabase` VALUES ('3326', '90', '620700', '620724', '0', '0', '1', '高台县');
INSERT INTO `system_areabase` VALUES ('3327', '90', '620700', '620725', '0', '0', '1', '山丹县');
INSERT INTO `system_areabase` VALUES ('3328', '90', '620700', '620726', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3329', '90', '620000', '620800', '0', '0', '1', '平凉市');
INSERT INTO `system_areabase` VALUES ('3330', '90', '620800', '620802', '0', '0', '1', '崆峒区');
INSERT INTO `system_areabase` VALUES ('3331', '90', '620800', '620821', '0', '0', '1', '泾川县');
INSERT INTO `system_areabase` VALUES ('3332', '90', '620800', '620822', '0', '0', '1', '灵台县');
INSERT INTO `system_areabase` VALUES ('3333', '90', '620800', '620823', '0', '0', '1', '崇信县');
INSERT INTO `system_areabase` VALUES ('3334', '90', '620800', '620824', '0', '0', '1', '华亭县');
INSERT INTO `system_areabase` VALUES ('3335', '90', '620800', '620825', '0', '0', '1', '庄浪县');
INSERT INTO `system_areabase` VALUES ('3336', '90', '620800', '620826', '0', '0', '1', '静宁县');
INSERT INTO `system_areabase` VALUES ('3337', '90', '620800', '620827', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3338', '90', '620000', '620900', '0', '0', '1', '酒泉市');
INSERT INTO `system_areabase` VALUES ('3339', '90', '620900', '620902', '0', '0', '1', '肃州区');
INSERT INTO `system_areabase` VALUES ('3340', '90', '620900', '620921', '0', '0', '1', '金塔县');
INSERT INTO `system_areabase` VALUES ('3341', '90', '620900', '620922', '0', '0', '1', '安西县');
INSERT INTO `system_areabase` VALUES ('3342', '90', '620900', '620923', '0', '0', '1', '肃北蒙古族自治县');
INSERT INTO `system_areabase` VALUES ('3343', '90', '620900', '620924', '0', '0', '1', '阿克塞哈萨克族自治县');
INSERT INTO `system_areabase` VALUES ('3344', '90', '620900', '620981', '0', '0', '1', '玉门市');
INSERT INTO `system_areabase` VALUES ('3345', '90', '620900', '620982', '0', '0', '1', '敦煌市');
INSERT INTO `system_areabase` VALUES ('3346', '90', '620900', '620983', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3347', '90', '620000', '621000', '0', '0', '1', '庆阳市');
INSERT INTO `system_areabase` VALUES ('3348', '90', '621000', '621002', '0', '0', '1', '西峰区');
INSERT INTO `system_areabase` VALUES ('3349', '90', '621000', '621021', '0', '0', '1', '庆城县');
INSERT INTO `system_areabase` VALUES ('3350', '90', '621000', '621022', '0', '0', '1', '环县');
INSERT INTO `system_areabase` VALUES ('3351', '90', '621000', '621023', '0', '0', '1', '华池县');
INSERT INTO `system_areabase` VALUES ('3352', '90', '621000', '621024', '0', '0', '1', '合水县');
INSERT INTO `system_areabase` VALUES ('3353', '90', '621000', '621025', '0', '0', '1', '正宁县');
INSERT INTO `system_areabase` VALUES ('3354', '90', '621000', '621026', '0', '0', '1', '宁县');
INSERT INTO `system_areabase` VALUES ('3355', '90', '621000', '621027', '0', '0', '1', '镇原县');
INSERT INTO `system_areabase` VALUES ('3356', '90', '621000', '621028', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3357', '90', '620000', '621100', '0', '0', '1', '定西市');
INSERT INTO `system_areabase` VALUES ('3358', '90', '621100', '621102', '0', '0', '1', '安定区');
INSERT INTO `system_areabase` VALUES ('3359', '90', '621100', '621121', '0', '0', '1', '通渭县');
INSERT INTO `system_areabase` VALUES ('3360', '90', '621100', '621122', '0', '0', '1', '陇西县');
INSERT INTO `system_areabase` VALUES ('3361', '90', '621100', '621123', '0', '0', '1', '渭源县');
INSERT INTO `system_areabase` VALUES ('3362', '90', '621100', '621124', '0', '0', '1', '临洮县');
INSERT INTO `system_areabase` VALUES ('3363', '90', '621100', '621125', '0', '0', '1', '漳县');
INSERT INTO `system_areabase` VALUES ('3364', '90', '621100', '621126', '0', '0', '1', '岷县');
INSERT INTO `system_areabase` VALUES ('3365', '90', '621100', '621127', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3366', '90', '620000', '621200', '0', '0', '1', '陇南市');
INSERT INTO `system_areabase` VALUES ('3367', '90', '621200', '621202', '0', '0', '1', '武都区');
INSERT INTO `system_areabase` VALUES ('3368', '90', '621200', '621221', '0', '0', '1', '成县');
INSERT INTO `system_areabase` VALUES ('3369', '90', '621200', '621222', '0', '0', '1', '文县');
INSERT INTO `system_areabase` VALUES ('3370', '90', '621200', '621223', '0', '0', '1', '宕昌县');
INSERT INTO `system_areabase` VALUES ('3371', '90', '621200', '621224', '0', '0', '1', '康县');
INSERT INTO `system_areabase` VALUES ('3372', '90', '621200', '621225', '0', '0', '1', '西和县');
INSERT INTO `system_areabase` VALUES ('3373', '90', '621200', '621226', '0', '0', '1', '礼县');
INSERT INTO `system_areabase` VALUES ('3374', '90', '621200', '621227', '0', '0', '1', '徽县');
INSERT INTO `system_areabase` VALUES ('3375', '90', '621200', '621228', '0', '0', '1', '两当县');
INSERT INTO `system_areabase` VALUES ('3376', '90', '621200', '621229', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3377', '90', '620000', '622900', '0', '0', '1', '临夏回族自治州');
INSERT INTO `system_areabase` VALUES ('3378', '90', '622900', '622901', '0', '0', '1', '临夏市');
INSERT INTO `system_areabase` VALUES ('3379', '90', '622900', '622921', '0', '0', '1', '临夏县');
INSERT INTO `system_areabase` VALUES ('3380', '90', '622900', '622922', '0', '0', '1', '康乐县');
INSERT INTO `system_areabase` VALUES ('3381', '90', '622900', '622923', '0', '0', '1', '永靖县');
INSERT INTO `system_areabase` VALUES ('3382', '90', '622900', '622924', '0', '0', '1', '广河县');
INSERT INTO `system_areabase` VALUES ('3383', '90', '622900', '622925', '0', '0', '1', '和政县');
INSERT INTO `system_areabase` VALUES ('3384', '90', '622900', '622926', '0', '0', '1', '东乡族自治县');
INSERT INTO `system_areabase` VALUES ('3385', '90', '622900', '622927', '0', '0', '1', '积石山保安族东乡族撒拉族自治县');
INSERT INTO `system_areabase` VALUES ('3386', '90', '622900', '622928', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3387', '90', '620000', '623000', '0', '0', '1', '甘南藏族自治州');
INSERT INTO `system_areabase` VALUES ('3388', '90', '623000', '623001', '0', '0', '1', '合作市');
INSERT INTO `system_areabase` VALUES ('3389', '90', '623000', '623021', '0', '0', '1', '临潭县');
INSERT INTO `system_areabase` VALUES ('3390', '90', '623000', '623022', '0', '0', '1', '卓尼县');
INSERT INTO `system_areabase` VALUES ('3391', '90', '623000', '623023', '0', '0', '1', '舟曲县');
INSERT INTO `system_areabase` VALUES ('3392', '90', '623000', '623024', '0', '0', '1', '迭部县');
INSERT INTO `system_areabase` VALUES ('3393', '90', '623000', '623025', '0', '0', '1', '玛曲县');
INSERT INTO `system_areabase` VALUES ('3394', '90', '623000', '623026', '0', '0', '1', '碌曲县');
INSERT INTO `system_areabase` VALUES ('3395', '90', '623000', '623027', '0', '0', '1', '夏河县');
INSERT INTO `system_areabase` VALUES ('3396', '90', '623000', '623028', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3397', '90', '0', '630000', '0', '0', '1', '青海省');
INSERT INTO `system_areabase` VALUES ('3398', '90', '630000', '630100', '0', '0', '1', '西宁市');
INSERT INTO `system_areabase` VALUES ('3399', '90', '630100', '630102', '0', '0', '1', '城东区');
INSERT INTO `system_areabase` VALUES ('3400', '90', '630100', '630103', '0', '0', '1', '城中区');
INSERT INTO `system_areabase` VALUES ('3401', '90', '630100', '630104', '0', '0', '1', '城西区');
INSERT INTO `system_areabase` VALUES ('3402', '90', '630100', '630105', '0', '0', '1', '城北区');
INSERT INTO `system_areabase` VALUES ('3403', '90', '630100', '630121', '0', '0', '1', '大通回族土族自治县');
INSERT INTO `system_areabase` VALUES ('3404', '90', '630100', '630122', '0', '0', '1', '湟中县');
INSERT INTO `system_areabase` VALUES ('3405', '90', '630100', '630123', '0', '0', '1', '湟源县');
INSERT INTO `system_areabase` VALUES ('3406', '90', '630100', '630124', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3407', '90', '630000', '632100', '0', '0', '1', '海东地区');
INSERT INTO `system_areabase` VALUES ('3408', '90', '632100', '632121', '0', '0', '1', '平安县');
INSERT INTO `system_areabase` VALUES ('3409', '90', '632100', '632122', '0', '0', '1', '民和回族土族自治县');
INSERT INTO `system_areabase` VALUES ('3410', '90', '632100', '632123', '0', '0', '1', '乐都县');
INSERT INTO `system_areabase` VALUES ('3411', '90', '632100', '632126', '0', '0', '1', '互助土族自治县');
INSERT INTO `system_areabase` VALUES ('3412', '90', '632100', '632127', '0', '0', '1', '化隆回族自治县');
INSERT INTO `system_areabase` VALUES ('3413', '90', '632100', '632128', '0', '0', '1', '循化撒拉族自治县');
INSERT INTO `system_areabase` VALUES ('3414', '90', '632100', '632129', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3415', '90', '630000', '632200', '0', '0', '1', '海北藏族自治州');
INSERT INTO `system_areabase` VALUES ('3416', '90', '632200', '632221', '0', '0', '1', '门源回族自治县');
INSERT INTO `system_areabase` VALUES ('3417', '90', '632200', '632222', '0', '0', '1', '祁连县');
INSERT INTO `system_areabase` VALUES ('3418', '90', '632200', '632223', '0', '0', '1', '海晏县');
INSERT INTO `system_areabase` VALUES ('3419', '90', '632200', '632224', '0', '0', '1', '刚察县');
INSERT INTO `system_areabase` VALUES ('3420', '90', '632200', '632225', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3421', '90', '630000', '632300', '0', '0', '1', '黄南藏族自治州');
INSERT INTO `system_areabase` VALUES ('3422', '90', '632300', '632321', '0', '0', '1', '同仁县');
INSERT INTO `system_areabase` VALUES ('3423', '90', '632300', '632322', '0', '0', '1', '尖扎县');
INSERT INTO `system_areabase` VALUES ('3424', '90', '632300', '632323', '0', '0', '1', '泽库县');
INSERT INTO `system_areabase` VALUES ('3425', '90', '632300', '632324', '0', '0', '1', '河南蒙古族自治县');
INSERT INTO `system_areabase` VALUES ('3426', '90', '632300', '632325', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3427', '90', '630000', '632500', '0', '0', '1', '海南藏族自治州');
INSERT INTO `system_areabase` VALUES ('3428', '90', '632500', '632521', '0', '0', '1', '共和县');
INSERT INTO `system_areabase` VALUES ('3429', '90', '632500', '632522', '0', '0', '1', '同德县');
INSERT INTO `system_areabase` VALUES ('3430', '90', '632500', '632523', '0', '0', '1', '贵德县');
INSERT INTO `system_areabase` VALUES ('3431', '90', '632500', '632524', '0', '0', '1', '兴海县');
INSERT INTO `system_areabase` VALUES ('3432', '90', '632500', '632525', '0', '0', '1', '贵南县');
INSERT INTO `system_areabase` VALUES ('3433', '90', '632500', '632526', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3434', '90', '630000', '632600', '0', '0', '1', '果洛藏族自治州');
INSERT INTO `system_areabase` VALUES ('3435', '90', '632600', '632621', '0', '0', '1', '玛沁县');
INSERT INTO `system_areabase` VALUES ('3436', '90', '632600', '632622', '0', '0', '1', '班玛县');
INSERT INTO `system_areabase` VALUES ('3437', '90', '632600', '632623', '0', '0', '1', '甘德县');
INSERT INTO `system_areabase` VALUES ('3438', '90', '632600', '632624', '0', '0', '1', '达日县');
INSERT INTO `system_areabase` VALUES ('3439', '90', '632600', '632625', '0', '0', '1', '久治县');
INSERT INTO `system_areabase` VALUES ('3440', '90', '632600', '632626', '0', '0', '1', '玛多县');
INSERT INTO `system_areabase` VALUES ('3441', '90', '632600', '632627', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3442', '90', '630000', '632700', '0', '0', '1', '玉树藏族自治州');
INSERT INTO `system_areabase` VALUES ('3443', '90', '632700', '632721', '0', '0', '1', '玉树县');
INSERT INTO `system_areabase` VALUES ('3444', '90', '632700', '632722', '0', '0', '1', '杂多县');
INSERT INTO `system_areabase` VALUES ('3445', '90', '632700', '632723', '0', '0', '1', '称多县');
INSERT INTO `system_areabase` VALUES ('3446', '90', '632700', '632724', '0', '0', '1', '治多县');
INSERT INTO `system_areabase` VALUES ('3447', '90', '632700', '632725', '0', '0', '1', '囊谦县');
INSERT INTO `system_areabase` VALUES ('3448', '90', '632700', '632726', '0', '0', '1', '曲麻莱县');
INSERT INTO `system_areabase` VALUES ('3449', '90', '632700', '632727', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3450', '90', '630000', '632800', '0', '0', '1', '海西蒙古族藏族自治州');
INSERT INTO `system_areabase` VALUES ('3451', '90', '632800', '632801', '0', '0', '1', '格尔木市');
INSERT INTO `system_areabase` VALUES ('3452', '90', '632800', '632802', '0', '0', '1', '德令哈市');
INSERT INTO `system_areabase` VALUES ('3453', '90', '632800', '632821', '0', '0', '1', '乌兰县');
INSERT INTO `system_areabase` VALUES ('3454', '90', '632800', '632822', '0', '0', '1', '都兰县');
INSERT INTO `system_areabase` VALUES ('3455', '90', '632800', '632823', '0', '0', '1', '天峻县');
INSERT INTO `system_areabase` VALUES ('3456', '90', '632800', '632824', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3457', '90', '0', '640000', '0', '0', '1', '宁夏回族自治区');
INSERT INTO `system_areabase` VALUES ('3458', '90', '640000', '640100', '0', '0', '1', '银川市');
INSERT INTO `system_areabase` VALUES ('3459', '90', '640100', '640104', '0', '0', '1', '兴庆区');
INSERT INTO `system_areabase` VALUES ('3460', '90', '640100', '640105', '0', '0', '1', '西夏区');
INSERT INTO `system_areabase` VALUES ('3461', '90', '640100', '640106', '0', '0', '1', '金凤区');
INSERT INTO `system_areabase` VALUES ('3462', '90', '640100', '640121', '0', '0', '1', '永宁县');
INSERT INTO `system_areabase` VALUES ('3463', '90', '640100', '640122', '0', '0', '1', '贺兰县');
INSERT INTO `system_areabase` VALUES ('3464', '90', '640100', '640181', '0', '0', '1', '灵武市');
INSERT INTO `system_areabase` VALUES ('3465', '90', '640100', '640182', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3466', '90', '640000', '640200', '0', '0', '1', '石嘴山市');
INSERT INTO `system_areabase` VALUES ('3467', '90', '640200', '640202', '0', '0', '1', '大武口区');
INSERT INTO `system_areabase` VALUES ('3468', '90', '640200', '640205', '0', '0', '1', '惠农区');
INSERT INTO `system_areabase` VALUES ('3469', '90', '640200', '640221', '0', '0', '1', '平罗县');
INSERT INTO `system_areabase` VALUES ('3470', '90', '640200', '640222', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3471', '90', '640000', '640300', '0', '0', '1', '吴忠市');
INSERT INTO `system_areabase` VALUES ('3472', '90', '640300', '640302', '0', '0', '1', '利通区');
INSERT INTO `system_areabase` VALUES ('3473', '90', '640300', '640323', '0', '0', '1', '盐池县');
INSERT INTO `system_areabase` VALUES ('3474', '90', '640300', '640324', '0', '0', '1', '同心县');
INSERT INTO `system_areabase` VALUES ('3475', '90', '640300', '640381', '0', '0', '1', '青铜峡');
INSERT INTO `system_areabase` VALUES ('3476', '90', '640300', '640382', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3477', '90', '640000', '640400', '0', '0', '1', '固原市');
INSERT INTO `system_areabase` VALUES ('3478', '90', '640400', '640402', '0', '0', '1', '原州区');
INSERT INTO `system_areabase` VALUES ('3479', '90', '640400', '640422', '0', '0', '1', '西吉县');
INSERT INTO `system_areabase` VALUES ('3480', '90', '640400', '640423', '0', '0', '1', '隆德县');
INSERT INTO `system_areabase` VALUES ('3481', '90', '640400', '640424', '0', '0', '1', '泾源县');
INSERT INTO `system_areabase` VALUES ('3482', '90', '640400', '640425', '0', '0', '1', '彭阳县');
INSERT INTO `system_areabase` VALUES ('3483', '90', '640400', '640426', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3484', '90', '640000', '640500', '0', '0', '1', '中卫市');
INSERT INTO `system_areabase` VALUES ('3485', '90', '640500', '640502', '0', '0', '1', '沙坡头区');
INSERT INTO `system_areabase` VALUES ('3486', '90', '640500', '640521', '0', '0', '1', '中宁县');
INSERT INTO `system_areabase` VALUES ('3487', '90', '640500', '640522', '0', '0', '1', '海原县');
INSERT INTO `system_areabase` VALUES ('3488', '90', '640500', '640523', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3489', '90', '0', '650000', '0', '0', '1', '新疆维吾尔自治区');
INSERT INTO `system_areabase` VALUES ('3490', '90', '650000', '650100', '0', '0', '1', '乌鲁木齐市');
INSERT INTO `system_areabase` VALUES ('3491', '90', '650100', '650102', '0', '0', '1', '天山区');
INSERT INTO `system_areabase` VALUES ('3492', '90', '650100', '650103', '0', '0', '1', '沙依巴克区');
INSERT INTO `system_areabase` VALUES ('3493', '90', '650100', '650104', '0', '0', '1', '新市区');
INSERT INTO `system_areabase` VALUES ('3494', '90', '650100', '650105', '0', '0', '1', '水磨沟区');
INSERT INTO `system_areabase` VALUES ('3495', '90', '650100', '650106', '0', '0', '1', '头屯河区');
INSERT INTO `system_areabase` VALUES ('3496', '90', '650100', '650107', '0', '0', '1', '达坂城区');
INSERT INTO `system_areabase` VALUES ('3497', '90', '650100', '650108', '0', '0', '1', '东山区');
INSERT INTO `system_areabase` VALUES ('3498', '90', '650100', '650109', '0', '0', '1', '米东区');
INSERT INTO `system_areabase` VALUES ('3499', '90', '650100', '650121', '0', '0', '1', '乌鲁木齐县');
INSERT INTO `system_areabase` VALUES ('3500', '90', '650100', '650122', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3501', '90', '650000', '650200', '0', '0', '1', '克拉玛依市');
INSERT INTO `system_areabase` VALUES ('3502', '90', '650200', '650202', '0', '0', '1', '独山子区');
INSERT INTO `system_areabase` VALUES ('3503', '90', '650200', '650203', '0', '0', '1', '克拉玛依区');
INSERT INTO `system_areabase` VALUES ('3504', '90', '650200', '650204', '0', '0', '1', '白碱滩区');
INSERT INTO `system_areabase` VALUES ('3505', '90', '650200', '650205', '0', '0', '1', '乌尔禾区');
INSERT INTO `system_areabase` VALUES ('3506', '90', '650200', '650206', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3507', '90', '650000', '652100', '0', '0', '1', '吐鲁番地区');
INSERT INTO `system_areabase` VALUES ('3508', '90', '652100', '652101', '0', '0', '1', '吐鲁番市');
INSERT INTO `system_areabase` VALUES ('3509', '90', '652100', '652122', '0', '0', '1', '鄯善县');
INSERT INTO `system_areabase` VALUES ('3510', '90', '652100', '652123', '0', '0', '1', '托克逊县');
INSERT INTO `system_areabase` VALUES ('3511', '90', '652100', '652124', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3512', '90', '650000', '652200', '0', '0', '1', '哈密地区');
INSERT INTO `system_areabase` VALUES ('3513', '90', '652200', '652201', '0', '0', '1', '哈密市');
INSERT INTO `system_areabase` VALUES ('3514', '90', '652200', '652222', '0', '0', '1', '巴里坤哈萨克自治县');
INSERT INTO `system_areabase` VALUES ('3515', '90', '652200', '652223', '0', '0', '1', '伊吾县');
INSERT INTO `system_areabase` VALUES ('3516', '90', '652200', '652224', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3517', '90', '650000', '652300', '0', '0', '1', '昌吉回族自治州');
INSERT INTO `system_areabase` VALUES ('3518', '90', '652300', '652301', '0', '0', '1', '昌吉市');
INSERT INTO `system_areabase` VALUES ('3519', '90', '652300', '652302', '0', '0', '1', '阜康市');
INSERT INTO `system_areabase` VALUES ('3520', '90', '652300', '652303', '0', '0', '1', '米泉市');
INSERT INTO `system_areabase` VALUES ('3521', '90', '652300', '652323', '0', '0', '1', '呼图壁县');
INSERT INTO `system_areabase` VALUES ('3522', '90', '652300', '652324', '0', '0', '1', '玛纳斯县');
INSERT INTO `system_areabase` VALUES ('3523', '90', '652300', '652325', '0', '0', '1', '奇台县');
INSERT INTO `system_areabase` VALUES ('3524', '90', '652300', '652327', '0', '0', '1', '吉木萨尔县');
INSERT INTO `system_areabase` VALUES ('3525', '90', '652300', '652328', '0', '0', '1', '木垒哈萨克自治县');
INSERT INTO `system_areabase` VALUES ('3526', '90', '652300', '652329', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3527', '90', '650000', '652700', '0', '0', '1', '博尔塔拉蒙古自治州');
INSERT INTO `system_areabase` VALUES ('3528', '90', '652700', '652701', '0', '0', '1', '博乐市');
INSERT INTO `system_areabase` VALUES ('3529', '90', '652700', '652722', '0', '0', '1', '精河县');
INSERT INTO `system_areabase` VALUES ('3530', '90', '652700', '652723', '0', '0', '1', '温泉县');
INSERT INTO `system_areabase` VALUES ('3531', '90', '652700', '652724', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3532', '90', '650000', '652800', '0', '0', '1', '巴音郭楞蒙古自治州');
INSERT INTO `system_areabase` VALUES ('3533', '90', '652800', '652801', '0', '0', '1', '库尔勒市');
INSERT INTO `system_areabase` VALUES ('3534', '90', '652800', '652822', '0', '0', '1', '轮台县');
INSERT INTO `system_areabase` VALUES ('3535', '90', '652800', '652823', '0', '0', '1', '尉犁县');
INSERT INTO `system_areabase` VALUES ('3536', '90', '652800', '652824', '0', '0', '1', '若羌县');
INSERT INTO `system_areabase` VALUES ('3537', '90', '652800', '652825', '0', '0', '1', '且末县');
INSERT INTO `system_areabase` VALUES ('3538', '90', '652800', '652826', '0', '0', '1', '焉耆回族自治县');
INSERT INTO `system_areabase` VALUES ('3539', '90', '652800', '652827', '0', '0', '1', '和静县');
INSERT INTO `system_areabase` VALUES ('3540', '90', '652800', '652828', '0', '0', '1', '和硕县');
INSERT INTO `system_areabase` VALUES ('3541', '90', '652800', '652829', '0', '0', '1', '博湖县');
INSERT INTO `system_areabase` VALUES ('3542', '90', '652800', '652830', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3543', '90', '650000', '652900', '0', '0', '1', '阿克苏地区');
INSERT INTO `system_areabase` VALUES ('3544', '90', '652900', '652901', '0', '0', '1', '阿克苏市');
INSERT INTO `system_areabase` VALUES ('3545', '90', '652900', '652922', '0', '0', '1', '温宿县');
INSERT INTO `system_areabase` VALUES ('3546', '90', '652900', '652923', '0', '0', '1', '库车县');
INSERT INTO `system_areabase` VALUES ('3547', '90', '652900', '652924', '0', '0', '1', '沙雅县');
INSERT INTO `system_areabase` VALUES ('3548', '90', '652900', '652925', '0', '0', '1', '新和县');
INSERT INTO `system_areabase` VALUES ('3549', '90', '652900', '652926', '0', '0', '1', '拜城县');
INSERT INTO `system_areabase` VALUES ('3550', '90', '652900', '652927', '0', '0', '1', '乌什县');
INSERT INTO `system_areabase` VALUES ('3551', '90', '652900', '652928', '0', '0', '1', '阿瓦提县');
INSERT INTO `system_areabase` VALUES ('3552', '90', '652900', '652929', '0', '0', '1', '柯坪县');
INSERT INTO `system_areabase` VALUES ('3553', '90', '652900', '652930', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3554', '90', '650000', '653000', '0', '0', '1', '克孜勒苏柯尔克孜自治州');
INSERT INTO `system_areabase` VALUES ('3555', '90', '653000', '653001', '0', '0', '1', '阿图什市');
INSERT INTO `system_areabase` VALUES ('3556', '90', '653000', '653022', '0', '0', '1', '阿克陶县');
INSERT INTO `system_areabase` VALUES ('3557', '90', '653000', '653023', '0', '0', '1', '阿合奇县');
INSERT INTO `system_areabase` VALUES ('3558', '90', '653000', '653024', '0', '0', '1', '乌恰县');
INSERT INTO `system_areabase` VALUES ('3559', '90', '653000', '653025', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3560', '90', '650000', '653100', '0', '0', '1', '喀什地区');
INSERT INTO `system_areabase` VALUES ('3561', '90', '653100', '653101', '0', '0', '1', '喀什市');
INSERT INTO `system_areabase` VALUES ('3562', '90', '653100', '653121', '0', '0', '1', '疏附县');
INSERT INTO `system_areabase` VALUES ('3563', '90', '653100', '653122', '0', '0', '1', '疏勒县');
INSERT INTO `system_areabase` VALUES ('3564', '90', '653100', '653123', '0', '0', '1', '英吉沙县');
INSERT INTO `system_areabase` VALUES ('3565', '90', '653100', '653124', '0', '0', '1', '泽普县');
INSERT INTO `system_areabase` VALUES ('3566', '90', '653100', '653125', '0', '0', '1', '莎车县');
INSERT INTO `system_areabase` VALUES ('3567', '90', '653100', '653126', '0', '0', '1', '叶城县');
INSERT INTO `system_areabase` VALUES ('3568', '90', '653100', '653127', '0', '0', '1', '麦盖提县');
INSERT INTO `system_areabase` VALUES ('3569', '90', '653100', '653128', '0', '0', '1', '岳普湖县');
INSERT INTO `system_areabase` VALUES ('3570', '90', '653100', '653129', '0', '0', '1', '伽师县');
INSERT INTO `system_areabase` VALUES ('3571', '90', '653100', '653130', '0', '0', '1', '巴楚县');
INSERT INTO `system_areabase` VALUES ('3572', '90', '653100', '653131', '0', '0', '1', '塔什库尔干塔吉克自治县');
INSERT INTO `system_areabase` VALUES ('3573', '90', '653100', '653132', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3574', '90', '650000', '653200', '0', '0', '1', '和田地区');
INSERT INTO `system_areabase` VALUES ('3575', '90', '653200', '653201', '0', '0', '1', '和田市');
INSERT INTO `system_areabase` VALUES ('3576', '90', '653200', '653221', '0', '0', '1', '和田县');
INSERT INTO `system_areabase` VALUES ('3577', '90', '653200', '653222', '0', '0', '1', '墨玉县');
INSERT INTO `system_areabase` VALUES ('3578', '90', '653200', '653223', '0', '0', '1', '皮山县');
INSERT INTO `system_areabase` VALUES ('3579', '90', '653200', '653224', '0', '0', '1', '洛浦县');
INSERT INTO `system_areabase` VALUES ('3580', '90', '653200', '653225', '0', '0', '1', '策勒县');
INSERT INTO `system_areabase` VALUES ('3581', '90', '653200', '653226', '0', '0', '1', '于田县');
INSERT INTO `system_areabase` VALUES ('3582', '90', '653200', '653227', '0', '0', '1', '民丰县');
INSERT INTO `system_areabase` VALUES ('3583', '90', '653200', '653228', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3584', '90', '650000', '654000', '0', '0', '1', '伊犁哈萨克自治州');
INSERT INTO `system_areabase` VALUES ('3585', '90', '654000', '654002', '0', '0', '1', '伊宁市');
INSERT INTO `system_areabase` VALUES ('3586', '90', '654000', '654003', '0', '0', '1', '奎屯市');
INSERT INTO `system_areabase` VALUES ('3587', '90', '654000', '654021', '0', '0', '1', '伊宁县');
INSERT INTO `system_areabase` VALUES ('3588', '90', '654000', '654022', '0', '0', '1', '察布查尔锡伯自治县');
INSERT INTO `system_areabase` VALUES ('3589', '90', '654000', '654023', '0', '0', '1', '霍城县');
INSERT INTO `system_areabase` VALUES ('3590', '90', '654000', '654024', '0', '0', '1', '巩留县');
INSERT INTO `system_areabase` VALUES ('3591', '90', '654000', '654025', '0', '0', '1', '新源县');
INSERT INTO `system_areabase` VALUES ('3592', '90', '654000', '654026', '0', '0', '1', '昭苏县');
INSERT INTO `system_areabase` VALUES ('3593', '90', '654000', '654027', '0', '0', '1', '特克斯县');
INSERT INTO `system_areabase` VALUES ('3594', '90', '654000', '654028', '0', '0', '1', '尼勒克县');
INSERT INTO `system_areabase` VALUES ('3595', '90', '654000', '654029', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3596', '90', '650000', '654200', '0', '0', '1', '塔城地区');
INSERT INTO `system_areabase` VALUES ('3597', '90', '654200', '654201', '0', '0', '1', '塔城市');
INSERT INTO `system_areabase` VALUES ('3598', '90', '654200', '654202', '0', '0', '1', '乌苏市');
INSERT INTO `system_areabase` VALUES ('3599', '90', '654200', '654221', '0', '0', '1', '额敏县');
INSERT INTO `system_areabase` VALUES ('3600', '90', '654200', '654223', '0', '0', '1', '沙湾县');
INSERT INTO `system_areabase` VALUES ('3601', '90', '654200', '654224', '0', '0', '1', '托里县');
INSERT INTO `system_areabase` VALUES ('3602', '90', '654200', '654225', '0', '0', '1', '裕民县');
INSERT INTO `system_areabase` VALUES ('3603', '90', '654200', '654226', '0', '0', '1', '和布克赛尔蒙古自治县');
INSERT INTO `system_areabase` VALUES ('3604', '90', '654200', '654227', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3605', '90', '650000', '654300', '0', '0', '1', '阿勒泰地区');
INSERT INTO `system_areabase` VALUES ('3606', '90', '654300', '654301', '0', '0', '1', '阿勒泰市');
INSERT INTO `system_areabase` VALUES ('3607', '90', '654300', '654321', '0', '0', '1', '布尔津县');
INSERT INTO `system_areabase` VALUES ('3608', '90', '654300', '654322', '0', '0', '1', '富蕴县');
INSERT INTO `system_areabase` VALUES ('3609', '90', '654300', '654323', '0', '0', '1', '福海县');
INSERT INTO `system_areabase` VALUES ('3610', '90', '654300', '654324', '0', '0', '1', '哈巴河县');
INSERT INTO `system_areabase` VALUES ('3611', '90', '654300', '654325', '0', '0', '1', '青河县');
INSERT INTO `system_areabase` VALUES ('3612', '90', '654300', '654326', '0', '0', '1', '吉木乃县');
INSERT INTO `system_areabase` VALUES ('3613', '90', '654300', '654327', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3614', '90', '650000', '659001', '0', '0', '1', '石河子市');
INSERT INTO `system_areabase` VALUES ('3615', '90', '650000', '659002', '0', '0', '1', '阿拉尔市');
INSERT INTO `system_areabase` VALUES ('3616', '90', '650000', '659003', '0', '0', '1', '图木舒克市');
INSERT INTO `system_areabase` VALUES ('3617', '90', '650000', '659004', '0', '0', '1', '五家渠市');
INSERT INTO `system_areabase` VALUES ('3618', '90', '0', '710000', '0', '0', '1', '台湾');
INSERT INTO `system_areabase` VALUES ('3619', '90', '710000', '710100', '0', '0', '1', '台北市');
INSERT INTO `system_areabase` VALUES ('3620', '90', '710100', '710101', '0', '0', '1', '中正区');
INSERT INTO `system_areabase` VALUES ('3621', '90', '710100', '710102', '0', '0', '1', '大同区');
INSERT INTO `system_areabase` VALUES ('3622', '90', '710100', '710103', '0', '0', '1', '中山区');
INSERT INTO `system_areabase` VALUES ('3623', '90', '710100', '710104', '0', '0', '1', '松山区');
INSERT INTO `system_areabase` VALUES ('3624', '90', '710100', '710105', '0', '0', '1', '大安区');
INSERT INTO `system_areabase` VALUES ('3625', '90', '710100', '710106', '0', '0', '1', '万华区');
INSERT INTO `system_areabase` VALUES ('3626', '90', '710100', '710107', '0', '0', '1', '信义区');
INSERT INTO `system_areabase` VALUES ('3627', '90', '710100', '710108', '0', '0', '1', '士林区');
INSERT INTO `system_areabase` VALUES ('3628', '90', '710100', '710109', '0', '0', '1', '北投区');
INSERT INTO `system_areabase` VALUES ('3629', '90', '710100', '710110', '0', '0', '1', '内湖区');
INSERT INTO `system_areabase` VALUES ('3630', '90', '710100', '710111', '0', '0', '1', '南港区');
INSERT INTO `system_areabase` VALUES ('3631', '90', '710100', '710112', '0', '0', '1', '文山区');
INSERT INTO `system_areabase` VALUES ('3632', '90', '710100', '710113', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3633', '90', '710000', '710200', '0', '0', '1', '高雄市');
INSERT INTO `system_areabase` VALUES ('3634', '90', '710200', '710201', '0', '0', '1', '新兴区');
INSERT INTO `system_areabase` VALUES ('3635', '90', '710200', '710202', '0', '0', '1', '前金区');
INSERT INTO `system_areabase` VALUES ('3636', '90', '710200', '710203', '0', '0', '1', '芩雅区');
INSERT INTO `system_areabase` VALUES ('3637', '90', '710200', '710204', '0', '0', '1', '盐埕区');
INSERT INTO `system_areabase` VALUES ('3638', '90', '710200', '710205', '0', '0', '1', '鼓山区');
INSERT INTO `system_areabase` VALUES ('3639', '90', '710200', '710206', '0', '0', '1', '旗津区');
INSERT INTO `system_areabase` VALUES ('3640', '90', '710200', '710207', '0', '0', '1', '前镇区');
INSERT INTO `system_areabase` VALUES ('3641', '90', '710200', '710208', '0', '0', '1', '三民区');
INSERT INTO `system_areabase` VALUES ('3642', '90', '710200', '710209', '0', '0', '1', '左营区');
INSERT INTO `system_areabase` VALUES ('3643', '90', '710200', '710210', '0', '0', '1', '楠梓区');
INSERT INTO `system_areabase` VALUES ('3644', '90', '710200', '710211', '0', '0', '1', '小港区');
INSERT INTO `system_areabase` VALUES ('3645', '90', '710200', '710212', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3646', '90', '710000', '710300', '0', '0', '1', '台南市');
INSERT INTO `system_areabase` VALUES ('3647', '90', '710300', '710301', '0', '0', '1', '中西区');
INSERT INTO `system_areabase` VALUES ('3648', '90', '710300', '710302', '0', '0', '1', '东区');
INSERT INTO `system_areabase` VALUES ('3649', '90', '710300', '710303', '0', '0', '1', '南区');
INSERT INTO `system_areabase` VALUES ('3650', '90', '710300', '710304', '0', '0', '1', '北区');
INSERT INTO `system_areabase` VALUES ('3651', '90', '710300', '710305', '0', '0', '1', '安平区');
INSERT INTO `system_areabase` VALUES ('3652', '90', '710300', '710306', '0', '0', '1', '安南区');
INSERT INTO `system_areabase` VALUES ('3653', '90', '710300', '710307', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3654', '90', '710000', '710400', '0', '0', '1', '台中市');
INSERT INTO `system_areabase` VALUES ('3655', '90', '710400', '710401', '0', '0', '1', '中区');
INSERT INTO `system_areabase` VALUES ('3656', '90', '710400', '710402', '0', '0', '1', '东区');
INSERT INTO `system_areabase` VALUES ('3657', '90', '710400', '710403', '0', '0', '1', '南区');
INSERT INTO `system_areabase` VALUES ('3658', '90', '710400', '710404', '0', '0', '1', '西区');
INSERT INTO `system_areabase` VALUES ('3659', '90', '710400', '710405', '0', '0', '1', '北区');
INSERT INTO `system_areabase` VALUES ('3660', '90', '710400', '710406', '0', '0', '1', '北屯区');
INSERT INTO `system_areabase` VALUES ('3661', '90', '710400', '710407', '0', '0', '1', '西屯区');
INSERT INTO `system_areabase` VALUES ('3662', '90', '710400', '710408', '0', '0', '1', '南屯区');
INSERT INTO `system_areabase` VALUES ('3663', '90', '710400', '710409', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3664', '90', '710000', '710500', '0', '0', '1', '金门县');
INSERT INTO `system_areabase` VALUES ('3665', '90', '710000', '710600', '0', '0', '1', '南投县');
INSERT INTO `system_areabase` VALUES ('3666', '90', '710000', '710700', '0', '0', '1', '基隆市');
INSERT INTO `system_areabase` VALUES ('3667', '90', '710700', '710701', '0', '0', '1', '仁爱区');
INSERT INTO `system_areabase` VALUES ('3668', '90', '710700', '710702', '0', '0', '1', '信义区');
INSERT INTO `system_areabase` VALUES ('3669', '90', '710700', '710703', '0', '0', '1', '中正区');
INSERT INTO `system_areabase` VALUES ('3670', '90', '710700', '710704', '0', '0', '1', '中山区');
INSERT INTO `system_areabase` VALUES ('3671', '90', '710700', '710705', '0', '0', '1', '安乐区');
INSERT INTO `system_areabase` VALUES ('3672', '90', '710700', '710706', '0', '0', '1', '暖暖区');
INSERT INTO `system_areabase` VALUES ('3673', '90', '710700', '710707', '0', '0', '1', '七堵区');
INSERT INTO `system_areabase` VALUES ('3674', '90', '710700', '710708', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3675', '90', '710000', '710800', '0', '0', '1', '新竹市');
INSERT INTO `system_areabase` VALUES ('3676', '90', '710800', '710801', '0', '0', '1', '东区');
INSERT INTO `system_areabase` VALUES ('3677', '90', '710800', '710802', '0', '0', '1', '北区');
INSERT INTO `system_areabase` VALUES ('3678', '90', '710800', '710803', '0', '0', '1', '香山区');
INSERT INTO `system_areabase` VALUES ('3679', '90', '710800', '710804', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3680', '90', '710000', '710900', '0', '0', '1', '嘉义市');
INSERT INTO `system_areabase` VALUES ('3681', '90', '710900', '710901', '0', '0', '1', '东区');
INSERT INTO `system_areabase` VALUES ('3682', '90', '710900', '710902', '0', '0', '1', '西区');
INSERT INTO `system_areabase` VALUES ('3683', '90', '710900', '710903', '0', '0', '1', '其它区');
INSERT INTO `system_areabase` VALUES ('3684', '90', '710000', '711100', '0', '0', '1', '新北市');
INSERT INTO `system_areabase` VALUES ('3685', '90', '710000', '711200', '0', '0', '1', '宜兰县');
INSERT INTO `system_areabase` VALUES ('3686', '90', '710000', '711300', '0', '0', '1', '新竹县');
INSERT INTO `system_areabase` VALUES ('3687', '90', '710000', '711400', '0', '0', '1', '桃园县');
INSERT INTO `system_areabase` VALUES ('3688', '90', '710000', '711500', '0', '0', '1', '苗栗县');
INSERT INTO `system_areabase` VALUES ('3689', '90', '710000', '711700', '0', '0', '1', '彰化县');
INSERT INTO `system_areabase` VALUES ('3690', '90', '710000', '711900', '0', '0', '1', '嘉义县');
INSERT INTO `system_areabase` VALUES ('3691', '90', '710000', '712100', '0', '0', '1', '云林县');
INSERT INTO `system_areabase` VALUES ('3692', '90', '710000', '712200', '0', '0', '1', '台南县');
INSERT INTO `system_areabase` VALUES ('3693', '90', '710000', '712300', '0', '0', '1', '高雄县');
INSERT INTO `system_areabase` VALUES ('3694', '90', '710000', '712400', '0', '0', '1', '屏东县');
INSERT INTO `system_areabase` VALUES ('3695', '90', '710000', '712500', '0', '0', '1', '台东县');
INSERT INTO `system_areabase` VALUES ('3696', '90', '710000', '712600', '0', '0', '1', '花莲县');
INSERT INTO `system_areabase` VALUES ('3697', '90', '710000', '712700', '0', '0', '1', '澎湖县');
INSERT INTO `system_areabase` VALUES ('3698', '90', '0', '810000', '0', '0', '1', '香港特别行政区');
INSERT INTO `system_areabase` VALUES ('3699', '90', '810000', '810100', '0', '0', '1', '香港');
INSERT INTO `system_areabase` VALUES ('3700', '90', '810100', '810101', '0', '0', '1', '香港岛');
INSERT INTO `system_areabase` VALUES ('3701', '90', '810100', '810102', '0', '0', '1', '九龙');
INSERT INTO `system_areabase` VALUES ('3702', '90', '810100', '810103', '0', '0', '1', '新界');
INSERT INTO `system_areabase` VALUES ('3703', '90', '0', '820000', '0', '0', '1', '澳门特别行政区');
INSERT INTO `system_areabase` VALUES ('3704', '90', '820000', '820100', '0', '0', '1', '澳门');
INSERT INTO `system_areabase` VALUES ('3705', '90', '820100', '820101', '0', '0', '1', '澳门半岛');
INSERT INTO `system_areabase` VALUES ('3706', '90', '820100', '820102', '0', '0', '1', '离岛');

-- ----------------------------
-- Table structure for `system_areabase2`
-- ----------------------------

DROP TABLE IF EXISTS `system_areabase2`;

CREATE TABLE `system_areabase2` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `area_bn` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  `p_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sort` int(10) unsigned NOT NULL DEFAULT '90',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `system_areabase2`
-- ----------------------------

-- ----------------------------
-- Table structure for `system_article`
-- ----------------------------

DROP TABLE IF EXISTS `system_article`;

CREATE TABLE `system_article` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sort` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `typeid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `system_article`
-- ----------------------------

INSERT INTO `system_article` VALUES ('3', '0', '0', '0', '1526396066', '1528875500', '车管服务', '新车入户：
  机动车合格证，购车发票注册登记联，交强险副业，购置税副本，机动车所有人身份证明，代理人身份证明，委托
书。不属于免检车型的机动车需提供机动车检验报告单。
——————————————————————————————————————————————
转移登记：  机动车登记证书，机动车行驶证，机动车来源证明，买卖双方身份证明，代理人身份证明，委托书。
——————————————————————————————————————————————
抵押登记：  机动车登记证书，机动车所有人身份证明，代理人身份证明，委托书，抵押银行营业执照原件副本，抵押合同，银行抵押业务办理人员资料证明。
——————————————————————————————————————————————
补换行驶证：  机动车所有人身份证明，代理人身份证明，委托书。更换行驶证的需提供机动车行驶证。
——————————————————————————————————————————————
补换号牌：  机动车所有人身份证明，代理人身份证明，委托书。更换号牌需提供原机动车号牌。
——————————————————————————————————————————————
核发免检标志：  机动车行驶证，交强险副本，机动车所有人身份证明，代理人身份证明，委托书。');
INSERT INTO `system_article` VALUES ('4', '0', '1', '5', '1526396167', '1531307680', '测试2', '<p>用于全面考察被试运用语言的能力，考察能否把所掌握的语言知识要素综合起来并加以运用。</p><p>标准化测试和非标准化测试。</p><p>标准化测验特指采用客观性试题的、标有信度、效度、难度、区分度等各种测量学指标的、建立了常模的、在统一的环境和条件下施测的、由专业机构或专业人士开发或参与、指导开发的测试。主要包括四个环节：1、试题编制的标准化2、测验实施的标准化3、阅卷评分的标准化4、分数转化与解释的标准化。</p><p><a class="lemma-anchor para-title" style="color: rgb(19, 110, 194); position: absolute; top: -50px;"></a><a class="lemma-anchor " style="color: rgb(19, 110, 194); position: absolute; top: -50px;"></a><a class="lemma-anchor " style="color: rgb(19, 110, 194); position: absolute; top: -50px;"></a></p><p><br/></p>');
INSERT INTO `system_article` VALUES ('5', '0', '1', '5', '1531307591', '1538703108', '测试3', '<h2 class="title-text" style="margin: 0px; padding: 0px 8px 0px 18px; font-size: 22px; color: rgb(0, 0, 0); float: left; line-height: 24px; font-weight: 400; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">心理测试aaaaaa</h2><p><a class="edit-icon j-edit-link" style="color: rgb(136, 136, 136); display: block; float: right; height: 22px; line-height: 22px; padding-left: 24px; font-size: 12px; font-family: SimSun; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span class="cmn-icon wiki-lemma-icons wiki-lemma-icons_edit-lemma" style="font-family: baikeFont_layout; -webkit-font-smoothing: antialiased; speak: none; font-variant-numeric: normal; font-variant-east-asian: normal; line-height: 1; outline: 0px; margin: 0px 3px 0px 0px; vertical-align: text-bottom; color: rgb(170, 170, 170);"></span>编辑</a></p><p>心理测试就是通过心理科学方法和手段，对反映在人的行为活动中的心理特征，依据确定的原则进行推论和量化分析，并给以相应的科学指导。</p><p>尽管心理测试是心理学研究的必要手段，在实际的生活中也得到了越来越广泛的应用，然而要注意的是，心理测试只是提供一定的参考。人是发展成长和变化的，心理测试仅仅提供个人在进行测试的那个时间点的状况特点，因此过分夸大心理测评的效果也是不对的。</p><p><br/></p>');

-- ----------------------------
-- Table structure for `system_attachment`
-- ----------------------------

DROP TABLE IF EXISTS `system_attachment`;

CREATE TABLE `system_attachment` (
  `aid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `moduleid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `table` varchar(200) NOT NULL DEFAULT '',
  `key` int(8) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(50) NOT NULL DEFAULT '',
  `filepath` varchar(80) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `fileext` char(10) NOT NULL DEFAULT '',
  `isimage` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sort` int(11) DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0',
  `uploadip` char(15) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM AUTO_INCREMENT=274 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `system_attachment`
-- ----------------------------

INSERT INTO `system_attachment` VALUES ('2', '230', '', '5', '1.jpg', '/Uploads/201307/51e18ef6d514d.jpg', '76950', 'jpg', '1', '0', '1', '1373736694', '127.0.0.1', '1');
INSERT INTO `system_attachment` VALUES ('3', '230', '', '4', '2.jpg', '/Uploads/201307/51e18f0cb42c7.jpg', '62038', 'jpg', '1', '0', '1', '1373736716', '127.0.0.1', '1');
INSERT INTO `system_attachment` VALUES ('4', '230', '', '2', '1.jpg', '/Uploads/201307/51e18f3484266.jpg', '76950', 'jpg', '1', '0', '1', '1373736756', '127.0.0.1', '1');
INSERT INTO `system_attachment` VALUES ('5', '230', '', '3', '2.jpg', '/Uploads/201307/51e18f49d0720.jpg', '62038', 'jpg', '1', '0', '1', '1373736777', '127.0.0.1', '1');
INSERT INTO `system_attachment` VALUES ('6', '230', '', '1', '3.jpg', '/Uploads/201307/51e18f7ad7887.jpg', '72284', 'jpg', '1', '0', '1', '1373736826', '127.0.0.1', '1');
INSERT INTO `system_attachment` VALUES ('11', '7', '', '1', 'pic_01.jpg', '/Uploads/201307/51e4e2d39b1b0.jpg', '4957', 'jpg', '1', '0', '1', '1373954771', '148.56.106.183', '1');
INSERT INTO `system_attachment` VALUES ('68', '7', '', '6', 'chuzhaopian.png', '/Uploads/201308/5209a1b952ada.png', '375532', 'png', '1', '0', '1', '1376362937', '113.105.84.43', '1');
INSERT INTO `system_attachment` VALUES ('72', '7', '', '1', '1376036231.jpg', '/Uploads/201308/5209a34ce9e6b.jpg', '1457', 'jpg', '1', '0', '1', '1376363340', '113.105.84.43', '1');
INSERT INTO `system_attachment` VALUES ('172', '4', '', '0', '70dddc10daaeab1ab987cedb1511d2621426747009.jpg', '/Uploads/201508/55d5956741474.jpg', '30834', 'jpg', '1', '0', '1', '1440060775', '221.233.204.185', '0');
INSERT INTO `system_attachment` VALUES ('175', '29', '', '0', 'detail3.jpg', '/Uploads/201804/5ad17e89075ee.jpg', '22270', 'jpg', '1', '0', '1', '1523678857', '221.233.204.188', '0');
INSERT INTO `system_attachment` VALUES ('176', '16', '', '1', '11111.jpg', '/Uploads/201804/5ad1d9083f466.jpg', '486622', 'jpg', '1', '0', '1', '1523702024', '127.0.0.1', '1');
INSERT INTO `system_attachment` VALUES ('177', '20', '', '1', '11111.jpg', '/Uploads/201804/5ad2d01b8dd9d.jpg', '486622', 'jpg', '1', '0', '1', '1523765275', '127.0.0.1', '1');
INSERT INTO `system_attachment` VALUES ('178', '20', '', '1', 'weixin.jpg', '/Uploads/201804/5ad342e858d49.jpg', '6094', 'jpg', '1', '0', '1', '1523794664', '119.123.60.249', '1');
INSERT INTO `system_attachment` VALUES ('181', '0', '', '0', '1525183598.jpg', './Uploads/201805/', '22270', 'jpg', '0', '0', '0', '1525183598', '148.56.104.132', '1');
INSERT INTO `system_attachment` VALUES ('182', '0', '', '1', '1525183598.jpg', './Uploads/201805/', '24874', 'jpg', '1', '0', '0', '1525183598', '148.56.104.132', '1');
INSERT INTO `system_attachment` VALUES ('183', '0', '0', '0', '1525318762.gif', './Uploads/201805/', '4169', 'gif', '1', '0', '0', '1525318762', '148.56.106.181', '1');
INSERT INTO `system_attachment` VALUES ('184', '0', '0', '0', '1525318786.gif', './Uploads/201805/', '4169', 'gif', '1', '0', '0', '1525318787', '148.56.106.181', '1');
INSERT INTO `system_attachment` VALUES ('185', '0', '0', '0', '1525318851.gif', './Uploads/201805/', '4169', 'gif', '1', '0', '0', '1525318851', '148.56.106.181', '1');
INSERT INTO `system_attachment` VALUES ('186', '0', 'students', '0', '1525330692.jpg', './Uploads/201805/', '17328', 'jpg', '1', '0', '0', '1525330692', '148.56.104.118', '1');
INSERT INTO `system_attachment` VALUES ('187', '0', 'students', '0', '1525330771.jpg', './Uploads/201805/', '23244', 'jpg', '1', '0', '0', '1525330771', '148.56.104.118', '1');
INSERT INTO `system_attachment` VALUES ('188', '0', 'students', '0', '1525331189.jpg', './Uploads/201805/', '23244', 'jpg', '1', '0', '0', '1525331189', '148.56.104.118', '1');
INSERT INTO `system_attachment` VALUES ('189', '0', 'students', '0', '1525331882.jpg', './Uploads/201805/', '23244', 'jpg', '1', '0', '0', '1525331882', '148.56.104.118', '1');
INSERT INTO `system_attachment` VALUES ('190', '0', 'students', '0', '1525331901.jpg', './Uploads/201805/', '24874', 'jpg', '1', '0', '0', '1525331902', '148.56.104.118', '1');
INSERT INTO `system_attachment` VALUES ('191', '0', 'students', '0', '1525332123.jpg', './Uploads/201805/', '23244', 'jpg', '1', '0', '0', '1525332123', '148.56.104.118', '1');
INSERT INTO `system_attachment` VALUES ('192', '0', 'students', '0', '1525332208.jpg', './Uploads/201805/', '19686', 'jpg', '1', '0', '0', '1525332208', '148.56.104.118', '1');
INSERT INTO `system_attachment` VALUES ('193', '0', 'students', '0', '1525333570.jpg', './Uploads/201805/', '19686', 'jpg', '1', '0', '0', '1525333570', '148.56.104.118', '1');
INSERT INTO `system_attachment` VALUES ('194', '0', 'students', '0', '1525333911.jpg', './Uploads/201805/', '24516', 'jpg', '1', '0', '0', '1525333912', '148.56.104.118', '1');
INSERT INTO `system_attachment` VALUES ('195', '0', 'students', '0', '1525334001.jpg', './Uploads/201805/', '23244', 'jpg', '1', '0', '0', '1525334001', '148.56.104.118', '1');
INSERT INTO `system_attachment` VALUES ('196', '0', 'students', '0', '1525337110.jpg', './Uploads/201805/', '4772', 'jpg', '1', '0', '0', '1525337110', '148.56.104.118', '1');
INSERT INTO `system_attachment` VALUES ('197', '0', 'campus', '0', '1525747499.jpg', './Uploads/201805/', '22270', 'jpg', '1', '0', '0', '1525747499', '148.56.106.175', '1');
INSERT INTO `system_attachment` VALUES ('198', '0', '', '0', '1525761789.jpg', './Uploads/201805/', '23244', 'jpg', '1', '0', '0', '1525761789', '221.233.204.188', '1');
INSERT INTO `system_attachment` VALUES ('199', '0', 'teacher', '0', '1526697701.png', './Uploads/201805/', '464', 'png', '1', '0', '0', '1526697701', '127.0.0.1', '1');
INSERT INTO `system_attachment` VALUES ('200', '0', 'teacher', '0', '1526697783.png', './Uploads/201805/', '464', 'png', '1', '0', '0', '1526697783', '127.0.0.1', '1');
INSERT INTO `system_attachment` VALUES ('201', '0', 'teacher', '0', '1526697850.png', './Uploads/201805/', '498', 'png', '1', '0', '0', '1526697850', '127.0.0.1', '1');
INSERT INTO `system_attachment` VALUES ('202', '0', 'students', '0', '1526796550.jpg', './Uploads/201805/', '24516', 'jpg', '1', '0', '0', '1526796551', '148.56.106.174', '1');
INSERT INTO `system_attachment` VALUES ('203', '0', 'students', '0', '1526797934.jpg', './Uploads/201805/', '4772', 'jpg', '1', '0', '0', '1526797934', '148.56.106.174', '1');
INSERT INTO `system_attachment` VALUES ('204', '0', 'students', '0', '1526798141.jpg', './Uploads/201805/', '18962', 'jpg', '1', '0', '0', '1526798141', '148.56.106.174', '1');
INSERT INTO `system_attachment` VALUES ('205', '0', 'students', '0', '1526870240.png', './Uploads/201805/', '464', 'png', '1', '0', '0', '1526870240', '119.123.61.175', '1');
INSERT INTO `system_attachment` VALUES ('206', '0', 'students', '0', '1526870267.png', './Uploads/201805/', '464', 'png', '1', '0', '0', '1526870267', '119.123.61.175', '1');
INSERT INTO `system_attachment` VALUES ('207', '0', 'students', '0', '1526870358.png', './Uploads/201805/', '186', 'png', '1', '0', '0', '1526870358', '119.123.61.175', '1');
INSERT INTO `system_attachment` VALUES ('208', '0', 'students', '0', '1526892686.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1526892686', '219.134.115.66', '1');
INSERT INTO `system_attachment` VALUES ('209', '0', 'students', '0', '1526894182.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1526894182', '219.134.115.66', '1');
INSERT INTO `system_attachment` VALUES ('210', '0', 'students', '0', '1526895447.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1526895447', '219.134.115.66', '1');
INSERT INTO `system_attachment` VALUES ('211', '0', 'students', '0', '1526895992.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1526895992', '219.134.115.66', '1');
INSERT INTO `system_attachment` VALUES ('212', '0', 'students', '0', '1526955187.png', './Uploads/201805/', '464', 'png', '1', '0', '0', '1526955187', '183.11.243.155', '1');
INSERT INTO `system_attachment` VALUES ('213', '0', 'teacher', '0', '1527127573.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1527127574', '219.134.114.153', '1');
INSERT INTO `system_attachment` VALUES ('214', '0', 'teacher', '0', '1527127887.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1527127887', '219.134.114.153', '1');
INSERT INTO `system_attachment` VALUES ('215', '0', 'students', '0', '1527472279.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1527472279', '113.91.43.96', '1');
INSERT INTO `system_attachment` VALUES ('216', '0', 'students', '0', '1527472458.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1527472458', '113.91.43.96', '1');
INSERT INTO `system_attachment` VALUES ('217', '0', '', '0', '1527494043.png', './Uploads/201805/', '464', 'png', '1', '0', '0', '1527494043', '127.0.0.1', '1');
INSERT INTO `system_attachment` VALUES ('218', '0', '', '0', '1527494096.png', './Uploads/201805/', '464', 'png', '1', '0', '0', '1527494096', '127.0.0.1', '1');
INSERT INTO `system_attachment` VALUES ('219', '0', '', '0', '1527494299.jpg', './Uploads/201805/', '6094', 'jpg', '1', '0', '0', '1527494299', '127.0.0.1', '1');
INSERT INTO `system_attachment` VALUES ('220', '0', '', '0', '1527494336.jpg', './Uploads/201805/', '6094', 'jpg', '1', '0', '0', '1527494336', '127.0.0.1', '1');
INSERT INTO `system_attachment` VALUES ('221', '0', 'salesman', '0', '1527495600.jpg', './Uploads/201805/', '6094', 'jpg', '1', '0', '0', '1527495601', '127.0.0.1', '1');
INSERT INTO `system_attachment` VALUES ('222', '0', 'aftersales', '0', '1527495667.jpg', './Uploads/201805/', '6094', 'jpg', '1', '0', '0', '1527495667', '127.0.0.1', '1');
INSERT INTO `system_attachment` VALUES ('223', '0', 'market', '0', '1527556596.jpg', './Uploads/201805/', '22270', 'jpg', '1', '0', '0', '1527556596', '221.233.204.188', '1');
INSERT INTO `system_attachment` VALUES ('224', '0', 'market', '0', '1527556644.jpg', './Uploads/201805/', '22270', 'jpg', '1', '0', '0', '1527556644', '221.233.204.188', '1');
INSERT INTO `system_attachment` VALUES ('225', '0', 'market', '0', '1527559752.jpg', './Uploads/201805/', '6094', 'jpg', '1', '0', '0', '1527559752', '121.35.99.217', '1');
INSERT INTO `system_attachment` VALUES ('226', '0', 'market', '0', '1527559759.jpg', './Uploads/201805/', '6094', 'jpg', '1', '0', '0', '1527559759', '121.35.99.217', '1');
INSERT INTO `system_attachment` VALUES ('227', '0', 'market', '0', '1527559770.jpg', './Uploads/201805/', '6094', 'jpg', '1', '0', '0', '1527559770', '121.35.99.217', '1');
INSERT INTO `system_attachment` VALUES ('228', '0', 'market', '0', '1527559819.jpg', './Uploads/201805/', '6094', 'jpg', '1', '0', '0', '1527559819', '121.35.99.217', '1');
INSERT INTO `system_attachment` VALUES ('229', '0', 'salesman', '0', '1527559823.jpg', './Uploads/201805/', '23244', 'jpg', '1', '0', '0', '1527559823', '221.233.204.188', '1');
INSERT INTO `system_attachment` VALUES ('230', '0', 'teacher', '0', '1527560447.jpg', './Uploads/201805/', '6094', 'jpg', '1', '0', '0', '1527560447', '121.35.99.217', '1');
INSERT INTO `system_attachment` VALUES ('231', '0', 'teacher', '0', '1527560477.jpg', './Uploads/201805/', '6094', 'jpg', '1', '0', '0', '1527560477', '121.35.99.217', '1');
INSERT INTO `system_attachment` VALUES ('232', '0', 'teacher', '0', '1527560691.jpg', './Uploads/201805/', '6094', 'jpg', '1', '0', '0', '1527560691', '121.35.99.217', '1');
INSERT INTO `system_attachment` VALUES ('233', '0', 'teacher', '0', '1527560835.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1527560835', '113.91.43.215', '1');
INSERT INTO `system_attachment` VALUES ('234', '0', 'market', '0', '1527560966.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1527560966', '113.91.43.215', '1');
INSERT INTO `system_attachment` VALUES ('235', '0', 'aftersales', '0', '1527561068.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1527561068', '113.91.43.215', '1');
INSERT INTO `system_attachment` VALUES ('236', '0', 'students', '0', '1527561118.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1527561118', '113.91.43.215', '1');
INSERT INTO `system_attachment` VALUES ('237', '0', 'teacher', '0', '1527561246.png', './Uploads/201805/', '464', 'png', '1', '0', '0', '1527561246', '121.35.99.217', '1');
INSERT INTO `system_attachment` VALUES ('238', '0', 'market', '0', '1527577487.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1527577487', '119.123.35.92', '1');
INSERT INTO `system_attachment` VALUES ('239', '0', 'teacher', '0', '1527577596.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1527577596', '119.123.35.92', '1');
INSERT INTO `system_attachment` VALUES ('240', '0', 'salesman', '0', '1527577764.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1527577764', '119.123.35.92', '1');
INSERT INTO `system_attachment` VALUES ('241', '0', 'teacher', '0', '1527577793.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1527577793', '119.123.35.92', '1');
INSERT INTO `system_attachment` VALUES ('242', '0', 'teacher', '0', '1527577811.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1527577811', '119.123.35.92', '1');
INSERT INTO `system_attachment` VALUES ('243', '0', 'salesman', '0', '1527577827.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1527577827', '119.123.35.92', '1');
INSERT INTO `system_attachment` VALUES ('244', '0', 'aftersales', '0', '1527577836.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1527577836', '119.123.35.92', '1');
INSERT INTO `system_attachment` VALUES ('245', '0', 'students', '0', '1527577856.png', './Uploads/201805/', '87850', 'png', '1', '0', '0', '1527577856', '119.123.35.92', '1');
INSERT INTO `system_attachment` VALUES ('246', '0', 'students', '0', '1527818411.png', './Uploads/201806/', '87850', 'png', '1', '0', '0', '1527818411', '113.118.224.35', '1');
INSERT INTO `system_attachment` VALUES ('247', '0', 'students', '0', '1527818456.png', './Uploads/201806/', '87850', 'png', '1', '0', '0', '1527818456', '113.118.224.35', '1');
INSERT INTO `system_attachment` VALUES ('248', '0', 'students', '0', '1527818512.png', './Uploads/201806/', '87850', 'png', '1', '0', '0', '1527818512', '113.118.224.35', '1');
INSERT INTO `system_attachment` VALUES ('249', '0', 'students', '0', '1527821898.png', './Uploads/201806/', '87850', 'png', '1', '0', '0', '1527821898', '113.118.224.35', '1');
INSERT INTO `system_attachment` VALUES ('250', '0', 'students', '0', '1527821971.png', './Uploads/201806/', '87850', 'png', '1', '0', '0', '1527821971', '113.118.224.35', '1');
INSERT INTO `system_attachment` VALUES ('251', '0', 'students', '0', '1528161713.png', './Uploads/201806/', '87850', 'png', '1', '0', '0', '1528161713', '119.123.43.90', '1');
INSERT INTO `system_attachment` VALUES ('252', '0', 'students', '0', '1528941137.png', './Uploads/201806/', '87850', 'png', '1', '0', '0', '1528941137', '113.91.40.146', '1');
INSERT INTO `system_attachment` VALUES ('253', '0', 'teacher', '0', '1528941601.png', './Uploads/201806/', '87850', 'png', '1', '0', '0', '1528941601', '113.91.40.146', '1');
INSERT INTO `system_attachment` VALUES ('254', '0', 'market', '0', '1528941693.png', './Uploads/201806/', '87850', 'png', '1', '0', '0', '1528941693', '113.91.40.146', '1');
INSERT INTO `system_attachment` VALUES ('255', '0', 'salesman', '0', '1528941797.png', './Uploads/201806/', '87850', 'png', '1', '0', '0', '1528941797', '113.91.40.146', '1');
INSERT INTO `system_attachment` VALUES ('256', '0', 'aftersales', '0', '1528941895.png', './Uploads/201806/', '87850', 'png', '1', '0', '0', '1528941895', '113.91.40.146', '1');
INSERT INTO `system_attachment` VALUES ('257', '0', 'member', '0', '1538812026.gif', './Uploads/201810/', '132135', 'gif', '1', '0', '0', '1538812026', '221.233.204.188', '1');
INSERT INTO `system_attachment` VALUES ('258', '0', 'member', '0', '1538812033.jpg', './Uploads/201810/', '24516', 'jpg', '1', '0', '0', '1538812033', '221.233.204.188', '1');
INSERT INTO `system_attachment` VALUES ('259', '0', 'member', '0', '1538812228.jpg', './Uploads/201810/', '23244', 'jpg', '1', '0', '0', '1538812228', '221.233.204.188', '1');
INSERT INTO `system_attachment` VALUES ('260', '0', 'member', '0', '1538812230.jpg', './Uploads/201810/', '19686', 'jpg', '1', '0', '0', '1538812230', '221.233.204.188', '1');
INSERT INTO `system_attachment` VALUES ('261', '0', 'member', '0', '1538812509.jpg', './Uploads/201810/', '23244', 'jpg', '1', '0', '0', '1538812509', '221.233.204.188', '1');
INSERT INTO `system_attachment` VALUES ('262', '0', 'member', '0', '1538812618.jpg', './Uploads/201810/', '23244', 'jpg', '1', '0', '0', '1538812618', '221.233.204.188', '1');
INSERT INTO `system_attachment` VALUES ('263', '0', 'member', '0', '1538812621.jpg', './Uploads/201810/', '42322', 'jpg', '1', '0', '0', '1538812621', '221.233.204.188', '1');
INSERT INTO `system_attachment` VALUES ('264', '0', 'member', '0', '1538812686.jpg', './Uploads/201810/', '23244', 'jpg', '1', '0', '0', '1538812686', '221.233.204.188', '1');
INSERT INTO `system_attachment` VALUES ('265', '0', 'member', '0', '1538813014.jpg', './Uploads/201810/', '19686', 'jpg', '1', '0', '0', '1538813014', '221.233.204.188', '1');
INSERT INTO `system_attachment` VALUES ('266', '0', 'member', '0', '1538813027.jpg', './Uploads/201810/', '19686', 'jpg', '1', '0', '0', '1538813027', '221.233.204.188', '1');
INSERT INTO `system_attachment` VALUES ('267', '0', 'member', '0', '1538986681.jpg', './Uploads/201810/', '154497', 'jpg', '1', '0', '0', '1538986681', '121.14.14.241', '1');
INSERT INTO `system_attachment` VALUES ('268', '0', 'member', '0', '1538986766.jpg', './Uploads/201810/', '137181', 'jpg', '1', '0', '0', '1538986766', '221.233.204.188', '1');
INSERT INTO `system_attachment` VALUES ('269', '0', 'member', '0', '1538987409.jpg', './Uploads/201810/', '154497', 'jpg', '1', '0', '0', '1538987409', '121.14.14.243', '1');
INSERT INTO `system_attachment` VALUES ('270', '0', 'member', '0', '1538988200.jpg', './Uploads/201810/', '16917', 'jpg', '1', '0', '0', '1538988200', '221.233.204.188', '1');
INSERT INTO `system_attachment` VALUES ('271', '0', 'member', '0', '1538988247.jpg', './Uploads/201810/', '137181', 'jpg', '1', '0', '0', '1538988247', '221.233.204.188', '1');
INSERT INTO `system_attachment` VALUES ('272', '0', 'member', '0', '1538988249.jpg', './Uploads/201810/', '16917', 'jpg', '1', '0', '0', '1538988249', '221.233.204.188', '1');
INSERT INTO `system_attachment` VALUES ('273', '0', 'member', '0', '1538988646.jpg', './Uploads/201810/', '137181', 'jpg', '1', '0', '0', '1538988646', '221.233.204.188', '1');

-- ----------------------------
-- Table structure for `system_auth`
-- ----------------------------

DROP TABLE IF EXISTS `system_auth`;

CREATE TABLE `system_auth` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL COMMENT '权限名称',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '状态(1:禁用,2:启用)',
  `sort` smallint(6) unsigned DEFAULT '0' COMMENT '排序权重',
  `desc` varchar(255) DEFAULT NULL COMMENT '备注说明',
  `create_by` bigint(11) unsigned DEFAULT '0' COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_system_auth_title` (`title`) USING BTREE,
  KEY `index_system_auth_status` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='系统权限表';

-- ----------------------------
-- Data for the table `system_auth`
-- ----------------------------

INSERT INTO `system_auth` VALUES ('1', '分校权限', '1', '0', '分校权限', '0', '2018-05-10 16:47:51');
INSERT INTO `system_auth` VALUES ('2', '老师', '1', '0', '老师', '0', '2018-07-05 22:56:27');

-- ----------------------------
-- Table structure for `system_auth_node`
-- ----------------------------

DROP TABLE IF EXISTS `system_auth_node`;

CREATE TABLE `system_auth_node` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `auth` bigint(20) unsigned DEFAULT NULL COMMENT '角色ID',
  `node` varchar(200) DEFAULT NULL COMMENT '节点路径',
  PRIMARY KEY (`id`),
  KEY `index_system_auth_auth` (`auth`) USING BTREE,
  KEY `index_system_auth_node` (`node`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8 COMMENT='系统角色与节点绑定';

-- ----------------------------
-- Data for the table `system_auth_node`
-- ----------------------------

INSERT INTO `system_auth_node` VALUES ('156', '1', 'admin');
INSERT INTO `system_auth_node` VALUES ('157', '1', 'admin/classes');
INSERT INTO `system_auth_node` VALUES ('158', '1', 'admin/classes/index');
INSERT INTO `system_auth_node` VALUES ('159', '2', 'admin');
INSERT INTO `system_auth_node` VALUES ('160', '2', 'admin/log');
INSERT INTO `system_auth_node` VALUES ('161', '2', 'admin/log/index');
INSERT INTO `system_auth_node` VALUES ('162', '2', 'admin/log/sms');
INSERT INTO `system_auth_node` VALUES ('163', '2', 'admin/log/del');
INSERT INTO `system_auth_node` VALUES ('164', '2', 'admin/log/gettagslist');

-- ----------------------------
-- Table structure for `system_config`
-- ----------------------------

DROP TABLE IF EXISTS `system_config`;

CREATE TABLE `system_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '配置编码',
  `value` varchar(500) DEFAULT NULL COMMENT '配置值',
  PRIMARY KEY (`id`),
  KEY `index_system_config_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统参数配置';

-- ----------------------------
-- Data for the table `system_config`
-- ----------------------------

INSERT INTO `system_config` VALUES ('1', 'app_name', '非凡脑力');
INSERT INTO `system_config` VALUES ('2', 'site_name', '非凡脑力');
INSERT INTO `system_config` VALUES ('3', 'app_version', '1.0 dev');
INSERT INTO `system_config` VALUES ('4', 'site_copy', '©版权所有 2014-2018 非凡脑力');
INSERT INTO `system_config` VALUES ('5', 'browser_icon', 'http://ivehice.mydanweb.com/static/upload/e618480087d3066b/8279d68753fb602f.png');
INSERT INTO `system_config` VALUES ('6', 'tongji_baidu_key', '');
INSERT INTO `system_config` VALUES ('7', 'miitbeian', '粤ICP备16006642号-2');
INSERT INTO `system_config` VALUES ('8', 'storage_type', 'local');
INSERT INTO `system_config` VALUES ('9', 'storage_local_exts', 'png,jpg,rar,doc,icon,mp4');
INSERT INTO `system_config` VALUES ('10', 'storage_qiniu_bucket', '');
INSERT INTO `system_config` VALUES ('11', 'storage_qiniu_domain', '');
INSERT INTO `system_config` VALUES ('12', 'storage_qiniu_access_key', '');
INSERT INTO `system_config` VALUES ('13', 'storage_qiniu_secret_key', '');
INSERT INTO `system_config` VALUES ('14', 'storage_oss_bucket', 'cuci');
INSERT INTO `system_config` VALUES ('15', 'storage_oss_endpoint', 'oss-cn-beijing.aliyuncs.com');
INSERT INTO `system_config` VALUES ('16', 'storage_oss_domain', 'cuci.oss-cn-beijing.aliyuncs.com');
INSERT INTO `system_config` VALUES ('17', 'storage_oss_keyid', '用你自己的吧');
INSERT INTO `system_config` VALUES ('18', 'storage_oss_secret', '用你自己的吧');
INSERT INTO `system_config` VALUES ('34', 'wechat_appid', 'wx60a43dd8161666d4');
INSERT INTO `system_config` VALUES ('35', 'wechat_appkey', '9890a0d7c91801a609d151099e95b61a');
INSERT INTO `system_config` VALUES ('36', 'storage_oss_is_https', 'http');
INSERT INTO `system_config` VALUES ('37', 'wechat_type', 'api');
INSERT INTO `system_config` VALUES ('38', 'wechat_token', 'test');
INSERT INTO `system_config` VALUES ('39', 'wechat_appsecret', 'a041bec98ed015d52b99acea5c6a16ef');
INSERT INTO `system_config` VALUES ('40', 'wechat_encodingaeskey', 'BJIUzE0gqlWy0GxfPp4J1oPTBmOrNDIGPNav1YFH5Z5');
INSERT INTO `system_config` VALUES ('41', 'wechat_thr_appid', 'wx60a43dd8161666d4');
INSERT INTO `system_config` VALUES ('42', 'wechat_thr_appkey', '05db2aa335382c66ab56d69b1a9ad0ee');

-- ----------------------------
-- Table structure for `system_crontab`
-- ----------------------------

DROP TABLE IF EXISTS `system_crontab`;

CREATE TABLE `system_crontab` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cron_time` varchar(255) NOT NULL DEFAULT '',
  `cron_class` varchar(255) NOT NULL DEFAULT '',
  `cron_method` varchar(255) NOT NULL DEFAULT '',
  `memo` varchar(255) NOT NULL DEFAULT '',
  `sort` int(10) unsigned NOT NULL DEFAULT '90',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `system_crontab`
-- ----------------------------

INSERT INTO `system_crontab` VALUES ('3', '2 * * * *', 'Backup', 'testBackup', '测试使用', '90', '1', '1538746439', '1538746439');
INSERT INTO `system_crontab` VALUES ('4', '0 2 * * *', 'Backup', 'dbBackup', '定时备份系统数据库, 0 2 * * * 每天凌晨2点执行', '90', '1', '1538882380', '1538882380');

-- ----------------------------
-- Table structure for `system_field`
-- ----------------------------

DROP TABLE IF EXISTS `system_field`;

CREATE TABLE `system_field` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `mo_uuid` char(50) DEFAULT NULL,
  `tb_name` varchar(100) DEFAULT '',
  `field` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `tips` varchar(150) NOT NULL DEFAULT '',
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `minlength` int(10) unsigned NOT NULL DEFAULT '0',
  `maxlength` int(10) unsigned NOT NULL DEFAULT '0',
  `pattern` varchar(500) NOT NULL DEFAULT '' COMMENT '正式则',
  `errormsg` varchar(255) NOT NULL DEFAULT '',
  `class` varchar(20) NOT NULL DEFAULT '',
  `type` varchar(20) NOT NULL DEFAULT '',
  `setup` mediumtext,
  `ispost` tinyint(1) unsigned DEFAULT '0',
  `sort` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `issystem` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `queryfield` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0表示不显示在查询，1表示显示',
  `listShowField` tinyint(1) NOT NULL DEFAULT '1' COMMENT '列表显示,0表示不显示',
  `editShowField` tinyint(1) NOT NULL DEFAULT '1',
  `detailfield` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否详情显示',
  `inputwidth` int(5) unsigned NOT NULL DEFAULT '80' COMMENT '列宽度 ',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `mo_uuid` (`mo_uuid`)
) ENGINE=MyISAM AUTO_INCREMENT=154 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `system_field`
-- ----------------------------

INSERT INTO `system_field` VALUES ('153', '5bb81d8f13a4b', 'member', 'headimage', '头像', '', '0', '0', '0', '', '', '', 'images', 'array (
  \'default\' => \'\',
  \'upload_maxnum\' => \'3\',
  \'upload_maxsize\' => \'1\',
  \'upload_allowext\' => \'jpg,jpeg,gif,png\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('152', '5bb822abb6bb8', 'system_areabase', 'p_id', '父ID', '', '0', '0', '10', '', '', '', 'number', 'array (
  \'size\' => \'10\',
  \'numbertype\' => \'1\',
  \'decimaldigits\' => \'0\',
  \'default\' => \'\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('151', '5bb822abb6bb8', 'system_areabase', 'name', '名称', '', '0', '0', '100', '', '', '', 'text', 'array (
  \'size\' => \'10\',
  \'default\' => \'\',
  \'ispassword\' => \'0\',
  \'fieldtype\' => \'varchar\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '200', '');
INSERT INTO `system_field` VALUES ('39', '5bb5868dc6796', 'system_article', 'id', 'ID', '', '0', '0', '0', '', '', '', 'number', '', '0', '0', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('40', '5bb5868dc6796', 'system_article', 'sort', '排序', '', '0', '0', '0', '', '', '', 'number', '', '0', '100', '1', '1', '0', '0', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('41', '5bb5868dc6796', 'system_article', 'createtime', '发布时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '0', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('42', '5bb5868dc6796', 'system_article', 'updatetime', '更新时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '0', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('43', '5bb5868dc6796', 'system_article', 'status', '状态', '', '0', '0', '1', '', '', '', 'radio', 'array (
  \'options\' => \'有效|1
无效|0\',
  \'fieldtype\' => \'tinyint\',
  \'numbertype\' => \'1\',
  \'labelwidth\' => \'\',
  \'default\' => \'1\',
)', '0', '100', '1', '1', '0', '0', '0', '0', '80', '');
INSERT INTO `system_field` VALUES ('44', '5bb5868dc6796', 'system_article', 'title', '标题', '', '1', '0', '0', '', '', '', 'title', '', '0', '0', '1', '0', '0', '0', '0', '0', '80', '');
INSERT INTO `system_field` VALUES ('45', '5bb5868dc6796', 'system_article', 'typeId', '文章类型', '', '0', '0', '0', '', '', '', 'typeid', 'array (
  \'inputtype\' => \'select\',
  \'fieldtype\' => \'tinyint\',
  \'numbertype\' => \'1\',
  \'labelwidth\' => \'\',
  \'default\' => \'1\',
  \'tpl\' => \'\',
)', '0', '0', '1', '0', '0', '0', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('46', '5bb5868dc6796', 'system_article', 'description', '文章内容', '', '0', '0', '0', '', '', '', 'editor', 'array (
  \'edittype\' => \'kindeditor\',
  \'toolbar\' => \'full\',
  \'default\' => \'\',
  \'height\' => \'\',
  \'show_add_description\' => \'0\',
  \'show_auto_thumb\' => \'0\',
  \'showpage\' => \'0\',
  \'enablekeylink\' => \'0\',
  \'replacenum\' => \'\',
  \'enablesaveimage\' => \'0\',
  \'flashupload\' => \'0\',
  \'alowuploadexts\' => \'\',
  \'alowuploadlimit\' => \'\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('47', '5bb5868dc6797', 'system_message', 'id', 'ID', '', '0', '0', '0', '', '', '', 'number', '', '0', '0', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('48', '5bb5868dc6797', 'system_message', 'sort', '排序', '', '0', '0', '0', '', '', '', 'number', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('49', '5bb5868dc6797', 'system_message', 'createtime', '发布时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '0', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('50', '5bb5868dc6797', 'system_message', 'updatetime', '更新时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '120', '');
INSERT INTO `system_field` VALUES ('51', '5bb5868dc6797', 'system_message', 'status', '状态', '', '0', '0', '0', '', '', '', 'radio', 'array (
  \'options\' => \'有效|1
无效|0\',
  \'fieldtype\' => \'varchar\',
  \'numbertype\' => \'1\',
  \'labelwidth\' => \'\',
  \'default\' => \'1\',
)', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('52', '5bb5868dc6797', 'system_message', 'title', '标题', '', '1', '0', '0', '', '', '', 'title', 'array (
  \'thumb\' => \'0\',
  \'style\' => \'0\',
  \'size\' => \'\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('53', '5bb5868dc6797', 'system_message', 'memo', '内容', '', '0', '0', '0', '', '', '', 'textarea', 'array (
  \'fieldtype\' => \'mediumtext\',
  \'rows\' => \'\',
  \'cols\' => \'\',
  \'default\' => \'\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '300', '');
INSERT INTO `system_field` VALUES ('54', '5bb5868dc6797', 'system_message', 'ib_read', '已读', '', '0', '0', '0', '', '', '', 'number', 'array (
  \'size\' => \'\',
  \'numbertype\' => \'1\',
  \'decimaldigits\' => \'0\',
  \'default\' => \'0\',
)', '0', '0', '1', '0', '1', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('55', '5bb5868dc6797', 'system_message', 'user_id', '用户', '', '0', '0', '0', '', '', '', 'model_select', 'array (
  \'multiple\' => \'0\',
  \'fieldtype\' => \'int\',
  \'numbertype\' => \'1\',
  \'size\' => \'\',
  \'bindModule\' => \'users||username\',
)', '0', '0', '1', '0', '1', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('150', '5bb822abb6bb8', 'system_areabase', 'area_bn', '省市区编号', '', '0', '0', '10', '', '', '', 'number', 'array (
  \'size\' => \'10\',
  \'numbertype\' => \'1\',
  \'decimaldigits\' => \'0\',
  \'default\' => \'\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('149', '5bb822abb6bb8', 'system_areabase', 'status', '状态', '', '0', '0', '0', '', '', '', 'radio', 'array (\'options\' => \'有效|1\',\'fieldtype\' => \'varchar\',\'numbertype\' => \'1\',\'labelwidth\' => \'\',\'default\' => \'1\')', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('148', '5bb822abb6bb8', 'system_areabase', 'updatetime', '更新时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('146', '5bb822abb6bb8', 'system_areabase', 'sort', '排序', '', '0', '0', '0', '', '', '', 'number', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('147', '5bb822abb6bb8', 'system_areabase', 'createtime', '发布时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('145', '5bb822abb6bb8', 'system_areabase', 'id', 'ID', '', '0', '0', '0', '', '', '', 'number', '', '0', '0', '1', '1', '0', '1', '1', '0', '150', '');
INSERT INTO `system_field` VALUES ('144', '5bb81d8f13a4b', 'member', 'regional_Linkage', '地区', '', '1', '4', '20', '', '', '', 'regional_Linkage', '', '0', '0', '1', '0', '0', '1', '1', '0', '200', '');
INSERT INTO `system_field` VALUES ('143', '5bb81d8f13a4b', 'member', 'name', '姓名', '', '0', '0', '0', '', '', '', 'title', 'array (
  \'thumb\' => \'0\',
  \'style\' => \'0\',
  \'size\' => \'10\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('141', '5bb81d8f13a4b', 'member', 'updatetime', '更新时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('142', '5bb81d8f13a4b', 'member', 'status', '状态', '', '0', '0', '0', '', '', '', 'radio', 'array (\'options\' => \'有效|1\',\'fieldtype\' => \'varchar\',\'numbertype\' => \'1\',\'labelwidth\' => \'\',\'default\' => \'1\')', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('140', '5bb81d8f13a4b', 'member', 'createtime', '发布时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('138', '5bb81d8f13a4b', 'member', 'id', 'ID', '', '0', '0', '0', '', '', '', 'number', '', '0', '0', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('139', '5bb81d8f13a4b', 'member', 'sort', '排序', '', '0', '0', '0', '', '', '', 'number', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('137', '5bb815d5754e8', 'system_article', 'typeid', '类别', '', '0', '0', '0', '', '', '', 'typeid', 'array (
  \'inputtype\' => \'select\',
  \'fieldtype\' => \'tinyint\',
  \'numbertype\' => \'1\',
  \'labelwidth\' => \'\',
  \'default\' => \'2\',
)', '0', '20', '1', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('134', '5bb815d5754e8', 'system_article', 'status', '状态', '', '0', '0', '0', '', '', '', 'radio', 'array (\'options\' => \'有效|1\',\'fieldtype\' => \'varchar\',\'numbertype\' => \'1\',\'labelwidth\' => \'\',\'default\' => \'1\')', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('81', '5bb5868dc67910', 'system_crontab', 'cron_class', '类名', '', '0', '0', '0', '', '', '', 'text', 'array (
  \'size\' => \'\',
  \'default\' => \'\',
  \'ispassword\' => \'0\',
  \'fieldtype\' => \'varchar\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '类名');
INSERT INTO `system_field` VALUES ('82', '5bb5868dc67910', 'system_crontab', 'cron_method', '事件', '', '0', '0', '0', '', '', '', 'text', 'array (
  \'size\' => \'\',
  \'default\' => \'\',
  \'ispassword\' => \'0\',
  \'fieldtype\' => \'varchar\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('83', '5bb5868dc67910', 'system_crontab', 'cron_time', '执行时间', '', '0', '0', '0', '', '', '', 'text', 'array (
  \'size\' => \'\',
  \'default\' => \'\',
  \'ispassword\' => \'0\',
  \'fieldtype\' => \'varchar\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '执行时间');
INSERT INTO `system_field` VALUES ('84', '5bb5868dc67910', 'system_crontab', 'id', 'ID', '', '0', '0', '0', '', '', '', 'number', '', '0', '0', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('85', '5bb5868dc67910', 'system_crontab', 'memo', '备注', '', '0', '0', '0', '', '', '', 'text', 'array (
  \'size\' => \'\',
  \'default\' => \'\',
  \'ispassword\' => \'0\',
  \'fieldtype\' => \'varchar\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('86', '5bb5868dc67910', 'system_crontab', 'sort', '排序', '', '0', '0', '0', '', '', '', 'number', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('87', '5bb5868dc67910', 'system_crontab', 'updatetime', '更新时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('88', '5bb5868dc67910', 'system_crontab', 'status', '状态', '', '0', '0', '0', '', '', '', 'select', 'array (
  \'options\' => \'有效|1
无效|0\',
  \'multiple\' => \'0\',
  \'fieldtype\' => \'tinyint\',
  \'numbertype\' => \'1\',
  \'size\' => \'\',
  \'default\' => \'\',
)', '0', '0', '0', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('136', '5bb815d5754e8', 'system_article', 'description', '明细', '', '0', '0', '0', '', '', '', 'editor', 'array (
  \'edittype\' => \'kindeditor\',
  \'toolbar\' => \'full\',
  \'default\' => \'\',
  \'height\' => \'\',
  \'show_add_description\' => \'0\',
  \'show_auto_thumb\' => \'0\',
  \'showpage\' => \'0\',
  \'enablekeylink\' => \'0\',
  \'replacenum\' => \'\',
  \'enablesaveimage\' => \'0\',
  \'flashupload\' => \'0\',
  \'alowuploadexts\' => \'\',
  \'alowuploadlimit\' => \'\',
)', '0', '10', '1', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('119', '5bb7193364b15', 'member', 'sort', '排序', '', '0', '0', '0', '', '', '', 'number', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('120', '5bb7193364b15', 'member', 'createtime', '发布时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('135', '5bb815d5754e8', 'system_article', 'title', '标题', '', '1', '0', '1000', '', '', '', 'title', 'array (
  \'thumb\' => \'0\',
  \'style\' => \'0\',
  \'size\' => \'10\',
)', '0', '5', '1', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('121', '5bb7193364b15', 'member', 'updatetime', '更新时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('133', '5bb815d5754e8', 'system_article', 'updatetime', '更新时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('132', '5bb815d5754e8', 'system_article', 'createtime', '发布时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('131', '5bb815d5754e8', 'system_article', 'sort', '排序', '', '0', '0', '0', '', '', '', 'number', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('130', '5bb815d5754e8', 'system_article', 'id', 'ID', '', '0', '0', '0', '', '', '', 'number', '', '0', '0', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field` VALUES ('118', '5bb7193364b15', 'member', 'id', 'ID', '', '0', '0', '0', '', '', '', 'number', '', '0', '0', '1', '1', '0', '1', '1', '0', '80', '');

-- ----------------------------
-- Table structure for `system_field_cache`
-- ----------------------------

DROP TABLE IF EXISTS `system_field_cache`;

CREATE TABLE `system_field_cache` (
  `tb_name` varchar(100) NOT NULL,
  `content` text,
  PRIMARY KEY (`tb_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `system_field_cache`
-- ----------------------------

INSERT INTO `system_field_cache` VALUES ('member', '{"headimage":{"id":153,"mo_uuid":"5bb81d8f13a4b","tb_name":"member","field":"headimage","name":"头像","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"images","setup":"array (\n  \'default\' => \'\',\n  \'upload_maxnum\' => \'3\',\n  \'upload_maxsize\' => \'1\',\n  \'upload_allowext\' => \'jpg,jpeg,gif,png\',\n)","ispost":0,"sort":0,"status":1,"issystem":0,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"regional_Linkage":{"id":144,"mo_uuid":"5bb81d8f13a4b","tb_name":"member","field":"regional_Linkage","name":"地区","tips":"","required":1,"minlength":4,"maxlength":20,"pattern":"","errormsg":"","class":"","type":"regional_Linkage","setup":null,"ispost":0,"sort":0,"status":1,"issystem":0,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":200,"remark":""},"name":{"id":143,"mo_uuid":"5bb81d8f13a4b","tb_name":"member","field":"name","name":"姓名","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"title","setup":"array (\n  \'thumb\' => \'0\',\n  \'style\' => \'0\',\n  \'size\' => \'10\',\n)","ispost":0,"sort":0,"status":1,"issystem":0,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"id":{"id":138,"mo_uuid":"5bb81d8f13a4b","tb_name":"member","field":"id","name":"ID","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"number","setup":null,"ispost":0,"sort":0,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"updatetime":{"id":141,"mo_uuid":"5bb81d8f13a4b","tb_name":"member","field":"updatetime","name":"更新时间","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"datetime","setup":null,"ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"status":{"id":142,"mo_uuid":"5bb81d8f13a4b","tb_name":"member","field":"status","name":"状态","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"radio","setup":"array (\'options\' => \'有效|1\',\'fieldtype\' => \'varchar\',\'numbertype\' => \'1\',\'labelwidth\' => \'\',\'default\' => \'1\')","ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"createtime":{"id":140,"mo_uuid":"5bb81d8f13a4b","tb_name":"member","field":"createtime","name":"发布时间","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"datetime","setup":null,"ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"sort":{"id":139,"mo_uuid":"5bb81d8f13a4b","tb_name":"member","field":"sort","name":"排序","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"number","setup":null,"ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null}}');
INSERT INTO `system_field_cache` VALUES ('member_attachmentCache', '[{"aid":273,"moduleid":0,"table":"member","key":0,"filename":"1538988646.jpg","filepath":".\/Uploads\/201810\/","filesize":137181,"fileext":"jpg","isimage":1,"sort":0,"userid":0,"createtime":1538988646,"uploadip":"221.233.204.188","status":1}]');
INSERT INTO `system_field_cache` VALUES ('system_areabase', '{"p_id":{"id":152,"mo_uuid":"5bb822abb6bb8","tb_name":"system_areabase","field":"p_id","name":"父ID","tips":"","required":0,"minlength":0,"maxlength":10,"pattern":"","errormsg":"","class":"","type":"number","setup":"array (\n  \'size\' => \'10\',\n  \'numbertype\' => \'1\',\n  \'decimaldigits\' => \'0\',\n  \'default\' => \'\',\n)","ispost":0,"sort":0,"status":1,"issystem":0,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"name":{"id":151,"mo_uuid":"5bb822abb6bb8","tb_name":"system_areabase","field":"name","name":"名称","tips":"","required":0,"minlength":0,"maxlength":100,"pattern":"","errormsg":"","class":"","type":"text","setup":"array (\n  \'size\' => \'10\',\n  \'default\' => \'\',\n  \'ispassword\' => \'0\',\n  \'fieldtype\' => \'varchar\',\n)","ispost":0,"sort":0,"status":1,"issystem":0,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":200,"remark":""},"area_bn":{"id":150,"mo_uuid":"5bb822abb6bb8","tb_name":"system_areabase","field":"area_bn","name":"省市区编号","tips":"","required":0,"minlength":0,"maxlength":10,"pattern":"","errormsg":"","class":"","type":"number","setup":"array (\n  \'size\' => \'10\',\n  \'numbertype\' => \'1\',\n  \'decimaldigits\' => \'0\',\n  \'default\' => \'\',\n)","ispost":0,"sort":0,"status":1,"issystem":0,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"id":{"id":145,"mo_uuid":"5bb822abb6bb8","tb_name":"system_areabase","field":"id","name":"ID","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"number","setup":null,"ispost":0,"sort":0,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":150,"remark":null},"status":{"id":149,"mo_uuid":"5bb822abb6bb8","tb_name":"system_areabase","field":"status","name":"状态","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"radio","setup":"array (\'options\' => \'有效|1\',\'fieldtype\' => \'varchar\',\'numbertype\' => \'1\',\'labelwidth\' => \'\',\'default\' => \'1\')","ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"updatetime":{"id":148,"mo_uuid":"5bb822abb6bb8","tb_name":"system_areabase","field":"updatetime","name":"更新时间","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"datetime","setup":null,"ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"sort":{"id":146,"mo_uuid":"5bb822abb6bb8","tb_name":"system_areabase","field":"sort","name":"排序","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"number","setup":null,"ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"createtime":{"id":147,"mo_uuid":"5bb822abb6bb8","tb_name":"system_areabase","field":"createtime","name":"发布时间","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"datetime","setup":null,"ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null}}');
INSERT INTO `system_field_cache` VALUES ('system_article', '{"id":{"id":130,"mo_uuid":"5bb815d5754e8","tb_name":"system_article","field":"id","name":"ID","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"number","setup":null,"ispost":0,"sort":0,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"title":{"id":135,"mo_uuid":"5bb815d5754e8","tb_name":"system_article","field":"title","name":"标题","tips":"","required":1,"minlength":0,"maxlength":1000,"pattern":"","errormsg":"","class":"","type":"title","setup":"array (\n  \'thumb\' => \'0\',\n  \'style\' => \'0\',\n  \'size\' => \'10\',\n)","ispost":0,"sort":5,"status":1,"issystem":0,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"description":{"id":136,"mo_uuid":"5bb815d5754e8","tb_name":"system_article","field":"description","name":"明细","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"editor","setup":"array (\n  \'edittype\' => \'kindeditor\',\n  \'toolbar\' => \'full\',\n  \'default\' => \'\',\n  \'height\' => \'\',\n  \'show_add_description\' => \'0\',\n  \'show_auto_thumb\' => \'0\',\n  \'showpage\' => \'0\',\n  \'enablekeylink\' => \'0\',\n  \'replacenum\' => \'\',\n  \'enablesaveimage\' => \'0\',\n  \'flashupload\' => \'0\',\n  \'alowuploadexts\' => \'\',\n  \'alowuploadlimit\' => \'\',\n)","ispost":0,"sort":10,"status":1,"issystem":0,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"typeid":{"id":137,"mo_uuid":"5bb815d5754e8","tb_name":"system_article","field":"typeid","name":"类别","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"typeid","setup":"array (\n  \'inputtype\' => \'select\',\n  \'fieldtype\' => \'tinyint\',\n  \'numbertype\' => \'1\',\n  \'labelwidth\' => \'\',\n  \'default\' => \'2\',\n)","ispost":0,"sort":20,"status":1,"issystem":0,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"status":{"id":134,"mo_uuid":"5bb815d5754e8","tb_name":"system_article","field":"status","name":"状态","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"radio","setup":"array (\'options\' => \'有效|1\',\'fieldtype\' => \'varchar\',\'numbertype\' => \'1\',\'labelwidth\' => \'\',\'default\' => \'1\')","ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"updatetime":{"id":133,"mo_uuid":"5bb815d5754e8","tb_name":"system_article","field":"updatetime","name":"更新时间","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"datetime","setup":null,"ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"createtime":{"id":132,"mo_uuid":"5bb815d5754e8","tb_name":"system_article","field":"createtime","name":"发布时间","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"datetime","setup":null,"ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"sort":{"id":131,"mo_uuid":"5bb815d5754e8","tb_name":"system_article","field":"sort","name":"排序","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"number","setup":null,"ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null}}');
INSERT INTO `system_field_cache` VALUES ('system_crontab', '{"cron_class":{"id":81,"mo_uuid":"5bb5868dc67910","tb_name":"system_crontab","field":"cron_class","name":"类名","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"text","setup":"array (\n  \'size\' => \'\',\n  \'default\' => \'\',\n  \'ispassword\' => \'0\',\n  \'fieldtype\' => \'varchar\',\n)","ispost":0,"sort":0,"status":1,"issystem":0,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":"类名"},"cron_method":{"id":82,"mo_uuid":"5bb5868dc67910","tb_name":"system_crontab","field":"cron_method","name":"事件","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"text","setup":"array (\n  \'size\' => \'\',\n  \'default\' => \'\',\n  \'ispassword\' => \'0\',\n  \'fieldtype\' => \'varchar\',\n)","ispost":0,"sort":0,"status":1,"issystem":0,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"cron_time":{"id":83,"mo_uuid":"5bb5868dc67910","tb_name":"system_crontab","field":"cron_time","name":"执行时间","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"text","setup":"array (\n  \'size\' => \'\',\n  \'default\' => \'\',\n  \'ispassword\' => \'0\',\n  \'fieldtype\' => \'varchar\',\n)","ispost":0,"sort":0,"status":1,"issystem":0,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":"执行时间"},"id":{"id":84,"mo_uuid":"5bb5868dc67910","tb_name":"system_crontab","field":"id","name":"ID","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"number","setup":"","ispost":0,"sort":0,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"memo":{"id":85,"mo_uuid":"5bb5868dc67910","tb_name":"system_crontab","field":"memo","name":"备注","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"text","setup":"array (\n  \'size\' => \'\',\n  \'default\' => \'\',\n  \'ispassword\' => \'0\',\n  \'fieldtype\' => \'varchar\',\n)","ispost":0,"sort":0,"status":1,"issystem":0,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"status":{"id":88,"mo_uuid":"5bb5868dc67910","tb_name":"system_crontab","field":"status","name":"状态","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"select","setup":"array (\n  \'options\' => \'有效|1\r\n无效|0\',\n  \'multiple\' => \'0\',\n  \'fieldtype\' => \'tinyint\',\n  \'numbertype\' => \'1\',\n  \'size\' => \'\',\n  \'default\' => \'\',\n)","ispost":0,"sort":0,"status":0,"issystem":0,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"sort":{"id":86,"mo_uuid":"5bb5868dc67910","tb_name":"system_crontab","field":"sort","name":"排序","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"number","setup":"","ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"updatetime":{"id":87,"mo_uuid":"5bb5868dc67910","tb_name":"system_crontab","field":"updatetime","name":"更新时间","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"datetime","setup":"","ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""}}');
INSERT INTO `system_field_cache` VALUES ('system_message', '{"id":{"id":47,"mo_uuid":"5bb5868dc6797","tb_name":"system_message","field":"id","name":"ID","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"number","setup":null,"ispost":0,"sort":0,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"title":{"id":52,"mo_uuid":"5bb5868dc6797","tb_name":"system_message","field":"title","name":"标题","tips":"","required":1,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"title","setup":"array (\n  \'thumb\' => \'0\',\n  \'style\' => \'0\',\n  \'size\' => \'\',\n)","ispost":0,"sort":0,"status":1,"issystem":0,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"memo":{"id":53,"mo_uuid":"5bb5868dc6797","tb_name":"system_message","field":"memo","name":"内容","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"textarea","setup":"array (\n  \'fieldtype\' => \'mediumtext\',\n  \'rows\' => \'\',\n  \'cols\' => \'\',\n  \'default\' => \'\',\n)","ispost":0,"sort":0,"status":1,"issystem":0,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":300,"remark":""},"ib_read":{"id":54,"mo_uuid":"5bb5868dc6797","tb_name":"system_message","field":"ib_read","name":"已读","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"number","setup":"array (\n  \'size\' => \'\',\n  \'numbertype\' => \'1\',\n  \'decimaldigits\' => \'0\',\n  \'default\' => \'0\',\n)","ispost":0,"sort":0,"status":1,"issystem":0,"queryfield":1,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"user_id":{"id":55,"mo_uuid":"5bb5868dc6797","tb_name":"system_message","field":"user_id","name":"用户","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"model_select","setup":"array (\n  \'multiple\' => \'0\',\n  \'fieldtype\' => \'int\',\n  \'numbertype\' => \'1\',\n  \'size\' => \'\',\n  \'bindModule\' => \'users||username\',\n)","ispost":0,"sort":0,"status":1,"issystem":0,"queryfield":1,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"sort":{"id":48,"mo_uuid":"5bb5868dc6797","tb_name":"system_message","field":"sort","name":"排序","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"number","setup":null,"ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"createtime":{"id":49,"mo_uuid":"5bb5868dc6797","tb_name":"system_message","field":"createtime","name":"发布时间","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"datetime","setup":null,"ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":0,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"updatetime":{"id":50,"mo_uuid":"5bb5868dc6797","tb_name":"system_message","field":"updatetime","name":"更新时间","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"datetime","setup":null,"ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":120,"remark":null},"status":{"id":51,"mo_uuid":"5bb5868dc6797","tb_name":"system_message","field":"status","name":"状态","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"radio","setup":"array (\n  \'options\' => \'有效|1\r\n无效|0\',\n  \'fieldtype\' => \'varchar\',\n  \'numbertype\' => \'1\',\n  \'labelwidth\' => \'\',\n  \'default\' => \'1\',\n)","ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""}}');
INSERT INTO `system_field_cache` VALUES ('user_member', '{"id":{"id":95,"mo_uuid":"5bb5961db69b5","tb_name":"user_member","field":"id","name":"ID","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"number","setup":null,"ispost":0,"sort":0,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"name2":{"id":100,"mo_uuid":"5bb5961db69b5","tb_name":"user_member","field":"name2","name":"name2","tips":"","required":0,"minlength":0,"maxlength":5,"pattern":"","errormsg":"","class":"","type":"text","setup":"array (\n  \'size\' => \'10\',\n  \'default\' => \'\',\n  \'ispassword\' => \'0\',\n  \'fieldtype\' => \'varchar\',\n)","ispost":0,"sort":0,"status":1,"issystem":0,"queryfield":1,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"sex3":{"id":101,"mo_uuid":"5bb5961db69b5","tb_name":"user_member","field":"sex3","name":"sex3","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"select","setup":"array (\n  \'options\' => \'\',\n  \'multiple\' => \'0\',\n  \'fieldtype\' => \'int\',\n  \'numbertype\' => \'1\',\n  \'length\' => \'10\',\n  \'size\' => \'10\',\n  \'default\' => \'\',\n)","ispost":0,"sort":0,"status":1,"issystem":0,"queryfield":1,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"title1":{"id":102,"mo_uuid":"5bb5961db69b5","tb_name":"user_member","field":"title1","name":"title1","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"title","setup":"array (\n  \'size\' => \'10\',\n  \'style\' => \'0\',\n  \'thumb\' => \'0\',\n)","ispost":0,"sort":0,"status":1,"issystem":0,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"sort":{"id":96,"mo_uuid":"5bb5961db69b5","tb_name":"user_member","field":"sort","name":"排序","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"number","setup":null,"ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"createtime":{"id":97,"mo_uuid":"5bb5961db69b5","tb_name":"user_member","field":"createtime","name":"发布时间","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"datetime","setup":null,"ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"updatetime":{"id":98,"mo_uuid":"5bb5961db69b5","tb_name":"user_member","field":"updatetime","name":"更新时间","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"datetime","setup":null,"ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"status":{"id":99,"mo_uuid":"5bb5961db69b5","tb_name":"user_member","field":"status","name":"状态","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"radio","setup":"array (\'options\' => \'有效|1\'.\n.\'无效|0\',\'fieldtype\' => \'varchar\',\'numbertype\' => \'1\',\'labelwidth\' => \'\',\'default\' => \'1\')","ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null}}');
INSERT INTO `system_field_cache` VALUES ('user_tb', '{"articel_status":{"id":128,"mo_uuid":"5bb75810ad2bc","tb_name":"user_tb","field":"articel_status","name":"articel_status","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"model_select","setup":"array (\n  \'multiple\' => \'0\',\n  \'fieldtype\' => \'int\',\n  \'numbertype\' => \'1\',\n  \'size\' => \'10\',\n  \'bindModule\' => \'\',\n)","ispost":0,"sort":0,"status":1,"issystem":0,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"id":{"id":123,"mo_uuid":"5bb75810ad2bc","tb_name":"user_tb","field":"id","name":"ID","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"number","setup":null,"ispost":0,"sort":0,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"status":{"id":127,"mo_uuid":"5bb75810ad2bc","tb_name":"user_tb","field":"status","name":"状态","tips":"","required":0,"minlength":0,"maxlength":1,"pattern":"","errormsg":"","class":"","type":"radio","setup":"array (\n  \'options\' => \'有效|1\',\n  \'fieldtype\' => \'tinyint\',\n  \'numbertype\' => \'1\',\n  \'labelwidth\' => \'\',\n  \'default\' => \'1\',\n)","ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":""},"sort":{"id":124,"mo_uuid":"5bb75810ad2bc","tb_name":"user_tb","field":"sort","name":"排序","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"number","setup":null,"ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"createtime":{"id":125,"mo_uuid":"5bb75810ad2bc","tb_name":"user_tb","field":"createtime","name":"发布时间","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"datetime","setup":null,"ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null},"updatetime":{"id":126,"mo_uuid":"5bb75810ad2bc","tb_name":"user_tb","field":"updatetime","name":"更新时间","tips":"","required":0,"minlength":0,"maxlength":0,"pattern":"","errormsg":"","class":"","type":"datetime","setup":null,"ispost":0,"sort":100,"status":1,"issystem":1,"queryfield":0,"listShowField":1,"editShowField":1,"detailfield":0,"inputwidth":80,"remark":null}}');

-- ----------------------------
-- Table structure for `system_field_copy`
-- ----------------------------

DROP TABLE IF EXISTS `system_field_copy`;

CREATE TABLE `system_field_copy` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `mo_uuid` char(50) DEFAULT NULL,
  `moduleid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `field` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `tips` varchar(150) NOT NULL DEFAULT '',
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `minlength` int(10) unsigned NOT NULL DEFAULT '0',
  `maxlength` int(10) unsigned NOT NULL DEFAULT '0',
  `pattern` varchar(500) NOT NULL DEFAULT '' COMMENT '正式则',
  `errormsg` varchar(255) NOT NULL DEFAULT '',
  `class` varchar(20) NOT NULL DEFAULT '',
  `type` varchar(20) NOT NULL DEFAULT '',
  `setup` mediumtext,
  `ispost` tinyint(1) unsigned DEFAULT '0',
  `sort` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `issystem` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `queryfield` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0表示不显示在查询，1表示显示',
  `listShowField` tinyint(1) NOT NULL DEFAULT '1' COMMENT '列表显示,0表示不显示',
  `editShowField` tinyint(1) NOT NULL DEFAULT '1',
  `detailfield` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否详情显示',
  `inputwidth` int(5) unsigned NOT NULL DEFAULT '80' COMMENT '列宽度 ',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `moduleid_field` (`moduleid`,`field`)
) ENGINE=MyISAM AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `system_field_copy`
-- ----------------------------

INSERT INTO `system_field_copy` VALUES ('1', '', '2', 'id', 'ID', '', '0', '0', '0', '', '', '', 'number', '', '0', '0', '1', '1', '0', '1', '1', '1', '80', '');
INSERT INTO `system_field_copy` VALUES ('2', '', '2', 'sort', '排序', '', '0', '0', '0', '', '', '', 'number', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('3', '', '2', 'createtime', '发布时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '180', '');
INSERT INTO `system_field_copy` VALUES ('4', '', '2', 'updatetime', '更新时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '180', '');
INSERT INTO `system_field_copy` VALUES ('5', '', '2', 'status', '状态', '', '0', '0', '0', '', '', '', 'radio', 'array (\'options\' => \'有效|1 无效|0\',\'fieldtype\' => \'varchar\',\'numbertype\' => \'1\',\'labelwidth\' => \'\',\'default\' => \'1\')', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('7', '', '2', 'wificontrol', 'wifi控制器', '', '0', '0', '0', '', '', '', 'text', 'array (
  \'size\' => \'\',
  \'default\' => \'\',
  \'ispassword\' => \'0\',
  \'fieldtype\' => \'varchar\',
)', '0', '0', '1', '0', '1', '1', '0', '1', '180', '');
INSERT INTO `system_field_copy` VALUES ('8', '', '2', 'macaddress', 'mac地址', '', '0', '0', '0', '', '', '', 'text', 'array (
  \'size\' => \'\',
  \'default\' => \'\',
  \'ispassword\' => \'0\',
  \'fieldtype\' => \'varchar\',
)', '0', '0', '1', '0', '1', '1', '0', '1', '200', '');
INSERT INTO `system_field_copy` VALUES ('9', '', '3', 'id', 'ID', '', '0', '0', '0', '', '', '', 'number', '', '0', '0', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('10', '', '3', 'sort', '排序', '', '0', '0', '0', '', '', '', 'number', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('11', '', '3', 'createtime', '发布时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '180', '');
INSERT INTO `system_field_copy` VALUES ('12', '', '3', 'updatetime', '更新时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '180', '');
INSERT INTO `system_field_copy` VALUES ('13', '', '3', 'status', '状态', '', '0', '0', '0', '', '', '', 'radio', 'array (\'options\' => \'有效|1 无效|0\',\'fieldtype\' => \'varchar\',\'numbertype\' => \'1\',\'labelwidth\' => \'\',\'default\' => \'1\')', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('14', '', '3', 'username', '用户名', '', '0', '0', '0', '', '', '', 'text', '', '0', '0', '1', '0', '1', '1', '0', '0', '120', '用户名');
INSERT INTO `system_field_copy` VALUES ('15', '', '3', 'email', '邮箱', '', '0', '0', '0', 'email', '', '', 'text', '', '0', '0', '1', '0', '0', '1', '1', '0', '180', '邮箱');
INSERT INTO `system_field_copy` VALUES ('16', '', '3', 'password', '密码', '', '0', '0', '0', 'email', '', '', 'text', '', '0', '0', '1', '0', '0', '0', '0', '0', '80', '密码');
INSERT INTO `system_field_copy` VALUES ('17', '', '3', 'login_at', '登录时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '0', '0', '1', '1', '0', '180', '登录时间');
INSERT INTO `system_field_copy` VALUES ('18', '', '3', 'login_num', '登录次数', '', '0', '0', '0', '', '', '', 'number', '', '0', '0', '0', '0', '0', '1', '0', '0', '80', '登录次数');
INSERT INTO `system_field_copy` VALUES ('19', '', '4', 'id', 'ID', '', '0', '0', '0', '', '', '', 'number', '', '0', '0', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('20', '', '4', 'sort', '排序', '', '0', '0', '0', '', '', '', 'number', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('21', '', '4', 'createtime', '发布时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '160', '');
INSERT INTO `system_field_copy` VALUES ('22', '', '4', 'updatetime', '更新时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '160', '');
INSERT INTO `system_field_copy` VALUES ('23', '', '4', 'status', '状态', '', '0', '0', '0', '', '', '', 'radio', 'array (
  \'options\' => \'有效|1
无效|0\',
  \'fieldtype\' => \'varchar\',
  \'numbertype\' => \'1\',
  \'labelwidth\' => \'\',
  \'default\' => \'1\',
)', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('24', '', '4', 'doorname', '设备名称', '', '1', '0', '0', '', '', '', 'text', '', '0', '0', '1', '0', '0', '1', '0', '0', '120', '设备名称');
INSERT INTO `system_field_copy` VALUES ('25', '', '4', 'user_id', '用户ID', '', '1', '0', '0', '', '', '', 'model_select', 'array (
  \'multiple\' => \'0\',
  \'fieldtype\' => \'varchar\',
  \'numbertype\' => \'1\',
  \'size\' => \'\',
  \'bindModule\' => \'users||username\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '用户ID');
INSERT INTO `system_field_copy` VALUES ('26', '', '4', 'wificontrol_id', 'wifi控制器ID', '', '1', '0', '0', '', '', '', 'model_select', 'array (
  \'multiple\' => \'0\',
  \'fieldtype\' => \'varchar\',
  \'numbertype\' => \'1\',
  \'size\' => \'\',
  \'bindModule\' => \'wificontrol||wificontrol\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '130', 'wifi控制器ID');
INSERT INTO `system_field_copy` VALUES ('27', '', '4', 'sitesensor_id', '位置传感器id', '', '0', '0', '0', '', '', '', 'text', '', '0', '0', '1', '0', '0', '1', '1', '0', '130', '位置传感器id');
INSERT INTO `system_field_copy` VALUES ('28', '', '4', 'btn_type', ' 按键类型', '', '0', '0', '0', '', '', '', 'radio', 'array (
  \'options\' => \'一键|1
二键|2
三键|3
四键|4\',
  \'fieldtype\' => \'tinyint\',
  \'numbertype\' => \'1\',
  \'labelwidth\' => \'\',
  \'default\' => \'\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '按键类型1，2，3，4 ，分别代表1键，2键，3键，4键');
INSERT INTO `system_field_copy` VALUES ('29', '', '4', 'switch_status', '开关状态', '', '0', '0', '0', '', '', '', 'select', 'array (
  \'options\' => \'开门|1
关门|2
停止|3
解锁|4\',
  \'multiple\' => \'0\',
  \'fieldtype\' => \'varchar\',
  \'numbertype\' => \'1\',
  \'size\' => \'\',
  \'default\' => \'\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '1：开门/上升 ;2：关门/下降;3：停止;4:  解锁');
INSERT INTO `system_field_copy` VALUES ('30', '', '5', 'id', 'ID', '', '0', '0', '0', '', '', '', 'number', '', '0', '0', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('31', '', '5', 'sort', '排序', '', '0', '0', '0', '', '', '', 'number', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('32', '', '5', 'createtime', '发布时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '160', '');
INSERT INTO `system_field_copy` VALUES ('33', '', '5', 'updatetime', '更新时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '160', '');
INSERT INTO `system_field_copy` VALUES ('34', '', '5', 'status', '状态', '', '0', '0', '0', '', '', '', 'radio', 'array (\'options\' => \'有效|1 无效|0\',\'fieldtype\' => \'varchar\',\'numbertype\' => \'1\',\'labelwidth\' => \'\',\'default\' => \'1\')', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('35', '', '5', 'clientid', '客户端id', '', '0', '0', '0', '', '', '', 'number', '', '0', '0', '1', '0', '0', '1', '1', '0', '120', '客户端id（ 会员用户workerman的id，硬件的tcp的id）');
INSERT INTO `system_field_copy` VALUES ('36', '', '5', 'clientinfo', '客户端信息', '', '0', '0', '0', '', '', '', 'text', '', '0', '0', '1', '0', '0', '1', '1', '0', '120', '客户端信息（会用户id，硬件的mac地址）');
INSERT INTO `system_field_copy` VALUES ('37', '', '5', 'client_type', '客户端类型', '', '0', '0', '0', '', '', '', 'text', '', '0', '0', '1', '0', '0', '1', '1', '0', '120', '客户端类型（CD用户,DD硬件）');
INSERT INTO `system_field_copy` VALUES ('38', '', '2', 'user_id', '用户id', '', '0', '0', '0', '', '', '', 'model_select', 'array (
  \'multiple\' => \'0\',
  \'fieldtype\' => \'varchar\',
  \'numbertype\' => \'1\',
  \'size\' => \'\',
  \'bindModule\' => \'users||username\',
)', '0', '0', '1', '0', '1', '1', '0', '1', '80', '用户id');
INSERT INTO `system_field_copy` VALUES ('39', '', '6', 'id', 'ID', '', '0', '0', '0', '', '', '', 'number', '', '0', '0', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('40', '', '6', 'sort', '排序', '', '0', '0', '0', '', '', '', 'number', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('41', '', '6', 'createtime', '发布时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('42', '', '6', 'updatetime', '更新时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('43', '', '6', 'status', '状态', '', '0', '0', '0', '', '', '', 'radio', 'array (
  \'options\' => \'有效|1
无效|0\',
  \'fieldtype\' => \'tinyint\',
  \'numbertype\' => \'1\',
  \'labelwidth\' => \'\',
  \'default\' => \'1\',
)', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('44', '', '6', 'title', '标题', '', '1', '0', '0', '', '', '', 'title', '', '0', '0', '1', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('45', '', '6', 'typeId', '文章类型', '', '0', '0', '0', '', '', '', 'typeid', 'array (
  \'inputtype\' => \'select\',
  \'fieldtype\' => \'tinyint\',
  \'numbertype\' => \'1\',
  \'labelwidth\' => \'\',
  \'default\' => \'1\',
  \'tpl\' => \'\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('46', '', '6', 'description', '文章内容', '', '0', '0', '0', '', '', '', 'editor', 'array (
  \'edittype\' => \'kindeditor\',
  \'toolbar\' => \'full\',
  \'default\' => \'\',
  \'height\' => \'\',
  \'show_add_description\' => \'0\',
  \'show_auto_thumb\' => \'0\',
  \'showpage\' => \'0\',
  \'enablekeylink\' => \'0\',
  \'replacenum\' => \'\',
  \'enablesaveimage\' => \'0\',
  \'flashupload\' => \'0\',
  \'alowuploadexts\' => \'\',
  \'alowuploadlimit\' => \'\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('47', '', '7', 'id', 'ID', '', '0', '0', '0', '', '', '', 'number', '', '0', '0', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('48', '', '7', 'sort', '排序', '', '0', '0', '0', '', '', '', 'number', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('49', '', '7', 'createtime', '发布时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '0', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('50', '', '7', 'updatetime', '更新时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '120', '');
INSERT INTO `system_field_copy` VALUES ('51', '', '7', 'status', '状态', '', '0', '0', '0', '', '', '', 'radio', 'array (\'options\' => \'有效|1 无效|0\',\'fieldtype\' => \'varchar\',\'numbertype\' => \'1\',\'labelwidth\' => \'\',\'default\' => \'1\')', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('52', '', '7', 'title', '标题', '', '1', '0', '0', '', '', '', 'title', 'array (
  \'thumb\' => \'0\',
  \'style\' => \'0\',
  \'size\' => \'\',
)', '0', '0', '0', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('53', '', '7', 'memo', '内容', '', '0', '0', '0', '', '', '', 'textarea', 'array (
  \'fieldtype\' => \'mediumtext\',
  \'rows\' => \'\',
  \'cols\' => \'\',
  \'default\' => \'\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '140', '');
INSERT INTO `system_field_copy` VALUES ('54', '', '7', 'ib_read', '已读', '', '0', '0', '0', '', '', '', 'number', 'array (
  \'size\' => \'\',
  \'numbertype\' => \'1\',
  \'decimaldigits\' => \'0\',
  \'default\' => \'0\',
)', '0', '0', '1', '0', '1', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('55', '', '7', 'userid', '用户', '', '0', '0', '0', '', '', '', 'model_select', 'array (
  \'multiple\' => \'0\',
  \'fieldtype\' => \'int\',
  \'numbertype\' => \'1\',
  \'size\' => \'\',
  \'bindModule\' => \'users||username\',
)', '0', '0', '1', '0', '1', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('56', '', '3', 'reset_md5', '重置字符串', '', '0', '0', '0', '', '', '', 'text', 'array (
  \'size\' => \'\',
  \'default\' => \'\',
  \'ispassword\' => \'0\',
  \'fieldtype\' => \'varchar\',
)', '0', '0', '1', '0', '0', '0', '0', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('57', '', '3', 'update_passwd', '将生效的密码', '', '0', '0', '0', '', '', '', 'text', 'array (
  \'size\' => \'\',
  \'default\' => \'\',
  \'ispassword\' => \'0\',
  \'fieldtype\' => \'varchar\',
)', '0', '0', '0', '0', '0', '0', '0', '0', '80', '将生效的密码');
INSERT INTO `system_field_copy` VALUES ('58', '', '3', 'registrationID', '手机端推送id', '', '0', '0', '0', '', '', '', 'text', 'array (
  \'size\' => \'\',
  \'default\' => \'\',
  \'ispassword\' => \'0\',
  \'fieldtype\' => \'varchar\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '120', '手机端推送id');
INSERT INTO `system_field_copy` VALUES ('59', '', '8', 'id', 'ID', '', '0', '0', '0', '', '', '', 'number', '', '0', '0', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('60', '', '8', 'sort', '排序', '', '0', '0', '0', '', '', '', 'number', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('61', '', '8', 'createtime', '发布时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '120', '');
INSERT INTO `system_field_copy` VALUES ('62', '', '8', 'updatetime', '更新时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '120', '');
INSERT INTO `system_field_copy` VALUES ('63', '', '8', 'status', '状态', '', '0', '0', '0', '', '', '', 'radio', 'array (\'options\' => \'有效|1 无效|0\',\'fieldtype\' => \'varchar\',\'numbertype\' => \'1\',\'labelwidth\' => \'\',\'default\' => \'1\')', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('64', '', '8', 'content', '内容', '', '0', '0', '0', '', '', '', 'textarea', 'array (
  \'fieldtype\' => \'mediumtext\',
  \'rows\' => \'\',
  \'cols\' => \'\',
  \'default\' => \'\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '200', '内容');
INSERT INTO `system_field_copy` VALUES ('65', '', '8', 'user_id', '接收人', '', '0', '0', '0', '', '', '', 'model_select', 'array (
  \'multiple\' => \'0\',
  \'fieldtype\' => \'varchar\',
  \'numbertype\' => \'1\',
  \'size\' => \'\',
  \'bindModule\' => \'users||registrationID\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '120', '接收人');
INSERT INTO `system_field_copy` VALUES ('66', '', '9', 'id', 'ID', '', '0', '0', '0', '', '', '', 'number', '', '0', '0', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('67', '', '9', 'sort', '排序', '', '0', '0', '0', '', '', '', 'number', '', '0', '100', '1', '1', '0', '0', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('68', '', '9', 'createtime', '发布时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '0', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('69', '', '9', 'updatetime', '更新时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '160', '');
INSERT INTO `system_field_copy` VALUES ('70', '', '9', 'status', '状态', '', '0', '0', '0', '', '', '', 'radio', 'array (\'options\' => \'有效|1 无效|0\',\'fieldtype\' => \'varchar\',\'numbertype\' => \'1\',\'labelwidth\' => \'\',\'default\' => \'1\')', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('71', '', '9', 'title', '标题', '', '0', '0', '0', '', '', '', 'title', 'array (
  \'thumb\' => \'0\',
  \'style\' => \'0\',
  \'size\' => \'\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '200', '');
INSERT INTO `system_field_copy` VALUES ('72', '', '9', 'mac', 'mac', '', '0', '0', '0', '', '', '', 'text', 'array (
  \'size\' => \'\',
  \'default\' => \'\',
  \'ispassword\' => \'0\',
  \'fieldtype\' => \'varchar\',
)', '0', '0', '0', '0', '0', '1', '1', '0', '200', '');
INSERT INTO `system_field_copy` VALUES ('73', '', '9', 'type', 'type', '', '0', '0', '0', '', '', '', 'number', 'array (
  \'size\' => \'\',
  \'numbertype\' => \'1\',
  \'decimaldigits\' => \'0\',
  \'default\' => \'1\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('74', '', '9', 'cmd', 'cmd', '', '0', '0', '0', '', '', '', 'textarea', 'array (
  \'fieldtype\' => \'mediumtext\',
  \'rows\' => \'\',
  \'cols\' => \'\',
  \'default\' => \'\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '200', 'cmd');
INSERT INTO `system_field_copy` VALUES ('75', '', '5', 'online', '在线', '', '0', '0', '0', '', '', '', 'number', 'array (
  \'size\' => \'\',
  \'numbertype\' => \'1\',
  \'decimaldigits\' => \'0\',
  \'default\' => \'0\',
)', '0', '0', '0', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('76', '', '5', 'device_mac', 'device_mac', '', '0', '0', '0', '', '', '', 'text', 'array (
  \'size\' => \'100\',
  \'default\' => \'\',
  \'ispassword\' => \'0\',
  \'fieldtype\' => \'varchar\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '10', '');
INSERT INTO `system_field_copy` VALUES ('77', '', '2', 'type', 'type', '', '0', '0', '0', '', '', '', 'number', 'array (
  \'size\' => \'\',
  \'numbertype\' => \'1\',
  \'decimaldigits\' => \'0\',
  \'default\' => \'0\',
)', '0', '0', '1', '0', '0', '1', '1', '1', '80', '');
INSERT INTO `system_field_copy` VALUES ('78', '', '2', 'online', 'online', '', '0', '0', '0', '', '', '', 'number', 'array (
  \'size\' => \'\',
  \'numbertype\' => \'1\',
  \'decimaldigits\' => \'0\',
  \'default\' => \'0\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('79', '', '2', 'state', 'state', '', '0', '0', '0', '', '', '', 'number', 'array (
  \'size\' => \'\',
  \'numbertype\' => \'1\',
  \'decimaldigits\' => \'0\',
  \'default\' => \'0\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('80', '', '5', 'user_id', 'user_id', '', '0', '0', '0', '', '', '', 'number', 'array (
  \'size\' => \'\',
  \'numbertype\' => \'1\',
  \'decimaldigits\' => \'0\',
  \'default\' => \'\',
)', '0', '0', '0', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('81', '', '10', 'cron_class', '类名', '', '0', '0', '0', '', '', '', 'text', 'array (
  \'size\' => \'\',
  \'default\' => \'\',
  \'ispassword\' => \'0\',
  \'fieldtype\' => \'varchar\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '类名');
INSERT INTO `system_field_copy` VALUES ('82', '', '10', 'cron_method', '事件', '', '0', '0', '0', '', '', '', 'text', 'array (
  \'size\' => \'\',
  \'default\' => \'\',
  \'ispassword\' => \'0\',
  \'fieldtype\' => \'varchar\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('83', '', '10', 'cron_time', '执行时间', '', '0', '0', '0', '', '', '', 'text', 'array (
  \'size\' => \'\',
  \'default\' => \'\',
  \'ispassword\' => \'0\',
  \'fieldtype\' => \'varchar\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '执行时间');
INSERT INTO `system_field_copy` VALUES ('84', '', '10', 'id', 'ID', '', '0', '0', '0', '', '', '', 'number', '', '0', '0', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('85', '', '10', 'memo', '备注', '', '0', '0', '0', '', '', '', 'text', 'array (
  \'size\' => \'\',
  \'default\' => \'\',
  \'ispassword\' => \'0\',
  \'fieldtype\' => \'varchar\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('86', '', '10', 'sort', '排序', '', '0', '0', '0', '', '', '', 'number', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('87', '', '10', 'updatetime', '更新时间', '', '0', '0', '0', '', '', '', 'datetime', '', '0', '100', '1', '1', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('88', '', '10', 'status', '状态', '', '0', '0', '0', '', '', '', 'select', 'array (
  \'options\' => \'有效|1
无效|0\',
  \'multiple\' => \'0\',
  \'fieldtype\' => \'tinyint\',
  \'numbertype\' => \'1\',
  \'size\' => \'\',
  \'default\' => \'\',
)', '0', '0', '0', '0', '0', '1', '1', '0', '80', '');
INSERT INTO `system_field_copy` VALUES ('89', '', '3', 'sessionid', 'sessionid', '', '0', '0', '0', '', '', '', 'text', 'array (
  \'size\' => \'\',
  \'default\' => \'\',
  \'ispassword\' => \'0\',
  \'fieldtype\' => \'varchar\',
)', '0', '0', '1', '0', '0', '1', '1', '0', '80', '');

-- ----------------------------
-- Table structure for `system_log`
-- ----------------------------

DROP TABLE IF EXISTS `system_log`;

CREATE TABLE `system_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ip` char(15) NOT NULL DEFAULT '' COMMENT '操作者IP地址',
  `node` char(200) NOT NULL DEFAULT '' COMMENT '当前操作节点',
  `username` varchar(32) NOT NULL DEFAULT '' COMMENT '操作人用户名',
  `action` varchar(200) NOT NULL DEFAULT '' COMMENT '操作行为',
  `content` text NOT NULL COMMENT '操作内容描述',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=808 DEFAULT CHARSET=utf8 COMMENT='系统操作日志表';

-- ----------------------------
-- Data for the table `system_log`
-- ----------------------------

INSERT INTO `system_log` VALUES ('801', '221.233.204.188', 'admin/login/index', 'superman', '系统管理', '用户登录系统成功', '2018-10-04 09:46:43', '1538617603');
INSERT INTO `system_log` VALUES ('802', '221.233.204.188', 'admin/login/index', 'superman', '系统管理', '用户登录系统成功', '2018-10-08 16:16:31', '1538986591');
INSERT INTO `system_log` VALUES ('803', '221.233.204.188', 'admin/variable/index', 'superman', '系统管理', '系统参数配置成功', '2018-10-08 16:17:56', '1538986676');
INSERT INTO `system_log` VALUES ('804', '221.233.204.188', 'admin/variable/index', 'superman', '系统管理', '系统参数配置成功', '2018-10-08 16:18:01', '1538986681');
INSERT INTO `system_log` VALUES ('805', '221.233.204.188', 'admin/variable/index', 'superman', '系统管理', '系统参数配置成功', '2018-10-08 16:18:10', '1538986690');
INSERT INTO `system_log` VALUES ('806', '221.233.204.188', 'admin/variable/index', 'superman', '系统管理', '系统参数配置成功', '2018-10-08 16:18:15', '1538986695');
INSERT INTO `system_log` VALUES ('807', '221.233.204.188', 'admin/login/index', 'superman', '系统管理', '用户登录系统成功', '2018-10-12 15:07:41', '1539328061');

-- ----------------------------
-- Table structure for `system_menu`
-- ----------------------------

DROP TABLE IF EXISTS `system_menu`;

CREATE TABLE `system_menu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '父id',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `node` varchar(200) NOT NULL DEFAULT '' COMMENT '节点代码',
  `icon` varchar(100) NOT NULL DEFAULT '' COMMENT '菜单图标',
  `url` varchar(400) NOT NULL DEFAULT '' COMMENT '链接',
  `params` varchar(500) DEFAULT '' COMMENT '链接参数',
  `target` varchar(20) NOT NULL DEFAULT '_self' COMMENT '链接打开方式',
  `sort` int(11) unsigned DEFAULT '0' COMMENT '菜单排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态(0:禁用,1:启用)',
  `create_by` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `index_system_menu_node` (`node`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8 COMMENT='系统菜单表';

-- ----------------------------
-- Data for the table `system_menu`
-- ----------------------------

INSERT INTO `system_menu` VALUES ('1', '0', '系统设置', '', '', '#', '', '_self', '1000', '1', '10000', '2018-01-19 15:27:00');
INSERT INTO `system_menu` VALUES ('2', '10', '后台菜单', '', 'fa fa-leaf', 'admin/menu/index', '', '_self', '10', '1', '10000', '2018-01-19 15:27:17');
INSERT INTO `system_menu` VALUES ('3', '10', '系统参数', '', 'fa fa-modx', 'admin/variable/index', '', '_self', '20', '1', '10000', '2018-01-19 15:27:57');
INSERT INTO `system_menu` VALUES ('4', '11', '访问授权', '', 'fa fa-group', 'admin/auth/index', '', '_self', '20', '0', '10000', '2018-01-22 11:13:02');
INSERT INTO `system_menu` VALUES ('6', '11', '访问节点', '', 'fa fa-fort-awesome', 'admin/node/index', '', '_self', '30', '0', '0', '2018-01-23 12:36:54');
INSERT INTO `system_menu` VALUES ('7', '1', '后台首页', '', '', 'admin/index/main', '', '_self', '1', '1', '0', '2018-01-23 13:42:30');
INSERT INTO `system_menu` VALUES ('8', '16', '系统日志', '', 'fa fa-code', 'admin/log/index', '', '_self', '10', '1', '0', '2018-01-24 13:52:58');
INSERT INTO `system_menu` VALUES ('9', '10', '文件存储', '', 'fa fa-stop-circle', 'admin/config/file', '', '_self', '30', '0', '0', '2018-01-25 10:54:28');
INSERT INTO `system_menu` VALUES ('10', '1', '系统管理', '', 'fa fa-scribd', '#', '', '_self', '200', '1', '0', '2018-01-25 18:14:28');
INSERT INTO `system_menu` VALUES ('11', '1', '访问权限', '', 'fa fa-anchor', '#', '', '_self', '300', '1', '0', '2018-01-25 18:15:08');
INSERT INTO `system_menu` VALUES ('16', '1', '日志管理', '', 'fa fa-hashtag', '#', '', '_self', '400', '1', '0', '2018-02-10 16:31:15');
INSERT INTO `system_menu` VALUES ('17', '0', '微信管理', '', '', '#', '', '_self', '8000', '0', '0', '2018-03-06 14:42:49');
INSERT INTO `system_menu` VALUES ('18', '17', '公众号配置', '', 'fa fa-cogs', '#', '', '_self', '0', '0', '0', '2018-03-06 14:43:05');
INSERT INTO `system_menu` VALUES ('19', '18', '微信授权绑定', '', 'fa fa-cog', 'wechat/config/index', '', '_self', '0', '0', '0', '2018-03-06 14:43:26');
INSERT INTO `system_menu` VALUES ('20', '18', '关注默认回复', '', 'fa fa-comment-o', 'wechat/keys/subscribe', '', '_self', '0', '0', '0', '2018-03-06 14:44:45');
INSERT INTO `system_menu` VALUES ('21', '18', '无反馈默认回复', '', 'fa fa-commenting', 'wechat/keys/defaults', '', '_self', '0', '0', '0', '2018-03-06 14:45:55');
INSERT INTO `system_menu` VALUES ('22', '18', '微信关键字管理', '', 'fa fa-hashtag', 'wechat/keys/index', '', '_self', '0', '0', '0', '2018-03-06 14:46:23');
INSERT INTO `system_menu` VALUES ('23', '17', '微信服务定制', '', 'fa fa-cubes', '#', '', '_self', '0', '0', '0', '2018-03-06 14:47:11');
INSERT INTO `system_menu` VALUES ('24', '23', '微信菜单管理', '', 'fa fa-gg-circle', 'wechat/menu/index', '', '_self', '0', '0', '0', '2018-03-06 14:47:39');
INSERT INTO `system_menu` VALUES ('25', '23', '微信图文管理', '', 'fa fa-map-o', 'wechat/news/index', '', '_self', '0', '0', '0', '2018-03-06 14:48:14');
INSERT INTO `system_menu` VALUES ('26', '17', '微信粉丝管理', '', 'fa fa-user', '#', '', '_self', '0', '0', '0', '2018-03-06 14:48:33');
INSERT INTO `system_menu` VALUES ('27', '26', '微信粉丝列表', '', 'fa fa-users', 'wechat/fans/index', '', '_self', '20', '0', '0', '2018-03-06 14:49:04');
INSERT INTO `system_menu` VALUES ('28', '26', '微信黑名单管理', '', 'fa fa-user-times', 'wechat/block/index', '', '_self', '30', '0', '0', '2018-03-06 14:49:22');
INSERT INTO `system_menu` VALUES ('29', '26', '微信标签管理', '', 'fa fa-tags', 'wechat/tags/index', '', '_self', '10', '0', '0', '2018-03-06 14:49:39');
INSERT INTO `system_menu` VALUES ('32', '1', '模型管理', '', 'fa fa-modx', '#', '', '_self', '10', '1', '0', '2018-04-23 22:38:45');
INSERT INTO `system_menu` VALUES ('33', '32', '模型列表', '', 'fa fa-shopping-basket', 'admin/module/index', '', '_self', '0', '1', '0', '2018-04-23 22:40:35');
INSERT INTO `system_menu` VALUES ('34', '0', '商城管理', '', '', '#', '', '_self', '1500', '0', '0', '2018-04-23 22:45:00');
INSERT INTO `system_menu` VALUES ('35', '34', '商品管理', '', '', '#', '', '_self', '0', '0', '0', '2018-04-23 22:45:18');
INSERT INTO `system_menu` VALUES ('36', '35', '产品管理', '', '', '/goods/product/index', '', '_self', '0', '0', '0', '2018-04-23 22:45:35');
INSERT INTO `system_menu` VALUES ('37', '35', '品牌管理', '', '', '/goods/brand/index', '', '_self', '0', '0', '0', '2018-04-23 22:45:59');
INSERT INTO `system_menu` VALUES ('38', '35', '规格管理', '', '', '/goods/spec/index', '', '_self', '0', '0', '0', '2018-04-23 22:46:19');
INSERT INTO `system_menu` VALUES ('39', '35', '分类管理', '', '', '/goods/cate/index', '', '_self', '0', '0', '0', '2018-04-23 22:48:15');
INSERT INTO `system_menu` VALUES ('43', '0', '我的', '', 'fa fa-navicon', 'admin/types/index', '', '_self', '2000', '1', '0', '2018-05-04 08:36:12');
INSERT INTO `system_menu` VALUES ('44', '1', '清空缓存', '', 'fa fa-history', 'admin/recache/index', '', '_self', '500', '1', '0', '2018-05-05 17:48:58');
INSERT INTO `system_menu` VALUES ('65', '0', '模型管理', '', '', '#', '', '_self', '1100', '1', '0', '2018-05-24 10:37:16');
INSERT INTO `system_menu` VALUES ('66', '65', '模型管理', '', '', 'admin/module/index', '', '_self', '0', '1', '0', '2018-05-24 10:38:28');
INSERT INTO `system_menu` VALUES ('67', '65', '分类管理', '', '', 'admin/types/index', '', '_self', '0', '1', '0', '2018-05-24 10:38:46');
INSERT INTO `system_menu` VALUES ('75', '1', '轮播图管理', '', 'fa fa-photo', '#', '', '_self', '600', '1', '0', '2018-06-25 09:54:25');
INSERT INTO `system_menu` VALUES ('76', '1', '数据库管理', '', 'fa fa-align-justify', '#', '', '_self', '999', '1', '0', '2018-06-29 17:09:43');
INSERT INTO `system_menu` VALUES ('77', '76', '数据表列表', '', '', 'admin/database/index', '', '_self', '0', '1', '0', '2018-06-29 17:11:21');
INSERT INTO `system_menu` VALUES ('78', '76', '数据库恢复', '', '', 'admin/database/recover', '', '_self', '0', '1', '0', '2018-06-29 18:12:56');
INSERT INTO `system_menu` VALUES ('79', '1', '计划任务管理', '', 'fa fa-clock-o', 'admin/crontab/index', '', '_self', '1000', '1', '0', '2018-07-05 16:30:44');
INSERT INTO `system_menu` VALUES ('80', '1', '轮播图', '', 'fa fa-photo', 'admin/slide/index', '', '_self', '600', '1', '0', '2018-06-25 09:54:25');
INSERT INTO `system_menu` VALUES ('85', '10', '邮箱配置', '', 'fa fa-send', 'admin/variable/email', '', '_self', '40', '1', '0', '2018-07-11 10:08:02');
INSERT INTO `system_menu` VALUES ('86', '43', '文章', '', 'fa fa-cube', '#', '', '_self', '0', '1', '0', '2018-07-11 10:29:51');
INSERT INTO `system_menu` VALUES ('87', '86', '文章列表', '', 'fa fa-edge', 'admin/article/index', '', '_self', '0', '1', '0', '2018-07-11 10:31:13');
INSERT INTO `system_menu` VALUES ('88', '43', '消息列表', '', 'fa fa-usb', 'admin/message/index', '', '_self', '0', '1', '0', '2018-07-11 16:09:52');

-- ----------------------------
-- Table structure for `system_message`
-- ----------------------------

DROP TABLE IF EXISTS `system_message`;

CREATE TABLE `system_message` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `memo` mediumtext,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ib_read` int(10) unsigned NOT NULL DEFAULT '0',
  `sort` int(10) unsigned NOT NULL DEFAULT '90',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `status` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ib_read` (`ib_read`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `system_message`
-- ----------------------------

INSERT INTO `system_message` VALUES ('1', 'aaaaa', 'a', '3', '1', '90', '0', '1531299420', '1');
INSERT INTO `system_message` VALUES ('2', 'aaa', 'mmm', '0', '1', '90', '1531299128', '1531299437', '1');
INSERT INTO `system_message` VALUES ('3', '策士统领', '策士统领大战德玛西亚', '0', '0', '90', '1531311147', '0', '1');
INSERT INTO `system_message` VALUES ('4', '策士统领', '策士统领大战德玛西亚', '0', '1', '90', '1531311151', '1531362218', '1');
INSERT INTO `system_message` VALUES ('5', '策士统领', '策士统领大战德玛西亚', '0', '0', '90', '1531311869', '0', '1');
INSERT INTO `system_message` VALUES ('6', '策士统领123', '策士统领大战德玛西亚2', '0', '1', '90', '1531361900', '1531362319', '1');
INSERT INTO `system_message` VALUES ('7', '', '2223333', '7', '0', '90', '1531363875', '1538790376', '1');

-- ----------------------------
-- Table structure for `system_module`
-- ----------------------------

DROP TABLE IF EXISTS `system_module`;

CREATE TABLE `system_module` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `mo_uuid` char(50) NOT NULL,
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '模型标题',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '模型名称',
  `tb_name` varchar(100) DEFAULT '' COMMENT '表名',
  `description` varchar(200) NOT NULL DEFAULT '' COMMENT '简介',
  `hascache` tinyint(1) unsigned DEFAULT '0' COMMENT '是否缓存表',
  `hasquery` tinyint(1) unsigned DEFAULT '0' COMMENT '是否搜索（0：否，1：是）',
  `hasdelete` tinyint(1) DEFAULT '0' COMMENT '是否可删除操作（0：否；1：是）',
  `hasorder` tinyint(1) DEFAULT '0' COMMENT '是否可排序（0：否；1：是）',
  `hasedit` tinyint(1) DEFAULT '0',
  `hasdetail` tinyint(1) DEFAULT '0',
  `hasexport` tinyint(1) DEFAULT '0' COMMENT '是否可导出excel数据（0：否；1是）',
  `hasadd` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否可新增',
  `is_system` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `rows` tinyint(5) unsigned NOT NULL DEFAULT '0',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lang` smallint(1) DEFAULT '1',
  `createtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `system_module`
-- ----------------------------

INSERT INTO `system_module` VALUES ('7', '5bb5868dc6797', '消息', 'message', 'system_message', '消息', '0', '1', '0', '1', '0', '0', '1', '0', '0', '0', '0', '1', '1', '1531281011');
INSERT INTO `system_module` VALUES ('9', '5bb5868dc6799', 'ws日志表', 'wsoptlog', 'device_wsoptlog', 'workerman日志表', '0', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1531538906');
INSERT INTO `system_module` VALUES ('10', '5bb5868dc67910', '计划任务管理', 'crontab', 'system_crontab', '计划任务管理', '1', '0', '1', '1', '1', '0', '1', '1', '0', '0', '0', '1', '1', '1530779214');
INSERT INTO `system_module` VALUES ('31', '5bb81d8f13a4b', 'member', 'member', 'member', 'member', '0', '0', '1', '1', '1', '0', '1', '1', '0', '0', '0', '1', '1', '1538792847');
INSERT INTO `system_module` VALUES ('29', '5bb815d5754e8', '文章管理', 'article', 'system_article', '文章管理', '0', '0', '1', '1', '1', '0', '1', '1', '0', '0', '0', '1', '1', '1538790869');
INSERT INTO `system_module` VALUES ('32', '5bb822abb6bb8', '省市区', 'areabase', 'system_areabase', '省市区', '0', '0', '1', '1', '0', '0', '1', '1', '0', '0', '0', '1', '1', '1538794155');

-- ----------------------------
-- Table structure for `system_node`
-- ----------------------------

DROP TABLE IF EXISTS `system_node`;

CREATE TABLE `system_node` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `node` varchar(100) DEFAULT NULL COMMENT '节点代码',
  `title` varchar(500) DEFAULT NULL COMMENT '节点标题',
  `is_menu` tinyint(1) unsigned DEFAULT '0' COMMENT '是否可设置为菜单',
  `is_auth` tinyint(1) unsigned DEFAULT '1' COMMENT '是否启动RBAC权限控制',
  `is_login` tinyint(1) unsigned DEFAULT '1' COMMENT '是否启动登录控制',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `index_system_node_node` (`node`)
) ENGINE=InnoDB AUTO_INCREMENT=308 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统节点表';

-- ----------------------------
-- Data for the table `system_node`
-- ----------------------------

INSERT INTO `system_node` VALUES ('1', 'admin', '系统管理', '0', '1', '1', '2018-01-23 12:39:13');
INSERT INTO `system_node` VALUES ('13', 'admin/log/index', '日志记录', '0', '1', '1', '2018-01-23 12:39:28');
INSERT INTO `system_node` VALUES ('14', 'admin/log/sms', '短信记录', '0', '1', '1', '2018-01-23 12:39:29');
INSERT INTO `system_node` VALUES ('15', 'admin/log/del', '日志删除', '1', '1', '1', '2018-01-23 12:39:29');
INSERT INTO `system_node` VALUES ('16', 'admin/menu/index', '系统菜单列表', '1', '1', '1', '2018-01-23 12:39:31');
INSERT INTO `system_node` VALUES ('17', 'admin/menu/add', '添加系统菜单', '0', '1', '1', '2018-01-23 12:39:31');
INSERT INTO `system_node` VALUES ('18', 'admin/menu/edit', '编辑系统菜单', '0', '1', '1', '2018-01-23 12:39:32');
INSERT INTO `system_node` VALUES ('19', 'admin/menu/del', '删除系统菜单', '0', '1', '1', '2018-01-23 12:39:33');
INSERT INTO `system_node` VALUES ('20', 'admin/menu/forbid', '禁用系统菜单', '0', '1', '1', '2018-01-23 12:39:33');
INSERT INTO `system_node` VALUES ('21', 'admin/menu/resume', '启用系统菜单', '0', '1', '1', '2018-01-23 12:39:34');
INSERT INTO `system_node` VALUES ('22', 'admin/node/index', '系统节点列表', '1', '1', '1', '2018-01-23 12:39:36');
INSERT INTO `system_node` VALUES ('23', 'admin/node/save', '保存节点信息', '0', '1', '1', '2018-01-23 12:39:36');
INSERT INTO `system_node` VALUES ('24', 'admin/user/index', '系统用户列表', '1', '1', '1', '2018-01-23 12:39:37');
INSERT INTO `system_node` VALUES ('25', 'admin/user/auth', '用户授权操作', '0', '1', '1', '2018-01-23 12:39:38');
INSERT INTO `system_node` VALUES ('26', 'admin/user/add', '添加系统用户', '0', '1', '1', '2018-01-23 12:39:39');
INSERT INTO `system_node` VALUES ('27', 'admin/user/edit', '编辑系统用户', '0', '1', '1', '2018-01-23 12:39:39');
INSERT INTO `system_node` VALUES ('28', 'admin/user/pass', '修改用户密码', '0', '1', '1', '2018-01-23 12:39:40');
INSERT INTO `system_node` VALUES ('29', 'admin/user/del', '删除系统用户', '0', '1', '1', '2018-01-23 12:39:41');
INSERT INTO `system_node` VALUES ('30', 'admin/user/forbid', '禁用系统用户', '0', '1', '1', '2018-01-23 12:39:41');
INSERT INTO `system_node` VALUES ('31', 'admin/user/resume', '启用系统用户', '0', '1', '1', '2018-01-23 12:39:42');
INSERT INTO `system_node` VALUES ('33', 'admin/log', '日志管理', '0', '1', '1', '2018-01-23 13:34:48');
INSERT INTO `system_node` VALUES ('34', 'admin/menu', '系统菜单管理', '0', '1', '1', '2018-01-23 13:34:58');
INSERT INTO `system_node` VALUES ('35', 'admin/node', '系统节点管理', '0', '1', '1', '2018-01-23 13:35:17');
INSERT INTO `system_node` VALUES ('36', 'admin/user', '系统用户管理', '0', '1', '1', '2018-01-23 13:35:26');
INSERT INTO `system_node` VALUES ('229', 'admin/node/clear', '清理无效记录', '0', '1', '1', '2018-03-09 12:24:31');
INSERT INTO `system_node` VALUES ('230', 'admin/module/updatafields', '', '1', '1', '1', '2018-04-27 20:06:52');
INSERT INTO `system_node` VALUES ('231', 'admin/module/edit', '', '1', '1', '1', '2018-04-27 20:06:52');
INSERT INTO `system_node` VALUES ('237', 'admin/log/gettagslist', '', '0', '0', '0', '2018-05-11 16:59:05');
INSERT INTO `system_node` VALUES ('238', 'admin/classes', '班级管理', '0', '1', '1', '2018-05-11 17:01:09');
INSERT INTO `system_node` VALUES ('254', 'admin/classes/index', '班级列表', '1', '1', '1', '2018-05-11 17:01:09');
INSERT INTO `system_node` VALUES ('255', 'admin/module', '模块管理', '0', '1', '1', '2018-05-13 10:01:25');
INSERT INTO `system_node` VALUES ('256', 'admin/module/index', '模块列表', '1', '1', '1', '2018-05-13 10:01:37');
INSERT INTO `system_node` VALUES ('257', 'admin/module/delete', '', '1', '1', '1', '2018-05-13 10:01:41');
INSERT INTO `system_node` VALUES ('258', 'admin/module/forbid', '', '1', '1', '1', '2018-05-13 10:01:42');
INSERT INTO `system_node` VALUES ('259', 'admin/module/validatefield', '', '1', '1', '1', '2018-05-13 10:01:42');
INSERT INTO `system_node` VALUES ('260', 'admin/module/update', '', '1', '1', '1', '2018-05-13 10:01:42');
INSERT INTO `system_node` VALUES ('261', 'admin/module/insert', '', '1', '1', '1', '2018-05-13 10:01:42');
INSERT INTO `system_node` VALUES ('262', 'admin/module/resume', '', '1', '1', '1', '2018-05-13 10:01:43');
INSERT INTO `system_node` VALUES ('263', 'admin/module/gettagslist', '', '1', '1', '1', '2018-05-13 10:01:43');
INSERT INTO `system_node` VALUES ('264', 'admin/module/deleterec', '', '1', '1', '1', '2018-05-13 10:01:43');
INSERT INTO `system_node` VALUES ('265', 'admin/module/listorder', '', '1', '1', '1', '2018-05-13 10:01:43');
INSERT INTO `system_node` VALUES ('266', 'admin/module/upfile', '', '1', '1', '1', '2018-05-13 10:01:43');
INSERT INTO `system_node` VALUES ('267', 'admin/module/distpickercreate', '', '1', '1', '1', '2018-05-13 10:01:43');
INSERT INTO `system_node` VALUES ('268', 'admin/module/export', '', '1', '1', '1', '2018-05-13 10:01:43');
INSERT INTO `system_node` VALUES ('269', 'admin/module/getmodule', '', '1', '1', '1', '2018-05-13 10:01:44');
INSERT INTO `system_node` VALUES ('284', 'admin/variable', '', '1', '1', '1', '2018-05-13 12:09:07');
INSERT INTO `system_node` VALUES ('285', 'admin/variable/index', '', '1', '1', '1', '2018-05-13 12:09:07');
INSERT INTO `system_node` VALUES ('294', 'admin/types/index', '', '0', '1', '1', '2018-05-13 16:24:50');
INSERT INTO `system_node` VALUES ('305', 'admin/test', '', '0', '1', '1', '2018-07-01 16:17:25');
INSERT INTO `system_node` VALUES ('306', 'admin/test/index', '', '0', '1', '1', '2018-07-01 16:17:25');
INSERT INTO `system_node` VALUES ('307', 'admin/ajaxs', '', '0', '1', '1', '2018-07-01 16:20:41');

-- ----------------------------
-- Table structure for `system_sequence`
-- ----------------------------

DROP TABLE IF EXISTS `system_sequence`;

CREATE TABLE `system_sequence` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) DEFAULT NULL COMMENT '序号类型',
  `sequence` char(50) NOT NULL COMMENT '序号值',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_system_sequence_unique` (`type`,`sequence`) USING BTREE,
  KEY `index_system_sequence_type` (`type`),
  KEY `index_system_sequence_number` (`sequence`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统序号表';

-- ----------------------------
-- Data for the table `system_sequence`
-- ----------------------------

-- ----------------------------
-- Table structure for `system_slide`
-- ----------------------------

DROP TABLE IF EXISTS `system_slide`;

CREATE TABLE `system_slide` (
  `slide_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slide_name` varchar(30) NOT NULL DEFAULT '' COMMENT '英文名',
  `slide_cn_name` varchar(255) DEFAULT '' COMMENT '中文名',
  `tpl` varchar(255) NOT NULL DEFAULT '' COMMENT 'js文件模板,用于浏览器',
  `width` smallint(5) unsigned NOT NULL DEFAULT '0',
  `height` smallint(5) unsigned NOT NULL DEFAULT '0',
  `num` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '图片数量,暂不使用',
  `remark` mediumtext,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`slide_id`),
  UNIQUE KEY `slide_name` (`slide_name`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `system_slide`
-- ----------------------------

INSERT INTO `system_slide` VALUES ('1', '首页幻灯片', '首页幻灯片4', '2', '920', '300', '0', '首页轮播图', '1');

-- ----------------------------
-- Table structure for `system_slideattrdata`
-- ----------------------------

DROP TABLE IF EXISTS `system_slideattrdata`;

CREATE TABLE `system_slideattrdata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slide_id` tinyint(3) unsigned DEFAULT '0',
  `title` varchar(30) NOT NULL DEFAULT '',
  `pic` varchar(150) NOT NULL DEFAULT '',
  `link` varchar(150) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fid` (`slide_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `system_slideattrdata`
-- ----------------------------

INSERT INTO `system_slideattrdata` VALUES ('2', '2', '图片2', '/Uploads/Plugs/5c86a98c57257e9e/57a8c55fa0f77429.jpg', 'http://www.maxhom.cn', '测试', '1', '1');
INSERT INTO `system_slideattrdata` VALUES ('3', '3', '图片3', '/Uploads/Plugs/5c86a98c57257e9e/57a8c55fa0f77429.jpg', 'http://www.maxhom.cn', '', '2', '1');
INSERT INTO `system_slideattrdata` VALUES ('4', '4', '校区管理a68', '/Uploads/Plugs/5c86a98c57257e9e/57a8c55fa0f77429.jpg', '##', '', '4', '1');
INSERT INTO `system_slideattrdata` VALUES ('5', '5', 'flash2', '/Uploads/Plugs/5c86a98c57257e9e/57a8c55fa0f77429.jpg', 'http://www.maxhom.cn', '', '3', '1');

-- ----------------------------
-- Table structure for `system_types`
-- ----------------------------

DROP TABLE IF EXISTS `system_types`;

CREATE TABLE `system_types` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `group_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `extend` text,
  `css_class` char(200) NOT NULL DEFAULT '' COMMENT 'html类名',
  `sort` smallint(6) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `parentid` (`parentid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='系统分类表';

-- ----------------------------
-- Data for the table `system_types`
-- ----------------------------

INSERT INTO `system_types` VALUES ('1', '0', '1', '0', '文章分类', '', '', '0');
INSERT INTO `system_types` VALUES ('2', '0', '1', '0', '消息分类', '', '', '0');
INSERT INTO `system_types` VALUES ('3', '2', '1', '0', '系统消息', '', '', '0');
INSERT INTO `system_types` VALUES ('4', '2', '1', '0', '用户消息', '', '', '0');
INSERT INTO `system_types` VALUES ('5', '1', '1', '0', '常见问题', '', '', '0');

-- ----------------------------
-- Table structure for `system_user`
-- ----------------------------

DROP TABLE IF EXISTS `system_user`;

CREATE TABLE `system_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '用户登录名',
  `password` char(32) NOT NULL DEFAULT '' COMMENT '用户登录密码',
  `qq` varchar(16) DEFAULT NULL COMMENT '联系QQ',
  `mail` varchar(32) DEFAULT NULL COMMENT '联系邮箱',
  `phone` varchar(16) DEFAULT NULL COMMENT '联系手机号',
  `desc` varchar(255) DEFAULT '' COMMENT '备注说明',
  `login_num` bigint(20) unsigned DEFAULT '0' COMMENT '登录次数',
  `login_at` datetime DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态(0:禁用,1:启用)',
  `authorize` varchar(255) DEFAULT NULL,
  `campus_id` int(11) NOT NULL DEFAULT '0' COMMENT '校区',
  `module` varchar(100) DEFAULT NULL COMMENT '表名',
  `related_id` int(11) DEFAULT NULL COMMENT '关联ID',
  `is_deleted` tinyint(1) unsigned DEFAULT '0' COMMENT '删除状态(1:删除,0:未删)',
  `create_by` bigint(20) unsigned DEFAULT NULL COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_system_user_username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10003 DEFAULT CHARSET=utf8 COMMENT='系统用户表';

-- ----------------------------
-- Data for the table `system_user`
-- ----------------------------

INSERT INTO `system_user` VALUES ('10000', 'superman', 'f8fa4e44a3fd075ca595a0c2ea35b3cd', '22222222', '', '18993368867', '', '23415', '2018-10-12 15:07:41', '1', '2,4', '0', '', '0', '0', '', '2015-11-13 15:14:22');
INSERT INTO `system_user` VALUES ('10001', 'superman02', '0192023a7bbd73250516f069df18b500', '', '', '', '', '0', '', '1', '1', '13', 'teacher', '2', '0', '', '2018-05-10 16:50:37');
INSERT INTO `system_user` VALUES ('10002', 'superman03', '21232f297a57a5a743894a0e4a801fc3', '', 'admin03@163.com', '', '', '0', '', '1', '2', '13', '', '', '0', '', '2018-05-13 22:09:12');

-- ----------------------------
-- Table structure for `system_variable`
-- ----------------------------

DROP TABLE IF EXISTS `system_variable`;

CREATE TABLE `system_variable` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT '参数名称(英文)',
  `cn_name` varchar(255) NOT NULL DEFAULT '' COMMENT '参数名称（中文）',
  `value` text COMMENT '配置值',
  `memo` varchar(500) NOT NULL DEFAULT '' COMMENT '配置备注',
  `type_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1：string;2:image',
  `sort` int(10) NOT NULL DEFAULT '0',
  `group_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT '分组id，1：站点配置，2：参数配置',
  PRIMARY KEY (`id`),
  KEY `index_system_config_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统参数配置';

-- ----------------------------
-- Data for the table `system_variable`
-- ----------------------------

INSERT INTO `system_variable` VALUES ('1', 'app_name', '程序名称', '脑力', '当前程序名称，在后台主标题上显示', '1', '0', '1');
INSERT INTO `system_variable` VALUES ('2', 'site_name', '网站名称', '脑力', '网站名称，显示在浏览器标签上', '1', '0', '1');
INSERT INTO `system_variable` VALUES ('3', 'app_version', '程序版本', '3.0.0', '当前程序版本号，在后台主标题上标显示', '1', '0', '1');
INSERT INTO `system_variable` VALUES ('4', 'site_copy', '版权信息', '©版权所有 2014-2018', '程序的版权信息设置，在后台登录页面显示', '1', '0', '1');
INSERT INTO `system_variable` VALUES ('5', 'browser_icon', '浏览器图标', '/static/upload/e618480087d3066b/8279d68753fb602f.png', '建议上传ICO图标的尺寸为128x128px，此图标用于网站标题前，ICON在线制作', '2', '0', '1');
INSERT INTO `system_variable` VALUES ('7', 'miitbeian', '网站备案', '粤ICP备16006642号-2', '网站备案号，可以在备案管理中心查询获取', '1', '0', '1');
INSERT INTO `system_variable` VALUES ('57', 'logo', '网站logo', 'http://door.mydanweb.com/Uploads/Plugs/59bfea4c7a83eb00/d428dcc5cda51fc9.jpg', '网站logo', '2', '0', '2');
INSERT INTO `system_variable` VALUES ('67', 'System', '系统参数设置', 'testtesttest23', '系统参数设置测试', '1', '0', '2');
INSERT INTO `system_variable` VALUES ('68', 'storage_type', '存储方式', 'local', '本地', '1', '0', '2');
INSERT INTO `system_variable` VALUES ('69', 'storage_local_exts', '本地上传文件类型', 'png,jpg,rar,doc,icon,mp4', '本地上传文件类型', '1', '0', '2');
INSERT INTO `system_variable` VALUES ('70', 'mail_type', '邮件发送模式', '1', '邮件发送模式', '1', '0', '3');
INSERT INTO `system_variable` VALUES ('71', 'mail_server', '邮件服务器', 'smtp.qq.com', '邮件服务器', '1', '0', '3');
INSERT INTO `system_variable` VALUES ('72', 'mail_port', '邮件发送端口', '25', '邮件发送端口', '1', '0', '3');
INSERT INTO `system_variable` VALUES ('73', 'mail_from', '发件人地址', '1058942018@qq.com', '发件人地址', '1', '0', '3');
INSERT INTO `system_variable` VALUES ('74', 'mail_auth', 'AUTH LOGIN验证', '1', 'AUTH LOGIN验证', '1', '0', '3');
INSERT INTO `system_variable` VALUES ('75', 'mail_user', '验证用户名', '1058942018@qq.com', '验证用户名', '1', '0', '3');
INSERT INTO `system_variable` VALUES ('76', 'mail_password', '验证密码', 'nhkbwjmbtkbmbdid', '验证密码', '1', '0', '3');
INSERT INTO `system_variable` VALUES ('77', 'ADMIN_VERIFY', '后台登陆验证码', '0', '1：开启，0：关闭', '1', '0', '2');
INSERT INTO `system_variable` VALUES ('78', 'photos', '语言包是否随重建缓存生成', '1', '', '3', '0', '1');
INSERT INTO `system_variable` VALUES ('81', 'admin', '测试', '2134', '测试', '1', '0', '7');
INSERT INTO `system_variable` VALUES ('82', 'toplist_setup_head_display', '后台列表页面头部的列表参数设置', '1', '', '3', '0', '1');

-- ----------------------------
-- Table structure for `system_variable_group`
-- ----------------------------

DROP TABLE IF EXISTS `system_variable_group`;

CREATE TABLE `system_variable_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `display_name` varchar(255) NOT NULL COMMENT '展示名称',
  `sort` int(10) unsigned DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `system_variable_group`
-- ----------------------------

INSERT INTO `system_variable_group` VALUES ('1', '系统参数', '12');
INSERT INTO `system_variable_group` VALUES ('2', '站点配置', '6');
INSERT INTO `system_variable_group` VALUES ('3', '邮件配置', '3');
INSERT INTO `system_variable_group` VALUES ('7', '分组测试', '0');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` text,
  `email` text NOT NULL COMMENT '邮箱',
  `password` text NOT NULL COMMENT '密码',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '新建时间',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `sort` int(10) unsigned NOT NULL DEFAULT '90' COMMENT '排序',
  `login_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `login_at` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '登录时间',
  `reset_md5` varchar(255) NOT NULL DEFAULT '',
  `update_passwd` varchar(255) NOT NULL DEFAULT '',
  `registrationID` varchar(255) NOT NULL DEFAULT '',
  `emailcode` varchar(10) NOT NULL DEFAULT '',
  `emailcode_time` int(11) NOT NULL COMMENT '发送时间',
  `verfiy_emailcode_err_num` tinyint(4) NOT NULL DEFAULT '0' COMMENT '验证码错误次数',
  `sessionid` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Data for the table `users`
-- ----------------------------

INSERT INTO `users` VALUES ('3', 'conson', 'rongkangsen@163.com', 'c3ffb795d920347b07093482d70274f73c81d5bc', '1531121218', '1534140453', '1', '90', '34', '1534139930', '', '', 'fqwerqr1234124', '7701', '0', '0', '');
INSERT INTO `users` VALUES ('4', 'conson001', 'rongkangsen@qq.com', '71dd07494c5ee54992a27746d547e25dee01bd97', '1531128894', '0', '1', '90', '0', '0', '', '', '', '', '0', '0', '');
INSERT INTO `users` VALUES ('5', 'conson002', 'rongkangsen123@qq.com', '71dd07494c5ee54992a27746d547e25dee01bd97', '1531208682', '0', '1', '90', '0', '0', '', '', '', '', '0', '0', '');
INSERT INTO `users` VALUES ('6', 'rong', 'rongkangsen12344444@qq.com', '71dd07494c5ee54992a27746d547e25dee01bd97', '1531208900', '0', '1', '90', '0', '0', '', '', '', '', '0', '0', '');
INSERT INTO `users` VALUES ('7', 'adada', '2671471526@qq.com', '71dd07494c5ee54992a27746d547e25dee01bd97', '1531209376', '0', '1', '90', '167', '1537500815', '', '', '141fe1da9efecc72550,1507bfd3f7968d33e84', '', '0', '0', '');
INSERT INTO `users` VALUES ('8', 'a1dada', '26714715261@qq.com', '71dd07494c5ee54992a27746d547e25dee01bd97', '1531210417', '0', '1', '90', '0', '0', '', '', '', '', '0', '0', '');
INSERT INTO `users` VALUES ('9', '122222', '2976479646@qq.com', '976d3005cf91cbd39ea2fcaf599abecf48a3a256', '1531213724', '0', '1', '90', '0', '0', '', '', '', '', '0', '0', '');
INSERT INTO `users` VALUES ('10', '33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333', '1787441018@qq.com', '8cecace5c24064d0d33ea3cf01c5997c5366b663', '1531214806', '0', '1', '90', '0', '0', '', '', '', '', '0', '0', '');
INSERT INTO `users` VALUES ('11', '1234567890111111', '1270073156@qq.com', '71dd07494c5ee54992a27746d547e25dee01bd97', '1531273465', '0', '1', '90', '4', '1537340965', '', '', '141fe1da9efecc72550', '', '0', '0', '');
INSERT INTO `users` VALUES ('12', '4333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333', '209397402@qq.com', '4297f44b13955235245b2497399d7a93', '1531316920', '1531320570', '1', '90', '0', '0', '5972d6f4405b435220006e58ccd9bae1', '456456', '', '', '0', '0', '');
INSERT INTO `users` VALUES ('13', 'wang', '532003321@qq.com', '71dd07494c5ee54992a27746d547e25dee01bd97', '1533891760', '0', '1', '90', '0', '0', '', '', '111', '', '0', '0', '');
INSERT INTO `users` VALUES ('14', '', '', '7c4a8d09ca3762af61e59520943dc26494f8941b', '0', '1534128845', '1', '90', '0', '0', '', '', '', '', '0', '0', '');
INSERT INTO `users` VALUES ('15', '', '', '7c4a8d09ca3762af61e59520943dc26494f8941b', '0', '1534129768', '1', '90', '0', '0', '', '', '', '', '0', '0', '');

-- ----------------------------
-- Table structure for `wifi-clientinfo`
-- ----------------------------

DROP TABLE IF EXISTS `wifi-clientinfo`;

CREATE TABLE `wifi-clientinfo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientid` char(255) NOT NULL DEFAULT '0' COMMENT '客户端id（ 会员用户workerman的id，硬件的tcp的id',
  `clientinfo_del` text COMMENT '客户端信息（会用户id，硬件的mac地址）',
  `client_type` char(50) DEFAULT NULL COMMENT '客户端类型（CD用户,DD硬件）',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `online` int(10) unsigned NOT NULL DEFAULT '0',
  `device_mac` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `sort` int(10) unsigned NOT NULL DEFAULT '90',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Index 2` (`client_type`),
  KEY `Index 3` (`device_mac`)
) ENGINE=MyISAM AUTO_INCREMENT=95 DEFAULT CHARSET=utf8 COMMENT='设备在线表';

-- ----------------------------
-- Data for the table `wifi-clientinfo`
-- ----------------------------

INSERT INTO `wifi-clientinfo` VALUES ('28', '7f0000010b5600000002', '12', 'CD', '0', '1532573330', '0', '', '1', '90', '0');
INSERT INTO `wifi-clientinfo` VALUES ('94', '7f0000010a2a00000006', '', '', '0', '1534130425', '0', '', '1', '90', '7');

-- ----------------------------
-- Table structure for `wificontrol`
-- ----------------------------

DROP TABLE IF EXISTS `wificontrol`;

CREATE TABLE `wificontrol` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `wificontrol` varchar(255) NOT NULL DEFAULT '' COMMENT 'wifi控制器名称',
  `macaddress` varchar(100) DEFAULT NULL COMMENT 'mac地址',
  `client_id` varchar(100) DEFAULT NULL COMMENT '硬件client_id',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `sort` int(10) unsigned NOT NULL DEFAULT '90',
  `state` int(10) unsigned NOT NULL DEFAULT '0',
  `online` int(10) unsigned NOT NULL DEFAULT '0',
  `type` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COMMENT='wifi控制器';

-- ----------------------------
-- Data for the table `wificontrol`
-- ----------------------------

INSERT INTO `wificontrol` VALUES ('1', '车房门控制器001', '01:02:cd:00:02', '', '1531125182', '0', '1', '90', '0', '0', '0', '3');
INSERT INTO `wificontrol` VALUES ('4', '车库门控制器a004', 'bbb', '', '1531203683', '1531233047', '1', '90', '0', '0', '0', '3');
INSERT INTO `wificontrol` VALUES ('7', '车库门控制器006', '01:02:cd:00:06', '', '1531207195', '0', '1', '90', '0', '0', '0', '3');
INSERT INTO `wificontrol` VALUES ('19', 'wifi01', '01:02:cd:00:06', '', '1531232312', '0', '1', '90', '0', '0', '0', '4');
INSERT INTO `wificontrol` VALUES ('20', 'wifi02', '', '', '1531232326', '0', '1', '90', '0', '0', '0', '3');
INSERT INTO `wificontrol` VALUES ('34', 'aaa', '', '', '1531962975', '0', '1', '90', '0', '0', '0', '3,4');
INSERT INTO `wificontrol` VALUES ('39', '车库门控制器', '60:01:94:1d:49:fb', '', '1537500495', '0', '1', '90', '0', '0', '0', '7');

-- ----------------------------
-- Table structure for `wsoptlog`
-- ----------------------------

DROP TABLE IF EXISTS `wsoptlog`;

CREATE TABLE `wsoptlog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` varchar(200) NOT NULL,
  `cmd` mediumtext NOT NULL,
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '90',
  `ibHex` tinyint(1) unsigned NOT NULL DEFAULT '90',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2462 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Data for the table `wsoptlog`
-- ----------------------------

INSERT INTO `wsoptlog` VALUES ('2245', '7f0000010b5600000001', '2', '90', '0', '0', '1534130363', '1');
INSERT INTO `wsoptlog` VALUES ('2243', '7f0000010b5600000001', '2', '90', '0', '0', '1534130343', '1');
INSERT INTO `wsoptlog` VALUES ('2244', '7f0000010b5600000001', '2', '90', '0', '0', '1534130353', '1');
INSERT INTO `wsoptlog` VALUES ('2242', '7f0000010b5600000001', '2', '90', '0', '0', '1534130333', '1');
INSERT INTO `wsoptlog` VALUES ('2241', '7f0000010b5600000001', '2', '90', '0', '0', '1534130323', '1');
INSERT INTO `wsoptlog` VALUES ('2240', '7f0000010b5600000001', '2', '90', '0', '0', '1534130313', '1');
INSERT INTO `wsoptlog` VALUES ('2412', '7f0000010b5600000001', '2', '90', '0', '0', '1534131973', '1');
INSERT INTO `wsoptlog` VALUES ('2239', '7f0000010b5600000001', '2', '90', '0', '0', '1534130303', '1');
INSERT INTO `wsoptlog` VALUES ('2238', '7f0000010b5600000001', '2', '90', '0', '0', '1534130293', '1');
INSERT INTO `wsoptlog` VALUES ('2237', '7f0000010b5600000001', '2', '90', '0', '0', '1534130283', '1');
INSERT INTO `wsoptlog` VALUES ('2236', '7f0000010b5600000001', '2', '90', '0', '0', '1534130273', '1');
INSERT INTO `wsoptlog` VALUES ('2235', '7f0000010b5600000001', '2', '90', '0', '0', '1534130263', '1');
INSERT INTO `wsoptlog` VALUES ('2234', '7f0000010b5600000001', '2', '90', '0', '0', '1534130253', '1');
INSERT INTO `wsoptlog` VALUES ('2233', '7f0000010b5600000001', '2', '90', '0', '0', '1534130243', '1');
INSERT INTO `wsoptlog` VALUES ('2232', '7f0000010b5600000001', '2', '90', '0', '0', '1534130233', '1');
INSERT INTO `wsoptlog` VALUES ('2231', '7f0000010b5600000001', '2', '90', '0', '0', '1534130223', '1');
INSERT INTO `wsoptlog` VALUES ('2230', '7f0000010b5600000001', '2', '90', '0', '0', '1534130213', '1');
INSERT INTO `wsoptlog` VALUES ('2229', '7f0000010b5600000001', '2', '90', '0', '0', '1534130203', '1');
INSERT INTO `wsoptlog` VALUES ('2228', '7f0000010b5600000001', '2', '90', '0', '0', '1534130193', '1');
INSERT INTO `wsoptlog` VALUES ('2227', '7f0000010b5600000001', '2', '90', '0', '0', '1534130183', '1');
INSERT INTO `wsoptlog` VALUES ('2226', '7f0000010b5600000001', '2', '90', '0', '0', '1534130173', '1');
INSERT INTO `wsoptlog` VALUES ('2225', '7f0000010b5600000001', '2', '90', '0', '0', '1534130163', '1');
INSERT INTO `wsoptlog` VALUES ('2160', '7f0000010b5600000001', '2', '90', '0', '0', '1534129513', '1');
INSERT INTO `wsoptlog` VALUES ('2224', '7f0000010b5600000001', '2', '90', '0', '0', '1534130153', '1');
INSERT INTO `wsoptlog` VALUES ('2223', '7f0000010b5600000001', '2', '90', '0', '0', '1534130143', '1');
INSERT INTO `wsoptlog` VALUES ('2222', '7f0000010b5600000001', '2', '90', '0', '0', '1534130133', '1');
INSERT INTO `wsoptlog` VALUES ('2221', '7f0000010b5600000001', '2', '90', '0', '0', '1534130123', '1');
INSERT INTO `wsoptlog` VALUES ('2220', '7f0000010b5600000001', '2', '90', '0', '0', '1534130113', '1');
INSERT INTO `wsoptlog` VALUES ('2219', '7f0000010b5600000001', '2', '90', '0', '0', '1534130103', '1');
INSERT INTO `wsoptlog` VALUES ('2218', '7f0000010b5600000001', '2', '90', '0', '0', '1534130093', '1');
INSERT INTO `wsoptlog` VALUES ('2217', '7f0000010b5600000001', '2', '90', '0', '0', '1534130083', '1');
INSERT INTO `wsoptlog` VALUES ('2216', '7f0000010b5600000001', '2', '90', '0', '0', '1534130073', '1');
INSERT INTO `wsoptlog` VALUES ('2215', '7f0000010b5600000001', '2', '90', '0', '0', '1534130063', '1');
INSERT INTO `wsoptlog` VALUES ('2214', '7f0000010b5600000001', '2', '90', '0', '0', '1534130053', '1');
INSERT INTO `wsoptlog` VALUES ('2213', '7f0000010b5600000001', '2', '90', '0', '0', '1534130043', '1');
INSERT INTO `wsoptlog` VALUES ('2413', '7f0000010b5600000001', '2', '90', '0', '0', '1534131983', '1');
INSERT INTO `wsoptlog` VALUES ('2212', '7f0000010b5600000001', '2', '90', '0', '0', '1534130033', '1');
INSERT INTO `wsoptlog` VALUES ('2210', '7f0000010b5600000001', '2', '90', '0', '0', '1534130013', '1');
INSERT INTO `wsoptlog` VALUES ('2211', '7f0000010b5600000001', '2', '90', '0', '0', '1534130023', '1');
INSERT INTO `wsoptlog` VALUES ('2246', '7f0000010b5600000001', '2', '90', '0', '0', '1534130373', '1');
INSERT INTO `wsoptlog` VALUES ('2247', '7f0000010b5600000001', '2', '90', '0', '0', '1534130383', '1');
INSERT INTO `wsoptlog` VALUES ('2248', '7f0000010b5600000001', '2', '90', '0', '0', '1534130393', '1');
INSERT INTO `wsoptlog` VALUES ('2249', '7f0000010b5600000001', '2', '90', '0', '0', '1534130403', '1');
INSERT INTO `wsoptlog` VALUES ('2250', '7f0000010b5600000001', '2', '90', '0', '0', '1534130413', '1');
INSERT INTO `wsoptlog` VALUES ('2251', '7f0000010b5600000001', '2', '90', '0', '0', '1534130423', '1');
INSERT INTO `wsoptlog` VALUES ('2252', '7f0000010a2a00000006', 'cd7', '90', '0', '0', '1534130425', '1');
INSERT INTO `wsoptlog` VALUES ('2253', '7f0000010b5600000001', '2', '90', '0', '0', '1534130433', '1');
INSERT INTO `wsoptlog` VALUES ('2254', '7f0000010b5600000001', '2', '90', '0', '0', '1534130443', '1');
INSERT INTO `wsoptlog` VALUES ('2255', '7f0000010b5600000001', '2', '90', '0', '0', '1534130453', '1');
INSERT INTO `wsoptlog` VALUES ('2256', '7f0000010b5600000001', '2', '90', '0', '0', '1534130463', '1');
INSERT INTO `wsoptlog` VALUES ('2257', '7f0000010b5600000001', '2', '90', '0', '0', '1534130473', '1');
INSERT INTO `wsoptlog` VALUES ('2258', '7f0000010b5600000001', '2', '90', '0', '0', '1534130483', '1');
INSERT INTO `wsoptlog` VALUES ('2259', '7f0000010b5600000001', '2', '90', '0', '0', '1534130493', '1');
INSERT INTO `wsoptlog` VALUES ('2260', '7f0000010a2a00000006', 'cc#2c:3a:e8:39:9c:69#FF020101020000000006', '90', '0', '0', '1534130499', '1');
INSERT INTO `wsoptlog` VALUES ('2261', '7f0000010b5600000001', 'FF 6 0 1 2 3 0 0 0 D', '90', '1', '0', '1534130503', '1');
INSERT INTO `wsoptlog` VALUES ('2262', '7f0000010b5600000001', '2', '90', '0', '0', '1534130503', '1');
INSERT INTO `wsoptlog` VALUES ('2263', '7f0000010b5600000001', 'FF 6 0 1 2 3 0 0 0 D', '90', '1', '0', '1534130503', '1');
INSERT INTO `wsoptlog` VALUES ('2264', '7f0000010b5600000001', 'FF 6 0 1 2 3 0 0 0 D', '90', '1', '0', '1534130504', '1');
INSERT INTO `wsoptlog` VALUES ('2265', '7f0000010b5600000001', 'FF 6 0 1 2 1 0 0 0 A', '90', '1', '0', '1534130505', '1');
INSERT INTO `wsoptlog` VALUES ('2266', '7f0000010b5600000001', '2', '90', '0', '0', '1534130513', '1');
INSERT INTO `wsoptlog` VALUES ('2267', '7f0000010b5600000001', '2', '90', '0', '0', '1534130523', '1');
INSERT INTO `wsoptlog` VALUES ('2268', '7f0000010b5600000001', '2', '90', '0', '0', '1534130533', '1');
INSERT INTO `wsoptlog` VALUES ('2269', '7f0000010b5600000001', '2', '90', '0', '0', '1534130543', '1');
INSERT INTO `wsoptlog` VALUES ('2270', '7f0000010b5600000001', '2', '90', '0', '0', '1534130553', '1');
INSERT INTO `wsoptlog` VALUES ('2273', '7f0000010b5600000001', '2', '90', '0', '0', '1534130583', '1');
INSERT INTO `wsoptlog` VALUES ('2274', '7f0000010b5600000001', '2', '90', '0', '0', '1534130593', '1');
INSERT INTO `wsoptlog` VALUES ('2275', '7f0000010b5600000001', '2', '90', '0', '0', '1534130603', '1');
INSERT INTO `wsoptlog` VALUES ('2276', '7f0000010b5600000001', '2', '90', '0', '0', '1534130613', '1');
INSERT INTO `wsoptlog` VALUES ('2277', '7f0000010b5600000001', '2', '90', '0', '0', '1534130623', '1');
INSERT INTO `wsoptlog` VALUES ('2278', '7f0000010b5600000001', '2', '90', '0', '0', '1534130633', '1');
INSERT INTO `wsoptlog` VALUES ('2279', '7f0000010b5600000001', '2', '90', '0', '0', '1534130643', '1');
INSERT INTO `wsoptlog` VALUES ('2280', '7f0000010b5600000001', '2', '90', '0', '0', '1534130653', '1');
INSERT INTO `wsoptlog` VALUES ('2281', '7f0000010b5600000001', '2', '90', '0', '0', '1534130663', '1');
INSERT INTO `wsoptlog` VALUES ('2282', '7f0000010b5600000001', '2', '90', '0', '0', '1534130673', '1');
INSERT INTO `wsoptlog` VALUES ('2283', '7f0000010b5600000001', '2', '90', '0', '0', '1534130683', '1');
INSERT INTO `wsoptlog` VALUES ('2284', '7f0000010b5600000001', '2', '90', '0', '0', '1534130693', '1');
INSERT INTO `wsoptlog` VALUES ('2285', '7f0000010b5600000001', '2', '90', '0', '0', '1534130703', '1');
INSERT INTO `wsoptlog` VALUES ('2286', '7f0000010b5600000001', '2', '90', '0', '0', '1534130713', '1');
INSERT INTO `wsoptlog` VALUES ('2287', '7f0000010b5600000001', '2', '90', '0', '0', '1534130723', '1');
INSERT INTO `wsoptlog` VALUES ('2288', '7f0000010b5600000001', '2', '90', '0', '0', '1534130733', '1');
INSERT INTO `wsoptlog` VALUES ('2289', '7f0000010b5600000001', '2', '90', '0', '0', '1534130743', '1');
INSERT INTO `wsoptlog` VALUES ('2290', '7f0000010b5600000001', '2', '90', '0', '0', '1534130753', '1');
INSERT INTO `wsoptlog` VALUES ('2291', '7f0000010b5600000001', '2', '90', '0', '0', '1534130763', '1');
INSERT INTO `wsoptlog` VALUES ('2292', '7f0000010b5600000001', '2', '90', '0', '0', '1534130773', '1');
INSERT INTO `wsoptlog` VALUES ('2293', '7f0000010b5600000001', '2', '90', '0', '0', '1534130783', '1');
INSERT INTO `wsoptlog` VALUES ('2294', '7f0000010b5600000001', '2', '90', '0', '0', '1534130793', '1');
INSERT INTO `wsoptlog` VALUES ('2295', '7f0000010b5600000001', '2', '90', '0', '0', '1534130803', '1');
INSERT INTO `wsoptlog` VALUES ('2296', '7f0000010b5600000001', '2', '90', '0', '0', '1534130813', '1');
INSERT INTO `wsoptlog` VALUES ('2297', '7f0000010b5600000001', '2', '90', '0', '0', '1534130823', '1');
INSERT INTO `wsoptlog` VALUES ('2298', '7f0000010b5600000001', '2', '90', '0', '0', '1534130833', '1');
INSERT INTO `wsoptlog` VALUES ('2299', '7f0000010b5600000001', '2', '90', '0', '0', '1534130843', '1');
INSERT INTO `wsoptlog` VALUES ('2300', '7f0000010b5600000001', '2', '90', '0', '0', '1534130853', '1');
INSERT INTO `wsoptlog` VALUES ('2301', '7f0000010b5600000001', '2', '90', '0', '0', '1534130863', '1');
INSERT INTO `wsoptlog` VALUES ('2302', '7f0000010b5600000001', '2', '90', '0', '0', '1534130873', '1');
INSERT INTO `wsoptlog` VALUES ('2303', '7f0000010b5600000001', '2', '90', '0', '0', '1534130883', '1');
INSERT INTO `wsoptlog` VALUES ('2304', '7f0000010b5600000001', '2', '90', '0', '0', '1534130893', '1');
INSERT INTO `wsoptlog` VALUES ('2305', '7f0000010b5600000001', '2', '90', '0', '0', '1534130903', '1');
INSERT INTO `wsoptlog` VALUES ('2306', '7f0000010b5600000001', '2', '90', '0', '0', '1534130913', '1');
INSERT INTO `wsoptlog` VALUES ('2307', '7f0000010b5600000001', '2', '90', '0', '0', '1534130923', '1');
INSERT INTO `wsoptlog` VALUES ('2308', '7f0000010b5600000001', '2', '90', '0', '0', '1534130933', '1');
INSERT INTO `wsoptlog` VALUES ('2309', '7f0000010b5600000001', '2', '90', '0', '0', '1534130943', '1');
INSERT INTO `wsoptlog` VALUES ('2310', '7f0000010b5600000001', '2', '90', '0', '0', '1534130953', '1');
INSERT INTO `wsoptlog` VALUES ('2311', '7f0000010b5600000001', '2', '90', '0', '0', '1534130963', '1');
INSERT INTO `wsoptlog` VALUES ('2312', '7f0000010b5600000001', '2', '90', '0', '0', '1534130973', '1');
INSERT INTO `wsoptlog` VALUES ('2313', '7f0000010b5600000001', '2', '90', '0', '0', '1534130983', '1');
INSERT INTO `wsoptlog` VALUES ('2314', '7f0000010b5600000001', '2', '90', '0', '0', '1534130993', '1');
INSERT INTO `wsoptlog` VALUES ('2315', '7f0000010b5600000001', '2', '90', '0', '0', '1534131003', '1');
INSERT INTO `wsoptlog` VALUES ('2316', '7f0000010b5600000001', '2', '90', '0', '0', '1534131013', '1');
INSERT INTO `wsoptlog` VALUES ('2317', '7f0000010b5600000001', '2', '90', '0', '0', '1534131023', '1');
INSERT INTO `wsoptlog` VALUES ('2318', '7f0000010b5600000001', '2', '90', '0', '0', '1534131033', '1');
INSERT INTO `wsoptlog` VALUES ('2319', '7f0000010b5600000001', '2', '90', '0', '0', '1534131043', '1');
INSERT INTO `wsoptlog` VALUES ('2320', '7f0000010b5600000001', '2', '90', '0', '0', '1534131053', '1');
INSERT INTO `wsoptlog` VALUES ('2321', '7f0000010b5600000001', '2', '90', '0', '0', '1534131063', '1');
INSERT INTO `wsoptlog` VALUES ('2322', '7f0000010b5600000001', '2', '90', '0', '0', '1534131073', '1');
INSERT INTO `wsoptlog` VALUES ('2323', '7f0000010b5600000001', '2', '90', '0', '0', '1534131083', '1');
INSERT INTO `wsoptlog` VALUES ('2324', '7f0000010b5600000001', '2', '90', '0', '0', '1534131093', '1');
INSERT INTO `wsoptlog` VALUES ('2325', '7f0000010b5600000001', '2', '90', '0', '0', '1534131103', '1');
INSERT INTO `wsoptlog` VALUES ('2326', '7f0000010b5600000001', '2', '90', '0', '0', '1534131113', '1');
INSERT INTO `wsoptlog` VALUES ('2327', '7f0000010b5600000001', '2', '90', '0', '0', '1534131123', '1');
INSERT INTO `wsoptlog` VALUES ('2328', '7f0000010b5600000001', '2', '90', '0', '0', '1534131133', '1');
INSERT INTO `wsoptlog` VALUES ('2329', '7f0000010b5600000001', '2', '90', '0', '0', '1534131143', '1');
INSERT INTO `wsoptlog` VALUES ('2330', '7f0000010b5600000001', '2', '90', '0', '0', '1534131153', '1');
INSERT INTO `wsoptlog` VALUES ('2331', '7f0000010b5600000001', '2', '90', '0', '0', '1534131163', '1');
INSERT INTO `wsoptlog` VALUES ('2332', '7f0000010b5600000001', '2', '90', '0', '0', '1534131173', '1');
INSERT INTO `wsoptlog` VALUES ('2333', '7f0000010b5600000001', '2', '90', '0', '0', '1534131183', '1');
INSERT INTO `wsoptlog` VALUES ('2334', '7f0000010b5600000001', '2', '90', '0', '0', '1534131193', '1');
INSERT INTO `wsoptlog` VALUES ('2335', '7f0000010b5600000001', '2', '90', '0', '0', '1534131203', '1');
INSERT INTO `wsoptlog` VALUES ('2336', '7f0000010b5600000001', '2', '90', '0', '0', '1534131213', '1');
INSERT INTO `wsoptlog` VALUES ('2337', '7f0000010b5600000001', '2', '90', '0', '0', '1534131223', '1');
INSERT INTO `wsoptlog` VALUES ('2338', '7f0000010b5600000001', '2', '90', '0', '0', '1534131233', '1');
INSERT INTO `wsoptlog` VALUES ('2339', '7f0000010b5600000001', '2', '90', '0', '0', '1534131243', '1');
INSERT INTO `wsoptlog` VALUES ('2340', '7f0000010b5600000001', '2', '90', '0', '0', '1534131253', '1');
INSERT INTO `wsoptlog` VALUES ('2341', '7f0000010b5600000001', '2', '90', '0', '0', '1534131263', '1');
INSERT INTO `wsoptlog` VALUES ('2342', '7f0000010b5600000001', '2', '90', '0', '0', '1534131273', '1');
INSERT INTO `wsoptlog` VALUES ('2343', '7f0000010b5600000001', '2', '90', '0', '0', '1534131283', '1');
INSERT INTO `wsoptlog` VALUES ('2344', '7f0000010b5600000001', '2', '90', '0', '0', '1534131293', '1');
INSERT INTO `wsoptlog` VALUES ('2345', '7f0000010b5600000001', '2', '90', '0', '0', '1534131303', '1');
INSERT INTO `wsoptlog` VALUES ('2346', '7f0000010b5600000001', '2', '90', '0', '0', '1534131313', '1');
INSERT INTO `wsoptlog` VALUES ('2347', '7f0000010b5600000001', '2', '90', '0', '0', '1534131323', '1');
INSERT INTO `wsoptlog` VALUES ('2348', '7f0000010b5600000001', '2', '90', '0', '0', '1534131333', '1');
INSERT INTO `wsoptlog` VALUES ('2349', '7f0000010b5600000001', '2', '90', '0', '0', '1534131343', '1');
INSERT INTO `wsoptlog` VALUES ('2350', '7f0000010b5600000001', '2', '90', '0', '0', '1534131353', '1');
INSERT INTO `wsoptlog` VALUES ('2351', '7f0000010b5600000001', '2', '90', '0', '0', '1534131363', '1');
INSERT INTO `wsoptlog` VALUES ('2352', '7f0000010b5600000001', '2', '90', '0', '0', '1534131373', '1');
INSERT INTO `wsoptlog` VALUES ('2353', '7f0000010b5600000001', '2', '90', '0', '0', '1534131383', '1');
INSERT INTO `wsoptlog` VALUES ('2354', '7f0000010b5600000001', '2', '90', '0', '0', '1534131393', '1');
INSERT INTO `wsoptlog` VALUES ('2355', '7f0000010b5600000001', '2', '90', '0', '0', '1534131403', '1');
INSERT INTO `wsoptlog` VALUES ('2356', '7f0000010b5600000001', '2', '90', '0', '0', '1534131413', '1');
INSERT INTO `wsoptlog` VALUES ('2357', '7f0000010b5600000001', '2', '90', '0', '0', '1534131423', '1');
INSERT INTO `wsoptlog` VALUES ('2358', '7f0000010b5600000001', '2', '90', '0', '0', '1534131433', '1');
INSERT INTO `wsoptlog` VALUES ('2359', '7f0000010b5600000001', '2', '90', '0', '0', '1534131443', '1');
INSERT INTO `wsoptlog` VALUES ('2360', '7f0000010b5600000001', '2', '90', '0', '0', '1534131453', '1');
INSERT INTO `wsoptlog` VALUES ('2361', '7f0000010b5600000001', '2', '90', '0', '0', '1534131463', '1');
INSERT INTO `wsoptlog` VALUES ('2362', '7f0000010b5600000001', '2', '90', '0', '0', '1534131473', '1');
INSERT INTO `wsoptlog` VALUES ('2363', '7f0000010b5600000001', '2', '90', '0', '0', '1534131483', '1');
INSERT INTO `wsoptlog` VALUES ('2364', '7f0000010b5600000001', '2', '90', '0', '0', '1534131493', '1');
INSERT INTO `wsoptlog` VALUES ('2365', '7f0000010b5600000001', '2', '90', '0', '0', '1534131503', '1');
INSERT INTO `wsoptlog` VALUES ('2366', '7f0000010b5600000001', '2', '90', '0', '0', '1534131513', '1');
INSERT INTO `wsoptlog` VALUES ('2367', '7f0000010b5600000001', '2', '90', '0', '0', '1534131523', '1');
INSERT INTO `wsoptlog` VALUES ('2368', '7f0000010b5600000001', '2', '90', '0', '0', '1534131533', '1');
INSERT INTO `wsoptlog` VALUES ('2369', '7f0000010b5600000001', '2', '90', '0', '0', '1534131543', '1');
INSERT INTO `wsoptlog` VALUES ('2370', '7f0000010b5600000001', '2', '90', '0', '0', '1534131553', '1');
INSERT INTO `wsoptlog` VALUES ('2371', '7f0000010b5600000001', '2', '90', '0', '0', '1534131563', '1');
INSERT INTO `wsoptlog` VALUES ('2372', '7f0000010b5600000001', '2', '90', '0', '0', '1534131573', '1');
INSERT INTO `wsoptlog` VALUES ('2373', '7f0000010b5600000001', '2', '90', '0', '0', '1534131583', '1');
INSERT INTO `wsoptlog` VALUES ('2374', '7f0000010b5600000001', '2', '90', '0', '0', '1534131593', '1');
INSERT INTO `wsoptlog` VALUES ('2375', '7f0000010b5600000001', '2', '90', '0', '0', '1534131603', '1');
INSERT INTO `wsoptlog` VALUES ('2376', '7f0000010b5600000001', '2', '90', '0', '0', '1534131613', '1');
INSERT INTO `wsoptlog` VALUES ('2377', '7f0000010b5600000001', '2', '90', '0', '0', '1534131623', '1');
INSERT INTO `wsoptlog` VALUES ('2378', '7f0000010b5600000001', '2', '90', '0', '0', '1534131633', '1');
INSERT INTO `wsoptlog` VALUES ('2379', '7f0000010b5600000001', '2', '90', '0', '0', '1534131643', '1');
INSERT INTO `wsoptlog` VALUES ('2380', '7f0000010b5600000001', '2', '90', '0', '0', '1534131653', '1');
INSERT INTO `wsoptlog` VALUES ('2381', '7f0000010b5600000001', '2', '90', '0', '0', '1534131663', '1');
INSERT INTO `wsoptlog` VALUES ('2382', '7f0000010b5600000001', '2', '90', '0', '0', '1534131673', '1');
INSERT INTO `wsoptlog` VALUES ('2383', '7f0000010b5600000001', '2', '90', '0', '0', '1534131683', '1');
INSERT INTO `wsoptlog` VALUES ('2384', '7f0000010b5600000001', '2', '90', '0', '0', '1534131693', '1');
INSERT INTO `wsoptlog` VALUES ('2385', '7f0000010b5600000001', '2', '90', '0', '0', '1534131703', '1');
INSERT INTO `wsoptlog` VALUES ('2386', '7f0000010b5600000001', '2', '90', '0', '0', '1534131713', '1');
INSERT INTO `wsoptlog` VALUES ('2387', '7f0000010b5600000001', '2', '90', '0', '0', '1534131723', '1');
INSERT INTO `wsoptlog` VALUES ('2388', '7f0000010b5600000001', '2', '90', '0', '0', '1534131733', '1');
INSERT INTO `wsoptlog` VALUES ('2389', '7f0000010b5600000001', '2', '90', '0', '0', '1534131743', '1');
INSERT INTO `wsoptlog` VALUES ('2390', '7f0000010b5600000001', '2', '90', '0', '0', '1534131753', '1');
INSERT INTO `wsoptlog` VALUES ('2391', '7f0000010b5600000001', '2', '90', '0', '0', '1534131763', '1');
INSERT INTO `wsoptlog` VALUES ('2392', '7f0000010b5600000001', '2', '90', '0', '0', '1534131773', '1');
INSERT INTO `wsoptlog` VALUES ('2393', '7f0000010b5600000001', '2', '90', '0', '0', '1534131783', '1');
INSERT INTO `wsoptlog` VALUES ('2394', '7f0000010b5600000001', '2', '90', '0', '0', '1534131793', '1');
INSERT INTO `wsoptlog` VALUES ('2395', '7f0000010b5600000001', '2', '90', '0', '0', '1534131803', '1');
INSERT INTO `wsoptlog` VALUES ('2396', '7f0000010b5600000001', '2', '90', '0', '0', '1534131813', '1');
INSERT INTO `wsoptlog` VALUES ('2397', '7f0000010b5600000001', '2', '90', '0', '0', '1534131823', '1');
INSERT INTO `wsoptlog` VALUES ('2398', '7f0000010b5600000001', '2', '90', '0', '0', '1534131833', '1');
INSERT INTO `wsoptlog` VALUES ('2399', '7f0000010b5600000001', '2', '90', '0', '0', '1534131843', '1');
INSERT INTO `wsoptlog` VALUES ('2400', '7f0000010b5600000001', '2', '90', '0', '0', '1534131853', '1');
INSERT INTO `wsoptlog` VALUES ('2401', '7f0000010b5600000001', '2', '90', '0', '0', '1534131863', '1');
INSERT INTO `wsoptlog` VALUES ('2402', '7f0000010b5600000001', '2', '90', '0', '0', '1534131873', '1');
INSERT INTO `wsoptlog` VALUES ('2403', '7f0000010b5600000001', '2', '90', '0', '0', '1534131883', '1');
INSERT INTO `wsoptlog` VALUES ('2404', '7f0000010b5600000001', '2', '90', '0', '0', '1534131893', '1');
INSERT INTO `wsoptlog` VALUES ('2405', '7f0000010b5600000001', '2', '90', '0', '0', '1534131903', '1');
INSERT INTO `wsoptlog` VALUES ('2406', '7f0000010b5600000001', '2', '90', '0', '0', '1534131913', '1');
INSERT INTO `wsoptlog` VALUES ('2407', '7f0000010b5600000001', '2', '90', '0', '0', '1534131923', '1');
INSERT INTO `wsoptlog` VALUES ('2408', '7f0000010b5600000001', '2', '90', '0', '0', '1534131933', '1');
INSERT INTO `wsoptlog` VALUES ('2409', '7f0000010b5600000001', '2', '90', '0', '0', '1534131943', '1');
INSERT INTO `wsoptlog` VALUES ('2410', '7f0000010b5600000001', '2', '90', '0', '0', '1534131953', '1');
INSERT INTO `wsoptlog` VALUES ('2411', '7f0000010b5600000001', '2', '90', '0', '0', '1534131963', '1');
INSERT INTO `wsoptlog` VALUES ('2161', '7f0000010b5600000001', '2', '90', '0', '0', '1534129523', '1');
INSERT INTO `wsoptlog` VALUES ('2414', '7f0000010b5600000001', '2', '90', '0', '0', '1534131993', '1');
INSERT INTO `wsoptlog` VALUES ('2415', '7f0000010b5600000001', '2', '90', '0', '0', '1534132003', '1');
INSERT INTO `wsoptlog` VALUES ('2416', '7f0000010b5600000001', '2', '90', '0', '0', '1534132013', '1');
INSERT INTO `wsoptlog` VALUES ('2417', '7f0000010b5600000001', '2', '90', '0', '0', '1534132023', '1');
INSERT INTO `wsoptlog` VALUES ('2418', '7f0000010b5600000001', '2', '90', '0', '0', '1534132033', '1');
INSERT INTO `wsoptlog` VALUES ('2419', '7f0000010b5600000001', '2', '90', '0', '0', '1534132043', '1');
INSERT INTO `wsoptlog` VALUES ('2159', '7f0000010b5600000001', '2', '90', '0', '0', '1534129503', '1');
INSERT INTO `wsoptlog` VALUES ('2158', '7f0000010b5600000001', '2', '90', '0', '0', '1534129493', '1');
INSERT INTO `wsoptlog` VALUES ('2157', '7f0000010b5600000001', '2', '90', '0', '0', '1534129483', '1');
INSERT INTO `wsoptlog` VALUES ('2424', '7f0000010b5600000001', '2', '90', '0', '0', '1534132093', '1');
INSERT INTO `wsoptlog` VALUES ('2155', '7f0000010b5600000001', '2', '90', '0', '0', '1534129463', '1');
INSERT INTO `wsoptlog` VALUES ('2426', '7f0000010b5600000001', '2', '90', '0', '0', '1534132113', '1');
INSERT INTO `wsoptlog` VALUES ('2427', '7f0000010b5600000001', '2', '90', '0', '0', '1534132123', '1');
INSERT INTO `wsoptlog` VALUES ('2428', '7f0000010b5600000001', '2', '90', '0', '0', '1534132133', '1');
INSERT INTO `wsoptlog` VALUES ('2109', '7f0000010b5600000001', '2', '90', '0', '0', '1534129003', '1');
INSERT INTO `wsoptlog` VALUES ('2430', '7f0000010b5600000001', '2', '90', '0', '0', '1534132159', '1');
INSERT INTO `wsoptlog` VALUES ('2108', '7f0000010b5600000001', '2', '90', '0', '0', '1534128993', '1');
INSERT INTO `wsoptlog` VALUES ('2432', '7f0000010b5600000001', '2', '90', '0', '0', '1534132176', '1');
INSERT INTO `wsoptlog` VALUES ('2107', '7f0000010b5600000001', '2', '90', '0', '0', '1534128983', '1');
INSERT INTO `wsoptlog` VALUES ('2434', '7f0000010b5600000001', '2', '90', '0', '0', '1534132193', '1');
INSERT INTO `wsoptlog` VALUES ('2106', '7f0000010b5600000001', '2', '90', '0', '0', '1534128973', '1');
INSERT INTO `wsoptlog` VALUES ('2436', '7f0000010b5600000001', '2', '90', '0', '0', '1534132213', '1');
INSERT INTO `wsoptlog` VALUES ('2437', '7f0000010b5600000001', '2', '90', '0', '0', '1534132223', '1');
INSERT INTO `wsoptlog` VALUES ('2438', '7f0000010b5600000001', '2', '90', '0', '0', '1534132233', '1');
INSERT INTO `wsoptlog` VALUES ('2439', '7f0000010b5600000001', '2', '90', '0', '0', '1534132243', '1');
INSERT INTO `wsoptlog` VALUES ('2440', '7f0000010b5600000001', '2', '90', '0', '0', '1534132253', '1');
INSERT INTO `wsoptlog` VALUES ('2441', '7f0000010b5600000001', '2', '90', '0', '0', '1534132263', '1');
INSERT INTO `wsoptlog` VALUES ('2442', '7f0000010b5600000001', '2', '90', '0', '0', '1534132273', '1');
INSERT INTO `wsoptlog` VALUES ('2443', '7f0000010b5600000001', '2', '90', '0', '0', '1534132283', '1');
INSERT INTO `wsoptlog` VALUES ('2444', '7f0000010b5600000001', '2', '90', '0', '0', '1534132293', '1');
INSERT INTO `wsoptlog` VALUES ('2445', '7f0000010b5600000001', '2', '90', '0', '0', '1534132303', '1');
INSERT INTO `wsoptlog` VALUES ('2446', '7f0000010b5600000001', '2', '90', '0', '0', '1534132313', '1');
INSERT INTO `wsoptlog` VALUES ('2105', '7f0000010b5600000001', '2', '90', '0', '0', '1534128963', '1');
INSERT INTO `wsoptlog` VALUES ('2449', '7f0000010b5600000001', '2', '90', '0', '0', '1534132343', '1');
INSERT INTO `wsoptlog` VALUES ('2100', '7f0000010b5600000001', '2', '90', '0', '0', '1534128913', '1');
INSERT INTO `wsoptlog` VALUES ('2451', '7f0000010b5600000001', '2', '90', '0', '0', '1534132363', '1');
INSERT INTO `wsoptlog` VALUES ('2452', '7f0000010b5600000001', '2', '90', '0', '0', '1534132373', '1');
INSERT INTO `wsoptlog` VALUES ('2453', '7f0000010b5600000001', '2', '90', '0', '0', '1534132383', '1');
INSERT INTO `wsoptlog` VALUES ('2455', '7f0000010b5600000001', '2', '90', '0', '0', '1534132403', '1');
INSERT INTO `wsoptlog` VALUES ('2456', '7f0000010b5600000001', '2', '90', '0', '0', '1534132413', '1');
INSERT INTO `wsoptlog` VALUES ('2457', '7f0000010b5600000001', '2', '90', '0', '0', '1534132423', '1');
INSERT INTO `wsoptlog` VALUES ('2458', '7f0000010b5600000001', '2', '90', '0', '0', '1534132433', '1');
INSERT INTO `wsoptlog` VALUES ('2099', '7f0000010b5600000001', '2', '90', '0', '0', '1534128903', '1');
INSERT INTO `wsoptlog` VALUES ('2460', '7f0000010b5600000001', '2', '90', '0', '0', '1534132453', '1');
INSERT INTO `wsoptlog` VALUES ('1961', '7f0000010b5600000001', '2', '90', '0', '0', '1534128063', '1');
INSERT INTO `wsoptlog` VALUES ('2098', '7f0000010b5600000001', '2', '90', '0', '0', '1534128893', '1');
INSERT INTO `wsoptlog` VALUES ('1964', '7f0000010b5600000001', '2', '90', '0', '0', '1534128093', '1');
INSERT INTO `wsoptlog` VALUES ('1965', '7f0000010b5600000001', '2', '90', '0', '0', '1534128103', '1');
INSERT INTO `wsoptlog` VALUES ('1966', '7f0000010b5600000001', '2', '90', '0', '0', '1534128113', '1');
INSERT INTO `wsoptlog` VALUES ('1967', '7f0000010b5600000001', '2', '90', '0', '0', '1534128123', '1');
INSERT INTO `wsoptlog` VALUES ('1968', '7f0000010b5600000001', '2', '90', '0', '0', '1534128133', '1');
INSERT INTO `wsoptlog` VALUES ('2097', '7f0000010b5600000001', '2', '90', '0', '0', '1534128883', '1');
INSERT INTO `wsoptlog` VALUES ('1970', '7f0000010b5600000001', '2', '90', '0', '0', '1534128153', '1');
INSERT INTO `wsoptlog` VALUES ('1971', '7f0000010b5600000001', '2', '90', '0', '0', '1534128163', '1');
INSERT INTO `wsoptlog` VALUES ('1972', '7f0000010b5600000001', '2', '90', '0', '0', '1534128173', '1');
INSERT INTO `wsoptlog` VALUES ('1973', '7f0000010b5600000001', '2', '90', '0', '0', '1534128183', '1');
INSERT INTO `wsoptlog` VALUES ('1974', '7f0000010b5600000001', '2', '90', '0', '0', '1534128193', '1');
INSERT INTO `wsoptlog` VALUES ('1975', '7f0000010b5600000001', '2', '90', '0', '0', '1534128203', '1');
INSERT INTO `wsoptlog` VALUES ('1976', '7f0000010b5600000001', '2', '90', '0', '0', '1534128213', '1');
INSERT INTO `wsoptlog` VALUES ('1977', '7f0000010b5600000001', '2', '90', '0', '0', '1534128223', '1');
INSERT INTO `wsoptlog` VALUES ('1978', '7f0000010b5600000001', '2', '90', '0', '0', '1534128233', '1');
INSERT INTO `wsoptlog` VALUES ('1979', '7f0000010b5600000001', '2', '90', '0', '0', '1534128243', '1');
INSERT INTO `wsoptlog` VALUES ('1980', '7f0000010b5600000001', '2', '90', '0', '0', '1534128253', '1');
INSERT INTO `wsoptlog` VALUES ('1981', '7f0000010b5600000001', '2', '90', '0', '0', '1534128263', '1');
INSERT INTO `wsoptlog` VALUES ('1982', '7f0000010b5600000001', '2', '90', '0', '0', '1534128273', '1');
INSERT INTO `wsoptlog` VALUES ('1983', '7f0000010b5600000001', '2', '90', '0', '0', '1534128283', '1');
INSERT INTO `wsoptlog` VALUES ('1984', '7f0000010b5600000001', '2', '90', '0', '0', '1534128293', '1');
INSERT INTO `wsoptlog` VALUES ('1985', '7f0000010b5600000001', '2', '90', '0', '0', '1534128303', '1');
INSERT INTO `wsoptlog` VALUES ('1986', '7f0000010b5600000001', '2', '90', '0', '0', '1534128313', '1');
INSERT INTO `wsoptlog` VALUES ('1987', '7f0000010b5600000001', '2', '90', '0', '0', '1534128323', '1');
INSERT INTO `wsoptlog` VALUES ('1988', '7f0000010b5600000001', '2', '90', '0', '0', '1534128333', '1');
INSERT INTO `wsoptlog` VALUES ('1989', '7f0000010b5600000001', '2', '90', '0', '0', '1534128343', '1');
INSERT INTO `wsoptlog` VALUES ('1990', '7f0000010b5600000001', '2', '90', '0', '0', '1534128353', '1');
INSERT INTO `wsoptlog` VALUES ('1991', '7f0000010b5600000001', '2', '90', '0', '0', '1534128363', '1');
INSERT INTO `wsoptlog` VALUES ('1992', '7f0000010b5600000001', '2', '90', '0', '0', '1534128373', '1');
INSERT INTO `wsoptlog` VALUES ('1993', '7f0000010b5600000001', '2', '90', '0', '0', '1534128383', '1');
INSERT INTO `wsoptlog` VALUES ('1994', '7f0000010b5600000001', '2', '90', '0', '0', '1534128393', '1');
INSERT INTO `wsoptlog` VALUES ('1995', '7f0000010b5600000001', '2', '90', '0', '0', '1534128403', '1');
INSERT INTO `wsoptlog` VALUES ('1996', '7f0000010b5600000001', '2', '90', '0', '0', '1534128413', '1');
INSERT INTO `wsoptlog` VALUES ('1997', '7f0000010b5600000001', '2', '90', '0', '0', '1534128423', '1');
INSERT INTO `wsoptlog` VALUES ('1998', '7f0000010b5600000001', '2', '90', '0', '0', '1534128433', '1');
INSERT INTO `wsoptlog` VALUES ('1999', '7f0000010b5600000001', '2', '90', '0', '0', '1534128443', '1');
INSERT INTO `wsoptlog` VALUES ('2000', '7f0000010a2900000002', 'cd7', '90', '0', '0', '1534128444', '1');
INSERT INTO `wsoptlog` VALUES ('2001', '7f0000010b5600000001', '2', '90', '0', '0', '1534128453', '1');
INSERT INTO `wsoptlog` VALUES ('2002', '7f0000010b5600000001', '2', '90', '0', '0', '1534128463', '1');
INSERT INTO `wsoptlog` VALUES ('2003', '7f0000010b5600000001', '2', '90', '0', '0', '1534128473', '1');
INSERT INTO `wsoptlog` VALUES ('2004', '7f0000010b5600000001', '2', '90', '0', '0', '1534128483', '1');
INSERT INTO `wsoptlog` VALUES ('2005', '7f0000010b5600000001', '2', '90', '0', '0', '1534128493', '1');
INSERT INTO `wsoptlog` VALUES ('2006', '7f0000010b5600000001', '2', '90', '0', '0', '1534128503', '1');
INSERT INTO `wsoptlog` VALUES ('2007', '7f0000010b5600000001', '2', '90', '0', '0', '1534128513', '1');
INSERT INTO `wsoptlog` VALUES ('2008', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128520', '1');
INSERT INTO `wsoptlog` VALUES ('2009', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128522', '1');
INSERT INTO `wsoptlog` VALUES ('2010', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128523', '1');
INSERT INTO `wsoptlog` VALUES ('2011', '7f0000010b5600000001', '2', '90', '0', '0', '1534128523', '1');
INSERT INTO `wsoptlog` VALUES ('2012', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128524', '1');
INSERT INTO `wsoptlog` VALUES ('2013', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128525', '1');
INSERT INTO `wsoptlog` VALUES ('2014', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128526', '1');
INSERT INTO `wsoptlog` VALUES ('2015', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128527', '1');
INSERT INTO `wsoptlog` VALUES ('2016', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128528', '1');
INSERT INTO `wsoptlog` VALUES ('2017', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128530', '1');
INSERT INTO `wsoptlog` VALUES ('2018', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128531', '1');
INSERT INTO `wsoptlog` VALUES ('2019', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128532', '1');
INSERT INTO `wsoptlog` VALUES ('2020', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128533', '1');
INSERT INTO `wsoptlog` VALUES ('2021', '7f0000010b5600000001', '2', '90', '0', '0', '1534128533', '1');
INSERT INTO `wsoptlog` VALUES ('2022', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128534', '1');
INSERT INTO `wsoptlog` VALUES ('2023', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128535', '1');
INSERT INTO `wsoptlog` VALUES ('2024', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128537', '1');
INSERT INTO `wsoptlog` VALUES ('2025', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128538', '1');
INSERT INTO `wsoptlog` VALUES ('2026', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128539', '1');
INSERT INTO `wsoptlog` VALUES ('2027', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128541', '1');
INSERT INTO `wsoptlog` VALUES ('2028', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128542', '1');
INSERT INTO `wsoptlog` VALUES ('2029', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128543', '1');
INSERT INTO `wsoptlog` VALUES ('2030', '7f0000010b5600000001', '2', '90', '0', '0', '1534128543', '1');
INSERT INTO `wsoptlog` VALUES ('2031', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128544', '1');
INSERT INTO `wsoptlog` VALUES ('2032', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128545', '1');
INSERT INTO `wsoptlog` VALUES ('2033', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128546', '1');
INSERT INTO `wsoptlog` VALUES ('2034', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128547', '1');
INSERT INTO `wsoptlog` VALUES ('2035', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128548', '1');
INSERT INTO `wsoptlog` VALUES ('2036', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128549', '1');
INSERT INTO `wsoptlog` VALUES ('2037', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128550', '1');
INSERT INTO `wsoptlog` VALUES ('2038', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128552', '1');
INSERT INTO `wsoptlog` VALUES ('2039', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128553', '1');
INSERT INTO `wsoptlog` VALUES ('2040', '7f0000010b5600000001', '2', '90', '0', '0', '1534128553', '1');
INSERT INTO `wsoptlog` VALUES ('2041', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128554', '1');
INSERT INTO `wsoptlog` VALUES ('2042', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128555', '1');
INSERT INTO `wsoptlog` VALUES ('2043', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128556', '1');
INSERT INTO `wsoptlog` VALUES ('2044', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128557', '1');
INSERT INTO `wsoptlog` VALUES ('2045', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128558', '1');
INSERT INTO `wsoptlog` VALUES ('2046', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128559', '1');
INSERT INTO `wsoptlog` VALUES ('2047', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128560', '1');
INSERT INTO `wsoptlog` VALUES ('2048', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128561', '1');
INSERT INTO `wsoptlog` VALUES ('2049', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128563', '1');
INSERT INTO `wsoptlog` VALUES ('2050', '7f0000010b5600000001', '2', '90', '0', '0', '1534128563', '1');
INSERT INTO `wsoptlog` VALUES ('2051', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128564', '1');
INSERT INTO `wsoptlog` VALUES ('2052', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128565', '1');
INSERT INTO `wsoptlog` VALUES ('2053', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128566', '1');
INSERT INTO `wsoptlog` VALUES ('2054', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128567', '1');
INSERT INTO `wsoptlog` VALUES ('2055', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128568', '1');
INSERT INTO `wsoptlog` VALUES ('2056', '7f0000010b5400000001', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128569', '1');
INSERT INTO `wsoptlog` VALUES ('2057', '7f0000010b5600000001', '2', '90', '0', '0', '1534128573', '1');
INSERT INTO `wsoptlog` VALUES ('2058', '7f0000010b5600000001', '2', '90', '0', '0', '1534128583', '1');
INSERT INTO `wsoptlog` VALUES ('2059', '7f0000010b5600000001', '2', '90', '0', '0', '1534128593', '1');
INSERT INTO `wsoptlog` VALUES ('2060', '7f0000010b5500000002', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128594', '1');
INSERT INTO `wsoptlog` VALUES ('2061', '7f0000010b5500000002', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128595', '1');
INSERT INTO `wsoptlog` VALUES ('2062', '7f0000010b5500000002', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128596', '1');
INSERT INTO `wsoptlog` VALUES ('2063', '7f0000010b5500000002', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128597', '1');
INSERT INTO `wsoptlog` VALUES ('2064', '7f0000010b5500000002', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128598', '1');
INSERT INTO `wsoptlog` VALUES ('2065', '7f0000010b5500000002', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128599', '1');
INSERT INTO `wsoptlog` VALUES ('2066', '7f0000010b5500000002', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128600', '1');
INSERT INTO `wsoptlog` VALUES ('2067', '7f0000010b5500000002', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128601', '1');
INSERT INTO `wsoptlog` VALUES ('2068', '7f0000010b5500000002', '{"maxhom":"json","cmd":"login","mac":"84F3EB73B2AA"}', '90', '0', '0', '1534128603', '1');
INSERT INTO `wsoptlog` VALUES ('2069', '7f0000010b5600000001', '2', '90', '0', '0', '1534128603', '1');
INSERT INTO `wsoptlog` VALUES ('2070', '7f0000010b5600000001', '2', '90', '0', '0', '1534128613', '1');
INSERT INTO `wsoptlog` VALUES ('2071', '7f0000010b5600000001', '2', '90', '0', '0', '1534128623', '1');
INSERT INTO `wsoptlog` VALUES ('2072', '7f0000010b5600000001', '2', '90', '0', '0', '1534128633', '1');
INSERT INTO `wsoptlog` VALUES ('2073', '7f0000010b5600000001', '2', '90', '0', '0', '1534128643', '1');
INSERT INTO `wsoptlog` VALUES ('2074', '7f0000010b5600000001', '2', '90', '0', '0', '1534128653', '1');
INSERT INTO `wsoptlog` VALUES ('2075', '7f0000010b5600000001', '2', '90', '0', '0', '1534128663', '1');
INSERT INTO `wsoptlog` VALUES ('2076', '7f0000010b5600000001', '2', '90', '0', '0', '1534128673', '1');
INSERT INTO `wsoptlog` VALUES ('2077', '7f0000010b5600000001', '2', '90', '0', '0', '1534128683', '1');
INSERT INTO `wsoptlog` VALUES ('2078', '7f0000010b5600000001', '2', '90', '0', '0', '1534128693', '1');
INSERT INTO `wsoptlog` VALUES ('2079', '7f0000010b5600000001', '2', '90', '0', '0', '1534128703', '1');
INSERT INTO `wsoptlog` VALUES ('2080', '7f0000010b5600000001', '2', '90', '0', '0', '1534128713', '1');
INSERT INTO `wsoptlog` VALUES ('2081', '7f0000010b5600000001', '2', '90', '0', '0', '1534128723', '1');
INSERT INTO `wsoptlog` VALUES ('2082', '7f0000010b5600000001', '2', '90', '0', '0', '1534128733', '1');
INSERT INTO `wsoptlog` VALUES ('2083', '7f0000010b5600000001', '2', '90', '0', '0', '1534128743', '1');
INSERT INTO `wsoptlog` VALUES ('2084', '7f0000010b5600000001', '2', '90', '0', '0', '1534128753', '1');
INSERT INTO `wsoptlog` VALUES ('2085', '7f0000010b5600000001', '2', '90', '0', '0', '1534128763', '1');
INSERT INTO `wsoptlog` VALUES ('2086', '7f0000010b5600000001', '2', '90', '0', '0', '1534128773', '1');
INSERT INTO `wsoptlog` VALUES ('2087', '7f0000010b5600000001', '2', '90', '0', '0', '1534128783', '1');
INSERT INTO `wsoptlog` VALUES ('2088', '7f0000010b5600000001', '2', '90', '0', '0', '1534128793', '1');
INSERT INTO `wsoptlog` VALUES ('2089', '7f0000010b5600000001', '2', '90', '0', '0', '1534128803', '1');
INSERT INTO `wsoptlog` VALUES ('2090', '7f0000010b5600000001', '2', '90', '0', '0', '1534128813', '1');
INSERT INTO `wsoptlog` VALUES ('2091', '7f0000010b5600000001', '2', '90', '0', '0', '1534128823', '1');
INSERT INTO `wsoptlog` VALUES ('2092', '7f0000010b5600000001', '2', '90', '0', '0', '1534128833', '1');
INSERT INTO `wsoptlog` VALUES ('2093', '7f0000010b5600000001', '2', '90', '0', '0', '1534128843', '1');
INSERT INTO `wsoptlog` VALUES ('2094', '7f0000010b5600000001', '2', '90', '0', '0', '1534128853', '1');
INSERT INTO `wsoptlog` VALUES ('2095', '7f0000010b5600000001', '2', '90', '0', '0', '1534128863', '1');
INSERT INTO `wsoptlog` VALUES ('2096', '7f0000010b5600000001', '2', '90', '0', '0', '1534128873', '1');
INSERT INTO `wsoptlog` VALUES ('2101', '7f0000010b5600000001', '2', '90', '0', '0', '1534128923', '1');
INSERT INTO `wsoptlog` VALUES ('2102', '7f0000010b5600000001', '2', '90', '0', '0', '1534128933', '1');
INSERT INTO `wsoptlog` VALUES ('2103', '7f0000010b5600000001', '2', '90', '0', '0', '1534128943', '1');
INSERT INTO `wsoptlog` VALUES ('2104', '7f0000010b5600000001', '2', '90', '0', '0', '1534128953', '1');
INSERT INTO `wsoptlog` VALUES ('2110', '7f0000010b5600000001', '2', '90', '0', '0', '1534129013', '1');
INSERT INTO `wsoptlog` VALUES ('2111', '7f0000010b5600000001', '2', '90', '0', '0', '1534129023', '1');
INSERT INTO `wsoptlog` VALUES ('2112', '7f0000010b5600000001', '2', '90', '0', '0', '1534129033', '1');
INSERT INTO `wsoptlog` VALUES ('2113', '7f0000010b5600000001', '2', '90', '0', '0', '1534129043', '1');
INSERT INTO `wsoptlog` VALUES ('2114', '7f0000010b5600000001', '2', '90', '0', '0', '1534129053', '1');
INSERT INTO `wsoptlog` VALUES ('2115', '7f0000010b5600000001', '2', '90', '0', '0', '1534129063', '1');
INSERT INTO `wsoptlog` VALUES ('2116', '7f0000010b5600000001', '2', '90', '0', '0', '1534129073', '1');
INSERT INTO `wsoptlog` VALUES ('2117', '7f0000010b5600000001', '2', '90', '0', '0', '1534129083', '1');
INSERT INTO `wsoptlog` VALUES ('2118', '7f0000010b5600000001', '2', '90', '0', '0', '1534129093', '1');
INSERT INTO `wsoptlog` VALUES ('2119', '7f0000010b5600000001', '2', '90', '0', '0', '1534129103', '1');
INSERT INTO `wsoptlog` VALUES ('2120', '7f0000010b5600000001', '2', '90', '0', '0', '1534129113', '1');
INSERT INTO `wsoptlog` VALUES ('2121', '7f0000010b5600000001', '2', '90', '0', '0', '1534129123', '1');
INSERT INTO `wsoptlog` VALUES ('2122', '7f0000010b5600000001', '2', '90', '0', '0', '1534129133', '1');
INSERT INTO `wsoptlog` VALUES ('2123', '7f0000010b5600000001', '2', '90', '0', '0', '1534129143', '1');
INSERT INTO `wsoptlog` VALUES ('2124', '7f0000010b5600000001', '2', '90', '0', '0', '1534129153', '1');
INSERT INTO `wsoptlog` VALUES ('2125', '7f0000010b5600000001', '2', '90', '0', '0', '1534129163', '1');
INSERT INTO `wsoptlog` VALUES ('2126', '7f0000010b5600000001', '2', '90', '0', '0', '1534129173', '1');
INSERT INTO `wsoptlog` VALUES ('2127', '7f0000010b5600000001', '2', '90', '0', '0', '1534129183', '1');
INSERT INTO `wsoptlog` VALUES ('2128', '7f0000010b5600000001', '2', '90', '0', '0', '1534129193', '1');
INSERT INTO `wsoptlog` VALUES ('2129', '7f0000010b5600000001', '2', '90', '0', '0', '1534129203', '1');
INSERT INTO `wsoptlog` VALUES ('2130', '7f0000010b5600000001', '2', '90', '0', '0', '1534129213', '1');
INSERT INTO `wsoptlog` VALUES ('2131', '7f0000010b5600000001', '2', '90', '0', '0', '1534129223', '1');
INSERT INTO `wsoptlog` VALUES ('2132', '7f0000010b5600000001', '2', '90', '0', '0', '1534129233', '1');
INSERT INTO `wsoptlog` VALUES ('2133', '7f0000010b5600000001', '2', '90', '0', '0', '1534129243', '1');
INSERT INTO `wsoptlog` VALUES ('2134', '7f0000010b5600000001', '2', '90', '0', '0', '1534129253', '1');
INSERT INTO `wsoptlog` VALUES ('2135', '7f0000010b5600000001', '2', '90', '0', '0', '1534129263', '1');
INSERT INTO `wsoptlog` VALUES ('2136', '7f0000010b5600000001', '2', '90', '0', '0', '1534129273', '1');
INSERT INTO `wsoptlog` VALUES ('2137', '7f0000010b5600000001', '2', '90', '0', '0', '1534129283', '1');
INSERT INTO `wsoptlog` VALUES ('2138', '7f0000010b5600000001', '2', '90', '0', '0', '1534129293', '1');
INSERT INTO `wsoptlog` VALUES ('2139', '7f0000010b5600000001', '2', '90', '0', '0', '1534129303', '1');
INSERT INTO `wsoptlog` VALUES ('2140', '7f0000010b5600000001', '2', '90', '0', '0', '1534129313', '1');
INSERT INTO `wsoptlog` VALUES ('2141', '7f0000010b5600000001', '2', '90', '0', '0', '1534129323', '1');
INSERT INTO `wsoptlog` VALUES ('2142', '7f0000010b5600000001', '2', '90', '0', '0', '1534129333', '1');
INSERT INTO `wsoptlog` VALUES ('2143', '7f0000010b5600000001', '2', '90', '0', '0', '1534129343', '1');
INSERT INTO `wsoptlog` VALUES ('2144', '7f0000010b5600000001', '2', '90', '0', '0', '1534129353', '1');
INSERT INTO `wsoptlog` VALUES ('2145', '7f0000010b5600000001', '2', '90', '0', '0', '1534129363', '1');
INSERT INTO `wsoptlog` VALUES ('2146', '7f0000010b5600000001', '2', '90', '0', '0', '1534129373', '1');
INSERT INTO `wsoptlog` VALUES ('2147', '7f0000010b5600000001', '2', '90', '0', '0', '1534129383', '1');
INSERT INTO `wsoptlog` VALUES ('2148', '7f0000010b5600000001', '2', '90', '0', '0', '1534129393', '1');
INSERT INTO `wsoptlog` VALUES ('2149', '7f0000010b5600000001', '2', '90', '0', '0', '1534129403', '1');
INSERT INTO `wsoptlog` VALUES ('2150', '7f0000010b5600000001', '2', '90', '0', '0', '1534129413', '1');
INSERT INTO `wsoptlog` VALUES ('2151', '7f0000010b5600000001', '2', '90', '0', '0', '1534129423', '1');
INSERT INTO `wsoptlog` VALUES ('2152', '7f0000010b5600000001', '2', '90', '0', '0', '1534129433', '1');
INSERT INTO `wsoptlog` VALUES ('2153', '7f0000010b5600000001', '2', '90', '0', '0', '1534129443', '1');
INSERT INTO `wsoptlog` VALUES ('2154', '7f0000010b5600000001', '2', '90', '0', '0', '1534129453', '1');
INSERT INTO `wsoptlog` VALUES ('2156', '7f0000010b5600000001', '2', '90', '0', '0', '1534129473', '1');
INSERT INTO `wsoptlog` VALUES ('2162', '7f0000010b5600000001', '2', '90', '0', '0', '1534129533', '1');
INSERT INTO `wsoptlog` VALUES ('2170', '7f0000010b5600000001', '2', '90', '0', '0', '1534129613', '1');
INSERT INTO `wsoptlog` VALUES ('2171', '7f0000010b5600000001', '2', '90', '0', '0', '1534129623', '1');
INSERT INTO `wsoptlog` VALUES ('2172', '7f0000010b5600000001', '2', '90', '0', '0', '1534129633', '1');
INSERT INTO `wsoptlog` VALUES ('2173', '7f0000010b5600000001', '2', '90', '0', '0', '1534129643', '1');
INSERT INTO `wsoptlog` VALUES ('2174', '7f0000010b5600000001', '2', '90', '0', '0', '1534129653', '1');
INSERT INTO `wsoptlog` VALUES ('2175', '7f0000010b5600000001', '2', '90', '0', '0', '1534129663', '1');
INSERT INTO `wsoptlog` VALUES ('2176', '7f0000010b5600000001', '2', '90', '0', '0', '1534129673', '1');
INSERT INTO `wsoptlog` VALUES ('2177', '7f0000010b5600000001', '2', '90', '0', '0', '1534129683', '1');
INSERT INTO `wsoptlog` VALUES ('2178', '7f0000010b5600000001', '2', '90', '0', '0', '1534129693', '1');
INSERT INTO `wsoptlog` VALUES ('2179', '7f0000010b5600000001', '2', '90', '0', '0', '1534129703', '1');
INSERT INTO `wsoptlog` VALUES ('2180', '7f0000010b5600000001', '2', '90', '0', '0', '1534129713', '1');
INSERT INTO `wsoptlog` VALUES ('2181', '7f0000010b5600000001', '2', '90', '0', '0', '1534129723', '1');
INSERT INTO `wsoptlog` VALUES ('2182', '7f0000010b5600000001', '2', '90', '0', '0', '1534129733', '1');
INSERT INTO `wsoptlog` VALUES ('2183', '7f0000010b5600000001', '2', '90', '0', '0', '1534129743', '1');
INSERT INTO `wsoptlog` VALUES ('2184', '7f0000010b5600000001', '2', '90', '0', '0', '1534129753', '1');
INSERT INTO `wsoptlog` VALUES ('2185', '7f0000010b5600000001', '2', '90', '0', '0', '1534129763', '1');
INSERT INTO `wsoptlog` VALUES ('2186', '7f0000010b5600000001', '2', '90', '0', '0', '1534129773', '1');
INSERT INTO `wsoptlog` VALUES ('2187', '7f0000010b5600000001', '2', '90', '0', '0', '1534129783', '1');
INSERT INTO `wsoptlog` VALUES ('2188', '7f0000010b5600000001', '2', '90', '0', '0', '1534129793', '1');
INSERT INTO `wsoptlog` VALUES ('2189', '7f0000010b5600000001', '2', '90', '0', '0', '1534129803', '1');
INSERT INTO `wsoptlog` VALUES ('2190', '7f0000010b5600000001', '2', '90', '0', '0', '1534129813', '1');
INSERT INTO `wsoptlog` VALUES ('2191', '7f0000010b5600000001', '2', '90', '0', '0', '1534129823', '1');
INSERT INTO `wsoptlog` VALUES ('2192', '7f0000010b5600000001', '2', '90', '0', '0', '1534129833', '1');
INSERT INTO `wsoptlog` VALUES ('2193', '7f0000010b5600000001', '2', '90', '0', '0', '1534129843', '1');
INSERT INTO `wsoptlog` VALUES ('2194', '7f0000010b5600000001', '2', '90', '0', '0', '1534129853', '1');
INSERT INTO `wsoptlog` VALUES ('2195', '7f0000010b5600000001', '2', '90', '0', '0', '1534129863', '1');
INSERT INTO `wsoptlog` VALUES ('2196', '7f0000010b5600000001', '2', '90', '0', '0', '1534129873', '1');
INSERT INTO `wsoptlog` VALUES ('2197', '7f0000010b5600000001', '2', '90', '0', '0', '1534129883', '1');
INSERT INTO `wsoptlog` VALUES ('2198', '7f0000010b5600000001', '2', '90', '0', '0', '1534129893', '1');
INSERT INTO `wsoptlog` VALUES ('2199', '7f0000010b5600000001', '2', '90', '0', '0', '1534129903', '1');
INSERT INTO `wsoptlog` VALUES ('2200', '7f0000010b5600000001', '2', '90', '0', '0', '1534129913', '1');
INSERT INTO `wsoptlog` VALUES ('2201', '7f0000010b5600000001', '2', '90', '0', '0', '1534129923', '1');
INSERT INTO `wsoptlog` VALUES ('2202', '7f0000010b5600000001', '2', '90', '0', '0', '1534129933', '1');
INSERT INTO `wsoptlog` VALUES ('2203', '7f0000010b5600000001', '2', '90', '0', '0', '1534129943', '1');
INSERT INTO `wsoptlog` VALUES ('2204', '7f0000010b5600000001', '2', '90', '0', '0', '1534129953', '1');
INSERT INTO `wsoptlog` VALUES ('2205', '7f0000010b5600000001', '2', '90', '0', '0', '1534129963', '1');
INSERT INTO `wsoptlog` VALUES ('2206', '7f0000010b5600000001', '2', '90', '0', '0', '1534129973', '1');
INSERT INTO `wsoptlog` VALUES ('2207', '7f0000010b5600000001', '2', '90', '0', '0', '1534129983', '1');
INSERT INTO `wsoptlog` VALUES ('2208', '7f0000010b5600000001', '2', '90', '0', '0', '1534129993', '1');
INSERT INTO `wsoptlog` VALUES ('2209', '7f0000010b5600000001', '2', '90', '0', '0', '1534130003', '1');
INSERT INTO `wsoptlog` VALUES ('2420', '7f0000010b5600000001', '2', '90', '0', '0', '1534132053', '1');
INSERT INTO `wsoptlog` VALUES ('2421', '7f0000010b5600000001', '2', '90', '0', '0', '1534132063', '1');
INSERT INTO `wsoptlog` VALUES ('2422', '7f0000010b5600000001', '2', '90', '0', '0', '1534132073', '1');
INSERT INTO `wsoptlog` VALUES ('2423', '7f0000010b5600000001', '2', '90', '0', '0', '1534132083', '1');
INSERT INTO `wsoptlog` VALUES ('2425', '7f0000010b5600000001', '2', '90', '0', '0', '1534132103', '1');
INSERT INTO `wsoptlog` VALUES ('2429', '7f0000010b5600000001', '2', '90', '0', '0', '1534132143', '1');
INSERT INTO `wsoptlog` VALUES ('2431', '7f0000010b5600000001', '2', '90', '0', '0', '1534132163', '1');
INSERT INTO `wsoptlog` VALUES ('2433', '7f0000010b5600000001', '2', '90', '0', '0', '1534132183', '1');
INSERT INTO `wsoptlog` VALUES ('2435', '7f0000010b5600000001', '2', '90', '0', '0', '1534132203', '1');
INSERT INTO `wsoptlog` VALUES ('2447', '7f0000010b5600000001', '2', '90', '0', '0', '1534132323', '1');
INSERT INTO `wsoptlog` VALUES ('2448', '7f0000010b5600000001', '2', '90', '0', '0', '1534132333', '1');
INSERT INTO `wsoptlog` VALUES ('2450', '7f0000010b5600000001', '2', '90', '0', '0', '1534132353', '1');
INSERT INTO `wsoptlog` VALUES ('2454', '7f0000010b5600000001', '2', '90', '0', '0', '1534132393', '1');
INSERT INTO `wsoptlog` VALUES ('2459', '7f0000010b5600000001', '2', '90', '0', '0', '1534132443', '1');
INSERT INTO `wsoptlog` VALUES ('2461', '7f0000010b5600000001', '2', '90', '0', '0', '1534132463', '1');
INSERT INTO `wsoptlog` VALUES ('1962', '7f0000010b5600000001', '2', '90', '0', '0', '1534128073', '1');
INSERT INTO `wsoptlog` VALUES ('1963', '7f0000010b5600000001', '2', '90', '0', '0', '1534128083', '1');
INSERT INTO `wsoptlog` VALUES ('1969', '7f0000010b5600000001', '2', '90', '0', '0', '1534128143', '1');
INSERT INTO `wsoptlog` VALUES ('2163', '7f0000010b5600000001', '2', '90', '0', '0', '1534129543', '1');
INSERT INTO `wsoptlog` VALUES ('2164', '7f0000010b5600000001', '2', '90', '0', '0', '1534129553', '1');
INSERT INTO `wsoptlog` VALUES ('2165', '7f0000010b5600000001', '2', '90', '0', '0', '1534129563', '1');
INSERT INTO `wsoptlog` VALUES ('2166', '7f0000010b5600000001', '2', '90', '0', '0', '1534129573', '1');
INSERT INTO `wsoptlog` VALUES ('2167', '7f0000010b5600000001', '2', '90', '0', '0', '1534129583', '1');
INSERT INTO `wsoptlog` VALUES ('2168', '7f0000010b5600000001', '2', '90', '0', '0', '1534129593', '1');
INSERT INTO `wsoptlog` VALUES ('2169', '7f0000010b5600000001', '2', '90', '0', '0', '1534129603', '1');
INSERT INTO `wsoptlog` VALUES ('2271', '7f0000010b5600000001', '2', '90', '0', '0', '1534130563', '1');
INSERT INTO `wsoptlog` VALUES ('2272', '7f0000010b5600000001', '2', '90', '0', '0', '1534130573', '1');

