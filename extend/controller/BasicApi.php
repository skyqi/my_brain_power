<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2017 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://think.ctolog.com
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zoujingli/ThinkAdmin
// +----------------------------------------------------------------------

namespace controller;

use service\WechatService;
use think\Controller;
use think\Db;
use \think\facade\Cache;


/**
 * 接口基础控制器
 * Class BasicApi
 * @package controller
 */
class BasicApi extends Controller
{

    /**
     * 当前粉丝用户OPENID
     * @var string
     */
    protected $openid;

    /**
     * 获取粉丝用户OPENID
     * @return bool|string
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    protected function getOpenid ()
    {
        return WechatService::webOauth(0)['openid'];
    }

    /**
     * 获取微信粉丝信息
     * @return bool|array
     * @throws \Exception
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    protected function getFansinfo ()
    {
        return WechatService::webOauth(1)['fansinfo'];
    }

    //检查appid
    protected function verfiyAppID ($appid=false)
    {
        $appid = $appid?$appid:$this->request->request('appid', '', 'trim');

        if ($appid != session("AppId")) {
            $this->apiError("appid获取错误");
        }
        return 1;
    }

    /**
     * 登录 token 生成
     * @param $id
     * @param $username
     * @param $appid
     * @return string
     */
    protected function createToken ($id, $username, $appid)
    {

        if ($appid != session("AppId")) {
            $this->apiError("appid获取错误");
        }

        return sysmd5($id . $username . $appid);
    }

    /**
     * token 验证
     * @return int
     */
    protected function verfiyToken ()
    {
        $token = $this->request->request('token', '', 'trim');
        if (!$token) {
            $this->apiError('未定义token参数');
        }
        $token_data = Cache::get($token);
        if (false == $token_data) {
            $this->apiError('token参数错误');
        }
        $appid = $token_data['appid'];

//        if (false==$token_data['id']) {
//            $this->apiError('未正确登录！');
//        }

        if (intval(session("users.id"))>0) {
            $userRes = Db::name('users')->where('id', session("users.id"))->find();
            if ($userRes['status'] == 0) {
                $this->apiError('该用户被禁止登录');
            }
            if ($token != sysmd5($userRes['id'] . $userRes['username'] . $appid)) {
                $this->apiError('Token验证失败');
            }
        } else {
            $this->apiError('用户未登录');
        }
//
//        session("login", array(
//            'user_id'      => $token_data['id'],
//            'username'    => $token_data['username'],
//            'email'        => $token_data['email'],
//            // 'identity'    => $token_data['identity'],
//            // 'identity_id' => $token_data['identity_id'],
//        ));

        return 1;
    }


    /**
     * @param $message
     */
    protected function apiError ($message)
    {
        $this->result_new(null, 0, $message, 'error');
    }

    /**
     * @param null $data
     * @param $message
     */
    protected function apiSuccess ($data = null, $message)
    {
        $this->result_new($data, 1, $message, 'success');
    }

    /**
     * @param $data
     * @param int $code
     * @param string $msg
     * @param string $status
     */
    protected function result_new ($data, $code = 0, $msg = '', $status = 'error')
    {
        $data2 = $data;
        if (false == $data) $data2 = null;
        if (is_array($data) && count($data) == 0) $data2 = null;
        if (empty($data)) $data2 = null;

        $request = $_REQUEST;
        if (false == $_REQUEST) $request = null;
        if (is_array($_REQUEST) && count($_REQUEST) == 0) $request = null;
        if (empty($_REQUEST)) $request = null;

        $result = [
            'time'    => $_SERVER['REQUEST_TIME'],
            'error_code' => 0,
            'msg' => '',
            'data'    => $data2,
            'request' => $request,
        ];
        if ($status == 'error') {
            if(empty($data)){
                $result['error_code'] = 1;
            }else{
                $result['error_code'] = $code ? $code : 0;
            }
            $result['msg'] = $msg ? _t($msg) : _t('失败');
        } else {
            $result['error_code'] = 0;
            $result['msg'] = $msg ? _t($msg) : _t('成功');
        }

        die(json_encode($result, JSON_UNESCAPED_UNICODE));
    }


       /**
     * 列表集成处理方法
     * @param Query $dbQuery 数据库查询对象
     * @param bool $isPage 是启用分页
     * @param bool $isDisplay 是否直接输出显示
     * @param bool $total 总记录数
     * @param array $result 结果集
     * @return array|string
     */
    protected function _list($dbQuery = null, $isPage = true,  $total = false, $result = [])
    {
        $db = is_null($dbQuery) ? Db::name($this->table) : (is_string($dbQuery) ? Db::name($dbQuery) : $dbQuery);

        // 列表数据查询与显示
        $pk = $db->getPk();
        if (null === $db->getOptions('order')) {
            if (in_array('sort', $db->getTableFields())) {
                $db->order("sort asc,{$pk} desc");
            } else {
                $db->order("{$pk} desc ");
            }
        }


        if ($isPage) {
            // 分页数据处理
            $query = $this->request->get('', '', 'urlencode');
            ///
            if ($query['rows']>0) $rows =$query['rows'];
            if ($this->rows) $rows=$this->rows;
            false==$rows && $rows= 20;

            $page = $db->paginate($rows, $total, ['query' => $query]);

            if (($totalNum = $page->total()) > 0) {

                 $maxNum = $page->lastPage();

                for ($i = 1; $i <= $maxNum; $i++) {
                    list($query['rows'], $query['page']) = [$rows, $i];
                }

                $result['list'] = $page->all();
                $result['pages']['totalnum'] = $totalNum;
                $result['pages']['page'] = intval($page->currentPage());
                $result['pages']['totalpage'] = ceil($totalNum / $rows);
                $result['pages']['rows'] =   $rows ;

            } else {
                if ($totalNum>0) {
                    $result['list'] = $page->all();
                    $result['pages']['totalnum'] = $totalNum;
                    $result['pages']['page'] = 1;
                    $result['pages']['rows'] = $rows;
                }
            }

        } else {

            // 不分页
            $result['list'] = $db->select();


        }

        return $result;

    }


}
