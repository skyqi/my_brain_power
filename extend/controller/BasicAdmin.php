<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2017 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://think.ctolog.com
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zoujingli/ThinkAdmin
// +----------------------------------------------------------------------

namespace controller;

use service\DataService;
use think\Config;
use think\Controller;
use think\Db;
use think\db\Query;
use think\Cache;
use think\Exception;
use think\Request;
use org\FieldFromClass;
use org\UploadFile;
use org\CreateFromControl;
use org\RegexCheck;
use org\ContentList;
/**
 * 后台权限基础控制器
 * Class BasicAdmin
 * @package controller
 */
class BasicAdmin extends Controller
{
    /**
     * 页面标题
     * @var string
     */
    public $title, $url;

    /**
     * 默认操作数据表
     * @var string
     */
    public $table, $moduleCache, $moduleid, $request, $module_name, $controller_name, $action_name, $reuqest, $appRoot,$MethodClass;


    public function __construct()
    {

        parent::__construct();
        //$this->initialize();
        $this->request = \request();
//        $this->module_name = $this->request->module();
//        $this->controller_name = strtolower($this->request->controller());
//        $this->action_name = strtolower($this->request->action());
       // $this->moduleid = input('?get.moduleid');
        $this->MethodClass  = "app\admin\controller\Method"  ;

//        $this->assign('controller_name', $this->controller_name);
//        $this->assign('module_name', $this->module_name);
//        $this->assign('action_name', $this->action_name);
       // $this->assign('moduleid', $this->moduleid);
       // $this->assign("url_index",$this->url);

        define('ACTION_NAME', $this->action_name);
        $this->appRoot = $this->request->root(true);
        $this->assign('appRoot', $this->appRoot);


        if (session('user.id') == null && $this->module_name == "admin" && !in_array($this->controller_name, array('login', 'wsoptlog','out','test'))) {
            $this->redirect($this->appRoot . '/admin/login.html');
            exit;
        }

        $this->setRequest($this->request);
       // $this->url = url("/admin/{$this->controller_name}/index", ['spm' => $_REQUEST['spm'], 'runtime' => time()]);  // 放在前面组装

//        $this->table = DB::table("system_module")->cache(true,60)->where("name='{$this->controller_name}'")->value("tb_name");
//
//
//        $this->tbFields = overwrite_setfield($this->table, F($this->table));
//        $this->assign("tbField",$this->tbFields);
//        $this->assign("url_index",$this->url);

    }

    public function setRequest($request) {


        $this->module_name = $request->module();
        $this->controller_name = strtolower($request->controller());
        $this->action_name = strtolower($request->action());

        $this->assign('controller_name', $this->controller_name);
        $this->assign('module_name', $this->module_name);
        $this->assign('action_name', $this->action_name);

        if (false==$this->table) {
            $this->table = DB::table("system_module")->where("name='{$this->controller_name}'")->value("tb_name");
            if (false==$this->table) return false;
        }


        $this->url = url("/admin/{$this->controller_name}/index", ['spm' => $_REQUEST['spm'], 'runtime' => time()]);  // 放在前面组装
        $this->assign("url_index",$this->url);

        $this->tbFields = overwrite_setfield($this->table, F($this->table));

        $this->assign("tbField",$this->tbFields);
      

    }

    /**
     * 控制器基础方法，检验登陆
     */
    public function initialize()
    {
        if (session('user.id') == null && $this->request->action() !== 'out' && $this->module_name == "admin") {
            // $this->error('请登陆后操作！','@admin/login');
            $this->redirect('@admin/login');
        }
        //TODO 权限判断
    }

//    protected function getModuleId($table)
//    {
//        $moduleRes = F('system_module');
//        // var_dump($moduleRes);die;
//        if (false == $moduleRes) {
//            abort("错误：模块缓存为空");
//        }
//        foreach ($moduleRes as $module) {
//            if (strtolower($module['name']) == strtolower($table)) {
//                return $module['id'];
//            }
//        }
//        return 0;
//    }

    /**
     * 表单默认操作
     * @param Query $dbQuery 数据库查询对象
     * @param string $tplFile 显示模板名字
     * @param string $pkField 更新主键规则
     * @param array $where 查询规则
     * @param array $extendData 扩展数据
     * @return array|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     */
    protected function _form($dbQuery = null, $tplFile = '', $pkField = '', $where = [], $extendData = [])
    {
        $db = is_null($dbQuery) ? Db::name($this->table) : (is_string($dbQuery) ? Db::name($dbQuery) : $dbQuery);

        $pk = empty($pkField) ? ($db->getPk() ? $db->getPk() : 'id') : $pkField;
        $pkValue = $this->request->request($pk, isset($where[$pk]) ? $where[$pk] : (isset($extendData[$pk]) ? $extendData[$pk] : null));
        // 非POST请求, 获取数据并显示表单页面
        if (!$this->request->isPost()) {
            $vo = ($pkValue !== null) ? array_merge((array)$db->where($pk, $pkValue)->where($where)->find(), $extendData) : $extendData;
            if (false !== $this->_callback('_form_filter', $vo, [])) {
                empty($this->title) || $this->assign('title', $this->title);
                return $this->fetch($tplFile, ['vo' => $vo]);
            }
            return $vo;
        }
        // POST请求, 数据自动存库
        $data = array_merge($this->request->post(), $extendData?$extendData:[]);
        if (false !== $this->_callback('_form_filter', $data, [])) {
            $result = DataService::save($db, $data, $pk, $where);

            if (false !== $this->_callback('_form_result', $result, $data)) {
                if ($result !== false) {
                    $this->success('恭喜, 数据保存成功!', '');
                }
                $this->error('数据保存失败, 请稍候再试!');
            }
        }
    }

    /**
     * 列表集成处理方法
     * @param Query $dbQuery 数据库查询对象
     * @param bool $isPage 是启用分页
     * @param bool $isDisplay 是否直接输出显示
     * @param bool $total 总记录数
     * @param array $result 结果集
     * @return array|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     */
    protected function _list($dbQuery = null, $isPage = true, $isDisplay = true, $total = false, $result = [])
    {
        $db = is_null($dbQuery) ? Db::name($this->table) : (is_string($dbQuery) ? Db::name($dbQuery) : $dbQuery);
        // 列表排序默认处理
        if ($this->request->isPost() && $this->request->post('action') === 'resort') {

            foreach ($this->request->post() as $key => $value) {
                if (preg_match('/^_\d{1,}$/', $key) && preg_match('/^\d{1,}$/', $value)) {
                    list($where, $update) = [['id' => trim($key, '_')], ['sort' => $value]];
                    if (false === Db::table($db->getTable())->where($where)->update($update)) {
                        $this->error('列表排序失败, 请稍候再试');
                    }
                }
            }

            $tablename = $db->getTable();
            $this->FieldCache("tb_name='$tablename'", 'w');

            //if (false==$this->request->isAjax())
            //   $this->success('列表排序成功, 正在刷新列表', '');
        }

        // 列表数据查询与显示
        $pk = $db->getPk();

        if (null === $db->getOptions('order')) {
            if (in_array('sort', $db->getTableFields($db->getTable()))) {
                $db->order("sort asc,{$pk} desc ");
            } else {
                $db->order("{$pk} desc ");
            }
        } else {
            $db->order("{$pk} desc ");
        }
        $map = [];
        if (false !== $this->_callback('_query_filter', $map)) {
            if (is_array($map) && count($map) > 0) $db->where($map);

        }
        if ($isPage) {
            $rows = intval($this->request->get('rows', cookie('page-rows')));
            cookie('page-rows', $rows = $rows >= 10 ? $rows : 20);
            if ($this->rows) $rows=$this->rows;
            // 分页数据处理
            $this->assign("rows", $rows);
            $query = $this->request->get('', '', 'urlencode');
            $page = $db->paginate($rows, $total, ['query' => $query]);
            if ($query['rows']>0) $rows=$query['rows'];
            if (($totalNum = $page->total()) > 0) {
                list($rowsHTML, $pageHTML, $maxNum) = [[], [], $page->lastPage()];
                foreach ([10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200] as $num) {
                    list($query['rows'], $query['page']) = [$num, '1'];
                    $url = url('@admin') . '#' . $this->request->baseUrl() . '?' . http_build_query($query);
                    $rowsHTML[] = "<option data-url='{$url}' " . ($rows == $num ? 'selected' : '') . " value='{$num}'>{$num}</option>";
                }

                for ($i = 1; $i <= $maxNum; $i++) {
                    list($query['rows'], $query['page']) = [$rows, $i];
                    $url = url('@admin') . '#' . $this->request->baseUrl() . '?' . http_build_query($query);
                    $selected = $i === intval($page->currentPage()) ? 'selected' : '';
                    $pageHTML[] = "<option data-url='{$url}' {$selected} value='{$i}'>{$i}</option>";
                }
                list($pattern, $replacement) = [['|href="(.*?)"|', '|pagination|'], ['data-open="$1"', 'pagination pull-right']];
                $html = "<span class='pagination-trigger nowrap y_fneye'>共 {$totalNum} 条记录，每页显示 <select data-auto-none>" . join('', $rowsHTML) . "</select> 条，共 " . ceil($totalNum / $rows) . " 页当前显示第 <select>" . join('', $pageHTML) . "</select> 页。</span>";
                list($result['total'], $result['list'], $result['page']) = [$totalNum, $page->all(), $html . preg_replace($pattern, $replacement, $page->render())];
            } else {
                list($result['total'], $result['list'], $result['page']) = [$totalNum, $page->all(), $page->render()];
            }
            $this->assign("totalNum",$totalNum);
        } else {
            $result['list'] = $db->select();

        }
        //prt($db->getLastSql());
        if (false !== $this->_callback('_data_filter', $result['list'], []) && $isDisplay) {
            !empty($this->title) && $this->assign('title', $this->title);
            return $this->fetch('', $result);
        }

        $this->MethodClass  = "app\admin\controller\Method"  ;
        $call_method_name = strtolower($this->controller_name);
        $_method = "Method/{$call_method_name}_after_list";
        //回调 事件
        if (method_exists($this->MethodClass, $_method) && false === $this->$_method($result['list'], [])) {
            return false;
        }

        return $result;
    }

    //在method类写回调事件
    protected function MethodAction($method1,&$data1=null,&$data2=null) {
        $MethodClass  = "app\admin\controller\Method"  ;
        $MClass = new $MethodClass();
        $call_method_name = strtolower($this->controller_name);

        if( preg_match("/[a-z]+-[a-z]+/Ui",$call_method_name)) {
            $call_method_name = str_replace("-", "_", $call_method_name);
        }
        $_method2 = "{$call_method_name}_{$method1}_{$this->action_name}";
        //回调 事件
        if (method_exists($MClass, $_method2) && false == $MClass->$_method2($data1, $data2)) {
            return false;
        }
    }

    /**
     * 更新
     *
     */

    function edit()
    {
        $call_method_name = strtolower($this->controller_name);
        // prt($call_method_name);
        // prt($this->table,0);
        if (method_exists($this->MethodClass,"{$call_method_name}_before_edit")) {
            action("Method/{$call_method_name}_before_edit");
        }
        $model = Db::name($this->table);
        $pk = ucfirst($model->getPk());
        $id = intval($_REQUEST[$model->getPk()]);
        // prt($_REQUEST['Schedule_id'],0);
        // prt($_REQUEST['schedule_id']);
        // prt($id);
        if (!empty($id)) {
            $do = 'getBy' . $pk;
            $vo = $model->$do($id);
            if (false == $vo) {
                $this->returnMsg('error', $this->url);
            }
            if ($vo['setup']) $vo['setup'] = string2array($vo['setup']);
            if ($vo['querycontrol']) $vo['querycontrol'] = string2array($vo['querycontrol']);
            // prt($vo);
            $this->assign('vo', $vo);
        }
        $tplName = (app('view')->exists("{$this->controller_name}/edit")) ? "{$this->controller_name}/edit" : "content/edit";
        echo $this->fetch($tplName);
    }

//    //验证输入数据
//    function validateField($moduleid,$data=[])
//    {
//        if (false==$moduleid)  return false;
//        if (intval($moduleid)==0 && !empty($moduleid)) {
//            //找出moduleid
//            $moduleid = $this->getModuleId($this->table);
//            if ($moduleid==0) return false;
//        }
//
//
//        try {
//            $db = DB::name($this->table);
//        } catch (Exception $e) {
//            return false;
//        }
//        $tbFields = $db->getTableFields();
//        $cachefields = $this->FieldCache($moduleid, 'get');
//
//        $data = false==$data?$_POST:$data;
//        foreach ($data as $k => $postv) {
//            // prt($k,0);
//            if (in_array($k, $tbFields)) {
//                foreach ($cachefields as $kk => $vv) {
//                    if ($k == $vv['field']) {
//                        if ($vv['required'] === 1) {
//                            if (empty($postv) && false == $postv) {
//                                //必输
//                                $this->error($vv['errormsg'] ? $vv['errormsg'] : "{$vv['name']}字段必输！");
//                                exit;
//                            }
//                        }
//
//                        if (!empty($postv) && 　false == $vv['pattern']) {
//                            //输入的值必须符合条件
//                            $pattern = $vv['pattern'];
//                            $RegexCheck = lang('RegexCheck');
//                            $RegexArr = array_keys($RegexCheck);
//                            $tmp_Reg = implode(",", $RegexArr);
//                            $tmp_Reg = strtolower($tmp_Reg);
//                            $tmp_Reg = explode(",", $tmp_Reg);
//                            if (in_array(strtolower($pattern . 'Action'), $tmp_Reg)) {
//                                if (false == RegexCheck::$pattern($postv)) $this->error("请在{$vv['name']}字段,输入合法的" . $RegexCheck[$pattern] . "格式！");
//                            }
//                        }
//
//
//                    }
//                }
//            }
//        }
//
//    }

    function update()
    {
        // prt($_REQUEST);
        if ($_POST['setup']) $_POST['setup'] = array2string($_POST['setup']);
        $call_method_name = strtolower($this->controller_name);
      
        //回调更新之前的事件
        if (false !== $this->_callback('before_update', $_POST, [])) {
            //.todo
        }

        if (method_exists($this->MethodClass,"{$call_method_name}_before_update")) {
            action("Method/{$call_method_name}_before_update");
        }
        try {
            validateField($this->table);
        }catch (Exception $e) {
            $this->error($e->getMessage());
        }

        if (isset($_POST['updatetime']) && false!=$_POST['updatetime'] ) {
            $_POST['updatetime'] =  toTimestamp($_POST['updatetime']);
        }

        if (isset($_POST['createtime']) && false!=$_POST['createtime'] ) {
            $_POST['createtime'] =  toTimestamp($_POST['createtime']);
        }

        try {
            // prt($_POST);
            $saveRet = DataService::save($this->table, $_POST);
            //prt(Db::table($this->table)->getLastSql());
        } catch (\Exception $e) {
            $this->returnMsg('error', $this->url, '执行错误' . $e->getMessage());
        }


        //在存盘之后处理
        if (false !== $this->_callback('after_update', $saveRet, [])) {
            //.todo

        }

        //在存盘之后处理
        if (method_exists($this->MethodClass,"{$call_method_name}_after_update")) {
            action("Method/{$call_method_name}_after_update",$saveRet);
        }
        $forward = $_POST['forward']?$_POST['forward']:$this->url;
        $this->returnMsg('success',$forward);

    }


    /**
     * 当前对象回调成员方法
     * @param string $method
     * @param array|bool $data1
     * @param array|bool $data2
     * @return bool
     */
    protected function _callback($method, &$data1 = null, $data2 = null)
    {
        foreach ([$method, "_" . $this->request->action() . "{$method}"] as $_method) {
            if (method_exists($this, $_method) && false === $this->$_method($data1, $data2)) {
                return false;
            }
        }
        return true;
    }


    /*
     *  1,写入缓存cacheAttachment($table)
     *  2,返回缓存cacheAttachment($table,1)
     */
    protected function cacheAttachment($table, $moduleid = 0)
    {

        if (false == $table) return false;
        $filename = $table . "_attachmentCache";

        if ($moduleid == 0) {
            $result = Db::table("system_attachment")->where(["table" => $table, "status" => 1])->order("sort asc")->select();
            if (false == $result) {
                @unlink(RUNTIME_PATH_DATA . $filename);
                abort($table . '表缓存是空的，无法缓存数据');
            }
            //写入缓存图片数据库
            foreach ($result as $k => $v) {
                $table_arr[$v['moduleid']] = $v;
            }
            F($filename, $table_arr);
            return false;
        } else {
            //返回
            $table_arr = F($filename);
            foreach ($table_arr as $k => $v) {
                if ($v['moduleid'] == $moduleid) {
                    return $v;
                }
            }
        }

    }


//    protected function getcache($table, $field = "*")
//    {
//        $data = F($table);
//        if (false == $data) {
//            savecache($table, $field);
//        }
//        //二次取
//        $data = F($table);
//        if (false == $data) {
//            abort($table . '表缓存是无效的');
//        }
//        return $data;
//    }

    protected function FieldCache($wherestr , $do = 'get')
    {

        $table = DB::table("system_module")->where($wherestr)->value('tb_name');
        if (false==$table) {
            //exception("line:".__LINE__.",sql:".DB::table("system_module")->getLastSql()."语法错误");
            return;
        }
        if ($do == 'get') {
            //返回

            F($table);
        }

        if ($do == 'w') {
            //写入
            $res = DB::table("system_field")->where($wherestr)->order("sort asc")->select();
            $newRes = [];
            foreach ($res as $v) {
                $key = $v['field'];
                $newRes[$key] = $v;
            }
            if ($newRes) {
               // F($table . "_Field.php", $newRes);
                F($table,$newRes);

            }

        }

        //删除
        if ($do == 'del') {
            DB::table("system_module")->where($wherestr)->delete();
            cache('system_field_cache', NULL); //清除
        }

    }



    public function _empty() {
        $request = app('request');
        list($module, $controller, $action) = [$request->module(), $request->controller(), $request->action()];
        $controller = strtolower($controller);
        $class  = "app\admin\controller\\" .ucfirst(strtolower($controller));
//        prt($class,0);
//        prt(method_exists($class,'add'),0);
//        prt(method_exists($class,'edit'),0);
        if ($action=="add") {
            if (class_exists($class) && method_exists($class, 'edit')) {
                action("{$controller}/edit");    exit;
            }else{
                action("content/edit");    exit;
            }
        } else {
            //其它事件，未定义

        }


    }


    //标签使用
    public function getTagsList() {

        $markList = DB::name('system_tags')->where(" user_status=1")->select();

        if (false==$markList) $markList='';
        if ($_GET['is_ajax']) {
            if (false == $markList) die('');
            die(json_encode($markList));
        }else {
            return $markList;
        }
    }

    protected function returnMsg($status='success',$url='',$msg) {
        //   $url = url('/admin/field/index',['moduleid'=>$moduleid,'spm'=>$_REQUEST['spm']]);  放在前面组装
        if (false==$url)   { $url = url("/admin/{$this->controller_name}/index",['moduleid'=>$_REQUEST['moduleid'],'spm'=>$_REQUEST['spm']]);   }
        list($base,$url) = [('/admin'),$url];
        if ($status=='success' && false==$msg)  $msg='操作成功！';
        if ($status!='success' && false==$msg)  $msg='操作失败！';

        if ($status=='success') {
            $this->success($msg, "{$base}#{$url}");
        } else {
            $this->error($msg, "{$base}#{$url}");
        }

    }


    public function upfile() {

        $config=array(
            'allowExts'=>array('jpg','gif','png'),
            //   'savePath'=>'./Uploads',
            'saveRule'=>'time',
        );
        $moduleid = intval($_REQUEST['moduleid']);
        $table = trim($_REQUEST['table']);

        $upload = new UploadFile($config);
        $upload->thumb=true;
        $upload->thumbMaxHeight=100;
        $upload->thumbMaxWidth=100;
        if (!$upload->upload()) {

            $this->error($upload->getErrorMsg());
        } else {
            $info = $upload->getUploadFileInfo();
            !is_array($info) && $info = (array)$info;
            $attachment = DB::table('system_attachment');
            $data = array();
            $newIDs = array(0);
            foreach ($info as $k=>$v) {
                $data  =['key'=>$v['key']
                    ,'filename'=>$v['savename']
                    ,'filepath'=>$v['savepath']
                    ,'filesize'=>$v['size']
                    ,'fileext'=>$v['extension']
                    ,'isimage'=>strstr($v['type'],'image')?1:0
                    ,'createtime'=>time()
                    ,'uploadip'=>getIP()
                    ,'status'=>1
                    ,'moduleid'=>$moduleid
                    ,'table'=>$table
                ];
                $res = $attachment->insert($data);
                $res>0 &&  $newIDs[] = $attachment->getLastInsID();
            }

            $this->cacheAttachment($table);
            //$this->assign('filename', $info[0]['savename']);

            $data = getAttachment($newIDs);
            $data && $data = json_encode($data);

            $this->success("成功",'',$data);
        }
    }

    /**
     * 批量操作
     *
     */
    public function listorder()
    {

        $controller_name = $_POST['controller_name'];
        if (false==$controller_name) {
            $this->error("controller_name取参错误,Line:".__LINE__);
        }
        $model = DB::table($controller_name);
        $pk = $model->getPk();

        $ids = $_POST['listorders'];
        foreach ($_POST['sort'] as $id=>$sort) {
            if (intval($id)>0 && !empty($sort)) {
                try {
                    $res = $model->strict(false)->where([$pk => $id])->update(['sort' => $sort]);
                }catch (exception $e) {
                    $this->error("更新失败,请重试!error:".$e->getMessage());
                }

            }
        }

        $this->success (L('do_ok'));
    }



    //在列表页面，用于全选删除
    public function deleteRec()
    {
        $controller_name = $_REQUEST['controller_name'];
        if (false==$controller_name) {
            $this->error("controller_name取参错误,Line:".__LINE__);
        }

        $tb_name = DB::table("system_module")->where("name='{$controller_name}'")->value("tb_name");

        if (false==$_REQUEST["id"]) {
            $this->error("pk取参错误,Line:".__LINE__);
        }
        $ids = explode(",",$_REQUEST["id"]);

        foreach ($ids as $k=>$id) {
            if (intval($id)>0) {
                try {
                    DB::table($tb_name)->delete($id);
                }catch (Exception $e) {
                    $this->error("更新失败,请重试!error:".$e->getMessage());
                }
            }
        }
        $this->success(L('do_ok'));
    }

    //地区生成表
    public function distpickerCreate(){
        if (false==($distpicker_json_arr=F("distpicker_json"))) {
            $city2 = file_get_contents(ROOT_DIR . "/static/plugs/Province_js/cityJson");
            //  $city2 = json_encode($city);
            $city = json_decode($city2, true);
            foreach ($city as $k => $v) {

                $addr[$v[0]] = $v[1];

                foreach ($v[2] as $k2 => $v2) {

                    $addr[$v2[0]] = $v2[1];

                    foreach ($v2[2] as $k3 => $v3) {

                        $addr[$v3[0]] = $v3[1];
                    }
                }

            }

            F("distpicker_json", $addr);
        }
    }

    //http://ivehice2.mydanweb.com/admin#/admin/campus/export.html?spm=m-45-40-58
//    public function export() {
//
//        require  './extend/PHPExcel/Autoloader.php';
//        return ;
//
//        $arr =parse_url($_SERVER['HTTP_REFERER']);
//        //    prt( urldecode($arr['query']));
//        parse_str(urldecode($arr['query']),$query);
//
//        clearstatcache();
//        $xlsCell = array();
//        foreach ($this->fieldList as $k=>$v) {
//
//            if ($v['listShowField']==1) {
//                $xlsCell[] = array($v['field'], $v['name']);
//                $tmp_cell[] = $v['field'];
//            }
//        }
//        unset($k,$v);
//        $map = array();
//        foreach ($query['querycontrol'] as $k=>$v) {
//            if ($v['value']) {
//                if ($v['formula0']=='like') {
//                    $map[$k] = array($v['formula'], "%".$v['value']."%");
//                } else
//                    $map[$k] = array($v['formula'], $v['value']);
//            }
//        }
//
//        //  prt($map);
//        $tbList = $this->_list(MODULE_NAME,$map,'','',9999);
//
//
//        $module = D('module')->where('name="'.MODULE_NAME.'"')->select();
//        $xlsName = isset($module[0]['title']) ? $module[0]['title'] : '';
//        //组织数据格式
//        set_time_limit(0);      //执行时间无限
//        ini_set('memory_limit', '-1');
//        $this->dataExcel($xlsName, $xlsCell, $tbList['list']);
//    }


    public function detail()
    {
        $id = intval($_REQUEST['id']);
        if (false==$id) return false;
        $vo = DB::name($this->table)->find($id);
        if (false !== $this->_callback('before_detail', $vo, [])) {
            //.todo
        }
        $this->assign("vo",$vo);
        $this->assign("ContentList",new ContentList());
        $tplName = (app('view')->exists("{$this->controller_name}/detail")) ? "{$this->controller_name}/detail" : "content/detail";
        die($this->fetch($tplName));
    }


    /**
     * 导出数据为excal文件
     *
     * @param unknown $expTitle
     * @param unknown $expCellName
     * @param unknown $expTableData
     */
    public function export()
    {
        $arr =parse_url($_SERVER['HTTP_REFERER']);
        parse_str(urldecode($arr['query']),$query);
        clearstatcache();
        $xlsCell = array();
        //$table
        $table = $this->table?$this->table:$this->controller_name;
        $table = str_replace("2","-",$table);


        $moduleId = $this->getModuleId($table);
        $fields = F($moduleId."_Field");

        foreach ($fields as $k=>$v) {
            if ($v['status']>0) {
                $xlsCell[] = array($v['field'], $v['name']);
                $tmp_cell[] = $v['field'];
            }
        }


        $db = DB::table($table)->field($tmp_cell) ;

        $this->rows = 9999;
        $tbList = $this->_list( $db ,true,false);
        $exprot_data = $tbList['list'];
        //TODO 数据转换

        unset($k,$v);

        foreach ($exprot_data as $kk => $data) {
            foreach ($data as $field =>$value) {
                foreach ($fields as $k2 => $f) {
                    if ($f['field'] == $field && $f['status']>0) {
                        $exprot_data[$kk][$field] = ContentList::index($value, $f);
                    }
                }
            }
        }

        $module = Db::name('system_module')->where('name="'.$table.'"')->select();
        $xlsName = isset($module[0]['title']) ? $module[0]['title'] : '';
        //组织数据格式
        set_time_limit(0);      //执行时间无限
        ini_set('memory_limit', '-1');

        dataExcel($xlsName, $xlsCell, $exprot_data);

    }

    /**
     * 导出数据为excal文件
     *
     * @param unknown $expTitle
     * @param unknown $expCellName
     * @param unknown $expTableData
     */
    public function dataExcel_del($expTitle, $expCellName, $expTableData)
    {
        // import ( "@.ORG.PHPExcel" );
        Loader::import('extend.PHPExcel');
        $xlsTitle = iconv('utf-8', 'gb2312', $expTitle); // 文件名称
        $fileName = $expTitle . date('_YmdHis'); // or $xlsTitle 文件名称可根据自己情况设定
        $cellNum = count($expCellName);
        $dataNum = count($expTableData);
        $objPHPExcel = new \PHPExcel();
        $cellName = array(
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
            'V',
            'W',
            'X',
            'Y',
            'Z',
            'AA',
            'AB',
            'AC',
            'AD',
            'AE',
            'AF',
            'AG',
            'AH',
            'AI',
            'AJ',
            'AK',
            'AL',
            'AM',
            'AN',
            'AO',
            'AP',
            'AQ',
            'AR',
            'AS',
            'AT',
            'AU',
            'AV',
            'AW',
            'AX',
            'AY',
            'AZ'
        );
        for ($i = 0; $i < $cellNum; $i ++) {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[$i] . '2', $expCellName[$i][1]);
        }
        for ($i = 0; $i < $dataNum; $i ++) {
            for ($j = 0; $j < $cellNum; $j ++) {
                $objPHPExcel->getActiveSheet(0)->setCellValue($cellName[$j] . ($i + 3), $expTableData[$i][$expCellName[$j][0]]);
            }
        }
        $objPHPExcel->setActiveSheetIndex(0);
        header('pragma:public');
        header('Content-type:application/vnd.ms-excel;charset=utf-8;name="' . $xlsTitle . '.xls"');
        header("Content-Disposition:attachment;filename=$fileName.xls"); // attachment新窗口打印inline本窗口打印
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }


    /**
     * @param string $modulename 数据模型
     * @param string $id 记录id
     * @param string $field 记录字段
     * @param int $is_ajax 是否ajax请求
     * @param int $is_like 是否模糊匹配
     * @return bool|int|mixed|string
     */
    public function getModule($modulename='',$id='',$field='',$is_ajax=0, $is_like=0) {
        // prt($_REQUEST,0);
        if ($_REQUEST['modulename']) {
            $modulename = $_REQUEST['modulename'];
        }
        if ($_REQUEST['id']) {
            $id = $_REQUEST['id'];
        }
        if ($_REQUEST['field']) {
            $field = $_REQUEST['field'];
        }
        if ($_REQUEST['is_ajax']) {
            $is_ajax = 1;
        }
        if ($_REQUEST['is_like']) {
            $is_like = 1;
        }

        if ($_REQUEST['like_str']) {
            $like_str = $_REQUEST['like_str'];
        }

        $data = F($modulename);
        if (false==$data) return false;
        if (intval($id)==0) {
            if ($is_ajax) {
                if($field){
                    if($is_like==1 && $like_str!=false){
                        // prt($data);

                        foreach ($data as $k=>$v){
                            if(isset($v[$field]) && strstr( $v[$field] , $like_str ) !== false){
                                $field_data[$k]['id'] = $v['id'];
                                $field_data[$k][$field] = $v[$field];
                            }
                        }
                    }else{
                        foreach ($data as $k=>$v){
                            $field_data[$k]['id'] = $v['id'];
                            $field_data[$k][$field] = isset($v[$field]) ? $v[$field] : '';
                        }
                    }
                    die(json_encode($field_data));
                }else{
                    die(json_encode($data));
                }
            }else {
                return $data;
            }
        }else {
            foreach ($data as $k=>$v) {
                if ($k==$id) {
                    if ($is_ajax) {
                        die(json_encode($v));
                    }else {
                        return $v;
                    }
                }
            }
        }
    }

    /**
     *  列表排序默认处理22
     */
    public function resort_list(){
        if ($this->request->isPost() && $this->request->post('action') === 'resort_list') {
            // prt($_REQUEST);
            foreach ($this->request->post() as $key => $value) {
                if (preg_match('/^_\d{1,}$/', $key) && preg_match('/^\d{1,}$/', $value)) {
                    list($where, $update) = [['id' => trim($key, '_')], ['sort' => $value]];
                    if (false === Db::table('system_field')->where($where)->update($update)) {
                        $this->error('列表排序失败, 请稍候再试');
                    }
                }
            }
            $moduleid = $this->request->post('moduleid');
            $this->FieldCache($moduleid,'w');
            $this->success('排序成功','');
        }
    }

    //图片上传
    public function uploadAlbum ()
    {
        $album_upfile = 'Uploads';
        $attachment = DB::table('system_attachment');
        if ($this->request->isPost()) {
            $res['code'] = 1;
            $res['msg'] = '上传成功！';
            $file = $this->request->file('file');
            $info = $file->move($album_upfile);
            $file_path = preg_replace('/\\/[a-zA-Z0-9]{1,}[.][a-zA-Z]+/', '/',$info->getPathName());
            $data = array(
                'filename_old'   => $info->getInfo()['name'], //文件原名
                'filename'   => $info->getSaveName(), //文件原名
                'filepath'   => $file_path, //上传文件路径
                'filesize'   => $info->getInfo()['size'], //w文件大小
                'fileext'    => $info->getExtension(),
                'isimage'    => strstr($info->getInfo()['type'], 'image') ? 1 : 0,
                'createtime' => time(),
                'uploadip'   => getIP(),
                'status'     => 1,
                'table'      => 'Album',
                'moduleid'   => 0,

            );

            $result = $attachment->insert($data);

            //生成各个尺寸的图
            $imgpath = $info->getPathName();

            $image_size = array(
                array(
                    'name'=>'s',
                    'Height' => 60,
                    'Width' => 60
                ),
                array(
                    'name'=>'m',
                    'Height' => 120,
                    'Width' => 120
                ),
                array(
                    'name'=>'b',
                    'Height' => 640,
                    'Width' => 640
                ),
            );
            foreach ($image_size as $k=>$v){
                $image = \think\Image::open($imgpath);
                $thumb_path = str_replace('.'.$info->getExtension(),'_'.$v['name'].'.'.$info->getExtension(),$info->getPathName());
                $image->thumb($v['Height'], $v['Width'])->save($thumb_path);
            }

            if ($info && $result) {
                $res['name'] = $info->getFilename();
                $res['filepath'] = $album_upfile . $info->getSaveName();
            } else {
                $res['code'] = 0;
                $res['msg'] = '上传失败！' . $file->getError();
            }

            return $res;
        }
    }
}


