<?php

namespace org;
use service\DataService;
use think\Controller;



class Component extends Controller
{
   // public $province_select_tmpl,$province_select_val,$city_select_val;
    public function province($province_select_tmpl=false,$province_select_val=false,$city_select_val=false,$d_select_val=false){
        $this->assign("tpl_type","basic");
        if (!empty($province_select_tmpl) ) {
            $this->assign("tpl_type",$province_select_tmpl);
        }
 
        $province_select_val && $this->assign('province_select_val',$province_select_val);
        $city_select_val && $this->assign('city_select_val', $city_select_val);
		$d_select_val && $this->assign('d_select_val', $d_select_val);

        return $this->fetch( 'widget/component_province_city');

    }

}

