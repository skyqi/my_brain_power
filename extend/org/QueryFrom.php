<?php

namespace org;
use service\DataService;
use think\Controller;
use think\Db;
use think\db\Query;
use org\TreeClass;
use org\CreateFromControl;
use org\Component;
use service\ToolsService;
use service\NodeService;
use think\Cache;


class QueryFrom extends Controller {
    protected $field,$table,$map;
    public function __construct() {
        parent::__construct();

    }


    public function index($field,$map) {
        $this->fields = $field;
        $this->map = $map;
        $this->assign('map',$map);
        if(method_exists(__CLASS__,$field['type']."_action")){
            $action = $field['type']."_action";
            $field['setup']=is_array($field['setup']) ? $field['setup'] : string2array($field['setup']);
            $this->assign('info',$field);
//            prt($field,0);
           //prt($map,0);
            $attr = array('name'=>"query[{$field['field']}][symbol]",'class'=>"query_{$field['field']}_symbol");
            if(in_array($field['field'],array_keys($map))) $attr['select_val'] =  $map[$field['field']]['symbol'];
            $this->query_symbol($attr);
            return $this->$action($field);
        }else {
            echo "{$field['type']}不能做为查询的类型，请在模型设置内关闭";
        }

    }

    public function model_select_action($info) {
        $info['setup']=is_array($info['setup']) ? $info['setup'] : string2array($info['setup']);
        if (false==$info['setup']['bindModule']) {
            return 'model_select的值是空的';
        }

        $bindModules = explode("||", $info['setup']['bindModule']);

        if (empty($bindModules[0]) || empty($bindModules[1]) ) {
            return 'model_select的值是不符合要求';
        }

        try{
            $Mdl = DB::name($bindModules[0]);


            $fieldList =  $Mdl->getTableFields();
            $map=[];
            if (in_array('status',$fieldList)) {
                $map[] = ['status','=',1];
            };
            $pk = $Mdl->getPk();
            // prt($pk, 0);
            $order = "$pk asc";
            if (in_array('sort',$fieldList)){
                $order = 'sort asc';
            };

            if($Mdl->getTable()=='campus') {
                if (session('user.campus_id')>0) {
                    if (in_array("campus",$fieldList)){
                        $map[] = ['id', '=', session('user.campus_id')];
                    }
                }
            } elseif(session('user.campus_id')>0) {

                if (in_array("campus_id",$fieldList)){
                    $map[] = ['campus_id', '=', session('user.campus_id')];
                }

            }



            $moduleResList = $Mdl->where($map)->order($order)->column([$pk,$bindModules[1]]);

            $select_name = "query[{$info['field']}][title]";
            $parseStr = CreateFromControl::select($moduleResList, array('name' => $select_name, 'option_null' => ''));
            return $parseStr;
        }catch (Exception $e) {
          //  abort("未找到数据表{$bindModules[0]},错误：".$e->getMessage());
        }
    }

    public function query_symbol($attr=array()) {
        $query_symbol= [
            'eq'=> '等于',
            'neq'=> '不等于',
            'egt'=> '大等于',
            'elt'=>'小等于',
            'gt'=>'大于',
            'lt'=> '小于',
            'Like'=> '类似',
            'IN'=>'包含',
        ];

        $this->assign("select_html",$this->select($query_symbol,$attr));
    }

    /**
     * @return  regional_Linkage
     */
    public function regional_Linkage_action($info)
    {
       // prt( $this->map,0);
        if ($info['field']=='province') {
            echo Component::province(false );
        }

    }

    private function select($arr_list,$attr,$debug){
        // prt($arr_list,0);
        // prt($attr,0);
        if(!is_array($arr_list)) return "";
        $str_return="";
        $multiple = '';
        $size = '';
        $id = '';
        $name = '';
        $class = '';
        $select_val = '';
        $disabled = '';
        if ($attr['multiple']) $multiple = ' multiple="multiple" ';
        if ($attr['size']) $size = " size='".$attr['size']."'";
        if ($attr['name']) $name = " name='".$attr['name']."'";
        if ($attr['id']) $id = " id='".$attr['id']."'";
        if ($attr['class']) $class = " class='".$attr['class']."'";
        if (!empty($attr['select_val'])) $select_val = $attr['select_val'];

        if ($attr['option_null'])  {
            $str_return ="<option value=' '>请选择</option>";
        }
// prt($arr_list);
        foreach($arr_list as $k=>$item){
            $str_sel="";
            if(false!=$select_val && $k==$select_val) $str_sel=" selected ";
            $str_return.="<option value='".$k."' ".$str_sel.">".$item."</option>";
        }
        $_select = "<select {$multiple} {$size} {$name} {$id} {$class} {$disabled}>";
        return $_select.$str_return."</select>";
    }

    //title_action
    public function title_action($info) {
        $this->assign('block','title');
        echo  $this->fetch("content/query_block");

    }

    //datetime_action
    public function datetime_action($info) {
        $this->assign('block','datetime');
        echo  $this->fetch("content/query_block");

    }

    //number_action
    public function number_action($info) {
        $this->assign('block','number');
        echo  $this->fetch("content/query_block");
    }
    //radio_action
    public function radio_action($info) {

        $this->assign('options', fieldoption($info['setup']['options']));
        $this->assign('block','radio');
        echo  $this->fetch("content/query_block");
    }

    public function text_action($info) {

        $this->assign('info',$info);
        $this->assign('block','text');
        echo  $this->fetch("content/query_block");
    }
    /**
     * @return App
     */
    public function typeid_action($info)
    {
        $info['setup']=is_array($info['setup']) ? $info['setup'] : string2array($info['setup']);

        $types = F("system_types".$info['setup']['default']);

        $this->_form_filter2('system_types',$info['setup']['default']);
        $this->assign('parentid',$info['setup']['default']);
        $this->assign('block','typeid');
        echo  $this->fetch("content/query_block");

    }


    public function _form_filter2($table,$parentid)
    {

        $_menu  = [['name' => '请选择', 'id' => '', 'parentid' => '-1']];
       // prt($parentid);
        // 上级菜单处理
        if ($parentid != 0) {
            $where = "status=1 and (parentid={$parentid})";
            $menus2 = Db::name($table)->where($where)->field("id,name")->order('sort asc,id asc')->select();
        } else {
            $menus2 = Db::name($table)->where("status=1")->field("id,name")->order('sort asc,id asc')->select();
        }
        $menus = array_merge([['name' => '请选择', 'id' => '', 'parentid' => '-1']],$menus2);

 


        $this->assign('menus',$menus);

    }



    /**
     * @return App
     */
    public function select_action($value)
    {
        $types = F("system_types");

        if (false== $types[$value]) {
            return "error:typeid not found {$value}";
        }
        return  $types[$value]['typename'];
    }


//    protected function _form_del2($dbQuery = null, $tplFile = '', $pkField = '', $where = [], $extendData = [])
//    {
//        $db = is_null($dbQuery) ? Db::name($this->table) : (is_string($dbQuery) ? Db::name($dbQuery) : $dbQuery);
//
//        $pk = empty($pkField) ? ($db->getPk() ? $db->getPk() : 'id') : $pkField;
//        $pkValue = $this->request->request($pk, isset($where[$pk]) ? $where[$pk] : (isset($extendData[$pk]) ? $extendData[$pk] : null));
//        // 非POST请求, 获取数据并显示表单页面
//        if (!$this->request->isPost()) {
//            $vo = ($pkValue !== null) ? array_merge((array)$db->where($pk, $pkValue)->where($where)->find(), $extendData) : $extendData;
//            if (false !== $this->_callback('_form_filter', $vo, [])) {
//                empty($this->title) || $this->assign('title', $this->title);
//                return $this->fetch($tplFile, ['vo' => $vo]);
//            }
//            return $vo;
//        }
//        // POST请求, 数据自动存库
//        $data = array_merge($this->request->post(), $extendData);
//        if (false !== $this->_callback('_form_filter', $data, [])) {
//            $result = DataService::save($db, $data, $pk, $where);
//            if (false !== $this->_callback('_form_result', $result, $data)) {
//                if ($result !== false) {
//                    $this->success('恭喜, 数据保存成功!', '');
//                }
//                $this->error('数据保存失败, 请稍候再试!');
//            }
//        }
//    }

    protected function _callback($method, &$data1, $data2)
    {
        foreach ([$method, "_" . $this->request->action() . "{$method}"] as $_method) {
            if (method_exists($this, $_method) && false === $this->$_method($data1, $data2)) {
                return false;
            }
        }
        return true;
    }

}
