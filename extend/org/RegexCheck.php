<?php
namespace org;
/**
 * @description: 正则表达式匹配
 */
class RegexCheck  {
    /**
     * @手机号
     */
    public static function mobileAction($subject) {
        $pattern='/^(0|86|17951)?(13|14|15|17|18)[0-9]{9}$/';
        return RegexCheck::PublicMethod($pattern, $subject);
    }


    //字母数字下划线
    public static function englishAction($subject) {
        $pattern='/^[a-zA-Z]+$/';
        return RegexCheck::PublicMethod($pattern, $subject);
    }


    //字母开头加数字加下划线的字符串
    public static function en_numAction($subject) {
        $pattern='^[A-Za-z]{1}([A-Za-z0-9]|[_])+$';
        return RegexCheck::PublicMethod($pattern, $subject);
    }

    //UTF-8汉字字母数字下划线正则表达式
    public static function cn_usernameAction($subject) {
        $pattern='/^[\x7f-\xffa-zA-Z0-9_]+$/';
        return RegexCheck::PublicMethod($pattern, $subject);
    }

    //UTF-8汉字字母数字下划线正则表达式
    public static function chineseAction($subject) {
        $pattern='/^[\x7f-\xff]+$/';
        return RegexCheck::PublicMethod($pattern, $subject);
    }


    /**
     * @数字
     */
    public static function digitsAction($subject) {
        $pattern='/^[0-9]+$/';
        return RegexCheck::PublicMethod($pattern, $subject);
    }

    /**
     * @数字
     */
    public static function numberAction($subject) {
        $pattern='/^[0-9.]+$/';
        return RegexCheck::PublicMethod($pattern, $subject);
    }
    /**
     * @年份 格式：yyyy
     */
    public static function Year($subject) {
        $pattern='/^(\d{4})$/';
        return RegexCheck::PublicMethod($pattern, $subject);
    }
    /**
     * @月份 格式:mm
     */
    public static function Month($subject) {
        $pattern='/^0?([1-9])$|^(1[0-2])$/';
        return RegexCheck::PublicMethod($pattern, $subject);
    }
    /**
     * @日期 格式：yyyy-mm-dd
     */
    public static function Day($subject) {
        $pattern='/^(\d{4})-(0?\d{1}|1[0-2])-(0?\d{1}|[12]\d{1}|3[01])$/';
        return RegexCheck::PublicMethod($pattern, $subject);
    }
    /**
     * @日期时间 格式：yyyy-mm-dd hh:ii:ss
     */
    public static function dateAction($subject) {
        $pattern='/^(\d{4})-(0?\d{1}|1[0-2])-(0?\d{1}|[12]\d{1}|3[01])\s(0\d{1}|1\d{1}|2[0-3]):[0-5]\d{1}:([0-5]\d{1})$/';
        return RegexCheck::PublicMethod($pattern, $subject);
    }
    /**
     * @邮箱
     */
    public static function email($subject) {
        $pattern='/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/';
        return RegexCheck::PublicMethod($pattern, $subject);
    }
    /**
     * @邮编
     */
    public static function Postcode($subject) {
        $pattern='/[1-9]\d{5}(?!\d)/';
        return RegexCheck::PublicMethod($pattern, $subject);
    }
    /**
     * @有效图片地址
     */
    public static function Photo($subject) {
        $pattern='/\b(([\w-]+:\/\/?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|\/)))/';
        return RegexCheck::PublicMethod($pattern, $subject);
    }
    /**
     * @URL地址
     */
    public static function urlAction($subject) {
        $pattern='/\b(([\w-]+:\/\/?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|\/)))/';
        return RegexCheck::PublicMethod($pattern, $subject);
    }
    /**
     * @有效HTTP地址
     */
    public static function EffectiveHttp($subject) {
        $pattern='/\b(([\w-]+:\/\/?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|\/)))/';
        return RegexCheck::PublicMethod($pattern, $subject);
    }
    /**
     * @身份证
     */
    public static function creditcard($subject) {
        $pattern='/(^\d{15}$)|(^\d{17}([0-9]|X)$)/';
        return RegexCheck::PublicMethod($pattern, $subject);
    }
    /**
     * @IPv4
     */
    public static function Ipv4($subject) {
        $pattern='/^(((\d{1,2})|(1\d{2})|(2[0-4]\d)|(25[0-5]))\.){3}((\d{1,2})|(1\d{2})|(2[0-4]\d)|(25[0-5]))$/';
        return RegexCheck::PublicMethod($pattern, $subject);
    }
    /**
     * @IPv6
     */
    public static function Ipv6($subject) {
        $pattern='/^([\da-fA-F]{1,4}:){7}[\da-fA-F]{1,4}$/';
        return RegexCheck::PublicMethod($pattern, $subject);
    }
    /**
     * @匹配正则公共方法
     */
    public static function PublicMethod($pattern, $subject){
        if(preg_match($pattern, $subject)){
            return true;
        }
        return false;
    }
}