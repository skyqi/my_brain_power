<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2017 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://think.ctolog.com
// +----------------------------------------------------------------------
// | 开源协议 ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zoujingli/ThinkAdmin
// +----------------------------------------------------------------------

namespace org;
use service\DataService;
use think\Controller;
use think\Db;
use think\db\Query;
use org\TreeClass;
use org\Component;
use org\CreateFromControl;
use think\Exception;
use think\Cookie;

class FieldFromClass extends Controller
{
    public $data = array() , $appRoot ;

    protected $field;
    public function __construct() {
        parent::__construct();

    }

    public function index($field,$value) {

        $action = $field['type'] ;

        if(method_exists(__CLASS__,$field['type'] )){

            return FieldFromClass::$action($field,$value);
        }else {
            return $action.",未定义";
            // return $value;
        }
    }

    public function model_select($info,$value){

        $info['setup']=is_array($info['setup']) ? $info['setup'] : string2array($info['setup']);
        if (false==$info['setup']['bindModule']) {
            return 'model_select的值是空的';
        }
        $field = $info['field'];
        $value = $value[$info['field']];

        $bindModules = explode("||", $info['setup']['bindModule']);

        if (empty($bindModules[0]) || empty($bindModules[1]) ) {
            return 'model_select的值是不符合要求';
        }

        try{
            $Mdl = DB::name($bindModules[0]);
            $fieldList =  $Mdl->getTableFields();
            $map=[];
            if (in_array('status',$fieldList)) {
                $map['status'] = 1;
            };
            //权限
            if (intval(session('user.campus_id'))>0 && $bindModules[0]=='campus' ) {
                  $map['id'] = intval(session('user.campus_id'));
            };
            $pk = $Mdl->getPk();
            $order = "$pk asc";
            if (in_array('listorder',$fieldList)){
                $order = 'sort asc';
            };
            $moduleResList = $Mdl->where($map)->order($order)->column([$pk,$bindModules[1]]);

        }catch (Exception $e) {
            abort("未找到数据表{$bindModules[0]},错误：".$e->getMessage());
        }
        $select_name = $info['field'];
        $validate = [];
        if ($info['required']==1) $validate[]="required:true";
        if ($info['minlength']>0) $validate[]="minlength:{$info['minlength']}";
        if ($info['maxlength']>0) $validate[]="maxlength:{$info['maxlength']}";
        if (!empty($info['pattern'])) $validate[]="{$info['pattern']}:true";

        $attr = array('name' => $select_name, 'option_null' =>'', 'select_val'=>$value);
        if (count($validate)>0) {
            $attr = array_merge($attr,$validate);
        }

        if (intval(session('user.campus_id'))>0 && $bindModules[0]=='campus' ) {
             unset($attr['option_null']);
        };

        $parseStr = CreateFromControl::select($moduleResList,$attr);
        return $parseStr;
    }

    //
    public function typeid($info,$value){

        $types=F('system_types');

        $info['setup']=is_array($info['setup']) ? $info['setup'] : string2array($info['setup']);
        $id = $field = $info['field'];
        $value = $value[$info['field']];

        $value = $value ? $value : $this->data[$field];
        $parentid=$info['setup']['default']?$info['setup']['default']:0;
        $keyid = $types[$parentid]['keyid'];

        $options=array();
        $options[0]= L('please_chose');
        foreach((array)$types as $key => $r) {
            if($r['keyid']!=$keyid) continue;
            $r['id']=$r['typeid'];
            $array[] = $r;
            $options[$key]=$r['typename'];
        }
        $validate = getvalidate($info);

        if ($parentid==0) {
            // $typesRes = DB::name('system_types')->where("status=1")->field("id,name")->order("sort asc,id asc")->select();
            return '在模块内未设定分类';
        } else {

            $typesRes = DB::name('system_types')->where("(parentid=$parentid) and status=1")->field("id,name")->order("sort asc,id asc")->select();
        }

        $typesRes = array_column($typesRes,'name','id');

        if ($info['setup']['inputtype']=='select') {
            return CreateFromControl::select($typesRes, array('name' => $field, 'option_null' =>'', 'select_val' => $value));
        }elseif($info['setup']['inputtype']=='checkbox') {
             $valueArr = explode(",",$value);
             $_html = '';
             foreach ($typesRes as $k=>$v) {
                 $_checkStatus = (in_array($k,$valueArr))?" checked ":'';
                 $_html .= "<label><input type='checkbox'  name='{$field}[]' value='{$k}' {$_checkStatus} {$validate} />&nbsp;". $v."</label>&nbsp;&nbsp;" ;
             }
             echo $_html;
        }

    }


    public function title($info,$value){
        $info['setup']=is_array($info['setup']) ? $info['setup'] : string2array($info['setup']);
        $thumb=$info['setup']['thumb'];
        $style=$info['setup']['style'];
        $id = $field = $info['field'];
        $validate = getvalidate($info);
        $value = $value[$info['field']];

        $value = $value ? $value : $this->data[$field];

//        $title_style = explode(';',$this->data['title_style']);
//        $style_color = explode(':',$title_style[0]);
//        $style_color = $style_color[1];
//        $style_bold = explode(':',$title_style[1]);
//        $style_bold = $style_bold[1];
//
//        if(empty($info['setup']['upload_maxsize'])){
//            $info['setup']['upload_maxsize'] =  intval(byte_format(config("config.attach_maxsize")));
//        }

//prt($value['title']);

//        $maxhom_auth_key = sysmd5(config('config.ADMIN_ACCESS').$_SERVER['HTTP_USER_AGENT']);
//        $maxhom_auth = authcode($this->isadmin.'-1-1-1-jpeg,jpg,png,gif-'.$info['setup']['upload_maxsize'].'-'.$info['moduleid'], 'ENCODE',$maxhom_auth_key);
//        $thumb_ico = $this->data['thumb']? $this->data['thumb'] : __ROOT__.'/Public/Images/admin_upload_thumb.png';
//        $boldchecked= $style_bold=='bold' ? 'checked' : '';
//        $thumbstr ='<div class="thumb_box"  id="thumb_box"><div id="thumb_aid_box"></div>
//				<a href="javascript:swfupload(\'thumb_uploadfile\',\'thumb\',\''.L('uploadfiles').'\','.$this->isadmin.',1,1,1,\'jpeg,jpg,png,gif\','.$info['setup']['upload_maxsize'].','.$info['moduleid'].',\''.$maxhom_auth.'\',yesdo,nodo)"><img src="'.$thumb_ico.'" id="thumb_pic" ></a><br> <input type="button" value="'.L('clean_thumb').'" onclick="javascript:clean_thumb(\'thumb\');" class="button" />
//				<input type="hidden"  id="thumb" name="thumb"  value="'.$this->data['thumb'].'" /></div>';

        $parseStr   = '<input type="text"   class="input-text input-title f_l" name="'.$field .'"  placeholder="'.$info['placeholder'] .'"  id="'.$id.'" value="'.$value.'" size="'.$info['setup']['size'].'"  '.$validate.'  /> ';

//        $stylestr = '<div id="'.$id.'_colorimg" class="colorimg" style="background-color:'.$style_color.'"><img src="__PUBLIC__/Images/admin_color_arrow.gif"></div><input type="hidden" id="'.$id.'_style_color" name="style_color" value="'.$style_color.'" /><input type="checkbox" class="style_bold" id="style_bold" name="style_bold" value="bold" '.$boldchecked.' /><b>'. L('style_bold').'</b><script>$.showcolor("'.$id.'_colorimg","'.$id.'_style_color");</script>';
//        if($thumb &&  $this->doThumb)$parseStr = $thumbstr.$parseStr;

        // if($style) $parseStr = $parseStr ;
        return $parseStr;
    }

    public function textarea($info,$value){
        $info['setup']=is_array($info['setup']) ? $info['setup'] : string2array($info['setup']);
        $id = $field = $info['field'];
        $validate = getvalidate($info);
        $info['setup']['ispassword'] ? $inputtext = 'password' : $inputtext = 'text';
        $value = $value[$info['field']];
        if(ACTION_NAME=='add'){
            $value = $value ? $value : $info['setup']['default'];
        }else{
            $value = $value ? $value : $this->data[$field];
        }

        $info['setup']['rows']>0 && $attr['rows'] = " rows='{$info['setup']['rows']}'";
        $info['setup']['cols']>0 && $attr['cols'] = " cols='{$info['setup']['cols']}'";


        $parseStr = '<textarea class="input-text ' . $info['class'] .'" title="'.$info['divtitle']. '" placeholder="' .$info['placeholder'] . '" name="' . $field . '"  id="' . $id . '"' .implode(" ",$attr) .  $validate . '>'.htmlspecialchars($value).'</textarea>';
        return $parseStr;
    }

    public function text($info,$value){
        $info['setup']=is_array($info['setup']) ? $info['setup'] : string2array($info['setup']);
        $id = $field = $info['field'];

        $validate = getvalidate($info);

        $info['setup']['ispassword'] ? $inputtext = 'password' : $inputtext = 'text';
        $value = $value[$info['field']];
        if(ACTION_NAME=='add'){
            $value = $value ? $value : $info['setup']['default'];
        }else{
            $value = $value ? $value : $this->data[$field];
        }

        $placeholder = $info['placeholder'];

        $parseStr = '<input type="' . $inputtext . '"   class="input-text ' . $info['class'] .'" title="'.$info['divtitle']. '" placeholder="' . $placeholder . '" name="' . $field . '"  id="' . $id . '" value="' . stripcslashes($value) . '" size="' . $info['setup']['size'] . '"  ' . $validate . '/> ';

        return $parseStr;
    }

    public function select($info,$value){
        $info['setup']=is_array($info['setup']) ? $info['setup'] : string2array($info['setup']);
        $id = $field = $info['field'];
        $validate = getvalidate($info);

        if(ACTION_NAME=='add'){
            $value = $value ? $value : $info['setup']['default'];
        }else{
            $value = $value ? $value : $this->data[$field];
        }
 
        if($value != '') $value = strpos($value, ',') ? explode(',', $value) : $value;

        if(is_array($info['options'])){
            if($info['options_key']){
                $options_key=explode(',',$info['options_key']);
                foreach((array)$info['options'] as $key=>$res){
                    if($options_key[0]=='key'){
                        $optionsarr[$key]=$res[$options_key[1]];
                    }else{
                        $optionsarr[$res[$options_key[0]]]=$res[$options_key[1]];
                    }
                }
            }else{
                $optionsarr = $info['options'];
            }
        }else{
            $options    = $info['setup']['options'];
            $options = explode("\n",$info['setup']['options']);
            foreach($options as $r) {
                $v = explode("|",$r);
                $k = trim($v[1]);
                $optionsarr[$k] = $v[0];
            }
        }


        if(!empty($info['setup']['multiple'])) {
            $parseStr = '<select id="'.$id.'" name="'.$field.'"  onchange="'.$info['setup']['onchange'].'" class="'.$info['class'].'"  '.$validate.' size="'.$info['setup']['size'].'" multiple="multiple" >';
        }else {
            $parseStr = '<select id="'.$id.'" name="'.$field.'" onchange="'.$info['setup']['onchange'].'"  class="'.$info['class'].'" '.$validate.'>';
        }

        if(is_array($optionsarr)) {
            foreach($optionsarr as $key=>$val) {
                if(!empty($value)){
                    $selected='';
                    if($value==$key || in_array($key,$value)) $selected = ' selected="selected"';
                    $parseStr   .= '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
                }else{
                    $parseStr   .= '<option value="'.$key.'">'.$val.'</option>';
                }
            }
        }
        $parseStr   .= '</select>';
        return   ($parseStr);
    }


    public function checkbox($info,$value){
        $info['setup']=is_array($info['setup']) ? $info['setup'] : string2array($info['setup']);
        $id = $field = $info['field'];
        $validate = getvalidate($info);
        if(ACTION_NAME=='add'){
            $value = $value ? $value : $info['setup']['default'];
        }else{
            $value = $value ? $value : $this->data[$field];
        }
        $labelwidth = $info['setup']['labelwidth'];


        if(is_array($info['options'])){
            if($info['options_key']){
                $options_key=explode(',',$info['options_key']);
                foreach((array)$info['options'] as $key=>$res){
                    if($options_key[0]=='key'){
                        $optionsarr[$key]=$res[$options_key[1]];
                    }else{
                        $optionsarr[$res[$options_key[0]]]=$res[$options_key[1]];
                    }
                }
            }else{
                $optionsarr = $info['options'];
            }
        }else{
            $options    = $info['setup']['options'];
            $options = explode("\n",$info['setup']['options']);
            foreach($options as $r) {
                $v = explode("|",$r);
                $k = trim($v[1]);
                $optionsarr[$k] = $v[0];
            }
        }
        if($value != '') $value = (strpos($value, ',') && !is_array($value)) ? explode(',', $value) :  $value ;
        $value = is_array($value) ? $value : array($value);
        $i = 1;
        $onclick = $info['setup']['onclick'] ? ' onclick="'.$info['setup']['onclick'].'" ' : '' ;

        foreach($optionsarr as $key=>$r) {

            $key = trim($key);
            if($i>1) $validate='';
            $checked = ($value && in_array($key, $value)) ? 'checked' : '';
            if($labelwidth) $parseStr .= '<label style="float:left;width:'.$labelwidth.'px" class="checkbox_'.$id.'" >';
            $parseStr .= '<input type="checkbox" class="input_checkbox '.$info['class'].'" title="'.$info['divtitle'].'" name="'.$field.'[]" id="'.$id.'_'.$i.'" '.$checked.$onclick.' value="'.htmlspecialchars($key).'"  '.$validate.'> '.htmlspecialchars($r);
            if($labelwidth) $parseStr .= '</label>';
            $i++;
        }
        return $parseStr;

    }


    public function radio($info,$value){

        $info['setup']=is_array($info['setup']) ? $info['setup'] : string2array($info['setup']);
        $id = $field = $info['field'];
        $validate = getvalidate($info);
        $value = $value[$field];
        if(ACTION_NAME=='add'){
            $value = $value ? $value : $info['setup']['default'];
        }else{
            $value = $value ? $value : $this->data[$field];
        }
        $labelwidth = $info['setup']['labelwidth'];

        if(is_array($info['options'])){
            if($info['options_key']){
                $options_key=explode(',',$info['options_key']);
                foreach((array)$info['options'] as $key=>$res){
                    if($options_key[0]=='key'){
                        $optionsarr[$key]=$res[$options_key[1]];
                    }else{
                        $optionsarr[$res[$options_key[0]]]=$res[$options_key[1]];
                    }
                }
            }else{
                $optionsarr = $info['options'];
            }
        }else{
            $options    = $info['setup']['options'];
            $options = explode("\n",$info['setup']['options']);
            foreach($options as $r) {
                $v = explode("|",$r);
                $k = trim($v[1]);
                $optionsarr[$k] = $v[0];
            }
        }

        $onclick = $info['setup']['onclick'] ? ' onclick="'.$info['setup']['onclick'].'" ' : '' ;

        $i = 1;
        foreach($optionsarr as $key=>$r) {
            if($i>1) $validate ='';
            $checked = trim($value)==trim($key) ? 'checked' : '';
            if(empty($value) && empty($key) ) $checked = 'checked';
            if($labelwidth) $parseStr .= '<label style="float:left;width:'.$labelwidth.'px" class="checkbox_'.$id.'" >';
            $parseStr .= '<input type="radio" class="input_radio '.$info['class'].'" title="'.$info['divtitle'].'" name="'.$field.'" id="'.$id.'_'.$i.'" '.$checked.$onclick.' value="'.$key.'" '.$validate.'> '.L($r);
            if($labelwidth) $parseStr .= '</label>';
            $i++;
        }
        return $parseStr;
    }

    public function datetime($info,$value){

        $info['setup']=is_array($info['setup']) ? $info['setup'] : string2array($info['setup']);
        $id = $field = $info['field'];
        $value = $value[$info['field']];
        $validate = getvalidate($info);
        if(ACTION_NAME=='add'){
            $value = $value ? $value : $info['setup']['default'];
        }else{
            $value = $value ? $value : $this->data[$field];
        }
        $value = $value ?  toDate($value,"Y-m-d H:i:s") : toDate(time(),"Y-m-d H:i:s");

        $parseStr = '<span id="'.$id.'View"></span><input  class="input-text  '.$info['class'].'"  '.$validate.'  name="'.$field.'" type="text" id="'.$id.'" size="25" value="'.$value.'"  />';
        $parseStr .= <<<EOF
        <script>
           layui.use('laydate', function(){
              var laydate = layui.laydate;
              laydate.render({
                elem: '#{$id}'
                ,type: 'datetime'
              });
           });
</script>
EOF;
        return $parseStr;
    }

    public function date($info,$value){

        $info['setup']=is_array($info['setup']) ? $info['setup'] : string2array($info['setup']);
        $id = $field = $info['field'];
        $value = $value[$info['field']];
        $validate = getvalidate($info);
        if(ACTION_NAME=='add'){
            $value = $value ? $value : $info['setup']['default'];
        }else{
            $value = $value ? $value : $this->data[$field];
        }
        $value = $value ?  toDate($value,"Y-m-d") : toDate(time(),"Y-m-d");

        $parseStr = '<span id="'.$id.'View"></span><input  class="input-text  '.$info['class'].'"  '.$validate.'  name="'.$field.'" type="text" id="'.$id.'" size="25" value="'.$value.'"  />';
        $parseStr .= <<<EOF
        <script>
           layui.use('laydate', function(){
              var laydate = layui.laydate;
              laydate.render({
                elem: '#{$id}'
                ,type: 'date'
              });
           });
</script>
EOF;
        return $parseStr;
    }


    public function number($info,$value){
        $info['setup']=is_array($info['setup']) ? $info['setup'] : string2array($info['setup']);
        $id = $field = $info['field'];
        $validate = getvalidate($info);
        $value = $value[$info['field']];
        $info['setup']['ispassowrd'] ? $inputtext = 'passowrd' : $inputtext = 'number';
        if(ACTION_NAME=='add'){
            $value = $value ? $value : $info['setup']['default'];
        }else{
            $value = $value ? $value : $this->data[$field];
        }

        $parseStr   = '<input type="'.$inputtext.'"   class="input-text '.$info['class'].'" title="'.$info['divtitle'] .'" placeholder="'.$info['placeholder'].'" name="'.$field.'"  id="'.$id.'" value="'.$value.'" size="'.$info['setup']['size'].'"  '.$validate.'/> ';

        return $parseStr;
    }


    public function images_del($info,$value){

        $system_module = Db::table("system_module")->column("*","mo_uuid");

        $info['setup']=is_array($info['setup']) ? $info['setup'] : string2array($info['setup']);

        $id = $field = $info['field'];
        $validate = getvalidate($info);
        $value = $value[$field];
        $mo_uuid = $info['mo_uuid'];

        if(ACTION_NAME=='add'){
            $value = $value ? $value : $info['setup']['default'];
            $btn_txt = "上传图片";
        }else{
            $value = $value ? $value : $this->data[$field];
            $value && $btn_txt = "更换图片";
            $value || $btn_txt = "上传图片";
        }

        $upfile_url = url('basicadmin/upfile',array("table"=>strtolower($system_module[$mo_uuid]['name'])));
       
        $info['setup']['upload_maxsize'] =  intval(byte_format(config('config.attach_maxsize')));
        $maxhom_auth_key = sysmd5(config('config.ADMIN_ACCESS').$_SERVER['HTTP_USER_AGENT']);
        $maxhom_auth = authcode($this->isadmin.'-'.$info['setup']['more'].'-0-1-'.$info['setup']['upload_allowext'].'-'.$info['setup']['upload_maxsize'].'-'.$info['moduleid'], 'ENCODE',$maxhom_auth_key);
        if (!empty($value)) {
            $imgRes =  getAttachment(explode(",",$value));
            $parseStr  = "<div class='box_avatar_imgs'>";
            foreach ($imgRes as $k=>$v) {
                $parseStr  .= "<span style=''><img src='{$v['imagepath']['thumb']}'></span>";
            }
            $parseStr  .= "</div>";

        } else {
            $parseStr  = "<div class='box_avatar_imgs'></div>";
        }

        $parseStr   .=<<<EOF
                 <div id={$field}_aid_box></div><button type='button' class='layui-btn' id='{$id}' ><i class='layui-icon'>&#xe67c;</i>{$btn_txt}</button>
                
                        <script>
                        
                        layui.use('upload', function(){
                            var upload = layui.upload;
                            var uploadInst = upload.render({
                     elem: '#{$id}' //绑定元素
                    ,acceptMime: 'image/*'                    
                    ,accept: 'images'                    
                    ,exts:'jpg|png|gif|bmp|jpeg'
                    ,url: "{$upfile_url}" //上传接口
                    ,done: function(res){
                                //上传完毕回调
                                debugger;
                                if (res.code!=1) {
                                    alert(res.msg);
                                    return false;
                                }else {
                                    _objImg = eval("("+res.data+")");
                                    for(i in _objImg) {   
                                        //多图
                                        // $(".box_avatar_imgs").append("<span><img src='"+_objImg[i].imagepath.s+"'><input type='hidden' name='{$field}[]' value='"+_objImg[i].aid+"'></span>");
                                        //单图
                                        $(".box_avatar_imgs").html("<span><img src='"+_objImg[i].imagepath.s+"'><input type='hidden' name='{$field}[]' value='"+_objImg[i].aid+"'></span>");
                                    }
                                }
                            }
                    ,error: function(){
                                //请求异常回调
                            }
                  });
                });
                </script>        
EOF;
        return $parseStr;
    }

    public function images($info,$value){

        $system_module = Db::table("system_module")->column("*","mo_uuid");

        $info['setup']=is_array($info['setup']) ? $info['setup'] : string2array($info['setup']);
        $id = $field = $info['field'];
        $validate = getvalidate($info);
        $value = $value[$field];
        $mo_uuid = $info['mo_uuid'];

        $upload_maxnumb = $info['setup']['upload_maxnum']?$info['setup']['upload_maxnum']:1;
        $upload_maxsize = $info['setup']['upload_maxsize']?$info['setup']['upload_maxsize']:(config("config.attach_maxsize") );
        $upload_maxsize = str_replace(array("M","G","K"),'',$upload_maxsize);

        if(ACTION_NAME=='add'){
            $value = $value ? $value : $info['setup']['default'];
            $btn_txt = "上传图片[最多{$upload_maxnumb}张，字节限{$upload_maxsize}MB]";
        }else{
            $value = $value ? $value : $this->data[$field];
            $value && $btn_txt = "更换图片[最多{$upload_maxnumb}张，字节限{$upload_maxsize}MB]";
            $value || $btn_txt = "上传图片[最多{$upload_maxnumb}张，字节限{$upload_maxsize}MB]";
        }

        $upfile_url = url('basicadmin/upfile',array("table"=>strtolower($system_module[$mo_uuid]['name']),"upload_maxsize"=>$upload_maxsize));


        $maxhom_auth_key = sysmd5(config('config.ADMIN_ACCESS').$_SERVER['HTTP_USER_AGENT']);
        $maxhom_auth = authcode($this->isadmin.'-'.$info['setup']['more'].'-0-1-'.$info['setup']['upload_allowext'].'-'.$info['setup']['upload_maxsize'].'-'.$info['moduleid'], 'ENCODE',$maxhom_auth_key);
        if (!empty($value)) {
            $imgRes =  getAttachment(explode(",",$value));

            $parseStr  = "<div class='box_avatar_imgs {$id}'>";
            $parseStr  .= "<input type='hidden' name='{$field}[]' class='upimage_tbid' value=''>";
            foreach ($imgRes as $k=>$v) {
                $parseStr  .= "<span style=\"width: 130px;height:150px;float: left;border: 5px solid #ccc;border-radius: 8px;margin-bottom: 10px;margin-right: 10px;\" ><img src='{$v['imagepath']['thumb']}' style=\"width: 100%;\" >";

                $parseStr .= "<input type='hidden' name='{$field}[]' class='upimage_tbid' value='{$v["aid"]}'><div class='imgs_delete' upimage_tbid='{$v["aid"]}' style='height: 20px; text-align: center;background-color: #eae9e9;cursor: pointer;'>删除</div></span>";

            }

            $parseStr  .= "</div>";

        } else {
            $parseStr  = "<div class='box_avatar_imgs {$id}'></div>";
        }

        $parseStr   .=<<<EOF
                 <div id={$field}_aid_box></div><div class="layui-clear"><button type='button' class='layui-btn layui-clear' id='{$id}' upload_maxnumb="{$upload_maxnumb}"  ><i class='layui-icon'>&#xe67c;</i>{$btn_txt}</button></div>
 
                        <script>
                        
                        layui.use('upload', function(){
                           
                            var upload = layui.upload;
 
                            var uploadInst = upload.render({
                     elem: '#{$id}' //绑定元素
                   // ,acceptMime: 'image/jpg|png|gif|bmp|jpeg'                    
                    ,accept: 'images'                    
                    ,exts:'jpg|png|gif|bmp|jpeg'
                    ,size: {$upload_maxsize}*1000*1024
                    ,url: "{$upfile_url}" //上传接口
                     ,before: function(obj){
                            
                             if ($(".box_avatar_imgs.{$id}").find("img").length >=  $("#{$id}").attr("upload_maxnumb")-0) {
                                alert("上传文件数量大于最大数"+ $("#{$id}").attr("upload_maxnumb")+"张");
                                throw new Error("上传文件数量大于最大数");  
                            }
                        }
                    ,done: function(res){
                                //上传完毕回调
                         
                                if (res.code!=1) {
                                    alert(res.msg);
                                    return false;
                                }else {
                                    _objImg = eval("("+res.data+")");
                                    for(i in _objImg) {   
                                        //多图
                                         $(".box_avatar_imgs.{$id}").append("<span style='width: 130px;height:150px;float: left;border: 5px solid #ccc;border-radius: 8px;margin-bottom: 10px;margin-right: 10px;'><img src='"+_objImg[i].imagepath.m+"'><input type='hidden' name='{$field}[]' class='upimage_tbid' value='"+_objImg[i].aid+"'><div class='imgs_delete' upimage_tbid='"+_objImg[i].aid+"' style='height: 20px; text-align: center;background-color: #eae9e9;cursor: pointer;'>删除</div></span>");                                        
                                    }
                                }
                            }
                    ,error: function(){
                                //请求异常回调
                            }
                  });
                });
                        
                        $(document).on("click",".imgs_delete",function(_res) {                 
                             _self = $(this);
                             if (_self.parents("span").length>0)   _self.parents("span").remove()                                
                        })
                </script>        
EOF;
        return $parseStr;
    }

    public function editor($info,$value){


        $info['setup']=is_array($info['setup']) ? $info['setup'] : string2array($info['setup']);
        $id = $field = $info['field'];
        $validate = getvalidate($info);
        $value = $value[$info['field']];
        if(ACTION_NAME=='add'){
            $value = $value ? $value : $info['setup']['default'];
        }else{
            $value = $value ? $value : $this->data[$field];
        }
        //$value = stripslashes(htmlspecialchars_decode($value));

        $textareaid = $field;
        $toolbar = $info['setup']['toolbar'];
        $moduleid = $info['moduleid'];
        $height = $info['setup']['height'] ? $info['setup']['height'] : 300;
        $flashupload = $info['setup']['flashupload']==1 ? 1 : '';
        $alowuploadexts = $info['setup']['alowuploadexts'] ? $info['setup']['alowuploadexts'] :  'jpg,gif,png';
        $alowuploadlimit=$info['setup']['alowuploadlimit'] ? $info['setup']['alowuploadlimit'] : 20 ;
        $show_page=$info['setup']['showpage'];


        $file_size = intval(byte_format(config('config.attach_maxsize')));

        $toolbarStr = "[['fullscreen','source','undo','redo','bold','italic','underline','fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor','insertorderedlist','insertunorderedlist','selectall', 'cleardoc']]";
        $toolbarStr = "[['fullscreen','source','selectall', 'cleardoc']]";


        if ($info['setup']['toolbar']=='full') {
            $toolbarStr = "[['source','anchor','undo','redo','bold','indent','snapscreen','italic','underline','strikethrough','subscript','fontborder','superscript','formatmatch','blockquote','pasteplain','selectall','print','preview','horizontal','removeformat','time','date','unlink','insertrow','insertcol','mergeright','mergedown','deleterow','deletecol','splittorows','splittocols','splittocells','deletecaption','inserttitle','mergecells','deletetable','cleardoc','insertparagraphbeforetable','insertcode','fontfamily','fontsize','paragraph','simpleupload','insertimage','edittable','edittd','link','emotion','spechars','searchreplace','map','gmap','insertvideo','help','justifyleft','justifyright','justifycenter','justifyjustify','forecolor','backcolor','insertorderedlist','insertunorderedlist','fullscreen','directionalityltr','directionalityrtl','rowspacingtop','rowspacingbottom','pagebreak','insertframe','imagenone','imageleft','imageright','attachment','imagecenter','wordimage','lineheight','edittip ','customstyle','autotypeset','webapp','touppercase','tolowercase','background','template','scrawl','music','inserttable','drafts']]";

        }
        //prt($toolbarStr);
        // $toolbarStr = "'fullscreen', 'source', 'undo', 'redo','bold', 'italic', 'underline',   'selectall', 'cleardoc'";
//            $toolbars   = explode(",",$toolbarStr);
//            $toolbars2 = array_flip($toolbars);
//            //关掉
//            unset($toolbars2["'map'"],$toolbars2["'gmap'"]);   //地图
//            unset($toolbars2["'simpleupload'"],$toolbars2["'insertimage'"]);   //图上传
//            unset($toolbars2["'insertcode'"]);   //代码语言

        // $toolbars3 = array_flip($toolbars2);

        //  $toolbarsJson = "[['fullscreen','source','undo','redo','bold','cleardoc']]" ;
        //prt($toolbarsJson);


        $toolbarStr2 = "'fullscreen','source','undo','redo','bold','italic','underline','fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor','insertorderedlist','insertunorderedlist','selectall', 'cleardoc'";

        if ($info['setup']['toolbar']=='full') {
            $toolbarStr2 = "'source','anchor','undo','redo','bold','indent','snapscreen','italic','underline','strikethrough','subscript','fontborder','superscript','formatmatch','blockquote','pasteplain','selectall','print','preview','horizontal','removeformat','time','date','unlink','insertrow','insertcol','mergeright','mergedown','deleterow','deletecol','splittorows','splittocols','splittocells','deletecaption','inserttitle','mergecells','deletetable','cleardoc','insertparagraphbeforetable','insertcode','fontfamily','fontsize','paragraph','simpleupload','insertimage','edittable','edittd','link','emotion','spechars','searchreplace','map','gmap','insertvideo','help','justifyleft','justifyright','justifycenter','justifyjustify','forecolor','backcolor','insertorderedlist','insertunorderedlist','fullscreen','directionalityltr','directionalityrtl','rowspacingtop','rowspacingbottom','pagebreak','insertframe','imagenone','imageleft','imageright','attachment','imagecenter','wordimage','lineheight','edittip ','customstyle','autotypeset','webapp','touppercase','tolowercase','background','template','scrawl','music','inserttable','drafts'";

        }
        $parseStr ='';

//            if ($_COOKIE['ueditor_load']!=date('i')) {
//                setcookie("ueditor_load", date('i'));
//                prt("test",0);
//                $parseStr .= "<script type='text/javascript' charset='utf-8' src='{$this->appRoot}/static/ueditor-1.4.3.3/ueditor.config.js'></script>";
//                $parseStr .= "<script type='text/javascript' charset='utf-8' src='{$this->appRoot}/static/ueditor-1.4.3.3/ueditor.all.js?v=2.34'></script>";
//                $parseStr .= "<script type='text/javascript' charset='utf-8' src='{$this->appRoot}/static/ueditor-1.4.3.3/lang/zh-cn/zh-cn.js'></script>";
//            }

        $parseStr .= "<script type='text/javascript' charset='utf-8' src='{$this->appRoot}/static/ueditor-1.4.3.3/ueditor.config.js'></script>";
        $parseStr .= "<script type='text/javascript' charset='utf-8' src='{$this->appRoot}/static/ueditor-1.4.3.3/ueditor.all.js?v=2.34'></script>";
        $parseStr .= "<script type='text/javascript' charset='utf-8' src='{$this->appRoot}/static/ueditor-1.4.3.3/lang/zh-cn/zh-cn.js'></script>";
        $parseStr .= <<<EOF
              <script id="{$textareaid}" name="{$textareaid}" type="text/plain" style="width:1024px;height:{$height};">{$value}</script>             
              <script type="text/javascript">               
                var ue = UE.getEditor('{$textareaid}',{
                     toolbars: [[{$toolbarStr2}]],
                        autoHeightEnabled: true,
                        autoFloatEnabled: true                       
                });
                 
            </script>   
EOF;
        return $parseStr;

    }


    public function regional_Linkage($info,$value) {


        $regional  = explode(",",$value['regional_Linkage']);
        if (false!==strpos($regional[0],"p:")) {  //省
            $arr = explode(":",$regional[0]);
            $province_select_val = $arr[1];
        }
        if (false!==strpos($regional[1],"c:")) {  //市
            $arr = explode(":",$regional[1]);
            $city_select_val = $arr[1];
        }

        if (false!==strpos($regional[2],"d:")) {    //县，区
            $arr = explode(":",$regional[2]);
            $d_select_val = $arr[1];
        }


       if ($info['field']=='regional_Linkage') {
            $component = new Component();
            echo $component->province('edit',$province_select_val,$city_select_val,$d_select_val);
        }

    }

}
