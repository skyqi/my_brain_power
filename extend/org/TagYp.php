<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

namespace org;
use think\template\TagLib;
use think\Exception;
use think\db;
/**
 * CX标签库解析类
 * @category   Think
 * @package  Think
 * @subpackage  Driver.Taglib
 * @author    liu21st <liu21st@gmail.com>
 */
class TagYp extends TagLib
{
    protected $tags   =  array(
        //attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
        'list'=>array('attr'=>'name,field,limit,order,catid,thumb,posid,where,sql,key,page,mod,id,ids,status','level'=>3),
        'subcat'=>array('attr'=>'catid,type,self,key,id','level'=>3),
        'catpos' => array('attr'=>'catid,space','close'=>0),
        'nav' => array('attr'=>'catid,bcid,level,id,class,home,enhomefont','close'=>0),

        'link' => array('attr'=>'typeid,linktype,field,limit,order,field','level'=>3),

        'block' => array('attr'=>'blockid,pos,key,id','close'=>0),
        'flash' => array('attr'=>'flashid,key,mod,id','close'=>0),
        'tags' => array('attr'=>'keywords,list,key,mod,moduleid,id,limit,order','close'=>3),
        'pre' => array('attr'=>'blank,msg','close'=>0),
        'next' => array('attr'=>'blank,msg','close'=>0),
        'sql' => array('attr'=>'name,field,limit,order,catid,where,sql,key,page,mod,id,ids,status','close'=>1),
    );

    /**
     * TagLib标签属性分析 返回标签属性数组
     * @access public
     * @param string $tagStr 标签内容
     * @return array
     */
    public function parseXmlAttr($attr,$tag) {
        //XML解析安全过滤
        if (is_array($attr)) {
            foreach ($attr as $k=>$v) {
                $attrStr .=" $k='{$v}'";
            }
        }
      //  $attr   =   str_replace('&','___', $attr);
     //   prt($xml);

        $xml    =   '<tpl><tag '.$attrStr.' /></tpl>';
        $xml    =   simplexml_load_string($xml);

        if(!$xml) {
            \exception(L('_XML_TAG_ERROR_').' : '.$attr);
        }
        $xml    =   (array)($xml->tag->attributes());
        $array  =   array_change_key_case($xml['@attributes']);
        if($array) {
            $attrs  = explode(',',$this->tags[strtolower($tag)]['attr']);
            if(isset($this->tags[strtolower($tag)]['must'])){
                $must   =   explode(',',$this->tags[strtolower($tag)]['must']);
            }else{
                $must   =   array();
            }
            foreach($attrs as $name) {
                if( isset($array[$name])) {
                    $array[$name] = str_replace('___','&',$array[$name]);
                }elseif(false !== array_search($name,$must)){
                    \exception(L('_PARAM_ERROR_').':'.$name);
                }
            }
            return $array;
        }
    }
    




    public function tagblock($attr,$content) {
        $tag    = $this->parseXmlAttr($attr,'block');
        $id = !empty($tag['id'])?$tag['id']:'r';  //定义数据查询的结果存放变量
        $key    = !empty($tag['key'])?$tag['key']:'i';
        $pos   = !empty($tag['pos'])? $tag['pos'] : '';
        $mod    = isset($tag['mod'])?$tag['mod']:'2';
        $blockid   = !empty($tag['blockid'])? $tag['blockid'] : '';

        if($blockid && !is_numeric($blockid)){
            if(substr($blockid,0,2)=='T['){
                $T = $this->tpl->get('T');
                preg_match_all("/T\[(.*)\]$/",$blockid,$arr);
                $blockid=$T[$arr[1][0]];
            }else{
                $blockid= $this->tpl->get($blockid);
            }
        }


        $where = ' 1 ';

//        if(APP_LANG){
//            $lang=$this->tpl->get('langid');
//            $where .= ' and lang='.$lang;
//        }

        if($pos) $where .=" and pos='$pos' ";
        if($blockid) $where .=" and id=$blockid ";
        $r = \think\db::table('Block')->where($where)->find();
        return  $r['content'];
    }


    public function taglist($attr,$content) {

        $tag    = $this->parseXmlAttr($attr,'list');
        $id = !empty($tag['id'])?$tag['id']:'r';  //定义数据查询的结果存放变量
        $key    = !empty($tag['key'])?$tag['key']:'i';
        $page   = !empty($tag['page'])? '1' : '0';
        $mod    = isset($tag['mod'])?$tag['mod']:'2';


        if ($tag['table'])
        {   //根据用户输入的值拼接查询条件
            $sql='';
            $module = $tag['table'];
            $order  = isset($tag['order'])?$tag['order']:'id desc';
            $field  = isset($tag['field'])?$tag['field']:'*';
            $where  = isset($tag['where'])?$tag['where']: ' 1 ';
            $limit  = isset($tag['limit'])?$tag['limit']: '10';
            $status = isset($tag['status'])? intval($tag['status']) : '1';



//            if($tag['catid']){
//                $onezm  = substr($tag['catid'],0,1);
//                if(substr($tag['catid'],0,2)=='T['){
//                    $T = $this->tpl->get('T');
//                    preg_match_all("/T\[(.*)\]$/",$tag['catid'],$cidarr);
//                    $catid=$T[$cidarr[1][0]];
//                }elseif(!is_numeric($onezm)) {
//                    $catid = $this->tpl->get($tag['catid']);
//
//                }else{
//                    $catid = $tag['catid'];
//                }

//                if(is_numeric($catid)){
//                    $category_arr = $this->tpl->get('Categorys');
//                    $module = $category_arr[$catid]['module'];
//                    if(!$module) return '';
//                    if($category_arr[$catid]['child']){
//                        $where .= " AND catid in(".$category_arr[$catid]['arrchildid'].")";
//                    }else{
//                        $where .=  " AND catid=".$catid;
//                    }
//                }elseif($onezm=='$') {
//                    $where .=  ' AND catid in('.$tag['catid'].')';
//                }else{
//                    $where .=  ' AND catid in('.strip_tags($tag['catid']).')';
//                }
 //           }
            unset($category_arr);

//            if($tag['posid']){
//                $posid = $tag['posid'];
//                if(!is_numeric($posid) && substr($posid,0,2)=='T['){
//                    $T = $this->tpl->get('T');
//                    preg_match_all("/T\[(.*)\]$/",$posid,$cidarr);
//                    $posid=$T[$cidarr[1][0]];
//                }
//                if(is_numeric($posid)){
//                    $where .=  '  AND posid ='.$posid;
//                }else{
//                    $where .=  ' AND posid in('.strip_tags($posid).')';
//                }
//            }


            $sql  = "DB::table(\"{$module}\")->field(\"{$field}\")->where(\"{$where}\")->order(\"{$order}\")->limit(\"{$limit}\")->select();";
        }else{
            if (!$tag['sql']) return ''; //排除没有指定model名称，也没有指定sql语句的情况
            $sql = "DB::query(\"{$tag['sql']}\")";
        }

        //下面拼接输出语句
        $parsestr  = '';
        $parsestr .= '<?php  $_result='.$sql.'; if ($_result): $'.$key.'=0;';
        $parsestr .= 'foreach($_result as $key=>$'.$id.'):';
        $parsestr .= '++$'.$key.';$mod = ($'.$key.' % '.$mod.' );?>';
        $parsestr .= $content;//解析在article标签中的内容
        $parsestr .= '<?php endforeach; endif;?>';
        return  $parsestr;
    }

    #万能get.sql;
    public function tagsql($attr,$content) {
        //prt($attr,0);
        $tag = $this->parseXmlAttr($attr);
        $id = !empty($tag['id'])?$tag['id']:'r';  //输出句柄;
        $table   = !empty($tag['table'])?$tag['table']:'Article'; //默认文章表;
        $status   = !empty($tag['status'])?$tag['status']:false; //id倒序;
        $where   = !empty($tag['where'])?$tag['where']:'1'; //默认是全部;
        $field   = !empty($tag['field'])?$tag['field']:'*'; //默认是全部;
        $limit   = !empty($tag['limit'])?$tag['limit']:'0,10';//默认是10条;
        $order   = !empty($tag['order'])?$tag['order']:'id DESC'; //id倒序; 

        $mod    = isset($tag['mod'])?$tag['mod']:'2'; //轮换输出;

        if ($status) {
            $where .=" and status={$tag['status']}";
        }

        $sql = "\\think\\db::table('$table')->where('$where')->field('$field')->limit('$limit')->order('$order')->select()";
        //prt($sql,0);
        //下面拼接输出语句
        $parsestr  = '';
        $parsestr .= "<?php \$_result=$sql;";
        $parsestr .= 'if ($_result): $key=0;';
        $parsestr .= 'foreach($_result as $key=>$'.$id.'):';
        $parsestr .= '++$key;$mod = ($key % $mod);?>';
        $parsestr .= $content;//前台标签,内置;
        $parsestr .= '<?php endforeach; endif;?>';
        return  $parsestr;
    }

    

}