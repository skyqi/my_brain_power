<?php

namespace org;
use service\DataService;
use think\Controller;
use think\Db;
use think\db\Query;
use org\TreeClass;

class CreateFromControl extends Controller {
    /**
     * 自动生成form中的select表单
     * edit by www.jbxue.com
     */
    static public function select($msg_list,$attr,$debug){

        $arr_list=$msg_list;
        if(!is_array($arr_list)) return "";
        $str_return="";
        $multiple = '';
        $size = '';
        $id = '';
        $name = '';
        $class = '';
        $select_val = '';
        $disabled = '';
        if ($attr['class']) $class2[] = $attr['class'];
        if ($attr['required']) $class2[] = 'required';

        if ($attr['multiple']) $multiple = ' multiple="multiple" ';
        if ($attr['size']) $size = " size='".$attr['size']."'";
        if ($attr['name']) $name = " name='".$attr['name']."'";
        if ($attr['id']) $id = " id='".$attr['id']."'";
        if ($class2) $class = " class='".implode(" ",$class2)."'";
        if (!empty($attr['select_val'])) $select_val = $attr['select_val'];
        if (!empty($attr['disabled'])) $disabled = " disabled='true' ";
        if (isset($attr['option_null']))  {
            $option_null_val = $attr['option_null'];
            $str_return ="<option value='{$option_null_val}'>请选择</option>";
        }

        foreach($arr_list as $k=>$item){
            $str_sel="";
            if($k==$select_val) {
                $str_sel=" selected ";
            }
            $str_return.="<option value='".$k."' ".$str_sel.">".trim($item)."</option>";
        }

        $_select = "<select {$multiple} {$size} {$name} {$id} {$class} {$disabled}>";
        return $_select.$str_return."</select>";
    }

    static public function checkbox($attr,$title='') {

        $attr['name'] && $new['name'] = $attr['name'];
        $attr['id'] && $new['id'] = $attr['id'];
        $attr['class'] && $new['class'] = $attr['class'];
        if ($attr['value']>0) { $new['checked'] = "checked" ; $new['value']=$attr['value'];}
        $attr['disabled'] && $new['disabled']="disabled";

        foreach ($new as $k=>$v) {
            $new2[] = "{$k}='{$v}'";
        }

        $attr2 = implode(" ",$new2);
        unset($attr,$new);
        return "<input type='checkbox' {$attr2} /> ".$title;
    }

    static public  function radio($attr,$title='') {
        //http://www.layui.com/doc/element/form.html#switch
    }

    static public  function test() {
        //http://www.layui.com/doc/element/form.html#switch
        return 'rongkangsen';
    }
}

