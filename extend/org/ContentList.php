<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2017 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://think.ctolog.com
// +----------------------------------------------------------------------
// | 开源协议 ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zoujingli/ThinkAdmin
// +----------------------------------------------------------------------

namespace org;
use service\DataService;
use think\Controller;
use think\Db;

/**
 * 显示值
 
 */
class ContentList extends Controller
{

    protected $field;

    public function __construct()
    {
        parent::__construct();

    }


    public function index($value, $field)
    {

        $this->field = $field;

        if (!empty($this->field['bindModule'])) {
            return ContentList::bindModule($value);
        } else {

            if (method_exists(__CLASS__, $field['type'] . "_action")) {
                $action = $field['type'] . "_action";
                return ContentList::$action($value);
            } else {
                return htmlentities($value);
            }
        }
    }

    public function model_select_action($value)
    {
        $setup = $this->field['setup'];
        if (is_string($this->field['setup'])) {
            $setup = string2array($setup);
        }

        if (!empty($setup['bindModule'])) {
            $bindModules = explode("||", $setup['bindModule']);
            if (empty($bindModules[0]) || empty($bindModules[1])) return 'bindModule格式不对！';
            $table = trim($bindModules[0]);
            $field = trim($bindModules[1]);
            $db = DB::table($table);
            $pk = $db->getPk();

            if (false == $pk) {
                return 'bindModule绑定的表没有主键，不能使用！';
            }
            $res = $db->field($field)->cache(40, true)->find($value);
            return trim($res[$field]);
        }
    }

    public function  editor_action($value) {
            $subject = strip_tags($value);//去除html标签
            $pattern = '/\s/';//去除空白
            $content = preg_replace($pattern, '', $subject);
            return mb_substr($content, 0, 80);//截取80个汉字
    }
    /**
     * @return App
     */
    public function datetime_action($date)
    {
        if (false==$date) return false;
        if (date("Y-m-d H:i:s",strtotime($date))==$date) {
            return $date;
        } else {
            return date("Y-m-d H:i:s",$date);
        }
    }

    public function date_action($date)
    {
        if (false==$date) return false;
        $time = toTimestamp($date);
        return date("Y-m-d",$time);
    }

    /**
     * @return App
     */
    public function typeid_action($value)
    {


        $types = Db::table("system_types")->cache("system_types_cache",60)->column("*","id");

        if ($value===0) {
            return '';
        }

        $expArr= explode(",",$value);

        if (false == $types[$value] && count($expArr)>0 ) {
            //数组

            foreach ($expArr as $k=>$v) {
                $_join[] = ($k+1). "." . $types[$v]['name'] ;
            }
            if ($_join) {
                return implode(" ",$_join);
            } else
                return "error:typeid not found {$value}";
        } else {
            return  $types[$value]['name'];
        }


    }

    /**
     * @return App
     */
    public function radio_action($value)
    {
         $setup =  $this->field['setup'];
         if (is_string($this->field['setup'])) {
             $setup = string2array($setup);
         }

        $resList = fieldoption($setup['options']);
        return  $resList[$value];
    }

    /**
     * @return App
     */
    public function images_action($value)
    {
        if (false==$value) {
            return false;
        }
        $setup =  $this->field['setup'];
        if (is_string($this->field['setup'])) {
            $setup = string2array($setup);
        }

        $file = getAttachment($value);
        return  "<img src='{$file[0]['imagepath']['s']}' />";
    }

    public function regional_Linkage_action($value) {
		$data = explode(',',$value);



		foreach($data as $k=>$v){
			if($k!=0){
				$char = '';
			}else{
				$char = '';
			}
			if(is_array($arr = explode(':',$v))){
				$area .= $char.$arr[2];
			}
		}

        return $area;

    }


}